#__AW_Repository_Version '14.2.6.0000';
#__AW_Product_Version '14.2.6.1113';
#__AW_ATL_Locale 'eng_us.utf-8';
 AlGUIComment ("ActaName_1" = 'RParallelizable', "ActaName_2" = 'RisScriptValidationFunction', "ActaName_3" = 'RDate_modified', "ActaName_4" = 'RActa_user_func_category', "ActaName_5" = 'RDate_created', "ActaName_6" = 'RSavedAfterCheckOut', "ActaValue_1" = 'yes', "ActaValue_2" = 'no', "ActaValue_3" = 'Fri May 11 10:30:10 2018', "ActaValue_4" = 'User_Script_Function', "ActaValue_5" = 'Fri May 11 09:21:13 2018', "ActaValue_6" = 'YES', "InputSchema_WidthProp" = '31', "Input_1st_Column_Name" = 'Type', "Input_2nd_Column_Name" = 'Description', "Input_Column_Name_3" = 'Content_Type', "Input_Column_Name_4" = 'Business_Name', "Input_Column_Width_3" = '100', "Input_Column_Width_4" = '100', "Input_Width_Description" = '130', "Input_Width_Type" = '85', "Output_1st_Column_Name" = 'Type', "Output_2nd_Column_Name" = 'Mapping', "Output_3rd_Column_Name" = 'Description', "Output_Column_Name_4" = 'Content_Type', "Output_Column_Name_5" = 'Business_Name', "Output_Column_Width_4" = '100', "Output_Column_Width_5" = '100', "Output_Width_Decsription" = '130', "Output_Width_Mapping" = '85', "Output_Width_Type" = '80', "UpperContainer_HeightProp" = '53', "ui_script_text" = '##	Function:	BF_ExecModularJob
##	Purpose:	Executes a specified job
##	Parameters:	$PV_JobName - Name of the job
##	Returns:	1
##	Notes:		All parameters need to be in single quotes unless null.

PRINT(\'BF_ExecModularJob: Starting Job <[$PV_JobName]>.\');

$LV_SystemConfig       = current_system_configuration();
$LV_RepositoryName     = ifthenelse(word_ext(repository_name(), 1, \'.\') = \'ORACLE\', word_ext(repository_name(), -1, \'.\'), word_ext(repository_name() || \'x\', -2, \'.\'));

$LV_CMS_HostPort       = BF_GetConfigValue( \'CMS_HOST_PORT\');
$LV_CMS_Host           = word_ext($LV_CMS_HostPort, 1, \':\');
$LV_CMS_Port           = word_ext($LV_CMS_HostPort, 2, \':\');

$LV_Jobserver_HostPort = BF_GetConfigValue( \'JOBSERVER_HOST_PORT\');
$LV_Jobserver_Host     = word_ext($LV_Jobserver_HostPort, 1, \':\');
$LV_Jobserver_Port     = word_ext($LV_Jobserver_HostPort, 2, \':\');

#LV_CodePage sets the command line parameter locale. Its a boolean flag (-PLocaleUTF8 = True or blank = False).
$LV_CodePage           = \' -PLocaleUTF8 \'; 

# Get the GUID for the job name passed into the function.  
$LV_GUID               = sql(\'CURR_REPO_DS\',\'select GUID from AL_LANG where name ={$PV_JobName}\');


# Validate certain variables 

if ($LV_RepositoryName is null)  
	raise_exception(\'BF_ExecModularJob: Error - $LV_RepositoryName is Null \' || 90001);

if ($LV_CMS_Host is null)  
	raise_exception(\'BF_ExecModularJob: Error - $LV_CMS_Host is Null \' || 90002);

if (($LV_CMS_Port is null) or ($LV_CMS_Port = 0))
	raise_exception(\'BF_ExecModularJob: Error - $LV_CMS_Port is not numeric or is equal to 0 \' || 90003);

if ($LV_Jobserver_Host is null)  
	raise_exception(\'BF_ExecModularJob: Error - $LV_Jobserver_Host is Null \' || 90004);

if (($LV_Jobserver_Port is null) or ($LV_Jobserver_Port = 0))
	raise_exception(\'BF_ExecModularJob: Error - $LV_Jobserver_Port is not numeric or is equal to 0 \' || 90005);

if ($LV_GUID is null)  
	raise_exception(\'BF_ExecModularJob: Error - $LV_GUID is Null \' || 90006);


if(BF_GetJobServerOS() = \'WINDOWS\')
begin
    # Create a temporary text file that the job launcher will call when executing. 
	$LV_ParamsFilename    = \'[$PV_JobName]_[$LV_SystemConfig]_[$LV_RepositoryName].txt\';
	$LV_PathJobLauncher   = \'AL_RWJobLauncher.exe\'; 
	$LV_PathDSLog         = \'%DS_COMMON_DIR%\\\\log\'; 

	# Format expected parameters for the the Job Launcher
	$LV_ParamsJobLauncher = \'[$LV_PathJobLauncher]  -w "inet:[$LV_Jobserver_HostPort]" -C "[$LV_PathDSLog]\\\\[$LV_ParamsFilename]\';

	# Format the text that is expected in the engine command file
    $LV_ParamsFileData    = \' [$LV_CodePage] -R"[$LV_RepositoryName].txt" -G"[$LV_GUID]" -t5 -T14 -no_audit -Ksp[$LV_SystemConfig] -CtBatch -Cm[$LV_CMS_Host] -Cj[$LV_Jobserver_Host] -Cp[$LV_Jobserver_Port] \';

	# Write the temporary text file 
	BF_Exec(\'echo [$LV_ParamsFileData] > "[$LV_PathDSLog]\\\\[$LV_ParamsFilename]"\', \'**** Temporary Parameter File Creation Failed for Job=[$PV_JobName] ****\'  ,\'N\');

	# Execute the job launcher
	BF_Exec(\'[$LV_ParamsJobLauncher]\', \'**** Execute Job Launcher Failed for Job=[$PV_JobName] ****\', \'N\');

	# Delete the temporary text file
    BF_DeleteTextFiles($LV_PathDSLog, $LV_ParamsFilename);
end 
else
begin
    # Get paths, variables and the default shell from Linux
	$LV_PathDSLog       = \'$DS_COMMON_DIR\';
	$LV_PathShellScript = BF_GetPathShellScripts();
    
	# Format expected parameters for the the Job Launcher
    $LV_ParamsFileData  = \'[$LV_PathDSLog] [$LV_CodePage] [$LV_RepositoryName] [$LV_GUID] [$LV_SystemConfig] [$LV_CMS_Host] [$LV_Jobserver_Host] [$LV_Jobserver_Port]\';

	#Grant permissions to execute shell file
    BF_Exec(\'-c "chmod +x [$LV_PathShellScript]/exec_joblauncher.sh"\', \'**** Grant shell script access failed for Job=[$PV_JobName] ****\' , \'N\');

	# Execute the Job Launcher
    BF_Exec(\'-c "[$LV_PathShellScript]/exec_joblauncher.sh [$LV_ParamsFileData]"\', \'**** Execution of exec_joblauncher.sh failed for Job=[$PV_JobName] ****\', \'N\' );
end

PRINT(\'BF_ExecModularJob: Job <[$PV_JobName]> is completed.\');

RETURN 1;
', "x" = '-1', "y" = '-1')
CREATE FUNCTION BF_ExecModularJob($PV_JobName VARCHAR(255)  IN )
RETURNS  INT 
DECLARE
     $LV_ParamsFileData VARCHAR(2000) ;
     $LV_ParamsFilename VARCHAR(255) ;
     $LV_DeleteString VARCHAR(1000) ;
     $LV_PathDSLog VARCHAR(2000) ;
     $LV_PathJobLauncher VARCHAR(2000) ;
     $LV_GUID VARCHAR(100) ;
     $LV_SystemConfig VARCHAR(100) ;
     $LV_RepositoryName VARCHAR(255) ;
     $LV_CMS_Host VARCHAR(255) ;
     $LV_CMS_HostPort VARCHAR(255) ;
     $LV_Jobserver_HostPort VARCHAR(255) ;
     $LV_ParamsJobLauncher VARCHAR(1000) ;
     $LV_CMS_Port INT ;
     $LV_Jobserver_Host VARCHAR(255) ;
     $LV_Jobserver_Port INT ;
     $LV_PathShellScript VARCHAR(2000) ;
     $LV_CodePage VARCHAR(255) ;
BEGIN
print('BF_ExecModularJob: Starting Job <[$PV_JobName]>.');$LV_SystemConfig = current_system_configuration();$LV_RepositoryName = ifthenelse((word_ext(repository_name(), 1, '.') = 'ORACLE'), word_ext(repository_name(), -1, '.'), word_ext((repository_name() || 'x'), -2, '.'));$LV_CMS_HostPort = bf_getconfigvalue('CMS_HOST_PORT');$LV_CMS_Host = word_ext($LV_CMS_HostPort, 1, ':');$LV_CMS_Port = word_ext($LV_CMS_HostPort, 2, ':');$LV_Jobserver_HostPort = bf_getconfigvalue('JOBSERVER_HOST_PORT');$LV_Jobserver_Host = word_ext($LV_Jobserver_HostPort, 1, ':');$LV_Jobserver_Port = word_ext($LV_Jobserver_HostPort, 2, ':');$LV_CodePage = ' -PLocaleUTF8 ';$LV_GUID = sql('CURR_REPO_DS', 'select GUID from AL_LANG where name ={$PV_JobName}');IF (($LV_RepositoryName IS  NULL ) )
raise_exception(('BF_ExecModularJob: Error - $LV_RepositoryName is Null ' || 90001));IF (($LV_CMS_Host IS  NULL ) )
raise_exception(('BF_ExecModularJob: Error - $LV_CMS_Host is Null ' || 90002));IF ((($LV_CMS_Port IS  NULL ) OR
   ($LV_CMS_Port = 0)) )
raise_exception(('BF_ExecModularJob: Error - $LV_CMS_Port is not numeric or is equal to 0 ' || 90003));IF (($LV_Jobserver_Host IS  NULL ) )
raise_exception(('BF_ExecModularJob: Error - $LV_Jobserver_Host is Null ' || 90004));IF ((($LV_Jobserver_Port IS  NULL ) OR
   ($LV_Jobserver_Port = 0)) )
raise_exception(('BF_ExecModularJob: Error - $LV_Jobserver_Port is not numeric or is equal to 0 ' || 90005));IF (($LV_GUID IS  NULL ) )
raise_exception(('BF_ExecModularJob: Error - $LV_GUID is Null ' || 90006));IF ((BF_GetJobServerOS() = 'WINDOWS') )
BEGIN
$LV_ParamsFilename = '[$PV_JobName]_[$LV_SystemConfig]_[$LV_RepositoryName].txt';$LV_PathJobLauncher = 'AL_RWJobLauncher.exe';$LV_PathDSLog = '%DS_COMMON_DIR%\\log';$LV_ParamsJobLauncher = '[$LV_PathJobLauncher]  -w "inet:[$LV_Jobserver_HostPort]" -C "[$LV_PathDSLog]\\[$LV_ParamsFilename]';$LV_ParamsFileData = ' [$LV_CodePage] -R"[$LV_RepositoryName].txt" -G"[$LV_GUID]" -t5 -T14 -no_audit -Ksp[$LV_SystemConfig] -CtBatch -Cm[$LV_CMS_Host] -Cj[$LV_Jobserver_Host] -Cp[$LV_Jobserver_Port] ';bf_exec('echo [$LV_ParamsFileData] > "[$LV_PathDSLog]\\[$LV_ParamsFilename]"', '**** Temporary Parameter File Creation Failed for Job=[$PV_JobName] ****', 'N');bf_exec('[$LV_ParamsJobLauncher]', '**** Execute Job Launcher Failed for Job=[$PV_JobName] ****', 'N');bf_deletetextfiles($LV_PathDSLog, $LV_ParamsFilename);END
ELSE
BEGIN
$LV_PathDSLog = '$DS_COMMON_DIR';$LV_PathShellScript = BF_GetPathShellScripts();$LV_ParamsFileData = '[$LV_PathDSLog] [$LV_CodePage] [$LV_RepositoryName] [$LV_GUID] [$LV_SystemConfig] [$LV_CMS_Host] [$LV_Jobserver_Host] [$LV_Jobserver_Port]';bf_exec('-c "chmod +x [$LV_PathShellScript]/exec_joblauncher.sh"', '**** Grant shell script access failed for Job=[$PV_JobName] ****', 'N');bf_exec('-c "[$LV_PathShellScript]/exec_joblauncher.sh [$LV_ParamsFileData]"', '**** Execution of exec_joblauncher.sh failed for Job=[$PV_JobName] ****', 'N');END
print('BF_ExecModularJob: Job <[$PV_JobName]> is completed.');RETURN 1;
END

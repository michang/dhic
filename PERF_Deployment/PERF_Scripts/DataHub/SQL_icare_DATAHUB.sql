/*==============================================================*/
/* Table: TRF_INCIDENT                                          */
/*==============================================================*/
create table Z_TRF_INCIDENT (
   INCIDENT_KEY            varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   CLAIM_KEY            varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   INCIDENT_TYPE_KEY            varchar(255)         not null,
   BODY_LOC_CD          varchar(255)         not null,   
   BODY_LOC_DESC        varchar(255)         not null,
   NATURE_INJURY_CD     varchar(255)         not null,   
   NATURE_INJURY_DESC   varchar(255)         not null,
   ICD_CD			    varchar(255)         not null,   
   ICD_DESC				varchar(255)         not null,   
   RETIRED_FL        varchar(1)           not null,
   CLAIM_INCIDENT_FL        varchar(1)           not null,    
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go


/*==============================================================*/
/* Table: CS_INCIDENT_BASE                                      */
/*==============================================================*/
create table Z_CS_INCIDENT_BASE (
   INCIDENT_BASE_ID        int                  not null,
   INCIDENT_KEY            varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CLAIM_KEY            varchar(100)         not null,
   INCIDENT_TYPE_KEY            varchar(255)         not null,
   ETL_ACTIVE_FL        varchar(1)           not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table Z_CS_INCIDENT_BASE
   add constraint CIBA1_PK primary key nonclustered (INCIDENT_KEY)
go

alter table Z_CS_INCIDENT_BASE
   add constraint CIBA1_AK1 unique (INCIDENT_BASE_ID)
go





/*==============================================================*/
/* Index: CIBA_CIBA_XFK                                         */
/*==============================================================*/




create nonclustered index CIBA_CIBA_XFK on Z_CS_INCIDENT_BASE (CLAIM_KEY ASC)
go

/*==============================================================*/
/* Table: Z_CS_INCIDENT_DELTA                                     */
/*==============================================================*/
create table Z_CS_INCIDENT_DELTA (
   INCIDENT_ID             int                  not null,
   INCIDENT_KEY            varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CLAIM_KEY            varchar(100)         not null,
   BODY_LOC_CD          varchar(255)         not null,   
   BODY_LOC_DESC        varchar(255)         not null,
   NATURE_INJURY_CD     varchar(255)         not null,   
   NATURE_INJURY_DESC   varchar(255)         not null,
   ICD_CD			    varchar(255)         not null,   
   ICD_DESC				varchar(255)         not null,   
   RETIRED_FL        varchar(1)           not null,
   CLAIM_INCIDENT_FL        varchar(1)           not null,    
   ETL_CURR_ROW_FL      varchar(1)           not null,
   ETL_LATE_ARRIVING_FL varchar(1)           not null,
   ETL_ACTIVE_FL        varchar(1)           not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table Z_CS_INCIDENT_DELTA
   add constraint CIDE_PK primary key nonclustered (INCIDENT_KEY, ETL_ROW_EFF_DTS)
go

alter table Z_CS_INCIDENT_DELTA
   add constraint CIDE_AK1 unique (INCIDENT_ID)
go


alter table Z_CS_INCIDENT_BASE
   add constraint FK_CIBA_CCBA foreign key (CLAIM_KEY)
      references CS_CLAIM_BASE (CLAIM_KEY)
go

alter table Z_CS_INCIDENT_DELTA
   add constraint FK_CIDE_CIBA1 foreign key (INCIDENT_KEY)
      references Z_CS_INCIDENT_BASE (INCIDENT_KEY)
go

/* MD_FK_REF Entries */
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('Z_TRF_INCIDENT', 'Z_CS_INCIDENT_DELTA', 'INCIDENT_KEY', 'Z_CS_INCIDENT_BASE', 'INCIDENT_KEY', 'CLAIMS', 'FK_CIDE_CIBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('Z_TRF_INCIDENT', 'Z_CS_INCIDENT_BASE', 'CLAIM_KEY', 'CS_CLAIM_BASE', 'CLAIM_KEY', 'CLAIMS', 'FK_CIBA_CCBA', 'N');
		

CREATE INDEX CIDE_CVIX1   ON Z_CS_INCIDENT_DELTA      (INCIDENT_KEY, ETL_ROW_EFF_DTS) INCLUDE (ETL_ROW_EXP_DTS, INCIDENT_ID, ETL_CURR_ROW_FL);


/* ADDING INCIDENT_KEY TO LOSS TRAN*/
ALTER TABLE TRF_LOSS_TRAN
	ADD  Z_INCIDENT_KEY	varchar(100)         null
go


ALTER TABLE CS_LOSS_TRAN
	ADD  Z_INCIDENT_KEY	varchar(100)         null
go

create nonclustered index CLTR_CIBA_XFK on CS_LOSS_TRAN (Z_INCIDENT_KEY ASC)
go



alter table CS_LOSS_TRAN
   add constraint FK_CLTR_CIBA foreign key (Z_INCIDENT_KEY)
      references Z_CS_INCIDENT_BASE (INCIDENT_KEY)
go


INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES 
					('TRF_LOSS_TRAN', 'CS_LOSS_TRAN', 'Z_INCIDENT_KEY', 'Z_CS_INCIDENT_BASE', 'INCIDENT_KEY', 'CLAIM', 'FK_CLTR_CIBA', 'N')
go

					
/*** adding INCIDENT_KEY TO THE VIEW ***/

DROP VIEW V_CS_LOSS_TRAN
go

CREATE VIEW V_CS_LOSS_TRAN AS
SELECT T.LOSS_TRANS_ID,
       T.FEATURE_KEY,
       T.SOURCE_SYSTEM,
       T.CLMNT_KEY,
       T.CLAIM_KEY,
       T.POL_KEY,
       T.LOC_KEY,
       T.RISK_KEY,
       T.COVG_LIMIT_KEY,
       T.REC_TYPE_KEY,
       T.AGNT_KEY,
       T.COVG_ID,
       T.VNDR_KEY,
       T.AGCY_KEY,
       T.RISK_TYPE_CD,
       T.LOSS_DT,
       T.TRANS_PROC_DTS,
       T.ACCTG_PRD_ID,
       T.TRANS_CD,
       T.TRANS_TYPE_CD,
       T.TRANS_SEQ,
       T.TRANS_ALLOC_CD,
       T.CHECK_NO,
       T.CHECK_DT,
       T.CURR_CD,
       T.TRANS_AMT,
	   T.CLAIM_AMT,
	   T.CLAIM_EX_ADJ_AMT,
	   T.CLAIM_CURR_CD,
	   T.TRANS_TO_CLAIM_EX_RATE,
	   T.RSRV_AMT,
	   T.RSRV_EX_ADJ_AMT,
	   T.RSRV_CURR_CD,
	   T.TRANS_TO_RSRV_EX_RATE,
	   T.RPT_AMT,
	   T.RPT_EX_ADJ_AMT,
	   T.RPT_CURR_CD,
	   T.CLAIM_TO_RPT_EX_RATE,
	   T.CLAIM_RSRV_CHNG_AMT,
	   T.RSRV_RSRV_CHNG_AMT,
	   T.RPT_RSRV_CHNG_AMT,
       T.RSRV_CHNG_AMT,
       CASE WHEN T.TRANS_ALLOC_CD = 'INDEMNITY' THEN T.RPT_AMT ELSE 0.0 END IND_PD_AMT,
       CASE WHEN T.TRANS_ALLOC_CD = 'MEDICAL' THEN T.RPT_AMT ELSE 0.0 END MED_PD_AMT,
       CASE WHEN T.TRANS_ALLOC_CD = 'LAE EXPENSE' THEN T.RPT_AMT ELSE 0.0 END ALAE_PD_AMT,
       CASE WHEN T.TRANS_ALLOC_CD = 'ULAE EXPENSE' THEN T.RPT_AMT ELSE 0.0 END ULAE_PD_AMT,
       CASE WHEN T.TRANS_ALLOC_CD = 'SALVAGE' THEN T.RPT_AMT ELSE 0.0 END SLVG_RCVRY_AMT,
       CASE WHEN T.TRANS_ALLOC_CD = 'SUBROGATION' THEN T.RPT_AMT ELSE 0.0 END SUBRO_RCVRY_AMT,
       CASE WHEN T.TRANS_ALLOC_CD = 'DEDUCTIBLE' THEN T.RPT_AMT ELSE 0.0 END DED_RCVRY_AMT,
       CASE WHEN T.TRANS_ALLOC_CD = 'INDEMNITY_RECOVERY' THEN T.RPT_AMT ELSE 0.0 END IND_RCVRY_AMT,
       CASE WHEN T.TRANS_ALLOC_CD = 'EXPENSE_RECOVERY' THEN T.RPT_AMT ELSE 0.0 END EXP_RCVRY_AMT,
       CASE WHEN T.TRANS_ALLOC_CD = 'INDEMNITY' THEN T.RPT_RSRV_CHNG_AMT ELSE 0.0 END IND_RSRV_CHNG_AMT,
       CASE WHEN T.TRANS_ALLOC_CD = 'MEDICAL' THEN T.RPT_RSRV_CHNG_AMT ELSE 0.0 END MED_RSRV_CHNG_AMT,
       CASE WHEN T.TRANS_ALLOC_CD = 'LAE EXPENSE' THEN T.RPT_RSRV_CHNG_AMT ELSE 0.0 END ALAE_RSRV_CHNG_AMT,
       CASE WHEN T.TRANS_ALLOC_CD = 'ULAE EXPENSE' THEN T.RPT_RSRV_CHNG_AMT ELSE 0.0 END ULAE_RSRV_CHNG_AMT,
       CASE WHEN T.TRANS_ALLOC_CD = 'SALVAGE' THEN T.RPT_RSRV_CHNG_AMT ELSE 0.0 END SLVG_RSRV_CHNG_AMT,
       CASE WHEN T.TRANS_ALLOC_CD = 'SUBROGATION' THEN T.RPT_RSRV_CHNG_AMT ELSE 0.0 END SUBRO_RSRV_CHNG_AMT,
       T.ETL_ACTIVE_FL,
       T.ETL_ADD_DTS,
       T.ETL_LAST_UPDATE_DTS,
	   Z_INCIDENT_KEY
  FROM CS_LOSS_TRAN T
;

GO	




/** CLAIM LIABILITY STATUS ***/

/*==============================================================*/
/* Table: TRF_CLAIM_LIABILITY_STATUS                                      */
/*==============================================================*/
create table Z_TRF_CLAIM_LIABILITY_STATUS (
   CLAIM_KEY            varchar(100)         not null,
   STATUS_PROC_DTS      DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CLAIM_LIAB_STATUS_KEY            varchar(100)         not null,   
   STATUS_CD            varchar(255)         not null,
   X_SEQ_NO             bigint               not null
)
go

--DROP TABLE Z_CS_CLAIM_LIABILITY_STATUS_DIM;

/*==============================================================*/
/* Table: CS_CLAIM_LIABILITY_STATUS_DIM                                   */
/*==============================================================*/
create table Z_CS_CLAIM_LIABILITY_STATUS_DIM (
   CLAIM_LIAB_STATUS_ID      int                  not null,
   CLAIM_LIAB_STATUS_KEY            varchar(100)         not null,   
   SOURCE_SYSTEM        varchar(10)          not null,
   STATUS_CD            varchar(255)         not null,
   ETL_ACTIVE_FL        varchar(1)           not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table Z_CS_CLAIM_LIABILITY_STATUS_DIM
   add constraint CCLSD_PK primary key nonclustered (CLAIM_LIAB_STATUS_ID)
go

alter table Z_CS_CLAIM_LIABILITY_STATUS_DIM
   add constraint CCLSD_AK1 unique (CLAIM_LIAB_STATUS_KEY)
go

/*==============================================================*/
/* Table: CS_CLAIM_LIABILITY_STATUS_TRAN                                  */
/*==============================================================*/
create table Z_CS_CLAIM_LIABILITY_STATUS_TRAN (
   CLAIM_LIAB_STATUS_TRANS_ID int                  identity,
   CLAIM_KEY            varchar(100)         not null,
   CLAIM_LIAB_STATUS_KEY            varchar(100)         not null,
   CLAIM_LIAB_STATUS_ID      int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   STATUS_PROC_DTS      DATETIME2            not null,
   ETL_ACTIVE_FL        varchar(1)           not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table Z_CS_CLAIM_LIABILITY_STATUS_TRAN
   add constraint CCLST_PK primary key nonclustered (CLAIM_KEY, CLAIM_LIAB_STATUS_ID, STATUS_PROC_DTS)
go

alter table Z_CS_CLAIM_LIABILITY_STATUS_TRAN
   add constraint CCLST_AK1 unique (CLAIM_LIAB_STATUS_TRANS_ID)
go

/*==============================================================*/
/* Index: CCLST_CCLSD_XFK                                         */
/*==============================================================*/




create nonclustered index CCLST_CCLSD_XFK on Z_CS_CLAIM_LIABILITY_STATUS_TRAN (CLAIM_LIAB_STATUS_ID ASC)
go



alter table Z_CS_CLAIM_LIABILITY_STATUS_TRAN
   add constraint FK_CCLST_CCLSD foreign key (CLAIM_LIAB_STATUS_ID)
      references Z_CS_CLAIM_LIABILITY_STATUS_DIM (CLAIM_LIAB_STATUS_ID)
go

alter table Z_CS_CLAIM_LIABILITY_STATUS_TRAN
   add constraint FK_CCLST_CCLSDK foreign key (CLAIM_LIAB_STATUS_KEY)
      references Z_CS_CLAIM_LIABILITY_STATUS_DIM (CLAIM_LIAB_STATUS_KEY)
go

alter table Z_CS_CLAIM_LIABILITY_STATUS_TRAN
   add constraint FK_CCLST_CCBA foreign key (CLAIM_KEY)
      references CS_CLAIM_BASE (CLAIM_KEY)
go



INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL  ) VALUES ('Z_TRF_CLAIM_LIABILITY_STATUS', 'Z_CS_CLAIM_LIABILITY_STATUS_TRAN', 'CLAIM_LIAB_STATUS_ID', 'Z_CS_CLAIM_LIABILITY_STATUS_DIM', 'CLAIM_LIAB_STATUS_ID', 'CLAIMS', 'FK_CCLST_CCLSD', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL  ) VALUES ('Z_TRF_CLAIM_LIABILITY_STATUS', 'Z_CS_CLAIM_LIABILITY_STATUS_TRAN', 'CLAIM_LIAB_STATUS_KEY', 'Z_CS_CLAIM_LIABILITY_STATUS_DIM', 'CLAIM_LIAB_STATUS_KEY', 'CLAIMS', 'FK_CCLST_CCLSDK', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL ) VALUES ('Z_TRF_CLAIM_LIABILITY_STATUS', 'Z_CS_CLAIM_LIABILITY_STATUS_TRAN', 'CLAIM_KEY', 'CS_CLAIM_BASE', 'CLAIM_KEY', 'CLAIMS', 'FK_CCLST_CCBA', 'N');


/*** ADD WIC CODE, WIC INDUSTRY -EXT, TRF, ODS and DIM ***/

ALTER TABLE TRF_CLAIM
	ADD  Z_WIC_CD 	varchar(255)         null,
	  Z_WIC_INDUSTRY 	varchar(255)         null;
go

ALTER TABLE CS_CLAIM_DELTA
	ADD  Z_WIC_CD 	varchar(255)         null,
	  Z_WIC_INDUSTRY 	varchar(255)         null;
go



/*** Enhance Claim ODS and DIM, adding Last Liability Status Process Time and Liability Status  ***/

--CLAIM_LIAB_STATUS_ID, STATUS_PROC_DTS - ODS/DIM

ALTER TABLE CS_CLAIM_BASE
	ADD  Z_LAST_LIAB_STATUS_ID 	int         null,
	  Z_LAST_LIAB_STATUS_PROC_DTS 	DATETIME2         null;
go

create nonclustered index CCBA_CCLSD_XFK on CS_CLAIM_BASE (Z_LAST_LIAB_STATUS_ID ASC)
go

alter table CS_CLAIM_BASE
   add constraint FK_CCBA_CCLSD foreign key (Z_LAST_LIAB_STATUS_ID)
      references Z_CS_CLAIM_LIABILITY_STATUS_DIM (CLAIM_LIAB_STATUS_ID)
go


INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('TRF_CLAIM', 'CS_CLAIM_BASE', 'Z_LAST_LIAB_STATUS_ID', 'Z_CS_CLAIM_LIABILITY_STATUS_DIM', 'CLAIM_LIAB_STATUS_ID', 'CLAIMS', 'FK_CCBA_CCLSD', 'N');
go


 alter table trf_claim 
 add Z_LOB_CD varchar(50) null ,
 Z_POL_TYPE_CD varchar(50) null
go

 alter table cs_claim_delta 
 add Z_LOB_CD varchar(50) null ,
 Z_POL_TYPE_CD varchar(50) null
go



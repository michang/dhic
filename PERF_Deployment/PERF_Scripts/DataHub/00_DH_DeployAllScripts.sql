/*******************************************************************************
DESCRIPTION:
  Executes all sql files needed to deploy the SQL Environment
  
  **** REQUIRES SQLCMD MODE **** 
******************************************************************************/
SET NOCOUNT ON
GO

:On Error exit
	:setvar FilePath "C:\Users\michang\Documents\icare\icare\icare\DEV1\Scripts\DataHub"
	-- The 01_DROP_DH_Schema_Objects.sql checks for a valid database(DH_XXX). If that fails processing will stop.
	:r $(FilePath)\01_DH_DROP_Schema_Objects.sql
	:r $(FilePath)\SQL_FULL_DATAHUB.sql
	:r $(FilePath)\SQL_DH_FULL_DB_OBJECTS.sql
	--:r $(FilePath)\SQL_DH_FULL_GWCR_OBJECTS.sql
	:r $(FilePath)\SQL_icare_DATAHUB.sql
	:r $(FilePath)\SQL_icare_GWCR_DATAHUB.sql

PRINT 'SQL SCRIPT DEPLOYMENT IS COMPLETE'


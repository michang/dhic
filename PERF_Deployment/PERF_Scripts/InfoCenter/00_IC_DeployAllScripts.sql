/******************************************************************************
DESCRIPTION:
  Executes all sql files needed to deploy the SQL Environment
  
  **** REQUIRES SQLCMD MODE ****    
******************************************************************************/
SET NOCOUNT ON
GO
--use ic_UAT
:On Error exit
	:setvar FilePath "C:\Users\michang\Documents\icare\icare\icare\DEV1\Scripts\InfoCenter"

	-- The 01DROP_ODS_Schema_Objects.sql checks for a valid database(IC_XXX). If that fails processing will stop.
	:r $(FilePath)\01_IC_DROP_Schema_Objects.sql
	:r $(FilePath)\SQL_FULL_INFOCENTER.sql
	:r $(FilePath)\SQL_IC_FULL_DB_OBJECTS.sql
	--:r $(FilePath)\SQL_IC_FULL_GWCR_OBJECTS.sql
	:r $(FilePath)\SQL_icare_INFOCENTER.sql
	:r $(FilePath)\SQL_icare_GWCR_INFOCENTER.sql
	
PRINT 'SQL SCRIPT DEPLOYMENT IS COMPLETE'

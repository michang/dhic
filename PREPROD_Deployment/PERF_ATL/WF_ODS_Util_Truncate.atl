#__AW_Repository_Version '14.2.6.0000';
#__AW_Product_Version '14.2.6.1113';
#__AW_ATL_Locale 'eng_us.utf-8';
 AlGUIComment ("ANNOTATION_0" = 'ALGUICOMMENT( x = \'34\', y = \'-53\',ObjectDesc=\'Purpose:  Truncate ODS Tables
Notes:      Exludes ETL_ tables except
	ETL_TRF_EXCEPTION
	\\\'ETL_TRF_CONTROL
History:   13 Feb. 2013 Guidewire Base Version\',UI_DATA_XML=\'<UIDATA><MAINICON><LOCATION><X>-34</X><Y>53</Y></LOCATION><SIZE><CX>0</CX><CY>0</CY></SIZE></MAINICON><DESCRIPTION><LOCATION><X>0</X><Y>0</Y></LOCATION><SIZE><CX>1032</CX><CY>328</CY></SIZE><VISIBLE>1</VISIBLE></DESCRIPTION></UIDATA>\' )
', "ActaName_1" = 'RSavedAfterCheckOut', "ActaName_2" = 'RDate_created', "ActaName_3" = 'RDate_modified', "ActaValue_1" = 'YES', "ActaValue_2" = 'Wed Feb 13 12:33:42 2013', "ActaValue_3" = 'Fri Aug 03 09:44:14 2018', "x" = '-1', "y" = '-1')
CREATE PLAN WF_ODS_Util_Truncate::'5c158e24-26df-4b02-8e2e-f483120bde31'( )
DECLARE
     $LV_TABLE_NAME VARCHAR(50) ;
BEGIN
 AlGUIComment ("UI_DATA_XML" = '<UIDATA><MAINICON><LOCATION><X>0</X><Y>0</Y></LOCATION><SIZE><CX>357</CX><CY>-179</CY></SIZE></MAINICON><DESCRIPTION><LOCATION><X>0</X><Y>-190</Y></LOCATION><SIZE><CX>200</CX><CY>200</CY></SIZE><VISIBLE>0</VISIBLE></DESCRIPTION></UIDATA>', "ui_display_name" = 'TruncateODSTables', "ui_script_text" = '
IF (upper(BF_GetDatabaseTypeDW( )) = \'SQL\')
BEGIN
SQL(\'ODS_DS\', \'SET NOCOUNT ON 

-- Creates temp table used to store scripts to recreate foreign key constraints
IF(select count(*) from sys.tables where name = \\\'TMP_FK_RECREATE\\\') = 0
BEGIN
	EXEC(\\\'create table tmp_fk_recreate(recreate_sql varchar(2000))\\\')
END
ELSE
	EXEC(\\\'TRUNCATE TABLE TMP_FK_RECREATE\\\')

DECLARE @tableName sysname 


-- Drops all foreign key constraints and loads scripts to recreate them into a temp table
DECLARE all_tables CURSOR
	FOR select name from sys.tables where type = \\\'U\\\' 
AND (name like \\\'CS#_%\\\' ESCAPE \\\'#\\\' 
OR name like \\\'Z_CS#_%\\\' ESCAPE \\\'#\\\' 
OR name like \\\'TRF#_%\\\' ESCAPE \\\'#\\\' 
OR name like \\\'Z_TRF#_%\\\' ESCAPE \\\'#\\\' 
OR name like \\\'REF#_%\\\' ESCAPE \\\'#\\\'
OR name like \\\'K%#_%\\\' ESCAPE \\\'#\\\'
OR name in (\\\'ETL_TRF_EXCEPTION\\\', \\\'ETL_TRF_CONTROL\\\',\\\'ETL_ODS_CYCLE_STATUS\\\', \\\'ETL_ODS_AUDIT_METRIC\\\', \\\'ETL_ODS_CYCLE_STATISTIC\\\',
	\\\'ETL_ENGINE_ACTIVITY\\\',\\\'ETL_ENGINE_LOG\\\',\\\'ETL_ENGINE_STATS\\\',\\\'ETL_EXTRACT_STATS\\\'))

OPEN all_tables

FETCH NEXT FROM all_tables INTO @tableName

WHILE @@FETCH_STATUS = 0

BEGIN

	DECLARE @cmd NVARCHAR(1000)
	DECLARE @drop_cmd NVARCHAR(1000)

	DECLARE  
	   @FK_NAME sysname, 
	   @FK_OBJECTID INT, 
	   @FK_DISABLED INT, 
	   @FK_NOT_FOR_REPLICATION INT, 
	   @DELETE_RULE    smallint,    
	   @UPDATE_RULE    smallint,    
	   @FKTABLE_NAME sysname, 
	   @FKTABLE_OWNER sysname, 
	   @PKTABLE_NAME sysname, 
	   @PKTABLE_OWNER sysname, 
	   @FKCOLUMN_NAME sysname, 
	   @PKCOLUMN_NAME sysname, 
	   @CONSTRAINT_COLID INT 


	DECLARE cursor_fkeys CURSOR FOR  
	   SELECT  Fk.name, 
			   Fk.OBJECT_ID,  
			   Fk.is_disabled,  
			   Fk.is_not_for_replication,  
			   Fk.delete_referential_action,  
			   Fk.update_referential_action,  
			   OBJECT_NAME(Fk.parent_object_id) AS Fk_table_name,  
			   schema_name(Fk.schema_id) AS Fk_table_schema,  
			   TbR.name AS Pk_table_name,  
			   schema_name(TbR.schema_id) Pk_table_schema 
	   FROM    sys.foreign_keys Fk LEFT OUTER JOIN  
			   sys.tables TbR ON TbR.OBJECT_ID = Fk.referenced_object_id --inner join  
	   WHERE   TbR.name = @tableName 

	OPEN cursor_fkeys 

	FETCH NEXT FROM   cursor_fkeys  
	   INTO @FK_NAME,@FK_OBJECTID, 
		   @FK_DISABLED, 
		   @FK_NOT_FOR_REPLICATION, 
		   @DELETE_RULE,    
		   @UPDATE_RULE,    
		   @FKTABLE_NAME, 
		   @FKTABLE_OWNER, 
		   @PKTABLE_NAME, 
		   @PKTABLE_OWNER 

	WHILE @@FETCH_STATUS = 0  
	BEGIN     

		   -- drop statement 
		   SET @drop_cmd = \\\'ALTER TABLE \\[\\\' + @FKTABLE_OWNER + \\\'\\].\\[\\\' + @FKTABLE_NAME  
		   + \\\'\\]  DROP CONSTRAINT \\[\\\' + @FK_NAME + \\\'\\]\\\'         

		   -- create process 
		   DECLARE @FKCOLUMNS VARCHAR(1000), @PKCOLUMNS VARCHAR(1000), @COUNTER INT 

		   -- create cursor to get FK columns 
		   DECLARE cursor_fkeyCols CURSOR FOR  
		   SELECT  COL_NAME(Fk.parent_object_id, Fk_Cl.parent_column_id) AS Fk_col_name,  
				   COL_NAME(Fk.referenced_object_id, Fk_Cl.referenced_column_id) AS Pk_col_name 
		   FROM    sys.foreign_keys Fk LEFT OUTER JOIN  
				   sys.tables TbR ON TbR.OBJECT_ID = Fk.referenced_object_id INNER JOIN  
				   sys.foreign_key_columns Fk_Cl ON Fk_Cl.constraint_object_id = Fk.OBJECT_ID  
		   WHERE   TbR.name = @tableName 
				   AND Fk_Cl.constraint_object_id = @FK_OBJECTID -- added 6/12/2008 
		   ORDER BY Fk_Cl.constraint_column_id 

		   OPEN cursor_fkeyCols 

		   FETCH NEXT FROM    cursor_fkeyCols INTO @FKCOLUMN_NAME,@PKCOLUMN_NAME 

		   SET @COUNTER = 1 
		   SET @FKCOLUMNS = \\\'\\\' 
		   SET @PKCOLUMNS = \\\'\\\' 
        
		   WHILE @@FETCH_STATUS = 0  
		   BEGIN  

			   IF @COUNTER > 1  
			   BEGIN 
				   SET @FKCOLUMNS = @FKCOLUMNS + \\\',\\\' 
				   SET @PKCOLUMNS = @PKCOLUMNS + \\\',\\\' 
			   END 

			   SET @FKCOLUMNS = @FKCOLUMNS + \\\'\\[\\\' + @FKCOLUMN_NAME + \\\'\\]\\\' 
			   SET @PKCOLUMNS = @PKCOLUMNS + \\\'\\[\\\' + @PKCOLUMN_NAME + \\\'\\]\\\' 

			   SET @COUNTER = @COUNTER + 1 
            
			   FETCH NEXT FROM    cursor_fkeyCols INTO @FKCOLUMN_NAME,@PKCOLUMN_NAME 
		   END 

		   CLOSE cursor_fkeyCols  
		   DEALLOCATE cursor_fkeyCols  

		   -- generate create FK statement 
		   SET @cmd = \\\'ALTER TABLE \\[\\\' + @FKTABLE_OWNER + \\\'\\].\\[\\\' + @FKTABLE_NAME + \\\'\\]  WITH \\\' +  
			   CASE @FK_DISABLED  
				   WHEN 0 THEN \\\' CHECK \\\' 
				   WHEN 1 THEN \\\' NOCHECK \\\' 
			   END +  \\\' ADD CONSTRAINT \\[\\\' + @FK_NAME  
			   + \\\'\\] FOREIGN KEY (\\\' + @FKCOLUMNS  
			   + \\\') REFERENCES \\[\\\' + @PKTABLE_OWNER + \\\'\\].\\[\\\' + @PKTABLE_NAME + \\\'\\] (\\\'  
			   + @PKCOLUMNS + \\\') ON UPDATE \\\' +  
			   CASE @UPDATE_RULE  
				   WHEN 0 THEN \\\' NO ACTION \\\' 
				   WHEN 1 THEN \\\' CASCADE \\\'  
				   WHEN 2 THEN \\\' SET_NULL \\\'  
				   END + \\\' ON DELETE \\\' +  
			   CASE @DELETE_RULE 
				   WHEN 0 THEN \\\' NO ACTION \\\'  
				   WHEN 1 THEN \\\' CASCADE \\\'  
				   WHEN 2 THEN \\\' SET_NULL \\\'  
				   END + \\\'\\\' + 
			   CASE @FK_NOT_FOR_REPLICATION 
				   WHEN 0 THEN \\\'\\\' 
				   WHEN 1 THEN \\\' NOT FOR REPLICATION \\\' 
			   END 

		  EXEC(\\\'INSERT INTO tmp_fk_recreate VALUES (\\\'\\\'\\\' + @cmd + \\\'\\\'\\\')\\\')
		  EXEC(@drop_cmd)
	   

	   FETCH NEXT FROM    cursor_fkeys  
		  INTO @FK_NAME,@FK_OBJECTID, 
			   @FK_DISABLED, 
			   @FK_NOT_FOR_REPLICATION, 
			   @DELETE_RULE,    
			   @UPDATE_RULE,    
			   @FKTABLE_NAME, 
			   @FKTABLE_OWNER, 
			   @PKTABLE_NAME, 
			   @PKTABLE_OWNER 
	END 

	CLOSE cursor_fkeys;
	DEALLOCATE cursor_fkeys;  

	FETCH NEXT FROM all_tables INTO @tableName
END

CLOSE all_tables;
DEALLOCATE all_tables;

-- Truncate all tables
DECLARE @table_name varchar(255)
DECLARE all_tables CURSOR
	FOR select name from sys.tables where type = \\\'U\\\' 
AND (name like \\\'CS#_%\\\' ESCAPE \\\'#\\\' 
OR name like \\\'TRF#_%\\\' ESCAPE \\\'#\\\' 
OR name like \\\'Z_CS#_%\\\' ESCAPE \\\'#\\\' 
OR name like \\\'Z_TRF#_%\\\' ESCAPE \\\'#\\\' 

OR name like \\\'REF#_%\\\' ESCAPE \\\'#\\\'
OR name like \\\'K%#_%\\\' ESCAPE \\\'#\\\'
OR name in (\\\'ETL_TRF_EXCEPTION\\\', \\\'ETL_TRF_CONTROL\\\',\\\'ETL_ODS_CYCLE_STATUS\\\', \\\'ETL_ODS_AUDIT_METRIC\\\', \\\'ETL_ODS_CYCLE_STATISTIC\\\',
	\\\'ETL_ENGINE_ACTIVITY\\\',\\\'ETL_ENGINE_LOG\\\',\\\'ETL_ENGINE_STATS\\\',\\\'ETL_EXTRACT_STATS\\\'))

	OPEN all_tables

	FETCH NEXT FROM all_tables INTO @table_name

	WHILE @@FETCH_STATUS = 0

	BEGIN
		EXEC(\\\'TRUNCATE TABLE \\\' + @table_name)

		FETCH NEXT FROM all_tables INTO @table_name
	END
CLOSE all_tables;
DEALLOCATE all_tables;

-- Recreate all the foreign key constraints from scripts stored in the temp table
DECLARE @recreate_sql varchar(2000)
DECLARE recreate_tables CURSOR
	FOR select recreate_sql from tmp_fk_recreate

	OPEN recreate_tables

	FETCH NEXT FROM recreate_tables INTO @recreate_sql

	WHILE @@FETCH_STATUS = 0

	BEGIN
		EXEC(\\\'\\\' + @recreate_sql + \\\'\\\')
		
		FETCH NEXT FROM recreate_tables INTO @recreate_sql
	END
CLOSE recreate_tables;
DEALLOCATE recreate_tables;

DROP TABLE TMP_FK_RECREATE;\');

END



IF (upper(BF_GetDatabaseTypeDW( )) = \'ORACLE\')
BEGIN

$LV_TABLE_NAME =
SQL(\'ODS_DS\', \'BEGIN
  FOR table_name IN (
    SELECT TABLE_NAME FROM USER_ALL_TABLES
        WHERE (TABLE_NAME like \\\'CS#_%\\\' ESCAPE \\\'#\\\'
          OR TABLE_NAME like \\\'TRF#_%\\\' ESCAPE \\\'#\\\'
          OR TABLE_NAME like \\\'REF#_%\\\' ESCAPE \\\'#\\\'
		  OR TABLE_NAME like \\\'K%#_%\\\' ESCAPE \\\'#\\\'
          OR TABLE_NAME in (\\\'ETL_TRF_EXCEPTION\\\', \\\'ETL_TRF_CONTROL\\\',\\\'ETL_ODS_CYCLE_STATUS\\\', \\\'ETL_ODS_AUDIT_METRIC\\\', \\\'ETL_ODS_CYCLE_STATISTIC\\\',
			\\\'ETL_ENGINE_ACTIVITY\\\',\\\'ETL_ENGINE_LOG\\\',\\\'ETL_ENGINE_STATS\\\',\\\'ETL_EXTRACT_STATS\\\'))
  )
  LOOP
     EXECUTE IMMEDIATE \\\'TRUNCATE TABLE \\\' || table_name.TABLE_NAME;
  END LOOP;
END;\');
END', "x" = '212', "y" = '-499')
BEGIN_SCRIPT
IF ((upper(BF_GetDatabaseTypeDW()) = 'SQL') )
BEGIN
sql('ODS_DS', 'SET NOCOUNT ON 

-- Creates temp table used to store scripts to recreate foreign key constraints
IF(select count(*) from sys.tables where name = \'TMP_FK_RECREATE\') = 0
BEGIN
	EXEC(\'create table tmp_fk_recreate(recreate_sql varchar(2000))\')
END
ELSE
	EXEC(\'TRUNCATE TABLE TMP_FK_RECREATE\')

DECLARE @tableName sysname 


-- Drops all foreign key constraints and loads scripts to recreate them into a temp table
DECLARE all_tables CURSOR
	FOR select name from sys.tables where type = \'U\' 
AND (name like \'CS#_%\' ESCAPE \'#\' 
OR name like \'Z_CS#_%\' ESCAPE \'#\' 
OR name like \'TRF#_%\' ESCAPE \'#\' 
OR name like \'Z_TRF#_%\' ESCAPE \'#\' 
OR name like \'REF#_%\' ESCAPE \'#\'
OR name like \'K%#_%\' ESCAPE \'#\'
OR name in (\'ETL_TRF_EXCEPTION\', \'ETL_TRF_CONTROL\',\'ETL_ODS_CYCLE_STATUS\', \'ETL_ODS_AUDIT_METRIC\', \'ETL_ODS_CYCLE_STATISTIC\',
	\'ETL_ENGINE_ACTIVITY\',\'ETL_ENGINE_LOG\',\'ETL_ENGINE_STATS\',\'ETL_EXTRACT_STATS\'))

OPEN all_tables

FETCH NEXT FROM all_tables INTO @tableName

WHILE @@FETCH_STATUS = 0

BEGIN

	DECLARE @cmd NVARCHAR(1000)
	DECLARE @drop_cmd NVARCHAR(1000)

	DECLARE  
	   @FK_NAME sysname, 
	   @FK_OBJECTID INT, 
	   @FK_DISABLED INT, 
	   @FK_NOT_FOR_REPLICATION INT, 
	   @DELETE_RULE    smallint,    
	   @UPDATE_RULE    smallint,    
	   @FKTABLE_NAME sysname, 
	   @FKTABLE_OWNER sysname, 
	   @PKTABLE_NAME sysname, 
	   @PKTABLE_OWNER sysname, 
	   @FKCOLUMN_NAME sysname, 
	   @PKCOLUMN_NAME sysname, 
	   @CONSTRAINT_COLID INT 


	DECLARE cursor_fkeys CURSOR FOR  
	   SELECT  Fk.name, 
			   Fk.OBJECT_ID,  
			   Fk.is_disabled,  
			   Fk.is_not_for_replication,  
			   Fk.delete_referential_action,  
			   Fk.update_referential_action,  
			   OBJECT_NAME(Fk.parent_object_id) AS Fk_table_name,  
			   schema_name(Fk.schema_id) AS Fk_table_schema,  
			   TbR.name AS Pk_table_name,  
			   schema_name(TbR.schema_id) Pk_table_schema 
	   FROM    sys.foreign_keys Fk LEFT OUTER JOIN  
			   sys.tables TbR ON TbR.OBJECT_ID = Fk.referenced_object_id --inner join  
	   WHERE   TbR.name = @tableName 

	OPEN cursor_fkeys 

	FETCH NEXT FROM   cursor_fkeys  
	   INTO @FK_NAME,@FK_OBJECTID, 
		   @FK_DISABLED, 
		   @FK_NOT_FOR_REPLICATION, 
		   @DELETE_RULE,    
		   @UPDATE_RULE,    
		   @FKTABLE_NAME, 
		   @FKTABLE_OWNER, 
		   @PKTABLE_NAME, 
		   @PKTABLE_OWNER 

	WHILE @@FETCH_STATUS = 0  
	BEGIN     

		   -- drop statement 
		   SET @drop_cmd = \'ALTER TABLE \\[\' + @FKTABLE_OWNER + \'\\].\\[\' + @FKTABLE_NAME  
		   + \'\\]  DROP CONSTRAINT \\[\' + @FK_NAME + \'\\]\'         

		   -- create process 
		   DECLARE @FKCOLUMNS VARCHAR(1000), @PKCOLUMNS VARCHAR(1000), @COUNTER INT 

		   -- create cursor to get FK columns 
		   DECLARE cursor_fkeyCols CURSOR FOR  
		   SELECT  COL_NAME(Fk.parent_object_id, Fk_Cl.parent_column_id) AS Fk_col_name,  
				   COL_NAME(Fk.referenced_object_id, Fk_Cl.referenced_column_id) AS Pk_col_name 
		   FROM    sys.foreign_keys Fk LEFT OUTER JOIN  
				   sys.tables TbR ON TbR.OBJECT_ID = Fk.referenced_object_id INNER JOIN  
				   sys.foreign_key_columns Fk_Cl ON Fk_Cl.constraint_object_id = Fk.OBJECT_ID  
		   WHERE   TbR.name = @tableName 
				   AND Fk_Cl.constraint_object_id = @FK_OBJECTID -- added 6/12/2008 
		   ORDER BY Fk_Cl.constraint_column_id 

		   OPEN cursor_fkeyCols 

		   FETCH NEXT FROM    cursor_fkeyCols INTO @FKCOLUMN_NAME,@PKCOLUMN_NAME 

		   SET @COUNTER = 1 
		   SET @FKCOLUMNS = \'\' 
		   SET @PKCOLUMNS = \'\' 
        
		   WHILE @@FETCH_STATUS = 0  
		   BEGIN  

			   IF @COUNTER > 1  
			   BEGIN 
				   SET @FKCOLUMNS = @FKCOLUMNS + \',\' 
				   SET @PKCOLUMNS = @PKCOLUMNS + \',\' 
			   END 

			   SET @FKCOLUMNS = @FKCOLUMNS + \'\\[\' + @FKCOLUMN_NAME + \'\\]\' 
			   SET @PKCOLUMNS = @PKCOLUMNS + \'\\[\' + @PKCOLUMN_NAME + \'\\]\' 

			   SET @COUNTER = @COUNTER + 1 
            
			   FETCH NEXT FROM    cursor_fkeyCols INTO @FKCOLUMN_NAME,@PKCOLUMN_NAME 
		   END 

		   CLOSE cursor_fkeyCols  
		   DEALLOCATE cursor_fkeyCols  

		   -- generate create FK statement 
		   SET @cmd = \'ALTER TABLE \\[\' + @FKTABLE_OWNER + \'\\].\\[\' + @FKTABLE_NAME + \'\\]  WITH \' +  
			   CASE @FK_DISABLED  
				   WHEN 0 THEN \' CHECK \' 
				   WHEN 1 THEN \' NOCHECK \' 
			   END +  \' ADD CONSTRAINT \\[\' + @FK_NAME  
			   + \'\\] FOREIGN KEY (\' + @FKCOLUMNS  
			   + \') REFERENCES \\[\' + @PKTABLE_OWNER + \'\\].\\[\' + @PKTABLE_NAME + \'\\] (\'  
			   + @PKCOLUMNS + \') ON UPDATE \' +  
			   CASE @UPDATE_RULE  
				   WHEN 0 THEN \' NO ACTION \' 
				   WHEN 1 THEN \' CASCADE \'  
				   WHEN 2 THEN \' SET_NULL \'  
				   END + \' ON DELETE \' +  
			   CASE @DELETE_RULE 
				   WHEN 0 THEN \' NO ACTION \'  
				   WHEN 1 THEN \' CASCADE \'  
				   WHEN 2 THEN \' SET_NULL \'  
				   END + \'\' + 
			   CASE @FK_NOT_FOR_REPLICATION 
				   WHEN 0 THEN \'\' 
				   WHEN 1 THEN \' NOT FOR REPLICATION \' 
			   END 

		  EXEC(\'INSERT INTO tmp_fk_recreate VALUES (\'\'\' + @cmd + \'\'\')\')
		  EXEC(@drop_cmd)
	   

	   FETCH NEXT FROM    cursor_fkeys  
		  INTO @FK_NAME,@FK_OBJECTID, 
			   @FK_DISABLED, 
			   @FK_NOT_FOR_REPLICATION, 
			   @DELETE_RULE,    
			   @UPDATE_RULE,    
			   @FKTABLE_NAME, 
			   @FKTABLE_OWNER, 
			   @PKTABLE_NAME, 
			   @PKTABLE_OWNER 
	END 

	CLOSE cursor_fkeys;
	DEALLOCATE cursor_fkeys;  

	FETCH NEXT FROM all_tables INTO @tableName
END

CLOSE all_tables;
DEALLOCATE all_tables;

-- Truncate all tables
DECLARE @table_name varchar(255)
DECLARE all_tables CURSOR
	FOR select name from sys.tables where type = \'U\' 
AND (name like \'CS#_%\' ESCAPE \'#\' 
OR name like \'TRF#_%\' ESCAPE \'#\' 
OR name like \'Z_CS#_%\' ESCAPE \'#\' 
OR name like \'Z_TRF#_%\' ESCAPE \'#\' 

OR name like \'REF#_%\' ESCAPE \'#\'
OR name like \'K%#_%\' ESCAPE \'#\'
OR name in (\'ETL_TRF_EXCEPTION\', \'ETL_TRF_CONTROL\',\'ETL_ODS_CYCLE_STATUS\', \'ETL_ODS_AUDIT_METRIC\', \'ETL_ODS_CYCLE_STATISTIC\',
	\'ETL_ENGINE_ACTIVITY\',\'ETL_ENGINE_LOG\',\'ETL_ENGINE_STATS\',\'ETL_EXTRACT_STATS\'))

	OPEN all_tables

	FETCH NEXT FROM all_tables INTO @table_name

	WHILE @@FETCH_STATUS = 0

	BEGIN
		EXEC(\'TRUNCATE TABLE \' + @table_name)

		FETCH NEXT FROM all_tables INTO @table_name
	END
CLOSE all_tables;
DEALLOCATE all_tables;

-- Recreate all the foreign key constraints from scripts stored in the temp table
DECLARE @recreate_sql varchar(2000)
DECLARE recreate_tables CURSOR
	FOR select recreate_sql from tmp_fk_recreate

	OPEN recreate_tables

	FETCH NEXT FROM recreate_tables INTO @recreate_sql

	WHILE @@FETCH_STATUS = 0

	BEGIN
		EXEC(\'\' + @recreate_sql + \'\')
		
		FETCH NEXT FROM recreate_tables INTO @recreate_sql
	END
CLOSE recreate_tables;
DEALLOCATE recreate_tables;

DROP TABLE TMP_FK_RECREATE;');END
IF ((upper(BF_GetDatabaseTypeDW()) = 'ORACLE') )
BEGIN
$LV_TABLE_NAME = sql('ODS_DS', 'BEGIN
  FOR table_name IN (
    SELECT TABLE_NAME FROM USER_ALL_TABLES
        WHERE (TABLE_NAME like \'CS#_%\' ESCAPE \'#\'
          OR TABLE_NAME like \'TRF#_%\' ESCAPE \'#\'
          OR TABLE_NAME like \'REF#_%\' ESCAPE \'#\'
		  OR TABLE_NAME like \'K%#_%\' ESCAPE \'#\'
          OR TABLE_NAME in (\'ETL_TRF_EXCEPTION\', \'ETL_TRF_CONTROL\',\'ETL_ODS_CYCLE_STATUS\', \'ETL_ODS_AUDIT_METRIC\', \'ETL_ODS_CYCLE_STATISTIC\',
			\'ETL_ENGINE_ACTIVITY\',\'ETL_ENGINE_LOG\',\'ETL_ENGINE_STATS\',\'ETL_EXTRACT_STATS\'))
  )
  LOOP
     EXECUTE IMMEDIATE \'TRUNCATE TABLE \' || table_name.TABLE_NAME;
  END LOOP;
END;');END
END
END
 SET ("run_once" = 'no', "unit_of_recovery" = 'no', "workflow_type" = 'Regular')

#!/bin/bash

lv_FilePath=$1
lv_CodePage=$2
lv_RepoFileName=$3
lv_Guid=$4
lv_SystemConfig=$5
lv_CMSHost=$6
lv_JobserverHost=$7
lv_JobserverPort=$8


#Execute DataServices AL_RWLauncher
$lv_FilePath/bin/AL_RWJobLauncher  "$lv_FilePath/log/$lv_JobserverHost/" -w "inet:$lv_JobserverHost:$lv_JobserverPort" " $lv_CodePage -R\"$lv_RepoFileName.txt\" -G"$lv_Guid" -t5 -T14 -Ksp$lv_SystemConfig -LocaleGV -CtBatch -Cm$lv_CMSHost -Cj$lv_JobserverHost -Cp$lv_JobserverPort "
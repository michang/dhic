/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2012                    */
/* Created on:     4/8/2016 11:45:28 AM                         */
/*==============================================================*/


/*==============================================================*/
/* Table: CS_ADDR_BASE                                          */
/*==============================================================*/
create table CS_ADDR_BASE (
   ADDR_BID             int                  not null,
   ADDR_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_ADDR_BASE
   add constraint CABA_PK primary key nonclustered (ADDR_KEY)
go

alter table CS_ADDR_BASE
   add constraint CABA_AK1 unique (ADDR_BID)
go

/*==============================================================*/
/* Table: CS_ADDR_DELTA                                         */
/*==============================================================*/
create table CS_ADDR_DELTA (
   ADDR_DID             int                  not null,
   ADDR_KEY             varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   STREET_1_NAME        varchar(255)         not null,
   STREET_2_NAME        varchar(255)         not null,
   STREET_3_NAME        varchar(255)         not null,
   CNTRY_CD             varchar(255)         not null,
   REGION_CD            varchar(255)         not null,
   CITY_NAME            varchar(255)         not null,
   POSTAL_CD            varchar(15)          not null,
   COUNTY_NAME          varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_ADDR_DELTA
   add constraint CADE_PK primary key nonclustered (ADDR_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_ADDR_DELTA
   add constraint CADE_AK1 unique (ADDR_DID)
go

/*==============================================================*/
/* Table: CS_AGENCY_BASE                                        */
/*==============================================================*/
create table CS_AGENCY_BASE (
   AGCY_BASE_ID         int                  not null,
   AGCY_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   AGCY_CD              varchar(255)         not null,
   AGCY_ADDR_1          varchar(255)         not null,
   AGCY_ADDR_2          varchar(255)         not null,
   AGCY_ADDR_3          varchar(255)         not null,
   AGCY_CITY            varchar(255)         not null,
   AGCY_STATE_CD        varchar(255)         not null,
   AGCY_ZIP_CD          varchar(15)          not null,
   AGCY_PHONE_NO        varchar(50)          not null,
   AGCY_APPT_DT         DATE                 null,
   AGCY_TERM_DT         DATE                 null,
   AGCY_STATUS_CD       varchar(255)         not null,
   MKT_MANAGER_NAME     varchar(255)         not null,
   MKTG_REP_CD          varchar(255)         not null,
   REGION_CD            varchar(255)         not null,
   AGCY_LIC_NO          varchar(255)         not null,
   AGCY_TAX_ID          varchar(15)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_AGENCY_BASE
   add constraint CABA2_PK primary key nonclustered (AGCY_KEY)
go

alter table CS_AGENCY_BASE
   add constraint CABA2_AK1 unique (AGCY_BASE_ID)
go

/*==============================================================*/
/* Table: CS_AGENCY_DELTA                                       */
/*==============================================================*/
create table CS_AGENCY_DELTA (
   AGCY_ID              int                  not null,
   AGCY_KEY             varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   AGCY_NAME            varchar(255)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_AGENCY_DELTA
   add constraint CADE2_PK primary key nonclustered (AGCY_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_AGENCY_DELTA
   add constraint CADE2_AK1 unique (AGCY_ID)
go

/*==============================================================*/
/* Table: CS_AGENT_BASE                                         */
/*==============================================================*/
create table CS_AGENT_BASE (
   AGNT_BASE_ID         int                  not null,
   AGNT_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   AGCY_KEY             varchar(100)         not null,
   AGNT_CD              varchar(255)         not null,
   AGNT_ADDR_1          varchar(255)         not null,
   AGNT_ADDR_2          varchar(255)         not null,
   AGNT_ADDR_3          varchar(255)         not null,
   AGNT_CITY            varchar(255)         not null,
   AGNT_STATE_CD        varchar(255)         not null,
   AGNT_ZIP_CD          varchar(15)          not null,
   AGNT_PHONE_NO        varchar(50)          not null,
   AGNT_APPT_DT         DATE                 null,
   AGNT_TERM_DT         DATE                 null,
   AGNT_STATUS_CD       varchar(255)         not null,
   MKT_MANAGER_NAME     varchar(255)         not null,
   MKTG_REP_CD          varchar(255)         not null,
   REGION_CD            varchar(255)         not null,
   AGNT_LIC_NO          varchar(255)         not null,
   AGNT_TAX_ID          varchar(15)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_AGENT_BASE
   add constraint CABA1_PK primary key nonclustered (AGNT_KEY)
go

alter table CS_AGENT_BASE
   add constraint CABA1_AK1 unique (AGNT_BASE_ID)
go

/*==============================================================*/
/* Index: CABA_CABA_XFK                                         */
/*==============================================================*/




create nonclustered index CABA_CABA_XFK on CS_AGENT_BASE (AGCY_KEY ASC)
go

/*==============================================================*/
/* Table: CS_AGENT_DELTA                                        */
/*==============================================================*/
create table CS_AGENT_DELTA (
   AGNT_KEY             varchar(100)         not null,
   AGNT_ID              int                  not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   AGNT_NAME            varchar(255)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_AGENT_DELTA
   add constraint CADE1_PK primary key nonclustered (AGNT_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_AGENT_DELTA
   add constraint CADE1_AK1 unique (AGNT_ID)
go

/*==============================================================*/
/* Table: CS_AUDIT_INFO_BASE                                    */
/*==============================================================*/
create table CS_AUDIT_INFO_BASE (
   AUDIT_INFO_BID       int                  not null,
   AUDIT_INFO_KEY       varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_AUDIT_INFO_BASE
   add constraint CAIB_PK primary key nonclustered (AUDIT_INFO_KEY)
go

alter table CS_AUDIT_INFO_BASE
   add constraint CAIB_AK1 unique (AUDIT_INFO_BID)
go

/*==============================================================*/
/* Index: CAIB_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CAIB_CPBA_XFK on CS_AUDIT_INFO_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_AUDIT_INFO_DELTA                                   */
/*==============================================================*/
create table CS_AUDIT_INFO_DELTA (
   AUDIT_INFO_DID       int                  not null,
   AUDIT_INFO_KEY       varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   AUDIT_METHOD_CD      varchar(255)         not null,
   AUDIT_PRD_START_DT   DATE                 not null,
   AUDIT_PRD_END_DT     DATE                 not null,
   AUDIT_SCHED_TYPE_CD  varchar(255)         not null,
   ACT_AUDIT_METHOD_CD  varchar(255)         not null,
   AUDIT_FEE_AMT        numeric(18,2)        null,
   AUDT_FEE_CURR_CD     varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_AUDIT_INFO_DELTA
   add constraint CAID_PK primary key nonclustered (AUDIT_INFO_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_AUDIT_INFO_DELTA
   add constraint CAID_AK1 unique (AUDIT_INFO_DID)
go

/*==============================================================*/
/* Table: CS_BILL_ACCT_BASE                                     */
/*==============================================================*/
create table CS_BILL_ACCT_BASE (
   BILL_ACCT_BID        int                  identity,
   BILL_ACCT_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_ACCT_BASE
   add constraint CBAB_PK primary key nonclustered (BILL_ACCT_KEY)
go

alter table CS_BILL_ACCT_BASE
   add constraint CBAB_AK1 unique (BILL_ACCT_BID)
go

/*==============================================================*/
/* Table: CS_BILL_ACCT_DELTA                                    */
/*==============================================================*/
create table CS_BILL_ACCT_DELTA (
   BILL_ACCT_DID        int                  identity,
   BILL_ACCT_KEY        varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_PLAN_KEY        varchar(100)         not null,
   ALLOC_PLAN_KEY       varchar(100)         not null,
   DELNQNT_PLAN_KEY     varchar(100)         not null,
   COLLECTION_AGCY_KEY  varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   SECURITY_ZONE_KEY    varchar(100)         not null,
   CLOSE_DTS            DATETIME2            null,
   EOW_INV_ANCHOR_DT    DATE                 null,
   ACCT_NO              varchar(255)         not null,
   FIRST_TPM_INV_DOM    int                  not null,
   SECOND_TPM_INV_DOM   int                  not null,
   INV_DAY_OF_MTH       int                  not null,
   INV_DAY_OF_WEEK      int                  not null,
   INV_DELIVERY_TYPE_CD varchar(255)         not null,
   FEIN_CD              varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   DELNQNT_STATUS_CD    varchar(255)         not null,
   ACCT_TYPE_CD         varchar(255)         not null,
   DISTR_LMT_TYPE_CD    varchar(255)         not null,
   BILL_DT_TYPE_CD      varchar(255)         not null,
   BILL_LEVEL_CD        varchar(255)         not null,
   ORG_TYPE_CD          varchar(255)         not null,
   ACCT_SEGMENT_CD      varchar(255)         not null,
   ACCT_NAME            varchar(255)         not null,
   ACCT_DBA_NAME        varchar(255)         not null,
   SRVC_TIER_CD         varchar(255)         not null,
   NP_PMT_DISTRB_FL     char(1)              not null,
   COLLECTING_FL        char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_ACCT_DELTA
   add constraint CBAD_PK primary key nonclustered (BILL_ACCT_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_ACCT_DELTA
   add constraint CBAD_AK1 unique (BILL_ACCT_DID)
go

/*==============================================================*/
/* Index: CBAD_CBSZB_XFK                                        */
/*==============================================================*/




create nonclustered index CBAD_CBSZB_XFK on CS_BILL_ACCT_DELTA (SECURITY_ZONE_KEY ASC)
go

/*==============================================================*/
/* Index: CBAD_CBTCB_XFK                                        */
/*==============================================================*/




create nonclustered index CBAD_CBTCB_XFK on CS_BILL_ACCT_DELTA (TACCT_CONTAINER_KEY ASC)
go

/*==============================================================*/
/* Index: CBAD_CBPB_XFK                                         */
/*==============================================================*/




create nonclustered index CBAD_CBPB_XFK on CS_BILL_ACCT_DELTA (BILL_PLAN_KEY ASC)
go

/*==============================================================*/
/* Index: CBAD_CBPB1_XFK                                        */
/*==============================================================*/




create nonclustered index CBAD_CBPB1_XFK on CS_BILL_ACCT_DELTA (ALLOC_PLAN_KEY ASC)
go

/*==============================================================*/
/* Index: CBAD_CBPB2_XFK                                        */
/*==============================================================*/




create nonclustered index CBAD_CBPB2_XFK on CS_BILL_ACCT_DELTA (DELNQNT_PLAN_KEY ASC)
go

/*==============================================================*/
/* Index: CBAD_CBCAB_XFK                                        */
/*==============================================================*/




create nonclustered index CBAD_CBCAB_XFK on CS_BILL_ACCT_DELTA (COLLECTION_AGCY_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_AP_TRAN                                       */
/*==============================================================*/
create table CS_BILL_AP_TRAN (
   AP_JRNL_TID          int                  identity,
   AP_JRNL_KEY          varchar(100)         not null,
   CTX_KEY              varchar(100)         not null,
   PRDCR_KEY            varchar(100)         not null,
   CR_PYBL_OF_PRDCR_KEY varchar(100)         not null,
   PRDCR_CD_KEY         varchar(100)         not null,
   CHRG_COMM_KEY        varchar(100)         not null,
   BILL_INV_ITEM_KEY    varchar(100)         not null,
   COMM_RED_KEY         varchar(100)         not null,
   PRDCR_PMT_KEY        varchar(100)         not null,
   PRDCR_STMNT_KEY      varchar(100)         not null,
   PYBL_RECEIVER_STMNT_KEY varchar(100)         not null,
   AGCY_CYCLE_DISTR_KEY varchar(100)         not null,
   DISTR_ITEM_KEY       varchar(100)         not null,
   PRDCR_PYBL_TFR_KEY   varchar(100)         not null,
   INCOMMING_PRDCR_PMT_KEY varchar(100)         not null,
   NEG_WO_KEY           varchar(100)         not null,
   WO_KEY               varchar(100)         not null,
   POL_COMM_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   TRANS_NO             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   PYBL_CRITERIA_CD     varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_AP_TRAN
   add constraint CBAT1_PK primary key nonclustered (AP_JRNL_TID)
go

alter table CS_BILL_AP_TRAN
   add constraint CBAT1_AK1 unique (AP_JRNL_KEY)
go

/*==============================================================*/
/* Index: CBAT_CBDB_XFK                                         */
/*==============================================================*/




create nonclustered index CBAT_CBDB_XFK on CS_BILL_AP_TRAN (AGCY_CYCLE_DISTR_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBDIB1_XFK                                       */
/*==============================================================*/




create nonclustered index CBAT_CBDIB1_XFK on CS_BILL_AP_TRAN (DISTR_ITEM_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBCCB_XFK                                        */
/*==============================================================*/




create nonclustered index CBAT_CBCCB_XFK on CS_BILL_AP_TRAN (CHRG_COMM_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBCRB_XFK                                        */
/*==============================================================*/




create nonclustered index CBAT_CBCRB_XFK on CS_BILL_AP_TRAN (COMM_RED_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBIPPB_XFK                                       */
/*==============================================================*/




create nonclustered index CBAT_CBIPPB_XFK on CS_BILL_AP_TRAN (INCOMMING_PRDCR_PMT_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBIIB1_XFK                                       */
/*==============================================================*/




create nonclustered index CBAT_CBIIB1_XFK on CS_BILL_AP_TRAN (BILL_INV_ITEM_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBNWB_XFK                                        */
/*==============================================================*/




create nonclustered index CBAT_CBNWB_XFK on CS_BILL_AP_TRAN (NEG_WO_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBPCB_XFK                                        */
/*==============================================================*/




create nonclustered index CBAT_CBPCB_XFK on CS_BILL_AP_TRAN (POL_COMM_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBPB1_XFK                                        */
/*==============================================================*/




create nonclustered index CBAT_CBPB1_XFK on CS_BILL_AP_TRAN (PRDCR_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBPB2_XFK                                        */
/*==============================================================*/




create nonclustered index CBAT_CBPB2_XFK on CS_BILL_AP_TRAN (CR_PYBL_OF_PRDCR_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBPPTB_XFK                                       */
/*==============================================================*/




create nonclustered index CBAT_CBPPTB_XFK on CS_BILL_AP_TRAN (PRDCR_PYBL_TFR_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBPPB_XFK                                        */
/*==============================================================*/




create nonclustered index CBAT_CBPPB_XFK on CS_BILL_AP_TRAN (PRDCR_PMT_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBPSB_XFK                                        */
/*==============================================================*/




create nonclustered index CBAT_CBPSB_XFK on CS_BILL_AP_TRAN (PRDCR_STMNT_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBWB1_XFK                                        */
/*==============================================================*/




create nonclustered index CBAT_CBWB1_XFK on CS_BILL_AP_TRAN (WO_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBPCB1_XFK                                       */
/*==============================================================*/




create nonclustered index CBAT_CBPCB1_XFK on CS_BILL_AP_TRAN (PRDCR_CD_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBPSB1_XFK                                       */
/*==============================================================*/




create nonclustered index CBAT_CBPSB1_XFK on CS_BILL_AP_TRAN (PYBL_RECEIVER_STMNT_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_AR_TRAN                                       */
/*==============================================================*/
create table CS_BILL_AR_TRAN (
   AR_JRNL_TID          int                  identity,
   AR_JRNL_KEY          varchar(100)         not null,
   CHRG_KEY             varchar(100)         not null,
   DISTR_ITEM_KEY       varchar(100)         not null,
   FROM_ACCT_KEY        varchar(100)         not null,
   FROM_PRDCR_KEY       varchar(100)         not null,
   BILL_INV_ITEM_KEY    varchar(100)         not null,
   CHRG_COMM_KEY        varchar(100)         not null,
   WO_KEY               varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TRANS_PROC_DT_ID     int                  not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   TRANS_PROC_MTH_ID    int                  not null,
   TRANS_NO             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   WO_CHANNEL_CD        varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_AR_TRAN
   add constraint CBAT_PK primary key nonclustered (AR_JRNL_TID)
go

alter table CS_BILL_AR_TRAN
   add constraint CBAT_AK1 unique (AR_JRNL_KEY)
go

/*==============================================================*/
/* Index: CBAT_CBIIB_XFK                                        */
/*==============================================================*/




create nonclustered index CBAT_CBIIB_XFK on CS_BILL_AR_TRAN (BILL_INV_ITEM_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBDIB_XFK                                        */
/*==============================================================*/




create nonclustered index CBAT_CBDIB_XFK on CS_BILL_AR_TRAN (DISTR_ITEM_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBCB_XFK                                         */
/*==============================================================*/




create nonclustered index CBAT_CBCB_XFK on CS_BILL_AR_TRAN (CHRG_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBPB_XFK                                         */
/*==============================================================*/




create nonclustered index CBAT_CBPB_XFK on CS_BILL_AR_TRAN (FROM_PRDCR_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBWB_XFK                                         */
/*==============================================================*/




create nonclustered index CBAT_CBWB_XFK on CS_BILL_AR_TRAN (WO_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBAB_XFK                                         */
/*==============================================================*/




create nonclustered index CBAT_CBAB_XFK on CS_BILL_AR_TRAN (FROM_ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBAT_CBCCB1_XFK                                       */
/*==============================================================*/




create nonclustered index CBAT_CBCCB1_XFK on CS_BILL_AR_TRAN (CHRG_COMM_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_CASH_TRAN                                     */
/*==============================================================*/
create table CS_BILL_CASH_TRAN (
   CASH_JRNL_TID        int                  identity,
   CASH_JRNL_KEY        varchar(100)         not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   MON_RCVD_KEY         varchar(100)         not null,
   PRDCR_KEY            varchar(100)         not null,
   DISB_KEY             varchar(100)         not null,
   NEG_WO_KEY           varchar(100)         not null,
   UNAPPLIED_FUND_KEY   varchar(100)         not null,
   COLTRL_KEY           varchar(100)         not null,
   SRCE_COLTRL_REQ_KEY  varchar(100)         not null,
   COLTRL_REQ_KEY       varchar(100)         not null,
   SUSP_PMT_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   TRANS_NO             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   WO_CHANNEL_CD        varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_CASH_TRAN
   add constraint CBCT_PK primary key nonclustered (CASH_JRNL_TID)
go

alter table CS_BILL_CASH_TRAN
   add constraint CBCT_AK1 unique (CASH_JRNL_KEY)
go

/*==============================================================*/
/* Index: CBCT_CBAB_XFK                                         */
/*==============================================================*/




create nonclustered index CBCT_CBAB_XFK on CS_BILL_CASH_TRAN (BILL_ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBCT_CBDB_XFK                                         */
/*==============================================================*/




create nonclustered index CBCT_CBDB_XFK on CS_BILL_CASH_TRAN (DISB_KEY ASC)
go

/*==============================================================*/
/* Index: CBCT_CBNWB_XFK                                        */
/*==============================================================*/




create nonclustered index CBCT_CBNWB_XFK on CS_BILL_CASH_TRAN (NEG_WO_KEY ASC)
go

/*==============================================================*/
/* Index: CBCT_CBPB_XFK                                         */
/*==============================================================*/




create nonclustered index CBCT_CBPB_XFK on CS_BILL_CASH_TRAN (PRDCR_KEY ASC)
go

/*==============================================================*/
/* Index: CBCT_CBCRB_XFK                                        */
/*==============================================================*/




create nonclustered index CBCT_CBCRB_XFK on CS_BILL_CASH_TRAN (COLTRL_REQ_KEY ASC)
go

/*==============================================================*/
/* Index: CBCT_CBCRB1_XFK                                       */
/*==============================================================*/




create nonclustered index CBCT_CBCRB1_XFK on CS_BILL_CASH_TRAN (SRCE_COLTRL_REQ_KEY ASC)
go

/*==============================================================*/
/* Index: CBCT_CBCB_XFK                                         */
/*==============================================================*/




create nonclustered index CBCT_CBCB_XFK on CS_BILL_CASH_TRAN (COLTRL_KEY ASC)
go

/*==============================================================*/
/* Index: CBCT_CBMRB_XFK                                        */
/*==============================================================*/




create nonclustered index CBCT_CBMRB_XFK on CS_BILL_CASH_TRAN (MON_RCVD_KEY ASC)
go

/*==============================================================*/
/* Index: CBCT_CBSPB_XFK                                        */
/*==============================================================*/




create nonclustered index CBCT_CBSPB_XFK on CS_BILL_CASH_TRAN (SUSP_PMT_KEY ASC)
go

/*==============================================================*/
/* Index: CBCT_CBUFB_XFK                                        */
/*==============================================================*/




create nonclustered index CBCT_CBUFB_XFK on CS_BILL_CASH_TRAN (UNAPPLIED_FUND_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_CHRG_BASE                                     */
/*==============================================================*/
create table CS_BILL_CHRG_BASE (
   CHRG_BID             int                  identity,
   CHRG_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_CHRG_BASE
   add constraint CBCB_PK primary key nonclustered (CHRG_KEY)
go

alter table CS_BILL_CHRG_BASE
   add constraint CBCB_AK1 unique (CHRG_BID)
go

/*==============================================================*/
/* Table: CS_BILL_CHRG_COMM_BASE                                */
/*==============================================================*/
create table CS_BILL_CHRG_COMM_BASE (
   CHRG_COMM_BID        int                  identity,
   CHRG_COMM_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_CHRG_COMM_BASE
   add constraint CBCCB_PK primary key nonclustered (CHRG_COMM_KEY)
go

alter table CS_BILL_CHRG_COMM_BASE
   add constraint CBCCB_AK1 unique (CHRG_COMM_BID)
go

/*==============================================================*/
/* Table: CS_BILL_CHRG_COMM_DELTA                               */
/*==============================================================*/
create table CS_BILL_CHRG_COMM_DELTA (
   CHRG_COMM_DID        int                  identity,
   CHRG_COMM_KEY        varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   POL_COMM_KEY         varchar(100)         not null,
   CHRG_KEY             varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   PYBL_CRITERIA_CD     varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_CHRG_COMM_DELTA
   add constraint CBCCD_PK primary key nonclustered (CHRG_COMM_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_CHRG_COMM_DELTA
   add constraint CBCCD_AK1 unique (CHRG_COMM_DID)
go

/*==============================================================*/
/* Index: CBCCD_CBPCB_XFK                                       */
/*==============================================================*/




create nonclustered index CBCCD_CBPCB_XFK on CS_BILL_CHRG_COMM_DELTA (POL_COMM_KEY ASC)
go

/*==============================================================*/
/* Index: CBCCD_CBCB_XFK                                        */
/*==============================================================*/




create nonclustered index CBCCD_CBCB_XFK on CS_BILL_CHRG_COMM_DELTA (CHRG_KEY ASC)
go

/*==============================================================*/
/* Index: CBCCD_CBTCB_XFK                                       */
/*==============================================================*/




create nonclustered index CBCCD_CBTCB_XFK on CS_BILL_CHRG_COMM_DELTA (TACCT_CONTAINER_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_CHRG_DELTA                                    */
/*==============================================================*/
create table CS_BILL_CHRG_DELTA (
   CHRG_DID             int                  identity,
   CHRG_KEY             varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CHRG_PAT_KEY         varchar(100)         not null,
   BILL_INSTR_KEY       varchar(100)         not null,
   PRIM_COMM_PRDCR_CD_KEY varchar(100)         not null,
   INV_STREAM_KEY       varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   CHRG_DTS             DATETIME2            not null,
   WRITTEN_DTS          DATETIME2            null,
   HOLD_RELEASE_DTS     DATETIME2            null,
   HOLD_STATUS_CD       varchar(255)         not null,
   TOT_INSTALLMENT_NO   int                  not null,
   CHRG_GROUP_CD        varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   CHRG_AMT             numeric(18,2)        not null,
   REVERSED_FL          char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_CHRG_DELTA
   add constraint CBCD_PK primary key nonclustered (CHRG_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_CHRG_DELTA
   add constraint CBCD_AK1 unique (CHRG_DID)
go

/*==============================================================*/
/* Index: CBCD_CBCPB_XFK                                        */
/*==============================================================*/




create nonclustered index CBCD_CBCPB_XFK on CS_BILL_CHRG_DELTA (CHRG_PAT_KEY ASC)
go

/*==============================================================*/
/* Index: CBCD_CBIB_XFK                                         */
/*==============================================================*/




create nonclustered index CBCD_CBIB_XFK on CS_BILL_CHRG_DELTA (BILL_INSTR_KEY ASC)
go

/*==============================================================*/
/* Index: CBCD_CBPCB_XFK                                        */
/*==============================================================*/




create nonclustered index CBCD_CBPCB_XFK on CS_BILL_CHRG_DELTA (PRIM_COMM_PRDCR_CD_KEY ASC)
go

/*==============================================================*/
/* Index: CBCD_CBISB_XFK                                        */
/*==============================================================*/




create nonclustered index CBCD_CBISB_XFK on CS_BILL_CHRG_DELTA (INV_STREAM_KEY ASC)
go

/*==============================================================*/
/* Index: CBCD_CBTCB_XFK                                        */
/*==============================================================*/




create nonclustered index CBCD_CBTCB_XFK on CS_BILL_CHRG_DELTA (TACCT_CONTAINER_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_CHRG_PAT_BASE                                 */
/*==============================================================*/
create table CS_BILL_CHRG_PAT_BASE (
   CHRG_PAT_BID         int                  identity,
   CHRG_PAT_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_CHRG_PAT_BASE
   add constraint CBCPB_PK primary key nonclustered (CHRG_PAT_KEY)
go

alter table CS_BILL_CHRG_PAT_BASE
   add constraint CBCPB_AK1 unique (CHRG_PAT_BID)
go

/*==============================================================*/
/* Table: CS_BILL_CHRG_PAT_DELTA                                */
/*==============================================================*/
create table CS_BILL_CHRG_PAT_DELTA (
   CHRG_PAT_DID         int                  identity,
   CHRG_PAT_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TACCT_OWNER_PAT_KEY  varchar(100)         not null,
   CHRG_PAT_TYPE_CD     varchar(255)         not null,
   CHRG_CD              varchar(255)         not null,
   CHRG_NAME            varchar(255)         not null,
   CHRG_CATG_CD         varchar(255)         not null,
   PERIODICITY_CD       varchar(255)         not null,
   PRIORITY_CD          varchar(255)         not null,
   INV_TRTMNT_CD        varchar(255)         not null,
   INC_IN_EQUITY_DATING_FL char(1)              not null,
   RETIRED_FL           char(1)              not null,
   REVERSIBLE_FL        char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_CHRG_PAT_DELTA
   add constraint CBCPD_PK primary key nonclustered (CHRG_PAT_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_CHRG_PAT_DELTA
   add constraint CBCPD_AK1 unique (CHRG_PAT_DID)
go

/*==============================================================*/
/* Index: CBCPD_CBTOPB_XFK                                      */
/*==============================================================*/




create nonclustered index CBCPD_CBTOPB_XFK on CS_BILL_CHRG_PAT_DELTA (TACCT_OWNER_PAT_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_COMM_AGG                                      */
/*==============================================================*/
create table CS_BILL_COMM_AGG (
   TRANS_MTH_ID         int                  not null,
   PRDCR_KEY            varchar(100)         not null,
   PRDCR_CD_KEY         varchar(100)         not null,
   ROLE_CD              varchar(255)         not null,
   CHRG_KEY             varchar(100)         not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   BILL_POL_KEY         varchar(100)         not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   COMM_SPLAN_RATE_KEY  varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PMT_TYPE_CD          varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   AGNT_COMM_RSRV_AMT   numeric(18,2)        not null,
   AGNT_RSRV_CHNG_AMT   numeric(18,2)        not null,
   AGNT_COMM_EARNED_AMT numeric(18,2)        not null,
   AGNT_COMM_ADJ_AMT    numeric(18,2)        not null,
   BONUS_COMM_AMT       numeric(18,2)        not null,
   INCENTIVE_COMM_AMT   numeric(18,2)        not null,
   AGCY_COMM_EARNED_AMT numeric(18,2)        not null,
   STD_COMM_PD_AMT      numeric(18,2)        not null,
   NORMAL_COMM_PD_AMT   numeric(18,2)        not null,
   AGCY_PMT_RCVD_AMT    numeric(18,2)        not null,
   ADVANCE_COMM_PD_AMT  numeric(18,2)        not null,
   ADVANCE_COMM_REPAID_AMT numeric(18,2)        not null,
   COMM_TFR_AMT         numeric(18,2)        not null,
   COMM_WO_AMT          numeric(18,2)        not null,
   COMM_RATE            numeric(6,3)         not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_COMM_AGG
   add constraint CBCA_PK primary key nonclustered (TRANS_MTH_ID, SOURCE_SYSTEM, PRDCR_KEY, PRDCR_CD_KEY, ROLE_CD, CHRG_KEY, COMM_SPLAN_RATE_KEY, PMT_TYPE_CD)
go

/*==============================================================*/
/* Index: CBCA_CBAB_XFK                                         */
/*==============================================================*/




create nonclustered index CBCA_CBAB_XFK on CS_BILL_COMM_AGG (BILL_ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBCA_CBPB_XFK                                         */
/*==============================================================*/




create nonclustered index CBCA_CBPB_XFK on CS_BILL_COMM_AGG (PRDCR_KEY ASC)
go

/*==============================================================*/
/* Index: CBCA_CBPCB_XFK                                        */
/*==============================================================*/




create nonclustered index CBCA_CBPCB_XFK on CS_BILL_COMM_AGG (PRDCR_CD_KEY ASC)
go

/*==============================================================*/
/* Index: CBCA_CBPPB_XFK                                        */
/*==============================================================*/




create nonclustered index CBCA_CBPPB_XFK on CS_BILL_COMM_AGG (BILL_POL_PRD_KEY ASC)
go

/*==============================================================*/
/* Index: CBCA_CBPB1_XFK                                        */
/*==============================================================*/




create nonclustered index CBCA_CBPB1_XFK on CS_BILL_COMM_AGG (BILL_POL_KEY ASC)
go

/*==============================================================*/
/* Index: CBCA_CBCB_XFK                                         */
/*==============================================================*/




create nonclustered index CBCA_CBCB_XFK on CS_BILL_COMM_AGG (CHRG_KEY ASC)
go

/*==============================================================*/
/* Index: CBCA_CBCSRB_XFK                                       */
/*==============================================================*/




create nonclustered index CBCA_CBCSRB_XFK on CS_BILL_COMM_AGG (COMM_SPLAN_RATE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_COMM_REDUCT_BASE                              */
/*==============================================================*/
create table CS_BILL_COMM_REDUCT_BASE (
   COMM_RED_BID         int                  identity,
   COMM_RED_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_COMM_REDUCT_BASE
   add constraint CBCRB_PK primary key nonclustered (COMM_RED_KEY)
go

alter table CS_BILL_COMM_REDUCT_BASE
   add constraint CBCRB_AK1 unique (COMM_RED_BID)
go

/*==============================================================*/
/* Table: CS_BILL_COMM_REDUCT_DELTA                             */
/*==============================================================*/
create table CS_BILL_COMM_REDUCT_DELTA (
   COMM_RED_DID         int                  identity,
   COMM_RED_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   WO_KEY               varchar(100)         not null,
   BILL_INV_ITEM_KEY    varchar(100)         not null,
   CHRG_COMM_KEY        varchar(100)         not null,
   WRITE_OFF_DTS        DATETIME2            null,
   COMM_RED_TYPE_CD     varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   COMM_RED_AMT         numeric(18,2)        not null,
   REVERSED_FL          char(1)              not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_COMM_REDUCT_DELTA
   add constraint CBCRD_PK primary key nonclustered (COMM_RED_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_COMM_REDUCT_DELTA
   add constraint CBCRD_AK1 unique (COMM_RED_DID)
go

/*==============================================================*/
/* Index: CBCRD_CBCCB_XFK                                       */
/*==============================================================*/




create nonclustered index CBCRD_CBCCB_XFK on CS_BILL_COMM_REDUCT_DELTA (CHRG_COMM_KEY ASC)
go

/*==============================================================*/
/* Index: CBCRD_CBIIB_XFK                                       */
/*==============================================================*/




create nonclustered index CBCRD_CBIIB_XFK on CS_BILL_COMM_REDUCT_DELTA (BILL_INV_ITEM_KEY ASC)
go

/*==============================================================*/
/* Index: CBCRD_CBWB_XFK                                        */
/*==============================================================*/




create nonclustered index CBCRD_CBWB_XFK on CS_BILL_COMM_REDUCT_DELTA (WO_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_COMM_SPLAN_BASE                               */
/*==============================================================*/
create table CS_BILL_COMM_SPLAN_BASE (
   COMM_SPLAN_BID       int                  identity,
   COMM_SPLAN_KEY       varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_COMM_SPLAN_BASE
   add constraint CBCSB_PK primary key nonclustered (COMM_SPLAN_KEY)
go

alter table CS_BILL_COMM_SPLAN_BASE
   add constraint CBCSB_AK1 unique (COMM_SPLAN_BID)
go

/*==============================================================*/
/* Table: CS_BILL_COMM_SPLAN_DELTA                              */
/*==============================================================*/
create table CS_BILL_COMM_SPLAN_DELTA (
   COMM_SPLAN_DID       int                  identity,
   COMM_SPLAN_KEY       varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PLAN_KEY             varchar(100)         not null,
   SPLAN_NAME           varchar(255)         not null,
   SPLAN_ORDER          int                  not null,
   PYBL_CRITERIA_CD     varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_COMM_SPLAN_DELTA
   add constraint CBCSD_PK primary key nonclustered (COMM_SPLAN_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_COMM_SPLAN_DELTA
   add constraint CBCSD_AK1 unique (COMM_SPLAN_DID)
go

/*==============================================================*/
/* Index: CBCSD_CBPB_XFK                                        */
/*==============================================================*/




create nonclustered index CBCSD_CBPB_XFK on CS_BILL_COMM_SPLAN_DELTA (PLAN_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_COMM_SPLAN_RATE_BASE                          */
/*==============================================================*/
create table CS_BILL_COMM_SPLAN_RATE_BASE (
   COMM_SPLAN_RATE_BID  int                  identity,
   COMM_SPLAN_RATE_KEY  varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_COMM_SPLAN_RATE_BASE
   add constraint CBCSRB_PK primary key nonclustered (COMM_SPLAN_RATE_KEY)
go

alter table CS_BILL_COMM_SPLAN_RATE_BASE
   add constraint CBCSRB_AK1 unique (COMM_SPLAN_RATE_BID)
go

/*==============================================================*/
/* Table: CS_BILL_COMM_SPLAN_RATE_DELTA                         */
/*==============================================================*/
create table CS_BILL_COMM_SPLAN_RATE_DELTA (
   COMM_SPLAN_RATE_DID  int                  identity,
   COMM_SPLAN_RATE_KEY  varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   COMM_SPLAN_KEY       varchar(100)         not null,
   ROLE_CD              varchar(255)         not null,
   COMM_RATE            numeric(6,3)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_COMM_SPLAN_RATE_DELTA
   add constraint CBCSRD_PK primary key nonclustered (COMM_SPLAN_RATE_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_COMM_SPLAN_RATE_DELTA
   add constraint CBCSRD_AK1 unique (COMM_SPLAN_RATE_DID)
go

/*==============================================================*/
/* Index: CBCSRD_CBCSB_XFK                                      */
/*==============================================================*/




create nonclustered index CBCSRD_CBCSB_XFK on CS_BILL_COMM_SPLAN_RATE_DELTA (COMM_SPLAN_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_CREDIT_BASE                                   */
/*==============================================================*/
create table CS_BILL_CREDIT_BASE (
   CR_BID               int                  identity,
   CR_KEY               varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_CREDIT_BASE
   add constraint CBCB2_PK primary key nonclustered (CR_KEY)
go

alter table CS_BILL_CREDIT_BASE
   add constraint CBCB1_AK1 unique (CR_BID)
go

/*==============================================================*/
/* Table: CS_BILL_CREDIT_DELTA                                  */
/*==============================================================*/
create table CS_BILL_CREDIT_DELTA (
   CR_DID               int                  identity,
   CR_KEY               varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   UNAPPLIED_FUND_KEY   varchar(100)         not null,
   REQUESTING_USER_KEY  varchar(100)         not null,
   CR_DTS               DATETIME2            null,
   APPRVL_DTS           DATETIME2            null,
   APPRVL_STATUS_CD     varchar(255)         not null,
   CR_TYPE_CD           varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   CR_AMT               numeric(18,2)        not null,
   RETIRED_FL           char(1)              not null,
   REVERSED_FL          char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_CREDIT_DELTA
   add constraint CBCD1_PK primary key nonclustered (CR_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_CREDIT_DELTA
   add constraint CBCD1_AK1 unique (CR_DID)
go

/*==============================================================*/
/* Index: CBCD_CBAB_XFK                                         */
/*==============================================================*/




create nonclustered index CBCD_CBAB_XFK on CS_BILL_CREDIT_DELTA (BILL_ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBCD_CBUFB_XFK                                        */
/*==============================================================*/




create nonclustered index CBCD_CBUFB_XFK on CS_BILL_CREDIT_DELTA (UNAPPLIED_FUND_KEY ASC)
go

/*==============================================================*/
/* Index: CBCD_CBUB_XFK                                         */
/*==============================================================*/




create nonclustered index CBCD_CBUB_XFK on CS_BILL_CREDIT_DELTA (REQUESTING_USER_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_DISB_BASE                                     */
/*==============================================================*/
create table CS_BILL_DISB_BASE (
   DISB_BID             int                  identity,
   DISB_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_DISB_BASE
   add constraint CBDB1_PK primary key nonclustered (DISB_KEY)
go

alter table CS_BILL_DISB_BASE
   add constraint CBDB1_AK1 unique (DISB_BID)
go

/*==============================================================*/
/* Table: CS_BILL_DISB_DELTA                                    */
/*==============================================================*/
create table CS_BILL_DISB_DELTA (
   DISB_DID             int                  identity,
   DISB_KEY             varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   RPT_GROUP_KEY        varchar(100)         not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   PRDCR_KEY            varchar(100)         not null,
   DISTR_KEY            varchar(100)         not null,
   PMT_INSTRUMENT_KEY   varchar(100)         not null,
   COLTRL_KEY           varchar(100)         not null,
   SUSP_PMT_KEY         varchar(100)         not null,
   UNAPPLIED_FUND_KEY   varchar(100)         not null,
   REQUESTING_USER_KEY  varchar(100)         not null,
   ADDR_TEXT            varchar(1333)        not null,
   CURR_CD              varchar(255)         not null,
   DISB_AMT             numeric(18,2)        not null,
   DUE_DT               DATE                 not null,
   APPRVL_DTS           DATETIME2            null,
   CLOSE_DTS            DATETIME2            null,
   APPRVL_STATUS_CD     varchar(255)         not null,
   DISB_NO              varchar(255)         not null,
   MAIL_TO_NAME         varchar(255)         not null,
   MEMO_TEXT            varchar(255)         not null,
   PAY_TO_NAME          varchar(255)         not null,
   REASON_CD            varchar(255)         not null,
   REF_NO               varchar(255)         not null,
   STATUS_CD            varchar(255)         not null,
   DISB_TYPE_CD         varchar(255)         not null,
   VOID_REASON_CD       varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_DISB_DELTA
   add constraint CBDD_PK primary key nonclustered (DISB_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_DISB_DELTA
   add constraint CBDD_AK1 unique (DISB_DID)
go

/*==============================================================*/
/* Index: CBDD_CRGB_XFK                                         */
/*==============================================================*/




create nonclustered index CBDD_CRGB_XFK on CS_BILL_DISB_DELTA (RPT_GROUP_KEY ASC)
go

/*==============================================================*/
/* Index: CBDD_CBAB_XFK                                         */
/*==============================================================*/




create nonclustered index CBDD_CBAB_XFK on CS_BILL_DISB_DELTA (BILL_ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBDD_CBDB2_XFK                                        */
/*==============================================================*/




create nonclustered index CBDD_CBDB2_XFK on CS_BILL_DISB_DELTA (DISTR_KEY ASC)
go

/*==============================================================*/
/* Index: CBDD_CBPIB_XFK                                        */
/*==============================================================*/




create nonclustered index CBDD_CBPIB_XFK on CS_BILL_DISB_DELTA (PMT_INSTRUMENT_KEY ASC)
go

/*==============================================================*/
/* Index: CBDD_CBPB_XFK                                         */
/*==============================================================*/




create nonclustered index CBDD_CBPB_XFK on CS_BILL_DISB_DELTA (PRDCR_KEY ASC)
go

/*==============================================================*/
/* Index: CBDD_CBCB_XFK                                         */
/*==============================================================*/




create nonclustered index CBDD_CBCB_XFK on CS_BILL_DISB_DELTA (COLTRL_KEY ASC)
go

/*==============================================================*/
/* Index: CBDD_CBSPB_XFK                                        */
/*==============================================================*/




create nonclustered index CBDD_CBSPB_XFK on CS_BILL_DISB_DELTA (SUSP_PMT_KEY ASC)
go

/*==============================================================*/
/* Index: CBDD_CBUFB_XFK                                        */
/*==============================================================*/




create nonclustered index CBDD_CBUFB_XFK on CS_BILL_DISB_DELTA (UNAPPLIED_FUND_KEY ASC)
go

/*==============================================================*/
/* Index: CBDD_CBUB_XFK                                         */
/*==============================================================*/




create nonclustered index CBDD_CBUB_XFK on CS_BILL_DISB_DELTA (REQUESTING_USER_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_DISTR_BASE                                    */
/*==============================================================*/
create table CS_BILL_DISTR_BASE (
   DISTR_BID            int                  identity,
   DISTR_KEY            varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_DISTR_BASE
   add constraint CBDB_PK primary key nonclustered (DISTR_KEY)
go

alter table CS_BILL_DISTR_BASE
   add constraint CBDB_AK1 unique (DISTR_BID)
go

/*==============================================================*/
/* Table: CS_BILL_DISTR_DELTA                                   */
/*==============================================================*/
create table CS_BILL_DISTR_DELTA (
   DISTR_DID            int                  identity,
   DISTR_KEY            varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   APPLIED_DTS          DATETIME2            null,
   DISTRIBUTED_DTS      DATETIME2            null,
   REVERSAL_DTS         DATETIME2            null,
   DISTR_TYPE_CD        varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   WO_AMT               numeric(18,2)        not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_DISTR_DELTA
   add constraint CBDD1_PK primary key nonclustered (DISTR_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_DISTR_DELTA
   add constraint CBDD1_AK1 unique (DISTR_DID)
go

/*==============================================================*/
/* Table: CS_BILL_DISTR_ITEM_BASE                               */
/*==============================================================*/
create table CS_BILL_DISTR_ITEM_BASE (
   DISTR_ITEM_BID       int                  identity,
   DISTR_ITEM_KEY       varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_DISTR_ITEM_BASE
   add constraint CBDIB_PK primary key nonclustered (DISTR_ITEM_KEY)
go

alter table CS_BILL_DISTR_ITEM_BASE
   add constraint CBDIB_AK1 unique (DISTR_ITEM_BID)
go

/*==============================================================*/
/* Table: CS_BILL_DISTR_ITEM_DELTA                              */
/*==============================================================*/
create table CS_BILL_DISTR_ITEM_DELTA (
   DISTR_ITEM_DID       int                  identity,
   DISTR_ITEM_KEY       varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_INV_ITEM_KEY    varchar(100)         not null,
   PRDCR_CD_KEY         varchar(100)         not null,
   ACTIVE_DISTR_KEY     varchar(100)         not null,
   REVERSED_DISTR_KEY   varchar(100)         not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   EXECUTED_DTS         DATETIME2            null,
   APPLIED_DTS          DATETIME2            null,
   REVERSED_DTS         DATETIME2            null,
   DISTR_ITEM_TYPE_CD   varchar(255)         not null,
   DISPOSITION_CD       varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   GROSS_APPLY_AMT      numeric(18,2)        not null,
   COMM_APPLY_AMT       numeric(18,2)        not null,
   PMT_COMMENTS         varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_DISTR_ITEM_DELTA
   add constraint CBDID_PK primary key nonclustered (DISTR_ITEM_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_DISTR_ITEM_DELTA
   add constraint CBDID_AK1 unique (DISTR_ITEM_DID)
go

/*==============================================================*/
/* Index: CBDID_CBDB_XFK                                        */
/*==============================================================*/




create nonclustered index CBDID_CBDB_XFK on CS_BILL_DISTR_ITEM_DELTA (ACTIVE_DISTR_KEY ASC)
go

/*==============================================================*/
/* Index: CBDID_CBIIB_XFK                                       */
/*==============================================================*/




create nonclustered index CBDID_CBIIB_XFK on CS_BILL_DISTR_ITEM_DELTA (BILL_INV_ITEM_KEY ASC)
go

/*==============================================================*/
/* Index: CBDID_CBPCB_XFK                                       */
/*==============================================================*/




create nonclustered index CBDID_CBPCB_XFK on CS_BILL_DISTR_ITEM_DELTA (PRDCR_CD_KEY ASC)
go

/*==============================================================*/
/* Index: CBDID_CBPPB_XFK                                       */
/*==============================================================*/




create nonclustered index CBDID_CBPPB_XFK on CS_BILL_DISTR_ITEM_DELTA (BILL_POL_PRD_KEY ASC)
go

/*==============================================================*/
/* Index: CBDID_CBDB1_XFK                                       */
/*==============================================================*/




create nonclustered index CBDID_CBDB1_XFK on CS_BILL_DISTR_ITEM_DELTA (REVERSED_DISTR_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_FUNDS_TRNFR_BASE                              */
/*==============================================================*/
create table CS_BILL_FUNDS_TRNFR_BASE (
   FUNDS_TFR_BID        int                  identity,
   FUNDS_TFR_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_FUNDS_TRNFR_BASE
   add constraint CBFTB_PK primary key nonclustered (FUNDS_TFR_KEY)
go

alter table CS_BILL_FUNDS_TRNFR_BASE
   add constraint CBFTB_AK1 unique (FUNDS_TFR_BID)
go

/*==============================================================*/
/* Table: CS_BILL_FUNDS_TRNFR_DELTA                             */
/*==============================================================*/
create table CS_BILL_FUNDS_TRNFR_DELTA (
   FUNDS_TFR_DID        int                  identity,
   FUNDS_TFR_KEY        varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   SRCE_PRDCR_KEY       varchar(100)         not null,
   TARGET_PRDCR_KEY     varchar(100)         not null,
   SRCE_UNAPPLIED_FUND_KEY varchar(100)         not null,
   TARGET_UNAPPLIED_FUND_KEY varchar(100)         not null,
   REQUESTING_USER_KEY  varchar(100)         not null,
   TFR_DTS              DATETIME2            null,
   APPRVL_DTS           DATETIME2            null,
   APPRVL_STATUS_CD     varchar(255)         not null,
   REASON_CD            varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TFR_AMT              numeric(18,2)        not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_FUNDS_TRNFR_DELTA
   add constraint CBFTD_PK primary key nonclustered (FUNDS_TFR_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_FUNDS_TRNFR_DELTA
   add constraint CBFTD_AK1 unique (FUNDS_TFR_DID)
go

/*==============================================================*/
/* Index: CBFTD_CBPB_XFK                                        */
/*==============================================================*/




create nonclustered index CBFTD_CBPB_XFK on CS_BILL_FUNDS_TRNFR_DELTA (SRCE_PRDCR_KEY ASC)
go

/*==============================================================*/
/* Index: CBFTD_CBPB1_XFK                                       */
/*==============================================================*/




create nonclustered index CBFTD_CBPB1_XFK on CS_BILL_FUNDS_TRNFR_DELTA (TARGET_PRDCR_KEY ASC)
go

/*==============================================================*/
/* Index: CBFTD_CBUFB_XFK                                       */
/*==============================================================*/




create nonclustered index CBFTD_CBUFB_XFK on CS_BILL_FUNDS_TRNFR_DELTA (SRCE_UNAPPLIED_FUND_KEY ASC)
go

/*==============================================================*/
/* Index: CBFTD_CBUFB1_XFK                                      */
/*==============================================================*/




create nonclustered index CBFTD_CBUFB1_XFK on CS_BILL_FUNDS_TRNFR_DELTA (TARGET_UNAPPLIED_FUND_KEY ASC)
go

/*==============================================================*/
/* Index: CBFTD_CBUB_XFK                                        */
/*==============================================================*/




create nonclustered index CBFTD_CBUB_XFK on CS_BILL_FUNDS_TRNFR_DELTA (REQUESTING_USER_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_GENERAL_TRAN                                  */
/*==============================================================*/
create table CS_BILL_GENERAL_TRAN (
   GENERAL_JRNL_TID     int                  identity,
   GENERAL_JRNL_KEY     varchar(100)         not null,
   CR_ACCT_KEY          varchar(100)         not null,
   CR_KEY               varchar(100)         not null,
   SUSP_ACCT_KEY        varchar(100)         not null,
   SUSP_PRDCR_KEY       varchar(100)         not null,
   COLTRL_KEY           varchar(100)         not null,
   NON_RCVBL_DISTR_ITEM_KEY varchar(100)         not null,
   SRCE_UNAPPLIED_TACCT_KEY varchar(100)         not null,
   TARGET_UNAPPLIED_TACCT_KEY varchar(100)         not null,
   SRCE_PRDCR_KEY       varchar(100)         not null,
   TARGET_PRDCR_KEY     varchar(100)         not null,
   SRCE_ACCT_KEY        varchar(100)         not null,
   TARGET_ACCT_KEY      varchar(100)         not null,
   FUNDS_TFR_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   TRANS_NO             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TFR_REASON_CD        varchar(255)         not null,
   TFR_REVERSAL_REASON_CD varchar(255)         not null,
   WO_CHANNEL_CD        varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_GENERAL_TRAN
   add constraint CBGT_PK primary key nonclustered (GENERAL_JRNL_TID)
go

alter table CS_BILL_GENERAL_TRAN
   add constraint CBGT_AK1 unique (GENERAL_JRNL_KEY)
go

/*==============================================================*/
/* Index: CBGT_CBCB_XFK                                         */
/*==============================================================*/




create nonclustered index CBGT_CBCB_XFK on CS_BILL_GENERAL_TRAN (COLTRL_KEY ASC)
go

/*==============================================================*/
/* Index: CBGT_CBAB_XFK                                         */
/*==============================================================*/




create nonclustered index CBGT_CBAB_XFK on CS_BILL_GENERAL_TRAN (SRCE_ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBGT_CBPB_XFK                                         */
/*==============================================================*/




create nonclustered index CBGT_CBPB_XFK on CS_BILL_GENERAL_TRAN (SUSP_PRDCR_KEY ASC)
go

/*==============================================================*/
/* Index: CBGT_CBAB1_XFK                                        */
/*==============================================================*/




create nonclustered index CBGT_CBAB1_XFK on CS_BILL_GENERAL_TRAN (TARGET_ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBGT_CBPB1_XFK                                        */
/*==============================================================*/




create nonclustered index CBGT_CBPB1_XFK on CS_BILL_GENERAL_TRAN (SRCE_PRDCR_KEY ASC)
go

/*==============================================================*/
/* Index: CBGT_CBPB2_XFK                                        */
/*==============================================================*/




create nonclustered index CBGT_CBPB2_XFK on CS_BILL_GENERAL_TRAN (TARGET_PRDCR_KEY ASC)
go

/*==============================================================*/
/* Index: CBGT_CBFTB_XFK                                        */
/*==============================================================*/




create nonclustered index CBGT_CBFTB_XFK on CS_BILL_GENERAL_TRAN (FUNDS_TFR_KEY ASC)
go

/*==============================================================*/
/* Index: CBGT_CBAB2_XFK                                        */
/*==============================================================*/




create nonclustered index CBGT_CBAB2_XFK on CS_BILL_GENERAL_TRAN (CR_ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBGT_CBCB1_XFK                                        */
/*==============================================================*/




create nonclustered index CBGT_CBCB1_XFK on CS_BILL_GENERAL_TRAN (CR_KEY ASC)
go

/*==============================================================*/
/* Index: CBGT_CBNDIB_XFK                                       */
/*==============================================================*/




create nonclustered index CBGT_CBNDIB_XFK on CS_BILL_GENERAL_TRAN (NON_RCVBL_DISTR_ITEM_KEY ASC)
go

/*==============================================================*/
/* Index: CBGT_CBTB_XFK                                         */
/*==============================================================*/




create nonclustered index CBGT_CBTB_XFK on CS_BILL_GENERAL_TRAN (SRCE_UNAPPLIED_TACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBGT_CBTB1_XFK                                        */
/*==============================================================*/




create nonclustered index CBGT_CBTB1_XFK on CS_BILL_GENERAL_TRAN (TARGET_UNAPPLIED_TACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBGT_CBAB3_XFK                                        */
/*==============================================================*/




create nonclustered index CBGT_CBAB3_XFK on CS_BILL_GENERAL_TRAN (SUSP_ACCT_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_INSTRUCTION_BASE                              */
/*==============================================================*/
create table CS_BILL_INSTRUCTION_BASE (
   BILL_INSTR_BID       int                  identity,
   BILL_INSTR_KEY       varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_INSTRUCTION_BASE
   add constraint CBIB1_PK primary key nonclustered (BILL_INSTR_KEY)
go

alter table CS_BILL_INSTRUCTION_BASE
   add constraint CBIB1_AK1 unique (BILL_INSTR_BID)
go

/*==============================================================*/
/* Table: CS_BILL_INSTRUCTION_DELTA                             */
/*==============================================================*/
create table CS_BILL_INSTRUCTION_DELTA (
   BILL_INSTR_DID       int                  identity,
   BILL_INSTR_KEY       varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   ISSUANCE_BILL_ACCT_KEY varchar(100)         not null,
   NEW_RENEW_BILL_ACCT_KEY varchar(100)         not null,
   RENEW_BILL_ACCT_KEY  varchar(100)         not null,
   REWRITE_BILL_ACCT_KEY varchar(100)         not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   NEW_BILL_POL_PRD_KEY varchar(100)         not null,
   ASSOC_BILL_POL_PRD_KEY varchar(100)         not null,
   PRIOR_BILL_POL_PRD_KEY varchar(100)         not null,
   COLTRL_REQ_KEY       varchar(100)         not null,
   BILL_INSTR_TYPE_CD   varchar(255)         not null,
   BILL_INSTR_DTS       DATETIME2            null,
   MOD_DTS              DATETIME2            null,
   SPCL_HANDLING_CD     varchar(255)         not null,
   OFFER_NO             varchar(255)         not null,
   PMT_DUE_DT           DATE                 null,
   PRD_START_DT         DATE                 null,
   PRD_END_DT           DATE                 null,
   PMT_RCVD_FL          char(1)              not null,
   CANCEL_TYPE_CD       varchar(255)         not null,
   CANCEL_REASON_TEXT   varchar(255)         not null,
   HOLD_UB_PREM_CHRG_FL char(1)              not null,
   BILL_INSTR_TEXT      varchar(255)         not null,
   FINAL_AUDIT_FL       char(1)              not null,
   TOT_PREM_FL          char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_INSTRUCTION_DELTA
   add constraint CBID_PK primary key nonclustered (BILL_INSTR_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_INSTRUCTION_DELTA
   add constraint CBID_AK1 unique (BILL_INSTR_DID)
go

/*==============================================================*/
/* Index: CBID_CBAB_XFK                                         */
/*==============================================================*/




create nonclustered index CBID_CBAB_XFK on CS_BILL_INSTRUCTION_DELTA (BILL_ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBID_CBPPB_XFK                                        */
/*==============================================================*/




create nonclustered index CBID_CBPPB_XFK on CS_BILL_INSTRUCTION_DELTA (BILL_POL_PRD_KEY ASC)
go

/*==============================================================*/
/* Index: CBID_CBCRB_XFK                                        */
/*==============================================================*/




create nonclustered index CBID_CBCRB_XFK on CS_BILL_INSTRUCTION_DELTA (COLTRL_REQ_KEY ASC)
go

/*==============================================================*/
/* Index: CBID_CBPPB1_XFK                                       */
/*==============================================================*/




create nonclustered index CBID_CBPPB1_XFK on CS_BILL_INSTRUCTION_DELTA (ASSOC_BILL_POL_PRD_KEY ASC)
go

/*==============================================================*/
/* Index: CBID_CBAB1_XFK                                        */
/*==============================================================*/




create nonclustered index CBID_CBAB1_XFK on CS_BILL_INSTRUCTION_DELTA (ISSUANCE_BILL_ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBID_CBPPB2_XFK                                       */
/*==============================================================*/




create nonclustered index CBID_CBPPB2_XFK on CS_BILL_INSTRUCTION_DELTA (PRIOR_BILL_POL_PRD_KEY ASC)
go

/*==============================================================*/
/* Index: CBID_CBPPB3_XFK                                       */
/*==============================================================*/




create nonclustered index CBID_CBPPB3_XFK on CS_BILL_INSTRUCTION_DELTA (NEW_BILL_POL_PRD_KEY ASC)
go

/*==============================================================*/
/* Index: CBID_CBAB2_XFK                                        */
/*==============================================================*/




create nonclustered index CBID_CBAB2_XFK on CS_BILL_INSTRUCTION_DELTA (NEW_RENEW_BILL_ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBID_CBAB3_XFK                                        */
/*==============================================================*/




create nonclustered index CBID_CBAB3_XFK on CS_BILL_INSTRUCTION_DELTA (REWRITE_BILL_ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBID_CBAB4_XFK                                        */
/*==============================================================*/




create nonclustered index CBID_CBAB4_XFK on CS_BILL_INSTRUCTION_DELTA (RENEW_BILL_ACCT_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_INV_BASE                                      */
/*==============================================================*/
create table CS_BILL_INV_BASE (
   BILL_INV_BID         int                  identity,
   BILL_INV_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_INV_BASE
   add constraint CBIB_PK primary key nonclustered (BILL_INV_KEY)
go

alter table CS_BILL_INV_BASE
   add constraint CBIB_AK1 unique (BILL_INV_BID)
go

/*==============================================================*/
/* Table: CS_BILL_INV_DELTA                                     */
/*==============================================================*/
create table CS_BILL_INV_DELTA (
   BILL_INV_DID         int                  identity,
   BILL_INV_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   INV_STREAM_KEY       varchar(100)         not null,
   EVENT_DTS            DATETIME2            not null,
   PMT_DUE_DTS          DATETIME2            not null,
   INV_NO               varchar(255)         not null,
   RESEND_NO            varchar(255)         not null,
   INV_TYPE_CD          varchar(255)         not null,
   INV_STATUS_CD        varchar(255)         not null,
   INV_TEXT             varchar(1333)        not null,
   CURR_CD              varchar(255)         not null,
   INV_AMT              numeric(18,2)        not null,
   AD_HOC_FL            char(1)              not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_INV_DELTA
   add constraint CBID1_PK primary key nonclustered (BILL_INV_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_INV_DELTA
   add constraint CBID1_AK1 unique (BILL_INV_DID)
go

/*==============================================================*/
/* Index: CBID_CBAB5_XFK                                        */
/*==============================================================*/




create nonclustered index CBID_CBAB5_XFK on CS_BILL_INV_DELTA (BILL_ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBID_CBISB_XFK                                        */
/*==============================================================*/




create nonclustered index CBID_CBISB_XFK on CS_BILL_INV_DELTA (INV_STREAM_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_INV_ITEM_BASE                                 */
/*==============================================================*/
create table CS_BILL_INV_ITEM_BASE (
   BILL_INV_ITEM_BID    int                  identity,
   BILL_INV_ITEM_KEY    varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_INV_ITEM_BASE
   add constraint CBIIB_PK primary key nonclustered (BILL_INV_ITEM_KEY)
go

alter table CS_BILL_INV_ITEM_BASE
   add constraint CBIIB_AK1 unique (BILL_INV_ITEM_BID)
go

/*==============================================================*/
/* Table: CS_BILL_INV_ITEM_DELTA                                */
/*==============================================================*/
create table CS_BILL_INV_ITEM_DELTA (
   BILL_INV_ITEM_DID    int                  identity,
   BILL_INV_ITEM_KEY    varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_INV_KEY         varchar(100)         not null,
   CHRG_KEY             varchar(100)         not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   EVENT_DTS            DATETIME2            not null,
   PMT_EXCEPT_DTS       DATETIME2            null,
   PROMISE_EXCEPT_DTS   DATETIME2            null,
   INSTALLMENT_NO       varchar(255)         not null,
   LINE_ITEM_NO         varchar(255)         not null,
   INV_ITEM_TYPE_CD     varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   INV_ITEM_AMT         numeric(18,2)        not null,
   COMMENTS             varchar(1333)        not null,
   INV_ITEM_TEXT        varchar(1333)        not null,
   CSTM_PMT_GROUP_TEXT  varchar(1333)        null,
   EXCEPT_CMT           varchar(1333)        not null,
   GROSS_SETTLED_FL     char(1)              not null,
   RETIRED_FL           char(1)              not null,
   REVERSED_FL          char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_INV_ITEM_DELTA
   add constraint CBIID_PK primary key nonclustered (BILL_INV_ITEM_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_INV_ITEM_DELTA
   add constraint CBIID_AK1 unique (BILL_INV_ITEM_DID)
go

/*==============================================================*/
/* Index: CBIID_CBIB_XFK                                        */
/*==============================================================*/




create nonclustered index CBIID_CBIB_XFK on CS_BILL_INV_ITEM_DELTA (BILL_INV_KEY ASC)
go

/*==============================================================*/
/* Index: CBIID_CBCB_XFK                                        */
/*==============================================================*/




create nonclustered index CBIID_CBCB_XFK on CS_BILL_INV_ITEM_DELTA (CHRG_KEY ASC)
go

/*==============================================================*/
/* Index: CBIID_CBPPB_XFK                                       */
/*==============================================================*/




create nonclustered index CBIID_CBPPB_XFK on CS_BILL_INV_ITEM_DELTA (BILL_POL_PRD_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_INV_MONTH_AGG                                 */
/*==============================================================*/
create table CS_BILL_INV_MONTH_AGG (
   INV_MTH_AGG_ID       int                  identity,
   TRANS_MTH_ID         int                  not null,
   PRDCR_KEY            varchar(100)         not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   BILL_INV_KEY         varchar(100)         not null,
   CHRG_KEY             varchar(100)         not null,
   CHRG_PAT_KEY         varchar(100)         not null,
   WO_KEY               varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   INV_ITEM_TYPE_CD     varchar(255)         not null,
   CHRG_CATG_CD         varchar(255)         not null,
   CHRG_EFF_DT_ID       int                  null,
   WRITTEN_DT_ID        int                  null,
   INSTALLMENT_FL       char(1)              not null,
   PREM_FL              char(1)              not null,
   CURR_CD              varchar(255)         not null,
   CHRG_AMT             numeric(18,2)        not null,
   BILLED_AMT           numeric(18,2)        not null,
   DUE_AMT              numeric(18,2)        not null,
   PD_AMT               numeric(18,2)        not null,
   WO_AMT               numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_INV_MONTH_AGG
   add constraint CBIMA_PK primary key nonclustered (INV_MTH_AGG_ID)
go

/*==============================================================*/
/* Index: CBIMA_CBCB_XFK                                        */
/*==============================================================*/




create nonclustered index CBIMA_CBCB_XFK on CS_BILL_INV_MONTH_AGG (CHRG_KEY ASC)
go

/*==============================================================*/
/* Index: CBIMA_CBIB_XFK                                        */
/*==============================================================*/




create nonclustered index CBIMA_CBIB_XFK on CS_BILL_INV_MONTH_AGG (BILL_INV_KEY ASC)
go

/*==============================================================*/
/* Index: CBIMA_CBWB_XFK                                        */
/*==============================================================*/




create nonclustered index CBIMA_CBWB_XFK on CS_BILL_INV_MONTH_AGG (WO_KEY ASC)
go

/*==============================================================*/
/* Index: CBIMA_CBAB_XFK                                        */
/*==============================================================*/




create nonclustered index CBIMA_CBAB_XFK on CS_BILL_INV_MONTH_AGG (BILL_ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBIMA_CBPPB_XFK                                       */
/*==============================================================*/




create nonclustered index CBIMA_CBPPB_XFK on CS_BILL_INV_MONTH_AGG (BILL_POL_PRD_KEY ASC)
go

/*==============================================================*/
/* Index: CBIMA_CBCPB_XFK                                       */
/*==============================================================*/




create nonclustered index CBIMA_CBCPB_XFK on CS_BILL_INV_MONTH_AGG (CHRG_PAT_KEY ASC)
go

/*==============================================================*/
/* Index: CBIMA_CBPB_XFK                                        */
/*==============================================================*/




create nonclustered index CBIMA_CBPB_XFK on CS_BILL_INV_MONTH_AGG (PRDCR_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_MONEY_RCVD_BASE                               */
/*==============================================================*/
create table CS_BILL_MONEY_RCVD_BASE (
   MON_RCVD_BID         int                  identity,
   MON_RCVD_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_MONEY_RCVD_BASE
   add constraint CBMRB_PK primary key nonclustered (MON_RCVD_KEY)
go

alter table CS_BILL_MONEY_RCVD_BASE
   add constraint CBMRB_AK1 unique (MON_RCVD_BID)
go

/*==============================================================*/
/* Table: CS_BILL_MONEY_RCVD_DELTA                              */
/*==============================================================*/
create table CS_BILL_MONEY_RCVD_DELTA (
   MON_RCVD_DID         int                  identity,
   MON_RCVD_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   PAYING_PRDCR_KEY     varchar(100)         not null,
   PROMISING_PRDCR_KEY  varchar(100)         not null,
   BILL_INV_KEY         varchar(100)         not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   PMT_INSTRUMENT_KEY   varchar(100)         not null,
   RPT_GROUP_KEY        varchar(100)         not null,
   UNAPPLIED_FUND_KEY   varchar(100)         not null,
   DISTR_KEY            varchar(100)         not null,
   RCVD_AMT             numeric(18,2)        not null,
   APPLIED_DTS          DATETIME2            null,
   RCVD_DTS             DATETIME2            null,
   REVERSAL_DTS         DATETIME2            null,
   MON_RCVD_TEXT        varchar(1333)        not null,
   NAME                 varchar(255)         not null,
   REF_NO               varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   REVERSAL_REASON_CD   varchar(255)         not null,
   MON_RCVD_TYPE_CD     varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_MONEY_RCVD_DELTA
   add constraint CBMRD_PK primary key nonclustered (MON_RCVD_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_MONEY_RCVD_DELTA
   add constraint CBMRD_AK1 unique (MON_RCVD_DID)
go

/*==============================================================*/
/* Index: CBMRD_CBPB_XFK                                        */
/*==============================================================*/




create nonclustered index CBMRD_CBPB_XFK on CS_BILL_MONEY_RCVD_DELTA (PAYING_PRDCR_KEY ASC)
go

/*==============================================================*/
/* Index: CBMRD_CBAB_XFK                                        */
/*==============================================================*/




create nonclustered index CBMRD_CBAB_XFK on CS_BILL_MONEY_RCVD_DELTA (BILL_ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBMRD_CBIB_XFK                                        */
/*==============================================================*/




create nonclustered index CBMRD_CBIB_XFK on CS_BILL_MONEY_RCVD_DELTA (BILL_INV_KEY ASC)
go

/*==============================================================*/
/* Index: CBMRD_CBPPB_XFK                                       */
/*==============================================================*/




create nonclustered index CBMRD_CBPPB_XFK on CS_BILL_MONEY_RCVD_DELTA (BILL_POL_PRD_KEY ASC)
go

/*==============================================================*/
/* Index: CBMRD_CBPIB_XFK                                       */
/*==============================================================*/




create nonclustered index CBMRD_CBPIB_XFK on CS_BILL_MONEY_RCVD_DELTA (PMT_INSTRUMENT_KEY ASC)
go

/*==============================================================*/
/* Index: CBMRD_CRGB_XFK                                        */
/*==============================================================*/




create nonclustered index CBMRD_CRGB_XFK on CS_BILL_MONEY_RCVD_DELTA (RPT_GROUP_KEY ASC)
go

/*==============================================================*/
/* Index: CBMRD_CBUFB_XFK                                       */
/*==============================================================*/




create nonclustered index CBMRD_CBUFB_XFK on CS_BILL_MONEY_RCVD_DELTA (UNAPPLIED_FUND_KEY ASC)
go

/*==============================================================*/
/* Index: CBMRD_CBPB1_XFK                                       */
/*==============================================================*/




create nonclustered index CBMRD_CBPB1_XFK on CS_BILL_MONEY_RCVD_DELTA (PROMISING_PRDCR_KEY ASC)
go

/*==============================================================*/
/* Index: CBMRD_CBDB_XFK                                        */
/*==============================================================*/




create nonclustered index CBMRD_CBDB_XFK on CS_BILL_MONEY_RCVD_DELTA (DISTR_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_NEG_WRITEOFF_BASE                             */
/*==============================================================*/
create table CS_BILL_NEG_WRITEOFF_BASE (
   NEG_WO_BID           int                  identity,
   NEG_WO_KEY           varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_NEG_WRITEOFF_BASE
   add constraint CBNWB_PK primary key nonclustered (NEG_WO_KEY)
go

alter table CS_BILL_NEG_WRITEOFF_BASE
   add constraint CBNWB_AK1 unique (NEG_WO_BID)
go

/*==============================================================*/
/* Table: CS_BILL_NEG_WRITEOFF_DELTA                            */
/*==============================================================*/
create table CS_BILL_NEG_WRITEOFF_DELTA (
   NEG_WO_DID           int                  identity,
   NEG_WO_KEY           varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   UNAPPLIED_FUND_KEY   varchar(100)         not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   PRDCR_KEY            varchar(100)         not null,
   REQUESTING_USER_KEY  varchar(100)         not null,
   EXECUTION_DTS        DATETIME2            null,
   APPRVL_DTS           DATETIME2            null,
   APPRVL_STATUS_CD     varchar(255)         not null,
   WO_CHANNEL_CD        varchar(255)         not null,
   NEG_WO_TYPE_CD       varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   WO_AMT               numeric(18,2)        not null,
   REVERSED_FL          char(1)              not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_NEG_WRITEOFF_DELTA
   add constraint CBNWD_PK primary key nonclustered (NEG_WO_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_NEG_WRITEOFF_DELTA
   add constraint CBNWD_AK1 unique (NEG_WO_DID)
go

/*==============================================================*/
/* Index: CBNWD_CBUFB_XFK                                       */
/*==============================================================*/




create nonclustered index CBNWD_CBUFB_XFK on CS_BILL_NEG_WRITEOFF_DELTA (UNAPPLIED_FUND_KEY ASC)
go

/*==============================================================*/
/* Index: CBNWD_CBAB_XFK                                        */
/*==============================================================*/




create nonclustered index CBNWD_CBAB_XFK on CS_BILL_NEG_WRITEOFF_DELTA (BILL_ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBNWD_CBPB_XFK                                        */
/*==============================================================*/




create nonclustered index CBNWD_CBPB_XFK on CS_BILL_NEG_WRITEOFF_DELTA (PRDCR_KEY ASC)
go

/*==============================================================*/
/* Index: CBNWD_CBUB_XFK                                        */
/*==============================================================*/




create nonclustered index CBNWD_CBUB_XFK on CS_BILL_NEG_WRITEOFF_DELTA (REQUESTING_USER_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_NR_DISTR_ITEM_BASE                            */
/*==============================================================*/
create table CS_BILL_NR_DISTR_ITEM_BASE (
   NON_RCVBL_DISTR_ITEM_BID int                  identity,
   NON_RCVBL_DISTR_ITEM_KEY varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_NR_DISTR_ITEM_BASE
   add constraint CBNDIB_PK primary key nonclustered (NON_RCVBL_DISTR_ITEM_KEY)
go

alter table CS_BILL_NR_DISTR_ITEM_BASE
   add constraint CBNDIB_AK1 unique (NON_RCVBL_DISTR_ITEM_BID)
go

/*==============================================================*/
/* Table: CS_BILL_NR_DISTR_ITEM_DELTA                           */
/*==============================================================*/
create table CS_BILL_NR_DISTR_ITEM_DELTA (
   NON_RCVBL_DISTR_ITEM_DID int                  identity,
   NON_RCVBL_DISTR_ITEM_KEY varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ACTIVE_DISTR_KEY     varchar(100)         not null,
   REVERSED_DISTR_KEY   varchar(100)         not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   CURR_CD              varchar(255)         not null,
   COMM_APPLY_AMT       numeric(18,2)        not null,
   GROSS_APPLY_AMT      numeric(18,2)        not null,
   NET_APPLY_AMT        numeric(18,2)        not null,
   EXECUTED_DTS         DATETIME2            null,
   REVERSED_DTS         DATETIME2            null,
   RELEASED_DTS         DATETIME2            null,
   PMT_CMT_TEXT         varchar(255)         not null,
   POL_NO               varchar(255)         not null,
   ITEM_TYPE_CD         varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_NR_DISTR_ITEM_DELTA
   add constraint CBNDID_PK primary key nonclustered (NON_RCVBL_DISTR_ITEM_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_NR_DISTR_ITEM_DELTA
   add constraint CBNDID_AK1 unique (NON_RCVBL_DISTR_ITEM_DID)
go

/*==============================================================*/
/* Index: CBNDID_CBDB_XFK                                       */
/*==============================================================*/




create nonclustered index CBNDID_CBDB_XFK on CS_BILL_NR_DISTR_ITEM_DELTA (ACTIVE_DISTR_KEY ASC)
go

/*==============================================================*/
/* Index: CBNDID_CBDB1_XFK                                      */
/*==============================================================*/




create nonclustered index CBNDID_CBDB1_XFK on CS_BILL_NR_DISTR_ITEM_DELTA (REVERSED_DISTR_KEY ASC)
go

/*==============================================================*/
/* Index: CBNDID_CBPPB_XFK                                      */
/*==============================================================*/




create nonclustered index CBNDID_CBPPB_XFK on CS_BILL_NR_DISTR_ITEM_DELTA (BILL_POL_PRD_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_OUTGOING_PMT_BASE                             */
/*==============================================================*/
create table CS_BILL_OUTGOING_PMT_BASE (
   OUTGOING_PMT_BID     int                  identity,
   OUTGOING_PMT_KEY     varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_OUTGOING_PMT_BASE
   add constraint CBOPB_PK primary key nonclustered (OUTGOING_PMT_KEY)
go

alter table CS_BILL_OUTGOING_PMT_BASE
   add constraint CBOPB_AK1 unique (OUTGOING_PMT_BID)
go

/*==============================================================*/
/* Table: CS_BILL_OUTGOING_PMT_DELTA                            */
/*==============================================================*/
create table CS_BILL_OUTGOING_PMT_DELTA (
   OUTGOING_PMT_DID     int                  identity,
   OUTGOING_PMT_KEY     varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   DISB_KEY             varchar(100)         not null,
   PRDCR_PMT_KEY        varchar(100)         not null,
   PMT_INSTRUMENT_KEY   varchar(100)         not null,
   ISSUE_DTS            DATETIME2            null,
   REJECTED_DTS         DATETIME2            null,
   MAIL_TO_NAME         varchar(255)         not null,
   MAIL_TO_ADDR         varchar(255)         not null,
   MEMO                 varchar(255)         not null,
   PD_DTS               DATETIME2            null,
   PAY_TO_NAME          varchar(255)         not null,
   OUTGOING_PMT_TYPE_CD varchar(255)         not null,
   OUTGOING_PMT_STATUS_CD varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   PMT_AMT              numeric(18,2)        not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_OUTGOING_PMT_DELTA
   add constraint CBOPD_PK primary key nonclustered (OUTGOING_PMT_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_OUTGOING_PMT_DELTA
   add constraint CBOPD_AK1 unique (OUTGOING_PMT_DID)
go

/*==============================================================*/
/* Index: CBOPD_CBPPB_XFK                                       */
/*==============================================================*/




create nonclustered index CBOPD_CBPPB_XFK on CS_BILL_OUTGOING_PMT_DELTA (PRDCR_PMT_KEY ASC)
go

/*==============================================================*/
/* Index: CBOPD_CBDB_XFK                                        */
/*==============================================================*/




create nonclustered index CBOPD_CBDB_XFK on CS_BILL_OUTGOING_PMT_DELTA (DISB_KEY ASC)
go

/*==============================================================*/
/* Index: CBOPD_CBPIB_XFK                                       */
/*==============================================================*/




create nonclustered index CBOPD_CBPIB_XFK on CS_BILL_OUTGOING_PMT_DELTA (PMT_INSTRUMENT_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_PLAN_BASE                                     */
/*==============================================================*/
create table CS_BILL_PLAN_BASE (
   PLAN_BID             int                  identity,
   PLAN_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_PLAN_BASE
   add constraint CBPB1_PK primary key nonclustered (PLAN_KEY)
go

alter table CS_BILL_PLAN_BASE
   add constraint CBPB1_AK1 unique (PLAN_BID)
go

/*==============================================================*/
/* Table: CS_BILL_PLAN_DELTA                                    */
/*==============================================================*/
create table CS_BILL_PLAN_DELTA (
   PLAN_DID             int                  identity,
   PLAN_KEY             varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PLAN_TYPE_CD         varchar(255)         not null,
   PLAN_NAME            varchar(255)         not null,
   PLAN_EFF_DTS         DATETIME2            not null,
   PLAN_EXP_DTS         DATETIME2            null,
   PLAN_ORDER           int                  not null,
   PLAN_TEXT            varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_PLAN_DELTA
   add constraint CBPD1_PK primary key nonclustered (PLAN_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_PLAN_DELTA
   add constraint CBPD1_AK1 unique (PLAN_DID)
go

/*==============================================================*/
/* Table: CS_BILL_PMT_INSTR_BASE                                */
/*==============================================================*/
create table CS_BILL_PMT_INSTR_BASE (
   PMT_INSTRUMENT_BID   int                  identity,
   PMT_INSTRUMENT_KEY   varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_PMT_INSTR_BASE
   add constraint CBPIB_PK primary key nonclustered (PMT_INSTRUMENT_KEY)
go

alter table CS_BILL_PMT_INSTR_BASE
   add constraint CBPIB_AK1 unique (PMT_INSTRUMENT_BID)
go

/*==============================================================*/
/* Table: CS_BILL_PMT_INSTR_DELTA                               */
/*==============================================================*/
create table CS_BILL_PMT_INSTR_DELTA (
   PMT_INSTRUMENT_DID   int                  identity,
   PMT_INSTRUMENT_KEY   varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   PRDCR_KEY            varchar(100)         not null,
   TOKEN_TEXT           varchar(255)         not null,
   PMT_INSTRUMENT_TEXT  varchar(255)         not null,
   PMT_METHOD_CD        varchar(255)         not null,
   IMMUTABLE_FL         char(1)              not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_PMT_INSTR_DELTA
   add constraint CBPID_PK primary key nonclustered (PMT_INSTRUMENT_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_PMT_INSTR_DELTA
   add constraint CBPID_AK1 unique (PMT_INSTRUMENT_DID)
go

/*==============================================================*/
/* Index: CBPID_CBAB_XFK                                        */
/*==============================================================*/




create nonclustered index CBPID_CBAB_XFK on CS_BILL_PMT_INSTR_DELTA (BILL_ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBPID_CBPB_XFK                                        */
/*==============================================================*/




create nonclustered index CBPID_CBPB_XFK on CS_BILL_PMT_INSTR_DELTA (PRDCR_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_POL_BASE                                      */
/*==============================================================*/
create table CS_BILL_POL_BASE (
   BILL_POL_BID         int                  identity,
   BILL_POL_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_POL_BASE
   add constraint CBPB2_PK primary key nonclustered (BILL_POL_KEY)
go

alter table CS_BILL_POL_BASE
   add constraint CBPB2_AK1 unique (BILL_POL_BID)
go

/*==============================================================*/
/* Table: CS_BILL_POL_COMM_BASE                                 */
/*==============================================================*/
create table CS_BILL_POL_COMM_BASE (
   POL_COMM_BID         int                  identity,
   POL_COMM_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_POL_COMM_BASE
   add constraint CBPCB_PK primary key nonclustered (POL_COMM_KEY)
go

alter table CS_BILL_POL_COMM_BASE
   add constraint CBPCB_AK1 unique (POL_COMM_BID)
go

/*==============================================================*/
/* Table: CS_BILL_POL_COMM_DELTA                                */
/*==============================================================*/
create table CS_BILL_POL_COMM_DELTA (
   POL_COMM_DID         int                  identity,
   POL_COMM_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   PRIM_BILL_POL_PRD_KEY varchar(100)         not null,
   PRDCR_CD_KEY         varchar(100)         not null,
   COMM_SPLAN_KEY       varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   ROLE_CD              varchar(255)         not null,
   COMM_OVERRIDE_PCT    numeric(6,3)         not null,
   DEFAULT_FOR_POL_FL   char(1)              not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_POL_COMM_DELTA
   add constraint CBPCD1_PK primary key nonclustered (POL_COMM_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_POL_COMM_DELTA
   add constraint CBPCD1_AK1 unique (POL_COMM_DID)
go

/*==============================================================*/
/* Index: CBPCD_CBPPB_XFK                                       */
/*==============================================================*/




create nonclustered index CBPCD_CBPPB_XFK on CS_BILL_POL_COMM_DELTA (BILL_POL_PRD_KEY ASC)
go

/*==============================================================*/
/* Index: CBPCD_CBPPB1_XFK                                      */
/*==============================================================*/




create nonclustered index CBPCD_CBPPB1_XFK on CS_BILL_POL_COMM_DELTA (PRIM_BILL_POL_PRD_KEY ASC)
go

/*==============================================================*/
/* Index: CBPCD_CBPCB2_XFK                                      */
/*==============================================================*/




create nonclustered index CBPCD_CBPCB2_XFK on CS_BILL_POL_COMM_DELTA (PRDCR_CD_KEY ASC)
go

/*==============================================================*/
/* Index: CBPCD_CBTCB_XFK                                       */
/*==============================================================*/




create nonclustered index CBPCD_CBTCB_XFK on CS_BILL_POL_COMM_DELTA (TACCT_CONTAINER_KEY ASC)
go

/*==============================================================*/
/* Index: CBPCD_CBCSB_XFK                                       */
/*==============================================================*/




create nonclustered index CBPCD_CBCSB_XFK on CS_BILL_POL_COMM_DELTA (COMM_SPLAN_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_POL_DELTA                                     */
/*==============================================================*/
create table CS_BILL_POL_DELTA (
   BILL_POL_DID         int                  identity,
   BILL_POL_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   LOB_CD               varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_POL_DELTA
   add constraint CBPD2_PK primary key nonclustered (BILL_POL_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_POL_DELTA
   add constraint CBPD2_AK1 unique (BILL_POL_DID)
go

/*==============================================================*/
/* Index: CBPD_CBAB_XFK                                         */
/*==============================================================*/




create nonclustered index CBPD_CBAB_XFK on CS_BILL_POL_DELTA (BILL_ACCT_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_POL_PRD_BASE                                  */
/*==============================================================*/
create table CS_BILL_POL_PRD_BASE (
   BILL_POL_PRD_BID     int                  identity,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_POL_PRD_BASE
   add constraint CBPPB1_PK primary key nonclustered (BILL_POL_PRD_KEY)
go

alter table CS_BILL_POL_PRD_BASE
   add constraint CBPPB1_AK1 unique (BILL_POL_PRD_BID)
go

/*==============================================================*/
/* Table: CS_BILL_POL_PRD_DELTA                                 */
/*==============================================================*/
create table CS_BILL_POL_PRD_DELTA (
   BILL_POL_PRD_DID     int                  identity,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_POL_KEY         varchar(100)         not null,
   PMT_PLAN_KEY         varchar(100)         not null,
   DELNQNT_PLAN_KEY     varchar(100)         not null,
   RETURN_PREM_PLAN_KEY varchar(100)         not null,
   SECURITY_ZONE_KEY    varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   BILL_METHOD_CD       varchar(255)         not null,
   BOUND_DTS            DATETIME2            null,
   JURS_STATE_CD        varchar(255)         not null,
   UW_CO_CD             varchar(255)         not null,
   CANCEL_TYPE_CD       varchar(255)         not null,
   CANCEL_REASON_TEXT   varchar(255)         not null,
   CANCEL_STATUS_CD     varchar(255)         not null,
   CLOSE_DTS            DATETIME2            null,
   CLOSURE_STATUS_CD    varchar(255)         not null,
   CONFIRM_NOTIF_STATE_CD varchar(255)         not null,
   DBA_NAME             varchar(255)         not null,
   FULL_PAY_DISC_UNTIL_DT DATE                 null,
   OFFER_NO             varchar(255)         not null,
   POL_NO               varchar(255)         not null,
   POL_EFF_DT           DATE                 null,
   POL_EXP_DT           DATE                 null,
   PRIOR_POL_NO         varchar(255)         not null,
   TERM_NO              int                  not null,
   UNDERWRITER_NAME     varchar(255)         not null,
   EQUITY_BUFFER_DAYS   int                  not null,
   EQUITY_WARNINGS_ENABLED_FL char(1)              not null,
   WESTERN_METHOD_FL    char(1)              not null,
   CHRG_HELD_FL         char(1)              not null,
   HELD_FOR_INV_SENDING_FL char(1)              not null,
   UNDER_AUDIT_FL       char(1)              not null,
   TERM_CONFIRMED_FL    char(1)              not null,
   PMT_DISTR_ENABLED_FL char(1)              not null,
   HOLD_INV_WHEN_DELNQNT_FL char(1)              not null,
   FULL_PAY_DISC_EVALUATED_FL char(1)              not null,
   ELIG_FOR_FULL_PAY_DISC_FL char(1)              not null,
   ASSIGNED_RISK_FL     char(1)              not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_POL_PRD_DELTA
   add constraint CBPPD1_PK primary key nonclustered (BILL_POL_PRD_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_POL_PRD_DELTA
   add constraint CBPPD1_AK1 unique (BILL_POL_PRD_DID)
go

/*==============================================================*/
/* Index: CBPPD_CBPB1_XFK                                       */
/*==============================================================*/




create nonclustered index CBPPD_CBPB1_XFK on CS_BILL_POL_PRD_DELTA (PMT_PLAN_KEY ASC)
go

/*==============================================================*/
/* Index: CBPPD_CBPB2_XFK                                       */
/*==============================================================*/




create nonclustered index CBPPD_CBPB2_XFK on CS_BILL_POL_PRD_DELTA (DELNQNT_PLAN_KEY ASC)
go

/*==============================================================*/
/* Index: CBPPD_CBPB3_XFK                                       */
/*==============================================================*/




create nonclustered index CBPPD_CBPB3_XFK on CS_BILL_POL_PRD_DELTA (RETURN_PREM_PLAN_KEY ASC)
go

/*==============================================================*/
/* Index: CBPPD_CBSZB_XFK                                       */
/*==============================================================*/




create nonclustered index CBPPD_CBSZB_XFK on CS_BILL_POL_PRD_DELTA (SECURITY_ZONE_KEY ASC)
go

/*==============================================================*/
/* Index: CBPPD_CBTCB_XFK                                       */
/*==============================================================*/




create nonclustered index CBPPD_CBTCB_XFK on CS_BILL_POL_PRD_DELTA (TACCT_CONTAINER_KEY ASC)
go

/*==============================================================*/
/* Index: CBPPD_CBPB4_XFK                                       */
/*==============================================================*/




create nonclustered index CBPPD_CBPB4_XFK on CS_BILL_POL_PRD_DELTA (BILL_POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_PROD_BASE                                     */
/*==============================================================*/
create table CS_BILL_PROD_BASE (
   PRDCR_BID            int                  identity,
   PRDCR_KEY            varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_PROD_BASE
   add constraint CBPB_PK primary key nonclustered (PRDCR_KEY)
go

alter table CS_BILL_PROD_BASE
   add constraint CBPB_AK1 unique (PRDCR_BID)
go

/*==============================================================*/
/* Table: CS_BILL_PROD_CD_BASE                                  */
/*==============================================================*/
create table CS_BILL_PROD_CD_BASE (
   PRDCR_CD_BID         int                  identity,
   PRDCR_CD_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_PROD_CD_BASE
   add constraint CBPCB1_PK primary key nonclustered (PRDCR_CD_KEY)
go

alter table CS_BILL_PROD_CD_BASE
   add constraint CBPCB1_AK1 unique (PRDCR_CD_BID)
go

/*==============================================================*/
/* Table: CS_BILL_PROD_CD_DELTA                                 */
/*==============================================================*/
create table CS_BILL_PROD_CD_DELTA (
   PRDCR_CD_DID         int                  identity,
   PRDCR_CD_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PRDCR_KEY            varchar(100)         not null,
   COMM_PLAN_KEY        varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   PRDCR_CD             varchar(255)         not null,
   ACTIVE_FL            char(1)              not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_PROD_CD_DELTA
   add constraint CBPCD_PK primary key nonclustered (PRDCR_CD_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_PROD_CD_DELTA
   add constraint CBPCD_AK1 unique (PRDCR_CD_DID)
go

/*==============================================================*/
/* Index: CBPCD_CBPB_XFK                                        */
/*==============================================================*/




create nonclustered index CBPCD_CBPB_XFK on CS_BILL_PROD_CD_DELTA (PRDCR_KEY ASC)
go

/*==============================================================*/
/* Index: CBPCD_CBTCB1_XFK                                      */
/*==============================================================*/




create nonclustered index CBPCD_CBTCB1_XFK on CS_BILL_PROD_CD_DELTA (TACCT_CONTAINER_KEY ASC)
go

/*==============================================================*/
/* Index: CBPCD_CBPB1_XFK                                       */
/*==============================================================*/




create nonclustered index CBPCD_CBPB1_XFK on CS_BILL_PROD_CD_DELTA (COMM_PLAN_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_PROD_DELTA                                    */
/*==============================================================*/
create table CS_BILL_PROD_DELTA (
   PRDCR_DID            int                  identity,
   PRDCR_KEY            varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   RPT_GROUP_KEY        varchar(100)         not null,
   AGCY_BILL_PLAN_KEY   varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   SECURITY_ZONE_KEY    varchar(100)         not null,
   PRDCR_TIER_CD        varchar(255)         not null,
   PRDCR_NAME           varchar(255)         not null,
   COMBINED_STATEMENTS_FL char(1)              not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_PROD_DELTA
   add constraint CBPD_PK primary key nonclustered (PRDCR_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_PROD_DELTA
   add constraint CBPD_AK1 unique (PRDCR_DID)
go

/*==============================================================*/
/* Index: CBPD_CRGB_XFK                                         */
/*==============================================================*/




create nonclustered index CBPD_CRGB_XFK on CS_BILL_PROD_DELTA (RPT_GROUP_KEY ASC)
go

/*==============================================================*/
/* Index: CBPD_CBTCB_XFK                                        */
/*==============================================================*/




create nonclustered index CBPD_CBTCB_XFK on CS_BILL_PROD_DELTA (TACCT_CONTAINER_KEY ASC)
go

/*==============================================================*/
/* Index: CBPD_CBSZB_XFK                                        */
/*==============================================================*/




create nonclustered index CBPD_CBSZB_XFK on CS_BILL_PROD_DELTA (SECURITY_ZONE_KEY ASC)
go

/*==============================================================*/
/* Index: CBPD_CBPB1_XFK                                        */
/*==============================================================*/




create nonclustered index CBPD_CBPB1_XFK on CS_BILL_PROD_DELTA (AGCY_BILL_PLAN_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_PROD_PMT_BASE                                 */
/*==============================================================*/
create table CS_BILL_PROD_PMT_BASE (
   PRDCR_PMT_BID        int                  identity,
   PRDCR_PMT_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_PROD_PMT_BASE
   add constraint CBPPB_PK primary key nonclustered (PRDCR_PMT_KEY)
go

alter table CS_BILL_PROD_PMT_BASE
   add constraint CBPPB_AK1 unique (PRDCR_PMT_BID)
go

/*==============================================================*/
/* Table: CS_BILL_PROD_PMT_DELTA                                */
/*==============================================================*/
create table CS_BILL_PROD_PMT_DELTA (
   PRDCR_PMT_DID        int                  identity,
   PRDCR_PMT_KEY        varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PRDCR_KEY            varchar(100)         not null,
   RPT_GROUP_KEY        varchar(100)         not null,
   REVERSAL_DTS         DATETIME2            null,
   REVERSAL_REASON_CD   varchar(255)         not null,
   STATUS_CD            varchar(255)         not null,
   PMT_TYPE_CD          varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_PROD_PMT_DELTA
   add constraint CBPPD_PK primary key nonclustered (PRDCR_PMT_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_PROD_PMT_DELTA
   add constraint CBPPD_AK1 unique (PRDCR_PMT_DID)
go

/*==============================================================*/
/* Index: CBPPD_CBPB_XFK                                        */
/*==============================================================*/




create nonclustered index CBPPD_CBPB_XFK on CS_BILL_PROD_PMT_DELTA (PRDCR_KEY ASC)
go

/*==============================================================*/
/* Index: CBPPD_CRGB_XFK                                        */
/*==============================================================*/




create nonclustered index CBPPD_CRGB_XFK on CS_BILL_PROD_PMT_DELTA (RPT_GROUP_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_PROD_STMNT_BASE                               */
/*==============================================================*/
create table CS_BILL_PROD_STMNT_BASE (
   PRDCR_STMNT_BID      int                  identity,
   PRDCR_STMNT_KEY      varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_PROD_STMNT_BASE
   add constraint CBPSB_PK primary key nonclustered (PRDCR_STMNT_KEY)
go

alter table CS_BILL_PROD_STMNT_BASE
   add constraint CBPSB_AK1 unique (PRDCR_STMNT_BID)
go

/*==============================================================*/
/* Table: CS_BILL_PROD_STMNT_DELTA                              */
/*==============================================================*/
create table CS_BILL_PROD_STMNT_DELTA (
   PRDCR_STMNT_DID      int                  identity,
   PRDCR_STMNT_KEY      varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PRDCR_KEY            varchar(100)         not null,
   STMNT_DTS            DATETIME2            not null,
   STMNT_NO             varchar(255)         not null,
   STMNT_TYPE_CD        varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_PROD_STMNT_DELTA
   add constraint CBPSD_PK primary key nonclustered (PRDCR_STMNT_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_PROD_STMNT_DELTA
   add constraint CBPSD_AK1 unique (PRDCR_STMNT_DID)
go

/*==============================================================*/
/* Index: CBPSD_CBPB_XFK                                        */
/*==============================================================*/




create nonclustered index CBPSD_CBPB_XFK on CS_BILL_PROD_STMNT_DELTA (PRDCR_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_SUSP_PMT_BASE                                 */
/*==============================================================*/
create table CS_BILL_SUSP_PMT_BASE (
   SUSP_PMT_BID         int                  identity,
   SUSP_PMT_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_SUSP_PMT_BASE
   add constraint CBSPB_PK primary key nonclustered (SUSP_PMT_KEY)
go

alter table CS_BILL_SUSP_PMT_BASE
   add constraint CBSPB_AK1 unique (SUSP_PMT_BID)
go

/*==============================================================*/
/* Table: CS_BILL_SUSP_PMT_DELTA                                */
/*==============================================================*/
create table CS_BILL_SUSP_PMT_DELTA (
   SUSP_PMT_DID         int                  identity,
   SUSP_PMT_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   BILL_ACCT_APPLIED_TO_KEY varchar(100)         not null,
   BILL_POL_PRD_APPLIED_TO_KEY varchar(100)         not null,
   BILL_USER_APPLIED_BY_KEY varchar(100)         not null,
   MON_RCVD_KEY         varchar(100)         not null,
   RPT_GROUP_KEY        varchar(100)         not null,
   BILL_USER_REVERSED_BY_KEY varchar(100)         not null,
   PMT_INSTRUMENT_KEY   varchar(100)         not null,
   PRDCR_APPLIED_TO_KEY varchar(100)         not null,
   ACCT_NO              varchar(255)         not null,
   INV_NO               varchar(255)         not null,
   OFFER_NO             varchar(255)         not null,
   OFFER_OPT            varchar(255)         not null,
   PMT_DT               DATETIME2            not null,
   POL_NO               varchar(255)         not null,
   PRDCR_NAME           varchar(255)         not null,
   REF_NO               varchar(255)         not null,
   STATUS_CD            varchar(255)         not null,
   SUSP_PMT_TEXT        varchar(1333)        not null,
   CURR_CD              varchar(255)         not null,
   SUSP_AMT             numeric(18,2)        not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_SUSP_PMT_DELTA
   add constraint CBSPD_PK primary key nonclustered (SUSP_PMT_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_SUSP_PMT_DELTA
   add constraint CBSPD_AK1 unique (SUSP_PMT_DID)
go

/*==============================================================*/
/* Index: CBSPD_CBTCB_XFK                                       */
/*==============================================================*/




create nonclustered index CBSPD_CBTCB_XFK on CS_BILL_SUSP_PMT_DELTA (TACCT_CONTAINER_KEY ASC)
go

/*==============================================================*/
/* Index: CBSPD_CBAB_XFK                                        */
/*==============================================================*/




create nonclustered index CBSPD_CBAB_XFK on CS_BILL_SUSP_PMT_DELTA (BILL_ACCT_APPLIED_TO_KEY ASC)
go

/*==============================================================*/
/* Index: CBSPD_CBPPB_XFK                                       */
/*==============================================================*/




create nonclustered index CBSPD_CBPPB_XFK on CS_BILL_SUSP_PMT_DELTA (BILL_POL_PRD_APPLIED_TO_KEY ASC)
go

/*==============================================================*/
/* Index: CBSPD_CBUB_XFK                                        */
/*==============================================================*/




create nonclustered index CBSPD_CBUB_XFK on CS_BILL_SUSP_PMT_DELTA (BILL_USER_APPLIED_BY_KEY ASC)
go

/*==============================================================*/
/* Index: CBSPD_CBMRB_XFK                                       */
/*==============================================================*/




create nonclustered index CBSPD_CBMRB_XFK on CS_BILL_SUSP_PMT_DELTA (MON_RCVD_KEY ASC)
go

/*==============================================================*/
/* Index: CBSPD_CRGB_XFK                                        */
/*==============================================================*/




create nonclustered index CBSPD_CRGB_XFK on CS_BILL_SUSP_PMT_DELTA (RPT_GROUP_KEY ASC)
go

/*==============================================================*/
/* Index: CBSPD_CBUB1_XFK                                       */
/*==============================================================*/




create nonclustered index CBSPD_CBUB1_XFK on CS_BILL_SUSP_PMT_DELTA (BILL_USER_REVERSED_BY_KEY ASC)
go

/*==============================================================*/
/* Index: CBSPD_CBPIB_XFK                                       */
/*==============================================================*/




create nonclustered index CBSPD_CBPIB_XFK on CS_BILL_SUSP_PMT_DELTA (PMT_INSTRUMENT_KEY ASC)
go

/*==============================================================*/
/* Index: CBSPD_CBPB_XFK                                        */
/*==============================================================*/




create nonclustered index CBSPD_CBPB_XFK on CS_BILL_SUSP_PMT_DELTA (PRDCR_APPLIED_TO_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_TACCT_OWNER_PAT_BASE                          */
/*==============================================================*/
create table CS_BILL_TACCT_OWNER_PAT_BASE (
   TACCT_OWNER_PAT_BID  int                  identity,
   TACCT_OWNER_PAT_KEY  varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_TACCT_OWNER_PAT_BASE
   add constraint CBTOPB_PK primary key nonclustered (TACCT_OWNER_PAT_KEY)
go

alter table CS_BILL_TACCT_OWNER_PAT_BASE
   add constraint CBTOPB_AK1 unique (TACCT_OWNER_PAT_BID)
go

/*==============================================================*/
/* Table: CS_BILL_TACCT_OWNER_PAT_DELTA                         */
/*==============================================================*/
create table CS_BILL_TACCT_OWNER_PAT_DELTA (
   TACCT_OWNER_PAT_DID  int                  identity,
   TACCT_OWNER_PAT_KEY  varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TACCT_OWNER_NAME     varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_TACCT_OWNER_PAT_DELTA
   add constraint CBTOPD_PK primary key nonclustered (TACCT_OWNER_PAT_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_TACCT_OWNER_PAT_DELTA
   add constraint CBTOPD_AK1 unique (TACCT_OWNER_PAT_DID)
go

/*==============================================================*/
/* Table: CS_BILL_UNAPP_FUND_BASE                               */
/*==============================================================*/
create table CS_BILL_UNAPP_FUND_BASE (
   UNAPPLIED_FUND_BID   int                  identity,
   UNAPPLIED_FUND_KEY   varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_UNAPP_FUND_BASE
   add constraint CBUFB_PK primary key nonclustered (UNAPPLIED_FUND_KEY)
go

alter table CS_BILL_UNAPP_FUND_BASE
   add constraint CBUFB_AK1 unique (UNAPPLIED_FUND_BID)
go

/*==============================================================*/
/* Table: CS_BILL_UNAPP_FUND_DELTA                              */
/*==============================================================*/
create table CS_BILL_UNAPP_FUND_DELTA (
   UNAPPLIED_FUND_DID   int                  identity,
   UNAPPLIED_FUND_KEY   varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   BILL_POL_KEY         varchar(100)         not null,
   RPT_GROUP_KEY        varchar(100)         not null,
   TACCT_KEY            varchar(100)         not null,
   UNAPPLIED_FUND_TEXT  varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_UNAPP_FUND_DELTA
   add constraint CBUFD_PK primary key nonclustered (UNAPPLIED_FUND_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_UNAPP_FUND_DELTA
   add constraint CBUFD_AK1 unique (UNAPPLIED_FUND_DID)
go

/*==============================================================*/
/* Index: CBUFD_CBAB_XFK                                        */
/*==============================================================*/




create nonclustered index CBUFD_CBAB_XFK on CS_BILL_UNAPP_FUND_DELTA (BILL_ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBUFD_CRGB_XFK                                        */
/*==============================================================*/




create nonclustered index CBUFD_CRGB_XFK on CS_BILL_UNAPP_FUND_DELTA (RPT_GROUP_KEY ASC)
go

/*==============================================================*/
/* Index: CBUFD_CBTB_XFK                                        */
/*==============================================================*/




create nonclustered index CBUFD_CBTB_XFK on CS_BILL_UNAPP_FUND_DELTA (TACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CBUFD_CBPB_XFK                                        */
/*==============================================================*/




create nonclustered index CBUFD_CBPB_XFK on CS_BILL_UNAPP_FUND_DELTA (BILL_POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BILL_WRITEOFF_BASE                                 */
/*==============================================================*/
create table CS_BILL_WRITEOFF_BASE (
   WO_BID               int                  identity,
   WO_KEY               varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_LATE_ARRIVING_SCD char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_WRITEOFF_BASE
   add constraint CBWB_PK primary key nonclustered (WO_KEY)
go

alter table CS_BILL_WRITEOFF_BASE
   add constraint CBWB_AK1 unique (WO_BID)
go

/*==============================================================*/
/* Table: CS_BILL_WRITEOFF_DELTA                                */
/*==============================================================*/
create table CS_BILL_WRITEOFF_DELTA (
   WO_DID               int                  identity,
   WO_KEY               varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CHRG_PAT_KEY         varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   COMM_AGCY_PMT_ITEM_KEY varchar(100)         not null,
   GROSS_AGCY_PMT_ITEM_KEY varchar(100)         not null,
   CHRG_COMM_KEY        varchar(100)         not null,
   BILL_INV_ITEM_KEY    varchar(100)         not null,
   ITEM_COMM_KEY        varchar(100)         not null,
   PRDCR_KEY            varchar(100)         not null,
   REQUESTING_USER_KEY  varchar(100)         not null,
   EXECUTION_DTS        DATETIME2            null,
   APPRVL_DTS           DATETIME2            null,
   APPRVL_STATUS_CD     varchar(255)         not null,
   REASON_CD            varchar(255)         not null,
   WO_TYPE_CD           varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   WO_AMT               numeric(18,2)        not null,
   REVERSED_AMT         numeric(18,2)        not null,
   RETIRED_FL           char(1)              not null,
   REVERSED_FL          char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BILL_WRITEOFF_DELTA
   add constraint CBWD_PK primary key nonclustered (WO_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_BILL_WRITEOFF_DELTA
   add constraint CBWD_AK1 unique (WO_DID)
go

/*==============================================================*/
/* Index: CBWD_CBCPB_XFK                                        */
/*==============================================================*/




create nonclustered index CBWD_CBCPB_XFK on CS_BILL_WRITEOFF_DELTA (CHRG_PAT_KEY ASC)
go

/*==============================================================*/
/* Index: CBWD_CBTCB_XFK                                        */
/*==============================================================*/




create nonclustered index CBWD_CBTCB_XFK on CS_BILL_WRITEOFF_DELTA (TACCT_CONTAINER_KEY ASC)
go

/*==============================================================*/
/* Index: CBWD_CBDIB_XFK                                        */
/*==============================================================*/




create nonclustered index CBWD_CBDIB_XFK on CS_BILL_WRITEOFF_DELTA (COMM_AGCY_PMT_ITEM_KEY ASC)
go

/*==============================================================*/
/* Index: CBWD_CBDIB1_XFK                                       */
/*==============================================================*/




create nonclustered index CBWD_CBDIB1_XFK on CS_BILL_WRITEOFF_DELTA (GROSS_AGCY_PMT_ITEM_KEY ASC)
go

/*==============================================================*/
/* Index: CBWD_CBCCB_XFK                                        */
/*==============================================================*/




create nonclustered index CBWD_CBCCB_XFK on CS_BILL_WRITEOFF_DELTA (CHRG_COMM_KEY ASC)
go

/*==============================================================*/
/* Index: CBWD_CBIIB_XFK                                        */
/*==============================================================*/




create nonclustered index CBWD_CBIIB_XFK on CS_BILL_WRITEOFF_DELTA (BILL_INV_ITEM_KEY ASC)
go

/*==============================================================*/
/* Index: CBWD_CBICB_XFK                                        */
/*==============================================================*/




create nonclustered index CBWD_CBICB_XFK on CS_BILL_WRITEOFF_DELTA (ITEM_COMM_KEY ASC)
go

/*==============================================================*/
/* Index: CBWD_CBPB_XFK                                         */
/*==============================================================*/




create nonclustered index CBWD_CBPB_XFK on CS_BILL_WRITEOFF_DELTA (PRDCR_KEY ASC)
go

/*==============================================================*/
/* Index: CBWD_CBUB_XFK                                         */
/*==============================================================*/




create nonclustered index CBWD_CBUB_XFK on CS_BILL_WRITEOFF_DELTA (REQUESTING_USER_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BLDG_BASE                                          */
/*==============================================================*/
create table CS_BLDG_BASE (
   BLDG_BID             int                  not null,
   BLDG_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BLDG_BASE
   add constraint CBBA_PK primary key nonclustered (BLDG_KEY)
go

alter table CS_BLDG_BASE
   add constraint CBBA_AK1 unique (BLDG_BID)
go

/*==============================================================*/
/* Index: CBBA_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CBBA_CPBA_XFK on CS_BLDG_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_BLDG_DELTA                                         */
/*==============================================================*/
create table CS_BLDG_DELTA (
   BLDG_DID             int                  not null,
   BLDG_KEY             varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   LOC_KEY              varchar(100)         not null,
   ALARM_CERTIFICATE    varchar(255)         not null,
   ALARM_CERTIFICATION_CD varchar(255)         not null,
   ALARM_CL_CD          varchar(255)         not null,
   ALARM_EXP_DT         DATE                 null,
   ALARM_GRADE_CD       varchar(255)         not null,
   BLDG_FINISHED_AREA_SF_NO int                  null,
   BLDG_UNFINISHED_AREA_SF_NO int                  null,
   BASEMENT_AREA_SF_NO  int                  null,
   AREA_LEASED_CD       varchar(255)         not null,
   BLDG_ALARM_TYPE_CD   varchar(255)         not null,
   BLDG_NO              int                  null,
   BURGLAR_SAFEGUARD_CD varchar(255)         not null,
   CONSTR_TYPE_CD       varchar(255)         not null,
   BLDG_TEXT            varchar(255)         not null,
   EFFECTIVENESS_GRADE_CD varchar(255)         not null,
   HEATING_BOILER_ON_PRMSS_FL char(1)              not null,
   HEATING_BOILER_ELSE_WHERE_FL char(1)              not null,
   BASEMENTS_NO         int                  null,
   STORIES_NO           int                  null,
   UNITS_NO             int                  null,
   PCT_OCCUPIED_CD      varchar(255)         not null,
   PCT_VACANT_CD        varchar(255)         not null,
   PCT_RENTED_TO_OTHERS_CD varchar(255)         not null,
   ROOF_TYPE_CD         varchar(255)         not null,
   SPRINKLER_COVG_CD    varchar(255)         not null,
   BLDG_TYPE_CD         varchar(255)         not null,
   TOT_AREA_SF_NO       int                  null,
   WIND_RATING_CD       varchar(255)         not null,
   BUILD_YR             int                  null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_BLDG_DELTA
   add constraint CBDE_PK primary key nonclustered (BLDG_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_BLDG_DELTA
   add constraint CBDE_AK1 unique (BLDG_DID)
go

/*==============================================================*/
/* Index: CBDE_CLBA_XFK                                         */
/*==============================================================*/




create nonclustered index CBDE_CLBA_XFK on CS_BLDG_DELTA (LOC_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CATASTROPHE_DIM                                    */
/*==============================================================*/
create table CS_CATASTROPHE_DIM (
   CATASTROPHE_ID       int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CATASTROPHE_CD       varchar(255)         not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CATASTROPHE_DIM
   add constraint CCDI1_PK primary key nonclustered (CATASTROPHE_ID)
go

alter table CS_CATASTROPHE_DIM
   add constraint CCDI1_AK1 unique (SOURCE_SYSTEM, CATASTROPHE_CD)
go

/*==============================================================*/
/* Table: CS_CA_ADDL_INTRST_BASE                                */
/*==============================================================*/
create table CS_CA_ADDL_INTRST_BASE (
   CA_ADDL_INTRST_BID   int                  not null,
   CA_ADDL_INTRST_KEY   varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_ADDL_INTRST_BASE
   add constraint CCAIB_PK primary key nonclustered (CA_ADDL_INTRST_KEY)
go

alter table CS_CA_ADDL_INTRST_BASE
   add constraint CCAIB_AK1 unique (CA_ADDL_INTRST_BID)
go

/*==============================================================*/
/* Index: CCAIB_CCCB_XFK                                        */
/*==============================================================*/




create nonclustered index CCAIB_CCCB_XFK on CS_CA_ADDL_INTRST_BASE (CA_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CCAIB_CPBA_XFK                                        */
/*==============================================================*/




create nonclustered index CCAIB_CPBA_XFK on CS_CA_ADDL_INTRST_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CCAIB_CPPB_XFK                                        */
/*==============================================================*/




create nonclustered index CCAIB_CPPB_XFK on CS_CA_ADDL_INTRST_BASE (POL_PARTY_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CA_ADDL_INTRST_DELTA                               */
/*==============================================================*/
create table CS_CA_ADDL_INTRST_DELTA (
   CA_ADDL_INTRST_DID   int                  not null,
   CA_ADDL_INTRST_KEY   varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   ADDL_INTRST_TYPE_CD  varchar(255)         not null,
   CONTRACT_NO          varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_ADDL_INTRST_DELTA
   add constraint CCAID_PK primary key nonclustered (CA_ADDL_INTRST_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CA_ADDL_INTRST_DELTA
   add constraint CCAID_AK1 unique (CA_ADDL_INTRST_DID)
go

/*==============================================================*/
/* Table: CS_CA_COND_BASE                                       */
/*==============================================================*/
create table CS_CA_COND_BASE (
   CA_COND_BID          int                  not null,
   CA_COND_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   COND_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_COND_BASE
   add constraint CCCB1_PK primary key nonclustered (CA_COND_KEY)
go

alter table CS_CA_COND_BASE
   add constraint CCCB1_AK1 unique (CA_COND_BID)
go

/*==============================================================*/
/* Index: CCCB_CCBA_XFK                                         */
/*==============================================================*/




create nonclustered index CCCB_CCBA_XFK on CS_CA_COND_BASE (COND_KEY ASC)
go

/*==============================================================*/
/* Index: CCCB_CCCB_XFK                                         */
/*==============================================================*/




create nonclustered index CCCB_CCCB_XFK on CS_CA_COND_BASE (CA_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CCCB_CPBA1_XFK                                        */
/*==============================================================*/




create nonclustered index CCCB_CPBA1_XFK on CS_CA_COND_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CCCB_CPLB1_XFK                                        */
/*==============================================================*/




create nonclustered index CCCB_CPLB1_XFK on CS_CA_COND_BASE (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CA_COND_TERM_BASE                                  */
/*==============================================================*/
create table CS_CA_COND_TERM_BASE (
   CA_COND_TERM_BID     int                  not null,
   CA_COND_TERM_KEY     varchar(100)         not null,
   CA_COND_KEY          varchar(100)         not null,
   COND_TERM_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_COND_TERM_BASE
   add constraint CCCTB_PK primary key nonclustered (CA_COND_TERM_KEY)
go

alter table CS_CA_COND_TERM_BASE
   add constraint CCCTB_AK1 unique (CA_COND_TERM_BID)
go

/*==============================================================*/
/* Index: CCCTB_CCCB_XFK                                        */
/*==============================================================*/




create nonclustered index CCCTB_CCCB_XFK on CS_CA_COND_TERM_BASE (CA_COND_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CA_COND_TERM_DELTA                                 */
/*==============================================================*/
create table CS_CA_COND_TERM_DELTA (
   CA_COND_TERM_DID     int                  not null,
   CA_COND_TERM_KEY     varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   COND_CD              varchar(255)         not null,
   COND_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_COND_TERM_DELTA
   add constraint CCCTD_PK primary key nonclustered (CA_COND_TERM_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CA_COND_TERM_DELTA
   add constraint CCCTD_AK1 unique (CA_COND_TERM_DID)
go

/*==============================================================*/
/* Table: CS_CA_COVG_BASE                                       */
/*==============================================================*/
create table CS_CA_COVG_BASE (
   CA_COVG_BID          int                  not null,
   CA_COVG_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_COVG_BASE
   add constraint CCCB2_PK primary key nonclustered (CA_COVG_KEY)
go

alter table CS_CA_COVG_BASE
   add constraint CCCB2_AK1 unique (CA_COVG_BID)
go

/*==============================================================*/
/* Index: CCCB_CCBA1_XFK                                        */
/*==============================================================*/




create nonclustered index CCCB_CCBA1_XFK on CS_CA_COVG_BASE (COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CCCB_CCCB1_XFK                                        */
/*==============================================================*/




create nonclustered index CCCB_CCCB1_XFK on CS_CA_COVG_BASE (CA_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CCCB_CPBA2_XFK                                        */
/*==============================================================*/




create nonclustered index CCCB_CPBA2_XFK on CS_CA_COVG_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CCCB_CPLB2_XFK                                        */
/*==============================================================*/




create nonclustered index CCCB_CPLB2_XFK on CS_CA_COVG_BASE (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CA_COVG_TERM_BASE                                  */
/*==============================================================*/
create table CS_CA_COVG_TERM_BASE (
   CA_COVG_TERM_BID     int                  not null,
   CA_COVG_TERM_KEY     varchar(100)         not null,
   CA_COVG_KEY          varchar(100)         not null,
   COVG_TERM_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_COVG_TERM_BASE
   add constraint CCCTB1_PK primary key nonclustered (CA_COVG_TERM_KEY)
go

alter table CS_CA_COVG_TERM_BASE
   add constraint CCCTB1_AK1 unique (CA_COVG_TERM_BID)
go

/*==============================================================*/
/* Index: CCCTB_CCCB1_XFK                                       */
/*==============================================================*/




create nonclustered index CCCTB_CCCB1_XFK on CS_CA_COVG_TERM_BASE (CA_COVG_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CA_COVG_TERM_DELTA                                 */
/*==============================================================*/
create table CS_CA_COVG_TERM_DELTA (
   CA_COVG_TERM_DID     int                  not null,
   CA_COVG_TERM_KEY     varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   COVG_CD              varchar(255)         not null,
   COVG_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_COVG_TERM_DELTA
   add constraint CCCTD1_PK primary key nonclustered (CA_COVG_TERM_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CA_COVG_TERM_DELTA
   add constraint CCCTD1_AK1 unique (CA_COVG_TERM_DID)
go

/*==============================================================*/
/* Table: CS_CA_CVRBL_BASE                                      */
/*==============================================================*/
create table CS_CA_CVRBL_BASE (
   CA_CVRBL_BID         int                  identity,
   CA_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   OWNING_CVRBL_KEY     varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_CVRBL_BASE
   add constraint CCCB_PK primary key nonclustered (CA_CVRBL_KEY)
go

alter table CS_CA_CVRBL_BASE
   add constraint CCCB_AK1 unique (CA_CVRBL_BID)
go

/*==============================================================*/
/* Index: CCCB_CCCB2_XFK                                        */
/*==============================================================*/




create nonclustered index CCCB_CCCB2_XFK on CS_CA_CVRBL_BASE (OWNING_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CCCB_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CCCB_CPBA_XFK on CS_CA_CVRBL_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CCCB_CPLB_XFK                                         */
/*==============================================================*/




create nonclustered index CCCB_CPLB_XFK on CS_CA_CVRBL_BASE (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CA_DEALER_DELTA                                    */
/*==============================================================*/
create table CS_CA_DEALER_DELTA (
   CA_DEALER_DID        int                  not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   DEALER_LOC_KEY       varchar(100)         not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   DEALERS_NO           int                  null,
   GARAGE_DEALERS_CL_CD varchar(255)         not null,
   DEALERS_CL_CD        varchar(255)         not null,
   GARAGE_DEALERS_COVG_TYPE_CD varchar(255)         not null,
   FRAN_FL              char(1)              not null,
   TRNSPR_PLATES_CNT    int                  null,
   SETS_OF_DEALER_PLATES_CNT int                  null,
   RATING_BASE_CD       varchar(255)         not null,
   RATING_UNITS_TOT_CNT int                  null,
   FULL_TM_OPER_CNT     int                  null,
   PART_TM_OPER_CNT     int                  null,
   FULL_TM_EMP_CNT      int                  null,
   PART_TM_EMP_CNT      int                  null,
   NON_EMP_UNDER_AGE_25_CNT int                  null,
   NON_EMP_AGE_25_OR_OVER_CNT int                  null,
   PHYS_DMG_CLASS_CD    varchar(255)         not null,
   INS_VAL_AMT          numeric(18,2)        null,
   CURR_CD              varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_DEALER_DELTA
   add constraint CCDD1_PK primary key nonclustered (CA_CVRBL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CA_DEALER_DELTA
   add constraint CCDD1_AK1 unique (CA_DEALER_DID)
go

/*==============================================================*/
/* Index: CCDD_CLBA_XFK                                         */
/*==============================================================*/




create nonclustered index CCDD_CLBA_XFK on CS_CA_DEALER_DELTA (DEALER_LOC_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CA_DRVR_BASE                                       */
/*==============================================================*/
create table CS_CA_DRVR_BASE (
   CA_DRVR_BID          int                  not null,
   CA_DRVR_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_DRVR_BASE
   add constraint CCDB_PK primary key nonclustered (CA_DRVR_KEY)
go

alter table CS_CA_DRVR_BASE
   add constraint CCDB_AK1 unique (CA_DRVR_BID)
go

/*==============================================================*/
/* Index: CCDB_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CCDB_CPBA_XFK on CS_CA_DRVR_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CCDB_CPLB_XFK                                         */
/*==============================================================*/




create nonclustered index CCDB_CPLB_XFK on CS_CA_DRVR_BASE (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CA_DRVR_DELTA                                      */
/*==============================================================*/
create table CS_CA_DRVR_DELTA (
   CA_DRVR_DID          int                  not null,
   CA_DRVR_KEY          varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   DRVR_NO              int                  null,
   FIRST_NAME           varchar(255)         not null,
   LAST_NAME            varchar(255)         not null,
   BIRTH_DT             DATE                 null,
   GENDER_CD            varchar(255)         not null,
   MARITAL_STATUS_CD    varchar(255)         not null,
   STUDENT_FL           char(1)              not null,
   HIRE_DT              DATE                 null,
   LIC_NO               varchar(255)         not null,
   LIC_JURS_CD          varchar(255)         not null,
   DRVR_TRNG_FL         char(1)              not null,
   GOOD_DRVR_DISC_FL    char(1)              not null,
   MATURE_DRVR_TRNG_FL  char(1)              not null,
   ACCIDENT_CNT_CD      varchar(255)         not null,
   VIOLATION_CNT_CD     varchar(255)         not null,
   LIC_YR_NO            int                  null,
   DRVR_EXPER_CD        varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_DRVR_DELTA
   add constraint CCDD_PK primary key nonclustered (CA_DRVR_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CA_DRVR_DELTA
   add constraint CCDD_AK1 unique (CA_DRVR_DID)
go

/*==============================================================*/
/* Table: CS_CA_EXCL_BASE                                       */
/*==============================================================*/
create table CS_CA_EXCL_BASE (
   CA_EXCL_BID          int                  not null,
   CA_EXCL_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   EXCL_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_EXCL_BASE
   add constraint CCEB_PK primary key nonclustered (CA_EXCL_KEY)
go

alter table CS_CA_EXCL_BASE
   add constraint CCEB_AK1 unique (CA_EXCL_BID)
go

/*==============================================================*/
/* Index: CCEB_CCCB_XFK                                         */
/*==============================================================*/




create nonclustered index CCEB_CCCB_XFK on CS_CA_EXCL_BASE (CA_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CCEB_CEBA_XFK                                         */
/*==============================================================*/




create nonclustered index CCEB_CEBA_XFK on CS_CA_EXCL_BASE (EXCL_KEY ASC)
go

/*==============================================================*/
/* Index: CCEB_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CCEB_CPBA_XFK on CS_CA_EXCL_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CCEB_CPLB_XFK                                         */
/*==============================================================*/




create nonclustered index CCEB_CPLB_XFK on CS_CA_EXCL_BASE (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CA_EXCL_TERM_BASE                                  */
/*==============================================================*/
create table CS_CA_EXCL_TERM_BASE (
   CA_EXCL_TERM_BID     int                  not null,
   CA_EXCL_TERM_KEY     varchar(100)         not null,
   CA_EXCL_KEY          varchar(100)         not null,
   EXCL_TERM_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_EXCL_TERM_BASE
   add constraint CCETB_PK primary key nonclustered (CA_EXCL_TERM_KEY)
go

alter table CS_CA_EXCL_TERM_BASE
   add constraint CCETB_AK1 unique (CA_EXCL_TERM_BID)
go

/*==============================================================*/
/* Index: CCETB_CCEB_XFK                                        */
/*==============================================================*/




create nonclustered index CCETB_CCEB_XFK on CS_CA_EXCL_TERM_BASE (CA_EXCL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CA_EXCL_TERM_DELTA                                 */
/*==============================================================*/
create table CS_CA_EXCL_TERM_DELTA (
   CA_EXCL_TERM_DID     int                  not null,
   CA_EXCL_TERM_KEY     varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   EXCL_CD              varchar(255)         not null,
   EXCL_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_EXCL_TERM_DELTA
   add constraint CCETD_PK primary key nonclustered (CA_EXCL_TERM_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CA_EXCL_TERM_DELTA
   add constraint CCETD_AK1 unique (CA_EXCL_TERM_DID)
go

/*==============================================================*/
/* Table: CS_CA_GAR_SRVC_DELTA                                  */
/*==============================================================*/
create table CS_CA_GAR_SRVC_DELTA (
   GARAGE_SRVC_DID      int                  not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   GARAGE_SRVC_LOC_KEY  varchar(100)         not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   LIAB_CL_CD           varchar(255)         not null,
   EMP_AS_INS_FL        char(1)              not null,
   GARAGE_SRVC_NO       int                  null,
   EMP_CNT_NO           int                  null,
   PARTNERS_CNT_NO      int                  null,
   UNIT_NO              int                  null,
   INS_VAL_AMT          numeric(18,2)        null,
   CURR_CD              varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_GAR_SRVC_DELTA
   add constraint CCGSD_PK primary key nonclustered (CA_CVRBL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CA_GAR_SRVC_DELTA
   add constraint CCGSD_AK1 unique (GARAGE_SRVC_DID)
go

/*==============================================================*/
/* Index: CCGSD_CLBA_XFK                                        */
/*==============================================================*/




create nonclustered index CCGSD_CLBA_XFK on CS_CA_GAR_SRVC_DELTA (GARAGE_SRVC_LOC_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CA_JURS_DELTA                                      */
/*==============================================================*/
create table CS_CA_JURS_DELTA (
   CA_JURS_DID          int                  not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_JURS_DELTA
   add constraint CCJD_PK primary key nonclustered (CA_CVRBL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CA_JURS_DELTA
   add constraint CCJD_AK1 unique (CA_JURS_DID)
go

/*==============================================================*/
/* Table: CS_CA_LINE_SI_COND_BASE                               */
/*==============================================================*/
create table CS_CA_LINE_SI_COND_BASE (
   CA_LINE_SI_COND_BID  int                  not null,
   CA_LINE_SI_COND_KEY  varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   CA_COND_KEY          varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_LINE_SI_COND_BASE
   add constraint CCLSCB_PK primary key nonclustered (CA_LINE_SI_COND_KEY)
go

alter table CS_CA_LINE_SI_COND_BASE
   add constraint CCLSCB_AK1 unique (CA_LINE_SI_COND_BID)
go

/*==============================================================*/
/* Index: CCLSCB_CCCB_XFK                                       */
/*==============================================================*/




create nonclustered index CCLSCB_CCCB_XFK on CS_CA_LINE_SI_COND_BASE (CA_COND_KEY ASC)
go

/*==============================================================*/
/* Index: CCLSCB_CPBA_XFK                                       */
/*==============================================================*/




create nonclustered index CCLSCB_CPBA_XFK on CS_CA_LINE_SI_COND_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CA_LINE_SI_COND_DELTA                              */
/*==============================================================*/
create table CS_CA_LINE_SI_COND_DELTA (
   CA_LINE_SI_COND_DID  int                  not null,
   CA_LINE_SI_COND_KEY  varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   POL_PARTY_KEY        varchar(100)         not null,
   SCHED_NO             int                  null,
   TYPE_KEY_COL_1       varchar(255)         not null,
   TYPE_KEY_COL_2       varchar(255)         not null,
   STR_COL_1            varchar(255)         not null,
   STR_COL_2            varchar(255)         not null,
   DT_COL_1_DTS         DATETIME2            null,
   INT_COL_1            int                  null,
   POS_INT_COL_1        int                  null,
   NON_NEG_INT_1        int                  null,
   NON_NEG_INT_2        int                  null,
   NON_NEG_INT_3        int                  null,
   BOOL_COL_1_FL        char(1)              not null,
   BOOL_COL_2_FL        char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_LINE_SI_COND_DELTA
   add constraint CCLSCD_PK primary key nonclustered (CA_LINE_SI_COND_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CA_LINE_SI_COND_DELTA
   add constraint CCLSCD_AK1 unique (CA_LINE_SI_COND_DID)
go

/*==============================================================*/
/* Index: CCLSCD_CPPB_XFK                                       */
/*==============================================================*/




create nonclustered index CCLSCD_CPPB_XFK on CS_CA_LINE_SI_COND_DELTA (POL_PARTY_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CA_MOD_BASE                                        */
/*==============================================================*/
create table CS_CA_MOD_BASE (
   CA_MOD_BID           int                  not null,
   CA_MOD_KEY           varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   MOD_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_MOD_BASE
   add constraint CCMB_PK primary key nonclustered (CA_MOD_KEY)
go

alter table CS_CA_MOD_BASE
   add constraint CCMB_AK1 unique (CA_MOD_BID)
go

/*==============================================================*/
/* Index: CCMB_CCCB_XFK                                         */
/*==============================================================*/




create nonclustered index CCMB_CCCB_XFK on CS_CA_MOD_BASE (CA_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CCMB_CMBA_XFK                                         */
/*==============================================================*/




create nonclustered index CCMB_CMBA_XFK on CS_CA_MOD_BASE (MOD_KEY ASC)
go

/*==============================================================*/
/* Index: CCMB_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CCMB_CPBA_XFK on CS_CA_MOD_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CCMB_CPLB_XFK                                         */
/*==============================================================*/




create nonclustered index CCMB_CPLB_XFK on CS_CA_MOD_BASE (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CA_MOD_DELTA                                       */
/*==============================================================*/
create table CS_CA_MOD_DELTA (
   CA_MOD_DID           int                  not null,
   CA_MOD_KEY           varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   MOD_CD               varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   MOD_VAL_TEXT         varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_MOD_DELTA
   add constraint CCMD_PK primary key nonclustered (CA_MOD_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CA_MOD_DELTA
   add constraint CCMD_AK1 unique (CA_MOD_DID)
go

/*==============================================================*/
/* Table: CS_CA_MOD_RF_BASE                                     */
/*==============================================================*/
create table CS_CA_MOD_RF_BASE (
   CA_MOD_RF_BID        int                  not null,
   CA_MOD_RF_KEY        varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   CA_MOD_KEY           varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_MOD_RF_BASE
   add constraint CCMRB_PK primary key nonclustered (CA_MOD_RF_KEY)
go

alter table CS_CA_MOD_RF_BASE
   add constraint CCMRB_AK1 unique (CA_MOD_RF_BID)
go

/*==============================================================*/
/* Index: CCMRB_CCMB_XFK                                        */
/*==============================================================*/




create nonclustered index CCMRB_CCMB_XFK on CS_CA_MOD_RF_BASE (CA_MOD_KEY ASC)
go

/*==============================================================*/
/* Index: CCMRB_CPBA1_XFK                                       */
/*==============================================================*/




create nonclustered index CCMRB_CPBA1_XFK on CS_CA_MOD_RF_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CA_MOD_RF_DELTA                                    */
/*==============================================================*/
create table CS_CA_MOD_RF_DELTA (
   CA_MOD_RF_DID        int                  not null,
   CA_MOD_RF_KEY        varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   RF_CD                varchar(255)         not null,
   ASSESS_RATE          numeric(18,4)        not null,
   JUST_TEXT            varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_MOD_RF_DELTA
   add constraint CCMRD_PK primary key nonclustered (CA_MOD_RF_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CA_MOD_RF_DELTA
   add constraint CCMRD_AK1 unique (CA_MOD_RF_DID)
go

/*==============================================================*/
/* Table: CS_CA_NMD_INDIV_DELTA                                 */
/*==============================================================*/
create table CS_CA_NMD_INDIV_DELTA (
   NMD_INDIV_DID        int                  not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   INDIV_NAME           varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_NMD_INDIV_DELTA
   add constraint CCNID_PK primary key nonclustered (CA_CVRBL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CA_NMD_INDIV_DELTA
   add constraint CCNID_AK1 unique (NMD_INDIV_DID)
go

/*==============================================================*/
/* Table: CS_CA_POL_LINE_DELTA                                  */
/*==============================================================*/
create table CS_CA_POL_LINE_DELTA (
   CA_POL_LINE_DID      int                  not null,
   POL_LINE_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   AUTO_SMBL_MNUL_EDIT_DT DATE                 null,
   CSTM_AUTO_SMBL_DESC  varchar(255)         not null,
   FLEET_TYPE_CD        varchar(255)         not null,
   LEGAL_ENTITY_TYPE_CD varchar(255)         not null,
   CA_POL_TYPE_CD       varchar(255)         not null,
   BUS_START_DT         DATE                 null,
   EXPER_RATING_FL      char(1)              not null,
   RISK_TYPE_CD         varchar(255)         not null,
   SCHED_RATING_MOD_FL  char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_POL_LINE_DELTA
   add constraint CCPLD_PK primary key nonclustered (POL_LINE_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CA_POL_LINE_DELTA
   add constraint CCPLD_AK1 unique (CA_POL_LINE_DID)
go

/*==============================================================*/
/* Table: CS_CA_PREM_TRAN                                       */
/*==============================================================*/
create table CS_CA_PREM_TRAN (
   CA_PREM_TRANS_TID    int                  identity,
   CA_PREM_TRANS_KEY    varchar(100)         not null,
   AGCY_KEY             varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   AUTO_DEALER_LOC_KEY  varchar(100)         not null,
   GARAGE_SRVC_LOC_KEY  varchar(100)         not null,
   VEH_GARAGE_LOC_KEY   varchar(100)         not null,
   CA_COVG_KEY          varchar(100)         not null,
   REC_TYPE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ACCTG_PRD_ID         int                  not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ORIG_TRANS_PROC_DTS  DATETIME2            not null,
   ORIG_END_EFF_DT      DATE                 not null,
   TRANS_EFF_DT         DATE                 not null,
   TRANS_EXP_DT         DATE                 not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   PREM_BASIS_AMT       numeric(18,2)        not null,
   STD_BASE_RATE        numeric(18,4)        not null,
   STD_ADJ_RATE         numeric(18,4)        not null,
   STD_AMT              numeric(18,2)        not null,
   STD_TERM_AMT         numeric(18,2)        not null,
   OVERRIDE_BASE_RATE   numeric(18,4)        not null,
   OVERRIDE_ADJ_RATE    numeric(18,4)        not null,
   OVERRIDE_AMT         numeric(18,2)        not null,
   OVERRIDE_TERM_AMT    numeric(18,2)        not null,
   ACT_BASE_RATE        numeric(18,4)        not null,
   ACT_ADJ_RATE         numeric(18,4)        not null,
   ACT_AMT              numeric(18,2)        not null,
   ACT_TERM_AMT         numeric(18,2)        not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   STTLMNT_PREM_BASIS_AMT numeric(18,2)        null,
   STTLMNT_STD_AMT      numeric(18,2)        null,
   STTLMNT_STD_TERM_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   STTLMNT_ACT_AMT      numeric(18,2)        null,
   STTLMNT_ACT_TERM_AMT numeric(18,2)        null,
   STTLMNT_CURR_CD      varchar(255)         not null,
   STTLMNT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_STTLMNT_RATE numeric(15,10)       null,
   REG_RPT_PREM_BASIS_AMT numeric(18,2)        null,
   REG_RPT_STD_AMT      numeric(18,2)        null,
   REG_RPT_STD_TERM_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   REG_RPT_ACT_AMT      numeric(18,2)        null,
   REG_RPT_ACT_TERM_AMT numeric(18,2)        null,
   REG_RPT_CURR_CD      varchar(255)         not null,
   REG_RPT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_REG_RPT_RATE numeric(15,10)       null,
   RPT_PREM_BASIS_AMT   numeric(18,2)        null,
   RPT_STD_AMT          numeric(18,2)        null,
   RPT_STD_TERM_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   RPT_ACT_AMT          numeric(18,2)        null,
   RPT_ACT_TERM_AMT     numeric(18,2)        null,
   RPT_CURR_CD          varchar(255)         not null,
   RPT_TRANS_AMT        numeric(18,2)        not null,
   TRANS_TO_RPT_RATE    numeric(15,10)       not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   COMM_PCT             numeric(6,3)         not null,
   EST_PREM_AMT         numeric(18,2)        not null,
   RPT_PREM_AMT         numeric(18,2)        not null,
   AUDIT_PREM_AMT       numeric(18,2)        not null,
   AUDITABLE_FL         char(1)              not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_PREM_TRAN
   add constraint CCPT_PK primary key nonclustered (CA_PREM_TRANS_TID)
go

alter table CS_CA_PREM_TRAN
   add constraint CCPT_AK1 unique (CA_PREM_TRANS_KEY)
go

/*==============================================================*/
/* Index: CCPT_CABA_XFK                                         */
/*==============================================================*/




create nonclustered index CCPT_CABA_XFK on CS_CA_PREM_TRAN (AGCY_KEY ASC)
go

/*==============================================================*/
/* Index: CCPT_CABA1_XFK                                        */
/*==============================================================*/




create nonclustered index CCPT_CABA1_XFK on CS_CA_PREM_TRAN (AGNT_KEY ASC)
go

/*==============================================================*/
/* Index: CCPT_CCBA_XFK                                         */
/*==============================================================*/




create nonclustered index CCPT_CCBA_XFK on CS_CA_PREM_TRAN (COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CCPT_CCCB_XFK                                         */
/*==============================================================*/




create nonclustered index CCPT_CCCB_XFK on CS_CA_PREM_TRAN (CA_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CCPT_CCCB1_XFK                                        */
/*==============================================================*/




create nonclustered index CCPT_CCCB1_XFK on CS_CA_PREM_TRAN (CA_COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CCPT_CLBA1_XFK                                        */
/*==============================================================*/




create nonclustered index CCPT_CLBA1_XFK on CS_CA_PREM_TRAN (AUTO_DEALER_LOC_KEY ASC)
go

/*==============================================================*/
/* Index: CCPT_CLBA2_XFK                                        */
/*==============================================================*/




create nonclustered index CCPT_CLBA2_XFK on CS_CA_PREM_TRAN (GARAGE_SRVC_LOC_KEY ASC)
go

/*==============================================================*/
/* Index: CCPT_CLBA3_XFK                                        */
/*==============================================================*/




create nonclustered index CCPT_CLBA3_XFK on CS_CA_PREM_TRAN (VEH_GARAGE_LOC_KEY ASC)
go

/*==============================================================*/
/* Index: CCPT_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CCPT_CPBA_XFK on CS_CA_PREM_TRAN (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CCPT_CPLB_XFK                                         */
/*==============================================================*/




create nonclustered index CCPT_CPLB_XFK on CS_CA_PREM_TRAN (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CA_SI_DELTA                                        */
/*==============================================================*/
create table CS_CA_SI_DELTA (
   CA_SI_DID            int                  not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CA_COVG_KEY          varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   CA_ADDL_INTRST_KEY   varchar(100)         not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   SCHED_NO             int                  null,
   TYPE_KEY_COL_1       varchar(255)         not null,
   TYPE_KEY_COL_2       varchar(255)         not null,
   STR_COL_1            varchar(255)         not null,
   STR_COL_2            varchar(255)         not null,
   DT_COL_1_DTS         DATETIME2            null,
   INT_COL_1            int                  null,
   POS_INT_COL_1        int                  null,
   NON_NEG_INT_1        int                  null,
   NON_NEG_INT_2        int                  null,
   NON_NEG_INT_3        int                  null,
   BOOL_COL_1_FL        char(1)              not null,
   BOOL_COL_2_FL        char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_SI_DELTA
   add constraint CCSD1_PK primary key nonclustered (CA_CVRBL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CA_SI_DELTA
   add constraint CCSD1_AK1 unique (CA_SI_DID)
go

/*==============================================================*/
/* Index: CCSD_CCAIB_XFK                                        */
/*==============================================================*/




create nonclustered index CCSD_CCAIB_XFK on CS_CA_SI_DELTA (CA_ADDL_INTRST_KEY ASC)
go

/*==============================================================*/
/* Index: CCSD_CCCB1_XFK                                        */
/*==============================================================*/




create nonclustered index CCSD_CCCB1_XFK on CS_CA_SI_DELTA (CA_COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CCSD_CLBA_XFK                                         */
/*==============================================================*/




create nonclustered index CCSD_CLBA_XFK on CS_CA_SI_DELTA (LOC_KEY ASC)
go

/*==============================================================*/
/* Index: CCSD_CPPB_XFK                                         */
/*==============================================================*/




create nonclustered index CCSD_CPPB_XFK on CS_CA_SI_DELTA (POL_PARTY_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CA_VEH_DELTA                                       */
/*==============================================================*/
create table CS_CA_VEH_DELTA (
   CA_VEH_DID           int                  not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   GARAGE_LOC_KEY       varchar(100)         not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   VEH_ID_NO            varchar(255)         not null,
   MAKE_NAME            varchar(255)         not null,
   MODEL_NAME           varchar(255)         not null,
   MODEL_YR             int                  null,
   VEH_NO               int                  null,
   ORIG_COST_NEW_AMT    numeric(18,2)        null,
   STATED_AMT           numeric(18,2)        null,
   UNIT_NO              int                  null,
   LIC_JURS_CD          varchar(255)         not null,
   LIC_PLATE            varchar(255)         not null,
   LEASE_OR_RENT_FL     char(1)              not null,
   LEN_OF_LEASE_CD      varchar(255)         not null,
   CL_CD                varchar(255)         not null,
   FLEET_FL             char(1)              not null,
   LESSOR_AN_EMP_FL     char(1)              not null,
   PRVT_PSNGR_OPER_EXPER_CD varchar(255)         not null,
   PRVT_PSNGR_USE_CD    varchar(255)         not null,
   PRVT_PSNGR_TYPE_CD   varchar(255)         not null,
   BUS_USE_CL_CD        varchar(255)         not null,
   GVW_NO               int                  null,
   TRUCK_RADIUS_CL_CD   varchar(255)         not null,
   TRUCK_SEC_CL_CD      varchar(255)         not null,
   SEC_CL_CD            varchar(255)         not null,
   SIZE_CL_CD           varchar(255)         not null,
   TRLR_HOW_USED_CD     varchar(255)         not null,
   FAR_TERM_ZONE        varchar(255)         not null,
   GARAGING_ZONE        varchar(255)         not null,
   DAYS_SCHL_YR_CNT     int                  null,
   SCHL_BUS_PRORATION_FL char(1)              not null,
   SEATING_CPCTY_CD     varchar(255)         not null,
   PUB_TRNSP_TYPE_CD    varchar(255)         not null,
   PUB_TRNSP_RADIUS_CL_CD varchar(255)         not null,
   SPCL_VEH_CL_CD       varchar(255)         not null,
   ENGINE_SIZE_CD       varchar(255)         not null,
   GROSS_RECEIPTS_AMT   numeric(18,2)        null,
   NON_OWNED_AUTOS_FL   char(1)              not null,
   DAYS_VEH_LEASED_CNT  varchar(255)         not null,
   FCTRY_TST_EMP_CNT    int                  null,
   REPO_AUTOS_CNT       int                  null,
   PHYS_DMG_CLASS_CD    varchar(255)         not null,
   SUPP_TYPE_CD         varchar(255)         not null,
   COST_HIRE_FOR_COLL_AMT numeric(18,2)        null,
   COST_HIRE_FOR_LIAB_AMT numeric(18,2)        null,
   COST_HIRE_FOR_EXCS_LIAB_AMT numeric(18,2)        null,
   COST_HIRE_FOR_OTC_AMT numeric(18,2)        null,
   SPCL_VEH_TYPE_CD     varchar(255)         not null,
   REG_USE_FL           char(1)              not null,
   CURR_CD              varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CA_VEH_DELTA
   add constraint CCVD_PK primary key nonclustered (CA_CVRBL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CA_VEH_DELTA
   add constraint CCVD_AK1 unique (CA_VEH_DID)
go

/*==============================================================*/
/* Index: CCVD_CLBA_XFK                                         */
/*==============================================================*/




create nonclustered index CCVD_CLBA_XFK on CS_CA_VEH_DELTA (GARAGE_LOC_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CLAIMANT_BASE                                      */
/*==============================================================*/
create table CS_CLAIMANT_BASE (
   CLMNT_BASE_ID        int                  not null,
   CLMNT_KEY            varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CLAIM_KEY            varchar(100)         not null,
   CLMNT_NO             varchar(255)         not null,
   CLMNT_NAME           varchar(255)         not null,
   CLMNT_ADDR_1         varchar(255)         not null,
   CLMNT_ADDR_2         varchar(255)         not null,
   CLMNT_ADDR_3         varchar(255)         not null,
   CLMNT_CITY           varchar(255)         not null,
   CLMNT_COUNTY         varchar(255)         not null,
   CLMNT_STATE_CD       varchar(255)         not null,
   CLMNT_ZIP_CD         varchar(15)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CLAIMANT_BASE
   add constraint CCBA1_PK primary key nonclustered (CLMNT_KEY)
go

alter table CS_CLAIMANT_BASE
   add constraint CCBA1_AK1 unique (CLMNT_BASE_ID)
go

/*==============================================================*/
/* Index: CCBA_CCBA_XFK                                         */
/*==============================================================*/




create nonclustered index CCBA_CCBA_XFK on CS_CLAIMANT_BASE (CLAIM_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CLAIMANT_DELTA                                     */
/*==============================================================*/
create table CS_CLAIMANT_DELTA (
   CLMNT_ID             int                  not null,
   CLMNT_KEY            varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CLAIM_KEY            varchar(100)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CLAIMANT_DELTA
   add constraint CCDE_PK primary key nonclustered (CLMNT_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_CLAIMANT_DELTA
   add constraint CCDE_AK1 unique (CLMNT_ID)
go

/*==============================================================*/
/* Table: CS_CLAIM_BASE                                         */
/*==============================================================*/
create table CS_CLAIM_BASE (
   CLAIM_BASE_ID        int                  not null,
   CLAIM_KEY            varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   POL_KEY              varchar(100)         not null,
   LAST_STATUS_ID       int                  null,
   LAST_STATUS_PROC_DTS DATETIME2            null,
   CLAIM_NO             varchar(255)         not null,
   CATASTROPHE_CD       varchar(255)         not null,
   CATASTROPHE_DECLARE_DT DATE                 null,
   RPRTD_DT             DATE                 null,
   SETUP_DT             DATE                 not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CLAIM_BASE
   add constraint CCBA2_PK primary key nonclustered (CLAIM_KEY)
go

alter table CS_CLAIM_BASE
   add constraint CCBA2_AK1 unique (CLAIM_BASE_ID)
go

/*==============================================================*/
/* Index: CCBA_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CCBA_CPBA_XFK on CS_CLAIM_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CCBA_CCSD_XFK                                         */
/*==============================================================*/




create nonclustered index CCBA_CCSD_XFK on CS_CLAIM_BASE (LAST_STATUS_ID ASC)
go

/*==============================================================*/
/* Table: CS_CLAIM_DELTA                                        */
/*==============================================================*/
create table CS_CLAIM_DELTA (
   CLAIM_ID             int                  not null,
   CLAIM_KEY            varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   LOSS_TYPE_CD         varchar(255)         not null,
   EXAMINER_CD          varchar(255)         not null,
   CAUSE_OF_LOSS_CD     varchar(255)         not null,
   LOSS_TEXT            varchar(1333)        not null,
   LOSS_DT              DATE                 not null,
   LOSS_CITY            varchar(255)         not null,
   LOSS_STATE_CD        varchar(255)         not null,
   LOSS_ZIP_CD          varchar(15)          not null,
   FAST_TRACK_FL        char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CLAIM_DELTA
   add constraint CCDE1_PK primary key nonclustered (CLAIM_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_CLAIM_DELTA
   add constraint CCDE1_AK1 unique (CLAIM_ID)
go

/*==============================================================*/
/* Table: CS_CLAIM_FEATURE_BASE                                 */
/*==============================================================*/
create table CS_CLAIM_FEATURE_BASE (
   FEATURE_BASE_ID      int                  not null,
   FEATURE_KEY          varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CLAIM_KEY            varchar(100)         not null,
   CLMNT_KEY            varchar(100)         not null,
   COVG_CD              varchar(255)         not null,
   LAST_STATUS_ID       int                  null,
   LAST_STATUS_PROC_DTS DATETIME2            null,
   FEATURE_RPRTD_DT     DATE                 null,
   FEATURE_SETUP_DT     DATE                 not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CLAIM_FEATURE_BASE
   add constraint CCFB_PK primary key nonclustered (FEATURE_KEY)
go

alter table CS_CLAIM_FEATURE_BASE
   add constraint CCFB_AK1 unique (FEATURE_BASE_ID)
go

/*==============================================================*/
/* Index: CCFB_CCSD_XFK                                         */
/*==============================================================*/




create nonclustered index CCFB_CCSD_XFK on CS_CLAIM_FEATURE_BASE (LAST_STATUS_ID ASC)
go

/*==============================================================*/
/* Index: CCFB_CCBA_XFK                                         */
/*==============================================================*/




create nonclustered index CCFB_CCBA_XFK on CS_CLAIM_FEATURE_BASE (CLMNT_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CLAIM_FEATURE_DELTA                                */
/*==============================================================*/
create table CS_CLAIM_FEATURE_DELTA (
   FEATURE_ID           int                  not null,
   FEATURE_KEY          varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   CLAIM_KEY            varchar(100)         not null,
   CLMNT_KEY            varchar(100)         not null,
   COVG_CD              varchar(255)         not null,
   SUIT_FL              char(1)              not null,
   ADJSTR_CD            varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CLAIM_FEATURE_DELTA
   add constraint CCFD_PK primary key nonclustered (FEATURE_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_CLAIM_FEATURE_DELTA
   add constraint CCFD_AK1 unique (FEATURE_ID)
go

/*==============================================================*/
/* Table: CS_CLAIM_FEATURE_STATUS_TRAN                          */
/*==============================================================*/
create table CS_CLAIM_FEATURE_STATUS_TRAN (
   FEATURE_STATUS_ID    int                  identity,
   FEATURE_KEY          varchar(100)         not null,
   CLAIM_STATUS_ID      int                  not null,
   STATUS_PROC_DTS      DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CLAIM_KEY            varchar(100)         not null,
   CLMNT_KEY            varchar(100)         not null,
   OPEN_DT              DATE                 null,
   REOPEN_DT            DATE                 null,
   CLOSED_DT            DATE                 null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CLAIM_FEATURE_STATUS_TRAN
   add constraint CCFST_PK primary key nonclustered (FEATURE_KEY, CLAIM_STATUS_ID, STATUS_PROC_DTS)
go

alter table CS_CLAIM_FEATURE_STATUS_TRAN
   add constraint CCFST_AK1 unique (FEATURE_STATUS_ID)
go

/*==============================================================*/
/* Index: CCFST_CCSD_XFK                                        */
/*==============================================================*/




create nonclustered index CCFST_CCSD_XFK on CS_CLAIM_FEATURE_STATUS_TRAN (CLAIM_STATUS_ID ASC)
go

/*==============================================================*/
/* Table: CS_CLAIM_STATUS_DIM                                   */
/*==============================================================*/
create table CS_CLAIM_STATUS_DIM (
   CLAIM_STATUS_ID      int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   STATUS_TYPE_CD       varchar(255)         not null,
   STATUS_CD            varchar(255)         not null,
   REASON_CD            varchar(1333)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CLAIM_STATUS_DIM
   add constraint CCSD_PK primary key nonclustered (CLAIM_STATUS_ID)
go

alter table CS_CLAIM_STATUS_DIM
   add constraint CCSD_AK1 unique (SOURCE_SYSTEM, STATUS_TYPE_CD, STATUS_CD, REASON_CD)
go

/*==============================================================*/
/* Table: CS_CLAIM_STATUS_TRAN                                  */
/*==============================================================*/
create table CS_CLAIM_STATUS_TRAN (
   CLAIM_STATUS_TRANS_ID int                  identity,
   CLAIM_KEY            varchar(100)         not null,
   CLAIM_STATUS_ID      int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   STATUS_PROC_DTS      DATETIME2            not null,
   OPEN_DT              DATE                 null,
   REOPEN_DT            DATE                 null,
   CLOSED_DT            DATE                 null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CLAIM_STATUS_TRAN
   add constraint CCST_PK primary key nonclustered (CLAIM_KEY, CLAIM_STATUS_ID, STATUS_PROC_DTS)
go

alter table CS_CLAIM_STATUS_TRAN
   add constraint CCST_AK1 unique (CLAIM_STATUS_TRANS_ID)
go

/*==============================================================*/
/* Index: CCST_CCSD_XFK                                         */
/*==============================================================*/




create nonclustered index CCST_CCSD_XFK on CS_CLAIM_STATUS_TRAN (CLAIM_STATUS_ID ASC)
go

/*==============================================================*/
/* Table: CS_COND_BASE                                          */
/*==============================================================*/
create table CS_COND_BASE (
   COND_BID             int                  not null,
   COND_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_COND_BASE
   add constraint CCBA3_PK primary key nonclustered (COND_KEY)
go

alter table CS_COND_BASE
   add constraint CCBA3_AK1 unique (COND_BID)
go

/*==============================================================*/
/* Table: CS_COND_DELTA                                         */
/*==============================================================*/
create table CS_COND_DELTA (
   COND_DID             int                  not null,
   COND_KEY             varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   COND_CD              varchar(255)         not null,
   CATG_CD              varchar(255)         not null,
   COND_TEXT            varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_COND_DELTA
   add constraint CCDE3_PK primary key nonclustered (COND_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_COND_DELTA
   add constraint CCDE3_AK1 unique (COND_DID)
go

/*==============================================================*/
/* Table: CS_COVG_BASE                                          */
/*==============================================================*/
create table CS_COVG_BASE (
   COVG_BID             int                  not null,
   COVG_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_COVG_BASE
   add constraint CCBA_PK primary key nonclustered (COVG_KEY)
go

alter table CS_COVG_BASE
   add constraint CCBA_AK1 unique (COVG_BID)
go

/*==============================================================*/
/* Table: CS_COVG_DELTA                                         */
/*==============================================================*/
create table CS_COVG_DELTA (
   COVG_DID             int                  not null,
   COVG_KEY             varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   COVG_PART_CD         varchar(255)         not null,
   COVG_CD              varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_COVG_DELTA
   add constraint CCDE2_PK primary key nonclustered (COVG_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_COVG_DELTA
   add constraint CCDE2_AK1 unique (COVG_DID)
go

/*==============================================================*/
/* Table: CS_CP_ADDL_INTRST_BASE                                */
/*==============================================================*/
create table CS_CP_ADDL_INTRST_BASE (
   CP_ADDL_INTRST_BID   int                  not null,
   CP_ADDL_INTRST_KEY   varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_ADDL_INTRST_BASE
   add constraint CCAIB1_PK primary key nonclustered (CP_ADDL_INTRST_KEY)
go

alter table CS_CP_ADDL_INTRST_BASE
   add constraint CCAIB1_AK1 unique (CP_ADDL_INTRST_BID)
go

/*==============================================================*/
/* Index: CCAIB_CCCB1_XFK                                       */
/*==============================================================*/




create nonclustered index CCAIB_CCCB1_XFK on CS_CP_ADDL_INTRST_BASE (CP_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CCAIB_CPBA1_XFK                                       */
/*==============================================================*/




create nonclustered index CCAIB_CPBA1_XFK on CS_CP_ADDL_INTRST_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CCAIB_CPPB1_XFK                                       */
/*==============================================================*/




create nonclustered index CCAIB_CPPB1_XFK on CS_CP_ADDL_INTRST_BASE (POL_PARTY_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_ADDL_INTRST_DELTA                               */
/*==============================================================*/
create table CS_CP_ADDL_INTRST_DELTA (
   CP_ADDL_INTRST_DID   int                  not null,
   CP_ADDL_INTRST_KEY   varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   ADDL_INTRST_TYPE_CD  varchar(255)         not null,
   CONTRACT_NO          varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_ADDL_INTRST_DELTA
   add constraint CCAID1_PK primary key nonclustered (CP_ADDL_INTRST_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CP_ADDL_INTRST_DELTA
   add constraint CCAID1_AK1 unique (CP_ADDL_INTRST_DID)
go

/*==============================================================*/
/* Table: CS_CP_BI_DELTA                                        */
/*==============================================================*/
create table CS_CP_BI_DELTA (
   CP_BUS_INC_DID       int                  not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CP_BLDG_KEY          varchar(100)         not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   WATERFRONT_PROP_FL   char(1)              not null,
   INC_IN_BLNKT_FL      char(1)              not null,
   FUNG_WET_DRY_ROT_BACT_FL char(1)              not null,
   BUS_INC_COVG_TYPE_CD varchar(255)         not null,
   WATERFRONT_CONSTR_TEXT varchar(255)         not null,
   TYPE_OF_RISK_TEXT    varchar(255)         not null,
   RISK_EDU_INST_PCT    varchar(255)         not null,
   RISK_MERC_PCT        varchar(255)         not null,
   RISK_RENTAL_PCT      varchar(255)         not null,
   RISK_MFG_PCT         varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_BI_DELTA
   add constraint CCBD2_PK primary key nonclustered (CP_CVRBL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CP_BI_DELTA
   add constraint CCBD2_AK1 unique (CP_BUS_INC_DID)
go

/*==============================================================*/
/* Index: CCBD_CCCB3_XFK                                        */
/*==============================================================*/




create nonclustered index CCBD_CCCB3_XFK on CS_CP_BI_DELTA (CP_BLDG_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_BLDG_DELTA                                      */
/*==============================================================*/
create table CS_CP_BLDG_DELTA (
   CP_BLDG_DID          int                  not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CP_LOC_KEY           varchar(100)         not null,
   CP_CL_CD_KEY         varchar(100)         not null,
   BLDG_KEY             varchar(100)         not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   BCEG_TEXT            varchar(255)         not null,
   BCEG_CL_CD           varchar(255)         not null,
   BLDG_INC_IN_OPER_UNIT_FL char(1)              not null,
   CONF_TO_CONDO_ACTS_FL char(1)              not null,
   EQ_CL_CD             varchar(255)         not null,
   EQ_ROOF_TANK_HAZARD_FL char(1)              not null,
   EXPSR                varchar(255)         not null,
   EXTENT_OF_PROT       varchar(255)         not null,
   BURGLAR_ALARM_CR_FCTR_TEXT varchar(255)         not null,
   BURGLAR_ALARM_TYPE_CD varchar(255)         not null,
   RATING_TYPE_CD       varchar(255)         not null,
   CONSTR_TYPE_CD       varchar(255)         not null,
   BLDG_COVG_TYPE_CD    varchar(255)         not null,
   HEATING_TYPE_CD      varchar(255)         not null,
   HEATING_TYPE_OTHR    varchar(255)         not null,
   HEATING_UPDATED_YR   int                  null,
   PLUMBING_UPDATED_YR  int                  null,
   ROOFING_UPDATED_YR   int                  null,
   ROOF_TYPE_CD         varchar(255)         not null,
   INC_IN_BLNKT_FL      char(1)              not null,
   TOT_EXTR_WALLS_FACE_PCT_CD varchar(255)         not null,
   STORIES_NO_CD        varchar(255)         not null,
   OPEN_SIDES_FL        char(1)              not null,
   RENTAL_PROP_FL       char(1)              not null,
   STATE_OWNED_PROP_FL  char(1)              not null,
   SPECIFIC_GROUP_II_RATE numeric(15,6)        not null,
   SPECIFIC_GROUP_I_RATE_TEXT varchar(255)         not null,
   SPRINKLER_SYS_TEXT   varchar(255)         not null,
   BR4_SMBL_FL          char(1)              not null,
   BR4_SMBL_TEXT        varchar(1333)        not null,
   SUB_STD_COND_A_FL    char(1)              not null,
   SUB_STD_COND_B_FL    char(1)              not null,
   SUB_STD_COND_C_FL    char(1)              not null,
   SUB_STD_COND_D_FL    char(1)              not null,
   SUB_STD_COND_E_FL    char(1)              not null,
   HEATING_BY_PLANT_FL  char(1)              not null,
   WATCHMAN_SRVC_CD     varchar(255)         not null,
   WIND_CL_CD           varchar(255)         not null,
   WIND_CL_OTHR         varchar(255)         not null,
   WIRING_UPDATED_YR    int                  null,
   VACANT_BLDG_FL       char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_BLDG_DELTA
   add constraint CCBD1_PK primary key nonclustered (CP_CVRBL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CP_BLDG_DELTA
   add constraint CCBD1_AK1 unique (CP_BLDG_DID)
go

/*==============================================================*/
/* Index: CCBD_CCCB4_XFK                                        */
/*==============================================================*/




create nonclustered index CCBD_CCCB4_XFK on CS_CP_BLDG_DELTA (CP_LOC_KEY ASC)
go

/*==============================================================*/
/* Index: CCBD_CCCCB_XFK                                        */
/*==============================================================*/




create nonclustered index CCBD_CCCCB_XFK on CS_CP_BLDG_DELTA (CP_CL_CD_KEY ASC)
go

/*==============================================================*/
/* Index: CCBD_CBBA_XFK                                         */
/*==============================================================*/




create nonclustered index CCBD_CBBA_XFK on CS_CP_BLDG_DELTA (BLDG_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_BLNKT_DELTA                                     */
/*==============================================================*/
create table CS_CP_BLNKT_DELTA (
   CP_BLNKT_DID         int                  not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   BLNKT_NO             int                  null,
   BLNKT_TEXT           varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_BLNKT_DELTA
   add constraint CCBD_PK primary key nonclustered (CP_CVRBL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CP_BLNKT_DELTA
   add constraint CCBD_AK1 unique (CP_BLNKT_DID)
go

/*==============================================================*/
/* Table: CS_CP_CL_CODE_BASE                                    */
/*==============================================================*/
create table CS_CP_CL_CODE_BASE (
   CP_CL_CD_BID         int                  not null,
   CP_CL_CD_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_CL_CODE_BASE
   add constraint CCCCB_PK primary key nonclustered (CP_CL_CD_KEY)
go

alter table CS_CP_CL_CODE_BASE
   add constraint CCCCB_AK1 unique (CP_CL_CD_BID)
go

/*==============================================================*/
/* Table: CS_CP_CL_CODE_DELTA                                   */
/*==============================================================*/
create table CS_CP_CL_CODE_DELTA (
   CP_CL_CD_DID         int                  not null,
   CP_CL_CD_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   EFF_DT               DATE                 not null,
   EXP_DT               DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CL_CD                varchar(255)         not null,
   RATING_TYPE_CD       varchar(255)         not null,
   CL_TEXT              varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_CL_CODE_DELTA
   add constraint CCCCD_PK primary key nonclustered (CP_CL_CD_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_CP_CL_CODE_DELTA
   add constraint CCCCD_AK1 unique (CP_CL_CD_DID)
go

/*==============================================================*/
/* Table: CS_CP_COND_BASE                                       */
/*==============================================================*/
create table CS_CP_COND_BASE (
   CP_COND_BID          int                  not null,
   CP_COND_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   COND_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_COND_BASE
   add constraint CCCB3_PK primary key nonclustered (CP_COND_KEY)
go

alter table CS_CP_COND_BASE
   add constraint CCCB3_AK1 unique (CP_COND_BID)
go

/*==============================================================*/
/* Index: CCCB_CCBA3_XFK                                        */
/*==============================================================*/




create nonclustered index CCCB_CCBA3_XFK on CS_CP_COND_BASE (COND_KEY ASC)
go

/*==============================================================*/
/* Index: CCCB_CCCB4_XFK                                        */
/*==============================================================*/




create nonclustered index CCCB_CCCB4_XFK on CS_CP_COND_BASE (CP_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CCCB_CPBA4_XFK                                        */
/*==============================================================*/




create nonclustered index CCCB_CPBA4_XFK on CS_CP_COND_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CCCB_CPLB4_XFK                                        */
/*==============================================================*/




create nonclustered index CCCB_CPLB4_XFK on CS_CP_COND_BASE (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_COND_TERM_BASE                                  */
/*==============================================================*/
create table CS_CP_COND_TERM_BASE (
   CP_COND_TERM_BID     int                  not null,
   CP_COND_TERM_KEY     varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CP_COND_KEY          varchar(100)         not null,
   COND_TERM_KEY        varchar(100)         not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_COND_TERM_BASE
   add constraint CCCTB2_PK primary key nonclustered (CP_COND_TERM_KEY)
go

alter table CS_CP_COND_TERM_BASE
   add constraint CCCTB2_AK1 unique (CP_COND_TERM_BID)
go

/*==============================================================*/
/* Index: CCCTB_CCCB3_XFK                                       */
/*==============================================================*/




create nonclustered index CCCTB_CCCB3_XFK on CS_CP_COND_TERM_BASE (CP_COND_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_COND_TERM_DELTA                                 */
/*==============================================================*/
create table CS_CP_COND_TERM_DELTA (
   CP_COND_TERM_DID     int                  not null,
   CP_COND_TERM_KEY     varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   COND_CD              varchar(255)         not null,
   COND_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_COND_TERM_DELTA
   add constraint CCCTD2_PK primary key nonclustered (CP_COND_TERM_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CP_COND_TERM_DELTA
   add constraint CCCTD2_AK1 unique (CP_COND_TERM_DID)
go

/*==============================================================*/
/* Table: CS_CP_COVG_BASE                                       */
/*==============================================================*/
create table CS_CP_COVG_BASE (
   CP_COVG_BID          int                  not null,
   CP_COVG_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_COVG_BASE
   add constraint CCCB5_PK primary key nonclustered (CP_COVG_KEY)
go

alter table CS_CP_COVG_BASE
   add constraint CCCB5_AK1 unique (CP_COVG_BID)
go

/*==============================================================*/
/* Index: CCCB_CCBA2_XFK                                        */
/*==============================================================*/




create nonclustered index CCCB_CCBA2_XFK on CS_CP_COVG_BASE (COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CCCB_CCCB3_XFK                                        */
/*==============================================================*/




create nonclustered index CCCB_CCCB3_XFK on CS_CP_COVG_BASE (CP_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CCCB_CPBA5_XFK                                        */
/*==============================================================*/




create nonclustered index CCCB_CPBA5_XFK on CS_CP_COVG_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CCCB_CPLB3_XFK                                        */
/*==============================================================*/




create nonclustered index CCCB_CPLB3_XFK on CS_CP_COVG_BASE (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_COVG_TERM_BASE                                  */
/*==============================================================*/
create table CS_CP_COVG_TERM_BASE (
   CP_COVG_TERM_BID     int                  not null,
   CP_COVG_TERM_KEY     varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CP_COVG_KEY          varchar(100)         not null,
   COVG_TERM_KEY        varchar(100)         not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_COVG_TERM_BASE
   add constraint CCCTB3_PK primary key nonclustered (CP_COVG_TERM_KEY)
go

alter table CS_CP_COVG_TERM_BASE
   add constraint CCCTB3_AK1 unique (CP_COVG_TERM_BID)
go

/*==============================================================*/
/* Index: CCCTB_CCCB2_XFK                                       */
/*==============================================================*/




create nonclustered index CCCTB_CCCB2_XFK on CS_CP_COVG_TERM_BASE (CP_COVG_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_COVG_TERM_DELTA                                 */
/*==============================================================*/
create table CS_CP_COVG_TERM_DELTA (
   CP_COVG_TERM_DID     int                  not null,
   CP_COVG_TERM_KEY     varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   COVG_CD              varchar(255)         not null,
   COVG_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_COVG_TERM_DELTA
   add constraint CCCTD3_PK primary key nonclustered (CP_COVG_TERM_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CP_COVG_TERM_DELTA
   add constraint CCCTD3_AK1 unique (CP_COVG_TERM_DID)
go

/*==============================================================*/
/* Table: CS_CP_CVRBL_BASE                                      */
/*==============================================================*/
create table CS_CP_CVRBL_BASE (
   CP_CVRBL_BID         int                  identity,
   CP_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_CVRBL_BASE
   add constraint CCCB4_PK primary key nonclustered (CP_CVRBL_KEY)
go

alter table CS_CP_CVRBL_BASE
   add constraint CCCB4_AK1 unique (CP_CVRBL_BID)
go

/*==============================================================*/
/* Index: CCCB_CPBA3_XFK                                        */
/*==============================================================*/




create nonclustered index CCCB_CPBA3_XFK on CS_CP_CVRBL_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CCCB_CPLB5_XFK                                        */
/*==============================================================*/




create nonclustered index CCCB_CPLB5_XFK on CS_CP_CVRBL_BASE (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_EXCL_BASE                                       */
/*==============================================================*/
create table CS_CP_EXCL_BASE (
   CP_EXCL_BID          int                  not null,
   CP_EXCL_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   EXCL_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_EXCL_BASE
   add constraint CCEB1_PK primary key nonclustered (CP_EXCL_KEY)
go

alter table CS_CP_EXCL_BASE
   add constraint CCEB1_AK1 unique (CP_EXCL_BID)
go

/*==============================================================*/
/* Index: CCEB_CCCB1_XFK                                        */
/*==============================================================*/




create nonclustered index CCEB_CCCB1_XFK on CS_CP_EXCL_BASE (CP_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CCEB_CEBA1_XFK                                        */
/*==============================================================*/




create nonclustered index CCEB_CEBA1_XFK on CS_CP_EXCL_BASE (EXCL_KEY ASC)
go

/*==============================================================*/
/* Index: CCEB_CPBA1_XFK                                        */
/*==============================================================*/




create nonclustered index CCEB_CPBA1_XFK on CS_CP_EXCL_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CCEB_CPLB1_XFK                                        */
/*==============================================================*/




create nonclustered index CCEB_CPLB1_XFK on CS_CP_EXCL_BASE (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_EXCL_TERM_BASE                                  */
/*==============================================================*/
create table CS_CP_EXCL_TERM_BASE (
   CP_EXCL_TERM_BID     int                  not null,
   CP_EXCL_TERM_KEY     varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CP_EXCL_KEY          varchar(100)         not null,
   EXCL_TERM_KEY        varchar(100)         not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_EXCL_TERM_BASE
   add constraint CCETB1_PK primary key nonclustered (CP_EXCL_TERM_KEY)
go

alter table CS_CP_EXCL_TERM_BASE
   add constraint CCETB1_AK1 unique (CP_EXCL_TERM_BID)
go

/*==============================================================*/
/* Index: CCETB_CCEB1_XFK                                       */
/*==============================================================*/




create nonclustered index CCETB_CCEB1_XFK on CS_CP_EXCL_TERM_BASE (CP_EXCL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_EXCL_TERM_DELTA                                 */
/*==============================================================*/
create table CS_CP_EXCL_TERM_DELTA (
   CP_EXCL_TERM_DID     int                  not null,
   CP_EXCL_TERM_KEY     varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   EXCL_CD              varchar(255)         not null,
   EXCL_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_EXCL_TERM_DELTA
   add constraint CCETD1_PK primary key nonclustered (CP_EXCL_TERM_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CP_EXCL_TERM_DELTA
   add constraint CCETD1_AK1 unique (CP_EXCL_TERM_DID)
go

/*==============================================================*/
/* Table: CS_CP_LOC_DELTA                                       */
/*==============================================================*/
create table CS_CP_LOC_DELTA (
   CP_LOC_DID           int                  not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   LOC_KEY              varchar(100)         not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   LOC_NO               varchar(255)         not null,
   EQ_CAUSE_OF_LOSS_FORM_CD varchar(255)         not null,
   MULTI_RES_SPCL_CR_CD varchar(255)         not null,
   PROT_CL              varchar(255)         not null,
   FLOOD_COVG_FL        char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_LOC_DELTA
   add constraint CCLD_PK primary key nonclustered (CP_CVRBL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CP_LOC_DELTA
   add constraint CCLD_AK1 unique (CP_LOC_DID)
go

/*==============================================================*/
/* Index: CCLD_CLBA_XFK                                         */
/*==============================================================*/




create nonclustered index CCLD_CLBA_XFK on CS_CP_LOC_DELTA (LOC_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_MOD_BASE                                        */
/*==============================================================*/
create table CS_CP_MOD_BASE (
   CP_MOD_BID           int                  not null,
   CP_MOD_KEY           varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   MOD_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_MOD_BASE
   add constraint CCMB1_PK primary key nonclustered (CP_MOD_KEY)
go

alter table CS_CP_MOD_BASE
   add constraint CCMB1_AK1 unique (CP_MOD_BID)
go

/*==============================================================*/
/* Index: CCMB_CCCB1_XFK                                        */
/*==============================================================*/




create nonclustered index CCMB_CCCB1_XFK on CS_CP_MOD_BASE (CP_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CCMB_CMBA1_XFK                                        */
/*==============================================================*/




create nonclustered index CCMB_CMBA1_XFK on CS_CP_MOD_BASE (MOD_KEY ASC)
go

/*==============================================================*/
/* Index: CCMB_CPBA1_XFK                                        */
/*==============================================================*/




create nonclustered index CCMB_CPBA1_XFK on CS_CP_MOD_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CCMB_CPLB1_XFK                                        */
/*==============================================================*/




create nonclustered index CCMB_CPLB1_XFK on CS_CP_MOD_BASE (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_MOD_DELTA                                       */
/*==============================================================*/
create table CS_CP_MOD_DELTA (
   CP_MOD_DID           int                  not null,
   CP_MOD_KEY           varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   MOD_CD               varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   MOD_VAL_TEXT         varchar(255)         not null,
   JUST_TEXT            varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_MOD_DELTA
   add constraint CCMD1_PK primary key nonclustered (CP_MOD_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CP_MOD_DELTA
   add constraint CCMD1_AK1 unique (CP_MOD_DID)
go

/*==============================================================*/
/* Table: CS_CP_MOD_RF_BASE                                     */
/*==============================================================*/
create table CS_CP_MOD_RF_BASE (
   CP_MOD_RF_BID        int                  not null,
   CP_MOD_RF_KEY        varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   CP_MOD_KEY           varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_MOD_RF_BASE
   add constraint CCMRB1_PK primary key nonclustered (CP_MOD_RF_KEY)
go

alter table CS_CP_MOD_RF_BASE
   add constraint CCMRB1_AK1 unique (CP_MOD_RF_BID)
go

/*==============================================================*/
/* Index: CCMRB_CCMB1_XFK                                       */
/*==============================================================*/




create nonclustered index CCMRB_CCMB1_XFK on CS_CP_MOD_RF_BASE (CP_MOD_KEY ASC)
go

/*==============================================================*/
/* Index: CCMRB_CPBA_XFK                                        */
/*==============================================================*/




create nonclustered index CCMRB_CPBA_XFK on CS_CP_MOD_RF_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_MOD_RF_DELTA                                    */
/*==============================================================*/
create table CS_CP_MOD_RF_DELTA (
   CP_MOD_RF_DID        int                  not null,
   CP_MOD_RF_KEY        varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   RF_CD                varchar(255)         not null,
   ASSESS_RATE          numeric(18,4)        not null,
   JUST_TEXT            varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_MOD_RF_DELTA
   add constraint CCMRD1_PK primary key nonclustered (CP_MOD_RF_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CP_MOD_RF_DELTA
   add constraint CCMRD1_AK1 unique (CP_MOD_RF_DID)
go

/*==============================================================*/
/* Table: CS_CP_OCC_CL_DELTA                                    */
/*==============================================================*/
create table CS_CP_OCC_CL_DELTA (
   CP_OCC_CL_DID        int                  not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CP_BLDG_KEY          varchar(100)         not null,
   CP_CL_CD_KEY         varchar(100)         not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   OCCPNCY_NO           int                  null,
   OCCPNCY_CATG_CD      varchar(255)         not null,
   AREA_SF_NO           int                  null,
   COVG_FORM_CD         varchar(255)         not null,
   SPRINKLER_SYS_TEXT   varchar(255)         not null,
   OCC_CL_TEXT          varchar(1333)        not null,
   CONF_TO_CONDO_ACTS_FL char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_OCC_CL_DELTA
   add constraint CCOCD_PK primary key nonclustered (CP_CVRBL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CP_OCC_CL_DELTA
   add constraint CCOCD_AK1 unique (CP_OCC_CL_DID)
go

/*==============================================================*/
/* Index: CCOCD_CCCB1_XFK                                       */
/*==============================================================*/




create nonclustered index CCOCD_CCCB1_XFK on CS_CP_OCC_CL_DELTA (CP_BLDG_KEY ASC)
go

/*==============================================================*/
/* Index: CCOCD_CCCCB_XFK                                       */
/*==============================================================*/




create nonclustered index CCOCD_CCCCB_XFK on CS_CP_OCC_CL_DELTA (CP_CL_CD_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_POL_LINE_DELTA                                  */
/*==============================================================*/
create table CS_CP_POL_LINE_DELTA (
   CP_POL_LINE_DID      int                  not null,
   POL_LINE_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BLKT_RATED_FL        char(1)              not null,
   CP_POL_TYPE_CD       varchar(255)         not null,
   COVG_CERT_TISM_TEXT  varchar(255)         not null,
   COVG_OTHR_TISM_TEXT  varchar(255)         not null,
   EQ_SUB_LMT_BLNKT_FL  char(1)              not null,
   FLOOD_COVG_BLNKT_FL  char(1)              not null,
   FUNG_WETDRYROT_BACT_COVG_TEXT varchar(255)         not null,
   IRPM_APPLIES_FL      char(1)              not null,
   MULTI_PREM_DISP_CR_FL char(1)              not null,
   MULTI_PREM_DISP_CR_TEXT varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_POL_LINE_DELTA
   add constraint CCPLD1_PK primary key nonclustered (POL_LINE_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CP_POL_LINE_DELTA
   add constraint CCPLD1_AK1 unique (CP_POL_LINE_DID)
go

/*==============================================================*/
/* Table: CS_CP_PP_DELTA                                        */
/*==============================================================*/
create table CS_CP_PP_DELTA (
   CP_PERS_PROP_DID     int                  not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   CP_OCC_CL_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   PERS_PROP_COVG_TYPE_CD varchar(255)         not null,
   INC_IN_BLNKT_FL      char(1)              not null,
   YARD_PROP_INDC_TEXT  varchar(255)         not null,
   SPECIFIC_GROUP_I_RATE_TEXT varchar(255)         not null,
   STATE_OWNED_PROP_FL  char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_PP_DELTA
   add constraint CCPD_PK primary key nonclustered (CP_CVRBL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CP_PP_DELTA
   add constraint CCPD_AK1 unique (CP_PERS_PROP_DID)
go

/*==============================================================*/
/* Index: CCPD_CCCB1_XFK                                        */
/*==============================================================*/




create nonclustered index CCPD_CCCB1_XFK on CS_CP_PP_DELTA (CP_OCC_CL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_PREM_TRAN                                       */
/*==============================================================*/
create table CS_CP_PREM_TRAN (
   CP_PREM_TRANS_TID    int                  identity,
   CP_PREM_TRANS_KEY    varchar(100)         not null,
   AGCY_KEY             varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   CP_LOC_LOC_KEY       varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   CP_COVG_KEY          varchar(100)         not null,
   REC_TYPE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ACCTG_PRD_ID         int                  not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ORIG_TRANS_PROC_DTS  DATETIME2            not null,
   ORIG_END_EFF_DT      DATE                 not null,
   TRANS_EFF_DT         DATE                 not null,
   TRANS_EXP_DT         DATE                 not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   PREM_BASIS_AMT       numeric(18,2)        not null,
   STD_BASE_RATE        numeric(18,4)        not null,
   STD_ADJ_RATE         numeric(18,4)        not null,
   STD_AMT              numeric(18,2)        not null,
   STD_TERM_AMT         numeric(18,2)        not null,
   OVERRIDE_BASE_RATE   numeric(18,4)        not null,
   OVERRIDE_ADJ_RATE    numeric(18,4)        not null,
   OVERRIDE_AMT         numeric(18,2)        not null,
   OVERRIDE_TERM_AMT    numeric(18,2)        not null,
   ACT_BASE_RATE        numeric(18,4)        not null,
   ACT_ADJ_RATE         numeric(18,4)        not null,
   ACT_AMT              numeric(18,2)        not null,
   ACT_TERM_AMT         numeric(18,2)        not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   STTLMNT_PREM_BASIS_AMT numeric(18,2)        null,
   STTLMNT_STD_AMT      numeric(18,2)        null,
   STTLMNT_STD_TERM_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   STTLMNT_ACT_AMT      numeric(18,2)        null,
   STTLMNT_ACT_TERM_AMT numeric(18,2)        null,
   STTLMNT_CURR_CD      varchar(255)         not null,
   STTLMNT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_STTLMNT_RATE numeric(15,10)       null,
   REG_RPT_PREM_BASIS_AMT numeric(18,2)        null,
   REG_RPT_STD_AMT      numeric(18,2)        null,
   REG_RPT_STD_TERM_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   REG_RPT_ACT_AMT      numeric(18,2)        null,
   REG_RPT_ACT_TERM_AMT numeric(18,2)        null,
   REG_RPT_CURR_CD      varchar(255)         not null,
   REG_RPT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_REG_RPT_RATE numeric(15,10)       null,
   RPT_PREM_BASIS_AMT   numeric(18,2)        null,
   RPT_STD_AMT          numeric(18,2)        null,
   RPT_STD_TERM_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   RPT_ACT_AMT          numeric(18,2)        null,
   RPT_ACT_TERM_AMT     numeric(18,2)        null,
   RPT_CURR_CD          varchar(255)         not null,
   RPT_TRANS_AMT        numeric(18,2)        not null,
   TRANS_TO_RPT_RATE    numeric(15,10)       not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   COMM_PCT             numeric(6,3)         not null,
   EST_PREM_AMT         numeric(18,2)        not null,
   RPT_PREM_AMT         numeric(18,2)        not null,
   AUDIT_PREM_AMT       numeric(18,2)        not null,
   AUDITABLE_FL         char(1)              not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_PREM_TRAN
   add constraint CCPT1_PK primary key nonclustered (CP_PREM_TRANS_TID)
go

alter table CS_CP_PREM_TRAN
   add constraint CCPT1_AK1 unique (CP_PREM_TRANS_KEY)
go

/*==============================================================*/
/* Index: CCPT_CABA2_XFK                                        */
/*==============================================================*/




create nonclustered index CCPT_CABA2_XFK on CS_CP_PREM_TRAN (AGCY_KEY ASC)
go

/*==============================================================*/
/* Index: CCPT_CABA3_XFK                                        */
/*==============================================================*/




create nonclustered index CCPT_CABA3_XFK on CS_CP_PREM_TRAN (AGNT_KEY ASC)
go

/*==============================================================*/
/* Index: CCPT_CCBA1_XFK                                        */
/*==============================================================*/




create nonclustered index CCPT_CCBA1_XFK on CS_CP_PREM_TRAN (COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CCPT_CCCB2_XFK                                        */
/*==============================================================*/




create nonclustered index CCPT_CCCB2_XFK on CS_CP_PREM_TRAN (CP_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CCPT_CCCB3_XFK                                        */
/*==============================================================*/




create nonclustered index CCPT_CCCB3_XFK on CS_CP_PREM_TRAN (CP_COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CCPT_CLBA_XFK                                         */
/*==============================================================*/




create nonclustered index CCPT_CLBA_XFK on CS_CP_PREM_TRAN (CP_LOC_LOC_KEY ASC)
go

/*==============================================================*/
/* Index: CCPT_CPBA1_XFK                                        */
/*==============================================================*/




create nonclustered index CCPT_CPBA1_XFK on CS_CP_PREM_TRAN (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CCPT_CPLB1_XFK                                        */
/*==============================================================*/




create nonclustered index CCPT_CPLB1_XFK on CS_CP_PREM_TRAN (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_SI_DELTA                                        */
/*==============================================================*/
create table CS_CP_SI_DELTA (
   CP_SI_DID            int                  not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CP_OWNING_CVRBL_KEY  varchar(100)         not null,
   CP_COVG_KEY          varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   CP_ADDL_INTRST_KEY   varchar(100)         not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   SCHED_NO             int                  null,
   TYPE_KEY_COL_1       varchar(255)         not null,
   TYPE_KEY_COL_2       varchar(255)         not null,
   TYPE_KEY_COL_3       varchar(255)         not null,
   STR_COL_1            varchar(255)         not null,
   STR_COL_2            varchar(255)         not null,
   STR_COL_3            varchar(255)         not null,
   DT_COL_1_DTS         DATETIME2            null,
   DT_COL_2_DTS         DATETIME2            null,
   INT_COL_1            int                  null,
   POS_INT_COL_1        int                  null,
   DECIMAL_COL_1        numeric(18,4)        null,
   DECIMAL_COL_2        numeric(18,4)        null,
   DECIMAL_COL_3        numeric(18,4)        null,
   LONG_STR_COL_1       varchar(1333)        not null,
   LONG_STR_COL_2       varchar(1333)        not null,
   LONG_STR_COL_3       varchar(1333)        not null,
   BOOL_COL_1_FL        char(1)              not null,
   BOOL_COL_2_FL        char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_SI_DELTA
   add constraint CCSD2_PK primary key nonclustered (CP_CVRBL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CP_SI_DELTA
   add constraint CCSD2_AK1 unique (CP_SI_DID)
go

/*==============================================================*/
/* Index: CCSD_CCAIB1_XFK                                       */
/*==============================================================*/




create nonclustered index CCSD_CCAIB1_XFK on CS_CP_SI_DELTA (CP_ADDL_INTRST_KEY ASC)
go

/*==============================================================*/
/* Index: CCSD_CCCB3_XFK                                        */
/*==============================================================*/




create nonclustered index CCSD_CCCB3_XFK on CS_CP_SI_DELTA (CP_COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CCSD_CCCB4_XFK                                        */
/*==============================================================*/




create nonclustered index CCSD_CCCB4_XFK on CS_CP_SI_DELTA (CP_OWNING_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CCSD_CLBA1_XFK                                        */
/*==============================================================*/




create nonclustered index CCSD_CLBA1_XFK on CS_CP_SI_DELTA (LOC_KEY ASC)
go

/*==============================================================*/
/* Index: CCSD_CPPB1_XFK                                        */
/*==============================================================*/




create nonclustered index CCSD_CPPB1_XFK on CS_CP_SI_DELTA (POL_PARTY_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_SPCL_CL_BI_DELTA                                */
/*==============================================================*/
create table CS_CP_SPCL_CL_BI_DELTA (
   CP_SPCL_CL_BUS_INC_DID int                  not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CP_SPCL_CL_KEY       varchar(100)         not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   INC_IN_BLNKT_FL      char(1)              not null,
   BUS_INC_COVG_TYPE_CD varchar(255)         not null,
   RISK_EDU_INST_PCT    varchar(255)         not null,
   RISK_MERC_PCT        varchar(255)         not null,
   RISK_RENTAL_PCT      varchar(255)         not null,
   RISK_MFG_PCT         varchar(255)         not null,
   TYPE_OF_RISK_TEXT    varchar(255)         not null,
   WATERFRONT_CONSTR_TEXT varchar(255)         not null,
   WATERFRONT_PROP_FL   char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_SPCL_CL_BI_DELTA
   add constraint CCSCBD_PK primary key nonclustered (CP_CVRBL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CP_SPCL_CL_BI_DELTA
   add constraint CCSCBD_AK1 unique (CP_SPCL_CL_BUS_INC_DID)
go

/*==============================================================*/
/* Index: CCSCBD_CCCB1_XFK                                      */
/*==============================================================*/




create nonclustered index CCSCBD_CCCB1_XFK on CS_CP_SPCL_CL_BI_DELTA (CP_SPCL_CL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_SPCL_CL_DELTA                                   */
/*==============================================================*/
create table CS_CP_SPCL_CL_DELTA (
   CP_SPCL_CL_DID       int                  not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CP_LOC_KEY           varchar(100)         not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   CP_SPCL_CL_TEXT      varchar(255)         not null,
   CONF_TO_CONDO_ACTS_FL char(1)              not null,
   CP_SPCL_CL_COVG_FORM_CD varchar(255)         not null,
   EQ_CL_CD             varchar(255)         not null,
   EQ_STRUC_TEXT        varchar(255)         not null,
   EXPSR                varchar(255)         not null,
   INC_IN_BLNKT_FL      char(1)              not null,
   RATING_TYPE_CD       varchar(255)         not null,
   RENTAL_PROP_FL       char(1)              not null,
   SPCL_CL_NO           int                  null,
   SPCL_CL_TEXT         varchar(255)         not null,
   BUILD_YR             int                  null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_SPCL_CL_DELTA
   add constraint CCSCD_PK primary key nonclustered (CP_CVRBL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CP_SPCL_CL_DELTA
   add constraint CCSCD_AK1 unique (CP_SPCL_CL_DID)
go

/*==============================================================*/
/* Index: CCSCD_CCCB1_XFK                                       */
/*==============================================================*/




create nonclustered index CCSCD_CCCB1_XFK on CS_CP_SPCL_CL_DELTA (CP_LOC_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_TERR_CODE_BASE                                  */
/*==============================================================*/
create table CS_CP_TERR_CODE_BASE (
   CP_TERR_CD_BID       int                  not null,
   CP_TERR_CD_KEY       varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   CP_LOC_KEY           varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_TERR_CODE_BASE
   add constraint CCTCB_PK primary key nonclustered (CP_TERR_CD_KEY)
go

alter table CS_CP_TERR_CODE_BASE
   add constraint CCTCB_AK1 unique (CP_TERR_CD_BID)
go

/*==============================================================*/
/* Index: CCTCB_CCCB_XFK                                        */
/*==============================================================*/




create nonclustered index CCTCB_CCCB_XFK on CS_CP_TERR_CODE_BASE (CP_LOC_KEY ASC)
go

/*==============================================================*/
/* Index: CCTCB_CPBA_XFK                                        */
/*==============================================================*/




create nonclustered index CCTCB_CPBA_XFK on CS_CP_TERR_CODE_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_CP_TERR_CODE_DELTA                                 */
/*==============================================================*/
create table CS_CP_TERR_CODE_DELTA (
   CP_TERR_CD_DID       int                  not null,
   CP_TERR_CD_KEY       varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   TERR_TEXT            varchar(255)         not null,
   TERR_CAUSE_OF_LOSS_CD varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CP_TERR_CODE_DELTA
   add constraint CCTCD_PK primary key nonclustered (CP_TERR_CD_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_CP_TERR_CODE_DELTA
   add constraint CCTCD_AK1 unique (CP_TERR_CD_DID)
go

/*==============================================================*/
/* Table: CS_CUST_ACCT_BASE                                     */
/*==============================================================*/
create table CS_CUST_ACCT_BASE (
   ACCT_BASE_ID         int                  not null,
   ACCT_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ACCT_NO              varchar(255)         not null,
   ACCT_NAME            varchar(255)         not null,
   MAILING_ADDR_1       varchar(255)         not null,
   MAILING_ADDR_2       varchar(255)         not null,
   MAILING_ADDR_3       varchar(255)         not null,
   MAILING_CITY         varchar(255)         not null,
   MAILING_STATE_CD     varchar(255)         not null,
   MAILING_ZIP_CD       varchar(15)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CUST_ACCT_BASE
   add constraint CCAB_PK primary key nonclustered (ACCT_KEY)
go

alter table CS_CUST_ACCT_BASE
   add constraint CCAB_AK1 unique (ACCT_BASE_ID)
go

/*==============================================================*/
/* Table: CS_CUST_ACCT_DELTA                                    */
/*==============================================================*/
create table CS_CUST_ACCT_DELTA (
   ACCT_ID              int                  not null,
   ACCT_KEY             varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_CUST_ACCT_DELTA
   add constraint CCAD_PK primary key nonclustered (ACCT_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_CUST_ACCT_DELTA
   add constraint CCAD_AK1 unique (ACCT_ID)
go

/*==============================================================*/
/* Table: CS_DATE_DIM                                           */
/*==============================================================*/
create table CS_DATE_DIM (
   DT_ID                int                  not null,
   DT_VAL               DATE                 not null,
   YR_NO                int                  not null,
   MTH_ID               int                  not null,
   QTR_ID               int                  not null,
   MTH_NO               char(2)              not null,
   WEEK_NO              char(2)              not null,
   QTR_NO               char(1)              not null,
   DAY_OF_YR_NO         char(3)              not null,
   DAY_OF_MTH_NO        char(2)              not null,
   DAY_OF_WEEK_NO       char(1)              not null,
   MTH_BEGIN_DT         DATE                 not null,
   MTH_END_DT           DATE                 not null,
   QTR_FIRST_MTH_NO     char(2)              not null,
   QTR_BEGIN_DT         DATE                 not null,
   QTR_END_DT           DATE                 not null,
   YR_BEGIN_DT          DATE                 not null,
   YR_END_DT            DATE                 not null,
   MTH_NAME             varchar(10)          not null,
   DAY_NAME             varchar(255)         not null
)
go

alter table CS_DATE_DIM
   add constraint CDDI_PK primary key nonclustered (DT_ID)
go

alter table CS_DATE_DIM
   add constraint CDDI_AK1 unique (DT_VAL)
go

/*==============================================================*/
/* Table: CS_EARNED_PREM_TRAN                                   */
/*==============================================================*/
create table CS_EARNED_PREM_TRAN (
   EARNED_PREM_TRANS_ID int                  not null,
   RPT_YR               int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   POL_KEY              varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   RISK_KEY             varchar(100)         not null,
   RISK_TYPE_CD         varchar(255)         not null,
   COVG_ID              int                  not null,
   ACCTG_PRD_ID         int                  not null,
   REC_TYPE_KEY         varchar(100)         not null,
   AGCY_KEY             varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ORIG_TRANS_PROC_DTS  DATETIME2            not null,
   ORIG_END_EFF_DT      DATE                 not null,
   TRANS_EFF_DT         DATE                 not null,
   TRANS_EXP_DT         DATE                 not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   UNEARNED_PRIOR_TO_JAN numeric(18,2)        not null,
   UNEARNED_JAN         numeric(18,2)        not null,
   UNEARNED_FEB         numeric(18,2)        not null,
   UNEARNED_MAR         numeric(18,2)        not null,
   UNEARNED_APR         numeric(18,2)        not null,
   UNEARNED_MAY         numeric(18,2)        not null,
   UNEARNED_JUN         numeric(18,2)        not null,
   UNEARNED_JUL         numeric(18,2)        not null,
   UNEARNED_AUG         numeric(18,2)        not null,
   UNEARNED_SEP         numeric(18,2)        not null,
   UNEARNED_OCT         numeric(18,2)        not null,
   UNEARNED_NOV         numeric(18,2)        not null,
   UNEARNED_DEC         numeric(18,2)        not null,
   WRITTEN_EXPSR_DAYS   int                  not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_EARNED_PREM_TRAN
   add constraint CEPT_PK primary key nonclustered (EARNED_PREM_TRANS_ID)
go

/*==============================================================*/
/* Index: CEPT_CRTD_XFK                                         */
/*==============================================================*/




create nonclustered index CEPT_CRTD_XFK on CS_EARNED_PREM_TRAN (REC_TYPE_KEY ASC)
go

/*==============================================================*/
/* Index: CEPT_CMDI_XFK                                         */
/*==============================================================*/




create nonclustered index CEPT_CMDI_XFK on CS_EARNED_PREM_TRAN (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: CEPT_CABA_XFK                                         */
/*==============================================================*/




create nonclustered index CEPT_CABA_XFK on CS_EARNED_PREM_TRAN (AGCY_KEY ASC)
go

/*==============================================================*/
/* Index: CEPT_CABA1_XFK                                        */
/*==============================================================*/




create nonclustered index CEPT_CABA1_XFK on CS_EARNED_PREM_TRAN (AGNT_KEY ASC)
go

/*==============================================================*/
/* Table: CS_EXCL_BASE                                          */
/*==============================================================*/
create table CS_EXCL_BASE (
   EXCL_BID             int                  not null,
   EXCL_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_EXCL_BASE
   add constraint CEBA_PK primary key nonclustered (EXCL_KEY)
go

alter table CS_EXCL_BASE
   add constraint CEBA_AK1 unique (EXCL_BID)
go

/*==============================================================*/
/* Table: CS_EXCL_DELTA                                         */
/*==============================================================*/
create table CS_EXCL_DELTA (
   EXCL_DID             int                  not null,
   EXCL_KEY             varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   EXCL_CD              varchar(255)         not null,
   CATG_CD              varchar(255)         not null,
   EXCL_TEXT            varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_EXCL_DELTA
   add constraint CEDE_PK primary key nonclustered (EXCL_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_EXCL_DELTA
   add constraint CEDE_AK1 unique (EXCL_DID)
go

/*==============================================================*/
/* Table: CS_EX_RATE_BASE                                       */
/*==============================================================*/
create table CS_EX_RATE_BASE (
   EX_RATE_BID          int                  identity,
   EX_RATE_KEY          varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   EFF_DTS              DATETIME2            not null,
   EXP_DTS              DATETIME2            not null,
   BASE_CURR_CD         varchar(255)         not null,
   PRICE_CURR_CD        varchar(255)         not null,
   RATE_SCALE           int                  not null,
   NORMALIZED_RATE      numeric(15,10)       not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_EX_RATE_BASE
   add constraint CERB_PK primary key nonclustered (EX_RATE_KEY)
go

alter table CS_EX_RATE_BASE
   add constraint CERB_AK1 unique (EX_RATE_BID)
go

/*==============================================================*/
/* Table: CS_FORM_BASE                                          */
/*==============================================================*/
create table CS_FORM_BASE (
   FORM_BID             int                  not null,
   FORM_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_FORM_BASE
   add constraint CFBA_PK primary key nonclustered (FORM_KEY)
go

alter table CS_FORM_BASE
   add constraint CFBA_AK1 unique (FORM_BID)
go

/*==============================================================*/
/* Index: CFBA_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CFBA_CPBA_XFK on CS_FORM_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_FORM_DELTA                                         */
/*==============================================================*/
create table CS_FORM_DELTA (
   FORM_DID             int                  not null,
   FORM_KEY             varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   EFF_DT               DATE                 not null,
   EXP_DT               DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   FORM_PATTERN_CD      varchar(255)         not null,
   FORM_NO              varchar(255)         not null,
   FORM_TEXT            varchar(255)         not null,
   END_NO               int                  null,
   EDITION              varchar(255)         not null,
   GROUP_CD             varchar(255)         not null,
   REMOVED_SUPERSEDED_FL char(1)              not null,
   INFERENCE_TM_CD      varchar(255)         not null,
   FORM_REMOVAL_DT      DATETIME2            null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_FORM_DELTA
   add constraint CFDE_PK primary key nonclustered (FORM_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_FORM_DELTA
   add constraint CFDE_AK1 unique (FORM_DID)
go

/*==============================================================*/
/* Table: CS_GL_CL_CODE_BASE                                    */
/*==============================================================*/
create table CS_GL_CL_CODE_BASE (
   GL_CL_CD_BID         int                  not null,
   GL_CL_CD_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_CL_CODE_BASE
   add constraint CGCCB_PK primary key nonclustered (GL_CL_CD_KEY)
go

alter table CS_GL_CL_CODE_BASE
   add constraint CGCCB_AK1 unique (GL_CL_CD_BID)
go

/*==============================================================*/
/* Table: CS_GL_CL_CODE_DELTA                                   */
/*==============================================================*/
create table CS_GL_CL_CODE_DELTA (
   GL_CL_CD_DID         int                  not null,
   GL_CL_CD_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   EFF_DT               DATE                 not null,
   EXP_DT               DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CD_CLASS             varchar(1333)        not null,
   LINE_CL_TEXT         varchar(255)         not null,
   COVG_PART_TYPE_CD    varchar(255)         not null,
   SUBLINE_RATED_INDIV_FL char(1)              not null,
   BASIS_CD             varchar(255)         not null,
   BASIS_TEXT           varchar(1333)        not null,
   BASIS_RF             numeric(18,4)        null,
   BASIS_VARIABLE_FL    char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_CL_CODE_DELTA
   add constraint CGCCD_PK primary key nonclustered (GL_CL_CD_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_GL_CL_CODE_DELTA
   add constraint CGCCD_AK1 unique (GL_CL_CD_DID)
go

/*==============================================================*/
/* Table: CS_GL_COND_BASE                                       */
/*==============================================================*/
create table CS_GL_COND_BASE (
   GL_COND_BID          int                  not null,
   GL_COND_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   COND_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_COND_BASE
   add constraint CGCB2_PK primary key nonclustered (GL_COND_KEY)
go

alter table CS_GL_COND_BASE
   add constraint CGCB2_AK1 unique (GL_COND_BID)
go

/*==============================================================*/
/* Index: CGCB_CCBA1_XFK                                        */
/*==============================================================*/




create nonclustered index CGCB_CCBA1_XFK on CS_GL_COND_BASE (COND_KEY ASC)
go

/*==============================================================*/
/* Index: CGCB_CGCB1_XFK                                        */
/*==============================================================*/




create nonclustered index CGCB_CGCB1_XFK on CS_GL_COND_BASE (GL_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CGCB_CPBA2_XFK                                        */
/*==============================================================*/




create nonclustered index CGCB_CPBA2_XFK on CS_GL_COND_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CGCB_CPLB2_XFK                                        */
/*==============================================================*/




create nonclustered index CGCB_CPLB2_XFK on CS_GL_COND_BASE (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_GL_COND_TERM_BASE                                  */
/*==============================================================*/
create table CS_GL_COND_TERM_BASE (
   GL_COND_TERM_BID     int                  not null,
   GL_COND_TERM_KEY     varchar(100)         not null,
   GL_COND_KEY          varchar(100)         not null,
   COND_TERM_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_COND_TERM_BASE
   add constraint CGCTB1_PK primary key nonclustered (GL_COND_TERM_KEY)
go

alter table CS_GL_COND_TERM_BASE
   add constraint CGCTB1_AK1 unique (GL_COND_TERM_BID)
go

/*==============================================================*/
/* Index: CGCTB_CGCB1_XFK                                       */
/*==============================================================*/




create nonclustered index CGCTB_CGCB1_XFK on CS_GL_COND_TERM_BASE (GL_COND_KEY ASC)
go

/*==============================================================*/
/* Table: CS_GL_COND_TERM_DELTA                                 */
/*==============================================================*/
create table CS_GL_COND_TERM_DELTA (
   GL_COND_TERM_DID     int                  not null,
   GL_COND_TERM_KEY     varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   COND_CD              varchar(255)         not null,
   COND_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_COND_TERM_DELTA
   add constraint CGCTD1_PK primary key nonclustered (GL_COND_TERM_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_GL_COND_TERM_DELTA
   add constraint CGCTD1_AK1 unique (GL_COND_TERM_DID)
go

/*==============================================================*/
/* Table: CS_GL_COVG_BASE                                       */
/*==============================================================*/
create table CS_GL_COVG_BASE (
   GL_COVG_BID          int                  not null,
   GL_COVG_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_COVG_BASE
   add constraint CGCB1_PK primary key nonclustered (GL_COVG_KEY)
go

alter table CS_GL_COVG_BASE
   add constraint CGCB1_AK1 unique (GL_COVG_BID)
go

/*==============================================================*/
/* Index: CGCB_CCBA_XFK                                         */
/*==============================================================*/




create nonclustered index CGCB_CCBA_XFK on CS_GL_COVG_BASE (COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CGCB_CGCB_XFK                                         */
/*==============================================================*/




create nonclustered index CGCB_CGCB_XFK on CS_GL_COVG_BASE (GL_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CGCB_CPBA1_XFK                                        */
/*==============================================================*/




create nonclustered index CGCB_CPBA1_XFK on CS_GL_COVG_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CGCB_CPLB1_XFK                                        */
/*==============================================================*/




create nonclustered index CGCB_CPLB1_XFK on CS_GL_COVG_BASE (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_GL_COVG_PART_DELTA                                 */
/*==============================================================*/
create table CS_GL_COVG_PART_DELTA (
   GL_COVG_PART_DID     int                  not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   CLAIMS_ORIG_EFF_DT   DATETIME2            null,
   COVG_FORM_CD         varchar(255)         not null,
   COVG_PART_TYPE_CD    varchar(255)         not null,
   LOC_LIMITS_FL        char(1)              not null,
   POLLUTION_CLEANUP_EXP_FL char(1)              not null,
   CLAIMS_RETROACTIVE_DT DATETIME2            null,
   SPLIT_BIPD_DED_FL    char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_COVG_PART_DELTA
   add constraint CGCPD_PK primary key nonclustered (GL_CVRBL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_GL_COVG_PART_DELTA
   add constraint CGCPD_AK1 unique (GL_COVG_PART_DID)
go

/*==============================================================*/
/* Table: CS_GL_COVG_TERM_BASE                                  */
/*==============================================================*/
create table CS_GL_COVG_TERM_BASE (
   GL_COVG_TERM_BID     int                  not null,
   GL_COVG_TERM_KEY     varchar(100)         not null,
   GL_COVG_KEY          varchar(100)         not null,
   COVG_TERM_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_COVG_TERM_BASE
   add constraint CGCTB_PK primary key nonclustered (GL_COVG_TERM_KEY)
go

alter table CS_GL_COVG_TERM_BASE
   add constraint CGCTB_AK1 unique (GL_COVG_TERM_BID)
go

/*==============================================================*/
/* Index: CGCTB_CGCB_XFK                                        */
/*==============================================================*/




create nonclustered index CGCTB_CGCB_XFK on CS_GL_COVG_TERM_BASE (GL_COVG_KEY ASC)
go

/*==============================================================*/
/* Table: CS_GL_COVG_TERM_DELTA                                 */
/*==============================================================*/
create table CS_GL_COVG_TERM_DELTA (
   GL_COVG_TERM_DID     int                  not null,
   GL_COVG_TERM_KEY     varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   COVG_CD              varchar(255)         not null,
   COVG_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_COVG_TERM_DELTA
   add constraint CGCTD_PK primary key nonclustered (GL_COVG_TERM_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_GL_COVG_TERM_DELTA
   add constraint CGCTD_AK1 unique (GL_COVG_TERM_DID)
go

/*==============================================================*/
/* Table: CS_GL_CVRBL_BASE                                      */
/*==============================================================*/
create table CS_GL_CVRBL_BASE (
   GL_CVRBL_BID         int                  identity,
   GL_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_CVRBL_BASE
   add constraint CGCB_PK primary key nonclustered (GL_CVRBL_KEY)
go

alter table CS_GL_CVRBL_BASE
   add constraint CGCB_AK1 unique (GL_CVRBL_BID)
go

/*==============================================================*/
/* Index: CGCB_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CGCB_CPBA_XFK on CS_GL_CVRBL_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CGCB_CPLB_XFK                                         */
/*==============================================================*/




create nonclustered index CGCB_CPLB_XFK on CS_GL_CVRBL_BASE (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_GL_EXCL_BASE                                       */
/*==============================================================*/
create table CS_GL_EXCL_BASE (
   GL_EXCL_BID          int                  not null,
   GL_EXCL_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   EXCL_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_EXCL_BASE
   add constraint CGEB_PK primary key nonclustered (GL_EXCL_KEY)
go

alter table CS_GL_EXCL_BASE
   add constraint CGEB_AK1 unique (GL_EXCL_BID)
go

/*==============================================================*/
/* Index: CGEB_CEBA_XFK                                         */
/*==============================================================*/




create nonclustered index CGEB_CEBA_XFK on CS_GL_EXCL_BASE (EXCL_KEY ASC)
go

/*==============================================================*/
/* Index: CGEB_CGCB_XFK                                         */
/*==============================================================*/




create nonclustered index CGEB_CGCB_XFK on CS_GL_EXCL_BASE (GL_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CGEB_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CGEB_CPBA_XFK on CS_GL_EXCL_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CGEB_CPLB_XFK                                         */
/*==============================================================*/




create nonclustered index CGEB_CPLB_XFK on CS_GL_EXCL_BASE (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_GL_EXCL_TERM_BASE                                  */
/*==============================================================*/
create table CS_GL_EXCL_TERM_BASE (
   GL_EXCL_TERM_BID     int                  not null,
   GL_EXCL_TERM_KEY     varchar(100)         not null,
   GL_EXCL_KEY          varchar(100)         not null,
   EXCL_TERM_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_EXCL_TERM_BASE
   add constraint CGETB_PK primary key nonclustered (GL_EXCL_TERM_KEY)
go

alter table CS_GL_EXCL_TERM_BASE
   add constraint CGETB_AK1 unique (GL_EXCL_TERM_BID)
go

/*==============================================================*/
/* Index: CGETB_CGEB_XFK                                        */
/*==============================================================*/




create nonclustered index CGETB_CGEB_XFK on CS_GL_EXCL_TERM_BASE (GL_EXCL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_GL_EXCL_TERM_DELTA                                 */
/*==============================================================*/
create table CS_GL_EXCL_TERM_DELTA (
   GL_EXCL_TERM_DID     int                  not null,
   GL_EXCL_TERM_KEY     varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   EXCL_CD              varchar(255)         not null,
   EXCL_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_EXCL_TERM_DELTA
   add constraint CGETD_PK primary key nonclustered (GL_EXCL_TERM_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_GL_EXCL_TERM_DELTA
   add constraint CGETD_AK1 unique (GL_EXCL_TERM_DID)
go

/*==============================================================*/
/* Table: CS_GL_EXPSR_DELTA                                     */
/*==============================================================*/
create table CS_GL_EXPSR_DELTA (
   GL_EXPSR_DID         int                  not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   GL_COVG_PART_KEY     varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   GL_CL_CD_KEY         varchar(100)         not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   AUDITED_BASIS_AMT    numeric(18,2)        null,
   PRODS_COMP_AUDITED_BASIS_AMT numeric(18,2)        null,
   EXPSR_TEXT           varchar(255)         not null,
   FIXED_BASIS_AMT      numeric(18,2)        null,
   PRODS_COMP_FIXED_BASIS_AMT numeric(18,2)        null,
   SCALABLE_BASIS_AMT   numeric(18,2)        null,
   PRODS_COMP_SCALABLE_BASIS_AMT numeric(18,2)        null,
   TERM_BASIS_AMT       numeric(18,2)        null,
   PRODS_COMP_TERM_BASIS_AMT numeric(18,2)        null,
   CURR_CD              varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_EXPSR_DELTA
   add constraint CGED_PK primary key nonclustered (GL_CVRBL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_GL_EXPSR_DELTA
   add constraint CGED_AK1 unique (GL_EXPSR_DID)
go

/*==============================================================*/
/* Index: CGED_CGCB1_XFK                                        */
/*==============================================================*/




create nonclustered index CGED_CGCB1_XFK on CS_GL_EXPSR_DELTA (GL_COVG_PART_KEY ASC)
go

/*==============================================================*/
/* Index: CGED_CGCCB_XFK                                        */
/*==============================================================*/




create nonclustered index CGED_CGCCB_XFK on CS_GL_EXPSR_DELTA (GL_CL_CD_KEY ASC)
go

/*==============================================================*/
/* Index: CGED_CLBA_XFK                                         */
/*==============================================================*/




create nonclustered index CGED_CLBA_XFK on CS_GL_EXPSR_DELTA (LOC_KEY ASC)
go

/*==============================================================*/
/* Table: CS_GL_MOD_BASE                                        */
/*==============================================================*/
create table CS_GL_MOD_BASE (
   GL_MOD_BID           int                  not null,
   GL_MOD_KEY           varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   MOD_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_MOD_BASE
   add constraint CGMB_PK primary key nonclustered (GL_MOD_KEY)
go

alter table CS_GL_MOD_BASE
   add constraint CGMB_AK1 unique (GL_MOD_BID)
go

/*==============================================================*/
/* Index: CGMB_CGCB_XFK                                         */
/*==============================================================*/




create nonclustered index CGMB_CGCB_XFK on CS_GL_MOD_BASE (GL_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CGMB_CMBA_XFK                                         */
/*==============================================================*/




create nonclustered index CGMB_CMBA_XFK on CS_GL_MOD_BASE (MOD_KEY ASC)
go

/*==============================================================*/
/* Index: CGMB_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CGMB_CPBA_XFK on CS_GL_MOD_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CGMB_CPLB_XFK                                         */
/*==============================================================*/




create nonclustered index CGMB_CPLB_XFK on CS_GL_MOD_BASE (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Table: CS_GL_MOD_DELTA                                       */
/*==============================================================*/
create table CS_GL_MOD_DELTA (
   GL_MOD_DID           int                  not null,
   GL_MOD_KEY           varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   MOD_CD               varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   MOD_VAL_TEXT         varchar(255)         not null,
   JUST_TEXT            varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_MOD_DELTA
   add constraint CGMD_PK primary key nonclustered (GL_MOD_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_GL_MOD_DELTA
   add constraint CGMD_AK1 unique (GL_MOD_DID)
go

/*==============================================================*/
/* Table: CS_GL_MOD_RF_BASE                                     */
/*==============================================================*/
create table CS_GL_MOD_RF_BASE (
   GL_MOD_RF_BID        int                  not null,
   GL_MOD_RF_KEY        varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   GL_MOD_KEY           varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_MOD_RF_BASE
   add constraint CGMRB_PK primary key nonclustered (GL_MOD_RF_KEY)
go

alter table CS_GL_MOD_RF_BASE
   add constraint CGMRB_AK1 unique (GL_MOD_RF_BID)
go

/*==============================================================*/
/* Index: CGMRB_CGMB_XFK                                        */
/*==============================================================*/




create nonclustered index CGMRB_CGMB_XFK on CS_GL_MOD_RF_BASE (GL_MOD_KEY ASC)
go

/*==============================================================*/
/* Index: CGMRB_CPBA_XFK                                        */
/*==============================================================*/




create nonclustered index CGMRB_CPBA_XFK on CS_GL_MOD_RF_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_GL_MOD_RF_DELTA                                    */
/*==============================================================*/
create table CS_GL_MOD_RF_DELTA (
   GL_MOD_RF_DID        int                  not null,
   GL_MOD_RF_KEY        varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   RF_CD                varchar(255)         not null,
   ASSESS_RATE          numeric(18,4)        not null,
   JUST_TEXT            varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_MOD_RF_DELTA
   add constraint CGMRD_PK primary key nonclustered (GL_MOD_RF_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_GL_MOD_RF_DELTA
   add constraint CGMRD_AK1 unique (GL_MOD_RF_DID)
go

/*==============================================================*/
/* Table: CS_GL_POL_LINE_DELTA                                  */
/*==============================================================*/
create table CS_GL_POL_LINE_DELTA (
   GL_POL_LINE_DID      int                  not null,
   POL_LINE_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CLAIMS_ORIG_EFF_DT   DATETIME2            null,
   LOC_LIMITS_FL        char(1)              not null,
   POLLUTION_CLEANUP_EXP_FL char(1)              not null,
   CLAIMS_RETROACTIVE_DT DATETIME2            null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_POL_LINE_DELTA
   add constraint CGPLD_PK primary key nonclustered (POL_LINE_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_GL_POL_LINE_DELTA
   add constraint CGPLD_AK1 unique (GL_POL_LINE_DID)
go

/*==============================================================*/
/* Table: CS_GL_PREM_TRAN                                       */
/*==============================================================*/
create table CS_GL_PREM_TRAN (
   GL_PREM_TRANS_TID    int                  identity,
   GL_PREM_TRANS_KEY    varchar(100)         not null,
   AGCY_KEY             varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   EXPSR_LOC_KEY        varchar(100)         not null,
   ADDL_INS_KEY         varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   GL_COVG_KEY          varchar(100)         not null,
   GL_COND_KEY          varchar(100)         not null,
   GL_EXCL_KEY          varchar(100)         not null,
   REC_TYPE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ACCTG_PRD_ID         int                  not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ORIG_TRANS_PROC_DTS  DATETIME2            not null,
   ORIG_END_EFF_DT      DATE                 not null,
   TRANS_EFF_DT         DATE                 not null,
   TRANS_EXP_DT         DATE                 not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   STATE_COST_TYPE_CD   varchar(255)         not null,
   PREM_BASIS_AMT       numeric(18,2)        not null,
   STD_BASE_RATE        numeric(18,4)        not null,
   STD_ADJ_RATE         numeric(18,4)        not null,
   STD_AMT              numeric(18,2)        not null,
   STD_TERM_AMT         numeric(18,2)        not null,
   OVERRIDE_BASE_RATE   numeric(18,4)        not null,
   OVERRIDE_ADJ_RATE    numeric(18,4)        not null,
   OVERRIDE_AMT         numeric(18,2)        not null,
   OVERRIDE_TERM_AMT    numeric(18,2)        not null,
   ACT_BASE_RATE        numeric(18,4)        not null,
   ACT_ADJ_RATE         numeric(18,4)        not null,
   ACT_AMT              numeric(18,2)        not null,
   ACT_TERM_AMT         numeric(18,2)        not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   STTLMNT_PREM_BASIS_AMT numeric(18,2)        null,
   STTLMNT_STD_AMT      numeric(18,2)        null,
   STTLMNT_STD_TERM_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   STTLMNT_ACT_AMT      numeric(18,2)        null,
   STTLMNT_ACT_TERM_AMT numeric(18,2)        null,
   STTLMNT_CURR_CD      varchar(255)         not null,
   STTLMNT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_STTLMNT_RATE numeric(15,10)       null,
   REG_RPT_PREM_BASIS_AMT numeric(18,2)        null,
   REG_RPT_STD_AMT      numeric(18,2)        null,
   REG_RPT_STD_TERM_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   REG_RPT_ACT_AMT      numeric(18,2)        null,
   REG_RPT_ACT_TERM_AMT numeric(18,2)        null,
   REG_RPT_CURR_CD      varchar(255)         not null,
   REG_RPT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_REG_RPT_RATE numeric(15,10)       null,
   RPT_PREM_BASIS_AMT   numeric(18,2)        null,
   RPT_STD_AMT          numeric(18,2)        null,
   RPT_STD_TERM_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   RPT_ACT_AMT          numeric(18,2)        null,
   RPT_ACT_TERM_AMT     numeric(18,2)        null,
   RPT_CURR_CD          varchar(255)         not null,
   RPT_TRANS_AMT        numeric(18,2)        not null,
   TRANS_TO_RPT_RATE    numeric(15,10)       not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   COMM_PCT             numeric(6,3)         not null,
   EST_PREM_AMT         numeric(18,2)        not null,
   RPT_PREM_AMT         numeric(18,2)        not null,
   AUDIT_PREM_AMT       numeric(18,2)        not null,
   AUDITABLE_FL         char(1)              not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_PREM_TRAN
   add constraint CGPT_PK primary key nonclustered (GL_PREM_TRANS_TID)
go

alter table CS_GL_PREM_TRAN
   add constraint CGPT_AK1 unique (GL_PREM_TRANS_KEY)
go

/*==============================================================*/
/* Index: CGPT_CABA_XFK                                         */
/*==============================================================*/




create nonclustered index CGPT_CABA_XFK on CS_GL_PREM_TRAN (AGCY_KEY ASC)
go

/*==============================================================*/
/* Index: CGPT_CABA1_XFK                                        */
/*==============================================================*/




create nonclustered index CGPT_CABA1_XFK on CS_GL_PREM_TRAN (AGNT_KEY ASC)
go

/*==============================================================*/
/* Index: CGPT_CCBA_XFK                                         */
/*==============================================================*/




create nonclustered index CGPT_CCBA_XFK on CS_GL_PREM_TRAN (COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CGPT_CGCB_XFK                                         */
/*==============================================================*/




create nonclustered index CGPT_CGCB_XFK on CS_GL_PREM_TRAN (GL_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CGPT_CGCB1_XFK                                        */
/*==============================================================*/




create nonclustered index CGPT_CGCB1_XFK on CS_GL_PREM_TRAN (GL_COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CGPT_CGCB2_XFK                                        */
/*==============================================================*/




create nonclustered index CGPT_CGCB2_XFK on CS_GL_PREM_TRAN (GL_COND_KEY ASC)
go

/*==============================================================*/
/* Index: CGPT_CGEB_XFK                                         */
/*==============================================================*/




create nonclustered index CGPT_CGEB_XFK on CS_GL_PREM_TRAN (GL_EXCL_KEY ASC)
go

/*==============================================================*/
/* Index: CGPT_CLBA_XFK                                         */
/*==============================================================*/




create nonclustered index CGPT_CLBA_XFK on CS_GL_PREM_TRAN (EXPSR_LOC_KEY ASC)
go

/*==============================================================*/
/* Index: CGPT_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CGPT_CPBA_XFK on CS_GL_PREM_TRAN (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CGPT_CPLB_XFK                                         */
/*==============================================================*/




create nonclustered index CGPT_CPLB_XFK on CS_GL_PREM_TRAN (POL_LINE_KEY ASC)
go

/*==============================================================*/
/* Index: CGPT_CPPB_XFK                                         */
/*==============================================================*/




create nonclustered index CGPT_CPPB_XFK on CS_GL_PREM_TRAN (ADDL_INS_KEY ASC)
go

/*==============================================================*/
/* Table: CS_GL_SI_COND_BASE                                    */
/*==============================================================*/
create table CS_GL_SI_COND_BASE (
   GL_SI_COND_BID       int                  not null,
   GL_SI_COND_KEY       varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_SI_COND_BASE
   add constraint CGSCB_PK primary key nonclustered (GL_SI_COND_KEY)
go

alter table CS_GL_SI_COND_BASE
   add constraint CGSCB_AK1 unique (GL_SI_COND_BID)
go

/*==============================================================*/
/* Index: CGSCB_CGCB_XFK                                        */
/*==============================================================*/




create nonclustered index CGSCB_CGCB_XFK on CS_GL_SI_COND_BASE (GL_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CGSCB_CPBA_XFK                                        */
/*==============================================================*/




create nonclustered index CGSCB_CPBA_XFK on CS_GL_SI_COND_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_GL_SI_COND_DELTA                                   */
/*==============================================================*/
create table CS_GL_SI_COND_DELTA (
   GL_SI_COND_DID       int                  not null,
   GL_SI_COND_KEY       varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   GL_COND_KEY          varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   SCHED_NO             int                  null,
   TYPE_KEY_COL_1       varchar(255)         not null,
   TYPE_KEY_COL_2       varchar(255)         not null,
   STR_COL_1            varchar(255)         not null,
   STR_COL_2            varchar(255)         not null,
   STR_COL_3            varchar(255)         not null,
   STR_COL_4            varchar(255)         not null,
   STR_COL_5            varchar(255)         not null,
   STR_COL_6            varchar(255)         not null,
   DT_COL_1             DATETIME2            null,
   INT_COL_1            int                  null,
   POS_INT_COL_1        int                  null,
   BOOL_COL_1_FL        char(1)              not null,
   BOOL_COL_2_FL        char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_SI_COND_DELTA
   add constraint CGSCD_PK primary key nonclustered (GL_SI_COND_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_GL_SI_COND_DELTA
   add constraint CGSCD_AK1 unique (GL_SI_COND_DID)
go

/*==============================================================*/
/* Index: CGSCD_CGCB_XFK                                        */
/*==============================================================*/




create nonclustered index CGSCD_CGCB_XFK on CS_GL_SI_COND_DELTA (GL_COND_KEY ASC)
go

/*==============================================================*/
/* Index: CGSCD_CLBA_XFK                                        */
/*==============================================================*/




create nonclustered index CGSCD_CLBA_XFK on CS_GL_SI_COND_DELTA (LOC_KEY ASC)
go

/*==============================================================*/
/* Index: CGSCD_CPPB_XFK                                        */
/*==============================================================*/




create nonclustered index CGSCD_CPPB_XFK on CS_GL_SI_COND_DELTA (POL_PARTY_KEY ASC)
go

/*==============================================================*/
/* Table: CS_GL_SI_DELTA                                        */
/*==============================================================*/
create table CS_GL_SI_DELTA (
   GL_SI_DID            int                  not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   GL_OWNING_CVRBL_KEY  varchar(100)         not null,
   GL_COVG_KEY          varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   SCHED_NO             int                  null,
   TYPE_KEY_COL_1       varchar(255)         not null,
   TYPE_KEY_COL_2       varchar(255)         not null,
   STR_COL_1            varchar(255)         not null,
   STR_COL_2            varchar(255)         not null,
   STR_COL_3            varchar(255)         not null,
   STR_COL_4            varchar(255)         not null,
   STR_COL_5            varchar(255)         not null,
   STR_COL_6            varchar(255)         not null,
   DT_COL_1             DATETIME2            null,
   INT_COL_1            int                  null,
   POS_INT_COL_1        int                  null,
   BOOL_COL_1_FL        char(1)              not null,
   BOOL_COL_2_FL        char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_SI_DELTA
   add constraint CGSD_PK primary key nonclustered (GL_CVRBL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_GL_SI_DELTA
   add constraint CGSD_AK1 unique (GL_SI_DID)
go

/*==============================================================*/
/* Index: CGSD_CGCB1_XFK                                        */
/*==============================================================*/




create nonclustered index CGSD_CGCB1_XFK on CS_GL_SI_DELTA (GL_COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CGSD_CGCB2_XFK                                        */
/*==============================================================*/




create nonclustered index CGSD_CGCB2_XFK on CS_GL_SI_DELTA (GL_OWNING_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CGSD_CLBA_XFK                                         */
/*==============================================================*/




create nonclustered index CGSD_CLBA_XFK on CS_GL_SI_DELTA (LOC_KEY ASC)
go

/*==============================================================*/
/* Index: CGSD_CPPB_XFK                                         */
/*==============================================================*/




create nonclustered index CGSD_CPPB_XFK on CS_GL_SI_DELTA (POL_PARTY_KEY ASC)
go

/*==============================================================*/
/* Table: CS_GL_SI_EXCL_BASE                                    */
/*==============================================================*/
create table CS_GL_SI_EXCL_BASE (
   GL_SI_EXCL_BID       int                  not null,
   GL_SI_EXCL_KEY       varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_SI_EXCL_BASE
   add constraint CGSEB_PK primary key nonclustered (GL_SI_EXCL_KEY)
go

alter table CS_GL_SI_EXCL_BASE
   add constraint CGSEB_AK1 unique (GL_SI_EXCL_BID)
go

/*==============================================================*/
/* Index: CGSEB_CGCB_XFK                                        */
/*==============================================================*/




create nonclustered index CGSEB_CGCB_XFK on CS_GL_SI_EXCL_BASE (GL_CVRBL_KEY ASC)
go

/*==============================================================*/
/* Index: CGSEB_CPBA_XFK                                        */
/*==============================================================*/




create nonclustered index CGSEB_CPBA_XFK on CS_GL_SI_EXCL_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_GL_SI_EXCL_DELTA                                   */
/*==============================================================*/
create table CS_GL_SI_EXCL_DELTA (
   GL_SI_EXCL_DID       int                  not null,
   GL_SI_EXCL_KEY       varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   GL_EXCL_KEY          varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   SCHED_NO             int                  null,
   TYPE_KEY_COL_1       varchar(255)         not null,
   TYPE_KEY_COL_2       varchar(255)         not null,
   STR_COL_1            varchar(255)         not null,
   STR_COL_2            varchar(255)         not null,
   STR_COL_3            varchar(255)         not null,
   STR_COL_4            varchar(255)         not null,
   STR_COL_5            varchar(255)         not null,
   STR_COL_6            varchar(255)         not null,
   DT_COL_1             DATETIME2            null,
   INT_COL_1            int                  null,
   POS_INT_COL_1        int                  null,
   BOOL_COL_1_FL        char(1)              not null,
   BOOL_COL_2_FL        char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_GL_SI_EXCL_DELTA
   add constraint CGSED_PK primary key nonclustered (GL_SI_EXCL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_GL_SI_EXCL_DELTA
   add constraint CGSED_AK1 unique (GL_SI_EXCL_DID)
go

/*==============================================================*/
/* Index: CGSED_CGEB_XFK                                        */
/*==============================================================*/




create nonclustered index CGSED_CGEB_XFK on CS_GL_SI_EXCL_DELTA (GL_EXCL_KEY ASC)
go

/*==============================================================*/
/* Index: CGSED_CLBA_XFK                                        */
/*==============================================================*/




create nonclustered index CGSED_CLBA_XFK on CS_GL_SI_EXCL_DELTA (LOC_KEY ASC)
go

/*==============================================================*/
/* Index: CGSED_CPPB_XFK                                        */
/*==============================================================*/




create nonclustered index CGSED_CPPB_XFK on CS_GL_SI_EXCL_DELTA (POL_PARTY_KEY ASC)
go

/*==============================================================*/
/* Table: CS_HO_ADDL_INTRST_BASE                                */
/*==============================================================*/
create table CS_HO_ADDL_INTRST_BASE (
   HO_ADDL_INTRST_BID   int                  not null,
   HO_ADDL_INTRST_KEY   varchar(100)         not null,
   HO_DWLNG_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_ADDL_INTRST_BASE
   add constraint CHAIB_PK primary key nonclustered (HO_ADDL_INTRST_KEY)
go

alter table CS_HO_ADDL_INTRST_BASE
   add constraint CHAIB_AK1 unique (HO_ADDL_INTRST_BID)
go

/*==============================================================*/
/* Index: CHAIB_CHDB_XFK                                        */
/*==============================================================*/




create nonclustered index CHAIB_CHDB_XFK on CS_HO_ADDL_INTRST_BASE (HO_DWLNG_KEY ASC)
go

/*==============================================================*/
/* Index: CHAIB_CPPB_XFK                                        */
/*==============================================================*/




create nonclustered index CHAIB_CPPB_XFK on CS_HO_ADDL_INTRST_BASE (POL_PARTY_KEY ASC)
go

/*==============================================================*/
/* Index: CHAIB_CPBA_XFK                                        */
/*==============================================================*/




create nonclustered index CHAIB_CPBA_XFK on CS_HO_ADDL_INTRST_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_HO_ADDL_INTRST_DELTA                               */
/*==============================================================*/
create table CS_HO_ADDL_INTRST_DELTA (
   HO_ADDL_INTRST_DID   int                  not null,
   HO_ADDL_INTRST_KEY   varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ADDL_INTRST_TYPE_CD  varchar(255)         not null,
   CONTRACT_NO          varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_ADDL_INTRST_DELTA
   add constraint CHAID_PK primary key nonclustered (HO_ADDL_INTRST_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_HO_ADDL_INTRST_DELTA
   add constraint CHAID_AK1 unique (HO_ADDL_INTRST_DID)
go

/*==============================================================*/
/* Table: CS_HO_DWLNG_BASE                                      */
/*==============================================================*/
create table CS_HO_DWLNG_BASE (
   HO_DWLNG_BID         int                  not null,
   HO_DWLNG_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_DWLNG_BASE
   add constraint CHDB_PK primary key nonclustered (HO_DWLNG_KEY)
go

alter table CS_HO_DWLNG_BASE
   add constraint CHDB_AK1 unique (HO_DWLNG_BID)
go

/*==============================================================*/
/* Index: CHDB_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CHDB_CPBA_XFK on CS_HO_DWLNG_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_HO_DWLNG_COVG_BASE                                 */
/*==============================================================*/
create table CS_HO_DWLNG_COVG_BASE (
   HO_DWLNG_COVG_BID    int                  not null,
   HO_DWLNG_COVG_KEY    varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   HO_DWLNG_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_DWLNG_COVG_BASE
   add constraint CHDCB_PK primary key nonclustered (HO_DWLNG_COVG_KEY)
go

alter table CS_HO_DWLNG_COVG_BASE
   add constraint CHDCB_AK1 unique (HO_DWLNG_COVG_BID)
go

/*==============================================================*/
/* Index: CHDCB_CCBA_XFK                                        */
/*==============================================================*/




create nonclustered index CHDCB_CCBA_XFK on CS_HO_DWLNG_COVG_BASE (COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CHDCB_CHDB_XFK                                        */
/*==============================================================*/




create nonclustered index CHDCB_CHDB_XFK on CS_HO_DWLNG_COVG_BASE (HO_DWLNG_KEY ASC)
go

/*==============================================================*/
/* Index: CHDCB_CPBA_XFK                                        */
/*==============================================================*/




create nonclustered index CHDCB_CPBA_XFK on CS_HO_DWLNG_COVG_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_HO_DWLNG_COVG_TERM_BASE                            */
/*==============================================================*/
create table CS_HO_DWLNG_COVG_TERM_BASE (
   HO_DWLNG_COVG_TERM_BID int                  not null,
   HO_DWLNG_COVG_TERM_KEY varchar(100)         not null,
   HO_DWLNG_COVG_KEY    varchar(100)         not null,
   COVG_TERM_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_DWLNG_COVG_TERM_BASE
   add constraint CHDCTB_PK primary key nonclustered (HO_DWLNG_COVG_TERM_KEY)
go

alter table CS_HO_DWLNG_COVG_TERM_BASE
   add constraint CHDCTB_AK1 unique (HO_DWLNG_COVG_TERM_BID)
go

/*==============================================================*/
/* Index: CHDCTB_CHDCB_XFK                                      */
/*==============================================================*/




create nonclustered index CHDCTB_CHDCB_XFK on CS_HO_DWLNG_COVG_TERM_BASE (HO_DWLNG_COVG_KEY ASC)
go

/*==============================================================*/
/* Table: CS_HO_DWLNG_COVG_TERM_DELTA                           */
/*==============================================================*/
create table CS_HO_DWLNG_COVG_TERM_DELTA (
   HO_DWLNG_COVG_TERM_DID int                  not null,
   HO_DWLNG_COVG_TERM_KEY varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   COVG_CD              varchar(255)         not null,
   COVG_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_DWLNG_COVG_TERM_DELTA
   add constraint CHDCTD_PK primary key nonclustered (HO_DWLNG_COVG_TERM_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_HO_DWLNG_COVG_TERM_DELTA
   add constraint CHDCTD_AK1 unique (HO_DWLNG_COVG_TERM_DID)
go

/*==============================================================*/
/* Table: CS_HO_DWLNG_DELTA                                     */
/*==============================================================*/
create table CS_HO_DWLNG_DELTA (
   HO_DWLNG_DID         int                  not null,
   HO_DWLNG_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   HO_LOC_KEY           varchar(100)         not null,
   BUILD_YR             int                  not null,
   PURCH_YR             int                  not null,
   DWLNG_AREA_SF_NO     int                  not null,
   GARAGE_AREA_SF_NO    int                  not null,
   HO_POL_TYPE_CD       varchar(255)         not null,
   GARAGE_TYPE_CD       varchar(255)         not null,
   PRIM_HEATING_TYPE_CD varchar(255)         not null,
   CONSTR_CD            varchar(255)         not null,
   CONSTR_TYPE_CD       varchar(255)         not null,
   ELECTRICAL_TYPE_CD   varchar(255)         not null,
   OCCPCY_CD            varchar(255)         not null,
   PLUMBING_TYPE_CD     varchar(255)         not null,
   LOC_TYPE_CD          varchar(255)         not null,
   SPRINKLER_TYPE_CD    varchar(255)         not null,
   WIRING_TYPE_CD       varchar(255)         not null,
   ROOF_TYPE_CD         varchar(255)         not null,
   RESIDENCE_TYPE_CD    varchar(255)         not null,
   FOUNDATION_TYPE_CD   varchar(255)         not null,
   STORIES_CD           varchar(255)         not null,
   WIND_CLASS_CD        varchar(255)         not null,
   DWLNG_USAGE_CD       varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   REPL_COST_AMT        numeric(18,2)        not null,
   FIRE_EXTINGUISHERS_FL char(1)              not null,
   BURGLAR_ALARM_FL     char(1)              not null,
   BURGLAR_ALARM_TYPE_CD varchar(255)         not null,
   FIRE_ALARM_FL        char(1)              not null,
   SMOKE_ALARM_FL       char(1)              not null,
   SMOKE_ALARM_ON_ALL_FLOORS_FL char(1)              not null,
   COVERED_PORCH_FL     char(1)              not null,
   COVERED_PORCH_AREA_NO int                  not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_DWLNG_DELTA
   add constraint CHDD_PK primary key nonclustered (HO_DWLNG_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_HO_DWLNG_DELTA
   add constraint CHDD_AK1 unique (HO_DWLNG_DID)
go

/*==============================================================*/
/* Index: CHDD_CHLB_XFK                                         */
/*==============================================================*/




create nonclustered index CHDD_CHLB_XFK on CS_HO_DWLNG_DELTA (HO_LOC_KEY ASC)
go

/*==============================================================*/
/* Table: CS_HO_DWLNG_MOD_BASE                                  */
/*==============================================================*/
create table CS_HO_DWLNG_MOD_BASE (
   HO_DWLNG_MOD_BID     int                  not null,
   HO_DWLNG_MOD_KEY     varchar(100)         not null,
   HO_DWLNG_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_DWLNG_MOD_BASE
   add constraint CHDMB_PK primary key nonclustered (HO_DWLNG_MOD_KEY)
go

alter table CS_HO_DWLNG_MOD_BASE
   add constraint CHDMB_AK1 unique (HO_DWLNG_MOD_BID)
go

/*==============================================================*/
/* Index: CHDMB_CHDB_XFK                                        */
/*==============================================================*/




create nonclustered index CHDMB_CHDB_XFK on CS_HO_DWLNG_MOD_BASE (HO_DWLNG_KEY ASC)
go

/*==============================================================*/
/* Index: CHDMB_CPBA_XFK                                        */
/*==============================================================*/




create nonclustered index CHDMB_CPBA_XFK on CS_HO_DWLNG_MOD_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_HO_DWLNG_MOD_DELTA                                 */
/*==============================================================*/
create table CS_HO_DWLNG_MOD_DELTA (
   HO_DWLNG_MOD_DID     int                  not null,
   HO_DWLNG_MOD_KEY     varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   MOD_CD               varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   MOD_VAL_TEXT         varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_DWLNG_MOD_DELTA
   add constraint CHDMD_PK primary key nonclustered (HO_DWLNG_MOD_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_HO_DWLNG_MOD_DELTA
   add constraint CHDMD_AK1 unique (HO_DWLNG_MOD_DID)
go

/*==============================================================*/
/* Table: CS_HO_LINE_COVG_BASE                                  */
/*==============================================================*/
create table CS_HO_LINE_COVG_BASE (
   HO_LINE_COVG_BID     int                  not null,
   HO_LINE_COVG_KEY     varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_LINE_COVG_BASE
   add constraint CHLCB_PK primary key nonclustered (HO_LINE_COVG_KEY)
go

alter table CS_HO_LINE_COVG_BASE
   add constraint CHLCB_AK1 unique (HO_LINE_COVG_BID)
go

/*==============================================================*/
/* Index: CHLCB_CPBA_XFK                                        */
/*==============================================================*/




create nonclustered index CHLCB_CPBA_XFK on CS_HO_LINE_COVG_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CHLCB_CCBA_XFK                                        */
/*==============================================================*/




create nonclustered index CHLCB_CCBA_XFK on CS_HO_LINE_COVG_BASE (COVG_KEY ASC)
go

/*==============================================================*/
/* Table: CS_HO_LINE_COVG_TERM_BASE                             */
/*==============================================================*/
create table CS_HO_LINE_COVG_TERM_BASE (
   HO_LINE_COVG_TERM_BID int                  not null,
   HO_LINE_COVG_TERM_KEY varchar(100)         not null,
   HO_LINE_COVG_KEY     varchar(100)         not null,
   COVG_TERM_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_LINE_COVG_TERM_BASE
   add constraint CHLCTB_PK primary key nonclustered (HO_LINE_COVG_TERM_KEY)
go

alter table CS_HO_LINE_COVG_TERM_BASE
   add constraint CHLCTB_AK1 unique (HO_LINE_COVG_TERM_BID)
go

/*==============================================================*/
/* Index: CHLCTB_CHLCB_XFK                                      */
/*==============================================================*/




create nonclustered index CHLCTB_CHLCB_XFK on CS_HO_LINE_COVG_TERM_BASE (HO_LINE_COVG_KEY ASC)
go

/*==============================================================*/
/* Table: CS_HO_LINE_COVG_TERM_DELTA                            */
/*==============================================================*/
create table CS_HO_LINE_COVG_TERM_DELTA (
   HO_LINE_COVG_TERM_DID int                  not null,
   HO_LINE_COVG_TERM_KEY varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   COVG_CD              varchar(255)         not null,
   COVG_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_LINE_COVG_TERM_DELTA
   add constraint CHLCTD_PK primary key nonclustered (HO_LINE_COVG_TERM_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_HO_LINE_COVG_TERM_DELTA
   add constraint CHLCTD_AK1 unique (HO_LINE_COVG_TERM_DID)
go

/*==============================================================*/
/* Table: CS_HO_LOC_BASE                                        */
/*==============================================================*/
create table CS_HO_LOC_BASE (
   HO_LOC_BID           int                  not null,
   HO_LOC_KEY           varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_LOC_BASE
   add constraint CHLB_PK primary key nonclustered (HO_LOC_KEY)
go

alter table CS_HO_LOC_BASE
   add constraint CHLB_AK1 unique (HO_LOC_BID)
go

/*==============================================================*/
/* Index: CHLB_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CHLB_CPBA_XFK on CS_HO_LOC_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CHLB_CLBA_XFK                                         */
/*==============================================================*/




create nonclustered index CHLB_CLBA_XFK on CS_HO_LOC_BASE (LOC_KEY ASC)
go

/*==============================================================*/
/* Table: CS_HO_LOC_DELTA                                       */
/*==============================================================*/
create table CS_HO_LOC_DELTA (
   HO_LOC_DID           int                  not null,
   HO_LOC_KEY           varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   DIST_FIRE_STATION_NO int                  not null,
   DIST_FIRE_HYDRANT_NO int                  not null,
   PROT_CLASS_CD        varchar(255)         not null,
   FLOODING_HAZARD_FL   char(1)              not null,
   NEAR_COMML_FL        char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_LOC_DELTA
   add constraint CHLD_PK primary key nonclustered (HO_LOC_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_HO_LOC_DELTA
   add constraint CHLD_AK1 unique (HO_LOC_DID)
go

/*==============================================================*/
/* Table: CS_HO_MOD_BASE                                        */
/*==============================================================*/
create table CS_HO_MOD_BASE (
   HO_MOD_BID           int                  not null,
   HO_MOD_KEY           varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_MOD_BASE
   add constraint CHMB_PK primary key nonclustered (HO_MOD_KEY)
go

alter table CS_HO_MOD_BASE
   add constraint CHMB_AK1 unique (HO_MOD_BID)
go

/*==============================================================*/
/* Index: CHMB_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CHMB_CPBA_XFK on CS_HO_MOD_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_HO_MOD_DELTA                                       */
/*==============================================================*/
create table CS_HO_MOD_DELTA (
   HO_MOD_DID           int                  not null,
   HO_MOD_KEY           varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   MOD_CD               varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   MOD_VAL_TEXT         varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_MOD_DELTA
   add constraint CHMD_PK primary key nonclustered (HO_MOD_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_HO_MOD_DELTA
   add constraint CHMD_AK1 unique (HO_MOD_DID)
go

/*==============================================================*/
/* Table: CS_HO_OTHR_LOC_BASE                                   */
/*==============================================================*/
create table CS_HO_OTHR_LOC_BASE (
   HO_OTHR_LOC_BID      int                  not null,
   HO_OTHR_LOC_KEY      varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   HO_LINE_COVG_KEY     varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_OTHR_LOC_BASE
   add constraint CHOLB_PK primary key nonclustered (HO_OTHR_LOC_KEY)
go

alter table CS_HO_OTHR_LOC_BASE
   add constraint CHOLB_AK1 unique (HO_OTHR_LOC_BID)
go

/*==============================================================*/
/* Index: CHOLB_CHLCB_XFK                                       */
/*==============================================================*/




create nonclustered index CHOLB_CHLCB_XFK on CS_HO_OTHR_LOC_BASE (HO_LINE_COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CHOLB_CPBA_XFK                                        */
/*==============================================================*/




create nonclustered index CHOLB_CPBA_XFK on CS_HO_OTHR_LOC_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_HO_OTHR_LOC_DELTA                                  */
/*==============================================================*/
create table CS_HO_OTHR_LOC_DELTA (
   HO_OTHR_LOC_DID      int                  not null,
   HO_OTHR_LOC_KEY      varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   LOC_KEY              varchar(100)         not null,
   LOC_NO               varchar(255)         not null,
   INCR_LMT_AMT_CD      varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_OTHR_LOC_DELTA
   add constraint CHOLD_PK primary key nonclustered (HO_OTHR_LOC_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_HO_OTHR_LOC_DELTA
   add constraint CHOLD_AK1 unique (HO_OTHR_LOC_DID)
go

/*==============================================================*/
/* Index: CHOLD_CLBA_XFK                                        */
/*==============================================================*/




create nonclustered index CHOLD_CLBA_XFK on CS_HO_OTHR_LOC_DELTA (LOC_KEY ASC)
go

/*==============================================================*/
/* Table: CS_HO_PREM_TRAN                                       */
/*==============================================================*/
create table CS_HO_PREM_TRAN (
   HO_PREM_TRANS_TID    int                  identity,
   HO_PREM_TRANS_KEY    varchar(100)         not null,
   AGCY_KEY             varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   HO_SI_KEY            varchar(100)         not null,
   HO_SI_LOC_KEY        varchar(100)         not null,
   HO_DWLNG_KEY         varchar(100)         not null,
   HO_DWLNG_LOC_KEY     varchar(100)         not null,
   HO_LINE_COVG_KEY     varchar(100)         not null,
   HO_DWLNG_COVG_KEY    varchar(100)         not null,
   HO_OTHR_LOC_LOC_KEY  varchar(100)         not null,
   REC_TYPE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ACCTG_PRD_ID         int                  not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ORIG_TRANS_PROC_DTS  DATETIME2            not null,
   ORIG_END_EFF_DT      DATE                 not null,
   TRANS_EFF_DT         DATE                 not null,
   TRANS_EXP_DT         DATE                 not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   SCHED_TYPE_CD        varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   STD_BASE_RATE        numeric(18,4)        not null,
   STD_ADJ_RATE         numeric(18,4)        not null,
   STD_AMT              numeric(18,2)        not null,
   STD_TERM_AMT         numeric(18,2)        not null,
   OVERRIDE_BASE_RATE   numeric(18,4)        not null,
   OVERRIDE_ADJ_RATE    numeric(18,4)        not null,
   OVERRIDE_AMT         numeric(18,2)        not null,
   OVERRIDE_TERM_AMT    numeric(18,2)        not null,
   ACT_BASE_RATE        numeric(18,4)        not null,
   ACT_ADJ_RATE         numeric(18,4)        not null,
   ACT_AMT              numeric(18,2)        not null,
   ACT_TERM_AMT         numeric(18,2)        not null,
   STTLMNT_PREM_BASIS_AMT numeric(18,2)        null,
   STTLMNT_STD_AMT      numeric(18,2)        null,
   STTLMNT_STD_TERM_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   STTLMNT_ACT_AMT      numeric(18,2)        null,
   STTLMNT_ACT_TERM_AMT numeric(18,2)        null,
   STTLMNT_CURR_CD      varchar(255)         not null,
   STTLMNT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_STTLMNT_RATE numeric(15,10)       null,
   REG_RPT_PREM_BASIS_AMT numeric(18,2)        null,
   REG_RPT_STD_AMT      numeric(18,2)        null,
   REG_RPT_STD_TERM_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   REG_RPT_ACT_AMT      numeric(18,2)        null,
   REG_RPT_ACT_TERM_AMT numeric(18,2)        null,
   REG_RPT_CURR_CD      varchar(255)         not null,
   REG_RPT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_REG_RPT_RATE numeric(15,10)       null,
   RPT_PREM_BASIS_AMT   numeric(18,2)        null,
   RPT_STD_AMT          numeric(18,2)        null,
   RPT_STD_TERM_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   RPT_ACT_AMT          numeric(18,2)        null,
   RPT_ACT_TERM_AMT     numeric(18,2)        null,
   RPT_CURR_CD          varchar(255)         not null,
   RPT_TRANS_AMT        numeric(18,2)        not null,
   TRANS_TO_RPT_RATE    numeric(15,10)       not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   COMM_PCT             numeric(6,3)         not null,
   PREM_BASIS_AMT       numeric(18,2)        not null,
   AUDITABLE_FL         char(1)              not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_PREM_TRAN
   add constraint CHPT_PK primary key nonclustered (HO_PREM_TRANS_TID)
go

alter table CS_HO_PREM_TRAN
   add constraint CHPT_AK1 unique (HO_PREM_TRANS_KEY)
go

/*==============================================================*/
/* Index: CHPT_CCBA_XFK                                         */
/*==============================================================*/




create nonclustered index CHPT_CCBA_XFK on CS_HO_PREM_TRAN (COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CHPT_CABA_XFK                                         */
/*==============================================================*/




create nonclustered index CHPT_CABA_XFK on CS_HO_PREM_TRAN (AGCY_KEY ASC)
go

/*==============================================================*/
/* Index: CHPT_CABA1_XFK                                        */
/*==============================================================*/




create nonclustered index CHPT_CABA1_XFK on CS_HO_PREM_TRAN (AGNT_KEY ASC)
go

/*==============================================================*/
/* Index: CHPT_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CHPT_CPBA_XFK on CS_HO_PREM_TRAN (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CHPT_CLBA_XFK                                         */
/*==============================================================*/




create nonclustered index CHPT_CLBA_XFK on CS_HO_PREM_TRAN (HO_SI_LOC_KEY ASC)
go

/*==============================================================*/
/* Index: CHPT_CHDB_XFK                                         */
/*==============================================================*/




create nonclustered index CHPT_CHDB_XFK on CS_HO_PREM_TRAN (HO_DWLNG_KEY ASC)
go

/*==============================================================*/
/* Index: CHPT_CLBA1_XFK                                        */
/*==============================================================*/




create nonclustered index CHPT_CLBA1_XFK on CS_HO_PREM_TRAN (HO_OTHR_LOC_LOC_KEY ASC)
go

/*==============================================================*/
/* Index: CHPT_CMDI_XFK                                         */
/*==============================================================*/




create nonclustered index CHPT_CMDI_XFK on CS_HO_PREM_TRAN (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: CHPT_CHSB_XFK                                         */
/*==============================================================*/




create nonclustered index CHPT_CHSB_XFK on CS_HO_PREM_TRAN (HO_SI_KEY ASC)
go

/*==============================================================*/
/* Index: CHPT_CHLCB_XFK                                        */
/*==============================================================*/




create nonclustered index CHPT_CHLCB_XFK on CS_HO_PREM_TRAN (HO_LINE_COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CHPT_CHDCB_XFK                                        */
/*==============================================================*/




create nonclustered index CHPT_CHDCB_XFK on CS_HO_PREM_TRAN (HO_DWLNG_COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CHPT_CLBA2_XFK                                        */
/*==============================================================*/




create nonclustered index CHPT_CLBA2_XFK on CS_HO_PREM_TRAN (HO_DWLNG_LOC_KEY ASC)
go

/*==============================================================*/
/* Table: CS_HO_SI_BASE                                         */
/*==============================================================*/
create table CS_HO_SI_BASE (
   HO_SI_BID            int                  not null,
   HO_SI_KEY            varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   HO_DWLNG_COVG_KEY    varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_SI_BASE
   add constraint CHSB_PK primary key nonclustered (HO_SI_KEY)
go

alter table CS_HO_SI_BASE
   add constraint CHSB_AK1 unique (HO_SI_BID)
go

/*==============================================================*/
/* Index: CHSB_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CHSB_CPBA_XFK on CS_HO_SI_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CHSB_CHDCB_XFK                                        */
/*==============================================================*/




create nonclustered index CHSB_CHDCB_XFK on CS_HO_SI_BASE (HO_DWLNG_COVG_KEY ASC)
go

/*==============================================================*/
/* Table: CS_HO_SI_DELTA                                        */
/*==============================================================*/
create table CS_HO_SI_DELTA (
   HO_SI_DID            int                  not null,
   HO_SI_KEY            varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   LOC_KEY              varchar(100)         not null,
   ITEM_NO              varchar(255)         not null,
   SCHED_TYPE_CD        varchar(255)         not null,
   ITEM_TEXT            varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   INCR_LMT_PCT_CD      varchar(255)         not null,
   EXPSR_VAL_AMT        numeric(18,2)        not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_HO_SI_DELTA
   add constraint CHSD_PK primary key nonclustered (HO_SI_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_HO_SI_DELTA
   add constraint CHSD_AK1 unique (HO_SI_DID)
go

/*==============================================================*/
/* Index: CHSD_CLBA_XFK                                         */
/*==============================================================*/




create nonclustered index CHSD_CLBA_XFK on CS_HO_SI_DELTA (LOC_KEY ASC)
go

/*==============================================================*/
/* Table: CS_LOC_BASE                                           */
/*==============================================================*/
create table CS_LOC_BASE (
   LOC_BID              int                  not null,
   LOC_KEY              varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_LOC_BASE
   add constraint CLBA_PK primary key nonclustered (LOC_KEY)
go

alter table CS_LOC_BASE
   add constraint CLBA_AK1 unique (LOC_BID)
go

/*==============================================================*/
/* Index: CLBA_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CLBA_CPBA_XFK on CS_LOC_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_LOC_DELTA                                          */
/*==============================================================*/
create table CS_LOC_DELTA (
   LOC_DID              int                  not null,
   LOC_KEY              varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ADDR_KEY             varchar(100)         not null,
   LOC_NO               varchar(255)         not null,
   STREET_1_NAME        varchar(255)         not null,
   STREET_2_NAME        varchar(255)         not null,
   STREET_3_NAME        varchar(255)         not null,
   CNTRY_CD             varchar(255)         not null,
   REGION_CD            varchar(255)         not null,
   CITY_NAME            varchar(255)         not null,
   POSTAL_CD            varchar(15)          not null,
   COUNTY_NAME          varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_LOC_DELTA
   add constraint CLDE_PK primary key nonclustered (LOC_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_LOC_DELTA
   add constraint CLDE_AK1 unique (LOC_DID)
go

/*==============================================================*/
/* Index: CLDE_CABA_XFK                                         */
/*==============================================================*/




create nonclustered index CLDE_CABA_XFK on CS_LOC_DELTA (ADDR_KEY ASC)
go

/*==============================================================*/
/* Table: CS_LOSS_TRAN                                          */
/*==============================================================*/
create table CS_LOSS_TRAN (
   LOSS_TRANS_ID        int                  identity,
   FEATURE_KEY          varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CLMNT_KEY            varchar(100)         not null,
   CLAIM_KEY            varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   RISK_KEY             varchar(100)         not null,
   COVG_LIMIT_KEY       varchar(100)         not null,
   REC_TYPE_KEY         varchar(100)         not null,
   COVG_ID              int                  not null,
   VNDR_KEY             varchar(100)         not null,
   AGCY_KEY             varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   RISK_TYPE_CD         varchar(255)         not null,
   LOSS_DT              DATE                 not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ACCTG_PRD_ID         int                  not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   CHECK_NO             varchar(255)         not null,
   CHECK_DT             DATE                 null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   CLAIM_AMT            numeric(18,2)        not null,
   CLAIM_EX_ADJ_AMT     numeric(18,2)        not null,
   CLAIM_CURR_CD        varchar(255)         not null,
   TRANS_TO_CLAIM_EX_RATE numeric(15,10)       not null,
   RSRV_AMT             numeric(18,2)        null,
   RSRV_EX_ADJ_AMT      numeric(18,2)        null,
   RSRV_CURR_CD         varchar(255)         not null,
   TRANS_TO_RSRV_EX_RATE numeric(15,10)       null,
   RPT_AMT              numeric(18,2)        not null,
   RPT_EX_ADJ_AMT       numeric(18,2)        not null,
   RPT_CURR_CD          varchar(255)         not null,
   CLAIM_TO_RPT_EX_RATE numeric(15,10)       not null,
   CLAIM_RSRV_CHNG_AMT  numeric(18,2)        not null,
   RSRV_RSRV_CHNG_AMT   numeric(18,2)        not null,
   RPT_RSRV_CHNG_AMT    numeric(18,2)        not null,
   RSRV_CHNG_AMT        numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_LOSS_TRAN
   add constraint CLTR_PK primary key nonclustered (LOSS_TRANS_ID)
go

/*==============================================================*/
/* Index: CLTR_CRTD_XFK                                         */
/*==============================================================*/




create nonclustered index CLTR_CRTD_XFK on CS_LOSS_TRAN (REC_TYPE_KEY ASC)
go

/*==============================================================*/
/* Index: CLTR_CVBA_XFK                                         */
/*==============================================================*/




create nonclustered index CLTR_CVBA_XFK on CS_LOSS_TRAN (VNDR_KEY ASC)
go

/*==============================================================*/
/* Index: CLTR_CCFB_XFK                                         */
/*==============================================================*/




create nonclustered index CLTR_CCFB_XFK on CS_LOSS_TRAN (FEATURE_KEY ASC)
go

/*==============================================================*/
/* Index: CLTR_CABA_XFK                                         */
/*==============================================================*/




create nonclustered index CLTR_CABA_XFK on CS_LOSS_TRAN (AGNT_KEY ASC)
go

/*==============================================================*/
/* Index: CLTR_CABA1_XFK                                        */
/*==============================================================*/




create nonclustered index CLTR_CABA1_XFK on CS_LOSS_TRAN (AGCY_KEY ASC)
go

/*==============================================================*/
/* Index: CLTR_CMDI_XFK                                         */
/*==============================================================*/




create nonclustered index CLTR_CMDI_XFK on CS_LOSS_TRAN (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: CLTR_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CLTR_CPBA_XFK on CS_LOSS_TRAN (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CLTR_CCBA_XFK                                         */
/*==============================================================*/




create nonclustered index CLTR_CCBA_XFK on CS_LOSS_TRAN (CLAIM_KEY ASC)
go

/*==============================================================*/
/* Index: CLTR_CCBA1_XFK                                        */
/*==============================================================*/




create nonclustered index CLTR_CCBA1_XFK on CS_LOSS_TRAN (CLMNT_KEY ASC)
go

/*==============================================================*/
/* Index: CLTR_CLBA_XFK                                         */
/*==============================================================*/




create nonclustered index CLTR_CLBA_XFK on CS_LOSS_TRAN (LOC_KEY ASC)
go

/*==============================================================*/
/* Table: CS_LOSS_TRAN_AGG                                      */
/*==============================================================*/
create table CS_LOSS_TRAN_AGG (
   LOSS_TRANS_AGG_ID    int                  identity,
   CLAIM_KEY            varchar(100)         not null,
   FEATURE_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   RISK_KEY             varchar(100)         not null,
   COVG_LIMIT_KEY       varchar(100)         not null,
   REC_TYPE_KEY         varchar(100)         not null,
   ACCTG_PRD_ID         int                  not null,
   CLMNT_KEY            varchar(100)         not null,
   COVG_ID              int                  not null,
   VNDR_KEY             varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   AGCY_KEY             varchar(100)         not null,
   RISK_TYPE_CD         varchar(255)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CURR_CD              varchar(255)         not null,
   IND_PD_AMT           numeric(18,2)        not null,
   MED_PD_AMT           numeric(18,2)        not null,
   ALAE_PD_AMT          numeric(18,2)        not null,
   ULAE_PD_AMT          numeric(18,2)        not null,
   DED_RCVRY_AMT        numeric(18,2)        not null,
   SLVG_RCVRY_AMT       numeric(18,2)        not null,
   SUBRO_RCVRY_AMT      numeric(18,2)        not null,
   IND_RCVRY_AMT        numeric(18,2)        not null,
   EXP_RCVRY_AMT        numeric(18,2)        not null,
   IND_RSRV_CHNG_AMT    numeric(18,2)        not null,
   MED_RSRV_CHNG_AMT    numeric(18,2)        not null,
   ALAE_RSRV_CHNG_AMT   numeric(18,2)        not null,
   ULAE_RSRV_CHNG_AMT   numeric(18,2)        not null,
   SLVG_RSRV_CHNG_AMT   numeric(18,2)        not null,
   SUBRO_RSRV_CHNG_AMT  numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_LOSS_TRAN_AGG
   add constraint CLTA_PK primary key nonclustered (LOSS_TRANS_AGG_ID)
go

/*==============================================================*/
/* Table: CS_LOSS_TYPE_INFO_DIM                                 */
/*==============================================================*/
create table CS_LOSS_TYPE_INFO_DIM (
   LOSS_TYPE_INFO_ID    int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   LOSS_TYPE_CD         varchar(255)         not null,
   CAUSE_OF_LOSS_CD     varchar(255)         not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_LOSS_TYPE_INFO_DIM
   add constraint CLTID_PK primary key nonclustered (LOSS_TYPE_INFO_ID)
go

alter table CS_LOSS_TYPE_INFO_DIM
   add constraint CLTID_AK1 unique (SOURCE_SYSTEM, LOSS_TYPE_CD, CAUSE_OF_LOSS_CD)
go

/*==============================================================*/
/* Table: CS_MOD_BASE                                           */
/*==============================================================*/
create table CS_MOD_BASE (
   MOD_BID              int                  not null,
   MOD_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_MOD_BASE
   add constraint CMBA_PK primary key nonclustered (MOD_KEY)
go

alter table CS_MOD_BASE
   add constraint CMBA_AK1 unique (MOD_BID)
go

/*==============================================================*/
/* Table: CS_MOD_DELTA                                          */
/*==============================================================*/
create table CS_MOD_DELTA (
   MOD_DID              int                  not null,
   MOD_KEY              varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   MOD_CD               varchar(255)         not null,
   MOD_TEXT             varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_MOD_DELTA
   add constraint CMDE_PK primary key nonclustered (MOD_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_MOD_DELTA
   add constraint CMDE_AK1 unique (MOD_DID)
go

/*==============================================================*/
/* Table: CS_MONTH_DIM                                          */
/*==============================================================*/
create table CS_MONTH_DIM (
   MTH_ID               int                  not null,
   YR_NO                int                  not null,
   QTR_ID               int                  not null,
   MTH_NO               char(2)              not null,
   QTR_NO               char(1)              not null,
   MTH_BEGIN_DT         DATE                 not null,
   MTH_END_DT           DATE                 not null,
   QTR_FIRST_MTH_NO     char(2)              not null,
   QTR_BEGIN_DT         DATE                 not null,
   QTR_END_DT           DATE                 not null,
   YR_BEGIN_DT          DATE                 not null,
   YR_END_DT            DATE                 not null,
   MTH_NAME             varchar(10)          not null
)
go

alter table CS_MONTH_DIM
   add constraint CMDI_PK primary key nonclustered (MTH_ID)
go

/*==============================================================*/
/* Table: CS_NAMED_INSURED_BASE                                 */
/*==============================================================*/
create table CS_NAMED_INSURED_BASE (
   NAMED_INSURED_BASE_ID int                  not null,
   NAMED_INSURED_KEY    varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   MAILING_ADDR_1       varchar(255)         not null,
   MAILING_ADDR_2       varchar(255)         not null,
   MAILING_ADDR_3       varchar(255)         not null,
   MAILING_CITY         varchar(255)         not null,
   MAILING_STATE_CD     varchar(255)         not null,
   MAILING_ZIP_CD       varchar(15)          not null,
   PRIM_INSURED_FL      char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_NAMED_INSURED_BASE
   add constraint CNIB_PK primary key nonclustered (NAMED_INSURED_KEY)
go

alter table CS_NAMED_INSURED_BASE
   add constraint CNIB_AK1 unique (NAMED_INSURED_BASE_ID)
go

/*==============================================================*/
/* Index: CNIB_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CNIB_CPBA_XFK on CS_NAMED_INSURED_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_NAMED_INSURED_DELTA                                */
/*==============================================================*/
create table CS_NAMED_INSURED_DELTA (
   NAMED_INSURED_ID     int                  not null,
   NAMED_INSURED_KEY    varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_DELETE_DTS       DATETIME2            null,
   SOURCE_SYSTEM        varchar(10)          not null,
   POL_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   INSURED_NAME         varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_NAMED_INSURED_DELTA
   add constraint CNID_PK primary key nonclustered (NAMED_INSURED_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_NAMED_INSURED_DELTA
   add constraint CNID_AK1 unique (NAMED_INSURED_ID)
go

/*==============================================================*/
/* Table: CS_ORGANIZATION_DIM                                   */
/*==============================================================*/
create table CS_ORGANIZATION_DIM (
   ORG_ID               int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   MASTER_CO_CD         varchar(255)         not null,
   POL_CO_CD            varchar(255)         not null,
   BRANCH_OFC_CD        varchar(255)         not null,
   PROFIT_CTR_CD        varchar(255)         not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_ORGANIZATION_DIM
   add constraint CODI_PK primary key nonclustered (ORG_ID)
go

alter table CS_ORGANIZATION_DIM
   add constraint CODI_AK1 unique (SOURCE_SYSTEM, MASTER_CO_CD, POL_CO_CD, PROFIT_CTR_CD, BRANCH_OFC_CD)
go

/*==============================================================*/
/* Table: CS_PARTY_BASE                                         */
/*==============================================================*/
create table CS_PARTY_BASE (
   PARTY_BID            int                  not null,
   PARTY_KEY            varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PARTY_BASE
   add constraint CPBA_PK primary key nonclustered (PARTY_KEY)
go

alter table CS_PARTY_BASE
   add constraint CPBA_AK1 unique (PARTY_BID)
go

/*==============================================================*/
/* Table: CS_PARTY_DELTA                                        */
/*==============================================================*/
create table CS_PARTY_DELTA (
   PARTY_DID            int                  not null,
   PARTY_KEY            varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PRIM_ADDR_KEY        varchar(100)         not null,
   PARTY_TYPE_CD        varchar(255)         not null,
   ORG_NAME             varchar(255)         not null,
   PREFIX_NAME          varchar(255)         not null,
   FIRST_NAME           varchar(255)         not null,
   MIDDLE_NAME          varchar(255)         not null,
   LAST_NAME            varchar(255)         not null,
   SUFFIX_NAME          varchar(255)         not null,
   BIRTH_DT             DATE                 null,
   GENDER_CD            varchar(255)         not null,
   MARITAL_STATUS_CD    varchar(255)         not null,
   TAX_ID               varchar(15)          not null,
   OCCPTN_CD            varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PARTY_DELTA
   add constraint CPDE_PK primary key nonclustered (PARTY_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_PARTY_DELTA
   add constraint CPDE_AK1 unique (PARTY_DID)
go

/*==============================================================*/
/* Index: CPDE_CABA2_XFK                                        */
/*==============================================================*/




create nonclustered index CPDE_CABA2_XFK on CS_PARTY_DELTA (PRIM_ADDR_KEY ASC)
go

/*==============================================================*/
/* Table: CS_PA_ADDL_INTRST_BASE                                */
/*==============================================================*/
create table CS_PA_ADDL_INTRST_BASE (
   PA_ADDL_INTRST_BID   int                  not null,
   PA_ADDL_INTRST_KEY   varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   VEH_KEY              varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_ADDL_INTRST_BASE
   add constraint CPAIB_PK primary key nonclustered (PA_ADDL_INTRST_KEY)
go

alter table CS_PA_ADDL_INTRST_BASE
   add constraint CPAIB_AK1 unique (PA_ADDL_INTRST_BID)
go

/*==============================================================*/
/* Index: CPAIB_CPPB_XFK                                        */
/*==============================================================*/




create nonclustered index CPAIB_CPPB_XFK on CS_PA_ADDL_INTRST_BASE (POL_PARTY_KEY ASC)
go

/*==============================================================*/
/* Index: CPAIB_CPVB_XFK                                        */
/*==============================================================*/




create nonclustered index CPAIB_CPVB_XFK on CS_PA_ADDL_INTRST_BASE (VEH_KEY ASC)
go

/*==============================================================*/
/* Index: CPAIB_CPBA_XFK                                        */
/*==============================================================*/




create nonclustered index CPAIB_CPBA_XFK on CS_PA_ADDL_INTRST_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_PA_ADDL_INTRST_DELTA                               */
/*==============================================================*/
create table CS_PA_ADDL_INTRST_DELTA (
   PA_ADDL_INTRST_DID   int                  not null,
   PA_ADDL_INTRST_KEY   varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ADDL_INTRST_TYPE_CD  varchar(255)         not null,
   CONTRACT_NO          varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_ADDL_INTRST_DELTA
   add constraint CPAID_PK primary key nonclustered (PA_ADDL_INTRST_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_PA_ADDL_INTRST_DELTA
   add constraint CPAID_AK1 unique (PA_ADDL_INTRST_DID)
go

/*==============================================================*/
/* Table: CS_PA_DRVR_BASE                                       */
/*==============================================================*/
create table CS_PA_DRVR_BASE (
   PA_DRVR_BID          int                  not null,
   PA_DRVR_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_DRVR_BASE
   add constraint CPDB_PK primary key nonclustered (PA_DRVR_KEY)
go

alter table CS_PA_DRVR_BASE
   add constraint CPDB_AK1 unique (PA_DRVR_BID)
go

/*==============================================================*/
/* Index: CPDB_CPPB_XFK                                         */
/*==============================================================*/




create nonclustered index CPDB_CPPB_XFK on CS_PA_DRVR_BASE (POL_PARTY_KEY ASC)
go

/*==============================================================*/
/* Index: CPDB_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CPDB_CPBA_XFK on CS_PA_DRVR_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_PA_DRVR_DELTA                                      */
/*==============================================================*/
create table CS_PA_DRVR_DELTA (
   PA_DRVR_DID          int                  not null,
   PA_DRVR_KEY          varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   LIC_NO               varchar(15)          not null,
   LIC_JURS_CD          varchar(255)         not null,
   ACCIDENT_CNT_CD      varchar(255)         not null,
   VIOLATION_CNT_CD     varchar(255)         not null,
   GOOD_DRIVER_DISC_FL  char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_DRVR_DELTA
   add constraint CPDD_PK primary key nonclustered (PA_DRVR_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_PA_DRVR_DELTA
   add constraint CPDD_AK1 unique (PA_DRVR_DID)
go

/*==============================================================*/
/* Table: CS_PA_LINE_COVG_BASE                                  */
/*==============================================================*/
create table CS_PA_LINE_COVG_BASE (
   PA_LINE_COVG_BID     int                  not null,
   PA_LINE_COVG_KEY     varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_LINE_COVG_BASE
   add constraint CPLCB_PK primary key nonclustered (PA_LINE_COVG_KEY)
go

alter table CS_PA_LINE_COVG_BASE
   add constraint CPLCB_AK1 unique (PA_LINE_COVG_BID)
go

/*==============================================================*/
/* Index: CPLCB_CPBA_XFK                                        */
/*==============================================================*/




create nonclustered index CPLCB_CPBA_XFK on CS_PA_LINE_COVG_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CPLCB_CCBA_XFK                                        */
/*==============================================================*/




create nonclustered index CPLCB_CCBA_XFK on CS_PA_LINE_COVG_BASE (COVG_KEY ASC)
go

/*==============================================================*/
/* Table: CS_PA_LINE_COVG_TERM_BASE                             */
/*==============================================================*/
create table CS_PA_LINE_COVG_TERM_BASE (
   PA_LINE_COVG_TERM_BID int                  not null,
   PA_LINE_COVG_TERM_KEY varchar(100)         not null,
   PA_LINE_COVG_KEY     varchar(100)         not null,
   COVG_TERM_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_LINE_COVG_TERM_BASE
   add constraint CPLCTB_PK primary key nonclustered (PA_LINE_COVG_TERM_KEY)
go

alter table CS_PA_LINE_COVG_TERM_BASE
   add constraint CPLCTB_AK1 unique (PA_LINE_COVG_TERM_BID)
go

/*==============================================================*/
/* Index: CPLCTB_CPLCB_XFK                                      */
/*==============================================================*/




create nonclustered index CPLCTB_CPLCB_XFK on CS_PA_LINE_COVG_TERM_BASE (PA_LINE_COVG_KEY ASC)
go

/*==============================================================*/
/* Table: CS_PA_LINE_COVG_TERM_DELTA                            */
/*==============================================================*/
create table CS_PA_LINE_COVG_TERM_DELTA (
   PA_LINE_COVG_TERM_DID int                  not null,
   PA_LINE_COVG_TERM_KEY varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   COVG_CD              varchar(255)         not null,
   COVG_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_LINE_COVG_TERM_DELTA
   add constraint CPLCTD_PK primary key nonclustered (PA_LINE_COVG_TERM_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_PA_LINE_COVG_TERM_DELTA
   add constraint CPLCTD_AK1 unique (PA_LINE_COVG_TERM_DID)
go

/*==============================================================*/
/* Table: CS_PA_MOD_BASE                                        */
/*==============================================================*/
create table CS_PA_MOD_BASE (
   PA_MOD_BID           int                  not null,
   PA_MOD_KEY           varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_MOD_BASE
   add constraint CPMB_PK primary key nonclustered (PA_MOD_KEY)
go

alter table CS_PA_MOD_BASE
   add constraint CPMB_AK1 unique (PA_MOD_BID)
go

/*==============================================================*/
/* Index: CPMB_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CPMB_CPBA_XFK on CS_PA_MOD_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_PA_MOD_DELTA                                       */
/*==============================================================*/
create table CS_PA_MOD_DELTA (
   PA_POL_MOD_DID       int                  not null,
   PA_MOD_KEY           varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   MOD_CD               varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   MOD_VAL_TEXT         varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_MOD_DELTA
   add constraint CPMD_PK primary key nonclustered (PA_MOD_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_PA_MOD_DELTA
   add constraint CPMD_AK1 unique (PA_POL_MOD_DID)
go

/*==============================================================*/
/* Table: CS_PA_PREM_TRAN                                       */
/*==============================================================*/
create table CS_PA_PREM_TRAN (
   PA_PREM_TRANS_TID    int                  identity,
   PA_PREM_TRANS_KEY    varchar(100)         not null,
   AGCY_KEY             varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   PA_LINE_COVG_KEY     varchar(100)         not null,
   PA_VEH_COVG_KEY      varchar(100)         not null,
   VEH_KEY              varchar(100)         not null,
   VEH_GARAGE_LOC_KEY   varchar(100)         not null,
   ACCTG_PRD_ID         int                  not null,
   REC_TYPE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ORIG_TRANS_PROC_DTS  DATETIME2            not null,
   ORIG_END_EFF_DT      DATE                 not null,
   TRANS_EFF_DT         DATE                 not null,
   TRANS_EXP_DT         DATE                 not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   STD_BASE_RATE        numeric(18,4)        not null,
   STD_ADJ_RATE         numeric(18,4)        not null,
   STD_AMT              numeric(18,2)        not null,
   STD_TERM_AMT         numeric(18,2)        not null,
   OVERRIDE_BASE_RATE   numeric(18,4)        not null,
   OVERRIDE_ADJ_RATE    numeric(18,4)        not null,
   OVERRIDE_AMT         numeric(18,2)        not null,
   OVERRIDE_TERM_AMT    numeric(18,2)        not null,
   ACT_BASE_RATE        numeric(18,4)        not null,
   ACT_ADJ_RATE         numeric(18,4)        not null,
   ACT_AMT              numeric(18,2)        not null,
   ACT_TERM_AMT         numeric(18,2)        not null,
   STTLMNT_PREM_BASIS_AMT numeric(18,2)        null,
   STTLMNT_STD_AMT      numeric(18,2)        null,
   STTLMNT_STD_TERM_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   STTLMNT_ACT_AMT      numeric(18,2)        null,
   STTLMNT_ACT_TERM_AMT numeric(18,2)        null,
   STTLMNT_CURR_CD      varchar(255)         not null,
   STTLMNT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_STTLMNT_RATE numeric(15,10)       null,
   REG_RPT_PREM_BASIS_AMT numeric(18,2)        null,
   REG_RPT_STD_AMT      numeric(18,2)        null,
   REG_RPT_STD_TERM_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   REG_RPT_ACT_AMT      numeric(18,2)        null,
   REG_RPT_ACT_TERM_AMT numeric(18,2)        null,
   REG_RPT_CURR_CD      varchar(255)         not null,
   REG_RPT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_REG_RPT_RATE numeric(15,10)       null,
   RPT_PREM_BASIS_AMT   numeric(18,2)        null,
   RPT_STD_AMT          numeric(18,2)        null,
   RPT_STD_TERM_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   RPT_ACT_AMT          numeric(18,2)        null,
   RPT_ACT_TERM_AMT     numeric(18,2)        null,
   RPT_CURR_CD          varchar(255)         not null,
   RPT_TRANS_AMT        numeric(18,2)        not null,
   TRANS_TO_RPT_RATE    numeric(15,10)       not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   COMM_PCT             numeric(6,3)         not null,
   PREM_BASIS_AMT       numeric(18,2)        not null,
   AUDITABLE_FL         char(1)              not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_PREM_TRAN
   add constraint CPPT_PK primary key nonclustered (PA_PREM_TRANS_TID)
go

alter table CS_PA_PREM_TRAN
   add constraint CPPT_AK1 unique (PA_PREM_TRANS_KEY)
go

/*==============================================================*/
/* Index: CPPT_CABA_XFK                                         */
/*==============================================================*/




create nonclustered index CPPT_CABA_XFK on CS_PA_PREM_TRAN (AGCY_KEY ASC)
go

/*==============================================================*/
/* Index: CPPT_CABA1_XFK                                        */
/*==============================================================*/




create nonclustered index CPPT_CABA1_XFK on CS_PA_PREM_TRAN (AGNT_KEY ASC)
go

/*==============================================================*/
/* Index: CPPT_CCBA_XFK                                         */
/*==============================================================*/




create nonclustered index CPPT_CCBA_XFK on CS_PA_PREM_TRAN (COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CPPT_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CPPT_CPBA_XFK on CS_PA_PREM_TRAN (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CPPT_CPVB_XFK                                         */
/*==============================================================*/




create nonclustered index CPPT_CPVB_XFK on CS_PA_PREM_TRAN (VEH_KEY ASC)
go

/*==============================================================*/
/* Index: CPPT_CMDI_XFK                                         */
/*==============================================================*/




create nonclustered index CPPT_CMDI_XFK on CS_PA_PREM_TRAN (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: CPPT_CRTD_XFK                                         */
/*==============================================================*/




create nonclustered index CPPT_CRTD_XFK on CS_PA_PREM_TRAN (REC_TYPE_KEY ASC)
go

/*==============================================================*/
/* Index: CPPT_CPLCB_XFK                                        */
/*==============================================================*/




create nonclustered index CPPT_CPLCB_XFK on CS_PA_PREM_TRAN (PA_LINE_COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CPPT_CPVCB_XFK                                        */
/*==============================================================*/




create nonclustered index CPPT_CPVCB_XFK on CS_PA_PREM_TRAN (PA_VEH_COVG_KEY ASC)
go

/*==============================================================*/
/* Index: CPPT_CLBA_XFK                                         */
/*==============================================================*/




create nonclustered index CPPT_CLBA_XFK on CS_PA_PREM_TRAN (VEH_GARAGE_LOC_KEY ASC)
go

/*==============================================================*/
/* Table: CS_PA_VEH_BASE                                        */
/*==============================================================*/
create table CS_PA_VEH_BASE (
   PA_VEH_BID           int                  not null,
   VEH_KEY              varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_VEH_BASE
   add constraint CPVB_PK primary key nonclustered (VEH_KEY)
go

alter table CS_PA_VEH_BASE
   add constraint CPVB_AK1 unique (PA_VEH_BID)
go

/*==============================================================*/
/* Index: CPVB_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CPVB_CPBA_XFK on CS_PA_VEH_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_PA_VEH_COVG_BASE                                   */
/*==============================================================*/
create table CS_PA_VEH_COVG_BASE (
   PA_VEH_COVG_BID      int                  not null,
   PA_VEH_COVG_KEY      varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   VEH_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_VEH_COVG_BASE
   add constraint CPVCB_PK primary key nonclustered (PA_VEH_COVG_KEY)
go

alter table CS_PA_VEH_COVG_BASE
   add constraint CPVCB_AK1 unique (PA_VEH_COVG_BID)
go

/*==============================================================*/
/* Index: CPVCB_CPVB_XFK                                        */
/*==============================================================*/




create nonclustered index CPVCB_CPVB_XFK on CS_PA_VEH_COVG_BASE (VEH_KEY ASC)
go

/*==============================================================*/
/* Index: CPVCB_CPBA_XFK                                        */
/*==============================================================*/




create nonclustered index CPVCB_CPBA_XFK on CS_PA_VEH_COVG_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CPVCB_CCBA_XFK                                        */
/*==============================================================*/




create nonclustered index CPVCB_CCBA_XFK on CS_PA_VEH_COVG_BASE (COVG_KEY ASC)
go

/*==============================================================*/
/* Table: CS_PA_VEH_COVG_TERM_BASE                              */
/*==============================================================*/
create table CS_PA_VEH_COVG_TERM_BASE (
   PA_VEH_COVG_TERM_BID int                  not null,
   PA_VEH_COVG_TERM_KEY varchar(100)         not null,
   PA_VEH_COVG_KEY      varchar(100)         not null,
   COVG_TERM_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_VEH_COVG_TERM_BASE
   add constraint CPVCTB_PK primary key nonclustered (PA_VEH_COVG_TERM_KEY)
go

alter table CS_PA_VEH_COVG_TERM_BASE
   add constraint CPVCTB_AK1 unique (PA_VEH_COVG_TERM_BID)
go

/*==============================================================*/
/* Index: CPVCTB_CPVCB_XFK                                      */
/*==============================================================*/




create nonclustered index CPVCTB_CPVCB_XFK on CS_PA_VEH_COVG_TERM_BASE (PA_VEH_COVG_KEY ASC)
go

/*==============================================================*/
/* Table: CS_PA_VEH_COVG_TERM_DELTA                             */
/*==============================================================*/
create table CS_PA_VEH_COVG_TERM_DELTA (
   PA_VEH_COVG_TERM_DID int                  not null,
   PA_VEH_COVG_TERM_KEY varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   COVG_CD              varchar(255)         not null,
   COVG_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_VEH_COVG_TERM_DELTA
   add constraint CPVCTD_PK primary key nonclustered (PA_VEH_COVG_TERM_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_PA_VEH_COVG_TERM_DELTA
   add constraint CPVCTD_AK1 unique (PA_VEH_COVG_TERM_DID)
go

/*==============================================================*/
/* Table: CS_PA_VEH_DELTA                                       */
/*==============================================================*/
create table CS_PA_VEH_DELTA (
   PA_VEH_DID           int                  not null,
   VEH_KEY              varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   GARAGE_LOC_KEY       varchar(100)         not null,
   VEH_NO               varchar(255)         not null,
   MODEL_YR             int                  not null,
   MAKE_NAME            varchar(255)         not null,
   MODEL_NAME           varchar(255)         not null,
   VIN_NO               varchar(255)         not null,
   COLOR_NAME           varchar(255)         not null,
   BODY_TYPE_CD         varchar(255)         not null,
   VEH_TYPE_CD          varchar(255)         not null,
   LIC_JURS_CD          varchar(255)         not null,
   LIC_PLATE_NO         varchar(255)         not null,
   LEASE_RENTAL_FL      char(1)              not null,
   LEN_OF_LEASE_CD      varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   ORIG_LIST_PRICE_AMT  numeric(18,2)        not null,
   STATED_VAL_AMT       numeric(18,2)        not null,
   PRIM_USE_CD          varchar(255)         not null,
   ANNUAL_MILES_NO      int                  not null,
   COMMUTING_MILES_NO   int                  not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_VEH_DELTA
   add constraint CPVD_PK primary key nonclustered (VEH_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_PA_VEH_DELTA
   add constraint CPVD_AK1 unique (PA_VEH_DID)
go

/*==============================================================*/
/* Index: CPVD_CLBA_XFK                                         */
/*==============================================================*/




create nonclustered index CPVD_CLBA_XFK on CS_PA_VEH_DELTA (GARAGE_LOC_KEY ASC)
go

/*==============================================================*/
/* Table: CS_PA_VEH_DRVR_BASE                                   */
/*==============================================================*/
create table CS_PA_VEH_DRVR_BASE (
   PA_VEH_DRVR_BID      int                  not null,
   PA_VEH_DRVR_KEY      varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   VEH_KEY              varchar(100)         not null,
   PA_DRVR_KEY          varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_VEH_DRVR_BASE
   add constraint CPVDB_PK primary key nonclustered (PA_VEH_DRVR_KEY)
go

alter table CS_PA_VEH_DRVR_BASE
   add constraint CPVDB_AK1 unique (PA_VEH_DRVR_BID)
go

/*==============================================================*/
/* Index: CPVDB_CPVB_XFK                                        */
/*==============================================================*/




create nonclustered index CPVDB_CPVB_XFK on CS_PA_VEH_DRVR_BASE (VEH_KEY ASC)
go

/*==============================================================*/
/* Index: CPVDB_CPBA_XFK                                        */
/*==============================================================*/




create nonclustered index CPVDB_CPBA_XFK on CS_PA_VEH_DRVR_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CPVDB_CPDB_XFK                                        */
/*==============================================================*/




create nonclustered index CPVDB_CPDB_XFK on CS_PA_VEH_DRVR_BASE (PA_DRVR_KEY ASC)
go

/*==============================================================*/
/* Table: CS_PA_VEH_DRVR_DELTA                                  */
/*==============================================================*/
create table CS_PA_VEH_DRVR_DELTA (
   PA_VEH_DRVR_DID      int                  not null,
   PA_VEH_DRVR_KEY      varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   USE_PCT              numeric(6,3)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_VEH_DRVR_DELTA
   add constraint CPVDD_PK primary key nonclustered (PA_VEH_DRVR_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_PA_VEH_DRVR_DELTA
   add constraint CPVDD_AK1 unique (PA_VEH_DRVR_DID)
go

/*==============================================================*/
/* Table: CS_PA_VEH_MOD_BASE                                    */
/*==============================================================*/
create table CS_PA_VEH_MOD_BASE (
   PA_VEH_MOD_BID       int                  not null,
   PA_VEH_MOD_KEY       varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   VEH_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_VEH_MOD_BASE
   add constraint CPVMB_PK primary key nonclustered (PA_VEH_MOD_KEY)
go

alter table CS_PA_VEH_MOD_BASE
   add constraint CPVMB_AK1 unique (PA_VEH_MOD_BID)
go

/*==============================================================*/
/* Index: CPVMB_CPVB_XFK                                        */
/*==============================================================*/




create nonclustered index CPVMB_CPVB_XFK on CS_PA_VEH_MOD_BASE (VEH_KEY ASC)
go

/*==============================================================*/
/* Index: CPVMB_CPBA_XFK                                        */
/*==============================================================*/




create nonclustered index CPVMB_CPBA_XFK on CS_PA_VEH_MOD_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_PA_VEH_MOD_DELTA                                   */
/*==============================================================*/
create table CS_PA_VEH_MOD_DELTA (
   PA_VEH_MOD_DID       int                  not null,
   PA_VEH_MOD_KEY       varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   MOD_CD               varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   MOD_VAL_TEXT         varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PA_VEH_MOD_DELTA
   add constraint CPVMD_PK primary key nonclustered (PA_VEH_MOD_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_PA_VEH_MOD_DELTA
   add constraint CPVMD_AK1 unique (PA_VEH_MOD_DID)
go

/*==============================================================*/
/* Table: CS_POLICY_BASE                                        */
/*==============================================================*/
create table CS_POLICY_BASE (
   POL_BASE_ID          int                  not null,
   POL_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ACCT_KEY             varchar(100)         not null,
   POL_NO               varchar(255)         not null,
   POL_EFF_DT           DATE                 not null,
   POL_EXP_DT           DATE                 not null,
   ORIG_EFF_DT          DATE                 null,
   NEW_BUS_FL           char(1)              not null,
   POL_TERM             varchar(255)         not null,
   ENTITY_TYPE_CD       varchar(255)         not null,
   PRIM_STATE_CD        varchar(255)         not null,
   LAST_STATUS_ID       int                  null,
   LAST_STATUS_PROC_DTS DATETIME2            null,
   LAST_STATUS_EFF_DT   DATE                 null,
   CLAIMS_MADE_FL       char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_POLICY_BASE
   add constraint CPBA1_PK primary key nonclustered (POL_KEY)
go

alter table CS_POLICY_BASE
   add constraint CPBA1_AK1 unique (POL_BASE_ID)
go

/*==============================================================*/
/* Index: CPBA_CCAB_XFK                                         */
/*==============================================================*/




create nonclustered index CPBA_CCAB_XFK on CS_POLICY_BASE (ACCT_KEY ASC)
go

/*==============================================================*/
/* Index: CPBA_CPSD_XFK                                         */
/*==============================================================*/




create nonclustered index CPBA_CPSD_XFK on CS_POLICY_BASE (LAST_STATUS_ID ASC)
go

/*==============================================================*/
/* Table: CS_POLICY_DELTA                                       */
/*==============================================================*/
create table CS_POLICY_DELTA (
   POLICY_ID            int                  not null,
   POL_KEY              varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_DELETE_DTS       DATETIME2            null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   AGCY_KEY             varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   PRODUCT_ID           int                  not null,
   ORG_ID               int                  not null,
   PRIM_NAMED_INSURED_KEY varchar(100)         not null,
   PAY_PLAN_CD          varchar(255)         not null,
   BILL_PLAN_CD         varchar(255)         not null,
   POL_FORM_CD          varchar(255)         not null,
   SIC_CD               varchar(255)         not null,
   UNDERWRITER_CD       varchar(255)         not null,
   AUDIT_PLAN_CD        varchar(255)         not null,
   ELECT_FUNDS_TFR_FL   char(1)              not null,
   RETRO_FL             char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_POLICY_DELTA
   add constraint CPDE1_PK primary key nonclustered (POL_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_POLICY_DELTA
   add constraint CPDE1_AK1 unique (POLICY_ID)
go

/*==============================================================*/
/* Index: CPDE_CABA_XFK                                         */
/*==============================================================*/




create nonclustered index CPDE_CABA_XFK on CS_POLICY_DELTA (AGNT_KEY ASC)
go

/*==============================================================*/
/* Index: CPDE_CABA1_XFK                                        */
/*==============================================================*/




create nonclustered index CPDE_CABA1_XFK on CS_POLICY_DELTA (AGCY_KEY ASC)
go

/*==============================================================*/
/* Index: CPDE_CPDI_XFK                                         */
/*==============================================================*/




create nonclustered index CPDE_CPDI_XFK on CS_POLICY_DELTA (PRODUCT_ID ASC)
go

/*==============================================================*/
/* Index: CPDE_CODI_XFK                                         */
/*==============================================================*/




create nonclustered index CPDE_CODI_XFK on CS_POLICY_DELTA (ORG_ID ASC)
go

/*==============================================================*/
/* Index: CPDE_CNIB_XFK                                         */
/*==============================================================*/




create nonclustered index CPDE_CNIB_XFK on CS_POLICY_DELTA (PRIM_NAMED_INSURED_KEY ASC)
go

/*==============================================================*/
/* Table: CS_POLICY_STATUS_DIM                                  */
/*==============================================================*/
create table CS_POLICY_STATUS_DIM (
   POL_STATUS_ID        int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   STATUS_CD            varchar(255)         not null,
   REASON_CD            varchar(255)         not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_POLICY_STATUS_DIM
   add constraint CPSD_PK primary key nonclustered (POL_STATUS_ID)
go

/*==============================================================*/
/* Table: CS_POLICY_STATUS_TRAN                                 */
/*==============================================================*/
create table CS_POLICY_STATUS_TRAN (
   POL_STATUS_TRANS_ID  int                  identity,
   POL_KEY              varchar(100)         not null,
   POL_STATUS_ID        int                  not null,
   ACCTG_PRD_ID         int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   STATUS_PROC_DTS      DATETIME2            not null,
   STATUS_EFF_DT        DATE                 not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_POLICY_STATUS_TRAN
   add constraint CPST_PK primary key nonclustered (POL_STATUS_TRANS_ID)
go

/*==============================================================*/
/* Index: CPST_CPSD_XFK                                         */
/*==============================================================*/




create nonclustered index CPST_CPSD_XFK on CS_POLICY_STATUS_TRAN (POL_STATUS_ID ASC)
go

/*==============================================================*/
/* Index: CPST_CMDI_XFK                                         */
/*==============================================================*/




create nonclustered index CPST_CMDI_XFK on CS_POLICY_STATUS_TRAN (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: CPST_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CPST_CPBA_XFK on CS_POLICY_STATUS_TRAN (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_POL_LINE_BASE                                      */
/*==============================================================*/
create table CS_POL_LINE_BASE (
   POL_LINE_BID         int                  identity,
   POL_LINE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   POL_KEY              varchar(100)         not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_POL_LINE_BASE
   add constraint CPLB_PK primary key nonclustered (POL_LINE_KEY)
go

alter table CS_POL_LINE_BASE
   add constraint CPLB_AK1 unique (POL_LINE_BID)
go

/*==============================================================*/
/* Index: CPLB_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CPLB_CPBA_XFK on CS_POL_LINE_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_POL_PARTY_BASE                                     */
/*==============================================================*/
create table CS_POL_PARTY_BASE (
   POL_PARTY_BID        int                  not null,
   POL_PARTY_KEY        varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_POL_PARTY_BASE
   add constraint CPPB_PK primary key nonclustered (POL_PARTY_KEY)
go

alter table CS_POL_PARTY_BASE
   add constraint CPPB_AK1 unique (POL_PARTY_BID)
go

/*==============================================================*/
/* Index: CPPB_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CPPB_CPBA_XFK on CS_POL_PARTY_BASE (POL_KEY ASC)
go

/*==============================================================*/
/* Table: CS_POL_PARTY_DELTA                                    */
/*==============================================================*/
create table CS_POL_PARTY_DELTA (
   POL_PARTY_DID        int                  not null,
   POL_PARTY_KEY        varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PARTY_KEY            varchar(100)         not null,
   PARTY_ROLE_TYPE_CD   varchar(255)         not null,
   PARTY_NAME           varchar(255)         not null,
   BIRTH_DT             DATE                 null,
   MARITAL_STATUS_CD    varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_POL_PARTY_DELTA
   add constraint CPPD_PK primary key nonclustered (POL_PARTY_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table CS_POL_PARTY_DELTA
   add constraint CPPD_AK1 unique (POL_PARTY_DID)
go

/*==============================================================*/
/* Index: CPPD_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CPPD_CPBA_XFK on CS_POL_PARTY_DELTA (PARTY_KEY ASC)
go

/*==============================================================*/
/* Table: CS_PREM_TRAN                                          */
/*==============================================================*/
create table CS_PREM_TRAN (
   PREM_TRANS_ID        int                  identity,
   SOURCE_SYSTEM        varchar(10)          not null,
   POL_KEY              varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   RISK_KEY             varchar(100)         not null,
   AGCY_KEY             varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   COVG_LIMIT_KEY       varchar(100)         not null,
   REC_TYPE_KEY         varchar(100)         not null,
   COVG_ID              int                  not null,
   SCHED_ITEM_CLASS_KEY varchar(100)         null,
   ACCTG_PRD_ID         int                  not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ORIG_TRANS_PROC_DTS  DATETIME2            not null,
   ORIG_END_EFF_DT      DATE                 not null,
   TRANS_EFF_DT         DATE                 not null,
   TRANS_EXP_DT         DATE                 not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   STTLMNT_PREM_BASIS_AMT numeric(18,2)        null,
   STTLMNT_CURR_CD      varchar(255)         not null,
   STTLMNT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_STTLMNT_RATE numeric(15,10)       null,
   REG_RPT_PREM_BASIS_AMT numeric(18,2)        null,
   REG_RPT_CURR_CD      varchar(255)         not null,
   REG_RPT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_REG_RPT_RATE numeric(15,10)       null,
   RPT_PREM_BASIS_AMT   numeric(18,2)        null,
   RPT_CURR_CD          varchar(255)         not null,
   RPT_TRANS_AMT        numeric(18,2)        not null,
   TRANS_TO_RPT_RATE    numeric(15,10)       not null,
   RISK_TYPE_CD         varchar(255)         not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   COMM_PCT             numeric(6,3)         not null,
   EST_PREM_AMT         numeric(18,2)        not null,
   RPT_PREM_AMT         numeric(18,2)        not null,
   AUDIT_PREM_AMT       numeric(18,2)        not null,
   PREM_BASIS_AMT       numeric(18,2)        not null,
   AUDITABLE_FL         char(1)              not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PREM_TRAN
   add constraint CPTR_PK primary key nonclustered (PREM_TRANS_ID)
go

/*==============================================================*/
/* Index: CPTR_CRTD_XFK                                         */
/*==============================================================*/




create nonclustered index CPTR_CRTD_XFK on CS_PREM_TRAN (REC_TYPE_KEY ASC)
go

/*==============================================================*/
/* Index: CPTR_CPBA_XFK                                         */
/*==============================================================*/




create nonclustered index CPTR_CPBA_XFK on CS_PREM_TRAN (POL_KEY ASC)
go

/*==============================================================*/
/* Index: CPTR_CLBA_XFK                                         */
/*==============================================================*/




create nonclustered index CPTR_CLBA_XFK on CS_PREM_TRAN (LOC_KEY ASC)
go

/*==============================================================*/
/* Index: CPTR_CABA_XFK                                         */
/*==============================================================*/




create nonclustered index CPTR_CABA_XFK on CS_PREM_TRAN (AGNT_KEY ASC)
go

/*==============================================================*/
/* Index: CPTR_CABA1_XFK                                        */
/*==============================================================*/




create nonclustered index CPTR_CABA1_XFK on CS_PREM_TRAN (AGCY_KEY ASC)
go

/*==============================================================*/
/* Index: CPTR_CMDI_XFK                                         */
/*==============================================================*/




create nonclustered index CPTR_CMDI_XFK on CS_PREM_TRAN (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Table: CS_PRODUCT_DIM                                        */
/*==============================================================*/
create table CS_PRODUCT_DIM (
   PRODUCT_ID           int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   LOB_CD               varchar(255)         not null,
   LINE_DIV_CD          varchar(255)         not null,
   PROD_CD              varchar(255)         not null,
   PROG_CD              varchar(255)         not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_PRODUCT_DIM
   add constraint CPDI_PK primary key nonclustered (PRODUCT_ID)
go

alter table CS_PRODUCT_DIM
   add constraint CPDI_AK1 unique (SOURCE_SYSTEM, LINE_DIV_CD, LOB_CD, PROD_CD, PROG_CD)
go

/*==============================================================*/
/* Table: CS_QUARTER_DIM                                        */
/*==============================================================*/
create table CS_QUARTER_DIM (
   QTR_ID               int                  not null,
   QTR_NO               char(1)              not null,
   QTR_FIRST_MTH_NO     char(2)              not null,
   QTR_BEGIN_DT         DATE                 not null,
   QTR_END_DT           DATE                 not null,
   YR_NO                int                  not null,
   YR_BEGIN_DT          DATE                 not null,
   YR_END_DT            DATE                 not null
)
go

alter table CS_QUARTER_DIM
   add constraint CQDI_PK primary key nonclustered (QTR_ID)
go

/*==============================================================*/
/* Table: CS_RECORD_TYPE_DIM                                    */
/*==============================================================*/
create table CS_RECORD_TYPE_DIM (
   REC_TYPE_ID          int                  not null,
   REC_TYPE_KEY         varchar(100)         not null,
   REC_TYPE_CD          varchar(255)         not null,
   REC_TYPE_TEXT        varchar(255)         not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_RECORD_TYPE_DIM
   add constraint CRTD_PK primary key nonclustered (REC_TYPE_KEY)
go

alter table CS_RECORD_TYPE_DIM
   add constraint CRTD_AK1 unique (REC_TYPE_ID)
go

/*==============================================================*/
/* Table: CS_REGION_DIM                                         */
/*==============================================================*/
create table CS_REGION_DIM (
   REGION_ID            int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   REGION_CD            varchar(255)         not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_REGION_DIM
   add constraint CRDI_PK primary key nonclustered (REGION_ID)
go

alter table CS_REGION_DIM
   add constraint CRDI_AK1 unique (SOURCE_SYSTEM, REGION_CD)
go

/*==============================================================*/
/* Table: CS_USER_AGENT_RLS                                     */
/*==============================================================*/
create table CS_USER_AGENT_RLS (
   USER_AGNT_RLS_ID     int                  not null,
   USER_AGNT_KEY        varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PC_USERNAME          varchar(255)         not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_USER_AGENT_RLS
   add constraint CUAR_PK primary key nonclustered (USER_AGNT_KEY)
go

alter table CS_USER_AGENT_RLS
   add constraint CUAR_AK1 unique (USER_AGNT_RLS_ID)
go

/*==============================================================*/
/* Table: CS_VENDOR_BASE                                        */
/*==============================================================*/
create table CS_VENDOR_BASE (
   VNDR_BASE_ID         int                  not null,
   VNDR_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   VNDR_CD              varchar(255)         not null,
   VNDR_TYPE_CD         varchar(255)         not null,
   ENTITY_TYPE_CD       varchar(255)         not null,
   VNDR_NAME            varchar(255)         not null,
   VNDR_ADDR_1          varchar(255)         not null,
   VNDR_ADDR_2          varchar(255)         not null,
   VNDR_ADDR_3          varchar(255)         not null,
   VNDR_CITY            varchar(255)         not null,
   VNDR_STATE_CD        varchar(255)         not null,
   VNDR_ZIP_CD          varchar(15)          not null,
   VNDR_TAX_ID          varchar(15)          not null,
   VNDR_PHONE_NO        varchar(50)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_VENDOR_BASE
   add constraint CVBA_PK primary key nonclustered (VNDR_KEY)
go

alter table CS_VENDOR_BASE
   add constraint CVBA_AK1 unique (VNDR_BASE_ID)
go

/*==============================================================*/
/* Table: CS_VENDOR_DELTA                                       */
/*==============================================================*/
create table CS_VENDOR_DELTA (
   VENDOR_ID            int                  not null,
   VNDR_KEY             varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table CS_VENDOR_DELTA
   add constraint CVDE_PK primary key nonclustered (VNDR_KEY, ETL_ROW_EFF_DTS)
go

alter table CS_VENDOR_DELTA
   add constraint CVDE_AK1 unique (VENDOR_ID)
go

/*==============================================================*/
/* Table: ETL_EMPTY                                             */
/*==============================================================*/
create table ETL_EMPTY (
   EMPTY_COL            int                  not null
)
go

alter table ETL_EMPTY
   add constraint ETEM_PK primary key nonclustered (EMPTY_COL)
go

/*==============================================================*/
/* Table: ETL_ENGINE_ACTIVITY                                   */
/*==============================================================*/
create table ETL_ENGINE_ACTIVITY (
   ID                   int                  identity,
   CYCLE_ID             int                  not null,
   PROC_NAME            varchar(255)         not null,
   TAB_NAME             varchar(128)         not null,
   OBJECT_NAME          varchar(128)         not null,
   DROP_STMNT           varchar(4000)        not null,
   CREATE_STMNT         varchar(4000)        not null,
   DROP_STATUS_CD       varchar(255)         not null,
   CREATE_STATUS_CD     varchar(255)         not null
)
go

alter table ETL_ENGINE_ACTIVITY
   add constraint CKC_DROP_STATUS_CD_ETL_ENGI check (DROP_STATUS_CD in ('NEW','COMPLETE','READY','SKIPPED','FAILED') and DROP_STATUS_CD = upper(DROP_STATUS_CD))
go

alter table ETL_ENGINE_ACTIVITY
   add constraint CKC_CREATE_STATUS_CD_ETL_ENGI check (CREATE_STATUS_CD in ('NEW','COMPLETE','READY','SKIPPED','FAILED') and CREATE_STATUS_CD = upper(CREATE_STATUS_CD))
go

alter table ETL_ENGINE_ACTIVITY
   add constraint EEAC_PK primary key nonclustered (ID)
go

alter table ETL_ENGINE_ACTIVITY
   add constraint EEAC_AK1 unique (CYCLE_ID, PROC_NAME, TAB_NAME, OBJECT_NAME)
go

/*==============================================================*/
/* Table: ETL_ENGINE_LOG                                        */
/*==============================================================*/
create table ETL_ENGINE_LOG (
   ID                   int                  identity,
   LOG_DTS              DATETIME2            not null,
   SRCE_NAME            varchar(100)         null,
   SECT_NAME            varchar(100)         null,
   DEBUG_LEVEL          varchar(255)         not null,
   LOG_MSG              varchar(Max)         null
)
go

alter table ETL_ENGINE_LOG
   add constraint EELO_PK primary key nonclustered (ID)
go

/*==============================================================*/
/* Table: ETL_ENGINE_STATS                                      */
/*==============================================================*/
create table ETL_ENGINE_STATS (
   STATS_ID             int                  identity,
   CYCLE_ID             int                  not null,
   METHOD_NAME          varchar(255)         not null,
   TAB_NAME             varchar(128)         not null,
   ACTION_CD            varchar(255)         not null,
   ROW_CNT              bigint               not null,
   DURATION_SECONDS     int                  not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table ETL_ENGINE_STATS
   add constraint EEST_PK primary key nonclustered (CYCLE_ID, METHOD_NAME, TAB_NAME, ACTION_CD)
go

alter table ETL_ENGINE_STATS
   add constraint EEST_AK1 unique (STATS_ID)
go

/*==============================================================*/
/* Table: ETL_EXTRACT_CONTROL_PARM                              */
/*==============================================================*/
create table ETL_EXTRACT_CONTROL_PARM (
   CONFIG               varchar(255)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   START_EXTRACT_DATE   DATETIME2            not null,
   END_EXTRACT_DATE     DATETIME2            not null,
   LOAD_TYPE            varchar(255)         not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table ETL_EXTRACT_CONTROL_PARM
   add constraint EECP_PK primary key nonclustered (CONFIG, SOURCE_SYSTEM)
go

/*==============================================================*/
/* Table: ETL_EXTRACT_STATS                                     */
/*==============================================================*/
create table ETL_EXTRACT_STATS (
   EXTRACT_STATS_ID     int                  identity,
   CYCLE_ID             int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   STAGE_TAB_NAME       varchar(128)         not null,
   PART_NO              varchar(255)         not null,
   START_EXTRACT_DTS    DATETIME2            not null,
   END_EXTRACT_DTS      DATETIME2            not null,
   ROW_CNT              bigint               not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table ETL_EXTRACT_STATS
   add constraint EEST1_PK primary key nonclustered (CYCLE_ID, SOURCE_SYSTEM, STAGE_TAB_NAME, PART_NO)
go

alter table ETL_EXTRACT_STATS
   add constraint EEST1_AK1 unique (EXTRACT_STATS_ID)
go

/*==============================================================*/
/* Table: ETL_ODS_AUDIT_METRIC                                  */
/*==============================================================*/
create table ETL_ODS_AUDIT_METRIC (
   END_EXTRACT_DATE     DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   AUDIT_KEY            varchar(100)         not null,
   AUDIT_FROM_DATE      DATETIME2            not null,
   AUDIT_THRU_DATE      DATETIME2            not null,
   ODS_METRIC           numeric(18,2)        not null,
   SOURCE_SYSTEM_METRIC numeric(18,2)        not null,
   HAS_SS_METRIC_FL     char(1)              not null,
   CURR_CD              varchar(255)         not null,
   AUDIT_DIFF           numeric(18,2)        not null,
   AUDIT_DIFF_DELTA     numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ADD_DTS          DATETIME2            null
)
go

alter table ETL_ODS_AUDIT_METRIC
   add constraint EOAM_PK primary key nonclustered (END_EXTRACT_DATE, SOURCE_SYSTEM, AUDIT_KEY, CURR_CD)
go

/*==============================================================*/
/* Table: ETL_ODS_CYCLE_CONTROL_PARM                            */
/*==============================================================*/
create table ETL_ODS_CYCLE_CONTROL_PARM (
   CONFIG               varchar(255)         not null,
   RUN_MODE             varchar(255)         not null,
   BYPASS_ROLLBACK_FL   char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table ETL_ODS_CYCLE_CONTROL_PARM
   add constraint EOCCP_PK primary key nonclustered (CONFIG)
go

/*==============================================================*/
/* Table: ETL_ODS_CYCLE_STATISTIC                               */
/*==============================================================*/
create table ETL_ODS_CYCLE_STATISTIC (
   SOURCE_SYSTEM        varchar(10)          not null,
   CYCLE_DATE           DATETIME2            not null,
   ENTITY               varchar(50)          not null,
   ROW_CNT_EXTRACT      int                  not null,
   DI_B_LEVEL_ERRORS    int                  not null,
   DI_E_LEVEL_ERRORS    int                  not null,
   DI_F_LEVEL_ERRORS    int                  not null,
   ROW_CNT_TRF          int                  not null,
   AMT_TRF_01           numeric(18,2)        not null,
   AMT_TRF_02           numeric(18,2)        not null,
   AMT_TRF_03           numeric(18,2)        not null,
   AMT_TRF_04           numeric(18,2)        not null,
   AMT_TRF_05           numeric(18,2)        not null,
   AMT_TRF_06           numeric(18,2)        not null,
   AMT_TRF_07           numeric(18,2)        not null,
   AMT_TRF_08           numeric(18,2)        not null,
   AMT_TRF_09           numeric(18,2)        not null,
   AMT_TRF_10           numeric(18,2)        not null,
   AMT_TRF_11           numeric(18,2)        not null,
   AMT_TRF_12           numeric(18,2)        not null,
   ROW_CNT_INSERT_ODS   int                  not null,
   ROW_CNT_UPDATE_ODS   int                  not null,
   ROW_CNT_INSERT_DELTA_ODS int                  not null,
   ROW_CNT_UPDATE_DELTA_ODS int                  not null,
   ROW_CNT_LATE_ODS     int                  not null,
   ROW_CNT_TOT_ODS      int                  not null,
   ROW_CNT_TOT_DELTA_ODS int                  not null,
   AMT_ODS_01           numeric(18,2)        not null,
   AMT_ODS_02           numeric(18,2)        not null,
   AMT_ODS_03           numeric(18,2)        not null,
   AMT_ODS_04           numeric(18,2)        not null,
   AMT_ODS_05           numeric(18,2)        not null,
   AMT_ODS_06           numeric(18,2)        not null,
   AMT_ODS_07           numeric(18,2)        not null,
   AMT_ODS_08           numeric(18,2)        not null,
   AMT_ODS_09           numeric(18,2)        not null,
   AMT_ODS_10           numeric(18,2)        not null,
   AMT_ODS_11           numeric(18,2)        not null,
   AMT_ODS_12           numeric(18,2)        not null,
   BALANCED_FL          char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ADD_DTS          DATETIME2            null
)
go

alter table ETL_ODS_CYCLE_STATISTIC
   add constraint EOCS1_PK primary key nonclustered (SOURCE_SYSTEM, CYCLE_DATE, ENTITY)
go

/*==============================================================*/
/* Table: ETL_ODS_CYCLE_STATUS                                  */
/*==============================================================*/
create table ETL_ODS_CYCLE_STATUS (
   CYCLE_ID             int                  not null,
   CYCLE_START_DATE     DATETIME2            not null,
   CYCLE_END_DATE       DATETIME2            null,
   CYCLE_STATUS         varchar(255)         not null,
   CYCLE_ROLLED_BACK_FL char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table ETL_ODS_CYCLE_STATUS
   add constraint EOCS_PK primary key nonclustered (CYCLE_ID)
go

/*==============================================================*/
/* Table: ETL_TRF_CONTROL                                       */
/*==============================================================*/
create table ETL_TRF_CONTROL (
   SOURCE_SYSTEM        varchar(10)          not null,
   ENTITY               varchar(50)          not null,
   EXTRACT_PATH         varchar(255)         not null,
   EXTRACT_FILENAME     varchar(255)         not null,
   EXTRACT_FROM_DATE    DATETIME2            not null,
   EXTRACT_TO_DATE      DATETIME2            not null,
   AMT_CTL_01           numeric(18,2)        not null,
   AMT_CTL_02           numeric(18,2)        not null,
   AMT_CTL_03           numeric(18,2)        not null,
   AMT_CTL_04           numeric(18,2)        not null,
   AMT_CTL_05           numeric(18,2)        not null,
   AMT_CTL_06           numeric(18,2)        not null,
   AMT_CTL_07           numeric(18,2)        not null,
   AMT_CTL_08           numeric(18,2)        not null,
   AMT_CTL_09           numeric(18,2)        not null,
   AMT_CTL_10           numeric(18,2)        not null,
   AMT_CTL_11           numeric(18,2)        not null,
   AMT_CTL_12           numeric(18,2)        not null,
   ROW_CNT_CTL          int                  not null,
   AMT_TRF_01           numeric(18,2)        not null,
   AMT_TRF_02           numeric(18,2)        not null,
   AMT_TRF_03           numeric(18,2)        not null,
   AMT_TRF_04           numeric(18,2)        not null,
   AMT_TRF_05           numeric(18,2)        not null,
   AMT_TRF_06           numeric(18,2)        not null,
   AMT_TRF_07           numeric(18,2)        not null,
   AMT_TRF_08           numeric(18,2)        not null,
   AMT_TRF_09           numeric(18,2)        not null,
   AMT_TRF_10           numeric(18,2)        not null,
   AMT_TRF_11           numeric(18,2)        not null,
   AMT_TRF_12           numeric(18,2)        not null,
   ROW_CNT_TRF          int                  not null,
   DI_B_LEVEL_ERRORS    int                  not null,
   DI_E_LEVEL_ERRORS    int                  not null,
   DI_F_LEVEL_ERRORS    int                  not null,
   TRANSFORMED_FL       char(1)              not null,
   BALANCED_FL          char(1)              not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null
)
go

alter table ETL_TRF_CONTROL
   add constraint ETCO_PK primary key nonclustered (SOURCE_SYSTEM, ENTITY, EXTRACT_PATH, EXTRACT_FILENAME)
go

/*==============================================================*/
/* Table: ETL_TRF_EXCEPTION                                     */
/*==============================================================*/
create table ETL_TRF_EXCEPTION (
   ERR_ID               int                  identity,
   SOURCE_SYSTEM        varchar(10)          not null,
   ENTITY               varchar(50)          not null,
   EXT_FILENAME         varchar(255)         not null,
   KEYS                 varchar(4000)        not null,
   DATA                 varchar(4000)        not null,
   DI_ERR_ACTION        char(1)              not null,
   DI_ERR_COLUMNS       varchar(4000)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null
)
go

alter table ETL_TRF_EXCEPTION
   add constraint ETEX_PK primary key nonclustered (ERR_ID)
go

/*==============================================================*/
/* Table: ETL_USER_CONFIG                                       */
/*==============================================================*/
create table ETL_USER_CONFIG (
   ID                   int                  identity,
   OPT_NAME             varchar(100)         not null,
   OPT_VAL              varchar(100)         not null,
   OPT_TEXT             varchar(255)         not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table ETL_USER_CONFIG
   add constraint EUCO_PK primary key nonclustered (ID)
go

alter table ETL_USER_CONFIG
   add constraint EUCO_AK1 unique (OPT_NAME)
go

/*==============================================================*/
/* Table: ETL_USER_DEFAULT_LAD_VAL                              */
/*==============================================================*/
create table ETL_USER_DEFAULT_LAD_VAL (
   DEFAULT_VAL_ID       int                  identity,
   LOGICAL_DATA_TYPE_KEY varchar(100)         not null,
   TAB_NAME             varchar(255)         null,
   COL_NAME             varchar(255)         null,
   CL_NAME              varchar(255)         null,
   DEFAULT_VAL          varchar(255)         not null,
   JAVA_DT_FORMAT       varchar(255)         null,
   PRIORITY_NO          int                  not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table ETL_USER_DEFAULT_LAD_VAL
   add constraint CKC_LOGICAL_DATA_TYPE_ETL_USER check (LOGICAL_DATA_TYPE_KEY in ('STRING','NUMBER','DATE') and LOGICAL_DATA_TYPE_KEY = upper(LOGICAL_DATA_TYPE_KEY))
go

alter table ETL_USER_DEFAULT_LAD_VAL
   add constraint EUDLV_PK primary key nonclustered (DEFAULT_VAL_ID)
go

alter table ETL_USER_DEFAULT_LAD_VAL
   add constraint EUDLV_AK1 unique (LOGICAL_DATA_TYPE_KEY, TAB_NAME, COL_NAME, CL_NAME)
go

/*==============================================================*/
/* Table: ETL_USER_EARNING                                      */
/*==============================================================*/
create table ETL_USER_EARNING (
   CONFIG               varchar(255)         not null,
   EARNING_RULE         varchar(255)         not null,
   BEGIN_DT             DATE                 not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null
)
go

alter table ETL_USER_EARNING
   add constraint EUEA_PK primary key nonclustered (CONFIG)
go

/*==============================================================*/
/* Table: ETL_USER_EMAIL_CONFIG                                 */
/*==============================================================*/
create table ETL_USER_EMAIL_CONFIG (
   CONFIG               varchar(255)         not null,
   EMAIL_ON_FAILURE     varchar(255)         not null,
   EMAIL_ON_SUCCESS     varchar(255)         not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table ETL_USER_EMAIL_CONFIG
   add constraint EUEC1_PK primary key nonclustered (CONFIG)
go

/*==============================================================*/
/* Table: ETL_USER_ETL_CONFIG                                   */
/*==============================================================*/
create table ETL_USER_ETL_CONFIG (
   CONFIG               varchar(255)         not null,
   ABORT_ON_ERR_FL      char(1)              not null,
   EMAIL_ON_ERR_FL      char(1)              not null,
   EMAIL_ON_SUCCESS_FL  char(1)              not null,
   USE_TESTBED_FL       char(1)              not null,
   JOBSERVER_OS         varchar(255)         not null,
   DW_DATABASE_TYPE     varchar(255)         not null,
   LOAD_TYPE            varchar(255)         not null,
   ROLLBACK_FL          char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table ETL_USER_ETL_CONFIG
   add constraint EUEC_PK primary key nonclustered (CONFIG)
go

/*==============================================================*/
/* Table: ETL_USER_EXTRACT_ENTITY                               */
/*==============================================================*/
create table ETL_USER_EXTRACT_ENTITY (
   CONFIG               varchar(255)         not null,
   ENTITY               varchar(50)          not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   COMPLETED_DATE       DATETIME2            null,
   START_EXTRACT_DATE_IL DATETIME2            not null,
   INIT_LOAD_FL         char(1)              not null,
   LOAD_FL              char(1)              not null,
   RPT_DATE             DATETIME2            null,
   TEST_BED_SQL_1       varchar(4000)        null,
   TEST_BED_SQL_2       varchar(4000)        null,
   TEST_BED_SQL_3       varchar(4000)        null,
   TEST_BED_SQL_4       varchar(4000)        null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null,
   PARTITION_CNT_SRCE   int                  not null,
   PARTITION_CNT_TARGET int                  not null
)
go

alter table ETL_USER_EXTRACT_ENTITY
   add constraint EUEE_PK primary key nonclustered (CONFIG, ENTITY, SOURCE_SYSTEM)
go

/*==============================================================*/
/* Table: ETL_USER_FILE_PATH                                    */
/*==============================================================*/
create table ETL_USER_FILE_PATH (
   CONFIG               varchar(255)         not null,
   EXTRACT_FILE_PATH    varchar(255)         not null,
   EXTRACT_ERR_FILE_PATH varchar(255)         not null,
   DATAIN_FILE_PATH     varchar(255)         not null,
   ARC_FILE_PATH        varchar(255)         not null,
   UNIX_SCRIPTS_FILE_PATH varchar(255)         not null,
   SHELL_FILE_PATH      varchar(255)         not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table ETL_USER_FILE_PATH
   add constraint EUFP_PK primary key nonclustered (CONFIG)
go

/*==============================================================*/
/* Table: ETL_USER_ROLLUP_CONFIG                                */
/*==============================================================*/
create table ETL_USER_ROLLUP_CONFIG (
   CONFIG               varchar(255)         not null,
   ROLLUP_TYPE_CD       varchar(255)         not null,
   YR_START             int                  null,
   YR_END               int                  null,
   MTH_START            int                  not null,
   MTH_OFFSET           int                  null,
   LOAD_FL              char(1)              not null,
   LAST_MTH_ONLY_FL     char(1)              not null,
   REFRESH_INTERVAL     varchar(255)         not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null
)
go

alter table ETL_USER_ROLLUP_CONFIG
   add constraint EURC_PK primary key nonclustered (CONFIG, ROLLUP_TYPE_CD)
go

/*==============================================================*/
/* Table: ETL_USER_SOURCE_SYSTEM                                */
/*==============================================================*/
create table ETL_USER_SOURCE_SYSTEM (
   CONFIG               varchar(255)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   SOURCE_SYSTEM_TEXT   varchar(255)         not null,
   DATABASE_TYPE        varchar(255)         not null,
   START_EXTRACT_DATE_IL DATETIME2            not null,
   END_EXTRACT_DATE_IL  DATETIME2            not null,
   LOAD_FL              char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table ETL_USER_SOURCE_SYSTEM
   add constraint EUSS_PK primary key nonclustered (CONFIG, SOURCE_SYSTEM)
go

/*==============================================================*/
/* Table: MD_FK_REF                                             */
/*==============================================================*/
create table MD_FK_REF (
   ID                   int                  identity,
   TRF_NAME             varchar(128)         not null,
   FK_TAB_NAME          varchar(128)         not null,
   FK_COL_NAME          varchar(128)         not null,
   PK_TAB_NAME          varchar(128)         not null,
   PK_COL_NAME          varchar(128)         not null,
   LOB_CD               varchar(128)         null,
   FK_CONSTR_NAME       varchar(128)         null,
   OOTB_FUTURE_USE_FL   char(1)              not null
)
go

alter table MD_FK_REF
   add constraint MFRE_PK primary key nonclustered (ID)
go

alter table MD_FK_REF
   add constraint MFRE_AK1 unique (TRF_NAME, FK_TAB_NAME, FK_COL_NAME)
go

/*==============================================================*/
/* Table: REF_COLUMN_ASNMNT                                     */
/*==============================================================*/
create table REF_COLUMN_ASNMNT (
   REF_COL_ASSIGNMENT_ID int                  not null,
   REF_ENTITY_TYPE_CD   varchar(50)          not null,
   TRF_TAB              varchar(128)         not null,
   TRF_COL              varchar(128)         not null,
   CS_TAB               varchar(128)         not null,
   CS_COL               varchar(128)         not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table REF_COLUMN_ASNMNT
   add constraint RCAS_PK primary key nonclustered (REF_COL_ASSIGNMENT_ID)
go

alter table REF_COLUMN_ASNMNT
   add constraint RCAS_AK1 unique (REF_ENTITY_TYPE_CD, TRF_TAB, TRF_COL, CS_TAB, CS_COL)
go

/*==============================================================*/
/* Table: REF_CONF_STRATEGY                                     */
/*==============================================================*/
create table REF_CONF_STRATEGY (
   REF_CONF_STRATEGY_ID int                  not null,
   REF_CONF_TYPE_CD     varchar(255)         not null,
   REF_STRATEGY_ID      int                  not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table REF_CONF_STRATEGY
   add constraint RCST_PK primary key nonclustered (REF_CONF_STRATEGY_ID)
go

alter table REF_CONF_STRATEGY
   add constraint RCST_AK1 unique (REF_CONF_TYPE_CD, REF_STRATEGY_ID)
go

/*==============================================================*/
/* Index: RCST_REST_XFK                                         */
/*==============================================================*/




create nonclustered index RCST_REST_XFK on REF_CONF_STRATEGY (REF_STRATEGY_ID ASC)
go

/*==============================================================*/
/* Table: REF_CONF_TYPE                                         */
/*==============================================================*/
create table REF_CONF_TYPE (
   REF_CONF_TYPE_CD     varchar(255)         not null,
   REF_CONF_TYPE_NAME   varchar(255)         not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table REF_CONF_TYPE
   add constraint RCTY_PK primary key nonclustered (REF_CONF_TYPE_CD)
go

/*==============================================================*/
/* Table: REF_ENTITY_TYPE                                       */
/*==============================================================*/
create table REF_ENTITY_TYPE (
   REF_ENTITY_TYPE_CD   varchar(50)          not null,
   REF_ENTITY_TYPE_NAME varchar(255)         not null,
   LOOKUP_TAB_NAME      varchar(128)         null,
   LOOKUP_CREATE_DTS    DATETIME2            null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table REF_ENTITY_TYPE
   add constraint RETY_PK primary key nonclustered (REF_ENTITY_TYPE_CD)
go

/*==============================================================*/
/* Table: REF_MSTR                                              */
/*==============================================================*/
create table REF_MSTR (
   REF_MASTER_ID        int                  not null,
   REF_ENTITY_TYPE_CD   varchar(50)          not null,
   REF_SOURCE_SYSTEM    varchar(10)          not null,
   REF_CD               varchar(255)         not null,
   SHORT_TEXT           varchar(255)         not null,
   LONG_TEXT            varchar(255)         not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table REF_MSTR
   add constraint REMS_PK primary key nonclustered (REF_MASTER_ID)
go

alter table REF_MSTR
   add constraint REMS_AK1 unique (REF_ENTITY_TYPE_CD, REF_SOURCE_SYSTEM, REF_CD)
go

/*==============================================================*/
/* Index: REMS_RSSY_XFK                                         */
/*==============================================================*/




create nonclustered index REMS_RSSY_XFK on REF_MSTR (REF_SOURCE_SYSTEM ASC)
go

/*==============================================================*/
/* Table: REF_MSTR_CONF                                         */
/*==============================================================*/
create table REF_MSTR_CONF (
   REF_MASTER_CONF_ID   int                  not null,
   REF_STRATEGY_ID      int                  not null,
   TARGET_REF_MASTER_ID int                  not null,
   SRCE_REF_MASTER_ID   int                  not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table REF_MSTR_CONF
   add constraint RMCO_PK primary key nonclustered (REF_MASTER_CONF_ID)
go

alter table REF_MSTR_CONF
   add constraint RMCO_AK1 unique (REF_STRATEGY_ID, TARGET_REF_MASTER_ID, SRCE_REF_MASTER_ID)
go

/*==============================================================*/
/* Index: RMCO_REMS_XFK                                         */
/*==============================================================*/




create nonclustered index RMCO_REMS_XFK on REF_MSTR_CONF (SRCE_REF_MASTER_ID ASC)
go

/*==============================================================*/
/* Index: RMCO_REMS1_XFK                                        */
/*==============================================================*/




create nonclustered index RMCO_REMS1_XFK on REF_MSTR_CONF (TARGET_REF_MASTER_ID ASC)
go

/*==============================================================*/
/* Table: REF_SOURCE_SYSTEM                                     */
/*==============================================================*/
create table REF_SOURCE_SYSTEM (
   REF_SOURCE_SYSTEM    varchar(10)          not null,
   REF_SOURCE_SYSTEM_NAME varchar(255)         not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table REF_SOURCE_SYSTEM
   add constraint RSSY_PK primary key nonclustered (REF_SOURCE_SYSTEM)
go

/*==============================================================*/
/* Table: REF_STRATEGY                                          */
/*==============================================================*/
create table REF_STRATEGY (
   REF_STRATEGY_ID      int                  not null,
   REF_ENTITY_TYPE_CD   varchar(50)          not null,
   TARGET_REF_SOURCE_SYSTEM varchar(10)          not null,
   SRCE_REF_SOURCE_SYSTEM varchar(10)          not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table REF_STRATEGY
   add constraint REST_PK primary key nonclustered (REF_STRATEGY_ID)
go

alter table REF_STRATEGY
   add constraint REST_AK1 unique (REF_ENTITY_TYPE_CD, TARGET_REF_SOURCE_SYSTEM, SRCE_REF_SOURCE_SYSTEM)
go

/*==============================================================*/
/* Index: REST_RSSY_XFK                                         */
/*==============================================================*/




create nonclustered index REST_RSSY_XFK on REF_STRATEGY (TARGET_REF_SOURCE_SYSTEM ASC)
go

/*==============================================================*/
/* Index: REST_RSSY1_XFK                                        */
/*==============================================================*/




create nonclustered index REST_RSSY1_XFK on REF_STRATEGY (SRCE_REF_SOURCE_SYSTEM ASC)
go

/*==============================================================*/
/* Table: SBS_BILL_ACCT                                         */
/*==============================================================*/
create table SBS_BILL_ACCT (
   BILL_ACCT_KEY        varchar(100)         not null,
   BILL_PLAN_KEY        varchar(100)         not null,
   ALLOC_PLAN_KEY       varchar(100)         not null,
   DELNQNT_PLAN_KEY     varchar(100)         not null,
   COLLECTION_AGCY_KEY  varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   SECURITY_ZONE_KEY    varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   CLOSE_DTS            DATETIME2            null,
   EOW_INV_ANCHOR_DT    DATE                 null,
   ACCT_NO              varchar(255)         not null,
   FIRST_TPM_INV_DOM    int                  not null,
   SECOND_TPM_INV_DOM   int                  not null,
   INV_DAY_OF_MTH       int                  not null,
   INV_DAY_OF_WEEK      int                  not null,
   INV_DELIVERY_TYPE_CD varchar(255)         not null,
   FEIN_CD              varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   DELNQNT_STATUS_CD    varchar(255)         not null,
   ACCT_TYPE_CD         varchar(255)         not null,
   DISTR_LMT_TYPE_CD    varchar(255)         not null,
   BILL_DT_TYPE_CD      varchar(255)         not null,
   BILL_LEVEL_CD        varchar(255)         not null,
   ORG_TYPE_CD          varchar(255)         not null,
   ACCT_SEGMENT_CD      varchar(255)         not null,
   ACCT_NAME            varchar(255)         not null,
   ACCT_DBA_NAME        varchar(255)         not null,
   SRVC_TIER_CD         varchar(255)         not null,
   NP_PMT_DISTRB_FL     char(1)              not null,
   COLLECTING_FL        char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_AP_TRAN                                      */
/*==============================================================*/
create table SBS_BILL_AP_TRAN (
   AP_JRNL_KEY          varchar(100)         not null,
   DISTR_ITEM_KEY       varchar(100)         not null,
   CHRG_COMM_KEY        varchar(100)         not null,
   BILL_INV_ITEM_KEY    varchar(100)         not null,
   NEG_WO_KEY           varchar(100)         not null,
   POL_COMM_KEY         varchar(100)         not null,
   PRDCR_KEY            varchar(100)         not null,
   CR_PYBL_OF_PRDCR_KEY varchar(100)         not null,
   COMM_RED_KEY         varchar(100)         not null,
   INCOMMING_PRDCR_PMT_KEY varchar(100)         not null,
   PRDCR_PYBL_TFR_KEY   varchar(100)         not null,
   PRDCR_PMT_KEY        varchar(100)         not null,
   PRDCR_CD_KEY         varchar(100)         not null,
   PRDCR_STMNT_KEY      varchar(100)         not null,
   PYBL_RECEIVER_STMNT_KEY varchar(100)         not null,
   WO_KEY               varchar(100)         not null,
   AGCY_CYCLE_DISTR_KEY varchar(100)         not null,
   CTX_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   TRANS_NO             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   PYBL_CRITERIA_CD     varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_AR_TRAN                                      */
/*==============================================================*/
create table SBS_BILL_AR_TRAN (
   AR_JRNL_KEY          varchar(100)         not null,
   CHRG_KEY             varchar(100)         not null,
   DISTR_ITEM_KEY       varchar(100)         not null,
   FROM_ACCT_KEY        varchar(100)         not null,
   FROM_PRDCR_KEY       varchar(100)         not null,
   BILL_INV_ITEM_KEY    varchar(100)         not null,
   CHRG_COMM_KEY        varchar(100)         not null,
   WO_KEY               varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TRANS_PROC_DT_ID     int                  not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   TRANS_PROC_MTH_ID    int                  not null,
   TRANS_NO             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   WO_CHANNEL_CD        varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_CASH_TRAN                                    */
/*==============================================================*/
create table SBS_BILL_CASH_TRAN (
   CASH_JRNL_KEY        varchar(100)         not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   NEG_WO_KEY           varchar(100)         not null,
   DISB_KEY             varchar(100)         not null,
   PRDCR_KEY            varchar(100)         not null,
   COLTRL_REQ_KEY       varchar(100)         not null,
   SRCE_COLTRL_REQ_KEY  varchar(100)         not null,
   COLTRL_KEY           varchar(100)         not null,
   MON_RCVD_KEY         varchar(100)         not null,
   SUSP_PMT_KEY         varchar(100)         not null,
   UNAPPLIED_FUND_KEY   varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   TRANS_NO             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   WO_CHANNEL_CD        varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_CHRG                                         */
/*==============================================================*/
create table SBS_BILL_CHRG (
   CHRG_KEY             varchar(100)         not null,
   CHRG_PAT_KEY         varchar(100)         not null,
   BILL_INSTR_KEY       varchar(100)         not null,
   PRIM_COMM_PRDCR_CD_KEY varchar(100)         not null,
   INV_STREAM_KEY       varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   CHRG_DTS             DATETIME2            not null,
   WRITTEN_DTS          DATETIME2            null,
   HOLD_RELEASE_DTS     DATETIME2            null,
   HOLD_STATUS_CD       varchar(255)         not null,
   TOT_INSTALLMENT_NO   int                  not null,
   CHRG_GROUP_CD        varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   CHRG_AMT             numeric(18,2)        not null,
   REVERSED_FL          char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_CHRG_COMM                                    */
/*==============================================================*/
create table SBS_BILL_CHRG_COMM (
   CHRG_COMM_KEY        varchar(100)         not null,
   POL_COMM_KEY         varchar(100)         not null,
   CHRG_KEY             varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   PYBL_CRITERIA_CD     varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_CHRG_PAT                                     */
/*==============================================================*/
create table SBS_BILL_CHRG_PAT (
   CHRG_PAT_KEY         varchar(100)         not null,
   TACCT_OWNER_PAT_KEY  varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   CHRG_PAT_TYPE_CD     varchar(255)         not null,
   CHRG_CD              varchar(255)         not null,
   CHRG_NAME            varchar(255)         not null,
   CHRG_CATG_CD         varchar(255)         not null,
   PERIODICITY_CD       varchar(255)         not null,
   PRIORITY_CD          varchar(255)         not null,
   INV_TRTMNT_CD        varchar(255)         not null,
   INC_IN_EQUITY_DATING_FL char(1)              not null,
   RETIRED_FL           char(1)              not null,
   REVERSIBLE_FL        char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_COMM_REDUCT                                  */
/*==============================================================*/
create table SBS_BILL_COMM_REDUCT (
   COMM_RED_KEY         varchar(100)         not null,
   WO_KEY               varchar(100)         not null,
   BILL_INV_ITEM_KEY    varchar(100)         not null,
   CHRG_COMM_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   WRITE_OFF_DTS        DATETIME2            null,
   COMM_RED_TYPE_CD     varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   COMM_RED_AMT         numeric(18,2)        not null,
   REVERSED_FL          char(1)              not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_COMM_SPLAN                                   */
/*==============================================================*/
create table SBS_BILL_COMM_SPLAN (
   COMM_SPLAN_KEY       varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PLAN_KEY             varchar(100)         not null,
   SPLAN_NAME           varchar(255)         not null,
   SPLAN_ORDER          int                  not null,
   PYBL_CRITERIA_CD     varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_COMM_SPLAN_RATE                              */
/*==============================================================*/
create table SBS_BILL_COMM_SPLAN_RATE (
   COMM_SPLAN_RATE_KEY  varchar(100)         not null,
   COMM_SPLAN_KEY       varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   ROLE_CD              varchar(255)         not null,
   COMM_RATE            numeric(6,3)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_CREDIT                                       */
/*==============================================================*/
create table SBS_BILL_CREDIT (
   CR_KEY               varchar(100)         not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   UNAPPLIED_FUND_KEY   varchar(100)         not null,
   REQUESTING_USER_KEY  varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   CR_DTS               DATETIME2            null,
   APPRVL_DTS           DATETIME2            null,
   APPRVL_STATUS_CD     varchar(255)         not null,
   CR_TYPE_CD           varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   CR_AMT               numeric(18,2)        not null,
   RETIRED_FL           char(1)              not null,
   REVERSED_FL          char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_DISB                                         */
/*==============================================================*/
create table SBS_BILL_DISB (
   DISB_KEY             varchar(100)         not null,
   RPT_GROUP_KEY        varchar(100)         not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   PRDCR_KEY            varchar(100)         not null,
   DISTR_KEY            varchar(100)         not null,
   PMT_INSTRUMENT_KEY   varchar(100)         not null,
   COLTRL_KEY           varchar(100)         not null,
   SUSP_PMT_KEY         varchar(100)         not null,
   UNAPPLIED_FUND_KEY   varchar(100)         not null,
   REQUESTING_USER_KEY  varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   ADDR_TEXT            varchar(1333)        not null,
   CURR_CD              varchar(255)         not null,
   DISB_AMT             numeric(18,2)        not null,
   DUE_DT               DATE                 not null,
   APPRVL_DTS           DATETIME2            null,
   CLOSE_DTS            DATETIME2            null,
   APPRVL_STATUS_CD     varchar(255)         not null,
   DISB_NO              varchar(255)         not null,
   MAIL_TO_NAME         varchar(255)         not null,
   MEMO_TEXT            varchar(255)         not null,
   PAY_TO_NAME          varchar(255)         not null,
   REASON_CD            varchar(255)         not null,
   REF_NO               varchar(255)         not null,
   STATUS_CD            varchar(255)         not null,
   DISB_TYPE_CD         varchar(255)         not null,
   VOID_REASON_CD       varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_DISTR                                        */
/*==============================================================*/
create table SBS_BILL_DISTR (
   DISTR_KEY            varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   APPLIED_DTS          DATETIME2            null,
   DISTRIBUTED_DTS      DATETIME2            null,
   REVERSAL_DTS         DATETIME2            null,
   DISTR_TYPE_CD        varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   WO_AMT               numeric(18,2)        not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_DISTR_ITEM                                   */
/*==============================================================*/
create table SBS_BILL_DISTR_ITEM (
   DISTR_ITEM_KEY       varchar(100)         not null,
   BILL_INV_ITEM_KEY    varchar(100)         not null,
   PRDCR_CD_KEY         varchar(100)         not null,
   ACTIVE_DISTR_KEY     varchar(100)         not null,
   REVERSED_DISTR_KEY   varchar(100)         not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   EXECUTED_DTS         DATETIME2            null,
   APPLIED_DTS          DATETIME2            null,
   REVERSED_DTS         DATETIME2            null,
   DISTR_ITEM_TYPE_CD   varchar(255)         not null,
   DISPOSITION_CD       varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   GROSS_APPLY_AMT      numeric(18,2)        not null,
   COMM_APPLY_AMT       numeric(18,2)        not null,
   PMT_COMMENTS         varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_FUNDS_TRNFR                                  */
/*==============================================================*/
create table SBS_BILL_FUNDS_TRNFR (
   FUNDS_TFR_KEY        varchar(100)         not null,
   SRCE_PRDCR_KEY       varchar(100)         not null,
   TARGET_PRDCR_KEY     varchar(100)         not null,
   SRCE_UNAPPLIED_FUND_KEY varchar(100)         not null,
   TARGET_UNAPPLIED_FUND_KEY varchar(100)         not null,
   REQUESTING_USER_KEY  varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   TFR_DTS              DATETIME2            null,
   APPRVL_DTS           DATETIME2            null,
   APPRVL_STATUS_CD     varchar(255)         not null,
   REASON_CD            varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TFR_AMT              numeric(18,2)        not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_GEN_TRAN                                     */
/*==============================================================*/
create table SBS_BILL_GEN_TRAN (
   GENERAL_JRNL_KEY     varchar(100)         not null,
   CR_KEY               varchar(100)         not null,
   NON_RCVBL_DISTR_ITEM_KEY varchar(100)         not null,
   COLTRL_KEY           varchar(100)         not null,
   SUSP_PRDCR_KEY       varchar(100)         not null,
   FUNDS_TFR_KEY        varchar(100)         not null,
   SRCE_ACCT_KEY        varchar(100)         not null,
   SRCE_PRDCR_KEY       varchar(100)         not null,
   SRCE_UNAPPLIED_TACCT_KEY varchar(100)         not null,
   TARGET_ACCT_KEY      varchar(100)         not null,
   TARGET_PRDCR_KEY     varchar(100)         not null,
   TARGET_UNAPPLIED_TACCT_KEY varchar(100)         not null,
   CR_ACCT_KEY          varchar(100)         not null,
   SUSP_ACCT_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   TRANS_NO             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TFR_REASON_CD        varchar(255)         not null,
   TFR_REVERSAL_REASON_CD varchar(255)         not null,
   WO_CHANNEL_CD        varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_INSTR                                        */
/*==============================================================*/
create table SBS_BILL_INSTR (
   BILL_INSTR_KEY       varchar(100)         not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   ISSUANCE_BILL_ACCT_KEY varchar(100)         not null,
   NEW_RENEW_BILL_ACCT_KEY varchar(100)         not null,
   RENEW_BILL_ACCT_KEY  varchar(100)         not null,
   REWRITE_BILL_ACCT_KEY varchar(100)         not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   NEW_BILL_POL_PRD_KEY varchar(100)         not null,
   ASSOC_BILL_POL_PRD_KEY varchar(100)         not null,
   PRIOR_BILL_POL_PRD_KEY varchar(100)         not null,
   COLTRL_REQ_KEY       varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   BILL_INSTR_TYPE_CD   varchar(255)         not null,
   BILL_INSTR_DTS       DATETIME2            null,
   MOD_DTS              DATETIME2            null,
   SPCL_HANDLING_CD     varchar(255)         not null,
   OFFER_NO             varchar(255)         not null,
   PMT_DUE_DT           DATE                 null,
   PRD_START_DT         DATE                 null,
   PRD_END_DT           DATE                 null,
   PMT_RCVD_FL          char(1)              not null,
   CANCEL_TYPE_CD       varchar(255)         not null,
   CANCEL_REASON_TEXT   varchar(255)         not null,
   HOLD_UB_PREM_CHRG_FL char(1)              not null,
   BILL_INSTR_TEXT      varchar(255)         not null,
   FINAL_AUDIT_FL       char(1)              not null,
   TOT_PREM_FL          char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_INV                                          */
/*==============================================================*/
create table SBS_BILL_INV (
   BILL_INV_KEY         varchar(100)         not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   INV_STREAM_KEY       varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   EVENT_DTS            DATETIME2            not null,
   PMT_DUE_DTS          DATETIME2            not null,
   INV_NO               varchar(255)         not null,
   RESEND_NO            varchar(255)         not null,
   INV_TYPE_CD          varchar(255)         not null,
   INV_STATUS_CD        varchar(255)         not null,
   INV_TEXT             varchar(1333)        not null,
   CURR_CD              varchar(255)         not null,
   INV_AMT              numeric(18,2)        not null,
   AD_HOC_FL            char(1)              not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_INV_ITEM                                     */
/*==============================================================*/
create table SBS_BILL_INV_ITEM (
   BILL_INV_ITEM_KEY    varchar(100)         not null,
   BILL_INV_KEY         varchar(100)         not null,
   CHRG_KEY             varchar(100)         not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   EVENT_DTS            DATETIME2            not null,
   PMT_EXCEPT_DTS       DATETIME2            null,
   PROMISE_EXCEPT_DTS   DATETIME2            null,
   INSTALLMENT_NO       varchar(255)         not null,
   LINE_ITEM_NO         varchar(255)         not null,
   INV_ITEM_TYPE_CD     varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   INV_ITEM_AMT         numeric(18,2)        not null,
   COMMENTS             varchar(1333)        not null,
   INV_ITEM_TEXT        varchar(1333)        not null,
   CSTM_PMT_GROUP_TEXT  varchar(1333)        null,
   EXCEPT_CMT           varchar(1333)        not null,
   GROSS_SETTLED_FL     char(1)              not null,
   RETIRED_FL           char(1)              not null,
   REVERSED_FL          char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_MONEY_RCVD                                   */
/*==============================================================*/
create table SBS_BILL_MONEY_RCVD (
   MON_RCVD_KEY         varchar(100)         not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   PAYING_PRDCR_KEY     varchar(100)         not null,
   PROMISING_PRDCR_KEY  varchar(100)         not null,
   BILL_INV_KEY         varchar(100)         not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   PMT_INSTRUMENT_KEY   varchar(100)         not null,
   RPT_GROUP_KEY        varchar(100)         not null,
   UNAPPLIED_FUND_KEY   varchar(100)         not null,
   DISTR_KEY            varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   RCVD_AMT             numeric(18,2)        not null,
   APPLIED_DTS          DATETIME2            null,
   RCVD_DTS             DATETIME2            null,
   REVERSAL_DTS         DATETIME2            null,
   MON_RCVD_TEXT        varchar(1333)        not null,
   NAME                 varchar(255)         not null,
   REF_NO               varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   REVERSAL_REASON_CD   varchar(255)         not null,
   MON_RCVD_TYPE_CD     varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_NEG_WO                                       */
/*==============================================================*/
create table SBS_BILL_NEG_WO (
   NEG_WO_KEY           varchar(100)         not null,
   UNAPPLIED_FUND_KEY   varchar(100)         not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   PRDCR_KEY            varchar(100)         not null,
   REQUESTING_USER_KEY  varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   EXECUTION_DTS        DATETIME2            null,
   APPRVL_DTS           DATETIME2            null,
   APPRVL_STATUS_CD     varchar(255)         not null,
   WO_CHANNEL_CD        varchar(255)         not null,
   NEG_WO_TYPE_CD       varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   WO_AMT               numeric(18,2)        not null,
   REVERSED_FL          char(1)              not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_NR_DISTR_ITEM                                */
/*==============================================================*/
create table SBS_BILL_NR_DISTR_ITEM (
   NON_RCVBL_DISTR_ITEM_KEY varchar(100)         not null,
   ACTIVE_DISTR_KEY     varchar(100)         not null,
   REVERSED_DISTR_KEY   varchar(100)         not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   CURR_CD              varchar(255)         not null,
   COMM_APPLY_AMT       numeric(18,2)        not null,
   GROSS_APPLY_AMT      numeric(18,2)        not null,
   NET_APPLY_AMT        numeric(18,2)        not null,
   EXECUTED_DTS         DATETIME2            null,
   REVERSED_DTS         DATETIME2            null,
   RELEASED_DTS         DATETIME2            null,
   PMT_CMT_TEXT         varchar(255)         not null,
   POL_NO               varchar(255)         not null,
   ITEM_TYPE_CD         varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_OUT_PMT                                      */
/*==============================================================*/
create table SBS_BILL_OUT_PMT (
   OUTGOING_PMT_KEY     varchar(100)         not null,
   DISB_KEY             varchar(100)         not null,
   PRDCR_PMT_KEY        varchar(100)         not null,
   PMT_INSTRUMENT_KEY   varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   ISSUE_DTS            DATETIME2            null,
   REJECTED_DTS         DATETIME2            not null,
   MAIL_TO_NAME         varchar(255)         not null,
   MAIL_TO_ADDR         varchar(255)         not null,
   MEMO                 varchar(255)         not null,
   PD_DTS               DATETIME2            null,
   PAY_TO_NAME          varchar(255)         not null,
   OUTGOING_PMT_TYPE_CD varchar(255)         not null,
   OUTGOING_PMT_STATUS_CD varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   PMT_AMT              numeric(18,2)        not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_PLAN                                         */
/*==============================================================*/
create table SBS_BILL_PLAN (
   PLAN_KEY             varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PLAN_TYPE_CD         varchar(255)         not null,
   PLAN_NAME            varchar(255)         not null,
   PLAN_EFF_DTS         DATETIME2            not null,
   PLAN_EXP_DTS         DATETIME2            not null,
   PLAN_ORDER           int                  not null,
   PLAN_TEXT            varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_PMT_INSTR                                    */
/*==============================================================*/
create table SBS_BILL_PMT_INSTR (
   PMT_INSTRUMENT_KEY   varchar(100)         not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   PRDCR_KEY            varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   TOKEN_TEXT           varchar(255)         not null,
   PMT_INSTRUMENT_TEXT  varchar(255)         not null,
   PMT_METHOD_CD        varchar(255)         not null,
   IMMUTABLE_FL         char(1)              not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_POL                                          */
/*==============================================================*/
create table SBS_BILL_POL (
   BILL_POL_KEY         varchar(100)         not null,
   BILL_ACCT_KEY        varchar(100)         null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   LOB_CD               varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_POL_COMM                                     */
/*==============================================================*/
create table SBS_BILL_POL_COMM (
   POL_COMM_KEY         varchar(100)         not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   PRIM_BILL_POL_PRD_KEY varchar(100)         not null,
   PRDCR_CD_KEY         varchar(100)         not null,
   COMM_SPLAN_KEY       varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   ROLE_CD              varchar(255)         not null,
   COMM_OVERRIDE_PCT    numeric(6,3)         not null,
   DEFAULT_FOR_POL_FL   char(1)              not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_POL_PRD                                      */
/*==============================================================*/
create table SBS_BILL_POL_PRD (
   BILL_POL_PRD_KEY     varchar(100)         not null,
   BILL_POL_KEY         varchar(100)         not null,
   PMT_PLAN_KEY         varchar(100)         not null,
   DELNQNT_PLAN_KEY     varchar(100)         not null,
   RETURN_PREM_PLAN_KEY varchar(100)         not null,
   SECURITY_ZONE_KEY    varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   BILL_METHOD_CD       varchar(255)         not null,
   BOUND_DTS            DATETIME2            null,
   JURS_STATE_CD        varchar(255)         not null,
   UW_CO_CD             varchar(255)         not null,
   CANCEL_TYPE_CD       varchar(255)         not null,
   CANCEL_REASON_TEXT   varchar(255)         not null,
   CANCEL_STATUS_CD     varchar(255)         not null,
   CLOSE_DTS            DATETIME2            null,
   CLOSURE_STATUS_CD    varchar(255)         not null,
   CONFIRM_NOTIF_STATE_CD varchar(255)         not null,
   DBA_NAME             varchar(255)         not null,
   FULL_PAY_DISC_UNTIL_DT DATE                 null,
   OFFER_NO             varchar(255)         not null,
   POL_NO               varchar(255)         not null,
   POL_EFF_DT           DATE                 null,
   POL_EXP_DT           DATE                 null,
   PRIOR_POL_NO         varchar(255)         not null,
   TERM_NO              int                  not null,
   UNDERWRITER_NAME     varchar(255)         not null,
   EQUITY_BUFFER_DAYS   int                  not null,
   EQUITY_WARNINGS_ENABLED_FL char(1)              not null,
   WESTERN_METHOD_FL    char(1)              not null,
   CHRG_HELD_FL         char(1)              not null,
   HELD_FOR_INV_SENDING_FL char(1)              not null,
   UNDER_AUDIT_FL       char(1)              not null,
   TERM_CONFIRMED_FL    char(1)              not null,
   PMT_DISTR_ENABLED_FL char(1)              not null,
   HOLD_INV_WHEN_DELNQNT_FL char(1)              not null,
   FULL_PAY_DISC_EVALUATED_FL char(1)              not null,
   ELIG_FOR_FULL_PAY_DISC_FL char(1)              not null,
   ASSIGNED_RISK_FL     char(1)              not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_PROD                                         */
/*==============================================================*/
create table SBS_BILL_PROD (
   PRDCR_KEY            varchar(100)         not null,
   RPT_GROUP_KEY        varchar(100)         not null,
   AGCY_BILL_PLAN_KEY   varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   SECURITY_ZONE_KEY    varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   PRDCR_TIER_CD        varchar(255)         not null,
   PRDCR_NAME           varchar(255)         not null,
   COMBINED_STATEMENTS_FL char(1)              not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_PROD_CD                                      */
/*==============================================================*/
create table SBS_BILL_PROD_CD (
   PRDCR_CD_KEY         varchar(100)         not null,
   PRDCR_KEY            varchar(100)         not null,
   COMM_PLAN_KEY        varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   PRDCR_CD             varchar(255)         not null,
   ACTIVE_FL            char(1)              not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_PROD_PMT                                     */
/*==============================================================*/
create table SBS_BILL_PROD_PMT (
   PRDCR_PMT_KEY        varchar(100)         not null,
   PRDCR_KEY            varchar(100)         not null,
   RPT_GROUP_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   REVERSAL_DTS         DATETIME2            null,
   REVERSAL_REASON_CD   varchar(255)         not null,
   STATUS_CD            varchar(255)         not null,
   PMT_TYPE_CD          varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_PROD_STMNT                                   */
/*==============================================================*/
create table SBS_BILL_PROD_STMNT (
   PRDCR_STMNT_KEY      varchar(100)         not null,
   PRDCR_KEY            varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   STMNT_DTS            DATETIME2            not null,
   STMNT_NO             varchar(255)         not null,
   STMNT_TYPE_CD        varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_SUSP_PMT                                     */
/*==============================================================*/
create table SBS_BILL_SUSP_PMT (
   SUSP_PMT_KEY         varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   BILL_ACCT_APPLIED_TO_KEY varchar(100)         not null,
   BILL_POL_PRD_APPLIED_TO_KEY varchar(100)         not null,
   BILL_USER_APPLIED_BY_KEY varchar(100)         not null,
   MON_RCVD_KEY         varchar(100)         not null,
   RPT_GROUP_KEY        varchar(100)         not null,
   BILL_USER_REVERSED_BY_KEY varchar(100)         not null,
   PMT_INSTRUMENT_KEY   varchar(100)         not null,
   PRDCR_APPLIED_TO_KEY varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   ACCT_NO              varchar(255)         not null,
   INV_NO               varchar(255)         not null,
   OFFER_NO             varchar(255)         not null,
   OFFER_OPT            varchar(255)         not null,
   PMT_DT               DATETIME2            not null,
   POL_NO               varchar(255)         not null,
   PRDCR_NAME           varchar(255)         not null,
   REF_NO               varchar(255)         not null,
   STATUS_CD            varchar(255)         not null,
   SUSP_PMT_TEXT        varchar(1333)        not null,
   CURR_CD              varchar(255)         not null,
   SUSP_AMT             numeric(18,2)        not null,
   RETIRED_FL           char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_TACCT_OWNER_PAT                              */
/*==============================================================*/
create table SBS_BILL_TACCT_OWNER_PAT (
   TACCT_OWNER_PAT_KEY  varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   TACCT_OWNER_NAME     varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_UNAPP_FUND                                   */
/*==============================================================*/
create table SBS_BILL_UNAPP_FUND (
   UNAPPLIED_FUND_KEY   varchar(100)         not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   BILL_POL_KEY         varchar(100)         not null,
   RPT_GROUP_KEY        varchar(100)         not null,
   TACCT_KEY            varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   UNAPPLIED_FUND_TEXT  varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SBS_BILL_WO                                           */
/*==============================================================*/
create table SBS_BILL_WO (
   WO_KEY               varchar(100)         not null,
   CHRG_PAT_KEY         varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   COMM_AGCY_PMT_ITEM_KEY varchar(100)         not null,
   GROSS_AGCY_PMT_ITEM_KEY varchar(100)         not null,
   CHRG_COMM_KEY        varchar(100)         not null,
   BILL_INV_ITEM_KEY    varchar(100)         not null,
   ITEM_COMM_KEY        varchar(100)         not null,
   PRDCR_KEY            varchar(100)         not null,
   REQUESTING_USER_KEY  varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   EXECUTION_DTS        DATETIME2            null,
   APPRVL_DTS           DATETIME2            null,
   APPRVL_STATUS_CD     varchar(255)         not null,
   REASON_CD            varchar(255)         not null,
   WO_TYPE_CD           varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   WO_AMT               numeric(18,2)        not null,
   REVERSED_AMT         numeric(18,2)        not null,
   RETIRED_FL           char(1)              not null,
   REVERSED_FL          char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: SYSTEM_INFO                                           */
/*==============================================================*/
create table SYSTEM_INFO (
   KEY_NAME             varchar(100)         not null,
   KEY_VAL              varchar(255)         not null
)
go

alter table SYSTEM_INFO
   add constraint SYIN_PK primary key nonclustered (KEY_NAME)
go

/*==============================================================*/
/* Table: TRF_ADDR                                              */
/*==============================================================*/
create table TRF_ADDR (
   ADDR_KEY             varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   STREET_1_NAME        varchar(255)         not null,
   STREET_2_NAME        varchar(255)         not null,
   STREET_3_NAME        varchar(255)         not null,
   CNTRY_CD             varchar(255)         not null,
   REGION_CD            varchar(255)         not null,
   CITY_NAME            varchar(255)         not null,
   POSTAL_CD            varchar(15)          not null,
   COUNTY_NAME          varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_AGENCY                                            */
/*==============================================================*/
create table TRF_AGENCY (
   AGCY_KEY             varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   AGCY_CD              varchar(255)         not null,
   AGCY_ADDR_1          varchar(255)         not null,
   AGCY_ADDR_2          varchar(255)         not null,
   AGCY_ADDR_3          varchar(255)         not null,
   AGCY_CITY            varchar(255)         not null,
   AGCY_STATE_CD        varchar(255)         not null,
   AGCY_ZIP_CD          varchar(15)          not null,
   AGCY_PHONE_NO        varchar(50)          not null,
   AGCY_APPT_DT         DATE                 null,
   AGCY_TERM_DT         DATE                 null,
   AGCY_STATUS_CD       varchar(255)         not null,
   MKT_MANAGER_NAME     varchar(255)         not null,
   MKTG_REP_CD          varchar(255)         not null,
   REGION_CD            varchar(255)         not null,
   AGCY_LIC_NO          varchar(255)         not null,
   AGCY_TAX_ID          varchar(15)          not null,
   AGCY_NAME            varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_AGENT                                             */
/*==============================================================*/
create table TRF_AGENT (
   AGNT_KEY             varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   AGCY_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   AGNT_CD              varchar(255)         not null,
   AGNT_ADDR_1          varchar(255)         not null,
   AGNT_ADDR_2          varchar(255)         not null,
   AGNT_ADDR_3          varchar(255)         not null,
   AGNT_CITY            varchar(255)         not null,
   AGNT_STATE_CD        varchar(255)         not null,
   AGNT_ZIP_CD          varchar(15)          not null,
   AGNT_PHONE_NO        varchar(50)          not null,
   AGNT_APPT_DT         DATE                 null,
   AGNT_TERM_DT         DATE                 null,
   AGNT_STATUS_CD       varchar(255)         not null,
   MKT_MANAGER_NAME     varchar(255)         not null,
   MKTG_REP_CD          varchar(255)         not null,
   REGION_CD            varchar(255)         not null,
   AGNT_LIC_NO          varchar(255)         not null,
   AGNT_TAX_ID          varchar(15)          not null,
   AGNT_NAME            varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_AUDIT                                             */
/*==============================================================*/
create table TRF_AUDIT (
   END_EXTRACT_DATE     DATETIME2            null,
   SOURCE_SYSTEM        varchar(10)          null,
   AUDIT_ENTITY         varchar(50)          null,
   AUDIT_FROM_DATE      DATETIME2            null,
   AUDIT_THRU_DATE      DATETIME2            null,
   AUDIT_METRIC         numeric(18,2)        null,
   CURR_CD              varchar(255)         null
)
go

/*==============================================================*/
/* Table: TRF_AUDIT_INFO                                        */
/*==============================================================*/
create table TRF_AUDIT_INFO (
   AUDIT_INFO_KEY       varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   AUDIT_METHOD_CD      varchar(255)         not null,
   AUDIT_PRD_START_DT   DATE                 not null,
   AUDIT_PRD_END_DT     DATE                 not null,
   AUDIT_SCHED_TYPE_CD  varchar(255)         not null,
   ACT_AUDIT_METHOD_CD  varchar(255)         not null,
   AUDIT_FEE_AMT        numeric(18,2)        null,
   AUDT_FEE_CURR_CD     varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_BLDG                                              */
/*==============================================================*/
create table TRF_BLDG (
   BLDG_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ALARM_CERTIFICATE    varchar(255)         not null,
   ALARM_CERTIFICATION_CD varchar(255)         not null,
   ALARM_CL_CD          varchar(255)         not null,
   ALARM_EXP_DT         DATE                 null,
   ALARM_GRADE_CD       varchar(255)         not null,
   BLDG_FINISHED_AREA_SF_NO int                  null,
   BLDG_UNFINISHED_AREA_SF_NO int                  null,
   BASEMENT_AREA_SF_NO  int                  null,
   AREA_LEASED_CD       varchar(255)         not null,
   BLDG_ALARM_TYPE_CD   varchar(255)         not null,
   BLDG_NO              int                  null,
   BURGLAR_SAFEGUARD_CD varchar(255)         not null,
   CONSTR_TYPE_CD       varchar(255)         not null,
   BLDG_TEXT            varchar(255)         not null,
   EFFECTIVENESS_GRADE_CD varchar(255)         not null,
   HEATING_BOILER_ON_PRMSS_FL char(1)              not null,
   HEATING_BOILER_ELSE_WHERE_FL char(1)              not null,
   BASEMENTS_NO         int                  null,
   STORIES_NO           int                  null,
   UNITS_NO             int                  null,
   PCT_OCCUPIED_CD      varchar(255)         not null,
   PCT_VACANT_CD        varchar(255)         not null,
   PCT_RENTED_TO_OTHERS_CD varchar(255)         not null,
   ROOF_TYPE_CD         varchar(255)         not null,
   SPRINKLER_COVG_CD    varchar(255)         not null,
   BLDG_TYPE_CD         varchar(255)         not null,
   TOT_AREA_SF_NO       int                  null,
   WIND_RATING_CD       varchar(255)         not null,
   BUILD_YR             int                  null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CA_ADDL_INTRST                                    */
/*==============================================================*/
create table TRF_CA_ADDL_INTRST (
   CA_ADDL_INTRST_KEY   varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   ADDL_INTRST_TYPE_CD  varchar(255)         not null,
   CONTRACT_NO          varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CA_COND_TERM                                      */
/*==============================================================*/
create table TRF_CA_COND_TERM (
   CA_COND_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   COND_KEY             varchar(100)         not null,
   CA_COND_TERM_KEY     varchar(100)         not null,
   COND_TERM_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   COND_CD              varchar(255)         not null,
   COND_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CA_COVG_TERM                                      */
/*==============================================================*/
create table TRF_CA_COVG_TERM (
   CA_COVG_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   CA_COVG_TERM_KEY     varchar(100)         not null,
   COVG_TERM_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   COVG_CD              varchar(255)         not null,
   COVG_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CA_DEALER                                         */
/*==============================================================*/
create table TRF_CA_DEALER (
   CA_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   OWNING_CVRBL_KEY     varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   DEALER_LOC_KEY       varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   DEALERS_NO           int                  null,
   GARAGE_DEALERS_CL_CD varchar(255)         not null,
   DEALERS_CL_CD        varchar(255)         not null,
   GARAGE_DEALERS_COVG_TYPE_CD varchar(255)         not null,
   FRAN_FL              char(1)              not null,
   TRNSPR_PLATES_CNT    int                  null,
   SETS_OF_DEALER_PLATES_CNT int                  null,
   RATING_BASE_CD       varchar(255)         not null,
   RATING_UNITS_TOT_CNT int                  null,
   FULL_TM_OPER_CNT     int                  null,
   PART_TM_OPER_CNT     int                  null,
   FULL_TM_EMP_CNT      int                  null,
   PART_TM_EMP_CNT      int                  null,
   NON_EMP_UNDER_AGE_25_CNT int                  null,
   NON_EMP_AGE_25_OR_OVER_CNT int                  null,
   PHYS_DMG_CLASS_CD    varchar(255)         not null,
   INS_VAL_AMT          numeric(18,2)        null,
   CURR_CD              varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CA_DRVR                                           */
/*==============================================================*/
create table TRF_CA_DRVR (
   CA_DRVR_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   DRVR_NO              int                  null,
   FIRST_NAME           varchar(255)         not null,
   LAST_NAME            varchar(255)         not null,
   BIRTH_DT             DATE                 null,
   GENDER_CD            varchar(255)         not null,
   MARITAL_STATUS_CD    varchar(255)         not null,
   STUDENT_FL           char(1)              not null,
   HIRE_DT              DATE                 null,
   LIC_NO               varchar(255)         not null,
   LIC_JURS_CD          varchar(255)         not null,
   DRVR_TRNG_FL         char(1)              not null,
   GOOD_DRVR_DISC_FL    char(1)              not null,
   MATURE_DRVR_TRNG_FL  char(1)              not null,
   ACCIDENT_CNT_CD      varchar(255)         not null,
   VIOLATION_CNT_CD     varchar(255)         not null,
   LIC_YR_NO            int                  null,
   DRVR_EXPER_CD        varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CA_EXCL_TERM                                      */
/*==============================================================*/
create table TRF_CA_EXCL_TERM (
   CA_EXCL_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   EXCL_KEY             varchar(100)         not null,
   CA_EXCL_TERM_KEY     varchar(100)         not null,
   EXCL_TERM_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   EXCL_CD              varchar(255)         not null,
   EXCL_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CA_GAR_SRVC                                       */
/*==============================================================*/
create table TRF_CA_GAR_SRVC (
   CA_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   OWNING_CVRBL_KEY     varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   GARAGE_SRVC_LOC_KEY  varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   LIAB_CL_CD           varchar(255)         not null,
   EMP_AS_INS_FL        char(1)              not null,
   GARAGE_SRVC_NO       int                  null,
   EMP_CNT_NO           int                  null,
   PARTNERS_CNT_NO      int                  null,
   UNIT_NO              int                  null,
   INS_VAL_AMT          numeric(18,2)        null,
   CURR_CD              varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CA_JURS                                           */
/*==============================================================*/
create table TRF_CA_JURS (
   CA_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   OWNING_CVRBL_KEY     varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CA_LINE_SI_COND                                   */
/*==============================================================*/
create table TRF_CA_LINE_SI_COND (
   CA_LINE_SI_COND_KEY  varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   CA_COND_KEY          varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   SCHED_NO             int                  null,
   TYPE_KEY_COL_1       varchar(255)         not null,
   TYPE_KEY_COL_2       varchar(255)         not null,
   STR_COL_1            varchar(255)         not null,
   STR_COL_2            varchar(255)         not null,
   DT_COL_1_DTS         DATETIME2            null,
   INT_COL_1            int                  null,
   POS_INT_COL_1        int                  null,
   NON_NEG_INT_1        int                  null,
   NON_NEG_INT_2        int                  null,
   NON_NEG_INT_3        int                  null,
   BOOL_COL_1_FL        char(1)              not null,
   BOOL_COL_2_FL        char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CA_MOD                                            */
/*==============================================================*/
create table TRF_CA_MOD (
   CA_MOD_KEY           varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   MOD_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   MOD_CD               varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   MOD_VAL_TEXT         varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CA_MOD_RF                                         */
/*==============================================================*/
create table TRF_CA_MOD_RF (
   CA_MOD_RF_KEY        varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   CA_MOD_KEY           varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   RF_CD                varchar(255)         not null,
   ASSESS_RATE          numeric(18,4)        not null,
   JUST_TEXT            varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CA_NMD_INDIV                                      */
/*==============================================================*/
create table TRF_CA_NMD_INDIV (
   CA_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   OWNING_CVRBL_KEY     varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   INDIV_NAME           varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CA_POL_LINE                                       */
/*==============================================================*/
create table TRF_CA_POL_LINE (
   POL_LINE_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   AUTO_SMBL_MNUL_EDIT_DT DATE                 null,
   CSTM_AUTO_SMBL_DESC  varchar(255)         not null,
   FLEET_TYPE_CD        varchar(255)         not null,
   LEGAL_ENTITY_TYPE_CD varchar(255)         not null,
   CA_POL_TYPE_CD       varchar(255)         not null,
   BUS_START_DT         DATE                 null,
   EXPER_RATING_FL      char(1)              not null,
   RISK_TYPE_CD         varchar(255)         not null,
   SCHED_RATING_MOD_FL  char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CA_PREM_TRAN                                      */
/*==============================================================*/
create table TRF_CA_PREM_TRAN (
   CA_PREM_TRANS_KEY    varchar(100)         not null,
   AGCY_KEY             varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   AUTO_DEALER_LOC_KEY  varchar(100)         not null,
   GARAGE_SRVC_LOC_KEY  varchar(100)         not null,
   VEH_GARAGE_LOC_KEY   varchar(100)         not null,
   CA_COVG_KEY          varchar(100)         not null,
   REC_TYPE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ACCTG_PRD_ID         int                  not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ORIG_TRANS_PROC_DTS  DATETIME2            not null,
   ORIG_END_EFF_DT      DATE                 not null,
   TRANS_EFF_DT         DATE                 not null,
   TRANS_EXP_DT         DATE                 not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   PREM_BASIS_AMT       numeric(18,2)        not null,
   STD_BASE_RATE        numeric(18,4)        not null,
   STD_ADJ_RATE         numeric(18,4)        not null,
   STD_AMT              numeric(18,2)        not null,
   STD_TERM_AMT         numeric(18,2)        not null,
   OVERRIDE_BASE_RATE   numeric(18,4)        not null,
   OVERRIDE_ADJ_RATE    numeric(18,4)        not null,
   OVERRIDE_AMT         numeric(18,2)        not null,
   OVERRIDE_TERM_AMT    numeric(18,2)        not null,
   ACT_BASE_RATE        numeric(18,4)        not null,
   ACT_ADJ_RATE         numeric(18,4)        not null,
   ACT_AMT              numeric(18,2)        not null,
   ACT_TERM_AMT         numeric(18,2)        not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   STTLMNT_PREM_BASIS_AMT numeric(18,2)        null,
   STTLMNT_STD_AMT      numeric(18,2)        null,
   STTLMNT_STD_TERM_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   STTLMNT_ACT_AMT      numeric(18,2)        null,
   STTLMNT_ACT_TERM_AMT numeric(18,2)        null,
   STTLMNT_CURR_CD      varchar(255)         not null,
   STTLMNT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_STTLMNT_RATE numeric(15,10)       null,
   REG_RPT_PREM_BASIS_AMT numeric(18,2)        null,
   REG_RPT_STD_AMT      numeric(18,2)        null,
   REG_RPT_STD_TERM_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   REG_RPT_ACT_AMT      numeric(18,2)        null,
   REG_RPT_ACT_TERM_AMT numeric(18,2)        null,
   REG_RPT_CURR_CD      varchar(255)         not null,
   REG_RPT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_REG_RPT_RATE numeric(15,10)       null,
   RPT_PREM_BASIS_AMT   numeric(18,2)        null,
   RPT_STD_AMT          numeric(18,2)        null,
   RPT_STD_TERM_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   RPT_ACT_AMT          numeric(18,2)        null,
   RPT_ACT_TERM_AMT     numeric(18,2)        null,
   RPT_CURR_CD          varchar(255)         not null,
   RPT_TRANS_AMT        numeric(18,2)        not null,
   TRANS_TO_RPT_RATE    numeric(15,10)       not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   COMM_PCT             numeric(6,3)         not null,
   EST_PREM_AMT         numeric(18,2)        not null,
   RPT_PREM_AMT         numeric(18,2)        not null,
   AUDIT_PREM_AMT       numeric(18,2)        not null,
   AUDITABLE_FL         char(1)              not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CA_SI                                             */
/*==============================================================*/
create table TRF_CA_SI (
   CA_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   OWNING_CVRBL_KEY     varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   CA_COVG_KEY          varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   CA_ADDL_INTRST_KEY   varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   SCHED_NO             int                  null,
   TYPE_KEY_COL_1       varchar(255)         not null,
   TYPE_KEY_COL_2       varchar(255)         not null,
   STR_COL_1            varchar(255)         not null,
   STR_COL_2            varchar(255)         not null,
   DT_COL_1_DTS         DATETIME2            null,
   INT_COL_1            int                  null,
   POS_INT_COL_1        int                  null,
   NON_NEG_INT_1        int                  null,
   NON_NEG_INT_2        int                  null,
   NON_NEG_INT_3        int                  null,
   BOOL_COL_1_FL        char(1)              not null,
   BOOL_COL_2_FL        char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CA_VEH                                            */
/*==============================================================*/
create table TRF_CA_VEH (
   CA_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   OWNING_CVRBL_KEY     varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   GARAGE_LOC_KEY       varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   VEH_ID_NO            varchar(255)         not null,
   MAKE_NAME            varchar(255)         not null,
   MODEL_NAME           varchar(255)         not null,
   MODEL_YR             int                  null,
   VEH_NO               int                  null,
   ORIG_COST_NEW_AMT    numeric(18,2)        null,
   STATED_AMT           numeric(18,2)        null,
   UNIT_NO              int                  null,
   LIC_JURS_CD          varchar(255)         not null,
   LIC_PLATE            varchar(255)         not null,
   LEASE_OR_RENT_FL     char(1)              not null,
   LEN_OF_LEASE_CD      varchar(255)         not null,
   CL_CD                varchar(255)         not null,
   FLEET_FL             char(1)              not null,
   LESSOR_AN_EMP_FL     char(1)              not null,
   PRVT_PSNGR_OPER_EXPER_CD varchar(255)         not null,
   PRVT_PSNGR_USE_CD    varchar(255)         not null,
   PRVT_PSNGR_TYPE_CD   varchar(255)         not null,
   BUS_USE_CL_CD        varchar(255)         not null,
   GVW_NO               int                  null,
   TRUCK_RADIUS_CL_CD   varchar(255)         not null,
   TRUCK_SEC_CL_CD      varchar(255)         not null,
   SEC_CL_CD            varchar(255)         not null,
   SIZE_CL_CD           varchar(255)         not null,
   TRLR_HOW_USED_CD     varchar(255)         not null,
   FAR_TERM_ZONE        varchar(255)         not null,
   GARAGING_ZONE        varchar(255)         not null,
   DAYS_SCHL_YR_CNT     int                  null,
   SCHL_BUS_PRORATION_FL char(1)              not null,
   SEATING_CPCTY_CD     varchar(255)         not null,
   PUB_TRNSP_TYPE_CD    varchar(255)         not null,
   PUB_TRNSP_RADIUS_CL_CD varchar(255)         not null,
   SPCL_VEH_CL_CD       varchar(255)         not null,
   ENGINE_SIZE_CD       varchar(255)         not null,
   GROSS_RECEIPTS_AMT   numeric(18,2)        null,
   NON_OWNED_AUTOS_FL   char(1)              not null,
   DAYS_VEH_LEASED_CNT  varchar(255)         not null,
   FCTRY_TST_EMP_CNT    int                  null,
   REPO_AUTOS_CNT       int                  null,
   PHYS_DMG_CLASS_CD    varchar(255)         not null,
   SUPP_TYPE_CD         varchar(255)         not null,
   COST_HIRE_FOR_COLL_AMT numeric(18,2)        null,
   COST_HIRE_FOR_LIAB_AMT numeric(18,2)        null,
   COST_HIRE_FOR_EXCS_LIAB_AMT numeric(18,2)        null,
   COST_HIRE_FOR_OTC_AMT numeric(18,2)        null,
   SPCL_VEH_TYPE_CD     varchar(255)         not null,
   REG_USE_FL           char(1)              not null,
   CURR_CD              varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CLAIM                                             */
/*==============================================================*/
create table TRF_CLAIM (
   CLAIM_KEY            varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   POL_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CLAIM_NO             varchar(255)         not null,
   CATASTROPHE_CD       varchar(255)         not null,
   CATASTROPHE_DECLARE_DT DATE                 null,
   RPRTD_DT             DATE                 null,
   SETUP_DT             DATE                 not null,
   LOSS_TYPE_CD         varchar(255)         not null,
   EXAMINER_CD          varchar(255)         not null,
   CAUSE_OF_LOSS_CD     varchar(255)         not null,
   LOSS_TEXT            varchar(1333)        not null,
   LOSS_DT              DATE                 null,
   LOSS_CITY            varchar(255)         not null,
   LOSS_STATE_CD        varchar(255)         not null,
   LOSS_ZIP_CD          varchar(15)          not null,
   FAST_TRACK_FL        char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CLAIMANT                                          */
/*==============================================================*/
create table TRF_CLAIMANT (
   CLMNT_KEY            varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   CLAIM_KEY            varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CLMNT_NO             varchar(255)         not null,
   CLMNT_NAME           varchar(255)         not null,
   CLMNT_ADDR_1         varchar(255)         not null,
   CLMNT_ADDR_2         varchar(255)         not null,
   CLMNT_ADDR_3         varchar(255)         not null,
   CLMNT_CITY           varchar(255)         not null,
   CLMNT_COUNTY         varchar(255)         not null,
   CLMNT_STATE_CD       varchar(255)         not null,
   CLMNT_ZIP_CD         varchar(15)          not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CLAIM_FEATURE                                     */
/*==============================================================*/
create table TRF_CLAIM_FEATURE (
   FEATURE_KEY          varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   CLAIM_KEY            varchar(100)         not null,
   CLMNT_KEY            varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   COVG_CD              varchar(255)         not null,
   FEATURE_RPRTD_DT     DATE                 null,
   FEATURE_SETUP_DT     DATE                 not null,
   SUIT_FL              char(1)              not null,
   ADJSTR_CD            varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CLAIM_FEATURE_STATUS                              */
/*==============================================================*/
create table TRF_CLAIM_FEATURE_STATUS (
   FEATURE_KEY          varchar(100)         not null,
   STATUS_PROC_DTS      DATETIME2            not null,
   CLAIM_KEY            varchar(100)         not null,
   CLMNT_KEY            varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   OPEN_DT              DATE                 null,
   REOPEN_DT            DATE                 null,
   CLOSED_DT            DATE                 null,
   STATUS_CD            varchar(255)         not null,
   STATUS_TYPE_CD       varchar(255)         not null,
   REASON_CD            varchar(1333)        not null,
   X_SEQ_NO             bigint               not null
)
go

/*==============================================================*/
/* Table: TRF_CLAIM_STATUS                                      */
/*==============================================================*/
create table TRF_CLAIM_STATUS (
   CLAIM_KEY            varchar(100)         not null,
   STATUS_PROC_DTS      DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   OPEN_DT              DATE                 null,
   REOPEN_DT            DATE                 null,
   CLOSED_DT            DATE                 null,
   STATUS_CD            varchar(255)         not null,
   STATUS_TYPE_CD       varchar(255)         not null,
   REASON_CD            varchar(1333)        not null,
   X_SEQ_NO             bigint               not null
)
go

/*==============================================================*/
/* Table: TRF_COND                                              */
/*==============================================================*/
create table TRF_COND (
   COND_KEY             varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   COND_CD              varchar(255)         not null,
   CATG_CD              varchar(255)         not null,
   COND_TEXT            varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CONFORMED                                         */
/*==============================================================*/
create table TRF_CONFORMED (
   REF_ENTITY_TYPE_CD   varchar(50)          null,
   SRCE_REF_SOURCE_SYSTEM varchar(10)          null,
   SRCE_REF_CD          varchar(255)         null,
   TARGET_REF_SOURCE_SYSTEM varchar(10)          null,
   TARGET_REF_CD        varchar(255)         null
)
go

/*==============================================================*/
/* Table: TRF_COVG                                              */
/*==============================================================*/
create table TRF_COVG (
   COVG_KEY             varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   COVG_PART_CD         varchar(255)         not null,
   COVG_CD              varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_ADDL_INTRST                                    */
/*==============================================================*/
create table TRF_CP_ADDL_INTRST (
   CP_ADDL_INTRST_KEY   varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   ADDL_INTRST_TYPE_CD  varchar(255)         not null,
   CONTRACT_NO          varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_BI                                             */
/*==============================================================*/
create table TRF_CP_BI (
   CP_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   CP_BLDG_KEY          varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   WATERFRONT_PROP_FL   char(1)              not null,
   INC_IN_BLNKT_FL      char(1)              not null,
   FUNG_WET_DRY_ROT_BACT_FL char(1)              not null,
   BUS_INC_COVG_TYPE_CD varchar(255)         not null,
   WATERFRONT_CONSTR_TEXT varchar(255)         not null,
   TYPE_OF_RISK_TEXT    varchar(255)         not null,
   RISK_EDU_INST_PCT    varchar(255)         not null,
   RISK_MERC_PCT        varchar(255)         not null,
   RISK_RENTAL_PCT      varchar(255)         not null,
   RISK_MFG_PCT         varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_BLDG                                           */
/*==============================================================*/
create table TRF_CP_BLDG (
   CP_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   CP_LOC_KEY           varchar(100)         not null,
   CP_CL_CD_KEY         varchar(100)         not null,
   BLDG_KEY             varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   BCEG_TEXT            varchar(255)         not null,
   BCEG_CL_CD           varchar(255)         not null,
   BLDG_INC_IN_OPER_UNIT_FL char(1)              not null,
   CONF_TO_CONDO_ACTS_FL char(1)              not null,
   EQ_CL_CD             varchar(255)         not null,
   EQ_ROOF_TANK_HAZARD_FL char(1)              not null,
   EXPSR                varchar(255)         not null,
   EXTENT_OF_PROT       varchar(255)         not null,
   BURGLAR_ALARM_CR_FCTR_TEXT varchar(255)         not null,
   BURGLAR_ALARM_TYPE_CD varchar(255)         not null,
   RATING_TYPE_CD       varchar(255)         not null,
   CONSTR_TYPE_CD       varchar(255)         not null,
   BLDG_COVG_TYPE_CD    varchar(255)         not null,
   HEATING_TYPE_CD      varchar(255)         not null,
   HEATING_TYPE_OTHR    varchar(255)         not null,
   HEATING_UPDATED_YR   int                  null,
   PLUMBING_UPDATED_YR  int                  null,
   ROOFING_UPDATED_YR   int                  null,
   ROOF_TYPE_CD         varchar(255)         not null,
   INC_IN_BLNKT_FL      char(1)              not null,
   TOT_EXTR_WALLS_FACE_PCT_CD varchar(255)         not null,
   STORIES_NO_CD        varchar(255)         not null,
   OPEN_SIDES_FL        char(1)              not null,
   RENTAL_PROP_FL       char(1)              not null,
   STATE_OWNED_PROP_FL  char(1)              not null,
   SPECIFIC_GROUP_II_RATE numeric(15,6)        not null,
   SPECIFIC_GROUP_I_RATE_TEXT varchar(255)         not null,
   SPRINKLER_SYS_TEXT   varchar(255)         not null,
   BR4_SMBL_FL          char(1)              not null,
   BR4_SMBL_TEXT        varchar(1333)        not null,
   SUB_STD_COND_A_FL    char(1)              not null,
   SUB_STD_COND_B_FL    char(1)              not null,
   SUB_STD_COND_C_FL    char(1)              not null,
   SUB_STD_COND_D_FL    char(1)              not null,
   SUB_STD_COND_E_FL    char(1)              not null,
   HEATING_BY_PLANT_FL  char(1)              not null,
   WATCHMAN_SRVC_CD     varchar(255)         not null,
   WIND_CL_CD           varchar(255)         not null,
   WIND_CL_OTHR         varchar(255)         not null,
   WIRING_UPDATED_YR    int                  null,
   VACANT_BLDG_FL       char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_BLNKT                                          */
/*==============================================================*/
create table TRF_CP_BLNKT (
   CP_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   BLNKT_NO             int                  null,
   BLNKT_TEXT           varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_CL_CODE                                        */
/*==============================================================*/
create table TRF_CP_CL_CODE (
   CP_CL_CD_KEY         varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   EFF_DT               DATE                 not null,
   EXP_DT               DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CL_CD                varchar(255)         not null,
   RATING_TYPE_CD       varchar(255)         not null,
   CL_TEXT              varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_COND_TERM                                      */
/*==============================================================*/
create table TRF_CP_COND_TERM (
   CP_COND_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   COND_KEY             varchar(100)         not null,
   CP_COND_TERM_KEY     varchar(100)         not null,
   COND_TERM_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   COND_CD              varchar(255)         not null,
   COND_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_COVG_TERM                                      */
/*==============================================================*/
create table TRF_CP_COVG_TERM (
   CP_COVG_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   CP_COVG_TERM_KEY     varchar(100)         not null,
   COVG_TERM_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   COVG_CD              varchar(255)         not null,
   COVG_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_EXCL_TERM                                      */
/*==============================================================*/
create table TRF_CP_EXCL_TERM (
   CP_EXCL_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   EXCL_KEY             varchar(100)         not null,
   CP_EXCL_TERM_KEY     varchar(100)         not null,
   EXCL_TERM_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   EXCL_CD              varchar(255)         not null,
   EXCL_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_LOC                                            */
/*==============================================================*/
create table TRF_CP_LOC (
   CP_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   LOC_NO               varchar(255)         not null,
   EQ_CAUSE_OF_LOSS_FORM_CD varchar(255)         not null,
   MULTI_RES_SPCL_CR_CD varchar(255)         not null,
   PROT_CL              varchar(255)         not null,
   FLOOD_COVG_FL        char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_MOD                                            */
/*==============================================================*/
create table TRF_CP_MOD (
   CP_MOD_KEY           varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   MOD_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   MOD_CD               varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   MOD_VAL_TEXT         varchar(255)         not null,
   JUST_TEXT            varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_MOD_RF                                         */
/*==============================================================*/
create table TRF_CP_MOD_RF (
   CP_MOD_RF_KEY        varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   CP_MOD_KEY           varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   RF_CD                varchar(255)         not null,
   ASSESS_RATE          numeric(18,4)        not null,
   JUST_TEXT            varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_OCC_CL                                         */
/*==============================================================*/
create table TRF_CP_OCC_CL (
   CP_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   CP_BLDG_KEY          varchar(100)         not null,
   CP_CL_CD_KEY         varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   OCCPNCY_NO           int                  null,
   OCCPNCY_CATG_CD      varchar(255)         not null,
   AREA_SF_NO           int                  null,
   COVG_FORM_CD         varchar(255)         not null,
   SPRINKLER_SYS_TEXT   varchar(255)         not null,
   OCC_CL_TEXT          varchar(1333)        not null,
   CONF_TO_CONDO_ACTS_FL char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_POL_LINE                                       */
/*==============================================================*/
create table TRF_CP_POL_LINE (
   POL_LINE_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BLKT_RATED_FL        char(1)              not null,
   CP_POL_TYPE_CD       varchar(255)         not null,
   COVG_CERT_TISM_TEXT  varchar(255)         not null,
   COVG_OTHR_TISM_TEXT  varchar(255)         not null,
   EQ_SUB_LMT_BLNKT_FL  char(1)              not null,
   FLOOD_COVG_BLNKT_FL  char(1)              not null,
   FUNG_WETDRYROT_BACT_COVG_TEXT varchar(255)         not null,
   IRPM_APPLIES_FL      char(1)              not null,
   MULTI_PREM_DISP_CR_FL char(1)              not null,
   MULTI_PREM_DISP_CR_TEXT varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_PP                                             */
/*==============================================================*/
create table TRF_CP_PP (
   CP_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   CP_OCC_CL_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   PERS_PROP_COVG_TYPE_CD varchar(255)         not null,
   INC_IN_BLNKT_FL      char(1)              not null,
   YARD_PROP_INDC_TEXT  varchar(255)         not null,
   SPECIFIC_GROUP_I_RATE_TEXT varchar(255)         not null,
   STATE_OWNED_PROP_FL  char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_PREM_TRAN                                      */
/*==============================================================*/
create table TRF_CP_PREM_TRAN (
   CP_PREM_TRANS_KEY    varchar(100)         not null,
   AGCY_KEY             varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   CP_LOC_LOC_KEY       varchar(100)         not null,
   CP_COVG_KEY          varchar(100)         not null,
   REC_TYPE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ACCTG_PRD_ID         int                  not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ORIG_TRANS_PROC_DTS  DATETIME2            not null,
   ORIG_END_EFF_DT      DATE                 not null,
   TRANS_EFF_DT         DATE                 not null,
   TRANS_EXP_DT         DATE                 not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   PREM_BASIS_AMT       numeric(18,2)        not null,
   STD_BASE_RATE        numeric(18,4)        not null,
   STD_ADJ_RATE         numeric(18,4)        not null,
   STD_AMT              numeric(18,2)        not null,
   STD_TERM_AMT         numeric(18,2)        not null,
   OVERRIDE_BASE_RATE   numeric(18,4)        not null,
   OVERRIDE_ADJ_RATE    numeric(18,4)        not null,
   OVERRIDE_AMT         numeric(18,2)        not null,
   OVERRIDE_TERM_AMT    numeric(18,2)        not null,
   ACT_BASE_RATE        numeric(18,4)        not null,
   ACT_ADJ_RATE         numeric(18,4)        not null,
   ACT_AMT              numeric(18,2)        not null,
   ACT_TERM_AMT         numeric(18,2)        not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   STTLMNT_PREM_BASIS_AMT numeric(18,2)        null,
   STTLMNT_STD_AMT      numeric(18,2)        null,
   STTLMNT_STD_TERM_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   STTLMNT_ACT_AMT      numeric(18,2)        null,
   STTLMNT_ACT_TERM_AMT numeric(18,2)        null,
   STTLMNT_CURR_CD      varchar(255)         not null,
   STTLMNT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_STTLMNT_RATE numeric(15,10)       null,
   REG_RPT_PREM_BASIS_AMT numeric(18,2)        null,
   REG_RPT_STD_AMT      numeric(18,2)        null,
   REG_RPT_STD_TERM_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   REG_RPT_ACT_AMT      numeric(18,2)        null,
   REG_RPT_ACT_TERM_AMT numeric(18,2)        null,
   REG_RPT_CURR_CD      varchar(255)         not null,
   REG_RPT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_REG_RPT_RATE numeric(15,10)       null,
   RPT_PREM_BASIS_AMT   numeric(18,2)        null,
   RPT_STD_AMT          numeric(18,2)        null,
   RPT_STD_TERM_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   RPT_ACT_AMT          numeric(18,2)        null,
   RPT_ACT_TERM_AMT     numeric(18,2)        null,
   RPT_CURR_CD          varchar(255)         not null,
   RPT_TRANS_AMT        numeric(18,2)        not null,
   TRANS_TO_RPT_RATE    numeric(15,10)       not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   COMM_PCT             numeric(6,3)         not null,
   EST_PREM_AMT         numeric(18,2)        not null,
   RPT_PREM_AMT         numeric(18,2)        not null,
   AUDIT_PREM_AMT       numeric(18,2)        not null,
   AUDITABLE_FL         char(1)              not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_SI                                             */
/*==============================================================*/
create table TRF_CP_SI (
   CP_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   CP_OWNING_CVRBL_KEY  varchar(100)         not null,
   CP_COVG_KEY          varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   CP_ADDL_INTRST_KEY   varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   SCHED_NO             int                  null,
   TYPE_KEY_COL_1       varchar(255)         not null,
   TYPE_KEY_COL_2       varchar(255)         not null,
   TYPE_KEY_COL_3       varchar(255)         not null,
   STR_COL_1            varchar(255)         not null,
   STR_COL_2            varchar(255)         not null,
   STR_COL_3            varchar(255)         not null,
   DT_COL_1_DTS         DATETIME2            null,
   DT_COL_2_DTS         DATETIME2            null,
   INT_COL_1            int                  null,
   POS_INT_COL_1        int                  null,
   DECIMAL_COL_1        numeric(18,4)        null,
   DECIMAL_COL_2        numeric(18,4)        null,
   DECIMAL_COL_3        numeric(18,4)        null,
   LONG_STR_COL_1       varchar(1333)        not null,
   LONG_STR_COL_2       varchar(1333)        not null,
   LONG_STR_COL_3       varchar(1333)        not null,
   BOOL_COL_1_FL        char(1)              not null,
   BOOL_COL_2_FL        char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_SPCL_CL                                        */
/*==============================================================*/
create table TRF_CP_SPCL_CL (
   CP_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   CP_LOC_KEY           varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   CP_SPCL_CL_TEXT      varchar(255)         not null,
   CONF_TO_CONDO_ACTS_FL char(1)              not null,
   CP_SPCL_CL_COVG_FORM_CD varchar(255)         not null,
   EQ_CL_CD             varchar(255)         not null,
   EQ_STRUC_TEXT        varchar(255)         not null,
   EXPSR                varchar(255)         not null,
   INC_IN_BLNKT_FL      char(1)              not null,
   RATING_TYPE_CD       varchar(255)         not null,
   RENTAL_PROP_FL       char(1)              not null,
   SPCL_CL_NO           int                  null,
   SPCL_CL_TEXT         varchar(255)         not null,
   BUILD_YR             int                  null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_SPCL_CL_BI                                     */
/*==============================================================*/
create table TRF_CP_SPCL_CL_BI (
   CP_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   CP_SPCL_CL_KEY       varchar(100)         null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   INC_IN_BLNKT_FL      char(1)              not null,
   BUS_INC_COVG_TYPE_CD varchar(255)         not null,
   RISK_EDU_INST_PCT    varchar(255)         not null,
   RISK_MERC_PCT        varchar(255)         not null,
   RISK_RENTAL_PCT      varchar(255)         not null,
   RISK_MFG_PCT         varchar(255)         not null,
   TYPE_OF_RISK_TEXT    varchar(255)         not null,
   WATERFRONT_CONSTR_TEXT varchar(255)         not null,
   WATERFRONT_PROP_FL   char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CP_TERR_CODE                                      */
/*==============================================================*/
create table TRF_CP_TERR_CODE (
   CP_TERR_CD_KEY       varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   CP_LOC_KEY           varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   TERR_TEXT            varchar(255)         not null,
   TERR_CAUSE_OF_LOSS_CD varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_CSTMRACCT                                         */
/*==============================================================*/
create table TRF_CSTMRACCT (
   ACCT_KEY             varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ACCT_NO              varchar(255)         not null,
   ACCT_NAME            varchar(255)         not null,
   MAILING_ADDR_1       varchar(255)         not null,
   MAILING_ADDR_2       varchar(255)         not null,
   MAILING_ADDR_3       varchar(255)         not null,
   MAILING_CITY         varchar(255)         not null,
   MAILING_STATE_CD     varchar(255)         not null,
   MAILING_ZIP_CD       varchar(15)          not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            null
)
go

/*==============================================================*/
/* Table: TRF_EXCL                                              */
/*==============================================================*/
create table TRF_EXCL (
   EXCL_KEY             varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   EXCL_CD              varchar(255)         not null,
   CATG_CD              varchar(255)         not null,
   EXCL_TEXT            varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_EX_RATE                                           */
/*==============================================================*/
create table TRF_EX_RATE (
   EX_RATE_KEY          varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   EFF_DTS              DATETIME2            not null,
   EXP_DTS              DATETIME2            not null,
   BASE_CURR_CD         varchar(255)         not null,
   PRICE_CURR_CD        varchar(255)         not null,
   RATE_SCALE           int                  not null,
   NORMALIZED_RATE      numeric(15,10)       not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_FORM                                              */
/*==============================================================*/
create table TRF_FORM (
   FORM_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   EFF_DT               DATE                 not null,
   EXP_DT               DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   FORM_PATTERN_CD      varchar(255)         not null,
   FORM_NO              varchar(255)         not null,
   FORM_TEXT            varchar(255)         not null,
   END_NO               int                  null,
   EDITION              varchar(255)         not null,
   GROUP_CD             varchar(255)         not null,
   REMOVED_SUPERSEDED_FL char(1)              not null,
   INFERENCE_TM_CD      varchar(255)         not null,
   FORM_REMOVAL_DT      DATETIME2            null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_GL_CL_CODE                                        */
/*==============================================================*/
create table TRF_GL_CL_CODE (
   GL_CL_CD_KEY         varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   EFF_DT               DATE                 not null,
   EXP_DT               DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CD_CLASS             varchar(1333)        not null,
   LINE_CL_TEXT         varchar(255)         not null,
   COVG_PART_TYPE_CD    varchar(255)         not null,
   SUBLINE_RATED_INDIV_FL char(1)              not null,
   BASIS_CD             varchar(255)         not null,
   BASIS_TEXT           varchar(1333)        not null,
   BASIS_RF             numeric(18,4)        null,
   BASIS_VARIABLE_FL    char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_GL_COND_TERM                                      */
/*==============================================================*/
create table TRF_GL_COND_TERM (
   GL_COND_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   COND_KEY             varchar(100)         not null,
   GL_COND_TERM_KEY     varchar(100)         not null,
   COND_TERM_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   COND_CD              varchar(255)         not null,
   COND_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_GL_COVG_PART                                      */
/*==============================================================*/
create table TRF_GL_COVG_PART (
   GL_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   CLAIMS_ORIG_EFF_DT   DATETIME2            null,
   COVG_FORM_CD         varchar(255)         not null,
   COVG_PART_TYPE_CD    varchar(255)         not null,
   LOC_LIMITS_FL        char(1)              not null,
   POLLUTION_CLEANUP_EXP_FL char(1)              not null,
   CLAIMS_RETROACTIVE_DT DATETIME2            null,
   SPLIT_BIPD_DED_FL    char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_GL_COVG_TERM                                      */
/*==============================================================*/
create table TRF_GL_COVG_TERM (
   GL_COVG_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   GL_COVG_TERM_KEY     varchar(100)         not null,
   COVG_TERM_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   COVG_CD              varchar(255)         not null,
   COVG_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_GL_EXCL_TERM                                      */
/*==============================================================*/
create table TRF_GL_EXCL_TERM (
   GL_EXCL_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   EXCL_KEY             varchar(100)         not null,
   GL_EXCL_TERM_KEY     varchar(100)         not null,
   EXCL_TERM_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   EXCL_CD              varchar(255)         not null,
   EXCL_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_GL_EXPSR                                          */
/*==============================================================*/
create table TRF_GL_EXPSR (
   GL_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   GL_COVG_PART_KEY     varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   GL_CL_CD_KEY         varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   AUDITED_BASIS_AMT    numeric(18,2)        null,
   PRODS_COMP_AUDITED_BASIS_AMT numeric(18,2)        null,
   EXPSR_TEXT           varchar(255)         not null,
   FIXED_BASIS_AMT      numeric(18,2)        null,
   PRODS_COMP_FIXED_BASIS_AMT numeric(18,2)        null,
   SCALABLE_BASIS_AMT   numeric(18,2)        null,
   PRODS_COMP_SCALABLE_BASIS_AMT numeric(18,2)        null,
   TERM_BASIS_AMT       numeric(18,2)        null,
   PRODS_COMP_TERM_BASIS_AMT numeric(18,2)        null,
   CURR_CD              varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_GL_MOD                                            */
/*==============================================================*/
create table TRF_GL_MOD (
   GL_MOD_KEY           varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   MOD_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   MOD_CD               varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   MOD_VAL_TEXT         varchar(255)         not null,
   JUST_TEXT            varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_GL_MOD_RF                                         */
/*==============================================================*/
create table TRF_GL_MOD_RF (
   GL_MOD_RF_KEY        varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   GL_MOD_KEY           varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   RF_CD                varchar(255)         not null,
   ASSESS_RATE          numeric(18,4)        not null,
   JUST_TEXT            varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_GL_POL_LINE                                       */
/*==============================================================*/
create table TRF_GL_POL_LINE (
   POL_LINE_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CLAIMS_ORIG_EFF_DT   DATETIME2            null,
   LOC_LIMITS_FL        char(1)              not null,
   POLLUTION_CLEANUP_EXP_FL char(1)              not null,
   CLAIMS_RETROACTIVE_DT DATETIME2            null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_GL_PREM_TRAN                                      */
/*==============================================================*/
create table TRF_GL_PREM_TRAN (
   GL_PREM_TRANS_KEY    varchar(100)         not null,
   AGCY_KEY             varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   EXPSR_LOC_KEY        varchar(100)         not null,
   ADDL_INS_KEY         varchar(100)         not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   GL_COVG_KEY          varchar(100)         not null,
   GL_COND_KEY          varchar(100)         not null,
   GL_EXCL_KEY          varchar(100)         not null,
   REC_TYPE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ACCTG_PRD_ID         int                  not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ORIG_TRANS_PROC_DTS  DATETIME2            not null,
   ORIG_END_EFF_DT      DATE                 not null,
   TRANS_EFF_DT         DATE                 not null,
   TRANS_EXP_DT         DATE                 not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   STATE_COST_TYPE_CD   varchar(255)         not null,
   PREM_BASIS_AMT       numeric(18,2)        not null,
   STD_BASE_RATE        numeric(18,4)        not null,
   STD_ADJ_RATE         numeric(18,4)        not null,
   STD_AMT              numeric(18,2)        not null,
   STD_TERM_AMT         numeric(18,2)        not null,
   OVERRIDE_BASE_RATE   numeric(18,4)        not null,
   OVERRIDE_ADJ_RATE    numeric(18,4)        not null,
   OVERRIDE_AMT         numeric(18,2)        not null,
   OVERRIDE_TERM_AMT    numeric(18,2)        not null,
   ACT_BASE_RATE        numeric(18,4)        not null,
   ACT_ADJ_RATE         numeric(18,4)        not null,
   ACT_AMT              numeric(18,2)        not null,
   ACT_TERM_AMT         numeric(18,2)        not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   STTLMNT_PREM_BASIS_AMT numeric(18,2)        null,
   STTLMNT_STD_AMT      numeric(18,2)        null,
   STTLMNT_STD_TERM_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   STTLMNT_ACT_AMT      numeric(18,2)        null,
   STTLMNT_ACT_TERM_AMT numeric(18,2)        null,
   STTLMNT_CURR_CD      varchar(255)         not null,
   STTLMNT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_STTLMNT_RATE numeric(15,10)       null,
   REG_RPT_PREM_BASIS_AMT numeric(18,2)        null,
   REG_RPT_STD_AMT      numeric(18,2)        null,
   REG_RPT_STD_TERM_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   REG_RPT_ACT_AMT      numeric(18,2)        null,
   REG_RPT_ACT_TERM_AMT numeric(18,2)        null,
   REG_RPT_CURR_CD      varchar(255)         not null,
   REG_RPT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_REG_RPT_RATE numeric(15,10)       null,
   RPT_PREM_BASIS_AMT   numeric(18,2)        null,
   RPT_STD_AMT          numeric(18,2)        null,
   RPT_STD_TERM_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   RPT_ACT_AMT          numeric(18,2)        null,
   RPT_ACT_TERM_AMT     numeric(18,2)        null,
   RPT_CURR_CD          varchar(255)         not null,
   RPT_TRANS_AMT        numeric(18,2)        not null,
   TRANS_TO_RPT_RATE    numeric(15,10)       not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   COMM_PCT             numeric(6,3)         not null,
   EST_PREM_AMT         numeric(18,2)        not null,
   RPT_PREM_AMT         numeric(18,2)        not null,
   AUDIT_PREM_AMT       numeric(18,2)        not null,
   AUDITABLE_FL         char(1)              not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_GL_SI                                             */
/*==============================================================*/
create table TRF_GL_SI (
   GL_CVRBL_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   CVRBL_TYPE_KEY       varchar(100)         not null,
   GL_OWNING_CVRBL_KEY  varchar(100)         not null,
   GL_COVG_KEY          varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   SCHED_NO             int                  null,
   TYPE_KEY_COL_1       varchar(255)         not null,
   TYPE_KEY_COL_2       varchar(255)         not null,
   STR_COL_1            varchar(255)         not null,
   STR_COL_2            varchar(255)         not null,
   STR_COL_3            varchar(255)         not null,
   STR_COL_4            varchar(255)         not null,
   STR_COL_5            varchar(255)         not null,
   STR_COL_6            varchar(255)         not null,
   DT_COL_1             DATETIME2            null,
   INT_COL_1            int                  null,
   POS_INT_COL_1        int                  null,
   BOOL_COL_1_FL        char(1)              not null,
   BOOL_COL_2_FL        char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_GL_SI_COND                                        */
/*==============================================================*/
create table TRF_GL_SI_COND (
   GL_SI_COND_KEY       varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   GL_COND_KEY          varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   SCHED_NO             int                  null,
   TYPE_KEY_COL_1       varchar(255)         not null,
   TYPE_KEY_COL_2       varchar(255)         not null,
   STR_COL_1            varchar(255)         not null,
   STR_COL_2            varchar(255)         not null,
   STR_COL_3            varchar(255)         not null,
   STR_COL_4            varchar(255)         not null,
   STR_COL_5            varchar(255)         not null,
   STR_COL_6            varchar(255)         not null,
   DT_COL_1             DATETIME2            null,
   INT_COL_1            int                  null,
   POS_INT_COL_1        int                  null,
   BOOL_COL_1_FL        char(1)              not null,
   BOOL_COL_2_FL        char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_GL_SI_EXCL                                        */
/*==============================================================*/
create table TRF_GL_SI_EXCL (
   GL_SI_EXCL_KEY       varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   GL_EXCL_KEY          varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   SCHED_NO             int                  null,
   TYPE_KEY_COL_1       varchar(255)         not null,
   TYPE_KEY_COL_2       varchar(255)         not null,
   STR_COL_1            varchar(255)         not null,
   STR_COL_2            varchar(255)         not null,
   STR_COL_3            varchar(255)         not null,
   STR_COL_4            varchar(255)         not null,
   STR_COL_5            varchar(255)         not null,
   STR_COL_6            varchar(255)         not null,
   DT_COL_1             DATETIME2            null,
   INT_COL_1            int                  null,
   POS_INT_COL_1        int                  null,
   BOOL_COL_1_FL        char(1)              not null,
   BOOL_COL_2_FL        char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_HO_ADDL_INTRST                                    */
/*==============================================================*/
create table TRF_HO_ADDL_INTRST (
   HO_ADDL_INTRST_KEY   varchar(100)         not null,
   HO_DWLNG_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ADDL_INTRST_TYPE_CD  varchar(255)         not null,
   CONTRACT_NO          varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_HO_DWLNG                                          */
/*==============================================================*/
create table TRF_HO_DWLNG (
   HO_DWLNG_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   HO_LOC_KEY           varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BUILD_YR             int                  not null,
   PURCH_YR             int                  not null,
   DWLNG_AREA_SF_NO     int                  not null,
   GARAGE_AREA_SF_NO    int                  not null,
   HO_POL_TYPE_CD       varchar(255)         not null,
   GARAGE_TYPE_CD       varchar(255)         not null,
   PRIM_HEATING_TYPE_CD varchar(255)         not null,
   CONSTR_CD            varchar(255)         not null,
   CONSTR_TYPE_CD       varchar(255)         not null,
   ELECTRICAL_TYPE_CD   varchar(255)         not null,
   OCCPCY_CD            varchar(255)         not null,
   PLUMBING_TYPE_CD     varchar(255)         not null,
   LOC_TYPE_CD          varchar(255)         not null,
   SPRINKLER_TYPE_CD    varchar(255)         not null,
   WIRING_TYPE_CD       varchar(255)         not null,
   ROOF_TYPE_CD         varchar(255)         not null,
   RESIDENCE_TYPE_CD    varchar(255)         not null,
   FOUNDATION_TYPE_CD   varchar(255)         not null,
   STORIES_CD           varchar(255)         not null,
   WIND_CLASS_CD        varchar(255)         not null,
   DWLNG_USAGE_CD       varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   REPL_COST_AMT        numeric(18,2)        not null,
   FIRE_EXTINGUISHERS_FL char(1)              not null,
   BURGLAR_ALARM_FL     char(1)              not null,
   BURGLAR_ALARM_TYPE_CD varchar(255)         not null,
   FIRE_ALARM_FL        char(1)              not null,
   SMOKE_ALARM_FL       char(1)              not null,
   SMOKE_ALARM_ON_ALL_FLOORS_FL char(1)              not null,
   COVERED_PORCH_FL     char(1)              not null,
   COVERED_PORCH_AREA_NO int                  not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_HO_DWLNG_COVG_TERM                                */
/*==============================================================*/
create table TRF_HO_DWLNG_COVG_TERM (
   HO_DWLNG_COVG_TERM_KEY varchar(100)         not null,
   HO_DWLNG_COVG_KEY    varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   HO_DWLNG_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   COVG_TERM_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   COVG_CD              varchar(255)         not null,
   COVG_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_HO_DWLNG_MOD                                      */
/*==============================================================*/
create table TRF_HO_DWLNG_MOD (
   HO_DWLNG_MOD_KEY     varchar(100)         not null,
   HO_DWLNG_KEY         varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   MOD_CD               varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   MOD_VAL_TEXT         varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_HO_LINE_COVG_TERM                                 */
/*==============================================================*/
create table TRF_HO_LINE_COVG_TERM (
   HO_LINE_COVG_TERM_KEY varchar(100)         not null,
   HO_LINE_COVG_KEY     varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   COVG_TERM_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   COVG_CD              varchar(255)         not null,
   COVG_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_HO_LOC                                            */
/*==============================================================*/
create table TRF_HO_LOC (
   HO_LOC_KEY           varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   DIST_FIRE_STATION_NO int                  not null,
   DIST_FIRE_HYDRANT_NO int                  not null,
   PROT_CLASS_CD        varchar(255)         not null,
   FLOODING_HAZARD_FL   char(1)              not null,
   NEAR_COMML_FL        char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_HO_MOD                                            */
/*==============================================================*/
create table TRF_HO_MOD (
   HO_MOD_KEY           varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   MOD_CD               varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   MOD_VAL_TEXT         varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_HO_OTHR_LOC                                       */
/*==============================================================*/
create table TRF_HO_OTHR_LOC (
   HO_OTHR_LOC_KEY      varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   HO_LINE_COVG_KEY     varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   LOC_NO               varchar(255)         not null,
   INCR_LMT_AMT_CD      varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_HO_PREM_TRAN                                      */
/*==============================================================*/
create table TRF_HO_PREM_TRAN (
   HO_PREM_TRANS_KEY    varchar(100)         not null,
   AGCY_KEY             varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   HO_SI_KEY            varchar(100)         not null,
   HO_SI_LOC_KEY        varchar(100)         not null,
   HO_DWLNG_KEY         varchar(100)         not null,
   HO_DWLNG_LOC_KEY     varchar(100)         not null,
   HO_LINE_COVG_KEY     varchar(100)         not null,
   HO_DWLNG_COVG_KEY    varchar(100)         not null,
   HO_OTHR_LOC_LOC_KEY  varchar(100)         not null,
   REC_TYPE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ACCTG_PRD_ID         int                  not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ORIG_TRANS_PROC_DTS  DATETIME2            not null,
   ORIG_END_EFF_DT      DATE                 not null,
   TRANS_EFF_DT         DATE                 not null,
   TRANS_EXP_DT         DATE                 not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   SCHED_TYPE_CD        varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   STD_BASE_RATE        numeric(18,4)        not null,
   STD_ADJ_RATE         numeric(18,4)        not null,
   STD_AMT              numeric(18,2)        not null,
   STD_TERM_AMT         numeric(18,2)        not null,
   OVERRIDE_BASE_RATE   numeric(18,4)        not null,
   OVERRIDE_ADJ_RATE    numeric(18,4)        not null,
   OVERRIDE_AMT         numeric(18,2)        not null,
   OVERRIDE_TERM_AMT    numeric(18,2)        not null,
   ACT_BASE_RATE        numeric(18,4)        not null,
   ACT_ADJ_RATE         numeric(18,4)        not null,
   ACT_AMT              numeric(18,2)        not null,
   ACT_TERM_AMT         numeric(18,2)        not null,
   STTLMNT_PREM_BASIS_AMT numeric(18,2)        null,
   STTLMNT_STD_AMT      numeric(18,2)        null,
   STTLMNT_STD_TERM_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   STTLMNT_ACT_AMT      numeric(18,2)        null,
   STTLMNT_ACT_TERM_AMT numeric(18,2)        null,
   STTLMNT_CURR_CD      varchar(255)         not null,
   STTLMNT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_STTLMNT_RATE numeric(15,10)       null,
   REG_RPT_PREM_BASIS_AMT numeric(18,2)        null,
   REG_RPT_STD_AMT      numeric(18,2)        null,
   REG_RPT_STD_TERM_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   REG_RPT_ACT_AMT      numeric(18,2)        null,
   REG_RPT_ACT_TERM_AMT numeric(18,2)        null,
   REG_RPT_CURR_CD      varchar(255)         not null,
   REG_RPT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_REG_RPT_RATE numeric(15,10)       null,
   RPT_PREM_BASIS_AMT   numeric(18,2)        null,
   RPT_STD_AMT          numeric(18,2)        null,
   RPT_STD_TERM_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   RPT_ACT_AMT          numeric(18,2)        null,
   RPT_ACT_TERM_AMT     numeric(18,2)        null,
   RPT_CURR_CD          varchar(255)         not null,
   RPT_TRANS_AMT        numeric(18,2)        not null,
   TRANS_TO_RPT_RATE    numeric(15,10)       not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   COMM_PCT             numeric(6,3)         not null,
   PREM_BASIS_AMT       numeric(18,2)        not null,
   AUDITABLE_FL         char(1)              not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_HO_SI                                             */
/*==============================================================*/
create table TRF_HO_SI (
   HO_SI_KEY            varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   HO_DWLNG_COVG_KEY    varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ITEM_NO              varchar(255)         not null,
   SCHED_TYPE_CD        varchar(255)         not null,
   ITEM_TEXT            varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   INCR_LMT_PCT_CD      varchar(255)         not null,
   EXPSR_VAL_AMT        numeric(18,2)        not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_LOC                                               */
/*==============================================================*/
create table TRF_LOC (
   LOC_KEY              varchar(100)         not null,
   ADDR_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   LOC_NO               varchar(255)         not null,
   STREET_1_NAME        varchar(255)         not null,
   STREET_2_NAME        varchar(255)         not null,
   STREET_3_NAME        varchar(255)         not null,
   CNTRY_CD             varchar(255)         not null,
   REGION_CD            varchar(255)         not null,
   CITY_NAME            varchar(255)         not null,
   POSTAL_CD            varchar(15)          not null,
   COUNTY_NAME          varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_LOSS_TRAN                                         */
/*==============================================================*/
create table TRF_LOSS_TRAN (
   CLAIM_KEY            varchar(100)         not null,
   CLMNT_KEY            varchar(100)         not null,
   FEATURE_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   RISK_KEY             varchar(100)         not null,
   COVG_LIMIT_KEY       varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   AGCY_KEY             varchar(100)         not null,
   REC_TYPE_KEY         varchar(100)         not null,
   VNDR_KEY             varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   RISK_TYPE_CD         varchar(255)         not null,
   COVG_CD              varchar(255)         not null,
   ASLOB_CD             varchar(255)         not null,
   FORM_NO_CD           varchar(255)         not null,
   PERIL_TYPE           varchar(255)         not null,
   LOSS_DT              DATE                 null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ACCTG_PRD_ID         int                  not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   CHECK_NO             varchar(255)         not null,
   CHECK_DT             DATE                 null,
   TRANS_AMT            numeric(18,2)        not null,
   RSRV_CHNG_AMT        numeric(18,2)        not null,
   CURR_CD              varchar(255)         not null,
   CLAIM_AMT            numeric(18,2)        not null,
   CLAIM_EX_ADJ_AMT     numeric(18,2)        not null,
   CLAIM_CURR_CD        varchar(255)         not null,
   TRANS_TO_CLAIM_EX_RATE numeric(15,10)       not null,
   RSRV_AMT             numeric(18,2)        null,
   RSRV_EX_ADJ_AMT      numeric(18,2)        null,
   RSRV_CURR_CD         varchar(255)         not null,
   TRANS_TO_RSRV_EX_RATE numeric(15,10)       null,
   RPT_AMT              numeric(18,2)        not null,
   RPT_EX_ADJ_AMT       numeric(18,2)        not null,
   RPT_CURR_CD          varchar(255)         not null,
   CLAIM_TO_RPT_EX_RATE numeric(15,10)       not null,
   CLAIM_RSRV_CHNG_AMT  numeric(18,2)        not null,
   RSRV_RSRV_CHNG_AMT   numeric(18,2)        not null,
   RPT_RSRV_CHNG_AMT    numeric(18,2)        not null
)
go

/*==============================================================*/
/* Table: TRF_MOD                                               */
/*==============================================================*/
create table TRF_MOD (
   MOD_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   MOD_CD               varchar(255)         not null,
   MOD_TEXT             varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_NMDINSRD                                          */
/*==============================================================*/
create table TRF_NMDINSRD (
   NAMED_INSURED_KEY    varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   POL_KEY              varchar(100)         not null,
   END_EXP_DT           DATE                 not null,
   INSURED_NAME         varchar(255)         not null,
   MAILING_ADDR_1       varchar(255)         not null,
   MAILING_ADDR_2       varchar(255)         not null,
   MAILING_ADDR_3       varchar(255)         not null,
   MAILING_CITY         varchar(255)         not null,
   MAILING_STATE_CD     varchar(255)         not null,
   MAILING_ZIP_CD       varchar(15)          not null,
   PRIM_INSURED_FL      char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_DELETE_DTS       DATETIME2            null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_PARTY                                             */
/*==============================================================*/
create table TRF_PARTY (
   PARTY_KEY            varchar(100)         not null,
   PRIM_ADDR_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PARTY_TYPE_CD        varchar(255)         not null,
   ORG_NAME             varchar(255)         not null,
   PREFIX_NAME          varchar(255)         not null,
   FIRST_NAME           varchar(255)         not null,
   MIDDLE_NAME          varchar(255)         not null,
   LAST_NAME            varchar(255)         not null,
   SUFFIX_NAME          varchar(255)         not null,
   BIRTH_DT             DATE                 null,
   GENDER_CD            varchar(255)         not null,
   MARITAL_STATUS_CD    varchar(255)         not null,
   TAX_ID               varchar(15)          not null,
   OCCPTN_CD            varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_PA_ADDL_INTRST                                    */
/*==============================================================*/
create table TRF_PA_ADDL_INTRST (
   PA_ADDL_INTRST_KEY   varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   VEH_KEY              varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ADDL_INTRST_TYPE_CD  varchar(255)         not null,
   CONTRACT_NO          varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_PA_DRVR                                           */
/*==============================================================*/
create table TRF_PA_DRVR (
   PA_DRVR_KEY          varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_PARTY_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   LIC_NO               varchar(15)          not null,
   LIC_JURS_CD          varchar(255)         not null,
   ACCIDENT_CNT_CD      varchar(255)         not null,
   VIOLATION_CNT_CD     varchar(255)         not null,
   GOOD_DRIVER_DISC_FL  char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_PA_LINE_COVG_TERM                                 */
/*==============================================================*/
create table TRF_PA_LINE_COVG_TERM (
   PA_LINE_COVG_TERM_KEY varchar(100)         not null,
   PA_LINE_COVG_KEY     varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   COVG_TERM_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   COVG_CD              varchar(255)         not null,
   COVG_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_PA_MOD                                            */
/*==============================================================*/
create table TRF_PA_MOD (
   PA_MOD_KEY           varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   MOD_CD               varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   MOD_VAL_TEXT         varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_PA_PREM_TRAN                                      */
/*==============================================================*/
create table TRF_PA_PREM_TRAN (
   PA_PREM_TRANS_KEY    varchar(100)         not null,
   AGCY_KEY             varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   PA_LINE_COVG_KEY     varchar(100)         not null,
   PA_VEH_COVG_KEY      varchar(100)         not null,
   VEH_KEY              varchar(100)         not null,
   REC_TYPE_KEY         varchar(100)         not null,
   VEH_GARAGE_LOC_KEY   varchar(100)         not null,
   ACCTG_PRD_ID         int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ORIG_TRANS_PROC_DTS  DATETIME2            not null,
   ORIG_END_EFF_DT      DATE                 not null,
   TRANS_EFF_DT         DATE                 not null,
   TRANS_EXP_DT         DATE                 not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   STD_BASE_RATE        numeric(18,4)        not null,
   STD_ADJ_RATE         numeric(18,4)        not null,
   STD_AMT              numeric(18,2)        not null,
   STD_TERM_AMT         numeric(18,2)        not null,
   OVERRIDE_BASE_RATE   numeric(18,4)        not null,
   OVERRIDE_ADJ_RATE    numeric(18,4)        not null,
   OVERRIDE_AMT         numeric(18,2)        not null,
   OVERRIDE_TERM_AMT    numeric(18,2)        not null,
   ACT_BASE_RATE        numeric(18,4)        not null,
   ACT_ADJ_RATE         numeric(18,4)        not null,
   ACT_AMT              numeric(18,2)        not null,
   ACT_TERM_AMT         numeric(18,2)        not null,
   STTLMNT_PREM_BASIS_AMT numeric(18,2)        null,
   STTLMNT_STD_AMT      numeric(18,2)        null,
   STTLMNT_STD_TERM_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_AMT numeric(18,2)        null,
   STTLMNT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   STTLMNT_ACT_AMT      numeric(18,2)        null,
   STTLMNT_ACT_TERM_AMT numeric(18,2)        null,
   STTLMNT_CURR_CD      varchar(255)         not null,
   STTLMNT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_STTLMNT_RATE numeric(15,10)       null,
   REG_RPT_PREM_BASIS_AMT numeric(18,2)        null,
   REG_RPT_STD_AMT      numeric(18,2)        null,
   REG_RPT_STD_TERM_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_AMT numeric(18,2)        null,
   REG_RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   REG_RPT_ACT_AMT      numeric(18,2)        null,
   REG_RPT_ACT_TERM_AMT numeric(18,2)        null,
   REG_RPT_CURR_CD      varchar(255)         not null,
   REG_RPT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_REG_RPT_RATE numeric(15,10)       null,
   RPT_PREM_BASIS_AMT   numeric(18,2)        null,
   RPT_STD_AMT          numeric(18,2)        null,
   RPT_STD_TERM_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_AMT     numeric(18,2)        null,
   RPT_OVERRIDE_TERM_AMT numeric(18,2)        null,
   RPT_ACT_AMT          numeric(18,2)        null,
   RPT_ACT_TERM_AMT     numeric(18,2)        null,
   RPT_CURR_CD          varchar(255)         not null,
   RPT_TRANS_AMT        numeric(18,2)        not null,
   TRANS_TO_RPT_RATE    numeric(15,10)       not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   COMM_PCT             numeric(6,3)         not null,
   PREM_BASIS_AMT       numeric(18,2)        not null,
   AUDITABLE_FL         char(1)              not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_PA_VEH                                            */
/*==============================================================*/
create table TRF_PA_VEH (
   VEH_KEY              varchar(100)         not null,
   GARAGE_LOC_KEY       varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   POL_LINE_KEY         varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   VEH_NO               varchar(255)         not null,
   MODEL_YR             int                  not null,
   MAKE_NAME            varchar(255)         not null,
   MODEL_NAME           varchar(255)         not null,
   VIN_NO               varchar(255)         not null,
   COLOR_NAME           varchar(255)         not null,
   BODY_TYPE_CD         varchar(255)         not null,
   VEH_TYPE_CD          varchar(255)         not null,
   LIC_JURS_CD          varchar(255)         not null,
   LIC_PLATE_NO         varchar(255)         not null,
   LEASE_RENTAL_FL      char(1)              not null,
   LEN_OF_LEASE_CD      varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   ORIG_LIST_PRICE_AMT  numeric(18,2)        not null,
   STATED_VAL_AMT       numeric(18,2)        not null,
   PRIM_USE_CD          varchar(255)         not null,
   ANNUAL_MILES_NO      int                  not null,
   COMMUTING_MILES_NO   int                  not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_PA_VEH_COVG_TERM                                  */
/*==============================================================*/
create table TRF_PA_VEH_COVG_TERM (
   PA_VEH_COVG_TERM_KEY varchar(100)         not null,
   PA_VEH_COVG_KEY      varchar(100)         not null,
   COVG_KEY             varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   VEH_KEY              varchar(100)         not null,
   COVG_TERM_KEY        varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   COVG_CD              varchar(255)         not null,
   COVG_TERM_CD         varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TERM_VAL_CD          varchar(255)         null,
   TERM_VAL_AMT         numeric(20,4)        null,
   TERM_VAL_TYPE_CD     varchar(255)         null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_PA_VEH_DRVR                                       */
/*==============================================================*/
create table TRF_PA_VEH_DRVR (
   PA_VEH_DRVR_KEY      varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   VEH_KEY              varchar(100)         not null,
   PA_DRVR_KEY          varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   USE_PCT              numeric(6,3)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_PA_VEH_MOD                                        */
/*==============================================================*/
create table TRF_PA_VEH_MOD (
   PA_VEH_MOD_KEY       varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   VEH_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   MOD_CD               varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   MOD_VAL_TEXT         varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_POLICY                                            */
/*==============================================================*/
create table TRF_POLICY (
   POL_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   AGCY_KEY             varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   ACCT_KEY             varchar(100)         not null,
   PRIM_NAMED_INSURED_KEY varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   POL_NO               varchar(255)         not null,
   POL_EFF_DT           DATE                 not null,
   POL_EXP_DT           DATE                 not null,
   ORIG_EFF_DT          DATE                 null,
   NEW_BUS_FL           char(1)              not null,
   POL_TERM             varchar(255)         not null,
   ENTITY_TYPE_CD       varchar(255)         not null,
   PRIM_STATE_CD        varchar(255)         not null,
   CLAIMS_MADE_FL       char(1)              not null,
   MASTER_CO_CD         varchar(255)         not null,
   POL_CO_CD            varchar(255)         not null,
   BRANCH_OFC_CD        varchar(255)         not null,
   PROFIT_CTR_CD        varchar(255)         not null,
   LOB_CD               varchar(255)         not null,
   LINE_DIV_CD          varchar(255)         not null,
   PROD_CD              varchar(255)         not null,
   END_EXP_DT           DATE                 not null,
   PROG_CD              varchar(255)         not null,
   PAY_PLAN_CD          varchar(255)         not null,
   BILL_PLAN_CD         varchar(255)         not null,
   POL_FORM_CD          varchar(255)         not null,
   SIC_CD               varchar(255)         not null,
   UNDERWRITER_CD       varchar(255)         not null,
   AUDIT_PLAN_CD        varchar(255)         not null,
   ELECT_FUNDS_TFR_FL   char(1)              not null,
   RETRO_FL             char(1)              not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_DELETE_DTS       DATETIME2            null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_POLICY_STATUS                                     */
/*==============================================================*/
create table TRF_POLICY_STATUS (
   POL_KEY              varchar(100)         not null,
   STATUS_PROC_DTS      DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   STATUS_CD            varchar(255)         not null,
   REASON_CD            varchar(255)         not null,
   ACCTG_PRD_ID         int                  not null,
   STATUS_EFF_DT        DATE                 not null,
   X_SEQ_NO             bigint               not null
)
go

/*==============================================================*/
/* Table: TRF_POL_PARTY                                         */
/*==============================================================*/
create table TRF_POL_PARTY (
   POL_PARTY_KEY        varchar(100)         not null,
   PARTY_KEY            varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PARTY_ROLE_TYPE_CD   varchar(255)         not null,
   PARTY_NAME           varchar(255)         not null,
   BIRTH_DT             DATE                 null,
   MARITAL_STATUS_CD    varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_PREM_TRANS                                        */
/*==============================================================*/
create table TRF_PREM_TRANS (
   POL_KEY              varchar(100)         not null,
   LOC_KEY              varchar(100)         not null,
   RISK_KEY             varchar(100)         not null,
   AGCY_KEY             varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   COVG_LIMIT_KEY       varchar(100)         not null,
   REC_TYPE_KEY         varchar(100)         not null,
   SCHED_ITEM_CLASS_KEY varchar(100)         null,
   SOURCE_SYSTEM        varchar(10)          not null,
   RISK_TYPE_CD         varchar(255)         not null,
   ASLOB_CD             varchar(255)         not null,
   COVG_CD              varchar(255)         not null,
   PERIL_TYPE           varchar(255)         not null,
   FORM_NO_CD           varchar(255)         not null,
   ACCTG_PRD_ID         int                  not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ORIG_TRANS_PROC_DTS  DATETIME2            not null,
   ORIG_END_EFF_DT      DATE                 not null,
   TRANS_EFF_DT         DATE                 not null,
   TRANS_EXP_DT         DATE                 not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   TRANS_AMT            numeric(18,2)        not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   STTLMNT_PREM_BASIS_AMT numeric(18,2)        null,
   STTLMNT_CURR_CD      varchar(255)         not null,
   STTLMNT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_STTLMNT_RATE numeric(15,10)       null,
   REG_RPT_PREM_BASIS_AMT numeric(18,2)        null,
   REG_RPT_CURR_CD      varchar(255)         not null,
   REG_RPT_TRANS_AMT    numeric(18,2)        null,
   TRANS_TO_REG_RPT_RATE numeric(15,10)       null,
   RPT_PREM_BASIS_AMT   numeric(18,2)        null,
   RPT_CURR_CD          varchar(255)         not null,
   RPT_TRANS_AMT        numeric(18,2)        not null,
   TRANS_TO_RPT_RATE    numeric(15,10)       not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   COMM_PCT             numeric(6,3)         not null,
   EST_PREM_AMT         numeric(18,2)        not null,
   RPT_PREM_AMT         numeric(18,2)        not null,
   AUDIT_PREM_AMT       numeric(18,2)        not null,
   PREM_BASIS_AMT       numeric(18,2)        not null,
   AUDITABLE_FL         char(1)              not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   CURR_CD              varchar(255)         not null
)
go

/*==============================================================*/
/* Table: TRF_REFERENCE                                         */
/*==============================================================*/
create table TRF_REFERENCE (
   REF_ENTITY_TYPE_CD   varchar(50)          not null,
   REF_SOURCE_SYSTEM    varchar(10)          not null,
   REF_CD               varchar(255)         not null,
   SHORT_TEXT           varchar(255)         null,
   LONG_TEXT            varchar(255)         null
)
go

/*==============================================================*/
/* Table: TRF_USER_AGENT                                        */
/*==============================================================*/
create table TRF_USER_AGENT (
   USER_AGNT_KEY        varchar(100)         not null,
   AGNT_KEY             varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PC_USERNAME          varchar(255)         not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

/*==============================================================*/
/* Table: TRF_VENDOR                                            */
/*==============================================================*/
create table TRF_VENDOR (
   VNDR_KEY             varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   VNDR_CD              varchar(255)         not null,
   VNDR_TYPE_CD         varchar(255)         not null,
   ENTITY_TYPE_CD       varchar(255)         not null,
   VNDR_NAME            varchar(255)         not null,
   VNDR_ADDR_1          varchar(255)         not null,
   VNDR_ADDR_2          varchar(255)         not null,
   VNDR_ADDR_3          varchar(255)         not null,
   VNDR_CITY            varchar(255)         not null,
   VNDR_STATE_CD        varchar(255)         not null,
   VNDR_ZIP_CD          varchar(15)          not null,
   VNDR_TAX_ID          varchar(15)          not null,
   VNDR_PHONE_NO        varchar(50)          not null,
   X_SEQ_NO             bigint               not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null
)
go

alter table CS_ADDR_DELTA
   add constraint FK_CADE_CABA foreign key (ADDR_KEY)
      references CS_ADDR_BASE (ADDR_KEY)
go

alter table CS_AGENCY_DELTA
   add constraint FK_CADE_CABA2 foreign key (AGCY_KEY)
      references CS_AGENCY_BASE (AGCY_KEY)
go

alter table CS_AGENT_BASE
   add constraint FK_CABA_CABA foreign key (AGCY_KEY)
      references CS_AGENCY_BASE (AGCY_KEY)
go

alter table CS_AGENT_DELTA
   add constraint FK_CADE_CABA1 foreign key (AGNT_KEY)
      references CS_AGENT_BASE (AGNT_KEY)
go

alter table CS_AUDIT_INFO_BASE
   add constraint FK_CAIB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_AUDIT_INFO_DELTA
   add constraint FK_CAID_CAIB foreign key (AUDIT_INFO_KEY)
      references CS_AUDIT_INFO_BASE (AUDIT_INFO_KEY)
go

alter table CS_BILL_ACCT_DELTA
   add constraint FK_CBAD_CBAB foreign key (BILL_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_ACCT_DELTA
   add constraint FK_CBAD_CBPB foreign key (BILL_PLAN_KEY)
      references CS_BILL_PLAN_BASE (PLAN_KEY)
go

alter table CS_BILL_ACCT_DELTA
   add constraint FK_CBAD_CBPB1 foreign key (ALLOC_PLAN_KEY)
      references CS_BILL_PLAN_BASE (PLAN_KEY)
go

alter table CS_BILL_ACCT_DELTA
   add constraint FK_CBAD_CBPB2 foreign key (DELNQNT_PLAN_KEY)
      references CS_BILL_PLAN_BASE (PLAN_KEY)
go

alter table CS_BILL_AP_TRAN
   add constraint FK_CBAT_CBCCB foreign key (CHRG_COMM_KEY)
      references CS_BILL_CHRG_COMM_BASE (CHRG_COMM_KEY)
go

alter table CS_BILL_AP_TRAN
   add constraint FK_CBAT_CBCRB foreign key (COMM_RED_KEY)
      references CS_BILL_COMM_REDUCT_BASE (COMM_RED_KEY)
go

alter table CS_BILL_AP_TRAN
   add constraint FK_CBAT_CBDB foreign key (AGCY_CYCLE_DISTR_KEY)
      references CS_BILL_DISTR_BASE (DISTR_KEY)
go

alter table CS_BILL_AP_TRAN
   add constraint FK_CBAT_CBDIB1 foreign key (DISTR_ITEM_KEY)
      references CS_BILL_DISTR_ITEM_BASE (DISTR_ITEM_KEY)
go

alter table CS_BILL_AP_TRAN
   add constraint FK_CBAT_CBIIB1 foreign key (BILL_INV_ITEM_KEY)
      references CS_BILL_INV_ITEM_BASE (BILL_INV_ITEM_KEY)
go

alter table CS_BILL_AP_TRAN
   add constraint FK_CBAT_CBNWB foreign key (NEG_WO_KEY)
      references CS_BILL_NEG_WRITEOFF_BASE (NEG_WO_KEY)
go

alter table CS_BILL_AP_TRAN
   add constraint FK_CBAT_CBPB1 foreign key (PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_AP_TRAN
   add constraint FK_CBAT_CBPB2 foreign key (CR_PYBL_OF_PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_AP_TRAN
   add constraint FK_CBAT_CBPCB foreign key (POL_COMM_KEY)
      references CS_BILL_POL_COMM_BASE (POL_COMM_KEY)
go

alter table CS_BILL_AP_TRAN
   add constraint FK_CBAT_CBPCB1 foreign key (PRDCR_CD_KEY)
      references CS_BILL_PROD_CD_BASE (PRDCR_CD_KEY)
go

alter table CS_BILL_AP_TRAN
   add constraint FK_CBAT_CBPPB foreign key (PRDCR_PMT_KEY)
      references CS_BILL_PROD_PMT_BASE (PRDCR_PMT_KEY)
go

alter table CS_BILL_AP_TRAN
   add constraint FK_CBAT_CBPSB foreign key (PRDCR_STMNT_KEY)
      references CS_BILL_PROD_STMNT_BASE (PRDCR_STMNT_KEY)
go

alter table CS_BILL_AP_TRAN
   add constraint FK_CBAT_CBPSB1 foreign key (PYBL_RECEIVER_STMNT_KEY)
      references CS_BILL_PROD_STMNT_BASE (PRDCR_STMNT_KEY)
go

alter table CS_BILL_AP_TRAN
   add constraint FK_CBAT_CBWB1 foreign key (WO_KEY)
      references CS_BILL_WRITEOFF_BASE (WO_KEY)
go

alter table CS_BILL_AR_TRAN
   add constraint FK_CBAT_CBAB foreign key (FROM_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_AR_TRAN
   add constraint FK_CBAT_CBCB foreign key (CHRG_KEY)
      references CS_BILL_CHRG_BASE (CHRG_KEY)
go

alter table CS_BILL_AR_TRAN
   add constraint FK_CBAT_CBCCB1 foreign key (CHRG_COMM_KEY)
      references CS_BILL_CHRG_COMM_BASE (CHRG_COMM_KEY)
go

alter table CS_BILL_AR_TRAN
   add constraint FK_CBAT_CBDIB foreign key (DISTR_ITEM_KEY)
      references CS_BILL_DISTR_ITEM_BASE (DISTR_ITEM_KEY)
go

alter table CS_BILL_AR_TRAN
   add constraint FK_CBAT_CBIIB foreign key (BILL_INV_ITEM_KEY)
      references CS_BILL_INV_ITEM_BASE (BILL_INV_ITEM_KEY)
go

alter table CS_BILL_AR_TRAN
   add constraint FK_CBAT_CBPB foreign key (FROM_PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_AR_TRAN
   add constraint FK_CBAT_CBWB foreign key (WO_KEY)
      references CS_BILL_WRITEOFF_BASE (WO_KEY)
go

alter table CS_BILL_CASH_TRAN
   add constraint FK_CBCT_CBAB foreign key (BILL_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_CASH_TRAN
   add constraint FK_CBCT_CBDB foreign key (DISB_KEY)
      references CS_BILL_DISB_BASE (DISB_KEY)
go

alter table CS_BILL_CASH_TRAN
   add constraint FK_CBCT_CBMRB foreign key (MON_RCVD_KEY)
      references CS_BILL_MONEY_RCVD_BASE (MON_RCVD_KEY)
go

alter table CS_BILL_CASH_TRAN
   add constraint FK_CBCT_CBNWB foreign key (NEG_WO_KEY)
      references CS_BILL_NEG_WRITEOFF_BASE (NEG_WO_KEY)
go

alter table CS_BILL_CASH_TRAN
   add constraint FK_CBCT_CBPB foreign key (PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_CASH_TRAN
   add constraint FK_CBCT_CBSPB foreign key (SUSP_PMT_KEY)
      references CS_BILL_SUSP_PMT_BASE (SUSP_PMT_KEY)
go

alter table CS_BILL_CASH_TRAN
   add constraint FK_CBCT_CBUFB foreign key (UNAPPLIED_FUND_KEY)
      references CS_BILL_UNAPP_FUND_BASE (UNAPPLIED_FUND_KEY)
go

alter table CS_BILL_CHRG_COMM_DELTA
   add constraint FK_CBCCD_CBCB foreign key (CHRG_KEY)
      references CS_BILL_CHRG_BASE (CHRG_KEY)
go

alter table CS_BILL_CHRG_COMM_DELTA
   add constraint FK_CBCCD_CBCCB foreign key (CHRG_COMM_KEY)
      references CS_BILL_CHRG_COMM_BASE (CHRG_COMM_KEY)
go

alter table CS_BILL_CHRG_COMM_DELTA
   add constraint FK_CBCCD_CBPCB foreign key (POL_COMM_KEY)
      references CS_BILL_POL_COMM_BASE (POL_COMM_KEY)
go

alter table CS_BILL_CHRG_DELTA
   add constraint FK_CBCD_CBCB foreign key (CHRG_KEY)
      references CS_BILL_CHRG_BASE (CHRG_KEY)
go

alter table CS_BILL_CHRG_DELTA
   add constraint FK_CBCD_CBCPB foreign key (CHRG_PAT_KEY)
      references CS_BILL_CHRG_PAT_BASE (CHRG_PAT_KEY)
go

alter table CS_BILL_CHRG_DELTA
   add constraint FK_CBCD_CBIB foreign key (BILL_INSTR_KEY)
      references CS_BILL_INSTRUCTION_BASE (BILL_INSTR_KEY)
go

alter table CS_BILL_CHRG_DELTA
   add constraint FK_CBCD_CBPCB foreign key (PRIM_COMM_PRDCR_CD_KEY)
      references CS_BILL_PROD_CD_BASE (PRDCR_CD_KEY)
go

alter table CS_BILL_CHRG_PAT_DELTA
   add constraint FK_CBCPD_CBCPB foreign key (CHRG_PAT_KEY)
      references CS_BILL_CHRG_PAT_BASE (CHRG_PAT_KEY)
go

alter table CS_BILL_CHRG_PAT_DELTA
   add constraint FK_CBCPD_CBTOPB foreign key (TACCT_OWNER_PAT_KEY)
      references CS_BILL_TACCT_OWNER_PAT_BASE (TACCT_OWNER_PAT_KEY)
go

alter table CS_BILL_COMM_AGG
   add constraint FK_CBCA_CBAB foreign key (BILL_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_COMM_AGG
   add constraint FK_CBCA_CBCB foreign key (CHRG_KEY)
      references CS_BILL_CHRG_BASE (CHRG_KEY)
go

alter table CS_BILL_COMM_AGG
   add constraint FK_CBCA_CBCSRB foreign key (COMM_SPLAN_RATE_KEY)
      references CS_BILL_COMM_SPLAN_RATE_BASE (COMM_SPLAN_RATE_KEY)
go

alter table CS_BILL_COMM_AGG
   add constraint FK_CBCA_CBPB foreign key (PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_COMM_AGG
   add constraint FK_CBCA_CBPB1 foreign key (BILL_POL_KEY)
      references CS_BILL_POL_BASE (BILL_POL_KEY)
go

alter table CS_BILL_COMM_AGG
   add constraint FK_CBCA_CBPCB foreign key (PRDCR_CD_KEY)
      references CS_BILL_PROD_CD_BASE (PRDCR_CD_KEY)
go

alter table CS_BILL_COMM_AGG
   add constraint FK_CBCA_CBPPB foreign key (BILL_POL_PRD_KEY)
      references CS_BILL_POL_PRD_BASE (BILL_POL_PRD_KEY)
go

alter table CS_BILL_COMM_AGG
   add constraint FK_CBCA_CMDI foreign key (TRANS_MTH_ID)
      references CS_MONTH_DIM (MTH_ID)
go

alter table CS_BILL_COMM_REDUCT_DELTA
   add constraint FK_CBCRD_CBCCB foreign key (CHRG_COMM_KEY)
      references CS_BILL_CHRG_COMM_BASE (CHRG_COMM_KEY)
go

alter table CS_BILL_COMM_REDUCT_DELTA
   add constraint FK_CBCRD_CBCRB foreign key (COMM_RED_KEY)
      references CS_BILL_COMM_REDUCT_BASE (COMM_RED_KEY)
go

alter table CS_BILL_COMM_REDUCT_DELTA
   add constraint FK_CBCRD_CBIIB foreign key (BILL_INV_ITEM_KEY)
      references CS_BILL_INV_ITEM_BASE (BILL_INV_ITEM_KEY)
go

alter table CS_BILL_COMM_REDUCT_DELTA
   add constraint FK_CBCRD_CBWB foreign key (WO_KEY)
      references CS_BILL_WRITEOFF_BASE (WO_KEY)
go

alter table CS_BILL_COMM_SPLAN_DELTA
   add constraint FK_CBCSD_CBCSB foreign key (COMM_SPLAN_KEY)
      references CS_BILL_COMM_SPLAN_BASE (COMM_SPLAN_KEY)
go

alter table CS_BILL_COMM_SPLAN_DELTA
   add constraint FK_CBCSD_CBPB foreign key (PLAN_KEY)
      references CS_BILL_PLAN_BASE (PLAN_KEY)
go

alter table CS_BILL_COMM_SPLAN_RATE_DELTA
   add constraint FK_CBCSRD_CBCSB foreign key (COMM_SPLAN_KEY)
      references CS_BILL_COMM_SPLAN_BASE (COMM_SPLAN_KEY)
go

alter table CS_BILL_COMM_SPLAN_RATE_DELTA
   add constraint FK_CBCSRD_CBCSRB foreign key (COMM_SPLAN_RATE_KEY)
      references CS_BILL_COMM_SPLAN_RATE_BASE (COMM_SPLAN_RATE_KEY)
go

alter table CS_BILL_CREDIT_DELTA
   add constraint FK_CBCD_CBAB foreign key (BILL_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_CREDIT_DELTA
   add constraint FK_CBCD_CBCB1 foreign key (CR_KEY)
      references CS_BILL_CREDIT_BASE (CR_KEY)
go

alter table CS_BILL_CREDIT_DELTA
   add constraint FK_CBCD_CBUFB foreign key (UNAPPLIED_FUND_KEY)
      references CS_BILL_UNAPP_FUND_BASE (UNAPPLIED_FUND_KEY)
go

alter table CS_BILL_DISB_DELTA
   add constraint FK_CBDD_CBAB foreign key (BILL_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_DISB_DELTA
   add constraint FK_CBDD_CBDB foreign key (DISB_KEY)
      references CS_BILL_DISB_BASE (DISB_KEY)
go

alter table CS_BILL_DISB_DELTA
   add constraint FK_CBDD_CBDB2 foreign key (DISTR_KEY)
      references CS_BILL_DISTR_BASE (DISTR_KEY)
go

alter table CS_BILL_DISB_DELTA
   add constraint FK_CBDD_CBPB foreign key (PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_DISB_DELTA
   add constraint FK_CBDD_CBPIB foreign key (PMT_INSTRUMENT_KEY)
      references CS_BILL_PMT_INSTR_BASE (PMT_INSTRUMENT_KEY)
go

alter table CS_BILL_DISB_DELTA
   add constraint FK_CBDD_CBSPB foreign key (SUSP_PMT_KEY)
      references CS_BILL_SUSP_PMT_BASE (SUSP_PMT_KEY)
go

alter table CS_BILL_DISB_DELTA
   add constraint FK_CBDD_CBUFB foreign key (UNAPPLIED_FUND_KEY)
      references CS_BILL_UNAPP_FUND_BASE (UNAPPLIED_FUND_KEY)
go

alter table CS_BILL_DISTR_DELTA
   add constraint FK_CBDD_CBDB1 foreign key (DISTR_KEY)
      references CS_BILL_DISTR_BASE (DISTR_KEY)
go

alter table CS_BILL_DISTR_ITEM_DELTA
   add constraint FK_CBDID_CBDB foreign key (ACTIVE_DISTR_KEY)
      references CS_BILL_DISTR_BASE (DISTR_KEY)
go

alter table CS_BILL_DISTR_ITEM_DELTA
   add constraint FK_CBDID_CBDB1 foreign key (REVERSED_DISTR_KEY)
      references CS_BILL_DISTR_BASE (DISTR_KEY)
go

alter table CS_BILL_DISTR_ITEM_DELTA
   add constraint FK_CBDID_CBDIB foreign key (DISTR_ITEM_KEY)
      references CS_BILL_DISTR_ITEM_BASE (DISTR_ITEM_KEY)
go

alter table CS_BILL_DISTR_ITEM_DELTA
   add constraint FK_CBDID_CBIIB foreign key (BILL_INV_ITEM_KEY)
      references CS_BILL_INV_ITEM_BASE (BILL_INV_ITEM_KEY)
go

alter table CS_BILL_DISTR_ITEM_DELTA
   add constraint FK_CBDID_CBPCB foreign key (PRDCR_CD_KEY)
      references CS_BILL_PROD_CD_BASE (PRDCR_CD_KEY)
go

alter table CS_BILL_DISTR_ITEM_DELTA
   add constraint FK_CBDID_CBPPB foreign key (BILL_POL_PRD_KEY)
      references CS_BILL_POL_PRD_BASE (BILL_POL_PRD_KEY)
go

alter table CS_BILL_FUNDS_TRNFR_DELTA
   add constraint FK_CBFTD_CBFTB foreign key (FUNDS_TFR_KEY)
      references CS_BILL_FUNDS_TRNFR_BASE (FUNDS_TFR_KEY)
go

alter table CS_BILL_FUNDS_TRNFR_DELTA
   add constraint FK_CBFTD_CBPB foreign key (SRCE_PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_FUNDS_TRNFR_DELTA
   add constraint FK_CBFTD_CBPB1 foreign key (TARGET_PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_FUNDS_TRNFR_DELTA
   add constraint FK_CBFTD_CBUFB foreign key (SRCE_UNAPPLIED_FUND_KEY)
      references CS_BILL_UNAPP_FUND_BASE (UNAPPLIED_FUND_KEY)
go

alter table CS_BILL_FUNDS_TRNFR_DELTA
   add constraint FK_CBFTD_CBUFB1 foreign key (TARGET_UNAPPLIED_FUND_KEY)
      references CS_BILL_UNAPP_FUND_BASE (UNAPPLIED_FUND_KEY)
go

alter table CS_BILL_GENERAL_TRAN
   add constraint FK_CBGT_CBAB foreign key (SRCE_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_GENERAL_TRAN
   add constraint FK_CBGT_CBAB1 foreign key (TARGET_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_GENERAL_TRAN
   add constraint FK_CBGT_CBAB2 foreign key (CR_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_GENERAL_TRAN
   add constraint FK_CBGT_CBAB3 foreign key (SUSP_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_GENERAL_TRAN
   add constraint FK_CBGT_CBCB1 foreign key (CR_KEY)
      references CS_BILL_CREDIT_BASE (CR_KEY)
go

alter table CS_BILL_GENERAL_TRAN
   add constraint FK_CBGT_CBFTB foreign key (FUNDS_TFR_KEY)
      references CS_BILL_FUNDS_TRNFR_BASE (FUNDS_TFR_KEY)
go

alter table CS_BILL_GENERAL_TRAN
   add constraint FK_CBGT_CBNDIB foreign key (NON_RCVBL_DISTR_ITEM_KEY)
      references CS_BILL_NR_DISTR_ITEM_BASE (NON_RCVBL_DISTR_ITEM_KEY)
go

alter table CS_BILL_GENERAL_TRAN
   add constraint FK_CBGT_CBPB foreign key (SUSP_PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_GENERAL_TRAN
   add constraint FK_CBGT_CBPB1 foreign key (SRCE_PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_GENERAL_TRAN
   add constraint FK_CBGT_CBPB2 foreign key (TARGET_PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_INSTRUCTION_DELTA
   add constraint FK_CBID_CBAB foreign key (BILL_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_INSTRUCTION_DELTA
   add constraint FK_CBID_CBAB1 foreign key (ISSUANCE_BILL_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_INSTRUCTION_DELTA
   add constraint FK_CBID_CBAB2 foreign key (NEW_RENEW_BILL_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_INSTRUCTION_DELTA
   add constraint FK_CBID_CBAB3 foreign key (REWRITE_BILL_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_INSTRUCTION_DELTA
   add constraint FK_CBID_CBAB4 foreign key (RENEW_BILL_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_INSTRUCTION_DELTA
   add constraint FK_CBID_CBIB1 foreign key (BILL_INSTR_KEY)
      references CS_BILL_INSTRUCTION_BASE (BILL_INSTR_KEY)
go

alter table CS_BILL_INSTRUCTION_DELTA
   add constraint FK_CBID_CBPPB foreign key (BILL_POL_PRD_KEY)
      references CS_BILL_POL_PRD_BASE (BILL_POL_PRD_KEY)
go

alter table CS_BILL_INSTRUCTION_DELTA
   add constraint FK_CBID_CBPPB1 foreign key (ASSOC_BILL_POL_PRD_KEY)
      references CS_BILL_POL_PRD_BASE (BILL_POL_PRD_KEY)
go

alter table CS_BILL_INSTRUCTION_DELTA
   add constraint FK_CBID_CBPPB2 foreign key (PRIOR_BILL_POL_PRD_KEY)
      references CS_BILL_POL_PRD_BASE (BILL_POL_PRD_KEY)
go

alter table CS_BILL_INSTRUCTION_DELTA
   add constraint FK_CBID_CBPPB3 foreign key (NEW_BILL_POL_PRD_KEY)
      references CS_BILL_POL_PRD_BASE (BILL_POL_PRD_KEY)
go

alter table CS_BILL_INV_DELTA
   add constraint FK_CBID_CBAB5 foreign key (BILL_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_INV_DELTA
   add constraint FK_CBID_CBIB foreign key (BILL_INV_KEY)
      references CS_BILL_INV_BASE (BILL_INV_KEY)
go

alter table CS_BILL_INV_ITEM_DELTA
   add constraint FK_CBIID_CBCB foreign key (CHRG_KEY)
      references CS_BILL_CHRG_BASE (CHRG_KEY)
go

alter table CS_BILL_INV_ITEM_DELTA
   add constraint FK_CBIID_CBIB foreign key (BILL_INV_KEY)
      references CS_BILL_INV_BASE (BILL_INV_KEY)
go

alter table CS_BILL_INV_ITEM_DELTA
   add constraint FK_CBIID_CBIIB foreign key (BILL_INV_ITEM_KEY)
      references CS_BILL_INV_ITEM_BASE (BILL_INV_ITEM_KEY)
go

alter table CS_BILL_INV_ITEM_DELTA
   add constraint FK_CBIID_CBPPB foreign key (BILL_POL_PRD_KEY)
      references CS_BILL_POL_PRD_BASE (BILL_POL_PRD_KEY)
go

alter table CS_BILL_INV_MONTH_AGG
   add constraint FK_CBIMA_CBAB foreign key (BILL_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_INV_MONTH_AGG
   add constraint FK_CBIMA_CBCB foreign key (CHRG_KEY)
      references CS_BILL_CHRG_BASE (CHRG_KEY)
go

alter table CS_BILL_INV_MONTH_AGG
   add constraint FK_CBIMA_CBCPB foreign key (CHRG_PAT_KEY)
      references CS_BILL_CHRG_PAT_BASE (CHRG_PAT_KEY)
go

alter table CS_BILL_INV_MONTH_AGG
   add constraint FK_CBIMA_CBIB foreign key (BILL_INV_KEY)
      references CS_BILL_INV_BASE (BILL_INV_KEY)
go

alter table CS_BILL_INV_MONTH_AGG
   add constraint FK_CBIMA_CBPB foreign key (PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_INV_MONTH_AGG
   add constraint FK_CBIMA_CBPPB foreign key (BILL_POL_PRD_KEY)
      references CS_BILL_POL_PRD_BASE (BILL_POL_PRD_KEY)
go

alter table CS_BILL_INV_MONTH_AGG
   add constraint FK_CBIMA_CBWB foreign key (WO_KEY)
      references CS_BILL_WRITEOFF_BASE (WO_KEY)
go

alter table CS_BILL_MONEY_RCVD_DELTA
   add constraint FK_CBMRD_CBAB foreign key (BILL_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_MONEY_RCVD_DELTA
   add constraint FK_CBMRD_CBDB foreign key (DISTR_KEY)
      references CS_BILL_DISTR_BASE (DISTR_KEY)
go

alter table CS_BILL_MONEY_RCVD_DELTA
   add constraint FK_CBMRD_CBIB foreign key (BILL_INV_KEY)
      references CS_BILL_INV_BASE (BILL_INV_KEY)
go

alter table CS_BILL_MONEY_RCVD_DELTA
   add constraint FK_CBMRD_CBMRB foreign key (MON_RCVD_KEY)
      references CS_BILL_MONEY_RCVD_BASE (MON_RCVD_KEY)
go

alter table CS_BILL_MONEY_RCVD_DELTA
   add constraint FK_CBMRD_CBPB foreign key (PAYING_PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_MONEY_RCVD_DELTA
   add constraint FK_CBMRD_CBPB1 foreign key (PROMISING_PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_MONEY_RCVD_DELTA
   add constraint FK_CBMRD_CBPIB foreign key (PMT_INSTRUMENT_KEY)
      references CS_BILL_PMT_INSTR_BASE (PMT_INSTRUMENT_KEY)
go

alter table CS_BILL_MONEY_RCVD_DELTA
   add constraint FK_CBMRD_CBPPB foreign key (BILL_POL_PRD_KEY)
      references CS_BILL_POL_PRD_BASE (BILL_POL_PRD_KEY)
go

alter table CS_BILL_MONEY_RCVD_DELTA
   add constraint FK_CBMRD_CBUFB foreign key (UNAPPLIED_FUND_KEY)
      references CS_BILL_UNAPP_FUND_BASE (UNAPPLIED_FUND_KEY)
go

alter table CS_BILL_NEG_WRITEOFF_DELTA
   add constraint FK_CBNWD_CBAB foreign key (BILL_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_NEG_WRITEOFF_DELTA
   add constraint FK_CBNWD_CBNWB foreign key (NEG_WO_KEY)
      references CS_BILL_NEG_WRITEOFF_BASE (NEG_WO_KEY)
go

alter table CS_BILL_NEG_WRITEOFF_DELTA
   add constraint FK_CBNWD_CBPB foreign key (PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_NEG_WRITEOFF_DELTA
   add constraint FK_CBNWD_CBUFB foreign key (UNAPPLIED_FUND_KEY)
      references CS_BILL_UNAPP_FUND_BASE (UNAPPLIED_FUND_KEY)
go

alter table CS_BILL_NR_DISTR_ITEM_DELTA
   add constraint FK_CBNDID_CBDB foreign key (ACTIVE_DISTR_KEY)
      references CS_BILL_DISTR_BASE (DISTR_KEY)
go

alter table CS_BILL_NR_DISTR_ITEM_DELTA
   add constraint FK_CBNDID_CBDB1 foreign key (REVERSED_DISTR_KEY)
      references CS_BILL_DISTR_BASE (DISTR_KEY)
go

alter table CS_BILL_NR_DISTR_ITEM_DELTA
   add constraint FK_CBNDID_CBNDIB foreign key (NON_RCVBL_DISTR_ITEM_KEY)
      references CS_BILL_NR_DISTR_ITEM_BASE (NON_RCVBL_DISTR_ITEM_KEY)
go

alter table CS_BILL_NR_DISTR_ITEM_DELTA
   add constraint FK_CBNDID_CBPPB foreign key (BILL_POL_PRD_KEY)
      references CS_BILL_POL_PRD_BASE (BILL_POL_PRD_KEY)
go

alter table CS_BILL_OUTGOING_PMT_DELTA
   add constraint FK_CBOPD_CBDB foreign key (DISB_KEY)
      references CS_BILL_DISB_BASE (DISB_KEY)
go

alter table CS_BILL_OUTGOING_PMT_DELTA
   add constraint FK_CBOPD_CBOPB foreign key (OUTGOING_PMT_KEY)
      references CS_BILL_OUTGOING_PMT_BASE (OUTGOING_PMT_KEY)
go

alter table CS_BILL_OUTGOING_PMT_DELTA
   add constraint FK_CBOPD_CBPIB foreign key (PMT_INSTRUMENT_KEY)
      references CS_BILL_PMT_INSTR_BASE (PMT_INSTRUMENT_KEY)
go

alter table CS_BILL_OUTGOING_PMT_DELTA
   add constraint FK_CBOPD_CBPPB foreign key (PRDCR_PMT_KEY)
      references CS_BILL_PROD_PMT_BASE (PRDCR_PMT_KEY)
go

alter table CS_BILL_PLAN_DELTA
   add constraint FK_CBPD_CBPB3 foreign key (PLAN_KEY)
      references CS_BILL_PLAN_BASE (PLAN_KEY)
go

alter table CS_BILL_PMT_INSTR_DELTA
   add constraint FK_CBPID_CBAB foreign key (BILL_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_PMT_INSTR_DELTA
   add constraint FK_CBPID_CBPB foreign key (PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_PMT_INSTR_DELTA
   add constraint FK_CBPID_CBPIB foreign key (PMT_INSTRUMENT_KEY)
      references CS_BILL_PMT_INSTR_BASE (PMT_INSTRUMENT_KEY)
go

alter table CS_BILL_POL_COMM_DELTA
   add constraint FK_CBPCD_CBCSB foreign key (COMM_SPLAN_KEY)
      references CS_BILL_COMM_SPLAN_BASE (COMM_SPLAN_KEY)
go

alter table CS_BILL_POL_COMM_DELTA
   add constraint FK_CBPCD_CBPCB1 foreign key (POL_COMM_KEY)
      references CS_BILL_POL_COMM_BASE (POL_COMM_KEY)
go

alter table CS_BILL_POL_COMM_DELTA
   add constraint FK_CBPCD_CBPCB2 foreign key (PRDCR_CD_KEY)
      references CS_BILL_PROD_CD_BASE (PRDCR_CD_KEY)
go

alter table CS_BILL_POL_COMM_DELTA
   add constraint FK_CBPCD_CBPPB foreign key (BILL_POL_PRD_KEY)
      references CS_BILL_POL_PRD_BASE (BILL_POL_PRD_KEY)
go

alter table CS_BILL_POL_COMM_DELTA
   add constraint FK_CBPCD_CBPPB1 foreign key (PRIM_BILL_POL_PRD_KEY)
      references CS_BILL_POL_PRD_BASE (BILL_POL_PRD_KEY)
go

alter table CS_BILL_POL_DELTA
   add constraint FK_CBPD_CBAB foreign key (BILL_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_POL_DELTA
   add constraint FK_CBPD_CBPB2 foreign key (BILL_POL_KEY)
      references CS_BILL_POL_BASE (BILL_POL_KEY)
go

alter table CS_BILL_POL_PRD_DELTA
   add constraint FK_CBPPD_CBPB1 foreign key (PMT_PLAN_KEY)
      references CS_BILL_PLAN_BASE (PLAN_KEY)
go

alter table CS_BILL_POL_PRD_DELTA
   add constraint FK_CBPPD_CBPB2 foreign key (DELNQNT_PLAN_KEY)
      references CS_BILL_PLAN_BASE (PLAN_KEY)
go

alter table CS_BILL_POL_PRD_DELTA
   add constraint FK_CBPPD_CBPB3 foreign key (RETURN_PREM_PLAN_KEY)
      references CS_BILL_PLAN_BASE (PLAN_KEY)
go

alter table CS_BILL_POL_PRD_DELTA
   add constraint FK_CBPPD_CBPB4 foreign key (BILL_POL_KEY)
      references CS_BILL_POL_BASE (BILL_POL_KEY)
go

alter table CS_BILL_POL_PRD_DELTA
   add constraint FK_CBPPD_CBPPB1 foreign key (BILL_POL_PRD_KEY)
      references CS_BILL_POL_PRD_BASE (BILL_POL_PRD_KEY)
go

alter table CS_BILL_PROD_CD_DELTA
   add constraint FK_CBPCD_CBPB foreign key (PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_PROD_CD_DELTA
   add constraint FK_CBPCD_CBPB1 foreign key (COMM_PLAN_KEY)
      references CS_BILL_PLAN_BASE (PLAN_KEY)
go

alter table CS_BILL_PROD_CD_DELTA
   add constraint FK_CBPCD_CBPCB foreign key (PRDCR_CD_KEY)
      references CS_BILL_PROD_CD_BASE (PRDCR_CD_KEY)
go

alter table CS_BILL_PROD_DELTA
   add constraint FK_CBPD_CBPB foreign key (PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_PROD_DELTA
   add constraint FK_CBPD_CBPB1 foreign key (AGCY_BILL_PLAN_KEY)
      references CS_BILL_PLAN_BASE (PLAN_KEY)
go

alter table CS_BILL_PROD_PMT_DELTA
   add constraint FK_CBPPD_CBPB foreign key (PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_PROD_PMT_DELTA
   add constraint FK_CBPPD_CBPPB foreign key (PRDCR_PMT_KEY)
      references CS_BILL_PROD_PMT_BASE (PRDCR_PMT_KEY)
go

alter table CS_BILL_PROD_STMNT_DELTA
   add constraint FK_CBPSD_CBPB foreign key (PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_PROD_STMNT_DELTA
   add constraint FK_CBPSD_CBPSB foreign key (PRDCR_STMNT_KEY)
      references CS_BILL_PROD_STMNT_BASE (PRDCR_STMNT_KEY)
go

alter table CS_BILL_SUSP_PMT_DELTA
   add constraint FK_CBSPD_CBAB foreign key (BILL_ACCT_APPLIED_TO_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_SUSP_PMT_DELTA
   add constraint FK_CBSPD_CBMRB foreign key (MON_RCVD_KEY)
      references CS_BILL_MONEY_RCVD_BASE (MON_RCVD_KEY)
go

alter table CS_BILL_SUSP_PMT_DELTA
   add constraint FK_CBSPD_CBPB foreign key (PRDCR_APPLIED_TO_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_SUSP_PMT_DELTA
   add constraint FK_CBSPD_CBPIB foreign key (PMT_INSTRUMENT_KEY)
      references CS_BILL_PMT_INSTR_BASE (PMT_INSTRUMENT_KEY)
go

alter table CS_BILL_SUSP_PMT_DELTA
   add constraint FK_CBSPD_CBPPB foreign key (BILL_POL_PRD_APPLIED_TO_KEY)
      references CS_BILL_POL_PRD_BASE (BILL_POL_PRD_KEY)
go

alter table CS_BILL_SUSP_PMT_DELTA
   add constraint FK_CBSPD_CBSPB foreign key (SUSP_PMT_KEY)
      references CS_BILL_SUSP_PMT_BASE (SUSP_PMT_KEY)
go

alter table CS_BILL_TACCT_OWNER_PAT_DELTA
   add constraint FK_CBTOPD_CBTOPB foreign key (TACCT_OWNER_PAT_KEY)
      references CS_BILL_TACCT_OWNER_PAT_BASE (TACCT_OWNER_PAT_KEY)
go

alter table CS_BILL_UNAPP_FUND_DELTA
   add constraint FK_CBUFD_CBAB foreign key (BILL_ACCT_KEY)
      references CS_BILL_ACCT_BASE (BILL_ACCT_KEY)
go

alter table CS_BILL_UNAPP_FUND_DELTA
   add constraint FK_CBUFD_CBPB foreign key (BILL_POL_KEY)
      references CS_BILL_POL_BASE (BILL_POL_KEY)
go

alter table CS_BILL_UNAPP_FUND_DELTA
   add constraint FK_CBUFD_CBUFB foreign key (UNAPPLIED_FUND_KEY)
      references CS_BILL_UNAPP_FUND_BASE (UNAPPLIED_FUND_KEY)
go

alter table CS_BILL_WRITEOFF_DELTA
   add constraint FK_CBWD_CBCCB foreign key (CHRG_COMM_KEY)
      references CS_BILL_CHRG_COMM_BASE (CHRG_COMM_KEY)
go

alter table CS_BILL_WRITEOFF_DELTA
   add constraint FK_CBWD_CBCPB foreign key (CHRG_PAT_KEY)
      references CS_BILL_CHRG_PAT_BASE (CHRG_PAT_KEY)
go

alter table CS_BILL_WRITEOFF_DELTA
   add constraint FK_CBWD_CBDIB foreign key (COMM_AGCY_PMT_ITEM_KEY)
      references CS_BILL_DISTR_ITEM_BASE (DISTR_ITEM_KEY)
go

alter table CS_BILL_WRITEOFF_DELTA
   add constraint FK_CBWD_CBDIB1 foreign key (GROSS_AGCY_PMT_ITEM_KEY)
      references CS_BILL_DISTR_ITEM_BASE (DISTR_ITEM_KEY)
go

alter table CS_BILL_WRITEOFF_DELTA
   add constraint FK_CBWD_CBIIB foreign key (BILL_INV_ITEM_KEY)
      references CS_BILL_INV_ITEM_BASE (BILL_INV_ITEM_KEY)
go

alter table CS_BILL_WRITEOFF_DELTA
   add constraint FK_CBWD_CBPB foreign key (PRDCR_KEY)
      references CS_BILL_PROD_BASE (PRDCR_KEY)
go

alter table CS_BILL_WRITEOFF_DELTA
   add constraint FK_CBWD_CBWB foreign key (WO_KEY)
      references CS_BILL_WRITEOFF_BASE (WO_KEY)
go

alter table CS_BLDG_BASE
   add constraint FK_CBBA_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_BLDG_DELTA
   add constraint FK_CBDE_CBBA foreign key (BLDG_KEY)
      references CS_BLDG_BASE (BLDG_KEY)
go

alter table CS_BLDG_DELTA
   add constraint FK_CBDE_CLBA foreign key (LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_CA_ADDL_INTRST_BASE
   add constraint FK_CCAIB_CCCB foreign key (CA_CVRBL_KEY)
      references CS_CA_CVRBL_BASE (CA_CVRBL_KEY)
go

alter table CS_CA_ADDL_INTRST_BASE
   add constraint FK_CCAIB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CA_ADDL_INTRST_BASE
   add constraint FK_CCAIB_CPPB foreign key (POL_PARTY_KEY)
      references CS_POL_PARTY_BASE (POL_PARTY_KEY)
go

alter table CS_CA_ADDL_INTRST_DELTA
   add constraint FK_CCAID_CCAIB foreign key (CA_ADDL_INTRST_KEY)
      references CS_CA_ADDL_INTRST_BASE (CA_ADDL_INTRST_KEY)
go

alter table CS_CA_COND_BASE
   add constraint FK_CCCB_CCBA foreign key (COND_KEY)
      references CS_COND_BASE (COND_KEY)
go

alter table CS_CA_COND_BASE
   add constraint FK_CCCB_CCCB foreign key (CA_CVRBL_KEY)
      references CS_CA_CVRBL_BASE (CA_CVRBL_KEY)
go

alter table CS_CA_COND_BASE
   add constraint FK_CCCB_CPBA1 foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CA_COND_BASE
   add constraint FK_CCCB_CPLB1 foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_CA_COND_TERM_BASE
   add constraint FK_CCCTB_CCCB foreign key (CA_COND_KEY)
      references CS_CA_COND_BASE (CA_COND_KEY)
go

alter table CS_CA_COND_TERM_DELTA
   add constraint FK_CCCTD_CCCTB1 foreign key (CA_COND_TERM_KEY)
      references CS_CA_COND_TERM_BASE (CA_COND_TERM_KEY)
go

alter table CS_CA_COVG_BASE
   add constraint FK_CCCB_CCBA1 foreign key (COVG_KEY)
      references CS_COVG_BASE (COVG_KEY)
go

alter table CS_CA_COVG_BASE
   add constraint FK_CCCB_CCCB1 foreign key (CA_CVRBL_KEY)
      references CS_CA_CVRBL_BASE (CA_CVRBL_KEY)
go

alter table CS_CA_COVG_BASE
   add constraint FK_CCCB_CPBA2 foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CA_COVG_BASE
   add constraint FK_CCCB_CPLB2 foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_CA_COVG_TERM_BASE
   add constraint FK_CCCTB_CCCB1 foreign key (CA_COVG_KEY)
      references CS_CA_COVG_BASE (CA_COVG_KEY)
go

alter table CS_CA_COVG_TERM_DELTA
   add constraint FK_CCCTD_CCCTB foreign key (CA_COVG_TERM_KEY)
      references CS_CA_COVG_TERM_BASE (CA_COVG_TERM_KEY)
go

alter table CS_CA_CVRBL_BASE
   add constraint FK_CCCB_CCCB2 foreign key (OWNING_CVRBL_KEY)
      references CS_CA_CVRBL_BASE (CA_CVRBL_KEY)
go

alter table CS_CA_CVRBL_BASE
   add constraint FK_CCCB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CA_CVRBL_BASE
   add constraint FK_CCCB_CPLB foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_CA_DEALER_DELTA
   add constraint FK_CCDD_CCCB foreign key (CA_CVRBL_KEY)
      references CS_CA_CVRBL_BASE (CA_CVRBL_KEY)
go

alter table CS_CA_DEALER_DELTA
   add constraint FK_CCDD_CLBA foreign key (DEALER_LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_CA_DRVR_BASE
   add constraint FK_CCDB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CA_DRVR_BASE
   add constraint FK_CCDB_CPLB foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_CA_DRVR_DELTA
   add constraint FK_CCDD_CCDB foreign key (CA_DRVR_KEY)
      references CS_CA_DRVR_BASE (CA_DRVR_KEY)
go

alter table CS_CA_EXCL_BASE
   add constraint FK_CCEB_CCCB foreign key (CA_CVRBL_KEY)
      references CS_CA_CVRBL_BASE (CA_CVRBL_KEY)
go

alter table CS_CA_EXCL_BASE
   add constraint FK_CCEB_CEBA foreign key (EXCL_KEY)
      references CS_EXCL_BASE (EXCL_KEY)
go

alter table CS_CA_EXCL_BASE
   add constraint FK_CCEB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CA_EXCL_BASE
   add constraint FK_CCEB_CPLB foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_CA_EXCL_TERM_BASE
   add constraint FK_CCETB_CCEB foreign key (CA_EXCL_KEY)
      references CS_CA_EXCL_BASE (CA_EXCL_KEY)
go

alter table CS_CA_EXCL_TERM_DELTA
   add constraint FK_CCETD_CCETB foreign key (CA_EXCL_TERM_KEY)
      references CS_CA_EXCL_TERM_BASE (CA_EXCL_TERM_KEY)
go

alter table CS_CA_GAR_SRVC_DELTA
   add constraint FK_CCGSD_CCCB foreign key (CA_CVRBL_KEY)
      references CS_CA_CVRBL_BASE (CA_CVRBL_KEY)
go

alter table CS_CA_GAR_SRVC_DELTA
   add constraint FK_CCGSD_CLBA foreign key (GARAGE_SRVC_LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_CA_JURS_DELTA
   add constraint FK_CCJD_CCCB foreign key (CA_CVRBL_KEY)
      references CS_CA_CVRBL_BASE (CA_CVRBL_KEY)
go

alter table CS_CA_LINE_SI_COND_BASE
   add constraint FK_CCLSCB_CCCB foreign key (CA_COND_KEY)
      references CS_CA_COND_BASE (CA_COND_KEY)
go

alter table CS_CA_LINE_SI_COND_BASE
   add constraint FK_CCLSCB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CA_LINE_SI_COND_DELTA
   add constraint FK_CCLSCD_CCLSCB foreign key (CA_LINE_SI_COND_KEY)
      references CS_CA_LINE_SI_COND_BASE (CA_LINE_SI_COND_KEY)
go

alter table CS_CA_LINE_SI_COND_DELTA
   add constraint FK_CCLSCD_CPPB foreign key (POL_PARTY_KEY)
      references CS_POL_PARTY_BASE (POL_PARTY_KEY)
go

alter table CS_CA_MOD_BASE
   add constraint FK_CCMB_CCCB foreign key (CA_CVRBL_KEY)
      references CS_CA_CVRBL_BASE (CA_CVRBL_KEY)
go

alter table CS_CA_MOD_BASE
   add constraint FK_CCMB_CMBA foreign key (MOD_KEY)
      references CS_MOD_BASE (MOD_KEY)
go

alter table CS_CA_MOD_BASE
   add constraint FK_CCMB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CA_MOD_BASE
   add constraint FK_CCMB_CPLB foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_CA_MOD_DELTA
   add constraint FK_CCMD_CCMB foreign key (CA_MOD_KEY)
      references CS_CA_MOD_BASE (CA_MOD_KEY)
go

alter table CS_CA_MOD_RF_BASE
   add constraint FK_CCMRB_CCMB foreign key (CA_MOD_KEY)
      references CS_CA_MOD_BASE (CA_MOD_KEY)
go

alter table CS_CA_MOD_RF_BASE
   add constraint FK_CCMRB_CPBA1 foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CA_MOD_RF_DELTA
   add constraint FK_CCMRD_CCMRB foreign key (CA_MOD_RF_KEY)
      references CS_CA_MOD_RF_BASE (CA_MOD_RF_KEY)
go

alter table CS_CA_NMD_INDIV_DELTA
   add constraint FK_CCNID_CCCB foreign key (CA_CVRBL_KEY)
      references CS_CA_CVRBL_BASE (CA_CVRBL_KEY)
go

alter table CS_CA_POL_LINE_DELTA
   add constraint FK_CCPLD_CPLB foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_CA_PREM_TRAN
   add constraint FK_CCPT_CABA foreign key (AGCY_KEY)
      references CS_AGENCY_BASE (AGCY_KEY)
go

alter table CS_CA_PREM_TRAN
   add constraint FK_CCPT_CABA1 foreign key (AGNT_KEY)
      references CS_AGENT_BASE (AGNT_KEY)
go

alter table CS_CA_PREM_TRAN
   add constraint FK_CCPT_CCBA foreign key (COVG_KEY)
      references CS_COVG_BASE (COVG_KEY)
go

alter table CS_CA_PREM_TRAN
   add constraint FK_CCPT_CCCB foreign key (CA_CVRBL_KEY)
      references CS_CA_CVRBL_BASE (CA_CVRBL_KEY)
go

alter table CS_CA_PREM_TRAN
   add constraint FK_CCPT_CCCB1 foreign key (CA_COVG_KEY)
      references CS_CA_COVG_BASE (CA_COVG_KEY)
go

alter table CS_CA_PREM_TRAN
   add constraint FK_CCPT_CLBA1 foreign key (AUTO_DEALER_LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_CA_PREM_TRAN
   add constraint FK_CCPT_CLBA2 foreign key (GARAGE_SRVC_LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_CA_PREM_TRAN
   add constraint FK_CCPT_CLBA3 foreign key (VEH_GARAGE_LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_CA_PREM_TRAN
   add constraint FK_CCPT_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CA_PREM_TRAN
   add constraint FK_CCPT_CPLB foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_CA_SI_DELTA
   add constraint FK_CCSD_CCAIB foreign key (CA_ADDL_INTRST_KEY)
      references CS_CA_ADDL_INTRST_BASE (CA_ADDL_INTRST_KEY)
go

alter table CS_CA_SI_DELTA
   add constraint FK_CCSD_CCCB foreign key (CA_CVRBL_KEY)
      references CS_CA_CVRBL_BASE (CA_CVRBL_KEY)
go

alter table CS_CA_SI_DELTA
   add constraint FK_CCSD_CCCB1 foreign key (CA_COVG_KEY)
      references CS_CA_COVG_BASE (CA_COVG_KEY)
go

alter table CS_CA_SI_DELTA
   add constraint FK_CCSD_CLBA foreign key (LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_CA_SI_DELTA
   add constraint FK_CCSD_CPPB foreign key (POL_PARTY_KEY)
      references CS_POL_PARTY_BASE (POL_PARTY_KEY)
go

alter table CS_CA_VEH_DELTA
   add constraint FK_CCVD_CCCB foreign key (CA_CVRBL_KEY)
      references CS_CA_CVRBL_BASE (CA_CVRBL_KEY)
go

alter table CS_CA_VEH_DELTA
   add constraint FK_CCVD_CLBA foreign key (GARAGE_LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_CLAIMANT_BASE
   add constraint FK_CCBA_CCBA foreign key (CLAIM_KEY)
      references CS_CLAIM_BASE (CLAIM_KEY)
go

alter table CS_CLAIMANT_DELTA
   add constraint FK_CCDE_CCBA1 foreign key (CLMNT_KEY)
      references CS_CLAIMANT_BASE (CLMNT_KEY)
go

alter table CS_CLAIM_BASE
   add constraint FK_CCBA_CCSD foreign key (LAST_STATUS_ID)
      references CS_CLAIM_STATUS_DIM (CLAIM_STATUS_ID)
go

alter table CS_CLAIM_BASE
   add constraint FK_CCBA_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CLAIM_DELTA
   add constraint FK_CCDE_CCBA foreign key (CLAIM_KEY)
      references CS_CLAIM_BASE (CLAIM_KEY)
go

alter table CS_CLAIM_FEATURE_BASE
   add constraint FK_CCFB_CCBA foreign key (CLMNT_KEY)
      references CS_CLAIMANT_BASE (CLMNT_KEY)
go

alter table CS_CLAIM_FEATURE_BASE
   add constraint FK_CCFB_CCSD foreign key (LAST_STATUS_ID)
      references CS_CLAIM_STATUS_DIM (CLAIM_STATUS_ID)
go

alter table CS_CLAIM_FEATURE_DELTA
   add constraint FK_CCFD_CCFB foreign key (FEATURE_KEY)
      references CS_CLAIM_FEATURE_BASE (FEATURE_KEY)
go

alter table CS_CLAIM_FEATURE_STATUS_TRAN
   add constraint FK_CCFST_CCFB foreign key (FEATURE_KEY)
      references CS_CLAIM_FEATURE_BASE (FEATURE_KEY)
go

alter table CS_CLAIM_FEATURE_STATUS_TRAN
   add constraint FK_CCFST_CCSD foreign key (CLAIM_STATUS_ID)
      references CS_CLAIM_STATUS_DIM (CLAIM_STATUS_ID)
go

alter table CS_CLAIM_STATUS_TRAN
   add constraint FK_CCST_CCBA foreign key (CLAIM_KEY)
      references CS_CLAIM_BASE (CLAIM_KEY)
go

alter table CS_CLAIM_STATUS_TRAN
   add constraint FK_CCST_CCSD foreign key (CLAIM_STATUS_ID)
      references CS_CLAIM_STATUS_DIM (CLAIM_STATUS_ID)
go

alter table CS_COND_DELTA
   add constraint FK_CCDE_CCBA3 foreign key (COND_KEY)
      references CS_COND_BASE (COND_KEY)
go

alter table CS_COVG_DELTA
   add constraint FK_CCDE_CCBA2 foreign key (COVG_KEY)
      references CS_COVG_BASE (COVG_KEY)
go

alter table CS_CP_ADDL_INTRST_BASE
   add constraint FK_CCAIB_CCCB1 foreign key (CP_CVRBL_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_ADDL_INTRST_BASE
   add constraint FK_CCAIB_CPBA1 foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CP_ADDL_INTRST_BASE
   add constraint FK_CCAIB_CPPB1 foreign key (POL_PARTY_KEY)
      references CS_POL_PARTY_BASE (POL_PARTY_KEY)
go

alter table CS_CP_ADDL_INTRST_DELTA
   add constraint FK_CCAID_CCAIB1 foreign key (CP_ADDL_INTRST_KEY)
      references CS_CP_ADDL_INTRST_BASE (CP_ADDL_INTRST_KEY)
go

alter table CS_CP_BI_DELTA
   add constraint FK_CCBD_CCCB2 foreign key (CP_CVRBL_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_BI_DELTA
   add constraint FK_CCBD_CCCB3 foreign key (CP_BLDG_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_BLDG_DELTA
   add constraint FK_CCBD_CBBA foreign key (BLDG_KEY)
      references CS_BLDG_BASE (BLDG_KEY)
go

alter table CS_CP_BLDG_DELTA
   add constraint FK_CCBD_CCCB foreign key (CP_CVRBL_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_BLDG_DELTA
   add constraint FK_CCBD_CCCB4 foreign key (CP_LOC_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_BLDG_DELTA
   add constraint FK_CCBD_CCCCB foreign key (CP_CL_CD_KEY)
      references CS_CP_CL_CODE_BASE (CP_CL_CD_KEY)
go

alter table CS_CP_BLNKT_DELTA
   add constraint FK_CCBD_CCCB1 foreign key (CP_CVRBL_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_CL_CODE_DELTA
   add constraint FK_CCCCD_CCCCB foreign key (CP_CL_CD_KEY)
      references CS_CP_CL_CODE_BASE (CP_CL_CD_KEY)
go

alter table CS_CP_COND_BASE
   add constraint FK_CCCB_CCBA3 foreign key (COND_KEY)
      references CS_COND_BASE (COND_KEY)
go

alter table CS_CP_COND_BASE
   add constraint FK_CCCB_CCCB4 foreign key (CP_CVRBL_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_COND_BASE
   add constraint FK_CCCB_CPBA4 foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CP_COND_BASE
   add constraint FK_CCCB_CPLB4 foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_CP_COND_TERM_BASE
   add constraint FK_CCCTB_CCCB3 foreign key (CP_COND_KEY)
      references CS_CP_COND_BASE (CP_COND_KEY)
go

alter table CS_CP_COND_TERM_DELTA
   add constraint FK_CCCTD_CCCTB3 foreign key (CP_COND_TERM_KEY)
      references CS_CP_COND_TERM_BASE (CP_COND_TERM_KEY)
go

alter table CS_CP_COVG_BASE
   add constraint FK_CCCB_CCBA2 foreign key (COVG_KEY)
      references CS_COVG_BASE (COVG_KEY)
go

alter table CS_CP_COVG_BASE
   add constraint FK_CCCB_CCCB3 foreign key (CP_CVRBL_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_COVG_BASE
   add constraint FK_CCCB_CPBA5 foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CP_COVG_BASE
   add constraint FK_CCCB_CPLB3 foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_CP_COVG_TERM_BASE
   add constraint FK_CCCTB_CCCB2 foreign key (CP_COVG_KEY)
      references CS_CP_COVG_BASE (CP_COVG_KEY)
go

alter table CS_CP_COVG_TERM_DELTA
   add constraint FK_CCCTD_CCCTB2 foreign key (CP_COVG_TERM_KEY)
      references CS_CP_COVG_TERM_BASE (CP_COVG_TERM_KEY)
go

alter table CS_CP_CVRBL_BASE
   add constraint FK_CCCB_CPBA3 foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CP_CVRBL_BASE
   add constraint FK_CCCB_CPLB5 foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_CP_EXCL_BASE
   add constraint FK_CCEB_CCCB1 foreign key (CP_CVRBL_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_EXCL_BASE
   add constraint FK_CCEB_CEBA1 foreign key (EXCL_KEY)
      references CS_EXCL_BASE (EXCL_KEY)
go

alter table CS_CP_EXCL_BASE
   add constraint FK_CCEB_CPBA1 foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CP_EXCL_BASE
   add constraint FK_CCEB_CPLB1 foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_CP_EXCL_TERM_BASE
   add constraint FK_CCETB_CCEB1 foreign key (CP_EXCL_KEY)
      references CS_CP_EXCL_BASE (CP_EXCL_KEY)
go

alter table CS_CP_EXCL_TERM_DELTA
   add constraint FK_CCETD_CCETB1 foreign key (CP_EXCL_TERM_KEY)
      references CS_CP_EXCL_TERM_BASE (CP_EXCL_TERM_KEY)
go

alter table CS_CP_LOC_DELTA
   add constraint FK_CCLD_CCCB foreign key (CP_CVRBL_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_LOC_DELTA
   add constraint FK_CCLD_CLBA foreign key (LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_CP_MOD_BASE
   add constraint FK_CCMB_CCCB1 foreign key (CP_CVRBL_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_MOD_BASE
   add constraint FK_CCMB_CMBA1 foreign key (MOD_KEY)
      references CS_MOD_BASE (MOD_KEY)
go

alter table CS_CP_MOD_BASE
   add constraint FK_CCMB_CPBA1 foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CP_MOD_BASE
   add constraint FK_CCMB_CPLB1 foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_CP_MOD_DELTA
   add constraint FK_CCMD_CCMB1 foreign key (CP_MOD_KEY)
      references CS_CP_MOD_BASE (CP_MOD_KEY)
go

alter table CS_CP_MOD_RF_BASE
   add constraint FK_CCMRB_CCMB1 foreign key (CP_MOD_KEY)
      references CS_CP_MOD_BASE (CP_MOD_KEY)
go

alter table CS_CP_MOD_RF_BASE
   add constraint FK_CCMRB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CP_MOD_RF_DELTA
   add constraint FK_CCMRD_CCMRB1 foreign key (CP_MOD_RF_KEY)
      references CS_CP_MOD_RF_BASE (CP_MOD_RF_KEY)
go

alter table CS_CP_OCC_CL_DELTA
   add constraint FK_CCOCD_CCCB foreign key (CP_CVRBL_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_OCC_CL_DELTA
   add constraint FK_CCOCD_CCCB1 foreign key (CP_BLDG_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_OCC_CL_DELTA
   add constraint FK_CCOCD_CCCCB foreign key (CP_CL_CD_KEY)
      references CS_CP_CL_CODE_BASE (CP_CL_CD_KEY)
go

alter table CS_CP_POL_LINE_DELTA
   add constraint FK_CCPLD_CPLB1 foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_CP_PP_DELTA
   add constraint FK_CCPD_CCCB foreign key (CP_CVRBL_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_PP_DELTA
   add constraint FK_CCPD_CCCB1 foreign key (CP_OCC_CL_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_PREM_TRAN
   add constraint FK_CCPT_CABA2 foreign key (AGCY_KEY)
      references CS_AGENCY_BASE (AGCY_KEY)
go

alter table CS_CP_PREM_TRAN
   add constraint FK_CCPT_CABA3 foreign key (AGNT_KEY)
      references CS_AGENT_BASE (AGNT_KEY)
go

alter table CS_CP_PREM_TRAN
   add constraint FK_CCPT_CCBA1 foreign key (COVG_KEY)
      references CS_COVG_BASE (COVG_KEY)
go

alter table CS_CP_PREM_TRAN
   add constraint FK_CCPT_CCCB2 foreign key (CP_CVRBL_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_PREM_TRAN
   add constraint FK_CCPT_CCCB3 foreign key (CP_COVG_KEY)
      references CS_CP_COVG_BASE (CP_COVG_KEY)
go

alter table CS_CP_PREM_TRAN
   add constraint FK_CCPT_CLBA foreign key (CP_LOC_LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_CP_PREM_TRAN
   add constraint FK_CCPT_CPBA1 foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CP_PREM_TRAN
   add constraint FK_CCPT_CPLB1 foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_CP_SI_DELTA
   add constraint FK_CCSD_CCAIB1 foreign key (CP_ADDL_INTRST_KEY)
      references CS_CP_ADDL_INTRST_BASE (CP_ADDL_INTRST_KEY)
go

alter table CS_CP_SI_DELTA
   add constraint FK_CCSD_CCCB2 foreign key (CP_CVRBL_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_SI_DELTA
   add constraint FK_CCSD_CCCB3 foreign key (CP_COVG_KEY)
      references CS_CP_COVG_BASE (CP_COVG_KEY)
go

alter table CS_CP_SI_DELTA
   add constraint FK_CCSD_CCCB4 foreign key (CP_OWNING_CVRBL_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_SI_DELTA
   add constraint FK_CCSD_CLBA1 foreign key (LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_CP_SI_DELTA
   add constraint FK_CCSD_CPPB1 foreign key (POL_PARTY_KEY)
      references CS_POL_PARTY_BASE (POL_PARTY_KEY)
go

alter table CS_CP_SPCL_CL_BI_DELTA
   add constraint FK_CCSCBD_CCCB foreign key (CP_CVRBL_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_SPCL_CL_BI_DELTA
   add constraint FK_CCSCBD_CCCB1 foreign key (CP_SPCL_CL_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_SPCL_CL_DELTA
   add constraint FK_CCSCD_CCCB foreign key (CP_CVRBL_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_SPCL_CL_DELTA
   add constraint FK_CCSCD_CCCB1 foreign key (CP_LOC_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_TERR_CODE_BASE
   add constraint FK_CCTCB_CCCB foreign key (CP_LOC_KEY)
      references CS_CP_CVRBL_BASE (CP_CVRBL_KEY)
go

alter table CS_CP_TERR_CODE_BASE
   add constraint FK_CCTCB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_CP_TERR_CODE_DELTA
   add constraint FK_CCTCD_CCTCB foreign key (CP_TERR_CD_KEY)
      references CS_CP_TERR_CODE_BASE (CP_TERR_CD_KEY)
go

alter table CS_CUST_ACCT_DELTA
   add constraint FK_CCAD_CCAB foreign key (ACCT_KEY)
      references CS_CUST_ACCT_BASE (ACCT_KEY)
go

alter table CS_EARNED_PREM_TRAN
   add constraint FK_CEPT_CABA foreign key (AGCY_KEY)
      references CS_AGENCY_BASE (AGCY_KEY)
go

alter table CS_EARNED_PREM_TRAN
   add constraint FK_CEPT_CABA1 foreign key (AGNT_KEY)
      references CS_AGENT_BASE (AGNT_KEY)
go

alter table CS_EARNED_PREM_TRAN
   add constraint FK_CEPT_CMDI foreign key (ACCTG_PRD_ID)
      references CS_MONTH_DIM (MTH_ID)
go

alter table CS_EARNED_PREM_TRAN
   add constraint FK_CEPT_CRTD foreign key (REC_TYPE_KEY)
      references CS_RECORD_TYPE_DIM (REC_TYPE_KEY)
go

alter table CS_EXCL_DELTA
   add constraint FK_CEDE_CEBA foreign key (EXCL_KEY)
      references CS_EXCL_BASE (EXCL_KEY)
go

alter table CS_FORM_BASE
   add constraint FK_CFBA_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_FORM_DELTA
   add constraint FK_CFDE_CFBA foreign key (FORM_KEY)
      references CS_FORM_BASE (FORM_KEY)
go

alter table CS_GL_CL_CODE_DELTA
   add constraint FK_CGCCD_CGCCB foreign key (GL_CL_CD_KEY)
      references CS_GL_CL_CODE_BASE (GL_CL_CD_KEY)
go

alter table CS_GL_COND_BASE
   add constraint FK_CGCB_CCBA1 foreign key (COND_KEY)
      references CS_COND_BASE (COND_KEY)
go

alter table CS_GL_COND_BASE
   add constraint FK_CGCB_CGCB1 foreign key (GL_CVRBL_KEY)
      references CS_GL_CVRBL_BASE (GL_CVRBL_KEY)
go

alter table CS_GL_COND_BASE
   add constraint FK_CGCB_CPBA2 foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_GL_COND_BASE
   add constraint FK_CGCB_CPLB2 foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_GL_COND_TERM_BASE
   add constraint FK_CGCTB_CGCB1 foreign key (GL_COND_KEY)
      references CS_GL_COND_BASE (GL_COND_KEY)
go

alter table CS_GL_COND_TERM_DELTA
   add constraint FK_CGCTD_CGCTB1 foreign key (GL_COND_TERM_KEY)
      references CS_GL_COND_TERM_BASE (GL_COND_TERM_KEY)
go

alter table CS_GL_COVG_BASE
   add constraint FK_CGCB_CCBA foreign key (COVG_KEY)
      references CS_COVG_BASE (COVG_KEY)
go

alter table CS_GL_COVG_BASE
   add constraint FK_CGCB_CGCB foreign key (GL_CVRBL_KEY)
      references CS_GL_CVRBL_BASE (GL_CVRBL_KEY)
go

alter table CS_GL_COVG_BASE
   add constraint FK_CGCB_CPBA1 foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_GL_COVG_BASE
   add constraint FK_CGCB_CPLB1 foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_GL_COVG_PART_DELTA
   add constraint FK_CGCPD_CGCB foreign key (GL_CVRBL_KEY)
      references CS_GL_CVRBL_BASE (GL_CVRBL_KEY)
go

alter table CS_GL_COVG_TERM_BASE
   add constraint FK_CGCTB_CGCB foreign key (GL_COVG_KEY)
      references CS_GL_COVG_BASE (GL_COVG_KEY)
go

alter table CS_GL_COVG_TERM_DELTA
   add constraint FK_CGCTD_CGCTB foreign key (GL_COVG_TERM_KEY)
      references CS_GL_COVG_TERM_BASE (GL_COVG_TERM_KEY)
go

alter table CS_GL_CVRBL_BASE
   add constraint FK_CGCB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_GL_CVRBL_BASE
   add constraint FK_CGCB_CPLB foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_GL_EXCL_BASE
   add constraint FK_CGEB_CEBA foreign key (EXCL_KEY)
      references CS_EXCL_BASE (EXCL_KEY)
go

alter table CS_GL_EXCL_BASE
   add constraint FK_CGEB_CGCB foreign key (GL_CVRBL_KEY)
      references CS_GL_CVRBL_BASE (GL_CVRBL_KEY)
go

alter table CS_GL_EXCL_BASE
   add constraint FK_CGEB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_GL_EXCL_BASE
   add constraint FK_CGEB_CPLB foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_GL_EXCL_TERM_BASE
   add constraint FK_CGETB_CGEB foreign key (GL_EXCL_KEY)
      references CS_GL_EXCL_BASE (GL_EXCL_KEY)
go

alter table CS_GL_EXCL_TERM_DELTA
   add constraint FK_CGETD_CGETB foreign key (GL_EXCL_TERM_KEY)
      references CS_GL_EXCL_TERM_BASE (GL_EXCL_TERM_KEY)
go

alter table CS_GL_EXPSR_DELTA
   add constraint FK_CGED_CGCB foreign key (GL_CVRBL_KEY)
      references CS_GL_CVRBL_BASE (GL_CVRBL_KEY)
go

alter table CS_GL_EXPSR_DELTA
   add constraint FK_CGED_CGCB1 foreign key (GL_COVG_PART_KEY)
      references CS_GL_CVRBL_BASE (GL_CVRBL_KEY)
go

alter table CS_GL_EXPSR_DELTA
   add constraint FK_CGED_CGCCB foreign key (GL_CL_CD_KEY)
      references CS_GL_CL_CODE_BASE (GL_CL_CD_KEY)
go

alter table CS_GL_EXPSR_DELTA
   add constraint FK_CGED_CLBA foreign key (LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_GL_MOD_BASE
   add constraint FK_CGMB_CGCB foreign key (GL_CVRBL_KEY)
      references CS_GL_CVRBL_BASE (GL_CVRBL_KEY)
go

alter table CS_GL_MOD_BASE
   add constraint FK_CGMB_CMBA foreign key (MOD_KEY)
      references CS_MOD_BASE (MOD_KEY)
go

alter table CS_GL_MOD_BASE
   add constraint FK_CGMB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_GL_MOD_BASE
   add constraint FK_CGMB_CPLB foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_GL_MOD_DELTA
   add constraint FK_CGMD_CGMB foreign key (GL_MOD_KEY)
      references CS_GL_MOD_BASE (GL_MOD_KEY)
go

alter table CS_GL_MOD_RF_BASE
   add constraint FK_CGMRB_CGMB foreign key (GL_MOD_KEY)
      references CS_GL_MOD_BASE (GL_MOD_KEY)
go

alter table CS_GL_MOD_RF_BASE
   add constraint FK_CGMRB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_GL_MOD_RF_DELTA
   add constraint FK_CGMRD_CGMRB foreign key (GL_MOD_RF_KEY)
      references CS_GL_MOD_RF_BASE (GL_MOD_RF_KEY)
go

alter table CS_GL_POL_LINE_DELTA
   add constraint FK_CGPLD_CPLB foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_GL_PREM_TRAN
   add constraint FK_CGPT_CABA foreign key (AGCY_KEY)
      references CS_AGENCY_BASE (AGCY_KEY)
go

alter table CS_GL_PREM_TRAN
   add constraint FK_CGPT_CABA1 foreign key (AGNT_KEY)
      references CS_AGENT_BASE (AGNT_KEY)
go

alter table CS_GL_PREM_TRAN
   add constraint FK_CGPT_CCBA foreign key (COVG_KEY)
      references CS_COVG_BASE (COVG_KEY)
go

alter table CS_GL_PREM_TRAN
   add constraint FK_CGPT_CGCB foreign key (GL_CVRBL_KEY)
      references CS_GL_CVRBL_BASE (GL_CVRBL_KEY)
go

alter table CS_GL_PREM_TRAN
   add constraint FK_CGPT_CGCB1 foreign key (GL_COVG_KEY)
      references CS_GL_COVG_BASE (GL_COVG_KEY)
go

alter table CS_GL_PREM_TRAN
   add constraint FK_CGPT_CGCB2 foreign key (GL_COND_KEY)
      references CS_GL_COND_BASE (GL_COND_KEY)
go

alter table CS_GL_PREM_TRAN
   add constraint FK_CGPT_CGEB foreign key (GL_EXCL_KEY)
      references CS_GL_EXCL_BASE (GL_EXCL_KEY)
go

alter table CS_GL_PREM_TRAN
   add constraint FK_CGPT_CLBA foreign key (EXPSR_LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_GL_PREM_TRAN
   add constraint FK_CGPT_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_GL_PREM_TRAN
   add constraint FK_CGPT_CPLB foreign key (POL_LINE_KEY)
      references CS_POL_LINE_BASE (POL_LINE_KEY)
go

alter table CS_GL_PREM_TRAN
   add constraint FK_CGPT_CPPB foreign key (ADDL_INS_KEY)
      references CS_POL_PARTY_BASE (POL_PARTY_KEY)
go

alter table CS_GL_SI_COND_BASE
   add constraint FK_CGSCB_CGCB foreign key (GL_CVRBL_KEY)
      references CS_GL_CVRBL_BASE (GL_CVRBL_KEY)
go

alter table CS_GL_SI_COND_BASE
   add constraint FK_CGSCB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_GL_SI_COND_DELTA
   add constraint FK_CGSCD_CGCB foreign key (GL_COND_KEY)
      references CS_GL_COND_BASE (GL_COND_KEY)
go

alter table CS_GL_SI_COND_DELTA
   add constraint FK_CGSCD_CGSCB foreign key (GL_SI_COND_KEY)
      references CS_GL_SI_COND_BASE (GL_SI_COND_KEY)
go

alter table CS_GL_SI_COND_DELTA
   add constraint FK_CGSCD_CLBA foreign key (LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_GL_SI_COND_DELTA
   add constraint FK_CGSCD_CPPB foreign key (POL_PARTY_KEY)
      references CS_POL_PARTY_BASE (POL_PARTY_KEY)
go

alter table CS_GL_SI_DELTA
   add constraint FK_CGSD_CGCB foreign key (GL_CVRBL_KEY)
      references CS_GL_CVRBL_BASE (GL_CVRBL_KEY)
go

alter table CS_GL_SI_DELTA
   add constraint FK_CGSD_CGCB1 foreign key (GL_COVG_KEY)
      references CS_GL_COVG_BASE (GL_COVG_KEY)
go

alter table CS_GL_SI_DELTA
   add constraint FK_CGSD_CGCB2 foreign key (GL_OWNING_CVRBL_KEY)
      references CS_GL_CVRBL_BASE (GL_CVRBL_KEY)
go

alter table CS_GL_SI_DELTA
   add constraint FK_CGSD_CLBA foreign key (LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_GL_SI_DELTA
   add constraint FK_CGSD_CPPB foreign key (POL_PARTY_KEY)
      references CS_POL_PARTY_BASE (POL_PARTY_KEY)
go

alter table CS_GL_SI_EXCL_BASE
   add constraint FK_CGSEB_CGCB foreign key (GL_CVRBL_KEY)
      references CS_GL_CVRBL_BASE (GL_CVRBL_KEY)
go

alter table CS_GL_SI_EXCL_BASE
   add constraint FK_CGSEB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_GL_SI_EXCL_DELTA
   add constraint FK_CGSED_CGEB foreign key (GL_EXCL_KEY)
      references CS_GL_EXCL_BASE (GL_EXCL_KEY)
go

alter table CS_GL_SI_EXCL_DELTA
   add constraint FK_CGSED_CGSEB foreign key (GL_SI_EXCL_KEY)
      references CS_GL_SI_EXCL_BASE (GL_SI_EXCL_KEY)
go

alter table CS_GL_SI_EXCL_DELTA
   add constraint FK_CGSED_CLBA foreign key (LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_GL_SI_EXCL_DELTA
   add constraint FK_CGSED_CPPB foreign key (POL_PARTY_KEY)
      references CS_POL_PARTY_BASE (POL_PARTY_KEY)
go

alter table CS_HO_ADDL_INTRST_BASE
   add constraint FK_CHAIB_CHDB foreign key (HO_DWLNG_KEY)
      references CS_HO_DWLNG_BASE (HO_DWLNG_KEY)
go

alter table CS_HO_ADDL_INTRST_BASE
   add constraint FK_CHAIB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_HO_ADDL_INTRST_BASE
   add constraint FK_CHAIB_CPPB foreign key (POL_PARTY_KEY)
      references CS_POL_PARTY_BASE (POL_PARTY_KEY)
go

alter table CS_HO_ADDL_INTRST_DELTA
   add constraint FK_CHAID_CHAIB foreign key (HO_ADDL_INTRST_KEY)
      references CS_HO_ADDL_INTRST_BASE (HO_ADDL_INTRST_KEY)
go

alter table CS_HO_DWLNG_BASE
   add constraint FK_CHDB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_HO_DWLNG_COVG_BASE
   add constraint FK_CHDCB_CCBA foreign key (COVG_KEY)
      references CS_COVG_BASE (COVG_KEY)
go

alter table CS_HO_DWLNG_COVG_BASE
   add constraint FK_CHDCB_CHDB foreign key (HO_DWLNG_KEY)
      references CS_HO_DWLNG_BASE (HO_DWLNG_KEY)
go

alter table CS_HO_DWLNG_COVG_BASE
   add constraint FK_CHDCB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_HO_DWLNG_COVG_TERM_BASE
   add constraint FK_CHDCTB_CHDCB foreign key (HO_DWLNG_COVG_KEY)
      references CS_HO_DWLNG_COVG_BASE (HO_DWLNG_COVG_KEY)
go

alter table CS_HO_DWLNG_COVG_TERM_DELTA
   add constraint FK_CHDCTD_CHDCTB foreign key (HO_DWLNG_COVG_TERM_KEY)
      references CS_HO_DWLNG_COVG_TERM_BASE (HO_DWLNG_COVG_TERM_KEY)
go

alter table CS_HO_DWLNG_DELTA
   add constraint FK_CHDD_CHDB foreign key (HO_DWLNG_KEY)
      references CS_HO_DWLNG_BASE (HO_DWLNG_KEY)
go

alter table CS_HO_DWLNG_DELTA
   add constraint FK_CHDD_CHLB foreign key (HO_LOC_KEY)
      references CS_HO_LOC_BASE (HO_LOC_KEY)
go

alter table CS_HO_DWLNG_MOD_BASE
   add constraint FK_CHDMB_CHDB foreign key (HO_DWLNG_KEY)
      references CS_HO_DWLNG_BASE (HO_DWLNG_KEY)
go

alter table CS_HO_DWLNG_MOD_BASE
   add constraint FK_CHDMB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_HO_DWLNG_MOD_DELTA
   add constraint FK_CHDMD_CHDMB foreign key (HO_DWLNG_MOD_KEY)
      references CS_HO_DWLNG_MOD_BASE (HO_DWLNG_MOD_KEY)
go

alter table CS_HO_LINE_COVG_BASE
   add constraint FK_CHLCB_CCBA foreign key (COVG_KEY)
      references CS_COVG_BASE (COVG_KEY)
go

alter table CS_HO_LINE_COVG_BASE
   add constraint FK_CHLCB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_HO_LINE_COVG_TERM_BASE
   add constraint FK_CHLCTB_CHLCB foreign key (HO_LINE_COVG_KEY)
      references CS_HO_LINE_COVG_BASE (HO_LINE_COVG_KEY)
go

alter table CS_HO_LINE_COVG_TERM_DELTA
   add constraint FK_CHLCTD_CHLCTB foreign key (HO_LINE_COVG_TERM_KEY)
      references CS_HO_LINE_COVG_TERM_BASE (HO_LINE_COVG_TERM_KEY)
go

alter table CS_HO_LOC_BASE
   add constraint FK_CHLB_CLBA foreign key (LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_HO_LOC_BASE
   add constraint FK_CHLB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_HO_LOC_DELTA
   add constraint FK_CHLD_CHLB foreign key (HO_LOC_KEY)
      references CS_HO_LOC_BASE (HO_LOC_KEY)
go

alter table CS_HO_MOD_BASE
   add constraint FK_CHMB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_HO_MOD_DELTA
   add constraint FK_CHMD_CHMB foreign key (HO_MOD_KEY)
      references CS_HO_MOD_BASE (HO_MOD_KEY)
go

alter table CS_HO_OTHR_LOC_BASE
   add constraint FK_CHOLB_CHLCB foreign key (HO_LINE_COVG_KEY)
      references CS_HO_LINE_COVG_BASE (HO_LINE_COVG_KEY)
go

alter table CS_HO_OTHR_LOC_BASE
   add constraint FK_CHOLB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_HO_OTHR_LOC_DELTA
   add constraint FK_CHOLD_CHOLB foreign key (HO_OTHR_LOC_KEY)
      references CS_HO_OTHR_LOC_BASE (HO_OTHR_LOC_KEY)
go

alter table CS_HO_OTHR_LOC_DELTA
   add constraint FK_CHOLD_CLBA foreign key (LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_HO_PREM_TRAN
   add constraint FK_CHPT_CABA foreign key (AGCY_KEY)
      references CS_AGENCY_BASE (AGCY_KEY)
go

alter table CS_HO_PREM_TRAN
   add constraint FK_CHPT_CABA1 foreign key (AGNT_KEY)
      references CS_AGENT_BASE (AGNT_KEY)
go

alter table CS_HO_PREM_TRAN
   add constraint FK_CHPT_CCBA foreign key (COVG_KEY)
      references CS_COVG_BASE (COVG_KEY)
go

alter table CS_HO_PREM_TRAN
   add constraint FK_CHPT_CHDB foreign key (HO_DWLNG_KEY)
      references CS_HO_DWLNG_BASE (HO_DWLNG_KEY)
go

alter table CS_HO_PREM_TRAN
   add constraint FK_CHPT_CHDCB foreign key (HO_DWLNG_COVG_KEY)
      references CS_HO_DWLNG_COVG_BASE (HO_DWLNG_COVG_KEY)
go

alter table CS_HO_PREM_TRAN
   add constraint FK_CHPT_CHLCB foreign key (HO_LINE_COVG_KEY)
      references CS_HO_LINE_COVG_BASE (HO_LINE_COVG_KEY)
go

alter table CS_HO_PREM_TRAN
   add constraint FK_CHPT_CHSB foreign key (HO_SI_KEY)
      references CS_HO_SI_BASE (HO_SI_KEY)
go

alter table CS_HO_PREM_TRAN
   add constraint FK_CHPT_CLBA foreign key (HO_SI_LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_HO_PREM_TRAN
   add constraint FK_CHPT_CLBA1 foreign key (HO_OTHR_LOC_LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_HO_PREM_TRAN
   add constraint FK_CHPT_CLBA2 foreign key (HO_DWLNG_LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_HO_PREM_TRAN
   add constraint FK_CHPT_CMDI foreign key (ACCTG_PRD_ID)
      references CS_MONTH_DIM (MTH_ID)
go

alter table CS_HO_PREM_TRAN
   add constraint FK_CHPT_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_HO_SI_BASE
   add constraint FK_CHSB_CHDCB foreign key (HO_DWLNG_COVG_KEY)
      references CS_HO_DWLNG_COVG_BASE (HO_DWLNG_COVG_KEY)
go

alter table CS_HO_SI_BASE
   add constraint FK_CHSB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_HO_SI_DELTA
   add constraint FK_CHSD_CHSB foreign key (HO_SI_KEY)
      references CS_HO_SI_BASE (HO_SI_KEY)
go

alter table CS_HO_SI_DELTA
   add constraint FK_CHSD_CLBA foreign key (LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_LOC_BASE
   add constraint FK_CLBA_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_LOC_DELTA
   add constraint FK_CLDE_CABA foreign key (ADDR_KEY)
      references CS_ADDR_BASE (ADDR_KEY)
go

alter table CS_LOC_DELTA
   add constraint FK_CLDE_CLBA foreign key (LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_LOSS_TRAN
   add constraint FK_CLTR_CABA foreign key (AGNT_KEY)
      references CS_AGENT_BASE (AGNT_KEY)
go

alter table CS_LOSS_TRAN
   add constraint FK_CLTR_CABA1 foreign key (AGCY_KEY)
      references CS_AGENCY_BASE (AGCY_KEY)
go

alter table CS_LOSS_TRAN
   add constraint FK_CLTR_CCBA foreign key (CLAIM_KEY)
      references CS_CLAIM_BASE (CLAIM_KEY)
go

alter table CS_LOSS_TRAN
   add constraint FK_CLTR_CCBA1 foreign key (CLMNT_KEY)
      references CS_CLAIMANT_BASE (CLMNT_KEY)
go

alter table CS_LOSS_TRAN
   add constraint FK_CLTR_CCFB foreign key (FEATURE_KEY)
      references CS_CLAIM_FEATURE_BASE (FEATURE_KEY)
go

alter table CS_LOSS_TRAN
   add constraint FK_CLTR_CLBA foreign key (LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_LOSS_TRAN
   add constraint FK_CLTR_CMDI foreign key (ACCTG_PRD_ID)
      references CS_MONTH_DIM (MTH_ID)
go

alter table CS_LOSS_TRAN
   add constraint FK_CLTR_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_LOSS_TRAN
   add constraint FK_CLTR_CRTD foreign key (REC_TYPE_KEY)
      references CS_RECORD_TYPE_DIM (REC_TYPE_KEY)
go

alter table CS_LOSS_TRAN
   add constraint FK_CLTR_CVBA foreign key (VNDR_KEY)
      references CS_VENDOR_BASE (VNDR_KEY)
go

alter table CS_MOD_DELTA
   add constraint FK_CMDE_CMBA foreign key (MOD_KEY)
      references CS_MOD_BASE (MOD_KEY)
go

alter table CS_NAMED_INSURED_BASE
   add constraint FK_CNIB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_NAMED_INSURED_DELTA
   add constraint FK_CNID_CNIB foreign key (NAMED_INSURED_KEY)
      references CS_NAMED_INSURED_BASE (NAMED_INSURED_KEY)
go

alter table CS_PARTY_DELTA
   add constraint FK_CPDE_CABA2 foreign key (PRIM_ADDR_KEY)
      references CS_ADDR_BASE (ADDR_KEY)
go

alter table CS_PARTY_DELTA
   add constraint FK_CPDE_CPBA1 foreign key (PARTY_KEY)
      references CS_PARTY_BASE (PARTY_KEY)
go

alter table CS_PA_ADDL_INTRST_BASE
   add constraint FK_CPAIB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_PA_ADDL_INTRST_BASE
   add constraint FK_CPAIB_CPPB foreign key (POL_PARTY_KEY)
      references CS_POL_PARTY_BASE (POL_PARTY_KEY)
go

alter table CS_PA_ADDL_INTRST_BASE
   add constraint FK_CPAIB_CPVB foreign key (VEH_KEY)
      references CS_PA_VEH_BASE (VEH_KEY)
go

alter table CS_PA_ADDL_INTRST_DELTA
   add constraint FK_CPAID_CPAIB foreign key (PA_ADDL_INTRST_KEY)
      references CS_PA_ADDL_INTRST_BASE (PA_ADDL_INTRST_KEY)
go

alter table CS_PA_DRVR_BASE
   add constraint FK_CPDB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_PA_DRVR_BASE
   add constraint FK_CPDB_CPPB foreign key (POL_PARTY_KEY)
      references CS_POL_PARTY_BASE (POL_PARTY_KEY)
go

alter table CS_PA_DRVR_DELTA
   add constraint FK_CPDD_CPDB foreign key (PA_DRVR_KEY)
      references CS_PA_DRVR_BASE (PA_DRVR_KEY)
go

alter table CS_PA_LINE_COVG_BASE
   add constraint FK_CPLCB_CCBA foreign key (COVG_KEY)
      references CS_COVG_BASE (COVG_KEY)
go

alter table CS_PA_LINE_COVG_BASE
   add constraint FK_CPLCB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_PA_LINE_COVG_TERM_BASE
   add constraint FK_CPLCTB_CPLCB foreign key (PA_LINE_COVG_KEY)
      references CS_PA_LINE_COVG_BASE (PA_LINE_COVG_KEY)
go

alter table CS_PA_LINE_COVG_TERM_DELTA
   add constraint FK_CPLCTD_CPLCTB foreign key (PA_LINE_COVG_TERM_KEY)
      references CS_PA_LINE_COVG_TERM_BASE (PA_LINE_COVG_TERM_KEY)
go

alter table CS_PA_MOD_BASE
   add constraint FK_CPMB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_PA_MOD_DELTA
   add constraint FK_CPMD_CPMB foreign key (PA_MOD_KEY)
      references CS_PA_MOD_BASE (PA_MOD_KEY)
go

alter table CS_PA_PREM_TRAN
   add constraint FK_CPPT_CABA foreign key (AGCY_KEY)
      references CS_AGENCY_BASE (AGCY_KEY)
go

alter table CS_PA_PREM_TRAN
   add constraint FK_CPPT_CABA1 foreign key (AGNT_KEY)
      references CS_AGENT_BASE (AGNT_KEY)
go

alter table CS_PA_PREM_TRAN
   add constraint FK_CPPT_CCBA foreign key (COVG_KEY)
      references CS_COVG_BASE (COVG_KEY)
go

alter table CS_PA_PREM_TRAN
   add constraint FK_CPPT_CLBA foreign key (VEH_GARAGE_LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_PA_PREM_TRAN
   add constraint FK_CPPT_CMDI foreign key (ACCTG_PRD_ID)
      references CS_MONTH_DIM (MTH_ID)
go

alter table CS_PA_PREM_TRAN
   add constraint FK_CPPT_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_PA_PREM_TRAN
   add constraint FK_CPPT_CPLCB foreign key (PA_LINE_COVG_KEY)
      references CS_PA_LINE_COVG_BASE (PA_LINE_COVG_KEY)
go

alter table CS_PA_PREM_TRAN
   add constraint FK_CPPT_CPVB foreign key (VEH_KEY)
      references CS_PA_VEH_BASE (VEH_KEY)
go

alter table CS_PA_PREM_TRAN
   add constraint FK_CPPT_CPVCB foreign key (PA_VEH_COVG_KEY)
      references CS_PA_VEH_COVG_BASE (PA_VEH_COVG_KEY)
go

alter table CS_PA_PREM_TRAN
   add constraint FK_CPPT_CRTD foreign key (REC_TYPE_KEY)
      references CS_RECORD_TYPE_DIM (REC_TYPE_KEY)
go

alter table CS_PA_VEH_BASE
   add constraint FK_CPVB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_PA_VEH_COVG_BASE
   add constraint FK_CPVCB_CCBA foreign key (COVG_KEY)
      references CS_COVG_BASE (COVG_KEY)
go

alter table CS_PA_VEH_COVG_BASE
   add constraint FK_CPVCB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_PA_VEH_COVG_BASE
   add constraint FK_CPVCB_CPVB foreign key (VEH_KEY)
      references CS_PA_VEH_BASE (VEH_KEY)
go

alter table CS_PA_VEH_COVG_TERM_BASE
   add constraint FK_CPVCTB_CPVCB foreign key (PA_VEH_COVG_KEY)
      references CS_PA_VEH_COVG_BASE (PA_VEH_COVG_KEY)
go

alter table CS_PA_VEH_COVG_TERM_DELTA
   add constraint FK_CPVCTD_CPVCTB foreign key (PA_VEH_COVG_TERM_KEY)
      references CS_PA_VEH_COVG_TERM_BASE (PA_VEH_COVG_TERM_KEY)
go

alter table CS_PA_VEH_DELTA
   add constraint FK_CPVD_CLBA foreign key (GARAGE_LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_PA_VEH_DELTA
   add constraint FK_CPVD_CPVB foreign key (VEH_KEY)
      references CS_PA_VEH_BASE (VEH_KEY)
go

alter table CS_PA_VEH_DRVR_BASE
   add constraint FK_CPVDB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_PA_VEH_DRVR_BASE
   add constraint FK_CPVDB_CPDB foreign key (PA_DRVR_KEY)
      references CS_PA_DRVR_BASE (PA_DRVR_KEY)
go

alter table CS_PA_VEH_DRVR_BASE
   add constraint FK_CPVDB_CPVB foreign key (VEH_KEY)
      references CS_PA_VEH_BASE (VEH_KEY)
go

alter table CS_PA_VEH_DRVR_DELTA
   add constraint FK_CPVDD_CPVDB foreign key (PA_VEH_DRVR_KEY)
      references CS_PA_VEH_DRVR_BASE (PA_VEH_DRVR_KEY)
go

alter table CS_PA_VEH_MOD_BASE
   add constraint FK_CPVMB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_PA_VEH_MOD_BASE
   add constraint FK_CPVMB_CPVB foreign key (VEH_KEY)
      references CS_PA_VEH_BASE (VEH_KEY)
go

alter table CS_PA_VEH_MOD_DELTA
   add constraint FK_CPVMD_CPVMB foreign key (PA_VEH_MOD_KEY)
      references CS_PA_VEH_MOD_BASE (PA_VEH_MOD_KEY)
go

alter table CS_POLICY_BASE
   add constraint FK_CPBA_CCAB foreign key (ACCT_KEY)
      references CS_CUST_ACCT_BASE (ACCT_KEY)
go

alter table CS_POLICY_BASE
   add constraint FK_CPBA_CPSD foreign key (LAST_STATUS_ID)
      references CS_POLICY_STATUS_DIM (POL_STATUS_ID)
go

alter table CS_POLICY_DELTA
   add constraint FK_CPDE_CABA foreign key (AGNT_KEY)
      references CS_AGENT_BASE (AGNT_KEY)
go

alter table CS_POLICY_DELTA
   add constraint FK_CPDE_CABA1 foreign key (AGCY_KEY)
      references CS_AGENCY_BASE (AGCY_KEY)
go

alter table CS_POLICY_DELTA
   add constraint FK_CPDE_CNIB foreign key (PRIM_NAMED_INSURED_KEY)
      references CS_NAMED_INSURED_BASE (NAMED_INSURED_KEY)
go

alter table CS_POLICY_DELTA
   add constraint FK_CPDE_CODI foreign key (ORG_ID)
      references CS_ORGANIZATION_DIM (ORG_ID)
go

alter table CS_POLICY_DELTA
   add constraint FK_CPDE_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_POLICY_DELTA
   add constraint FK_CPDE_CPDI foreign key (PRODUCT_ID)
      references CS_PRODUCT_DIM (PRODUCT_ID)
go

alter table CS_POLICY_STATUS_TRAN
   add constraint FK_CPST_CMDI foreign key (ACCTG_PRD_ID)
      references CS_MONTH_DIM (MTH_ID)
go

alter table CS_POLICY_STATUS_TRAN
   add constraint FK_CPST_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_POLICY_STATUS_TRAN
   add constraint FK_CPST_CPSD foreign key (POL_STATUS_ID)
      references CS_POLICY_STATUS_DIM (POL_STATUS_ID)
go

alter table CS_POL_LINE_BASE
   add constraint FK_CPLB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_POL_PARTY_BASE
   add constraint FK_CPPB_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_POL_PARTY_DELTA
   add constraint FK_CPPD_CPBA foreign key (PARTY_KEY)
      references CS_PARTY_BASE (PARTY_KEY)
go

alter table CS_POL_PARTY_DELTA
   add constraint FK_CPPD_CPPB foreign key (POL_PARTY_KEY)
      references CS_POL_PARTY_BASE (POL_PARTY_KEY)
go

alter table CS_PREM_TRAN
   add constraint FK_CPTR_CABA foreign key (AGNT_KEY)
      references CS_AGENT_BASE (AGNT_KEY)
go

alter table CS_PREM_TRAN
   add constraint FK_CPTR_CABA1 foreign key (AGCY_KEY)
      references CS_AGENCY_BASE (AGCY_KEY)
go

alter table CS_PREM_TRAN
   add constraint FK_CPTR_CLBA foreign key (LOC_KEY)
      references CS_LOC_BASE (LOC_KEY)
go

alter table CS_PREM_TRAN
   add constraint FK_CPTR_CMDI foreign key (ACCTG_PRD_ID)
      references CS_MONTH_DIM (MTH_ID)
go

alter table CS_PREM_TRAN
   add constraint FK_CPTR_CPBA foreign key (POL_KEY)
      references CS_POLICY_BASE (POL_KEY)
go

alter table CS_PREM_TRAN
   add constraint FK_CPTR_CRTD foreign key (REC_TYPE_KEY)
      references CS_RECORD_TYPE_DIM (REC_TYPE_KEY)
go

alter table CS_VENDOR_DELTA
   add constraint FK_CVDE_CVBA foreign key (VNDR_KEY)
      references CS_VENDOR_BASE (VNDR_KEY)
go

alter table REF_COLUMN_ASNMNT
   add constraint FK_RCAS_RETY foreign key (REF_ENTITY_TYPE_CD)
      references REF_ENTITY_TYPE (REF_ENTITY_TYPE_CD)
go

alter table REF_CONF_STRATEGY
   add constraint FK_RCST_RCTY foreign key (REF_CONF_TYPE_CD)
      references REF_CONF_TYPE (REF_CONF_TYPE_CD)
go

alter table REF_CONF_STRATEGY
   add constraint FK_RCST_REST foreign key (REF_STRATEGY_ID)
      references REF_STRATEGY (REF_STRATEGY_ID)
go

alter table REF_MSTR
   add constraint FK_REMS_RETY foreign key (REF_ENTITY_TYPE_CD)
      references REF_ENTITY_TYPE (REF_ENTITY_TYPE_CD)
go

alter table REF_MSTR
   add constraint FK_REMS_RSSY foreign key (REF_SOURCE_SYSTEM)
      references REF_SOURCE_SYSTEM (REF_SOURCE_SYSTEM)
go

alter table REF_MSTR_CONF
   add constraint FK_RMCO_REMS foreign key (SRCE_REF_MASTER_ID)
      references REF_MSTR (REF_MASTER_ID)
go

alter table REF_MSTR_CONF
   add constraint FK_RMCO_REMS1 foreign key (TARGET_REF_MASTER_ID)
      references REF_MSTR (REF_MASTER_ID)
go

alter table REF_MSTR_CONF
   add constraint FK_RMCO_REST foreign key (REF_STRATEGY_ID)
      references REF_STRATEGY (REF_STRATEGY_ID)
go

alter table REF_STRATEGY
   add constraint FK_REST_RETY foreign key (REF_ENTITY_TYPE_CD)
      references REF_ENTITY_TYPE (REF_ENTITY_TYPE_CD)
go

alter table REF_STRATEGY
   add constraint FK_REST_RSSY foreign key (TARGET_REF_SOURCE_SYSTEM)
      references REF_SOURCE_SYSTEM (REF_SOURCE_SYSTEM)
go

alter table REF_STRATEGY
   add constraint FK_REST_RSSY1 foreign key (SRCE_REF_SOURCE_SYSTEM)
      references REF_SOURCE_SYSTEM (REF_SOURCE_SYSTEM)
go

insert into SYSTEM_INFO values ('Product', '9.0.0');
--FULL
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_ADDR_DELTA','ADDR_KEY','CS_ADDR_BASE','ADDR_KEY',NULL, 'FK_CADE_CABA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_AUDIT_INFO_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CAIB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_AUDIT_INFO_DELTA','AUDIT_INFO_KEY','CS_AUDIT_INFO_BASE','AUDIT_INFO_KEY',NULL, 'FK_CAID_CAIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_AGENT_DELTA','AGNT_KEY','CS_AGENT_BASE','AGNT_KEY',NULL, 'FK_CADE_CABA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_VENDOR_DELTA','VNDR_KEY','CS_VENDOR_BASE','VNDR_KEY',NULL, 'FK_CVDE_CVBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CUST_ACCT_DELTA','ACCT_KEY','CS_CUST_ACCT_BASE','ACCT_KEY',NULL, 'FK_CCAD_CCAB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CLAIM_FEATURE_STATUS_TRAN','FEATURE_KEY','CS_CLAIM_FEATURE_BASE','FEATURE_KEY',NULL, 'FK_CCFST_CCFB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_AGENCY_DELTA','AGCY_KEY','CS_AGENCY_BASE','AGCY_KEY',NULL, 'FK_CADE_CABA2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_AGENT_BASE','AGCY_KEY','CS_AGENCY_BASE','AGCY_KEY',NULL, 'FK_CABA_CABA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_EARNED_PREM_TRAN','REC_TYPE_KEY','CS_RECORD_TYPE_DIM','REC_TYPE_KEY',NULL, 'FK_CEPT_CRTD', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_NAMED_INSURED_DELTA','NAMED_INSURED_KEY','CS_NAMED_INSURED_BASE','NAMED_INSURED_KEY',NULL, 'FK_CNID_CNIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_POLICY_STATUS_TRAN','POL_STATUS_ID','CS_POLICY_STATUS_DIM','POL_STATUS_ID',NULL, 'FK_CPST_CPSD', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_POLICY_STATUS_TRAN','ACCTG_PRD_ID','CS_MONTH_DIM','MTH_ID',NULL, 'FK_CPST_CMDI', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CLAIM_STATUS_TRAN','CLAIM_STATUS_ID','CS_CLAIM_STATUS_DIM','CLAIM_STATUS_ID',NULL, 'FK_CCST_CCSD', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CLAIM_FEATURE_STATUS_TRAN','CLAIM_STATUS_ID','CS_CLAIM_STATUS_DIM','CLAIM_STATUS_ID',NULL, 'FK_CCFST_CCSD', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CLAIM_FEATURE_BASE','LAST_STATUS_ID','CS_CLAIM_STATUS_DIM','CLAIM_STATUS_ID',NULL, 'FK_CCFB_CCSD', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_EARNED_PREM_TRAN','ACCTG_PRD_ID','CS_MONTH_DIM','MTH_ID',NULL, 'FK_CEPT_CMDI', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_EARNED_PREM_TRAN','AGCY_KEY','CS_AGENCY_BASE','AGCY_KEY',NULL, 'FK_CEPT_CABA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_EARNED_PREM_TRAN','AGNT_KEY','CS_AGENT_BASE','AGNT_KEY',NULL, 'FK_CEPT_CABA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CLAIM_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCBA_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CLAIM_DELTA','CLAIM_KEY','CS_CLAIM_BASE','CLAIM_KEY',NULL, 'FK_CCDE_CCBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_BLDG_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CBBA_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CLAIMANT_BASE','CLAIM_KEY','CS_CLAIM_BASE','CLAIM_KEY',NULL, 'FK_CCBA_CCBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CLAIMANT_DELTA','CLMNT_KEY','CS_CLAIMANT_BASE','CLMNT_KEY',NULL, 'FK_CCDE_CCBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_LOSS_TRAN','REC_TYPE_KEY','CS_RECORD_TYPE_DIM','REC_TYPE_KEY',NULL, 'FK_CLTR_CRTD', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_LOSS_TRAN','VNDR_KEY','CS_VENDOR_BASE','VNDR_KEY',NULL, 'FK_CLTR_CVBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CLAIM_STATUS_TRAN','CLAIM_KEY','CS_CLAIM_BASE','CLAIM_KEY',NULL, 'FK_CCST_CCBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_LOC_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CLBA_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_LOC_DELTA','LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CLDE_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_POLICY_BASE','ACCT_KEY','CS_CUST_ACCT_BASE','ACCT_KEY',NULL, 'FK_CPBA_CCAB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_POLICY_DELTA','AGNT_KEY','CS_AGENT_BASE','AGNT_KEY',NULL, 'FK_CPDE_CABA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_POLICY_DELTA','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CPDE_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PREM_TRAN','REC_TYPE_KEY','CS_RECORD_TYPE_DIM','REC_TYPE_KEY',NULL, 'FK_CPTR_CRTD', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_LINE_COVG_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CPLCB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_DRVR_DELTA','PA_DRVR_KEY','CS_PA_DRVR_BASE','PA_DRVR_KEY',NULL, 'FK_CPDD_CPDB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_POLICY_DELTA','AGCY_KEY','CS_AGENCY_BASE','AGCY_KEY',NULL, 'FK_CPDE_CABA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PARTY_DELTA','PARTY_KEY','CS_PARTY_BASE','PARTY_KEY',NULL, 'FK_CPDE_CPBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_POLICY_DELTA','PRODUCT_ID','CS_PRODUCT_DIM','PRODUCT_ID',NULL, 'FK_CPDE_CPDI', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_POLICY_DELTA','ORG_ID','CS_ORGANIZATION_DIM','ORG_ID',NULL, 'FK_CPDE_CODI', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_MOD_DELTA','PA_MOD_KEY','CS_PA_MOD_BASE','PA_MOD_KEY',NULL, 'FK_CPMD_CPMB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_PREM_TRAN','AGCY_KEY','CS_AGENCY_BASE','AGCY_KEY',NULL, 'FK_CPPT_CABA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_PREM_TRAN','AGNT_KEY','CS_AGENT_BASE','AGNT_KEY',NULL, 'FK_CPPT_CABA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_PREM_TRAN','COVG_KEY','CS_COVG_BASE','COVG_KEY',NULL, 'FK_CPPT_CCBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_PREM_TRAN','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CPPT_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_PREM_TRAN','VEH_KEY','CS_PA_VEH_BASE','VEH_KEY',NULL, 'FK_CPPT_CPVB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_LOSS_TRAN','FEATURE_KEY','CS_CLAIM_FEATURE_BASE','FEATURE_KEY',NULL, 'FK_CLTR_CCFB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_VEH_DELTA','VEH_KEY','CS_PA_VEH_BASE','VEH_KEY',NULL, 'FK_CPVD_CPVB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CLAIM_FEATURE_DELTA','FEATURE_KEY','CS_CLAIM_FEATURE_BASE','FEATURE_KEY',NULL, 'FK_CCFD_CCFB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CLAIM_FEATURE_BASE','CLMNT_KEY','CS_CLAIMANT_BASE','CLMNT_KEY',NULL, 'FK_CCFB_CCBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PREM_TRAN','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CPTR_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PREM_TRAN','LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CPTR_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PREM_TRAN','AGNT_KEY','CS_AGENT_BASE','AGNT_KEY',NULL, 'FK_CPTR_CABA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PREM_TRAN','AGCY_KEY','CS_AGENCY_BASE','AGCY_KEY',NULL, 'FK_CPTR_CABA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_LOSS_TRAN','AGNT_KEY','CS_AGENT_BASE','AGNT_KEY',NULL, 'FK_CLTR_CABA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_LOSS_TRAN','AGCY_KEY','CS_AGENCY_BASE','AGCY_KEY',NULL, 'FK_CLTR_CABA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_NAMED_INSURED_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CNIB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_VEH_DRVR_DELTA','PA_VEH_DRVR_KEY','CS_PA_VEH_DRVR_BASE','PA_VEH_DRVR_KEY',NULL, 'FK_CPVDD_CPVDB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_VEH_DRVR_BASE','VEH_KEY','CS_PA_VEH_BASE','VEH_KEY',NULL, 'FK_CPVDB_CPVB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_VEH_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CPVB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_ADDL_INTRST_DELTA','PA_ADDL_INTRST_KEY','CS_PA_ADDL_INTRST_BASE','PA_ADDL_INTRST_KEY',NULL, 'FK_CPAID_CPAIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_ADDL_INTRST_BASE','POL_PARTY_KEY','CS_POL_PARTY_BASE','POL_PARTY_KEY',NULL, 'FK_CPAIB_CPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_ADDL_INTRST_BASE','VEH_KEY','CS_PA_VEH_BASE','VEH_KEY',NULL, 'FK_CPAIB_CPVB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_VEH_COVG_BASE','VEH_KEY','CS_PA_VEH_BASE','VEH_KEY',NULL, 'FK_CPVCB_CPVB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_VEH_MOD_DELTA','PA_VEH_MOD_KEY','CS_PA_VEH_MOD_BASE','PA_VEH_MOD_KEY',NULL, 'FK_CPVMD_CPVMB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_BLDG_DELTA','BLDG_KEY','CS_BLDG_BASE','BLDG_KEY',NULL, 'FK_CBDE_CBBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_BLDG_DELTA','LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CBDE_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_VEH_MOD_BASE','VEH_KEY','CS_PA_VEH_BASE','VEH_KEY',NULL, 'FK_CPVMB_CPVB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_POLICY_STATUS_TRAN','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CPST_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_POLICY_BASE','LAST_STATUS_ID','CS_POLICY_STATUS_DIM','POL_STATUS_ID',NULL, 'FK_CPBA_CPSD', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CLAIM_BASE','LAST_STATUS_ID','CS_CLAIM_STATUS_DIM','CLAIM_STATUS_ID',NULL, 'FK_CCBA_CCSD', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_LOSS_TRAN','ACCTG_PRD_ID','CS_MONTH_DIM','MTH_ID',NULL, 'FK_CLTR_CMDI', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PREM_TRAN','ACCTG_PRD_ID','CS_MONTH_DIM','MTH_ID',NULL, 'FK_CPTR_CMDI', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_POLICY_DELTA','PRIM_NAMED_INSURED_KEY','CS_NAMED_INSURED_BASE','NAMED_INSURED_KEY',NULL, 'FK_CPDE_CNIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_DISTR_ITEM','CS_BILL_DISTR_ITEM_DELTA','ACTIVE_DISTR_KEY','CS_BILL_DISTR_BASE','DISTR_KEY',NULL, 'FK_CBDID_CBDB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_LOSS_TRAN','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CLTR_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_LOSS_TRAN','CLAIM_KEY','CS_CLAIM_BASE','CLAIM_KEY',NULL, 'FK_CLTR_CCBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_LOSS_TRAN','CLMNT_KEY','CS_CLAIMANT_BASE','CLMNT_KEY',NULL, 'FK_CLTR_CCBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_LOSS_TRAN','LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CLTR_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_PREM_TRAN','ACCTG_PRD_ID','CS_MONTH_DIM','MTH_ID',NULL, 'FK_CPPT_CMDI', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_POL_PARTY_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CPPB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_VEH_DRVR_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CPVDB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_VEH_DRVR_BASE','PA_DRVR_KEY','CS_PA_DRVR_BASE','PA_DRVR_KEY',NULL, 'FK_CPVDB_CPDB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_POL_PARTY_DELTA','POL_PARTY_KEY','CS_POL_PARTY_BASE','POL_PARTY_KEY',NULL, 'FK_CPPD_CPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_VEH_MOD_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CPVMB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_PREM_TRAN','REC_TYPE_KEY','CS_RECORD_TYPE_DIM','REC_TYPE_KEY',NULL, 'FK_CPPT_CRTD', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_VEH_COVG_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CPVCB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_DWLNG_COVG_BASE','COVG_KEY','CS_COVG_BASE','COVG_KEY',NULL, 'FK_CHDCB_CCBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_DWLNG_COVG_TERM_DELTA','HO_DWLNG_COVG_TERM_KEY','CS_HO_DWLNG_COVG_TERM_BASE','HO_DWLNG_COVG_TERM_KEY',NULL, 'FK_CHDCTD_CHDCTB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_PREM_TRAN','COVG_KEY','CS_COVG_BASE','COVG_KEY',NULL, 'FK_CHPT_CCBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_DWLNG_COVG_BASE','HO_DWLNG_KEY','CS_HO_DWLNG_BASE','HO_DWLNG_KEY',NULL, 'FK_CHDCB_CHDB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_LINE_COVG_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CHLCB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_LINE_COVG_TERM_BASE','HO_LINE_COVG_KEY','CS_HO_LINE_COVG_BASE','HO_LINE_COVG_KEY',NULL, 'FK_CHLCTB_CHLCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_PREM_TRAN','AGCY_KEY','CS_AGENCY_BASE','AGCY_KEY',NULL, 'FK_CHPT_CABA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_PREM_TRAN','AGNT_KEY','CS_AGENT_BASE','AGNT_KEY',NULL, 'FK_CHPT_CABA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_PREM_TRAN','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CHPT_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_DWLNG_DELTA','HO_DWLNG_KEY','CS_HO_DWLNG_BASE','HO_DWLNG_KEY',NULL, 'FK_CHDD_CHDB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_LOC_DELTA','HO_LOC_KEY','CS_HO_LOC_BASE','HO_LOC_KEY',NULL, 'FK_CHLD_CHLB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_SI_DELTA','HO_SI_KEY','CS_HO_SI_BASE','HO_SI_KEY',NULL, 'FK_CHSD_CHSB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_SI_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CHSB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_PREM_TRAN','HO_SI_LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CHPT_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_DWLNG_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CHDB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_OTHR_LOC_DELTA','HO_OTHR_LOC_KEY','CS_HO_OTHR_LOC_BASE','HO_OTHR_LOC_KEY',NULL, 'FK_CHOLD_CHOLB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_PREM_TRAN','HO_DWLNG_KEY','CS_HO_DWLNG_BASE','HO_DWLNG_KEY',NULL, 'FK_CHPT_CHDB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_PREM_TRAN','HO_OTHR_LOC_LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CHPT_CLBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_COVG_DELTA','COVG_KEY','CS_COVG_BASE','COVG_KEY',NULL, 'FK_CCDE_CCBA2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_DWLNG_DELTA','HO_LOC_KEY','CS_HO_LOC_BASE','HO_LOC_KEY',NULL, 'FK_CHDD_CHLB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_MOD_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CHMB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_DWLNG_MOD_DELTA','HO_DWLNG_MOD_KEY','CS_HO_DWLNG_MOD_BASE','HO_DWLNG_MOD_KEY',NULL, 'FK_CHDMD_CHDMB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_DWLNG_MOD_BASE','HO_DWLNG_KEY','CS_HO_DWLNG_BASE','HO_DWLNG_KEY',NULL, 'FK_CHDMB_CHDB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_LOC_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CHLB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_LOC_BASE','LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CHLB_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_ADDL_INTRST_BASE','HO_DWLNG_KEY','CS_HO_DWLNG_BASE','HO_DWLNG_KEY',NULL, 'FK_CHAIB_CHDB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_ADDL_INTRST_BASE','POL_PARTY_KEY','CS_POL_PARTY_BASE','POL_PARTY_KEY',NULL, 'FK_CHAIB_CPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_PREM_TRAN','ACCTG_PRD_ID','CS_MONTH_DIM','MTH_ID',NULL, 'FK_CHPT_CMDI', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PARTY_DELTA','PRIM_ADDR_KEY','CS_ADDR_BASE','ADDR_KEY',NULL, 'FK_CPDE_CABA2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_POL_PARTY_DELTA','PARTY_KEY','CS_PARTY_BASE','PARTY_KEY',NULL, 'FK_CPPD_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_LOC_DELTA','ADDR_KEY','CS_ADDR_BASE','ADDR_KEY',NULL, 'FK_CLDE_CABA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_VEH_DELTA','GARAGE_LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CPVD_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_PREM_TRAN','HO_SI_KEY','CS_HO_SI_BASE','HO_SI_KEY',NULL, 'FK_CHPT_CHSB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_SI_DELTA','LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CHSD_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_MOD_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CPMB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_MOD_DELTA','HO_MOD_KEY','CS_HO_MOD_BASE','HO_MOD_KEY',NULL, 'FK_CHMD_CHMB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_LINE_COVG_TERM_DELTA','PA_LINE_COVG_TERM_KEY','CS_PA_LINE_COVG_TERM_BASE','PA_LINE_COVG_TERM_KEY',NULL, 'FK_CPLCTD_CPLCTB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_VEH_COVG_TERM_DELTA','PA_VEH_COVG_TERM_KEY','CS_PA_VEH_COVG_TERM_BASE','PA_VEH_COVG_TERM_KEY',NULL, 'FK_CPVCTD_CPVCTB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_LINE_COVG_BASE','COVG_KEY','CS_COVG_BASE','COVG_KEY',NULL, 'FK_CPLCB_CCBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_VEH_COVG_BASE','COVG_KEY','CS_COVG_BASE','COVG_KEY',NULL, 'FK_CPVCB_CCBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_ADDL_INTRST_DELTA','HO_ADDL_INTRST_KEY','CS_HO_ADDL_INTRST_BASE','HO_ADDL_INTRST_KEY',NULL, 'FK_CHAID_CHAIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_BILL_INV_MONTH_AGG','CHRG_KEY','CS_BILL_CHRG_BASE','CHRG_KEY',NULL, 'FK_CBIMA_CBCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_BILL_INV_MONTH_AGG','BILL_INV_KEY','CS_BILL_INV_BASE','BILL_INV_KEY',NULL, 'FK_CBIMA_CBIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_BILL_INV_MONTH_AGG','WO_KEY','CS_BILL_WRITEOFF_BASE','WO_KEY',NULL, 'FK_CBIMA_CBWB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_BILL_INV_MONTH_AGG','BILL_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBIMA_CBAB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_BILL_INV_MONTH_AGG','BILL_POL_PRD_KEY','CS_BILL_POL_PRD_BASE','BILL_POL_PRD_KEY',NULL, 'FK_CBIMA_CBPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_OTHR_LOC_BASE','HO_LINE_COVG_KEY','CS_HO_LINE_COVG_BASE','HO_LINE_COVG_KEY',NULL, 'FK_CHOLB_CHLCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_DWLNG_COVG_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CHDCB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_SI_BASE','HO_DWLNG_COVG_KEY','CS_HO_DWLNG_COVG_BASE','HO_DWLNG_COVG_KEY',NULL, 'FK_CHSB_CHDCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_PREM_TRAN','HO_LINE_COVG_KEY','CS_HO_LINE_COVG_BASE','HO_LINE_COVG_KEY',NULL, 'FK_CHPT_CHLCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_PREM_TRAN','HO_DWLNG_COVG_KEY','CS_HO_DWLNG_COVG_BASE','HO_DWLNG_COVG_KEY',NULL, 'FK_CHPT_CHDCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_PREM_TRAN','PA_LINE_COVG_KEY','CS_PA_LINE_COVG_BASE','PA_LINE_COVG_KEY',NULL, 'FK_CPPT_CPLCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_PREM_TRAN','PA_VEH_COVG_KEY','CS_PA_VEH_COVG_BASE','PA_VEH_COVG_KEY',NULL, 'FK_CPPT_CPVCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_LINE_COVG_BASE','COVG_KEY','CS_COVG_BASE','COVG_KEY',NULL, 'FK_CHLCB_CCBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_OTHR_LOC_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CHOLB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_OTHR_LOC_DELTA','LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CHOLD_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_DWLNG_MOD_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CHDMB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_ADDL_INTRST_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CHAIB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_ADDL_INTRST_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CPAIB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_DRVR_BASE','POL_PARTY_KEY','CS_POL_PARTY_BASE','POL_PARTY_KEY',NULL, 'FK_CPDB_CPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_DRVR_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CPDB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_LINE_COVG_TERM_BASE','PA_LINE_COVG_KEY','CS_PA_LINE_COVG_BASE','PA_LINE_COVG_KEY',NULL, 'FK_CPLCTB_CPLCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_VEH_COVG_TERM_BASE','PA_VEH_COVG_KEY','CS_PA_VEH_COVG_BASE','PA_VEH_COVG_KEY',NULL, 'FK_CPVCTB_CPVCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_LINE_COVG_TERM_DELTA','HO_LINE_COVG_TERM_KEY','CS_HO_LINE_COVG_TERM_BASE','HO_LINE_COVG_TERM_KEY',NULL, 'FK_CHLCTD_CHLCTB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_DWLNG_COVG_TERM_BASE','HO_DWLNG_COVG_KEY','CS_HO_DWLNG_COVG_BASE','HO_DWLNG_COVG_KEY',NULL, 'FK_CHDCTB_CHDCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_INV','CS_BILL_INV_DELTA','BILL_INV_KEY','CS_BILL_INV_BASE','BILL_INV_KEY',NULL, 'FK_CBID_CBIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_INV_ITEM','CS_BILL_INV_ITEM_DELTA','BILL_INV_KEY','CS_BILL_INV_BASE','BILL_INV_KEY',NULL, 'FK_CBIID_CBIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_INV_ITEM','CS_BILL_INV_ITEM_DELTA','BILL_INV_ITEM_KEY','CS_BILL_INV_ITEM_BASE','BILL_INV_ITEM_KEY',NULL, 'FK_CBIID_CBIIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_ACCT','CS_BILL_ACCT_DELTA','BILL_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBAD_CBAB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AR_TRAN','CS_BILL_AR_TRAN','BILL_INV_ITEM_KEY','CS_BILL_INV_ITEM_BASE','BILL_INV_ITEM_KEY',NULL, 'FK_CBAT_CBIIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AR_TRAN','CS_BILL_AR_TRAN','DISTR_ITEM_KEY','CS_BILL_DISTR_ITEM_BASE','DISTR_ITEM_KEY',NULL, 'FK_CBAT_CBDIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AR_TRAN','CS_BILL_AR_TRAN','CHRG_KEY','CS_BILL_CHRG_BASE','CHRG_KEY',NULL, 'FK_CBAT_CBCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AR_TRAN','CS_BILL_AR_TRAN','FROM_PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBAT_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AR_TRAN','CS_BILL_AR_TRAN','WO_KEY','CS_BILL_WRITEOFF_BASE','WO_KEY',NULL, 'FK_CBAT_CBWB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AR_TRAN','CS_BILL_AR_TRAN','FROM_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBAT_CBAB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AP_TRAN','CS_BILL_AP_TRAN','AGCY_CYCLE_DISTR_KEY','CS_BILL_DISTR_BASE','DISTR_KEY',NULL, 'FK_CBAT_CBDB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AP_TRAN','CS_BILL_AP_TRAN','DISTR_ITEM_KEY','CS_BILL_DISTR_ITEM_BASE','DISTR_ITEM_KEY',NULL, 'FK_CBAT_CBDIB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AP_TRAN','CS_BILL_AP_TRAN','CHRG_COMM_KEY','CS_BILL_CHRG_COMM_BASE','CHRG_COMM_KEY',NULL, 'FK_CBAT_CBCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_ADDL_INTRST_BASE','CA_CVRBL_KEY','CS_CA_CVRBL_BASE','CA_CVRBL_KEY',NULL, 'FK_CCAIB_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_ADDL_INTRST_BASE','CP_CVRBL_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCAIB_CCCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_ADDL_INTRST_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCAIB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_ADDL_INTRST_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCAIB_CPBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_ADDL_INTRST_BASE','POL_PARTY_KEY','CS_POL_PARTY_BASE','POL_PARTY_KEY',NULL, 'FK_CCAIB_CPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_ADDL_INTRST_BASE','POL_PARTY_KEY','CS_POL_PARTY_BASE','POL_PARTY_KEY',NULL, 'FK_CCAIB_CPPB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_ADDL_INTRST_DELTA','CA_ADDL_INTRST_KEY','CS_CA_ADDL_INTRST_BASE','CA_ADDL_INTRST_KEY',NULL, 'FK_CCAID_CCAIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_ADDL_INTRST_DELTA','CP_ADDL_INTRST_KEY','CS_CP_ADDL_INTRST_BASE','CP_ADDL_INTRST_KEY',NULL, 'FK_CCAID_CCAIB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AP_TRAN','CS_BILL_AP_TRAN','COMM_RED_KEY','CS_BILL_COMM_REDUCT_BASE','COMM_RED_KEY',NULL, 'FK_CBAT_CBCRB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AP_TRAN','CS_BILL_AP_TRAN','INCOMMING_PRDCR_PMT_KEY','CS_BILL_IN_PROD_PMT_BASE','INCOMMING_PRDCR_PMT_KEY',NULL, 'FK_CBAT_CBIPPB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AP_TRAN','CS_BILL_AP_TRAN','BILL_INV_ITEM_KEY','CS_BILL_INV_ITEM_BASE','BILL_INV_ITEM_KEY',NULL, 'FK_CBAT_CBIIB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_BLDG_DELTA','CP_CVRBL_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCBD_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_BLNKT_DELTA','CP_CVRBL_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCBD_CCCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_BI_DELTA','CP_CVRBL_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCBD_CCCB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_BI_DELTA','CP_BLDG_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCBD_CCCB3', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_BLDG_DELTA','CP_LOC_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCBD_CCCB4', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_BLDG_DELTA','CP_CL_CD_KEY','CS_CP_CL_CODE_BASE','CP_CL_CD_KEY',NULL, 'FK_CCBD_CCCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_COND_BASE','COND_KEY','CS_COND_BASE','COND_KEY',NULL, 'FK_CCCB_CCBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_COVG_BASE','COVG_KEY','CS_COVG_BASE','COVG_KEY',NULL, 'FK_CCCB_CCBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_COVG_BASE','COVG_KEY','CS_COVG_BASE','COVG_KEY',NULL, 'FK_CCCB_CCBA2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_COND_BASE','COND_KEY','CS_COND_BASE','COND_KEY',NULL, 'FK_CCCB_CCBA3', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_COND_BASE','CA_CVRBL_KEY','CS_CA_CVRBL_BASE','CA_CVRBL_KEY',NULL, 'FK_CCCB_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_COVG_BASE','CA_CVRBL_KEY','CS_CA_CVRBL_BASE','CA_CVRBL_KEY',NULL, 'FK_CCCB_CCCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_CVRBL_BASE','OWNING_CVRBL_KEY','CS_CA_CVRBL_BASE','CA_CVRBL_KEY',NULL, 'FK_CCCB_CCCB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_COVG_BASE','CP_CVRBL_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCCB_CCCB3', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_COND_BASE','CP_CVRBL_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCCB_CCCB4', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_CVRBL_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCCB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_COND_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCCB_CPBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_COVG_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCCB_CPBA2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_CVRBL_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCCB_CPBA3', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_COND_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCCB_CPBA4', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_COVG_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCCB_CPBA5', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_CVRBL_BASE','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CCCB_CPLB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_COND_BASE','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CCCB_CPLB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_COVG_BASE','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CCCB_CPLB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_COVG_BASE','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CCCB_CPLB3', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_COND_BASE','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CCCB_CPLB4', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_CVRBL_BASE','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CCCB_CPLB5', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_CL_CODE_DELTA','CP_CL_CD_KEY','CS_CP_CL_CODE_BASE','CP_CL_CD_KEY',NULL, 'FK_CCCCD_CCCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_COND_TERM_BASE','CA_COND_KEY','CS_CA_COND_BASE','CA_COND_KEY',NULL, 'FK_CCCTB_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_COVG_TERM_BASE','CA_COVG_KEY','CS_CA_COVG_BASE','CA_COVG_KEY',NULL, 'FK_CCCTB_CCCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_COVG_TERM_BASE','CP_COVG_KEY','CS_CP_COVG_BASE','CP_COVG_KEY',NULL, 'FK_CCCTB_CCCB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_COND_TERM_BASE','CP_COND_KEY','CS_CP_COND_BASE','CP_COND_KEY',NULL, 'FK_CCCTB_CCCB3', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_COVG_TERM_DELTA','CA_COVG_TERM_KEY','CS_CA_COVG_TERM_BASE','CA_COVG_TERM_KEY',NULL, 'FK_CCCTD_CCCTB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_COND_TERM_DELTA','CA_COND_TERM_KEY','CS_CA_COND_TERM_BASE','CA_COND_TERM_KEY',NULL, 'FK_CCCTD_CCCTB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_COVG_TERM_DELTA','CP_COVG_TERM_KEY','CS_CP_COVG_TERM_BASE','CP_COVG_TERM_KEY',NULL, 'FK_CCCTD_CCCTB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_COND_TERM_DELTA','CP_COND_TERM_KEY','CS_CP_COND_TERM_BASE','CP_COND_TERM_KEY',NULL, 'FK_CCCTD_CCCTB3', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_DRVR_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCDB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_BLDG_DELTA','BLDG_KEY','CS_BLDG_BASE','BLDG_KEY',NULL, 'FK_CCBD_CBBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_DRVR_BASE','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CCDB_CPLB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_DEALER_DELTA','CA_CVRBL_KEY','CS_CA_CVRBL_BASE','CA_CVRBL_KEY',NULL, 'FK_CCDD_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_DRVR_DELTA','CA_DRVR_KEY','CS_CA_DRVR_BASE','CA_DRVR_KEY',NULL, 'FK_CCDD_CCDB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_DEALER_DELTA','DEALER_LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CCDD_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AP_TRAN','CS_BILL_AP_TRAN','NEG_WO_KEY','CS_BILL_NEG_WRITEOFF_BASE','NEG_WO_KEY',NULL, 'FK_CBAT_CBNWB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AP_TRAN','CS_BILL_AP_TRAN','POL_COMM_KEY','CS_BILL_POL_COMM_BASE','POL_COMM_KEY',NULL, 'FK_CBAT_CBPCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AP_TRAN','CS_BILL_AP_TRAN','PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBAT_CBPB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_COND_DELTA','COND_KEY','CS_COND_BASE','COND_KEY',NULL, 'FK_CCDE_CCBA3', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_EXCL_BASE','CA_CVRBL_KEY','CS_CA_CVRBL_BASE','CA_CVRBL_KEY',NULL, 'FK_CCEB_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_EXCL_BASE','CP_CVRBL_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCEB_CCCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_EXCL_BASE','EXCL_KEY','CS_EXCL_BASE','EXCL_KEY',NULL, 'FK_CCEB_CEBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_EXCL_BASE','EXCL_KEY','CS_EXCL_BASE','EXCL_KEY',NULL, 'FK_CCEB_CEBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_EXCL_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCEB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_EXCL_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCEB_CPBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_EXCL_BASE','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CCEB_CPLB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_EXCL_BASE','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CCEB_CPLB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_EXCL_TERM_BASE','CA_EXCL_KEY','CS_CA_EXCL_BASE','CA_EXCL_KEY',NULL, 'FK_CCETB_CCEB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_EXCL_TERM_BASE','CP_EXCL_KEY','CS_CP_EXCL_BASE','CP_EXCL_KEY',NULL, 'FK_CCETB_CCEB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_EXCL_TERM_DELTA','CA_EXCL_TERM_KEY','CS_CA_EXCL_TERM_BASE','CA_EXCL_TERM_KEY',NULL, 'FK_CCETD_CCETB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_EXCL_TERM_DELTA','CP_EXCL_TERM_KEY','CS_CP_EXCL_TERM_BASE','CP_EXCL_TERM_KEY',NULL, 'FK_CCETD_CCETB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AP_TRAN','CS_BILL_AP_TRAN','CR_PYBL_OF_PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBAT_CBPB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AP_TRAN','CS_BILL_AP_TRAN','PRDCR_PYBL_TFR_KEY','CS_BILL_PROD_PYBL_TRNFR_BASE','PRDCR_PYBL_TFR_KEY',NULL, 'FK_CBAT_CBPPTB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AP_TRAN','CS_BILL_AP_TRAN','PRDCR_PMT_KEY','CS_BILL_PROD_PMT_BASE','PRDCR_PMT_KEY',NULL, 'FK_CBAT_CBPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AP_TRAN','CS_BILL_AP_TRAN','PRDCR_STMNT_KEY','CS_BILL_PROD_STMNT_BASE','PRDCR_STMNT_KEY',NULL, 'FK_CBAT_CBPSB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AP_TRAN','CS_BILL_AP_TRAN','WO_KEY','CS_BILL_WRITEOFF_BASE','WO_KEY',NULL, 'FK_CBAT_CBWB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_GAR_SRVC_DELTA','CA_CVRBL_KEY','CS_CA_CVRBL_BASE','CA_CVRBL_KEY',NULL, 'FK_CCGSD_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_GAR_SRVC_DELTA','GARAGE_SRVC_LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CCGSD_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_JURS_DELTA','CA_CVRBL_KEY','CS_CA_CVRBL_BASE','CA_CVRBL_KEY',NULL, 'FK_CCJD_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_LOC_DELTA','CP_CVRBL_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCLD_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_LOC_DELTA','LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CCLD_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_LINE_SI_COND_BASE','CA_COND_KEY','CS_CA_COND_BASE','CA_COND_KEY',NULL, 'FK_CCLSCB_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_LINE_SI_COND_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCLSCB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_LINE_SI_COND_DELTA','CA_LINE_SI_COND_KEY','CS_CA_LINE_SI_COND_BASE','CA_LINE_SI_COND_KEY',NULL, 'FK_CCLSCD_CCLSCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_LINE_SI_COND_DELTA','POL_PARTY_KEY','CS_POL_PARTY_BASE','POL_PARTY_KEY',NULL, 'FK_CCLSCD_CPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_MOD_BASE','CA_CVRBL_KEY','CS_CA_CVRBL_BASE','CA_CVRBL_KEY',NULL, 'FK_CCMB_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_MOD_BASE','CP_CVRBL_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCMB_CCCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_MOD_BASE','MOD_KEY','CS_MOD_BASE','MOD_KEY',NULL, 'FK_CCMB_CMBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_MOD_BASE','MOD_KEY','CS_MOD_BASE','MOD_KEY',NULL, 'FK_CCMB_CMBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_MOD_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCMB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_MOD_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCMB_CPBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_MOD_BASE','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CCMB_CPLB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_MOD_BASE','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CCMB_CPLB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_MOD_DELTA','CA_MOD_KEY','CS_CA_MOD_BASE','CA_MOD_KEY',NULL, 'FK_CCMD_CCMB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_MOD_DELTA','CP_MOD_KEY','CS_CP_MOD_BASE','CP_MOD_KEY',NULL, 'FK_CCMD_CCMB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_MOD_RF_BASE','CA_MOD_KEY','CS_CA_MOD_BASE','CA_MOD_KEY',NULL, 'FK_CCMRB_CCMB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_MOD_RF_BASE','CP_MOD_KEY','CS_CP_MOD_BASE','CP_MOD_KEY',NULL, 'FK_CCMRB_CCMB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_MOD_RF_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCMRB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_MOD_RF_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCMRB_CPBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_MOD_RF_DELTA','CA_MOD_RF_KEY','CS_CA_MOD_RF_BASE','CA_MOD_RF_KEY',NULL, 'FK_CCMRD_CCMRB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_MOD_RF_DELTA','CP_MOD_RF_KEY','CS_CP_MOD_RF_BASE','CP_MOD_RF_KEY',NULL, 'FK_CCMRD_CCMRB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_NMD_INDIV_DELTA','CA_CVRBL_KEY','CS_CA_CVRBL_BASE','CA_CVRBL_KEY',NULL, 'FK_CCNID_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_OCC_CL_DELTA','CP_CVRBL_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCOCD_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_OCC_CL_DELTA','CP_BLDG_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCOCD_CCCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_OCC_CL_DELTA','CP_CL_CD_KEY','CS_CP_CL_CODE_BASE','CP_CL_CD_KEY',NULL, 'FK_CCOCD_CCCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_PP_DELTA','CP_CVRBL_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCPD_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_PP_DELTA','CP_OCC_CL_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCPD_CCCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_POL_LINE_DELTA','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CCPLD_CPLB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_POL_LINE_DELTA','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CCPLD_CPLB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_PREM_TRAN','AGCY_KEY','CS_AGENCY_BASE','AGCY_KEY',NULL, 'FK_CCPT_CABA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_PREM_TRAN','AGNT_KEY','CS_AGENT_BASE','AGNT_KEY',NULL, 'FK_CCPT_CABA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_PREM_TRAN','AGCY_KEY','CS_AGENCY_BASE','AGCY_KEY',NULL, 'FK_CCPT_CABA2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_PREM_TRAN','AGNT_KEY','CS_AGENT_BASE','AGNT_KEY',NULL, 'FK_CCPT_CABA3', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_PREM_TRAN','COVG_KEY','CS_COVG_BASE','COVG_KEY',NULL, 'FK_CCPT_CCBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_PREM_TRAN','COVG_KEY','CS_COVG_BASE','COVG_KEY',NULL, 'FK_CCPT_CCBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_PREM_TRAN','CA_CVRBL_KEY','CS_CA_CVRBL_BASE','CA_CVRBL_KEY',NULL, 'FK_CCPT_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_PREM_TRAN','CA_COVG_KEY','CS_CA_COVG_BASE','CA_COVG_KEY',NULL, 'FK_CCPT_CCCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_PREM_TRAN','CP_CVRBL_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCPT_CCCB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_PREM_TRAN','CP_COVG_KEY','CS_CP_COVG_BASE','CP_COVG_KEY',NULL, 'FK_CCPT_CCCB3', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_PREM_TRAN','CP_LOC_LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CCPT_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_PREM_TRAN','AUTO_DEALER_LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CCPT_CLBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_PREM_TRAN','GARAGE_SRVC_LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CCPT_CLBA2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_PREM_TRAN','VEH_GARAGE_LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CCPT_CLBA3', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_PREM_TRAN','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCPT_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_PREM_TRAN','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCPT_CPBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_PREM_TRAN','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CCPT_CPLB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_PREM_TRAN','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CCPT_CPLB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_SPCL_CL_BI_DELTA','CP_CVRBL_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCSCBD_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_SPCL_CL_BI_DELTA','CP_SPCL_CL_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCSCBD_CCCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_SPCL_CL_DELTA','CP_CVRBL_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCSCD_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_SPCL_CL_DELTA','CP_LOC_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCSCD_CCCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_SI_DELTA','CA_ADDL_INTRST_KEY','CS_CA_ADDL_INTRST_BASE','CA_ADDL_INTRST_KEY',NULL, 'FK_CCSD_CCAIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_SI_DELTA','CP_ADDL_INTRST_KEY','CS_CP_ADDL_INTRST_BASE','CP_ADDL_INTRST_KEY',NULL, 'FK_CCSD_CCAIB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_SI_DELTA','CA_CVRBL_KEY','CS_CA_CVRBL_BASE','CA_CVRBL_KEY',NULL, 'FK_CCSD_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_SI_DELTA','CA_COVG_KEY','CS_CA_COVG_BASE','CA_COVG_KEY',NULL, 'FK_CCSD_CCCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_SI_DELTA','CP_CVRBL_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCSD_CCCB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_SI_DELTA','CP_COVG_KEY','CS_CP_COVG_BASE','CP_COVG_KEY',NULL, 'FK_CCSD_CCCB3', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_SI_DELTA','CP_OWNING_CVRBL_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCSD_CCCB4', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_SI_DELTA','LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CCSD_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_SI_DELTA','LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CCSD_CLBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_SI_DELTA','POL_PARTY_KEY','CS_POL_PARTY_BASE','POL_PARTY_KEY',NULL, 'FK_CCSD_CPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_SI_DELTA','POL_PARTY_KEY','CS_POL_PARTY_BASE','POL_PARTY_KEY',NULL, 'FK_CCSD_CPPB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_PROD','CS_BILL_PROD_DELTA','PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBPD_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_PROD','CS_BILL_PROD_DELTA','RPT_GROUP_KEY','CS_REPORT_GRP_BASE','RPT_GROUP_KEY',NULL, 'FK_CBPD_CRGB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_TERR_CODE_BASE','CP_LOC_KEY','CS_CP_CVRBL_BASE','CP_CVRBL_KEY',NULL, 'FK_CCTCB_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_TERR_CODE_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CCTCB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CP_TERR_CODE_DELTA','CP_TERR_CD_KEY','CS_CP_TERR_CODE_BASE','CP_TERR_CD_KEY',NULL, 'FK_CCTCD_CCTCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_VEH_DELTA','CA_CVRBL_KEY','CS_CA_CVRBL_BASE','CA_CVRBL_KEY',NULL, 'FK_CCVD_CCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_CA_VEH_DELTA','GARAGE_LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CCVD_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_EXCL_DELTA','EXCL_KEY','CS_EXCL_BASE','EXCL_KEY',NULL, 'FK_CEDE_CEBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AP_TRAN','CS_BILL_AP_TRAN','PRDCR_CD_KEY','CS_BILL_PROD_CD_BASE','PRDCR_CD_KEY',NULL, 'FK_CBAT_CBPCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_PROD_CD','CS_BILL_PROD_CD_DELTA','PRDCR_CD_KEY','CS_BILL_PROD_CD_BASE','PRDCR_CD_KEY',NULL, 'FK_CBPCD_CBPCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_PROD_CD','CS_BILL_PROD_CD_DELTA','PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBPCD_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_PROD_STMNT','CS_BILL_PROD_STMNT_DELTA','PRDCR_STMNT_KEY','CS_BILL_PROD_STMNT_BASE','PRDCR_STMNT_KEY',NULL, 'FK_CBPSD_CBPSB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_FORM_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CFBA_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_FORM_DELTA','FORM_KEY','CS_FORM_BASE','FORM_KEY',NULL, 'FK_CFDE_CFBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_COVG_BASE','COVG_KEY','CS_COVG_BASE','COVG_KEY',NULL, 'FK_CGCB_CCBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_COND_BASE','COND_KEY','CS_COND_BASE','COND_KEY',NULL, 'FK_CGCB_CCBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_COVG_BASE','GL_CVRBL_KEY','CS_GL_CVRBL_BASE','GL_CVRBL_KEY',NULL, 'FK_CGCB_CGCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_COND_BASE','GL_CVRBL_KEY','CS_GL_CVRBL_BASE','GL_CVRBL_KEY',NULL, 'FK_CGCB_CGCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_CVRBL_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CGCB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_COVG_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CGCB_CPBA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_COND_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CGCB_CPBA2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_CVRBL_BASE','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CGCB_CPLB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_COVG_BASE','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CGCB_CPLB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_COND_BASE','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CGCB_CPLB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_CL_CODE_DELTA','GL_CL_CD_KEY','CS_GL_CL_CODE_BASE','GL_CL_CD_KEY',NULL, 'FK_CGCCD_CGCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_COVG_PART_DELTA','GL_CVRBL_KEY','CS_GL_CVRBL_BASE','GL_CVRBL_KEY',NULL, 'FK_CGCPD_CGCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_COVG_TERM_BASE','GL_COVG_KEY','CS_GL_COVG_BASE','GL_COVG_KEY',NULL, 'FK_CGCTB_CGCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_COND_TERM_BASE','GL_COND_KEY','CS_GL_COND_BASE','GL_COND_KEY',NULL, 'FK_CGCTB_CGCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_COVG_TERM_DELTA','GL_COVG_TERM_KEY','CS_GL_COVG_TERM_BASE','GL_COVG_TERM_KEY',NULL, 'FK_CGCTD_CGCTB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_COND_TERM_DELTA','GL_COND_TERM_KEY','CS_GL_COND_TERM_BASE','GL_COND_TERM_KEY',NULL, 'FK_CGCTD_CGCTB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_EXCL_BASE','EXCL_KEY','CS_EXCL_BASE','EXCL_KEY',NULL, 'FK_CGEB_CEBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_EXCL_BASE','GL_CVRBL_KEY','CS_GL_CVRBL_BASE','GL_CVRBL_KEY',NULL, 'FK_CGEB_CGCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_EXCL_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CGEB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_EXCL_BASE','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CGEB_CPLB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_EXPSR_DELTA','GL_CVRBL_KEY','CS_GL_CVRBL_BASE','GL_CVRBL_KEY',NULL, 'FK_CGED_CGCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_EXPSR_DELTA','GL_COVG_PART_KEY','CS_GL_CVRBL_BASE','GL_CVRBL_KEY',NULL, 'FK_CGED_CGCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_EXPSR_DELTA','GL_CL_CD_KEY','CS_GL_CL_CODE_BASE','GL_CL_CD_KEY',NULL, 'FK_CGED_CGCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_EXPSR_DELTA','LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CGED_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_EXCL_TERM_BASE','GL_EXCL_KEY','CS_GL_EXCL_BASE','GL_EXCL_KEY',NULL, 'FK_CGETB_CGEB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_EXCL_TERM_DELTA','GL_EXCL_TERM_KEY','CS_GL_EXCL_TERM_BASE','GL_EXCL_TERM_KEY',NULL, 'FK_CGETD_CGETB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_MOD_BASE','GL_CVRBL_KEY','CS_GL_CVRBL_BASE','GL_CVRBL_KEY',NULL, 'FK_CGMB_CGCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_MOD_BASE','MOD_KEY','CS_MOD_BASE','MOD_KEY',NULL, 'FK_CGMB_CMBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_MOD_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CGMB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_MOD_BASE','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CGMB_CPLB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_MOD_DELTA','GL_MOD_KEY','CS_GL_MOD_BASE','GL_MOD_KEY',NULL, 'FK_CGMD_CGMB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_MOD_RF_BASE','GL_MOD_KEY','CS_GL_MOD_BASE','GL_MOD_KEY',NULL, 'FK_CGMRB_CGMB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_MOD_RF_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CGMRB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_MOD_RF_DELTA','GL_MOD_RF_KEY','CS_GL_MOD_RF_BASE','GL_MOD_RF_KEY',NULL, 'FK_CGMRD_CGMRB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_POL_LINE_DELTA','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CGPLD_CPLB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_PREM_TRAN','AGCY_KEY','CS_AGENCY_BASE','AGCY_KEY',NULL, 'FK_CGPT_CABA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_PREM_TRAN','AGNT_KEY','CS_AGENT_BASE','AGNT_KEY',NULL, 'FK_CGPT_CABA1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_PREM_TRAN','COVG_KEY','CS_COVG_BASE','COVG_KEY',NULL, 'FK_CGPT_CCBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_PREM_TRAN','GL_CVRBL_KEY','CS_GL_CVRBL_BASE','GL_CVRBL_KEY',NULL, 'FK_CGPT_CGCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_PREM_TRAN','GL_COVG_KEY','CS_GL_COVG_BASE','GL_COVG_KEY',NULL, 'FK_CGPT_CGCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_PREM_TRAN','GL_COND_KEY','CS_GL_COND_BASE','GL_COND_KEY',NULL, 'FK_CGPT_CGCB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_PREM_TRAN','GL_EXCL_KEY','CS_GL_EXCL_BASE','GL_EXCL_KEY',NULL, 'FK_CGPT_CGEB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_PREM_TRAN','EXPSR_LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CGPT_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_PREM_TRAN','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CGPT_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_PREM_TRAN','POL_LINE_KEY','CS_POL_LINE_BASE','POL_LINE_KEY',NULL, 'FK_CGPT_CPLB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_PREM_TRAN','ADDL_INS_KEY','CS_POL_PARTY_BASE','POL_PARTY_KEY',NULL, 'FK_CGPT_CPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_SI_COND_BASE','GL_CVRBL_KEY','CS_GL_CVRBL_BASE','GL_CVRBL_KEY',NULL, 'FK_CGSCB_CGCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_SI_COND_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CGSCB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_SI_COND_DELTA','GL_COND_KEY','CS_GL_COND_BASE','GL_COND_KEY',NULL, 'FK_CGSCD_CGCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_SI_COND_DELTA','GL_SI_COND_KEY','CS_GL_SI_COND_BASE','GL_SI_COND_KEY',NULL, 'FK_CGSCD_CGSCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_SI_COND_DELTA','LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CGSCD_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_SI_COND_DELTA','POL_PARTY_KEY','CS_POL_PARTY_BASE','POL_PARTY_KEY',NULL, 'FK_CGSCD_CPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_SI_DELTA','GL_CVRBL_KEY','CS_GL_CVRBL_BASE','GL_CVRBL_KEY',NULL, 'FK_CGSD_CGCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_SI_DELTA','GL_COVG_KEY','CS_GL_COVG_BASE','GL_COVG_KEY',NULL, 'FK_CGSD_CGCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_SI_DELTA','GL_OWNING_CVRBL_KEY','CS_GL_CVRBL_BASE','GL_CVRBL_KEY',NULL, 'FK_CGSD_CGCB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_SI_DELTA','LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CGSD_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_SI_DELTA','POL_PARTY_KEY','CS_POL_PARTY_BASE','POL_PARTY_KEY',NULL, 'FK_CGSD_CPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_SI_EXCL_BASE','GL_CVRBL_KEY','CS_GL_CVRBL_BASE','GL_CVRBL_KEY',NULL, 'FK_CGSEB_CGCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_SI_EXCL_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CGSEB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_SI_EXCL_DELTA','GL_EXCL_KEY','CS_GL_EXCL_BASE','GL_EXCL_KEY',NULL, 'FK_CGSED_CGEB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_SI_EXCL_DELTA','GL_SI_EXCL_KEY','CS_GL_SI_EXCL_BASE','GL_SI_EXCL_KEY',NULL, 'FK_CGSED_CGSEB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_SI_EXCL_DELTA','LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CGSED_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_GL_SI_EXCL_DELTA','POL_PARTY_KEY','CS_POL_PARTY_BASE','POL_PARTY_KEY',NULL, 'FK_CGSED_CPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_POL_COMM','CS_BILL_POL_COMM_DELTA','POL_COMM_KEY','CS_BILL_POL_COMM_BASE','POL_COMM_KEY',NULL, 'FK_CBPCD_CBPCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CHRG','CS_BILL_CHRG_DELTA','CHRG_KEY','CS_BILL_CHRG_BASE','CHRG_KEY',NULL, 'FK_CBCD_CBCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CHRG_PAT','CS_BILL_CHRG_PAT_DELTA','CHRG_PAT_KEY','CS_BILL_CHRG_PAT_BASE','CHRG_PAT_KEY',NULL, 'FK_CBCPD_CBCPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CHRG','CS_BILL_CHRG_DELTA','CHRG_PAT_KEY','CS_BILL_CHRG_PAT_BASE','CHRG_PAT_KEY',NULL, 'FK_CBCD_CBCPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_WO','CS_BILL_WRITEOFF_DELTA','WO_KEY','CS_BILL_WRITEOFF_BASE','WO_KEY',NULL, 'FK_CBWD_CBWB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_WO','CS_BILL_WRITEOFF_DELTA','CHRG_PAT_KEY','CS_BILL_CHRG_PAT_BASE','CHRG_PAT_KEY',NULL, 'FK_CBWD_CBCPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CASH_TRAN','CS_BILL_CASH_TRAN','BILL_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBCT_CBAB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_DISB','CS_BILL_DISB_DELTA','DISB_KEY','CS_BILL_DISB_BASE','DISB_KEY',NULL, 'FK_CBDD_CBDB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CASH_TRAN','CS_BILL_CASH_TRAN','DISB_KEY','CS_BILL_DISB_BASE','DISB_KEY',NULL, 'FK_CBCT_CBDB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CASH_TRAN','CS_BILL_CASH_TRAN','NEG_WO_KEY','CS_BILL_NEG_WRITEOFF_BASE','NEG_WO_KEY',NULL, 'FK_CBCT_CBNWB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CASH_TRAN','CS_BILL_CASH_TRAN','PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBCT_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CASH_TRAN','CS_BILL_CASH_TRAN','COLTRL_REQ_KEY','CS_BILL_COLL_RQRMNT_BASE','COLTRL_REQ_KEY',NULL, 'FK_CBCT_CBCRB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CASH_TRAN','CS_BILL_CASH_TRAN','SRCE_COLTRL_REQ_KEY','CS_BILL_COLL_RQRMNT_BASE','COLTRL_REQ_KEY',NULL, 'FK_CBCT_CBCRB1', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CASH_TRAN','CS_BILL_CASH_TRAN','COLTRL_KEY','CS_BILL_COLL_BASE','COLTRL_KEY',NULL, 'FK_CBCT_CBCB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CASH_TRAN','CS_BILL_CASH_TRAN','MON_RCVD_KEY','CS_BILL_MONEY_RCVD_BASE','MON_RCVD_KEY',NULL, 'FK_CBCT_CBMRB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CASH_TRAN','CS_BILL_CASH_TRAN','SUSP_PMT_KEY','CS_BILL_SUSP_PMT_BASE','SUSP_PMT_KEY',NULL, 'FK_CBCT_CBSPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_GEN_TRAN','CS_BILL_GENERAL_TRAN','COLTRL_KEY','CS_BILL_COLL_BASE','COLTRL_KEY',NULL, 'FK_CBGT_CBCB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_GEN_TRAN','CS_BILL_GENERAL_TRAN','SRCE_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBGT_CBAB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_GEN_TRAN','CS_BILL_GENERAL_TRAN','SUSP_PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBGT_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_GEN_TRAN','CS_BILL_GENERAL_TRAN','TARGET_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBGT_CBAB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_GEN_TRAN','CS_BILL_GENERAL_TRAN','SRCE_PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBGT_CBPB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_GEN_TRAN','CS_BILL_GENERAL_TRAN','TARGET_PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBGT_CBPB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_GEN_TRAN','CS_BILL_GENERAL_TRAN','FUNDS_TFR_KEY','CS_BILL_FUNDS_TRNFR_BASE','FUNDS_TFR_KEY',NULL, 'FK_CBGT_CBFTB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CHRG_COMM','CS_BILL_CHRG_COMM_DELTA','POL_COMM_KEY','CS_BILL_POL_COMM_BASE','POL_COMM_KEY',NULL, 'FK_CBCCD_CBPCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_HO_PREM_TRAN','HO_DWLNG_LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CHPT_CLBA2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CASH_TRAN','CS_BILL_CASH_TRAN','UNAPPLIED_FUND_KEY','CS_BILL_UNAPP_FUND_BASE','UNAPPLIED_FUND_KEY',NULL, 'FK_CBCT_CBUFB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AP_TRAN','CS_BILL_AP_TRAN','PYBL_RECEIVER_STMNT_KEY','CS_BILL_PROD_STMNT_BASE','PRDCR_STMNT_KEY',NULL, 'FK_CBAT_CBPSB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_DISTR','CS_BILL_DISTR_DELTA','DISTR_KEY','CS_BILL_DISTR_BASE','DISTR_KEY',NULL, 'FK_CBDD_CBDB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_DISTR_ITEM','CS_BILL_DISTR_ITEM_DELTA','DISTR_ITEM_KEY','CS_BILL_DISTR_ITEM_BASE','DISTR_ITEM_KEY',NULL, 'FK_CBDID_CBDIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_DISTR_ITEM','CS_BILL_DISTR_ITEM_DELTA','BILL_INV_ITEM_KEY','CS_BILL_INV_ITEM_BASE','BILL_INV_ITEM_KEY',NULL, 'FK_CBDID_CBIIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_DISTR_ITEM','CS_BILL_DISTR_ITEM_DELTA','PRDCR_CD_KEY','CS_BILL_PROD_CD_BASE','PRDCR_CD_KEY',NULL, 'FK_CBDID_CBPCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_NEG_WO','CS_BILL_NEG_WRITEOFF_DELTA','NEG_WO_KEY','CS_BILL_NEG_WRITEOFF_BASE','NEG_WO_KEY',NULL, 'FK_CBNWD_CBNWB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CHRG_COMM','CS_BILL_CHRG_COMM_DELTA','CHRG_COMM_KEY','CS_BILL_CHRG_COMM_BASE','CHRG_COMM_KEY',NULL, 'FK_CBCCD_CBCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_GEN_TRAN','CS_BILL_GENERAL_TRAN','CR_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBGT_CBAB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_MOD_DELTA','MOD_KEY','CS_MOD_BASE','MOD_KEY',NULL, 'FK_CMDE_CMBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_GEN_TRAN','CS_BILL_GENERAL_TRAN','CR_KEY','CS_BILL_CREDIT_BASE','CR_KEY',NULL, 'FK_CBGT_CBCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_GEN_TRAN','CS_BILL_GENERAL_TRAN','NON_RCVBL_DISTR_ITEM_KEY','CS_BILL_NR_DISTR_ITEM_BASE','NON_RCVBL_DISTR_ITEM_KEY',NULL, 'FK_CBGT_CBNDIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_POL_LINE_BASE','POL_KEY','CS_POLICY_BASE','POL_KEY',NULL, 'FK_CPLB_CPBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_POL_PRD','CS_BILL_POL_PRD_DELTA','BILL_POL_PRD_KEY','CS_BILL_POL_PRD_BASE','BILL_POL_PRD_KEY',NULL, 'FK_CBPPD_CBPPB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_INV_ITEM','CS_BILL_INV_ITEM_DELTA','CHRG_KEY','CS_BILL_CHRG_BASE','CHRG_KEY',NULL, 'FK_CBIID_CBCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_INV_ITEM','CS_BILL_INV_ITEM_DELTA','BILL_POL_PRD_KEY','CS_BILL_POL_PRD_BASE','BILL_POL_PRD_KEY',NULL, 'FK_CBIID_CBPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_INSTR','CS_BILL_INSTRUCTION_DELTA','BILL_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBID_CBAB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_INSTR','CS_BILL_INSTRUCTION_DELTA','BILL_POL_PRD_KEY','CS_BILL_POL_PRD_BASE','BILL_POL_PRD_KEY',NULL, 'FK_CBID_CBPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_INSTR','CS_BILL_INSTRUCTION_DELTA','COLTRL_REQ_KEY','CS_BILL_COLL_RQRMNT_BASE','COLTRL_REQ_KEY',NULL, 'FK_CBID_CBCRB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_INSTR','CS_BILL_INSTRUCTION_DELTA','ASSOC_BILL_POL_PRD_KEY','CS_BILL_POL_PRD_BASE','BILL_POL_PRD_KEY',NULL, 'FK_CBID_CBPPB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_INSTR','CS_BILL_INSTRUCTION_DELTA','ISSUANCE_BILL_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBID_CBAB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_INSTR','CS_BILL_INSTRUCTION_DELTA','PRIOR_BILL_POL_PRD_KEY','CS_BILL_POL_PRD_BASE','BILL_POL_PRD_KEY',NULL, 'FK_CBID_CBPPB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_PA_PREM_TRAN','VEH_GARAGE_LOC_KEY','CS_LOC_BASE','LOC_KEY',NULL, 'FK_CPPT_CLBA', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_INSTR','CS_BILL_INSTRUCTION_DELTA','NEW_BILL_POL_PRD_KEY','CS_BILL_POL_PRD_BASE','BILL_POL_PRD_KEY',NULL, 'FK_CBID_CBPPB3', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_INSTR','CS_BILL_INSTRUCTION_DELTA','NEW_RENEW_BILL_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBID_CBAB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_INSTR','CS_BILL_INSTRUCTION_DELTA','REWRITE_BILL_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBID_CBAB3', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_INSTR','CS_BILL_INSTRUCTION_DELTA','RENEW_BILL_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBID_CBAB4', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_INSTR','CS_BILL_INSTRUCTION_DELTA','BILL_INSTR_KEY','CS_BILL_INSTRUCTION_BASE','BILL_INSTR_KEY',NULL, 'FK_CBID_CBIB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CHRG','CS_BILL_CHRG_DELTA','BILL_INSTR_KEY','CS_BILL_INSTRUCTION_BASE','BILL_INSTR_KEY',NULL, 'FK_CBCD_CBIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_POL_COMM','CS_BILL_POL_COMM_DELTA','BILL_POL_PRD_KEY','CS_BILL_POL_PRD_BASE','BILL_POL_PRD_KEY',NULL, 'FK_CBPCD_CBPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_POL_COMM','CS_BILL_POL_COMM_DELTA','PRIM_BILL_POL_PRD_KEY','CS_BILL_POL_PRD_BASE','BILL_POL_PRD_KEY',NULL, 'FK_CBPCD_CBPPB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_POL_COMM','CS_BILL_POL_COMM_DELTA','PRDCR_CD_KEY','CS_BILL_PROD_CD_BASE','PRDCR_CD_KEY',NULL, 'FK_CBPCD_CBPCB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CHRG_COMM','CS_BILL_CHRG_COMM_DELTA','CHRG_KEY','CS_BILL_CHRG_BASE','CHRG_KEY',NULL, 'FK_CBCCD_CBCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_PROD_PMT','CS_BILL_PROD_PMT_DELTA','PRDCR_PMT_KEY','CS_BILL_PROD_PMT_BASE','PRDCR_PMT_KEY',NULL, 'FK_CBPPD_CBPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_PROD_PMT','CS_BILL_PROD_PMT_DELTA','PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBPPD_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_PROD_PMT','CS_BILL_PROD_PMT_DELTA','RPT_GROUP_KEY','CS_REPORT_GRP_BASE','RPT_GROUP_KEY',NULL, 'FK_CBPPD_CRGB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_OUT_PMT','CS_BILL_OUTGOING_PMT_DELTA','OUTGOING_PMT_KEY','CS_BILL_OUTGOING_PMT_BASE','OUTGOING_PMT_KEY',NULL, 'FK_CBOPD_CBOPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_OUT_PMT','CS_BILL_OUTGOING_PMT_DELTA','PRDCR_PMT_KEY','CS_BILL_PROD_PMT_BASE','PRDCR_PMT_KEY',NULL, 'FK_CBOPD_CBPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_OUT_PMT','CS_BILL_OUTGOING_PMT_DELTA','DISB_KEY','CS_BILL_DISB_BASE','DISB_KEY',NULL, 'FK_CBOPD_CBDB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_OUT_PMT','CS_BILL_OUTGOING_PMT_DELTA','PMT_INSTRUMENT_KEY','CS_BILL_PMT_INSTR_BASE','PMT_INSTRUMENT_KEY',NULL, 'FK_CBOPD_CBPIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_PMT_INSTR','CS_BILL_PMT_INSTR_DELTA','PMT_INSTRUMENT_KEY','CS_BILL_PMT_INSTR_BASE','PMT_INSTRUMENT_KEY',NULL, 'FK_CBPID_CBPIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_PMT_INSTR','CS_BILL_PMT_INSTR_DELTA','BILL_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBPID_CBAB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_PMT_INSTR','CS_BILL_PMT_INSTR_DELTA','PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBPID_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_DISB','CS_BILL_DISB_DELTA','RPT_GROUP_KEY','CS_REPORT_GRP_BASE','RPT_GROUP_KEY',NULL, 'FK_CBDD_CRGB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_DISB','CS_BILL_DISB_DELTA','BILL_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBDD_CBAB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_DISB','CS_BILL_DISB_DELTA','DISTR_KEY','CS_BILL_DISTR_BASE','DISTR_KEY',NULL, 'FK_CBDD_CBDB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_DISB','CS_BILL_DISB_DELTA','PMT_INSTRUMENT_KEY','CS_BILL_PMT_INSTR_BASE','PMT_INSTRUMENT_KEY',NULL, 'FK_CBDD_CBPIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_DISB','CS_BILL_DISB_DELTA','PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBDD_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_DISB','CS_BILL_DISB_DELTA','COLTRL_KEY','CS_BILL_COLL_BASE','COLTRL_KEY',NULL, 'FK_CBDD_CBCB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_DISB','CS_BILL_DISB_DELTA','SUSP_PMT_KEY','CS_BILL_SUSP_PMT_BASE','SUSP_PMT_KEY',NULL, 'FK_CBDD_CBSPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_DISB','CS_BILL_DISB_DELTA','UNAPPLIED_FUND_KEY','CS_BILL_UNAPP_FUND_BASE','UNAPPLIED_FUND_KEY',NULL, 'FK_CBDD_CBUFB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_DISTR_ITEM','CS_BILL_DISTR_ITEM_DELTA','BILL_POL_PRD_KEY','CS_BILL_POL_PRD_BASE','BILL_POL_PRD_KEY',NULL, 'FK_CBDID_CBPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_DISTR_ITEM','CS_BILL_DISTR_ITEM_DELTA','REVERSED_DISTR_KEY','CS_BILL_DISTR_BASE','DISTR_KEY',NULL, 'FK_CBDID_CBDB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_INV','CS_BILL_INV_DELTA','BILL_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBID_CBAB5', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_MONEY_RCVD','CS_BILL_MONEY_RCVD_DELTA','MON_RCVD_KEY','CS_BILL_MONEY_RCVD_BASE','MON_RCVD_KEY',NULL, 'FK_CBMRD_CBMRB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_MONEY_RCVD','CS_BILL_MONEY_RCVD_DELTA','PAYING_PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBMRD_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_MONEY_RCVD','CS_BILL_MONEY_RCVD_DELTA','BILL_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBMRD_CBAB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_MONEY_RCVD','CS_BILL_MONEY_RCVD_DELTA','BILL_INV_KEY','CS_BILL_INV_BASE','BILL_INV_KEY',NULL, 'FK_CBMRD_CBIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_MONEY_RCVD','CS_BILL_MONEY_RCVD_DELTA','BILL_POL_PRD_KEY','CS_BILL_POL_PRD_BASE','BILL_POL_PRD_KEY',NULL, 'FK_CBMRD_CBPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_MONEY_RCVD','CS_BILL_MONEY_RCVD_DELTA','PMT_INSTRUMENT_KEY','CS_BILL_PMT_INSTR_BASE','PMT_INSTRUMENT_KEY',NULL, 'FK_CBMRD_CBPIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_MONEY_RCVD','CS_BILL_MONEY_RCVD_DELTA','RPT_GROUP_KEY','CS_REPORT_GRP_BASE','RPT_GROUP_KEY',NULL, 'FK_CBMRD_CRGB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_MONEY_RCVD','CS_BILL_MONEY_RCVD_DELTA','UNAPPLIED_FUND_KEY','CS_BILL_UNAPP_FUND_BASE','UNAPPLIED_FUND_KEY',NULL, 'FK_CBMRD_CBUFB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_MONEY_RCVD','CS_BILL_MONEY_RCVD_DELTA','PROMISING_PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBMRD_CBPB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_UNAPP_FUND','CS_BILL_UNAPP_FUND_DELTA','UNAPPLIED_FUND_KEY','CS_BILL_UNAPP_FUND_BASE','UNAPPLIED_FUND_KEY',NULL, 'FK_CBUFD_CBUFB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_UNAPP_FUND','CS_BILL_UNAPP_FUND_DELTA','BILL_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBUFD_CBAB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_UNAPP_FUND','CS_BILL_UNAPP_FUND_DELTA','RPT_GROUP_KEY','CS_REPORT_GRP_BASE','RPT_GROUP_KEY',NULL, 'FK_CBUFD_CRGB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_UNAPP_FUND','CS_BILL_UNAPP_FUND_DELTA','TACCT_KEY','CS_BILL_TACCT_BASE','TACCT_KEY',NULL, 'FK_CBUFD_CBTB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_COMM_REDUCT','CS_BILL_COMM_REDUCT_DELTA','CHRG_COMM_KEY','CS_BILL_CHRG_COMM_BASE','CHRG_COMM_KEY',NULL, 'FK_CBCRD_CBCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_COMM_REDUCT','CS_BILL_COMM_REDUCT_DELTA','BILL_INV_ITEM_KEY','CS_BILL_INV_ITEM_BASE','BILL_INV_ITEM_KEY',NULL, 'FK_CBCRD_CBIIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_COMM_REDUCT','CS_BILL_COMM_REDUCT_DELTA','WO_KEY','CS_BILL_WRITEOFF_BASE','WO_KEY',NULL, 'FK_CBCRD_CBWB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_COMM_REDUCT','CS_BILL_COMM_REDUCT_DELTA','COMM_RED_KEY','CS_BILL_COMM_REDUCT_BASE','COMM_RED_KEY',NULL, 'FK_CBCRD_CBCRB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_NR_DISTR_ITEM','CS_BILL_NR_DISTR_ITEM_DELTA','NON_RCVBL_DISTR_ITEM_KEY','CS_BILL_NR_DISTR_ITEM_BASE','NON_RCVBL_DISTR_ITEM_KEY',NULL, 'FK_CBNDID_CBNDIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_NR_DISTR_ITEM','CS_BILL_NR_DISTR_ITEM_DELTA','ACTIVE_DISTR_KEY','CS_BILL_DISTR_BASE','DISTR_KEY',NULL, 'FK_CBNDID_CBDB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_NR_DISTR_ITEM','CS_BILL_NR_DISTR_ITEM_DELTA','REVERSED_DISTR_KEY','CS_BILL_DISTR_BASE','DISTR_KEY',NULL, 'FK_CBNDID_CBDB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_NR_DISTR_ITEM','CS_BILL_NR_DISTR_ITEM_DELTA','BILL_POL_PRD_KEY','CS_BILL_POL_PRD_BASE','BILL_POL_PRD_KEY',NULL, 'FK_CBNDID_CBPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_FUNDS_TRNFR','CS_BILL_FUNDS_TRNFR_DELTA','FUNDS_TFR_KEY','CS_BILL_FUNDS_TRNFR_BASE','FUNDS_TFR_KEY',NULL, 'FK_CBFTD_CBFTB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_FUNDS_TRNFR','CS_BILL_FUNDS_TRNFR_DELTA','SRCE_PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBFTD_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_FUNDS_TRNFR','CS_BILL_FUNDS_TRNFR_DELTA','TARGET_PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBFTD_CBPB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_FUNDS_TRNFR','CS_BILL_FUNDS_TRNFR_DELTA','SRCE_UNAPPLIED_FUND_KEY','CS_BILL_UNAPP_FUND_BASE','UNAPPLIED_FUND_KEY',NULL, 'FK_CBFTD_CBUFB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_FUNDS_TRNFR','CS_BILL_FUNDS_TRNFR_DELTA','TARGET_UNAPPLIED_FUND_KEY','CS_BILL_UNAPP_FUND_BASE','UNAPPLIED_FUND_KEY',NULL, 'FK_CBFTD_CBUFB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CREDIT','CS_BILL_CREDIT_DELTA','CR_KEY','CS_BILL_CREDIT_BASE','CR_KEY',NULL, 'FK_CBCD_CBCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CREDIT','CS_BILL_CREDIT_DELTA','BILL_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBCD_CBAB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CREDIT','CS_BILL_CREDIT_DELTA','UNAPPLIED_FUND_KEY','CS_BILL_UNAPP_FUND_BASE','UNAPPLIED_FUND_KEY',NULL, 'FK_CBCD_CBUFB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_SUSP_PMT','CS_BILL_SUSP_PMT_DELTA','SUSP_PMT_KEY','CS_BILL_SUSP_PMT_BASE','SUSP_PMT_KEY',NULL, 'FK_CBSPD_CBSPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CHRG','CS_BILL_CHRG_DELTA','PRIM_COMM_PRDCR_CD_KEY','CS_BILL_PROD_CD_BASE','PRDCR_CD_KEY',NULL, 'FK_CBCD_CBPCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CHRG','CS_BILL_CHRG_DELTA','INV_STREAM_KEY','CS_BILL_INV_STREAM_BASE','INV_STREAM_KEY',NULL, 'FK_CBCD_CBISB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_INV','CS_BILL_INV_DELTA','INV_STREAM_KEY','CS_BILL_INV_STREAM_BASE','INV_STREAM_KEY',NULL, 'FK_CBID_CBISB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_MONEY_RCVD','CS_BILL_MONEY_RCVD_DELTA','DISTR_KEY','CS_BILL_DISTR_BASE','DISTR_KEY',NULL, 'FK_CBMRD_CBDB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_NEG_WO','CS_BILL_NEG_WRITEOFF_DELTA','UNAPPLIED_FUND_KEY','CS_BILL_UNAPP_FUND_BASE','UNAPPLIED_FUND_KEY',NULL, 'FK_CBNWD_CBUFB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_NEG_WO','CS_BILL_NEG_WRITEOFF_DELTA','BILL_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBNWD_CBAB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_NEG_WO','CS_BILL_NEG_WRITEOFF_DELTA','PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBNWD_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_POL_PRD','CS_BILL_POL_PRD_DELTA','PMT_PLAN_KEY','CS_BILL_PLAN_BASE','PLAN_KEY',NULL, 'FK_CBPPD_CBPB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_POL_PRD','CS_BILL_POL_PRD_DELTA','DELNQNT_PLAN_KEY','CS_BILL_PLAN_BASE','PLAN_KEY',NULL, 'FK_CBPPD_CBPB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_POL_PRD','CS_BILL_POL_PRD_DELTA','RETURN_PREM_PLAN_KEY','CS_BILL_PLAN_BASE','PLAN_KEY',NULL, 'FK_CBPPD_CBPB3', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_POL_PRD','CS_BILL_POL_PRD_DELTA','SECURITY_ZONE_KEY','CS_BILL_SECURITY_ZONE_BASE','SECURITY_ZONE_KEY',NULL, 'FK_CBPPD_CBSZB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_ACCT','CS_BILL_ACCT_DELTA','SECURITY_ZONE_KEY','CS_BILL_SECURITY_ZONE_BASE','SECURITY_ZONE_KEY',NULL, 'FK_CBAD_CBSZB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_ACCT','CS_BILL_ACCT_DELTA','TACCT_CONTAINER_KEY','CS_BILL_TACCT_CONTAINER_BASE','TACCT_CONTAINER_KEY',NULL, 'FK_CBAD_CBTCB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_POL_PRD','CS_BILL_POL_PRD_DELTA','TACCT_CONTAINER_KEY','CS_BILL_TACCT_CONTAINER_BASE','TACCT_CONTAINER_KEY',NULL, 'FK_CBPPD_CBTCB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CHRG','CS_BILL_CHRG_DELTA','TACCT_CONTAINER_KEY','CS_BILL_TACCT_CONTAINER_BASE','TACCT_CONTAINER_KEY',NULL, 'FK_CBCD_CBTCB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CHRG_COMM','CS_BILL_CHRG_COMM_DELTA','TACCT_CONTAINER_KEY','CS_BILL_TACCT_CONTAINER_BASE','TACCT_CONTAINER_KEY',NULL, 'FK_CBCCD_CBTCB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_POL_COMM','CS_BILL_POL_COMM_DELTA','TACCT_CONTAINER_KEY','CS_BILL_TACCT_CONTAINER_BASE','TACCT_CONTAINER_KEY',NULL, 'FK_CBPCD_CBTCB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_PROD_CD','CS_BILL_PROD_CD_DELTA','TACCT_CONTAINER_KEY','CS_BILL_TACCT_CONTAINER_BASE','TACCT_CONTAINER_KEY',NULL, 'FK_CBPCD_CBTCB1', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_SUSP_PMT','CS_BILL_SUSP_PMT_DELTA','TACCT_CONTAINER_KEY','CS_BILL_TACCT_CONTAINER_BASE','TACCT_CONTAINER_KEY',NULL, 'FK_CBSPD_CBTCB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_WO','CS_BILL_WRITEOFF_DELTA','TACCT_CONTAINER_KEY','CS_BILL_TACCT_CONTAINER_BASE','TACCT_CONTAINER_KEY',NULL, 'FK_CBWD_CBTCB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_POL_COMM','CS_BILL_POL_COMM_DELTA','COMM_SPLAN_KEY','CS_BILL_COMM_SPLAN_BASE','COMM_SPLAN_KEY',NULL, 'FK_CBPCD_CBCSB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_PROD','CS_BILL_PROD_DELTA','TACCT_CONTAINER_KEY','CS_BILL_TACCT_CONTAINER_BASE','TACCT_CONTAINER_KEY',NULL, 'FK_CBPD_CBTCB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_PROD','CS_BILL_PROD_DELTA','SECURITY_ZONE_KEY','CS_BILL_SECURITY_ZONE_BASE','SECURITY_ZONE_KEY',NULL, 'FK_CBPD_CBSZB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_PROD','CS_BILL_PROD_DELTA','AGCY_BILL_PLAN_KEY','CS_BILL_PLAN_BASE','PLAN_KEY',NULL, 'FK_CBPD_CBPB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_PROD_CD','CS_BILL_PROD_CD_DELTA','COMM_PLAN_KEY','CS_BILL_PLAN_BASE','PLAN_KEY',NULL, 'FK_CBPCD_CBPB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_PROD_STMNT','CS_BILL_PROD_STMNT_DELTA','PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBPSD_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_SUSP_PMT','CS_BILL_SUSP_PMT_DELTA','BILL_ACCT_APPLIED_TO_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBSPD_CBAB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_SUSP_PMT','CS_BILL_SUSP_PMT_DELTA','BILL_POL_PRD_APPLIED_TO_KEY','CS_BILL_POL_PRD_BASE','BILL_POL_PRD_KEY',NULL, 'FK_CBSPD_CBPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_SUSP_PMT','CS_BILL_SUSP_PMT_DELTA','BILL_USER_APPLIED_BY_KEY','CS_BILL_USER_BASE','BILL_USER_KEY',NULL, 'FK_CBSPD_CBUB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_SUSP_PMT','CS_BILL_SUSP_PMT_DELTA','MON_RCVD_KEY','CS_BILL_MONEY_RCVD_BASE','MON_RCVD_KEY',NULL, 'FK_CBSPD_CBMRB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_SUSP_PMT','CS_BILL_SUSP_PMT_DELTA','RPT_GROUP_KEY','CS_REPORT_GRP_BASE','RPT_GROUP_KEY',NULL, 'FK_CBSPD_CRGB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_SUSP_PMT','CS_BILL_SUSP_PMT_DELTA','BILL_USER_REVERSED_BY_KEY','CS_BILL_USER_BASE','BILL_USER_KEY',NULL, 'FK_CBSPD_CBUB1', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_SUSP_PMT','CS_BILL_SUSP_PMT_DELTA','PMT_INSTRUMENT_KEY','CS_BILL_PMT_INSTR_BASE','PMT_INSTRUMENT_KEY',NULL, 'FK_CBSPD_CBPIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_SUSP_PMT','CS_BILL_SUSP_PMT_DELTA','PRDCR_APPLIED_TO_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBSPD_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_WO','CS_BILL_WRITEOFF_DELTA','COMM_AGCY_PMT_ITEM_KEY','CS_BILL_DISTR_ITEM_BASE','DISTR_ITEM_KEY',NULL, 'FK_CBWD_CBDIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_WO','CS_BILL_WRITEOFF_DELTA','GROSS_AGCY_PMT_ITEM_KEY','CS_BILL_DISTR_ITEM_BASE','DISTR_ITEM_KEY',NULL, 'FK_CBWD_CBDIB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_WO','CS_BILL_WRITEOFF_DELTA','CHRG_COMM_KEY','CS_BILL_CHRG_COMM_BASE','CHRG_COMM_KEY',NULL, 'FK_CBWD_CBCCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_WO','CS_BILL_WRITEOFF_DELTA','BILL_INV_ITEM_KEY','CS_BILL_INV_ITEM_BASE','BILL_INV_ITEM_KEY',NULL, 'FK_CBWD_CBIIB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_WO','CS_BILL_WRITEOFF_DELTA','ITEM_COMM_KEY','CS_BILL_ITEM_COMM_BASE','ITEM_COMM_KEY',NULL, 'FK_CBWD_CBICB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_WO','CS_BILL_WRITEOFF_DELTA','PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBWD_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_WO','CS_BILL_WRITEOFF_DELTA','REQUESTING_USER_KEY','CS_BILL_USER_BASE','BILL_USER_KEY',NULL, 'FK_CBWD_CBUB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_POL','CS_BILL_POL_DELTA','BILL_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBPD_CBAB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_DISB','CS_BILL_DISB_DELTA','REQUESTING_USER_KEY','CS_BILL_USER_BASE','BILL_USER_KEY',NULL, 'FK_CBDD_CBUB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_NEG_WO','CS_BILL_NEG_WRITEOFF_DELTA','REQUESTING_USER_KEY','CS_BILL_USER_BASE','BILL_USER_KEY',NULL, 'FK_CBNWD_CBUB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_FUNDS_TRNFR','CS_BILL_FUNDS_TRNFR_DELTA','REQUESTING_USER_KEY','CS_BILL_USER_BASE','BILL_USER_KEY',NULL, 'FK_CBFTD_CBUB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CREDIT','CS_BILL_CREDIT_DELTA','REQUESTING_USER_KEY','CS_BILL_USER_BASE','BILL_USER_KEY',NULL, 'FK_CBCD_CBUB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_AR_TRAN','CS_BILL_AR_TRAN','CHRG_COMM_KEY','CS_BILL_CHRG_COMM_BASE','CHRG_COMM_KEY',NULL, 'FK_CBAT_CBCCB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_GEN_TRAN','CS_BILL_GENERAL_TRAN','SRCE_UNAPPLIED_TACCT_KEY','CS_BILL_TACCT_BASE','TACCT_KEY',NULL, 'FK_CBGT_CBTB', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_GEN_TRAN','CS_BILL_GENERAL_TRAN','TARGET_UNAPPLIED_TACCT_KEY','CS_BILL_TACCT_BASE','TACCT_KEY',NULL, 'FK_CBGT_CBTB1', 'Y');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_POL','CS_BILL_POL_DELTA','BILL_POL_KEY','CS_BILL_POL_BASE','BILL_POL_KEY',NULL, 'FK_CBPD_CBPB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_POL_PRD','CS_BILL_POL_PRD_DELTA','BILL_POL_KEY','CS_BILL_POL_BASE','BILL_POL_KEY',NULL, 'FK_CBPPD_CBPB4', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_UNAPP_FUND','CS_BILL_UNAPP_FUND_DELTA','BILL_POL_KEY','CS_BILL_POL_BASE','BILL_POL_KEY',NULL, 'FK_CBUFD_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_GEN_TRAN','CS_BILL_GENERAL_TRAN','SUSP_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBGT_CBAB3', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_BILL_COMM_AGG','BILL_ACCT_KEY','CS_BILL_ACCT_BASE','BILL_ACCT_KEY',NULL, 'FK_CBCA_CBAB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_BILL_COMM_AGG','PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBCA_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_BILL_COMM_AGG','PRDCR_CD_KEY','CS_BILL_PROD_CD_BASE','PRDCR_CD_KEY',NULL, 'FK_CBCA_CBPCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_BILL_COMM_AGG','BILL_POL_PRD_KEY','CS_BILL_POL_PRD_BASE','BILL_POL_PRD_KEY',NULL, 'FK_CBCA_CBPPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_BILL_COMM_AGG','BILL_POL_KEY','CS_BILL_POL_BASE','BILL_POL_KEY',NULL, 'FK_CBCA_CBPB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_BILL_COMM_AGG','CHRG_KEY','CS_BILL_CHRG_BASE','CHRG_KEY',NULL, 'FK_CBCA_CBCB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_BILL_COMM_AGG','TRANS_MTH_ID','CS_MONTH_DIM','MTH_ID',NULL, 'FK_CBCA_CMDI', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_CHRG_PAT','CS_BILL_CHRG_PAT_DELTA','TACCT_OWNER_PAT_KEY','CS_BILL_TACCT_OWNER_PAT_BASE','TACCT_OWNER_PAT_KEY',NULL, 'FK_CBCPD_CBTOPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_TACCT_OWNER_PAT','CS_BILL_TACCT_OWNER_PAT_DELTA','TACCT_OWNER_PAT_KEY','CS_BILL_TACCT_OWNER_PAT_BASE','TACCT_OWNER_PAT_KEY',NULL, 'FK_CBTOPD_CBTOPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_PLAN','CS_BILL_PLAN_DELTA','PLAN_KEY','CS_BILL_PLAN_BASE','PLAN_KEY',NULL, 'FK_CBPD_CBPB3', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_COMM_SPLAN','CS_BILL_COMM_SPLAN_DELTA','COMM_SPLAN_KEY','CS_BILL_COMM_SPLAN_BASE','COMM_SPLAN_KEY',NULL, 'FK_CBCSD_CBCSB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_COMM_SPLAN_RATE','CS_BILL_COMM_SPLAN_RATE_DELTA','COMM_SPLAN_KEY','CS_BILL_COMM_SPLAN_BASE','COMM_SPLAN_KEY',NULL, 'FK_CBCSRD_CBCSB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_COMM_SPLAN_RATE','CS_BILL_COMM_SPLAN_RATE_DELTA','COMM_SPLAN_RATE_KEY','CS_BILL_COMM_SPLAN_RATE_BASE','COMM_SPLAN_RATE_KEY',NULL, 'FK_CBCSRD_CBCSRB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_BILL_INV_MONTH_AGG','CHRG_PAT_KEY','CS_BILL_CHRG_PAT_BASE','CHRG_PAT_KEY',NULL, 'FK_CBIMA_CBCPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_ACCT','CS_BILL_ACCT_DELTA','BILL_PLAN_KEY','CS_BILL_PLAN_BASE','PLAN_KEY',NULL, 'FK_CBAD_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_ACCT','CS_BILL_ACCT_DELTA','ALLOC_PLAN_KEY','CS_BILL_PLAN_BASE','PLAN_KEY',NULL, 'FK_CBAD_CBPB1', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_ACCT','CS_BILL_ACCT_DELTA','DELNQNT_PLAN_KEY','CS_BILL_PLAN_BASE','PLAN_KEY',NULL, 'FK_CBAD_CBPB2', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_COMM_SPLAN','CS_BILL_COMM_SPLAN_DELTA','PLAN_KEY','CS_BILL_PLAN_BASE','PLAN_KEY',NULL, 'FK_CBCSD_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_BILL_INV_MONTH_AGG','PRDCR_KEY','CS_BILL_PROD_BASE','PRDCR_KEY',NULL, 'FK_CBIMA_CBPB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES (' ','CS_BILL_COMM_AGG','COMM_SPLAN_RATE_KEY','CS_BILL_COMM_SPLAN_RATE_BASE','COMM_SPLAN_RATE_KEY',NULL, 'FK_CBCA_CBCSRB', 'N');
INSERT INTO MD_FK_REF (TRF_NAME, FK_TAB_NAME, FK_COL_NAME, PK_TAB_NAME, PK_COL_NAME, LOB_CD, FK_CONSTR_NAME, OOTB_FUTURE_USE_FL) VALUES ('V_TRF_BILL_ACCT','CS_BILL_ACCT_DELTA','COLLECTION_AGCY_KEY','CS_BILL_COLLECTION_AGENCY_BASE','COLLECTION_AGCY_KEY',NULL, 'FK_CBAD_CBCAB', 'Y');

go


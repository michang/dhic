/*==============================================================*/
/* Table: Z_DIM_INCIDENT                                          */
/*==============================================================*/
create table Z_DIM_INCIDENT (
   INCIDENT_ID             int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CLAIM_KEY            varchar(100)         not null,
   INCIDENT_KEY            varchar(100)         not null,
   INCIDENT_TYPE_KEY             varchar(255)         not null,
   BODY_LOC_CD          varchar(255)         not null,   
   BODY_LOC_DESC        varchar(255)         not null,
   NATURE_INJURY_CD     varchar(255)         not null,   
   NATURE_INJURY_DESC   varchar(255)         not null,
   ICD_CD			    varchar(255)         not null,   
   ICD_DESC				varchar(255)         not null,   
   RETIRED_FL			varchar(1)         not null,   
   CLAIM_INCIDENT_FL			varchar(1)         not null,   
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_LATE_ARRIVING_FL varchar(1)           not null,
   ETL_CURR_ROW_FL      varchar(1)           not null,
   ETL_ACTIVE_FL        varchar(1)           not null
)
go

alter table Z_DIM_INCIDENT
   add constraint DIIN_PK primary key nonclustered (INCIDENT_ID)
go


/* Adding INCIDENT_ID TO LOSS_TRAN AND  LOSS ITD */

ALTER TABLE FACT_LOSS_TRAN
	ADD  Z_INCIDENT_ID	int         null
go

create nonclustered index FLTR_DIIN_XFK on FACT_LOSS_TRAN (Z_INCIDENT_ID ASC)
go

alter table FACT_LOSS_TRAN
   add constraint FK_FLTR_DIIN foreign key (Z_INCIDENT_ID)
      references Z_DIM_INCIDENT (INCIDENT_ID)
go


ALTER TABLE FACT_LOSS_ITD
	ADD  Z_INCIDENT_ID	int         null
go

create nonclustered index FLIT_DIIN_XFK on FACT_LOSS_ITD (Z_INCIDENT_ID ASC)
go

alter table FACT_LOSS_ITD
   add constraint FK_FLIT_DIIN foreign key (Z_INCIDENT_ID)
      references Z_DIM_INCIDENT (INCIDENT_ID)
go






/*==============================================================*/
/* Table: DIM_CLAIM_LIABILITY_STATUS                                      */
/*==============================================================*/
create table Z_DIM_CLAIM_LIABILITY_STATUS (
   CLAIM_LIAB_STATUS_ID      int                  not null,
   CLAIM_LIAB_STATUS_KEY      VARCHAR(100)                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   STATUS_CD            varchar(255)         not null,
   	STATUS_CCD varchar(255) NOT NULL,
	STATUS_CST varchar(255) NOT NULL,
	STATUS_CLT varchar(255) NOT NULL,
   ETL_ACTIVE_FL        varchar(1)           not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table Z_DIM_CLAIM_LIABILITY_STATUS
   add constraint DCLST_PK primary key nonclustered (CLAIM_LIAB_STATUS_ID)
go

alter table Z_DIM_CLAIM_LIABILITY_STATUS
   add constraint DCLST_AK1 unique (CLAIM_LIAB_STATUS_KEY)
 go
 
 /*==============================================================*/
/* Table: FACT_CLAIM_LIABILITY_STATUS_TRAN                                */
/*==============================================================*/
create table Z_FACT_CLAIM_LIABILITY_STATUS_TRAN (
   CLAIM_LIAB_STATUS_TRANS_ID int                  not null,
   CLAIM_ID             int                  not null,
   CLAIM_LIAB_STATUS_ID      int                  not null,
   STATUS_PROC_DT_ID    int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   STATUS_PROC_DTS      DATETIME2            not null,
   ETL_ACTIVE_FL        varchar(1)           not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table Z_FACT_CLAIM_LIABILITY_STATUS_TRAN
   add constraint FCLST_PK primary key nonclustered (CLAIM_LIAB_STATUS_TRANS_ID)
go

/*==============================================================*/
/* Index: FCLST_DICL_XFK                                         */
/*==============================================================*/




create nonclustered index FCLST_DICL_XFK on Z_FACT_CLAIM_LIABILITY_STATUS_TRAN (CLAIM_ID ASC)
go

/*==============================================================*/
/* Index: FCLST_DCLST_XFK                                         */
/*==============================================================*/




create nonclustered index FCLST_DCLST_XFK on Z_FACT_CLAIM_LIABILITY_STATUS_TRAN (CLAIM_LIAB_STATUS_ID ASC)
go

/*==============================================================*/
/* Index: FCLST_DIDA_XFK                                         */
/*==============================================================*/




create nonclustered index FCLST_DIDA_XFK on Z_FACT_CLAIM_LIABILITY_STATUS_TRAN (STATUS_PROC_DT_ID ASC)
go


/*

alter table DIM_CLAIM
   add constraint FK_DICL_DCST foreign key (LAST_STATUS_ID)
      references DIM_CLAIM_STATUS (CLAIM_STATUS_ID)
go
*/
alter table Z_FACT_CLAIM_LIABILITY_STATUS_TRAN
   add constraint FK_FCLST_DCLST foreign key (CLAIM_LIAB_STATUS_ID)
      references Z_DIM_CLAIM_LIABILITY_STATUS (CLAIM_LIAB_STATUS_ID)
go

alter table Z_FACT_CLAIM_LIABILITY_STATUS_TRAN
   add constraint FK_FCLST_DICL foreign key (CLAIM_ID)
      references DIM_CLAIM (CLAIM_ID)
go

alter table Z_FACT_CLAIM_LIABILITY_STATUS_TRAN
   add constraint FK_FCLST_DIDA foreign key (STATUS_PROC_DT_ID)
      references DIM_DATE (DT_ID)
go



/*** ADD WIC CODE, WIC INDUSTRY -DIM ***/





/*** Enhance Claim  DIM, adding Last Liability Status Process Time and Liability Status  ***/

ALTER TABLE DIM_CLAIM
	ADD  Z_WIC_CD 	varchar(255)         null,
	  Z_WIC_INDUSTRY 	varchar(255)         null
go

ALTER TABLE DIM_CLAIM
	ADD  Z_LAST_LIAB_STATUS_ID 	int         null,
		  Z_LAST_LIAB_STATUS_PROC_DTS 	DATETIME2         null;
go



/****** Object:  View [dbo].[V_FACT_LOSS_TRAN]    Script Date: 5/23/2018 5:29:36 AM ******/
DROP VIEW [dbo].[V_FACT_LOSS_TRAN]
GO



CREATE VIEW [dbo].[V_FACT_LOSS_TRAN] AS
SELECT FLT.LOSS_TRANS_ID,
       FLT.ALT_LOSS_TRANS_ID,
       FLT.ORG_ID,
       FLT.PRODUCT_ID,
       FLT.POLICY_ID,
       FLT.LOC_ID,
       FLT.COVG_ID,
       FLT.CLAIM_ID,
       FLT.CLMNT_ID,
       FLT.CLAIM_FEATURE_ID,
       FLT.AGNT_ID,
       FLT.AGCY_ID,
       FLT.VENDOR_ID,
       FLT.REC_TYPE_ID,
       FLT.PRIM_NAMED_INSURED_ID,
       FLT.TRANS_PROC_DT_ID,
       FLT.ACCTG_PRD_ID,
       FLT.CHECK_DT_ID,
       FLT.ACCT_ID,
       FLT.TRANS_PROC_DTS,
       FLT.RISK_TYPE_CD,
       FLT.SOURCE_SYSTEM,
       FLT.TRANS_TYPE_CD,
       FLT.TRANS_CD,
       FLT.TRANS_SEQ,
       FLT.TRANS_ALLOC_CD,
       FLT.CHECK_NO,
       FLT.TRANS_AMT,
       FLT.RSRV_CHNG_AMT,
       FLT.CURR_CD,
       CASE WHEN FLT.TRANS_ALLOC_CD = 'INDEMNITY' THEN FLT.TRANS_AMT ELSE 0.0 END IND_PD_AMT,
       CASE WHEN FLT.TRANS_ALLOC_CD = 'MEDICAL' THEN FLT.TRANS_AMT ELSE 0.0 END MED_PD_AMT,
       CASE WHEN FLT.TRANS_ALLOC_CD = 'LAE EXPENSE' THEN FLT.TRANS_AMT ELSE 0.0 END ALAE_PD_AMT,
       CASE WHEN FLT.TRANS_ALLOC_CD = 'ULAE EXPENSE' THEN FLT.TRANS_AMT ELSE 0.0 END ULAE_PD_AMT,
       CASE WHEN FLT.TRANS_ALLOC_CD = 'SALVAGE' THEN FLT.TRANS_AMT ELSE 0.0 END SLVG_RCVRY_AMT,
       CASE WHEN FLT.TRANS_ALLOC_CD = 'SUBROGATION' THEN FLT.TRANS_AMT ELSE 0.0 END SUBRO_RCVRY_AMT,
       CASE WHEN FLT.TRANS_ALLOC_CD = 'DEDUCTIBLE' THEN FLT.TRANS_AMT ELSE 0.0 END DED_RCVRY_AMT,
       CASE WHEN FLT.TRANS_ALLOC_CD = 'INDEMNITY_RECOVERY' THEN FLT.TRANS_AMT ELSE 0.0 END IND_RCVRY_AMT,
       CASE WHEN FLT.TRANS_ALLOC_CD = 'EXPENSE_RECOVERY' THEN FLT.TRANS_AMT ELSE 0.0 END EXP_RCVRY_AMT,
       CASE WHEN FLT.TRANS_ALLOC_CD = 'INDEMNITY' THEN FLT.RSRV_CHNG_AMT ELSE 0.0 END IND_RSRV_CHNG_AMT,
       CASE WHEN FLT.TRANS_ALLOC_CD = 'MEDICAL' THEN FLT.RSRV_CHNG_AMT ELSE 0.0 END MED_RSRV_CHNG_AMT,
       CASE WHEN FLT.TRANS_ALLOC_CD = 'LAE EXPENSE' THEN FLT.RSRV_CHNG_AMT ELSE 0.0 END ALAE_RSRV_CHNG_AMT,
       CASE WHEN FLT.TRANS_ALLOC_CD = 'ULAE EXPENSE' THEN FLT.RSRV_CHNG_AMT ELSE 0.0 END ULAE_RSRV_CHNG_AMT,
       CASE WHEN FLT.TRANS_ALLOC_CD = 'SALVAGE' THEN FLT.RSRV_CHNG_AMT ELSE 0.0 END SLVG_RSRV_CHNG_AMT,
       CASE WHEN FLT.TRANS_ALLOC_CD = 'SUBROGATION' THEN FLT.RSRV_CHNG_AMT ELSE 0.0 END SUBRO_RSRV_CHNG_AMT,
       FLT.ETL_ACTIVE_FL,
       FLT.ETL_ADD_DTS,
       FLT.ETL_LAST_UPDATE_DTS,
	   FLT.Z_INCIDENT_ID
  FROM FACT_LOSS_TRAN FLT
;

GO




/****** Object:  View [dbo].[V_FACT_LOSS_ITD]    Script Date: 5/23/2018 5:30:54 AM ******/
DROP VIEW [dbo].[V_FACT_LOSS_ITD]
GO



CREATE VIEW [dbo].[V_FACT_LOSS_ITD] 
AS
SELECT LOSS_ITD_ID,
       ORG_ID,
       PRODUCT_ID,
       POLICY_ID,
       COVG_ID,
       CLAIM_ID,
       CLMNT_ID,
       CLAIM_FEATURE_ID,
       CLAIM_FEATURE_STATUS_ID,
       REC_TYPE_ID,
       ACCTG_PRD_ID,
       PRIM_NAMED_INSURED_ID,
       ACCT_ID,
       CLAIM_STATUS_ID,
       CURR_CD,
       OS_IND_RSRV_AMT,
       OS_MED_RSRV_AMT,
       OS_ALAE_RSRV_AMT,
       OS_ULAE_RSRV_AMT,
       OS_SLVG_RSRV_AMT,
       OS_SUBRO_RSRV_AMT,
       IND_PD_AMT,
       MED_PD_AMT,
       ALAE_PD_AMT,
       ULAE_PD_AMT,
       DED_RCVRY_AMT,
       SLVG_RCVRY_AMT,
       SUBRO_RCVRY_AMT,
       IND_RCVRY_AMT,
       EXP_RCVRY_AMT,
       ETL_ACTIVE_FL,
       ETL_ADD_DTS,
       ETL_LAST_UPDATE_DTS,
       OS_IND_RSRV_AMT + OS_MED_RSRV_AMT + OS_ALAE_RSRV_AMT + OS_ULAE_RSRV_AMT - OS_SLVG_RSRV_AMT - OS_SUBRO_RSRV_AMT OS_TOT_NET_RSRV_AMT,
       OS_IND_RSRV_AMT + OS_MED_RSRV_AMT + OS_ALAE_RSRV_AMT + OS_ULAE_RSRV_AMT OS_TOT_RSRV_AMT,
       OS_IND_RSRV_AMT + OS_MED_RSRV_AMT OS_TOT_LOSS_RSRV_AMT,
       OS_ALAE_RSRV_AMT + OS_ULAE_RSRV_AMT OS_TOT_EXP_RSRV_AMT,
       OS_SLVG_RSRV_AMT + OS_SUBRO_RSRV_AMT OS_TOT_RCVRY_RSRV_AMT,
       IND_PD_AMT + MED_PD_AMT + ALAE_PD_AMT + ULAE_PD_AMT - DED_RCVRY_AMT - SLVG_RCVRY_AMT - SUBRO_RCVRY_AMT - IND_RCVRY_AMT - EXP_RCVRY_AMT TOT_NET_PD_AMT,
       IND_PD_AMT + MED_PD_AMT + ALAE_PD_AMT + ULAE_PD_AMT TOT_PD_AMT,
       IND_PD_AMT + MED_PD_AMT TOT_LOSS_PD_AMT,
       IND_PD_AMT + MED_PD_AMT - DED_RCVRY_AMT - SLVG_RCVRY_AMT - SUBRO_RCVRY_AMT - IND_RCVRY_AMT TOT_LOSS_NET_PD_AMT,
       ALAE_PD_AMT + ULAE_PD_AMT TOT_EXP_PD_AMT,
       DED_RCVRY_AMT + SLVG_RCVRY_AMT + SUBRO_RCVRY_AMT + IND_RCVRY_AMT + EXP_RCVRY_AMT TOT_RCVRY_AMT,
       IND_PD_AMT + MED_PD_AMT + ALAE_PD_AMT + ULAE_PD_AMT - DED_RCVRY_AMT - SLVG_RCVRY_AMT - SUBRO_RCVRY_AMT - IND_RCVRY_AMT - EXP_RCVRY_AMT + OS_IND_RSRV_AMT + OS_MED_RSRV_AMT + OS_ALAE_RSRV_AMT + OS_ULAE_RSRV_AMT - OS_SLVG_RSRV_AMT - OS_SUBRO_RSRV_AMT TOT_NET_INCURRED_AMT,
       IND_PD_AMT + MED_PD_AMT + ALAE_PD_AMT + ULAE_PD_AMT + OS_IND_RSRV_AMT + OS_MED_RSRV_AMT + OS_ALAE_RSRV_AMT + OS_ULAE_RSRV_AMT TOT_INCURRED_AMT,
       IND_PD_AMT + MED_PD_AMT + OS_IND_RSRV_AMT + OS_MED_RSRV_AMT TOT_LOSS_INCURRED_AMT,
       IND_PD_AMT + MED_PD_AMT - DED_RCVRY_AMT - SLVG_RCVRY_AMT - SUBRO_RCVRY_AMT - IND_RCVRY_AMT + OS_IND_RSRV_AMT + OS_MED_RSRV_AMT - OS_SLVG_RSRV_AMT - OS_SUBRO_RSRV_AMT TOT_LOSS_NET_INCURRED_AMT,
       ALAE_PD_AMT + ULAE_PD_AMT + OS_ALAE_RSRV_AMT + OS_ULAE_RSRV_AMT TOT_EXP_INCURRED_AMT,
       SLVG_RCVRY_AMT + SUBRO_RCVRY_AMT + IND_RCVRY_AMT + EXP_RCVRY_AMT + OS_SLVG_RSRV_AMT + OS_SUBRO_RSRV_AMT TOT_RCVRY_INCURRED_AMT,
	   Z_INCIDENT_ID
  FROM FACT_LOSS_ITD
;

GO



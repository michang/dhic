/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2012                    */
/* Created on:     4/8/2016 12:17:14 PM                         */
/*==============================================================*/


/*==============================================================*/
/* Table: DIM_AGENCY                                            */
/*==============================================================*/
create table DIM_AGENCY (
   AGCY_ID              int                  not null,
   AGCY_KEY             varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   AGCY_CD              varchar(255)         not null,
   AGCY_NAME            varchar(255)         not null,
   AGCY_ADDR_1          varchar(255)         not null,
   AGCY_ADDR_2          varchar(255)         not null,
   AGCY_ADDR_3          varchar(255)         not null,
   AGCY_CITY            varchar(255)         not null,
   AGCY_STATE_CD        varchar(255)         not null,
   CONF_AGCY_STATE_CD   varchar(255)         not null,
   CONF_AGCY_STATE_SHORT_TEXT varchar(255)         not null,
   CONF_AGCY_STATE_LONG_TEXT varchar(255)         not null,
   AGCY_ZIP_CD          varchar(15)          not null,
   AGCY_PHONE_NO        varchar(50)          not null,
   AGCY_APPT_DT         DATE                 null,
   AGCY_TERM_DT         DATE                 null,
   AGCY_STATUS_CD       varchar(255)         not null,
   MKT_MANAGER_NAME     varchar(255)         not null,
   MKTG_REP_CD          varchar(255)         not null,
   REGION_CD            varchar(255)         not null,
   AGCY_LIC_NO          varchar(255)         not null,
   AGCY_TAX_ID          varchar(15)          not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_AGENCY
   add constraint DIAG1_PK primary key nonclustered (AGCY_ID)
go

/*==============================================================*/
/* Table: DIM_AGENT                                             */
/*==============================================================*/
create table DIM_AGENT (
   AGNT_ID              int                  not null,
   AGNT_KEY             varchar(100)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   AGNT_NAME            varchar(255)         not null,
   AGNT_CD              varchar(255)         not null,
   AGNT_ADDR_1          varchar(255)         not null,
   AGNT_ADDR_2          varchar(255)         not null,
   AGNT_ADDR_3          varchar(255)         not null,
   AGNT_CITY            varchar(255)         not null,
   AGNT_STATE_CD        varchar(255)         not null,
   CONF_AGNT_STATE_CD   varchar(255)         not null,
   CONF_AGNT_STATE_SHORT_TEXT varchar(255)         not null,
   CONF_AGNT_STATE_LONG_TEXT varchar(255)         not null,
   AGNT_ZIP_CD          varchar(15)          not null,
   AGNT_PHONE_NO        varchar(50)          not null,
   AGNT_APPT_DT         DATE                 null,
   AGNT_TERM_DT         DATE                 null,
   AGNT_STATUS_CD       varchar(255)         not null,
   MKT_MANAGER_NAME     varchar(255)         not null,
   MKTG_REP_CD          varchar(255)         not null,
   REGION_CD            varchar(255)         not null,
   AGNT_LIC_NO          varchar(255)         not null,
   AGNT_TAX_ID          varchar(15)          not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_AGENT
   add constraint DIAG_PK primary key nonclustered (AGNT_ID)
go

/*==============================================================*/
/* Table: DIM_BILL_ACCT                                         */
/*==============================================================*/
create table DIM_BILL_ACCT (
   BILL_ACCT_DID        int                  not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_PLAN_KEY        varchar(100)         not null,
   ALLOC_PLAN_KEY       varchar(100)         not null,
   DELNQNT_PLAN_KEY     varchar(100)         not null,
   COLLECTION_AGCY_KEY  varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   SECURITY_ZONE_KEY    varchar(100)         not null,
   CLOSE_DTS            DATETIME2            null,
   EOW_INV_ANCHOR_DT    DATE                 null,
   ACCT_NO              varchar(255)         not null,
   FIRST_TPM_INV_DOM    int                  not null,
   SECOND_TPM_INV_DOM   int                  not null,
   INV_DAY_OF_MTH       int                  not null,
   INV_DAY_OF_WEEK      int                  not null,
   INV_DELIVERY_TYPE_CD varchar(255)         not null,
   INV_DELIVERY_TYPE_CCD varchar(255)         not null,
   INV_DELIVERY_TYPE_CST varchar(255)         not null,
   INV_DELIVERY_TYPE_CLT varchar(255)         not null,
   FEIN_CD              varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   DELNQNT_STATUS_CD    varchar(255)         not null,
   DELNQNT_STATUS_CCD   varchar(255)         not null,
   DELNQNT_STATUS_CST   varchar(255)         not null,
   DELNQNT_STATUS_CLT   varchar(255)         not null,
   ACCT_TYPE_CD         varchar(255)         not null,
   ACCT_TYPE_CCD        varchar(255)         not null,
   ACCT_TYPE_CST        varchar(255)         not null,
   ACCT_TYPE_CLT        varchar(255)         not null,
   DISTR_LMT_TYPE_CD    varchar(255)         not null,
   DISTR_LMT_TYPE_CCD   varchar(255)         not null,
   DISTR_LMT_TYPE_CST   varchar(255)         not null,
   DISTR_LMT_TYPE_CLT   varchar(255)         not null,
   BILL_DT_TYPE_CD      varchar(255)         not null,
   BILL_DT_TYPE_CCD     varchar(255)         not null,
   BILL_DT_TYPE_CST     varchar(255)         not null,
   BILL_DT_TYPE_CLT     varchar(255)         not null,
   BILL_LEVEL_CD        varchar(255)         not null,
   BILL_LEVEL_CCD       varchar(255)         not null,
   BILL_LEVEL_CST       varchar(255)         not null,
   BILL_LEVEL_CLT       varchar(255)         not null,
   ORG_TYPE_CD          varchar(255)         not null,
   ACCT_SEGMENT_CD      varchar(255)         not null,
   ACCT_SEGMENT_CCD     varchar(255)         not null,
   ACCT_SEGMENT_CST     varchar(255)         not null,
   ACCT_SEGMENT_CLT     varchar(255)         not null,
   ACCT_NAME            varchar(255)         not null,
   ACCT_DBA_NAME        varchar(255)         not null,
   SRVC_TIER_CD         varchar(255)         not null,
   SRVC_TIER_CCD        varchar(255)         not null,
   SRVC_TIER_CST        varchar(255)         not null,
   SRVC_TIER_CLT        varchar(255)         not null,
   NP_PMT_DISTRB_FL     char(1)              not null,
   COLLECTING_FL        char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_ACCT
   add constraint DBAC_PK primary key nonclustered (BILL_ACCT_DID)
go

alter table DIM_BILL_ACCT
   add constraint DBAC_AK1 unique (BILL_ACCT_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_CHRG                                         */
/*==============================================================*/
create table DIM_BILL_CHRG (
   CHRG_DID             int                  not null,
   CHRG_KEY             varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CHRG_PAT_KEY         varchar(100)         not null,
   BILL_INSTR_KEY       varchar(100)         not null,
   PRIM_COMM_PRDCR_CD_KEY varchar(100)         not null,
   INV_STREAM_KEY       varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   CHRG_DTS             DATETIME2            not null,
   WRITTEN_DTS          DATETIME2            null,
   HOLD_RELEASE_DTS     DATETIME2            null,
   HOLD_STATUS_CD       varchar(255)         not null,
   HOLD_STATUS_CCD      varchar(255)         not null,
   HOLD_STATUS_CST      varchar(255)         not null,
   HOLD_STATUS_CLT      varchar(255)         not null,
   TOT_INSTALLMENT_NO   int                  not null,
   CHRG_GROUP_CD        varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   CHRG_AMT             numeric(18,2)        not null,
   REVERSED_FL          char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_CHRG
   add constraint DBCH_PK primary key nonclustered (CHRG_DID)
go

alter table DIM_BILL_CHRG
   add constraint DBCH_AK1 unique (CHRG_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_CHRG_COMM                                    */
/*==============================================================*/
create table DIM_BILL_CHRG_COMM (
   CHRG_COMM_DID        int                  not null,
   CHRG_COMM_KEY        varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   POL_COMM_KEY         varchar(100)         not null,
   CHRG_KEY             varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   PYBL_CRITERIA_CD     varchar(255)         not null,
   PYBL_CRITERIA_CCD    varchar(255)         not null,
   PYBL_CRITERIA_CST    varchar(255)         not null,
   PYBL_CRITERIA_CLT    varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_CHRG_COMM
   add constraint DBCC_PK primary key nonclustered (CHRG_COMM_DID)
go

alter table DIM_BILL_CHRG_COMM
   add constraint DBCC_AK1 unique (CHRG_COMM_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_CHRG_PAT                                     */
/*==============================================================*/
create table DIM_BILL_CHRG_PAT (
   CHRG_PAT_DID         int                  not null,
   CHRG_PAT_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CHRG_PAT_TYPE_CD     varchar(255)         not null,
   CHRG_PAT_TYPE_CCD    varchar(255)         not null,
   CHRG_PAT_TYPE_CST    varchar(255)         not null,
   CHRG_PAT_TYPE_CLT    varchar(255)         not null,
   CHRG_CD              varchar(255)         not null,
   CHRG_NAME            varchar(255)         not null,
   CHRG_CATG_CD         varchar(255)         not null,
   CHRG_CATG_CCD        varchar(255)         not null,
   CHRG_CATG_CST        varchar(255)         not null,
   CHRG_CATG_CLT        varchar(255)         not null,
   PERIODICITY_CD       varchar(255)         not null,
   PERIODICITY_CCD      varchar(255)         not null,
   PERIODICITY_CST      varchar(255)         not null,
   PERIODICITY_CLT      varchar(255)         not null,
   PRIORITY_CD          varchar(255)         not null,
   PRIORITY_CCD         varchar(255)         not null,
   PRIORITY_CST         varchar(255)         not null,
   PRIORITY_CLT         varchar(255)         not null,
   INV_TRTMNT_CD        varchar(255)         not null,
   INV_TRTMNT_CCD       varchar(255)         not null,
   INV_TRTMNT_CST       varchar(255)         not null,
   INV_TRTMNT_CLT       varchar(255)         not null,
   TACCT_OWNER_NAME     varchar(255)         not null,
   INC_IN_EQUITY_DATING_FL char(1)              not null,
   RETIRED_FL           char(1)              not null,
   REVERSIBLE_FL        char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_CHRG_PAT
   add constraint DBCP_PK primary key nonclustered (CHRG_PAT_DID)
go

alter table DIM_BILL_CHRG_PAT
   add constraint DBCP_AK1 unique (CHRG_PAT_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_COMM_PLAN                                    */
/*==============================================================*/
create table DIM_BILL_COMM_PLAN (
   COMM_PLAN_DID        int                  not null,
   COMM_PLAN_KEY        varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PLAN_EFF_DTS         DATETIME2            not null,
   PLAN_EXP_DTS         DATETIME2            null,
   PLAN_NAME            varchar(255)         not null,
   ROLE_CD              varchar(255)         not null,
   SPLAN_NAME           varchar(255)         not null,
   COMM_RATE            numeric(6,3)         not null,
   PLAN_TEXT            varchar(255)         not null,
   PLAN_ORDER           int                  not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_COMM_PLAN
   add constraint DBCP1_PK primary key nonclustered (COMM_PLAN_DID)
go

alter table DIM_BILL_COMM_PLAN
   add constraint DBCP1_AK1 unique (COMM_PLAN_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_COMM_REDUCT                                  */
/*==============================================================*/
create table DIM_BILL_COMM_REDUCT (
   COMM_RED_DID         int                  not null,
   COMM_RED_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   WO_KEY               varchar(100)         not null,
   BILL_INV_ITEM_KEY    varchar(100)         not null,
   CHRG_COMM_KEY        varchar(100)         not null,
   WRITE_OFF_DTS        DATETIME2            null,
   COMM_RED_TYPE_CD     varchar(255)         not null,
   COMM_RED_TYPE_CCD    varchar(255)         not null,
   COMM_RED_TYPE_CST    varchar(255)         not null,
   COMM_RED_TYPE_CLT    varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   COMM_RED_AMT         numeric(18,2)        not null,
   REVERSED_FL          char(1)              not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_COMM_REDUCT
   add constraint DBCR_PK primary key nonclustered (COMM_RED_DID)
go

alter table DIM_BILL_COMM_REDUCT
   add constraint DBCR_AK1 unique (COMM_RED_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_CREDIT                                       */
/*==============================================================*/
create table DIM_BILL_CREDIT (
   CR_DID               int                  not null,
   CR_KEY               varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   UNAPPLIED_FUND_KEY   varchar(100)         not null,
   REQUESTING_USER_KEY  varchar(100)         not null,
   CR_DTS               DATETIME2            null,
   APPRVL_DTS           DATETIME2            null,
   APPRVL_STATUS_CD     varchar(255)         not null,
   APPRVL_STATUS_CCD    varchar(255)         not null,
   APPRVL_STATUS_CST    varchar(255)         not null,
   APPRVL_STATUS_CLT    varchar(255)         not null,
   CR_TYPE_CD           varchar(255)         not null,
   CR_TYPE_CCD          varchar(255)         not null,
   CR_TYPE_CST          varchar(255)         not null,
   CR_TYPE_CLT          varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   CR_AMT               numeric(18,2)        not null,
   RETIRED_FL           char(1)              not null,
   REVERSED_FL          char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_CREDIT
   add constraint DBCR1_PK primary key nonclustered (CR_DID)
go

alter table DIM_BILL_CREDIT
   add constraint DBCR1_AK1 unique (CR_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_DISB                                         */
/*==============================================================*/
create table DIM_BILL_DISB (
   DISB_DID             int                  not null,
   DISB_KEY             varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   RPT_GROUP_KEY        varchar(100)         not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   PRDCR_KEY            varchar(100)         not null,
   DISTR_KEY            varchar(100)         not null,
   PMT_INSTRUMENT_KEY   varchar(100)         not null,
   COLTRL_KEY           varchar(100)         not null,
   SUSP_PMT_KEY         varchar(100)         not null,
   UNAPPLIED_FUND_KEY   varchar(100)         not null,
   REQUESTING_USER_KEY  varchar(100)         not null,
   ADDR_TEXT            varchar(1333)        not null,
   CURR_CD              varchar(255)         not null,
   DISB_AMT             numeric(18,2)        not null,
   DUE_DT               DATE                 not null,
   APPRVL_DTS           DATETIME2            null,
   CLOSE_DTS            DATETIME2            null,
   APPRVL_STATUS_CD     varchar(255)         not null,
   APPRVL_STATUS_CCD    varchar(255)         not null,
   APPRVL_STATUS_CST    varchar(255)         not null,
   APPRVL_STATUS_CLT    varchar(255)         not null,
   DISB_NO              varchar(255)         not null,
   MAIL_TO_NAME         varchar(255)         not null,
   MEMO_TEXT            varchar(255)         not null,
   PAY_TO_NAME          varchar(255)         not null,
   REASON_CD            varchar(255)         not null,
   REASON_CCD           varchar(255)         not null,
   REASON_CST           varchar(255)         not null,
   REASON_CLT           varchar(255)         not null,
   REF_NO               varchar(255)         not null,
   STATUS_CD            varchar(255)         not null,
   STATUS_CCD           varchar(255)         not null,
   STATUS_CST           varchar(255)         not null,
   STATUS_CLT           varchar(255)         not null,
   DISB_TYPE_CD         varchar(255)         not null,
   DISB_TYPE_CCD        varchar(255)         not null,
   DISB_TYPE_CST        varchar(255)         not null,
   DISB_TYPE_CLT        varchar(255)         not null,
   VOID_REASON_CD       varchar(255)         not null,
   VOID_REASON_CCD      varchar(255)         not null,
   VOID_REASON_CST      varchar(255)         not null,
   VOID_REASON_CLT      varchar(255)         not null,
   PMT_METHOD_CD        varchar(255)         not null,
   PMT_METHOD_CCD       varchar(255)         not null,
   PMT_METHOD_CST       varchar(255)         not null,
   PMT_METHOD_CLT       varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_DISB
   add constraint DBDI2_PK primary key nonclustered (DISB_DID)
go

alter table DIM_BILL_DISB
   add constraint DBDI2_AK1 unique (DISB_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_DISTR                                        */
/*==============================================================*/
create table DIM_BILL_DISTR (
   DISTR_DID            int                  not null,
   DISTR_KEY            varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   APPLIED_DTS          DATETIME2            null,
   DISTRIBUTED_DTS      DATETIME2            null,
   REVERSAL_DTS         DATETIME2            null,
   DISTR_TYPE_CD        varchar(255)         not null,
   DISTR_TYPE_CCD       varchar(255)         not null,
   DISTR_TYPE_CST       varchar(255)         not null,
   DISTR_TYPE_CLT       varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   WO_AMT               numeric(18,2)        not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_DISTR
   add constraint DBDI1_PK primary key nonclustered (DISTR_DID)
go

alter table DIM_BILL_DISTR
   add constraint DBDI1_AK1 unique (DISTR_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_DISTR_ITEM                                   */
/*==============================================================*/
create table DIM_BILL_DISTR_ITEM (
   DISTR_ITEM_DID       int                  not null,
   DISTR_ITEM_KEY       varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_INV_ITEM_KEY    varchar(100)         not null,
   PRDCR_CD_KEY         varchar(100)         not null,
   ACTIVE_DISTR_KEY     varchar(100)         not null,
   REVERSED_DISTR_KEY   varchar(100)         not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   EXECUTED_DTS         DATETIME2            null,
   APPLIED_DTS          DATETIME2            null,
   REVERSED_DTS         DATETIME2            null,
   DISTR_ITEM_TYPE_CD   varchar(255)         not null,
   DISTR_ITEM_TYPE_CCD  varchar(255)         not null,
   DISTR_ITEM_TYPE_CST  varchar(255)         not null,
   DISTR_ITEM_TYPE_CLT  varchar(255)         not null,
   DISPOSITION_CD       varchar(255)         not null,
   DISPOSITION_CCD      varchar(255)         not null,
   DISPOSITION_CST      varchar(255)         not null,
   DISPOSITION_CLT      varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   GROSS_APPLY_AMT      numeric(18,2)        not null,
   COMM_APPLY_AMT       numeric(18,2)        not null,
   PMT_COMMENTS         varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_DISTR_ITEM
   add constraint DBDI_PK primary key nonclustered (DISTR_ITEM_DID)
go

alter table DIM_BILL_DISTR_ITEM
   add constraint DBDI_AK1 unique (DISTR_ITEM_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_FUNDS_TRNFR                                  */
/*==============================================================*/
create table DIM_BILL_FUNDS_TRNFR (
   FUNDS_TFR_DID        int                  not null,
   FUNDS_TFR_KEY        varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   SRCE_PRDCR_KEY       varchar(100)         not null,
   TARGET_PRDCR_KEY     varchar(100)         not null,
   SRCE_UNAPPLIED_FUND_KEY varchar(100)         not null,
   TARGET_UNAPPLIED_FUND_KEY varchar(100)         not null,
   REQUESTING_USER_KEY  varchar(100)         not null,
   TFR_DTS              DATETIME2            null,
   APPRVL_DTS           DATETIME2            null,
   APPRVL_STATUS_CD     varchar(255)         not null,
   APPRVL_STATUS_CCD    varchar(255)         not null,
   APPRVL_STATUS_CST    varchar(255)         not null,
   APPRVL_STATUS_CLT    varchar(255)         not null,
   REASON_CD            varchar(255)         not null,
   REASON_CCD           varchar(255)         not null,
   REASON_CST           varchar(255)         not null,
   REASON_CLT           varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TFR_AMT              numeric(18,2)        not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_FUNDS_TRNFR
   add constraint DBFT_PK primary key nonclustered (FUNDS_TFR_DID)
go

alter table DIM_BILL_FUNDS_TRNFR
   add constraint DBFT_AK1 unique (FUNDS_TFR_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_INV                                          */
/*==============================================================*/
create table DIM_BILL_INV (
   BILL_INV_DID         int                  not null,
   BILL_INV_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   INV_STREAM_KEY       varchar(100)         not null,
   EVENT_DTS            DATETIME2            not null,
   PMT_DUE_DTS          DATETIME2            not null,
   INV_NO               varchar(255)         not null,
   RESEND_NO            varchar(255)         not null,
   INV_TYPE_CD          varchar(255)         not null,
   INV_TYPE_CCD         varchar(255)         not null,
   INV_TYPE_CST         varchar(255)         not null,
   INV_TYPE_CLT         varchar(255)         not null,
   INV_STATUS_CD        varchar(255)         not null,
   INV_STATUS_CCD       varchar(255)         not null,
   INV_STATUS_CST       varchar(255)         not null,
   INV_STATUS_CLT       varchar(255)         not null,
   INV_TEXT             varchar(1333)        not null,
   CURR_CD              varchar(255)         not null,
   INV_AMT              numeric(18,2)        not null,
   AD_HOC_FL            char(1)              not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_INV
   add constraint DBIN_PK primary key nonclustered (BILL_INV_DID)
go

alter table DIM_BILL_INV
   add constraint DBIN_AK1 unique (BILL_INV_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_INV_ITEM                                     */
/*==============================================================*/
create table DIM_BILL_INV_ITEM (
   BILL_INV_ITEM_DID    int                  not null,
   BILL_INV_ITEM_KEY    varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_INV_KEY         varchar(100)         not null,
   CHRG_KEY             varchar(100)         not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   EVENT_DTS            DATETIME2            not null,
   PMT_EXCEPT_DTS       DATETIME2            null,
   PROMISE_EXCEPT_DTS   DATETIME2            null,
   INSTALLMENT_NO       varchar(255)         not null,
   LINE_ITEM_NO         varchar(255)         not null,
   INV_ITEM_TYPE_CD     varchar(255)         not null,
   INV_ITEM_TYPE_CCD    varchar(255)         not null,
   INV_ITEM_TYPE_CST    varchar(255)         not null,
   INV_ITEM_TYPE_CLT    varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   INV_ITEM_AMT         numeric(18,2)        not null,
   COMMENTS             varchar(1333)        not null,
   INV_ITEM_TEXT        varchar(1333)        not null,
   CSTM_PMT_GROUP_TEXT  varchar(1333)        null,
   EXCEPT_CMT           varchar(1333)        not null,
   GROSS_SETTLED_FL     char(1)              not null,
   RETIRED_FL           char(1)              not null,
   REVERSED_FL          char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_INV_ITEM
   add constraint DBII_PK primary key nonclustered (BILL_INV_ITEM_DID)
go

alter table DIM_BILL_INV_ITEM
   add constraint DBII_AK1 unique (BILL_INV_ITEM_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_MONEY_RCVD                                   */
/*==============================================================*/
create table DIM_BILL_MONEY_RCVD (
   MON_RCVD_DID         int                  not null,
   MON_RCVD_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   PAYING_PRDCR_KEY     varchar(100)         not null,
   PROMISING_PRDCR_KEY  varchar(100)         not null,
   BILL_INV_KEY         varchar(100)         not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   PMT_INSTRUMENT_KEY   varchar(100)         not null,
   RPT_GROUP_KEY        varchar(100)         not null,
   UNAPPLIED_FUND_KEY   varchar(100)         not null,
   DISTR_KEY            varchar(100)         not null,
   RCVD_AMT             numeric(18,2)        not null,
   APPLIED_DTS          DATETIME2            null,
   RCVD_DTS             DATETIME2            null,
   REVERSAL_DTS         DATETIME2            null,
   MON_RCVD_TEXT        varchar(1333)        not null,
   NAME                 varchar(255)         not null,
   REF_NO               varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   REVERSAL_REASON_CD   varchar(255)         not null,
   REVERSAL_REASON_CCD  varchar(255)         not null,
   REVERSAL_REASON_CST  varchar(255)         not null,
   REVERSAL_REASON_CLT  varchar(255)         not null,
   MON_RCVD_TYPE_CD     varchar(255)         not null,
   MON_RCVD_TYPE_CCD    varchar(255)         not null,
   MON_RCVD_TYPE_CST    varchar(255)         not null,
   MON_RCVD_TYPE_CLT    varchar(255)         not null,
   PMT_METHOD_CD        varchar(255)         not null,
   PMT_METHOD_CCD       varchar(255)         not null,
   PMT_METHOD_CST       varchar(255)         not null,
   PMT_METHOD_CLT       varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_MONEY_RCVD
   add constraint DBMR_PK primary key nonclustered (MON_RCVD_DID)
go

alter table DIM_BILL_MONEY_RCVD
   add constraint DBMR_AK1 unique (MON_RCVD_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_NEG_WRITEOFF                                 */
/*==============================================================*/
create table DIM_BILL_NEG_WRITEOFF (
   NEG_WO_DID           int                  not null,
   NEG_WO_KEY           varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   UNAPPLIED_FUND_KEY   varchar(100)         not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   PRDCR_KEY            varchar(100)         not null,
   REQUESTING_USER_KEY  varchar(100)         not null,
   EXECUTION_DTS        DATETIME2            null,
   APPRVL_DTS           DATETIME2            null,
   APPRVL_STATUS_CD     varchar(255)         not null,
   APPRVL_STATUS_CCD    varchar(255)         not null,
   APPRVL_STATUS_CST    varchar(255)         not null,
   APPRVL_STATUS_CLT    varchar(255)         not null,
   WO_CHANNEL_CD        varchar(255)         not null,
   WO_CHANNEL_CCD       varchar(255)         not null,
   WO_CHANNEL_CST       varchar(255)         not null,
   WO_CHANNEL_CLT       varchar(255)         not null,
   NEG_WO_TYPE_CD       varchar(255)         not null,
   NEG_WO_TYPE_CCD      varchar(255)         not null,
   NEG_WO_TYPE_CST      varchar(255)         not null,
   NEG_WO_TYPE_CLT      varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   WO_AMT               numeric(18,2)        not null,
   REVERSED_FL          char(1)              not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_NEG_WRITEOFF
   add constraint DBNW_PK primary key nonclustered (NEG_WO_DID)
go

alter table DIM_BILL_NEG_WRITEOFF
   add constraint DBNW_AK1 unique (NEG_WO_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_NR_DISTR_ITEM                                */
/*==============================================================*/
create table DIM_BILL_NR_DISTR_ITEM (
   NON_RCVBL_DISTR_ITEM_DID int                  not null,
   NON_RCVBL_DISTR_ITEM_KEY varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ACTIVE_DISTR_KEY     varchar(100)         not null,
   REVERSED_DISTR_KEY   varchar(100)         not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   CURR_CD              varchar(255)         not null,
   COMM_APPLY_AMT       numeric(18,2)        not null,
   GROSS_APPLY_AMT      numeric(18,2)        not null,
   NET_APPLY_AMT        numeric(18,2)        not null,
   EXECUTED_DTS         DATETIME2            null,
   REVERSED_DTS         DATETIME2            null,
   RELEASED_DTS         DATETIME2            null,
   PMT_CMT_TEXT         varchar(255)         not null,
   POL_NO               varchar(255)         not null,
   ITEM_TYPE_CD         varchar(255)         not null,
   ITEM_TYPE_CCD        varchar(255)         not null,
   ITEM_TYPE_CST        varchar(255)         not null,
   ITEM_TYPE_CLT        varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_NR_DISTR_ITEM
   add constraint DBNDI_PK primary key nonclustered (NON_RCVBL_DISTR_ITEM_DID)
go

alter table DIM_BILL_NR_DISTR_ITEM
   add constraint DBNDI_AK1 unique (NON_RCVBL_DISTR_ITEM_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_POL                                          */
/*==============================================================*/
create table DIM_BILL_POL (
   BILL_POL_DID         int                  not null,
   BILL_POL_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_ACCT_KEY        varchar(100)         null,
   LOB_CD               varchar(255)         not null,
   LOB_CCD              varchar(255)         not null,
   LOB_CST              varchar(255)         not null,
   LOB_CLT              varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_POL
   add constraint DBPO_PK primary key nonclustered (BILL_POL_DID)
go

alter table DIM_BILL_POL
   add constraint DBPO_AK1 unique (BILL_POL_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_POL_PER                                      */
/*==============================================================*/
create table DIM_BILL_POL_PER (
   BILL_POL_PRD_DID     int                  not null,
   BILL_POL_PRD_KEY     varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PMT_PLAN_KEY         varchar(100)         not null,
   DELNQNT_PLAN_KEY     varchar(100)         not null,
   RETURN_PREM_PLAN_KEY varchar(100)         not null,
   SECURITY_ZONE_KEY    varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   JURS_STATE_CD        varchar(255)         not null,
   JURS_STATE_CCD       varchar(255)         not null,
   JURS_STATE_CST       varchar(255)         not null,
   JURS_STATE_CLT       varchar(255)         not null,
   UW_CO_CD             varchar(255)         not null,
   UW_CO_CCD            varchar(255)         not null,
   UW_CO_CST            varchar(255)         not null,
   UW_CO_CLT            varchar(255)         not null,
   BILL_METHOD_CD       varchar(255)         not null,
   BILL_METHOD_CCD      varchar(255)         not null,
   BILL_METHOD_CST      varchar(255)         not null,
   BILL_METHOD_CLT      varchar(255)         not null,
   BOUND_DTS            DATETIME2            null,
   CANCEL_TYPE_CD       varchar(255)         not null,
   CANCEL_TYPE_CCD      varchar(255)         not null,
   CANCEL_TYPE_CST      varchar(255)         not null,
   CANCEL_TYPE_CLT      varchar(255)         not null,
   CANCEL_REASON_TEXT   varchar(255)         not null,
   CANCEL_STATUS_CD     varchar(255)         not null,
   CANCEL_STATUS_CCD    varchar(255)         not null,
   CANCEL_STATUS_CST    varchar(255)         not null,
   CANCEL_STATUS_CLT    varchar(255)         not null,
   CLOSE_DTS            DATETIME2            null,
   CLOSURE_STATUS_CD    varchar(255)         not null,
   CLOSURE_STATUS_CCD   varchar(255)         not null,
   CLOSURE_STATUS_CST   varchar(255)         not null,
   CLOSURE_STATUS_CLT   varchar(255)         not null,
   CONFIRM_NOTIF_STATE_CD varchar(255)         not null,
   CONFIRM_NOTIF_STATE_CCD varchar(255)         not null,
   CONFIRM_NOTIF_STATE_CST varchar(255)         not null,
   CONFIRM_NOTIF_STATE_CLT varchar(255)         not null,
   DBA_NAME             varchar(255)         not null,
   FULL_PAY_DISC_UNTIL_DT DATE                 null,
   OFFER_NO             varchar(255)         not null,
   POL_NO               varchar(255)         not null,
   POL_EFF_DT           DATE                 null,
   POL_EXP_DT           DATE                 null,
   PRIOR_POL_NO         varchar(255)         not null,
   TERM_NO              int                  not null,
   UNDERWRITER_NAME     varchar(255)         not null,
   EQUITY_BUFFER_DAYS   int                  not null,
   EQUITY_WARNINGS_ENABLED_FL char(1)              not null,
   WESTERN_METHOD_FL    char(1)              not null,
   CHRG_HELD_FL         char(1)              not null,
   HELD_FOR_INV_SENDING_FL char(1)              not null,
   UNDER_AUDIT_FL       char(1)              not null,
   TERM_CONFIRMED_FL    char(1)              not null,
   PMT_DISTR_ENABLED_FL char(1)              not null,
   HOLD_INV_WHEN_DELNQNT_FL char(1)              not null,
   FULL_PAY_DISC_EVALUATED_FL char(1)              not null,
   ELIG_FOR_FULL_PAY_DISC_FL char(1)              not null,
   ASSIGNED_RISK_FL     char(1)              not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_POL_PER
   add constraint DBPP1_PK primary key nonclustered (BILL_POL_PRD_DID)
go

alter table DIM_BILL_POL_PER
   add constraint DBPP1_AK1 unique (BILL_POL_PRD_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_PROD                                         */
/*==============================================================*/
create table DIM_BILL_PROD (
   PRDCR_DID            int                  not null,
   PRDCR_KEY            varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   RPT_GROUP_KEY        varchar(100)         not null,
   AGCY_BILL_PLAN_KEY   varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   SECURITY_ZONE_KEY    varchar(100)         not null,
   PRDCR_TIER_CD        varchar(255)         not null,
   PRDCR_TIER_CCD       varchar(255)         not null,
   PRDCR_TIER_CST       varchar(255)         not null,
   PRDCR_TIER_CLT       varchar(255)         not null,
   PRDCR_NAME           varchar(255)         not null,
   COMBINED_STATEMENTS_FL char(1)              not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_PROD
   add constraint DBPR_PK primary key nonclustered (PRDCR_DID)
go

alter table DIM_BILL_PROD
   add constraint DBPR_AK1 unique (PRDCR_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_PROD_CD                                      */
/*==============================================================*/
create table DIM_BILL_PROD_CD (
   PRDCR_CD_DID         int                  not null,
   PRDCR_CD_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PRDCR_KEY            varchar(100)         not null,
   COMM_PLAN_KEY        varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   PRDCR_CD             varchar(255)         not null,
   ACTIVE_FL            char(1)              not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_PROD_CD
   add constraint DBPC_PK primary key nonclustered (PRDCR_CD_DID)
go

alter table DIM_BILL_PROD_CD
   add constraint DBPC_AK1 unique (PRDCR_CD_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_PROD_PMT                                     */
/*==============================================================*/
create table DIM_BILL_PROD_PMT (
   PRDCR_PMT_DID        int                  not null,
   PRDCR_PMT_KEY        varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PRDCR_KEY            varchar(100)         not null,
   RPT_GROUP_KEY        varchar(100)         not null,
   REVERSAL_DTS         DATETIME2            null,
   REVERSAL_REASON_CD   varchar(255)         not null,
   REVERSAL_REASON_CCD  varchar(255)         not null,
   REVERSAL_REASON_CST  varchar(255)         not null,
   REVERSAL_REASON_CLT  varchar(255)         not null,
   STATUS_CD            varchar(255)         not null,
   STATUS_CCD           varchar(255)         not null,
   STATUS_CST           varchar(255)         not null,
   STATUS_CLT           varchar(255)         not null,
   PMT_TYPE_CD          varchar(255)         not null,
   PMT_TYPE_CCD         varchar(255)         not null,
   PMT_TYPE_CST         varchar(255)         not null,
   PMT_TYPE_CLT         varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_PROD_PMT
   add constraint DBPP_PK primary key nonclustered (PRDCR_PMT_DID)
go

alter table DIM_BILL_PROD_PMT
   add constraint DBPP_AK1 unique (PRDCR_PMT_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_PROD_STMNT                                   */
/*==============================================================*/
create table DIM_BILL_PROD_STMNT (
   PRDCR_STMNT_DID      int                  not null,
   PRDCR_STMNT_KEY      varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PRDCR_KEY            varchar(100)         not null,
   STMNT_DTS            DATETIME2            not null,
   STMNT_NO             varchar(255)         not null,
   STMNT_TYPE_CD        varchar(255)         not null,
   STMNT_TYPE_CCD       varchar(255)         not null,
   STMNT_TYPE_CST       varchar(255)         not null,
   STMNT_TYPE_CLT       varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_PROD_STMNT
   add constraint DBPS_PK primary key nonclustered (PRDCR_STMNT_DID)
go

alter table DIM_BILL_PROD_STMNT
   add constraint DBPS_AK1 unique (PRDCR_STMNT_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_SUSP_PMT                                     */
/*==============================================================*/
create table DIM_BILL_SUSP_PMT (
   SUSP_PMT_DID         int                  not null,
   SUSP_PMT_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   BILL_ACCT_APPLIED_TO_KEY varchar(100)         not null,
   BILL_POL_PRD_APPLIED_TO_KEY varchar(100)         not null,
   BILL_USER_APPLIED_BY_KEY varchar(100)         not null,
   MON_RCVD_KEY         varchar(100)         not null,
   RPT_GROUP_KEY        varchar(100)         not null,
   BILL_USER_REVERSED_BY_KEY varchar(100)         not null,
   PMT_INSTRUMENT_KEY   varchar(100)         not null,
   PRDCR_APPLIED_TO_KEY varchar(100)         not null,
   ACCT_NO              varchar(255)         not null,
   INV_NO               varchar(255)         not null,
   OFFER_NO             varchar(255)         not null,
   OFFER_OPT            varchar(255)         not null,
   PMT_DT               DATETIME2            not null,
   POL_NO               varchar(255)         not null,
   PRDCR_NAME           varchar(255)         not null,
   REF_NO               varchar(255)         not null,
   STATUS_CD            varchar(255)         not null,
   STATUS_CCD           varchar(255)         not null,
   STATUS_CST           varchar(255)         not null,
   STATUS_CLT           varchar(255)         not null,
   SUSP_PMT_TEXT        varchar(1333)        not null,
   CURR_CD              varchar(255)         not null,
   SUSP_AMT             numeric(18,2)        not null,
   PMT_METHOD_CD        varchar(255)         not null,
   PMT_METHOD_CCD       varchar(255)         not null,
   PMT_METHOD_CST       varchar(255)         not null,
   PMT_METHOD_CLT       varchar(255)         not null,
   SUSP_TYPE_NAME       varchar(255)         not null,
   RETIRED_FL           char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_SUSP_PMT
   add constraint DBSP_PK primary key nonclustered (SUSP_PMT_DID)
go

alter table DIM_BILL_SUSP_PMT
   add constraint DBSP_AK1 unique (SUSP_PMT_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_UNAPP_FUND                                   */
/*==============================================================*/
create table DIM_BILL_UNAPP_FUND (
   UNAPPLIED_FUND_DID   int                  not null,
   UNAPPLIED_FUND_KEY   varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BILL_ACCT_KEY        varchar(100)         not null,
   BILL_POL_KEY         varchar(100)         not null,
   RPT_GROUP_KEY        varchar(100)         not null,
   TACCT_KEY            varchar(100)         not null,
   UNAPPLIED_FUND_TEXT  varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_UNAPP_FUND
   add constraint DBUF_PK primary key nonclustered (UNAPPLIED_FUND_DID)
go

alter table DIM_BILL_UNAPP_FUND
   add constraint DBUF_AK1 unique (UNAPPLIED_FUND_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BILL_WRITEOFF                                     */
/*==============================================================*/
create table DIM_BILL_WRITEOFF (
   WO_DID               int                  not null,
   WO_KEY               varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CHRG_PAT_KEY         varchar(100)         not null,
   TACCT_CONTAINER_KEY  varchar(100)         not null,
   COMM_AGCY_PMT_ITEM_KEY varchar(100)         not null,
   GROSS_AGCY_PMT_ITEM_KEY varchar(100)         not null,
   CHRG_COMM_KEY        varchar(100)         not null,
   BILL_INV_ITEM_KEY    varchar(100)         not null,
   ITEM_COMM_KEY        varchar(100)         not null,
   PRDCR_KEY            varchar(100)         not null,
   REQUESTING_USER_KEY  varchar(100)         not null,
   EXECUTION_DTS        DATETIME2            null,
   APPRVL_DTS           DATETIME2            null,
   APPRVL_STATUS_CD     varchar(255)         not null,
   APPRVL_STATUS_CCD    varchar(255)         not null,
   APPRVL_STATUS_CST    varchar(255)         not null,
   APPRVL_STATUS_CLT    varchar(255)         not null,
   REASON_CD            varchar(255)         not null,
   REASON_CCD           varchar(255)         not null,
   REASON_CST           varchar(255)         not null,
   REASON_CLT           varchar(255)         not null,
   WO_TYPE_CD           varchar(255)         not null,
   WO_TYPE_CCD          varchar(255)         not null,
   WO_TYPE_CST          varchar(255)         not null,
   WO_TYPE_CLT          varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   WO_AMT               numeric(18,2)        not null,
   REVERSED_AMT         numeric(18,2)        not null,
   RETIRED_FL           char(1)              not null,
   REVERSED_FL          char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BILL_WRITEOFF
   add constraint DBWR_PK primary key nonclustered (WO_DID)
go

alter table DIM_BILL_WRITEOFF
   add constraint DBWR_AK1 unique (WO_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_BLDG                                              */
/*==============================================================*/
create table DIM_BLDG (
   BLDG_DID             int                  not null,
   BLDG_KEY             varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ALARM_CERTIFICATE    varchar(255)         not null,
   ALARM_CERTIFICATION_CD varchar(255)         not null,
   ALARM_CERTIFICATION_CCD varchar(255)         not null,
   ALARM_CERTIFICATION_CST varchar(255)         not null,
   ALARM_CERTIFICATION_CLT varchar(255)         not null,
   ALARM_CL_CD          varchar(255)         not null,
   ALARM_CL_CCD         varchar(255)         not null,
   ALARM_CL_CST         varchar(255)         not null,
   ALARM_CL_CLT         varchar(255)         not null,
   ALARM_EXP_DT         DATE                 null,
   ALARM_GRADE_CD       varchar(255)         not null,
   ALARM_GRADE_CCD      varchar(255)         not null,
   ALARM_GRADE_CST      varchar(255)         not null,
   ALARM_GRADE_CLT      varchar(255)         not null,
   BLDG_FINISHED_AREA_SF_NO int                  null,
   BLDG_UNFINISHED_AREA_SF_NO int                  null,
   BASEMENT_AREA_SF_NO  int                  null,
   AREA_LEASED_CD       varchar(255)         not null,
   AREA_LEASED_CCD      varchar(255)         not null,
   AREA_LEASED_CST      varchar(255)         not null,
   AREA_LEASED_CLT      varchar(255)         not null,
   BLDG_ALARM_TYPE_CD   varchar(255)         not null,
   BLDG_ALARM_TYPE_CCD  varchar(255)         not null,
   BLDG_ALARM_TYPE_CST  varchar(255)         not null,
   BLDG_ALARM_TYPE_CLT  varchar(255)         not null,
   BLDG_NO              int                  null,
   BURGLAR_SAFEGUARD_CD varchar(255)         not null,
   BURGLAR_SAFEGUARD_CCD varchar(255)         not null,
   BURGLAR_SAFEGUARD_CST varchar(255)         not null,
   BURGLAR_SAFEGUARD_CLT varchar(255)         not null,
   CONSTR_TYPE_CD       varchar(255)         not null,
   CONSTR_TYPE_CCD      varchar(255)         not null,
   CONSTR_TYPE_CST      varchar(255)         not null,
   CONSTR_TYPE_CLT      varchar(255)         not null,
   BLDG_TEXT            varchar(255)         not null,
   EFFECTIVENESS_GRADE_CD varchar(255)         not null,
   EFFECTIVENESS_GRADE_CCD varchar(255)         not null,
   EFFECTIVENESS_GRADE_CST varchar(255)         not null,
   EFFECTIVENESS_GRADE_CLT varchar(255)         not null,
   HEATING_BOILER_ON_PRMSS_FL char(1)              not null,
   HEATING_BOILER_ELSE_WHERE_FL char(1)              not null,
   BASEMENTS_NO         int                  null,
   STORIES_NO           int                  null,
   UNITS_NO             int                  null,
   PCT_OCCUPIED_CD      varchar(255)         not null,
   PCT_OCCUPIED_CCD     varchar(255)         not null,
   PCT_OCCUPIED_CST     varchar(255)         not null,
   PCT_OCCUPIED_CLT     varchar(255)         not null,
   PCT_VACANT_CD        varchar(255)         not null,
   PCT_VACANT_CCD       varchar(255)         not null,
   PCT_VACANT_CST       varchar(255)         not null,
   PCT_VACANT_CLT       varchar(255)         not null,
   PCT_RENTED_TO_OTHERS_CD varchar(255)         not null,
   PCT_RENTED_TO_OTHERS_CCD varchar(255)         not null,
   PCT_RENTED_TO_OTHERS_CST varchar(255)         not null,
   PCT_RENTED_TO_OTHERS_CLT varchar(255)         not null,
   ROOF_TYPE_CD         varchar(255)         not null,
   ROOF_TYPE_CCD        varchar(255)         not null,
   ROOF_TYPE_CST        varchar(255)         not null,
   ROOF_TYPE_CLT        varchar(255)         not null,
   SPRINKLER_COVG_CD    varchar(255)         not null,
   SPRINKLER_COVG_CCD   varchar(255)         not null,
   SPRINKLER_COVG_CST   varchar(255)         not null,
   SPRINKLER_COVG_CLT   varchar(255)         not null,
   BLDG_TYPE_CD         varchar(255)         not null,
   BLDG_TYPE_CCD        varchar(255)         not null,
   BLDG_TYPE_CST        varchar(255)         not null,
   BLDG_TYPE_CLT        varchar(255)         not null,
   TOT_AREA_SF_NO       int                  null,
   WIND_RATING_CD       varchar(255)         not null,
   WIND_RATING_CCD      varchar(255)         not null,
   WIND_RATING_CST      varchar(255)         not null,
   WIND_RATING_CLT      varchar(255)         not null,
   BUILD_YR             int                  null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_BLDG
   add constraint DIBL_PK primary key nonclustered (BLDG_DID)
go

/*==============================================================*/
/* Table: DIM_CATASTROPHE                                       */
/*==============================================================*/
create table DIM_CATASTROPHE (
   CATASTROPHE_ID       int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CATASTROPHE_CD       varchar(255)         not null,
   CONF_CATASTROPHE_CD  varchar(255)         not null,
   CONF_CATASTROPHE_SHORT_TEXT varchar(255)         not null,
   CONF_CATASTROPHE_LONG_TEXT varchar(255)         not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table DIM_CATASTROPHE
   add constraint DICA_PK primary key nonclustered (CATASTROPHE_ID)
go

/*==============================================================*/
/* Table: DIM_CA_DEALER                                         */
/*==============================================================*/
create table DIM_CA_DEALER (
   CA_DEALER_DID        int                  not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   DEALERS_NO           int                  null,
   GARAGE_DEALERS_CL_CD varchar(255)         not null,
   GARAGE_DEALERS_CL_CCD varchar(255)         not null,
   GARAGE_DEALERS_CL_CST varchar(255)         not null,
   GARAGE_DEALERS_CL_CLT varchar(255)         not null,
   DEALERS_CL_CD        varchar(255)         not null,
   GARAGE_DEALERS_COVG_TYPE_CD varchar(255)         not null,
   GARAGE_DEALERS_COVG_TYPE_CCD varchar(255)         not null,
   GARAGE_DEALERS_COVG_TYPE_CST varchar(255)         not null,
   GARAGE_DEALERS_COVG_TYPE_CLT varchar(255)         not null,
   FRAN_FL              char(1)              not null,
   TRNSPR_PLATES_CNT    int                  null,
   SETS_OF_DEALER_PLATES_CNT int                  null,
   RATING_BASE_CD       varchar(255)         not null,
   RATING_BASE_CCD      varchar(255)         not null,
   RATING_BASE_CST      varchar(255)         not null,
   RATING_BASE_CLT      varchar(255)         not null,
   RATING_UNITS_TOT_CNT int                  null,
   FULL_TM_OPER_CNT     int                  null,
   PART_TM_OPER_CNT     int                  null,
   FULL_TM_EMP_CNT      int                  null,
   PART_TM_EMP_CNT      int                  null,
   NON_EMP_UNDER_AGE_25_CNT int                  null,
   NON_EMP_AGE_25_OR_OVER_CNT int                  null,
   PHYS_DMG_CLASS_CD    varchar(255)         not null,
   PHYS_DMG_CLASS_CCD   varchar(255)         not null,
   PHYS_DMG_CLASS_CST   varchar(255)         not null,
   PHYS_DMG_CLASS_CLT   varchar(255)         not null,
   INS_VAL_AMT          numeric(18,2)        null,
   CURR_CD              varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_CA_DEALER
   add constraint DCDE_PK primary key nonclustered (CA_DEALER_DID)
go

/*==============================================================*/
/* Table: DIM_CA_GAR_SRVC                                       */
/*==============================================================*/
create table DIM_CA_GAR_SRVC (
   GARAGE_SRVC_DID      int                  not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   LIAB_CL_CD           varchar(255)         not null,
   EMP_AS_INS_FL        char(1)              not null,
   GARAGE_SRVC_NO       int                  null,
   EMP_CNT_NO           int                  null,
   PARTNERS_CNT_NO      int                  null,
   UNIT_NO              int                  null,
   INS_VAL_AMT          numeric(18,2)        null,
   CURR_CD              varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_CA_GAR_SRVC
   add constraint DCGS_PK primary key nonclustered (GARAGE_SRVC_DID)
go

/*==============================================================*/
/* Table: DIM_CA_JURS                                           */
/*==============================================================*/
create table DIM_CA_JURS (
   CA_JURS_DID          int                  not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   JURS_CD              varchar(255)         not null,
   JURS_CCD             varchar(255)         not null,
   JURS_CST             varchar(255)         not null,
   JURS_CLT             varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_CA_JURS
   add constraint DCJU_PK primary key nonclustered (CA_JURS_DID)
go

/*==============================================================*/
/* Table: DIM_CA_NMD_INDIV                                      */
/*==============================================================*/
create table DIM_CA_NMD_INDIV (
   NMD_INDIV_DID        int                  not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   INDIV_NAME           varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_CA_NMD_INDIV
   add constraint DCNI_PK primary key nonclustered (NMD_INDIV_DID)
go

/*==============================================================*/
/* Table: DIM_CA_POL_LINE                                       */
/*==============================================================*/
create table DIM_CA_POL_LINE (
   CA_POL_LINE_DID      int                  not null,
   POL_LINE_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   AUTO_SMBL_MNUL_EDIT_DT DATE                 null,
   CSTM_AUTO_SMBL_DESC  varchar(255)         not null,
   FLEET_TYPE_CD        varchar(255)         not null,
   FLEET_TYPE_CCD       varchar(255)         not null,
   FLEET_TYPE_CST       varchar(255)         not null,
   FLEET_TYPE_CLT       varchar(255)         not null,
   LEGAL_ENTITY_TYPE_CD varchar(255)         not null,
   LEGAL_ENTITY_TYPE_CCD varchar(255)         not null,
   LEGAL_ENTITY_TYPE_CST varchar(255)         not null,
   LEGAL_ENTITY_TYPE_CLT varchar(255)         not null,
   CA_POL_TYPE_CD       varchar(255)         not null,
   CA_POL_TYPE_CCD      varchar(255)         not null,
   CA_POL_TYPE_CST      varchar(255)         not null,
   CA_POL_TYPE_CLT      varchar(255)         not null,
   BUS_START_DT         DATE                 null,
   EXPER_RATING_FL      char(1)              not null,
   RISK_TYPE_CD         varchar(255)         not null,
   RISK_TYPE_CCD        varchar(255)         not null,
   RISK_TYPE_CST        varchar(255)         not null,
   RISK_TYPE_CLT        varchar(255)         not null,
   SCHED_RATING_MOD_FL  char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_CA_POL_LINE
   add constraint DCPL_PK primary key nonclustered (CA_POL_LINE_DID)
go

/*==============================================================*/
/* Table: DIM_CA_VEH                                            */
/*==============================================================*/
create table DIM_CA_VEH (
   CA_VEH_DID           int                  not null,
   CA_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   VEH_ID_NO            varchar(255)         not null,
   MAKE_NAME            varchar(255)         not null,
   MODEL_NAME           varchar(255)         not null,
   MODEL_YR             int                  null,
   VEH_NO               int                  null,
   ORIG_COST_NEW_AMT    numeric(18,2)        null,
   STATED_AMT           numeric(18,2)        null,
   UNIT_NO              int                  null,
   LIC_JURS_CD          varchar(255)         not null,
   LIC_JURS_CCD         varchar(255)         not null,
   LIC_JURS_CST         varchar(255)         not null,
   LIC_JURS_CLT         varchar(255)         not null,
   LIC_PLATE            varchar(255)         not null,
   LEASE_OR_RENT_FL     char(1)              not null,
   LEN_OF_LEASE_CD      varchar(255)         not null,
   LEN_OF_LEASE_CCD     varchar(255)         not null,
   LEN_OF_LEASE_CST     varchar(255)         not null,
   LEN_OF_LEASE_CLT     varchar(255)         not null,
   CL_CD                varchar(255)         not null,
   FLEET_FL             char(1)              not null,
   LESSOR_AN_EMP_FL     char(1)              not null,
   PRVT_PSNGR_OPER_EXPER_CD varchar(255)         not null,
   PRVT_PSNGR_OPER_EXPER_CCD varchar(255)         not null,
   PRVT_PSNGR_OPER_EXPER_CST varchar(255)         not null,
   PRVT_PSNGR_OPER_EXPER_CLT varchar(255)         not null,
   PRVT_PSNGR_USE_CD    varchar(255)         not null,
   PRVT_PSNGR_USE_CCD   varchar(255)         not null,
   PRVT_PSNGR_USE_CST   varchar(255)         not null,
   PRVT_PSNGR_USE_CLT   varchar(255)         not null,
   PRVT_PSNGR_TYPE_CD   varchar(255)         not null,
   PRVT_PSNGR_TYPE_CCD  varchar(255)         not null,
   PRVT_PSNGR_TYPE_CST  varchar(255)         not null,
   PRVT_PSNGR_TYPE_CLT  varchar(255)         not null,
   BUS_USE_CL_CD        varchar(255)         not null,
   BUS_USE_CL_CCD       varchar(255)         not null,
   BUS_USE_CL_CST       varchar(255)         not null,
   BUS_USE_CL_CLT       varchar(255)         not null,
   GVW_NO               int                  null,
   TRUCK_RADIUS_CL_CD   varchar(255)         not null,
   TRUCK_RADIUS_CL_CCD  varchar(255)         not null,
   TRUCK_RADIUS_CL_CST  varchar(255)         not null,
   TRUCK_RADIUS_CL_CLT  varchar(255)         not null,
   TRUCK_SEC_CL_CD      varchar(255)         not null,
   TRUCK_SEC_CL_CCD     varchar(255)         not null,
   TRUCK_SEC_CL_CST     varchar(255)         not null,
   TRUCK_SEC_CL_CLT     varchar(255)         not null,
   SEC_CL_CD            varchar(255)         not null,
   SIZE_CL_CD           varchar(255)         not null,
   SIZE_CL_CCD          varchar(255)         not null,
   SIZE_CL_CST          varchar(255)         not null,
   SIZE_CL_CLT          varchar(255)         not null,
   TRLR_HOW_USED_CD     varchar(255)         not null,
   TRLR_HOW_USED_CCD    varchar(255)         not null,
   TRLR_HOW_USED_CST    varchar(255)         not null,
   TRLR_HOW_USED_CLT    varchar(255)         not null,
   FAR_TERM_ZONE        varchar(255)         not null,
   GARAGING_ZONE        varchar(255)         not null,
   DAYS_SCHL_YR_CNT     int                  null,
   SCHL_BUS_PRORATION_FL char(1)              not null,
   SEATING_CPCTY_CD     varchar(255)         not null,
   SEATING_CPCTY_CCD    varchar(255)         not null,
   SEATING_CPCTY_CST    varchar(255)         not null,
   SEATING_CPCTY_CLT    varchar(255)         not null,
   PUB_TRNSP_TYPE_CD    varchar(255)         not null,
   PUB_TRNSP_TYPE_CCD   varchar(255)         not null,
   PUB_TRNSP_TYPE_CST   varchar(255)         not null,
   PUB_TRNSP_TYPE_CLT   varchar(255)         not null,
   PUB_TRNSP_RADIUS_CL_CD varchar(255)         not null,
   PUB_TRNSP_RADIUS_CL_CCD varchar(255)         not null,
   PUB_TRNSP_RADIUS_CL_CST varchar(255)         not null,
   PUB_TRNSP_RADIUS_CL_CLT varchar(255)         not null,
   SPCL_VEH_CL_CD       varchar(255)         not null,
   SPCL_VEH_CL_CCD      varchar(255)         not null,
   SPCL_VEH_CL_CST      varchar(255)         not null,
   SPCL_VEH_CL_CLT      varchar(255)         not null,
   ENGINE_SIZE_CD       varchar(255)         not null,
   ENGINE_SIZE_CCD      varchar(255)         not null,
   ENGINE_SIZE_CST      varchar(255)         not null,
   ENGINE_SIZE_CLT      varchar(255)         not null,
   GROSS_RECEIPTS_AMT   numeric(18,2)        null,
   NON_OWNED_AUTOS_FL   char(1)              not null,
   DAYS_VEH_LEASED_CNT  varchar(255)         not null,
   FCTRY_TST_EMP_CNT    int                  null,
   REPO_AUTOS_CNT       int                  null,
   PHYS_DMG_CLASS_CD    varchar(255)         not null,
   PHYS_DMG_CLASS_CCD   varchar(255)         not null,
   PHYS_DMG_CLASS_CST   varchar(255)         not null,
   PHYS_DMG_CLASS_CLT   varchar(255)         not null,
   SUPP_TYPE_CD         varchar(255)         not null,
   SUPP_TYPE_CCD        varchar(255)         not null,
   SUPP_TYPE_CST        varchar(255)         not null,
   SUPP_TYPE_CLT        varchar(255)         not null,
   COST_HIRE_FOR_COLL_AMT numeric(18,2)        null,
   COST_HIRE_FOR_LIAB_AMT numeric(18,2)        null,
   COST_HIRE_FOR_EXCS_LIAB_AMT numeric(18,2)        null,
   COST_HIRE_FOR_OTC_AMT numeric(18,2)        null,
   SPCL_VEH_TYPE_CD     varchar(255)         not null,
   SPCL_VEH_TYPE_CCD    varchar(255)         not null,
   SPCL_VEH_TYPE_CST    varchar(255)         not null,
   SPCL_VEH_TYPE_CLT    varchar(255)         not null,
   REG_USE_FL           char(1)              not null,
   CURR_CD              varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_CA_VEH
   add constraint DCVE_PK primary key nonclustered (CA_VEH_DID)
go

/*==============================================================*/
/* Table: DIM_CLAIM                                             */
/*==============================================================*/
create table DIM_CLAIM (
   CLAIM_ID             int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CLAIM_BASE_ID        int                  not null,
   CLAIM_KEY            varchar(100)         not null,
   POL_KEY              varchar(100)         not null,
   LAST_STATUS_ID       int                  null,
   LAST_STATUS_PROC_DTS DATETIME2            null,
   CLAIM_NO             varchar(255)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   LOSS_DT              DATE                 null,
   LOSS_YR              int                  not null,
   RPRTD_DT             DATE                 null,
   RPRTD_YR             int                  null,
   SETUP_DT             DATE                 null,
   LOSS_TEXT            varchar(1333)        not null,
   EXAMINER_CD          varchar(255)         not null,
   CONF_EXAMINER_CD     varchar(255)         not null,
   CONF_EXAMINER_SHORT_TEXT varchar(255)         not null,
   CONF_EXAMINER_LONG_TEXT varchar(255)         not null,
   LOSS_CITY            varchar(255)         not null,
   LOSS_STATE_CD        varchar(255)         not null,
   CONF_LOSS_STATE_CD   varchar(255)         not null,
   CONF_LOSS_STATE_SHORT_TEXT varchar(255)         not null,
   CONF_LOSS_STATE_LONG_TEXT varchar(255)         not null,
   LOSS_ZIP_CD          varchar(15)          not null,
   LOSS_TYPE_CD         varchar(255)         not null,
   CONF_LOSS_TYPE_CD    varchar(255)         not null,
   CONF_LOSS_TYPE_SHORT_TEXT varchar(255)         not null,
   CONF_LOSS_TYPE_LONG_TEXT varchar(255)         not null,
   CAUSE_OF_LOSS_CD     varchar(255)         not null,
   CONF_CAUSE_OF_LOSS_CD varchar(255)         not null,
   CONF_CAUSE_OF_LOSS_SHORT_TEXT varchar(255)         not null,
   CONF_CAUSE_OF_LOSS_LONG_TEXT varchar(255)         not null,
   FAST_TRACK_FL        char(1)              not null,
   CATASTROPHE_CD       varchar(255)         not null,
   CONF_CATASTROPHE_CD  varchar(255)         not null,
   CONF_CATASTROPHE_SHORT_TEXT varchar(255)         not null,
   CONF_CATASTROPHE_LONG_TEXT varchar(255)         not null,
   CATASTROPHE_DECLARE_DT DATE                 null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table DIM_CLAIM
   add constraint DICL1_PK primary key nonclustered (CLAIM_ID)
go

/*==============================================================*/
/* Index: DICL_DCST_XFK                                         */
/*==============================================================*/




create nonclustered index DICL_DCST_XFK on DIM_CLAIM (LAST_STATUS_ID ASC)
go

/*==============================================================*/
/* Table: DIM_CLAIMANT                                          */
/*==============================================================*/
create table DIM_CLAIMANT (
   CLMNT_ID             int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CLAIM_KEY            varchar(100)         not null,
   CLMNT_KEY            varchar(100)         not null,
   CLMNT_NO             varchar(255)         not null,
   CLMNT_NAME           varchar(255)         not null,
   CLMNT_ADDR_1         varchar(255)         not null,
   CLMNT_ADDR_2         varchar(255)         not null,
   CLMNT_ADDR_3         varchar(255)         not null,
   CLMNT_CITY           varchar(255)         not null,
   CLMNT_COUNTY         varchar(255)         not null,
   CLMNT_STATE_CD       varchar(255)         not null,
   CONF_CLMNT_STATE_CD  varchar(255)         not null,
   CONF_CLMNT_STATE_SHORT_TEXT varchar(255)         not null,
   CONF_CLMNT_STATE_LONG_TEXT varchar(255)         not null,
   CLMNT_ZIP_CD         varchar(15)          not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table DIM_CLAIMANT
   add constraint DICL_PK primary key nonclustered (CLMNT_ID)
go

/*==============================================================*/
/* Table: DIM_CLAIM_FEATURE                                     */
/*==============================================================*/
create table DIM_CLAIM_FEATURE (
   CLAIM_FEATURE_ID     int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CLAIM_KEY            varchar(100)         not null,
   CLMNT_KEY            varchar(100)         not null,
   FEATURE_KEY          varchar(100)         not null,
   LAST_STATUS_ID       int                  null,
   LAST_STATUS_PROC_DTS DATETIME2            null,
   COVG_CD              varchar(255)         not null,
   FEATURE_SETUP_DT     DATE                 null,
   FEATURE_RPRTD_DT     DATE                 null,
   SUIT_FL              char(1)              not null,
   ADJSTR_CD            varchar(255)         not null,
   CONF_ADJSTR_CD       varchar(255)         not null,
   CONF_ADJSTR_SHORT_TEXT varchar(255)         not null,
   CONF_ADJSTR_LONG_TEXT varchar(255)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table DIM_CLAIM_FEATURE
   add constraint DCFE_PK primary key nonclustered (CLAIM_FEATURE_ID)
go

/*==============================================================*/
/* Index: DCFE_DCST_XFK                                         */
/*==============================================================*/




create nonclustered index DCFE_DCST_XFK on DIM_CLAIM_FEATURE (LAST_STATUS_ID ASC)
go

/*==============================================================*/
/* Table: DIM_CLAIM_STATUS                                      */
/*==============================================================*/
create table DIM_CLAIM_STATUS (
   CLAIM_STATUS_ID      int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   STATUS_TYPE_CD       varchar(255)         not null,
   STATUS_CD            varchar(255)         not null,
   REASON_CD            varchar(1333)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_CLAIM_STATUS
   add constraint DCST_PK primary key nonclustered (CLAIM_STATUS_ID)
go

alter table DIM_CLAIM_STATUS
   add constraint DCST_AK1 unique (SOURCE_SYSTEM, STATUS_TYPE_CD, STATUS_CD, REASON_CD)
go

/*==============================================================*/
/* Table: DIM_COVG                                              */
/*==============================================================*/
create table DIM_COVG (
   COVG_ID              int                  not null,
   COVG_KEY             varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   COVG_PART_CD         varchar(255)         not null,
   COVG_PART_CCD        varchar(255)         not null,
   COVG_PART_CST        varchar(255)         not null,
   COVG_PART_CLT        varchar(255)         not null,
   COVG_CD              varchar(255)         not null,
   COVG_CCD             varchar(255)         not null,
   COVG_CST             varchar(255)         not null,
   COVG_CLT             varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_COVG
   add constraint DICO_PK primary key nonclustered (COVG_ID)
go

alter table DIM_COVG
   add constraint DICO_AK1 unique (COVG_KEY, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_CP_BI                                             */
/*==============================================================*/
create table DIM_CP_BI (
   CP_BUS_INC_DID       int                  not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   WATERFRONT_PROP_FL   char(1)              not null,
   INC_IN_BLNKT_FL      char(1)              not null,
   FUNG_WET_DRY_ROT_BACT_FL char(1)              not null,
   BUS_INC_COVG_TYPE_CD varchar(255)         not null,
   BUS_INC_COVG_TYPE_CCD varchar(255)         not null,
   BUS_INC_COVG_TYPE_CST varchar(255)         not null,
   BUS_INC_COVG_TYPE_CLT varchar(255)         not null,
   WATERFRONT_CONSTR_TEXT varchar(255)         not null,
   TYPE_OF_RISK_TEXT    varchar(255)         not null,
   RISK_EDU_INST_PCT    varchar(255)         not null,
   RISK_MERC_PCT        varchar(255)         not null,
   RISK_RENTAL_PCT      varchar(255)         not null,
   RISK_MFG_PCT         varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_CP_BI
   add constraint DCBI_PK primary key nonclustered (CP_BUS_INC_DID)
go

/*==============================================================*/
/* Table: DIM_CP_BLDG                                           */
/*==============================================================*/
create table DIM_CP_BLDG (
   CP_BLDG_DID          int                  not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   BCEG_TEXT            varchar(255)         not null,
   BCEG_CL_CD           varchar(255)         not null,
   BCEG_CL_CCD          varchar(255)         not null,
   BCEG_CL_CST          varchar(255)         not null,
   BCEG_CL_CLT          varchar(255)         not null,
   BLDG_INC_IN_OPER_UNIT_FL char(1)              not null,
   CONF_TO_CONDO_ACTS_FL char(1)              not null,
   EQ_CL_CD             varchar(255)         not null,
   EQ_CL_CCD            varchar(255)         not null,
   EQ_CL_CST            varchar(255)         not null,
   EQ_CL_CLT            varchar(255)         not null,
   EQ_ROOF_TANK_HAZARD_FL char(1)              not null,
   EXPSR                varchar(255)         not null,
   EXTENT_OF_PROT       varchar(255)         not null,
   BURGLAR_ALARM_CR_FCTR_TEXT varchar(255)         not null,
   BURGLAR_ALARM_TYPE_CD varchar(255)         not null,
   BURGLAR_ALARM_TYPE_CCD varchar(255)         not null,
   BURGLAR_ALARM_TYPE_CST varchar(255)         not null,
   BURGLAR_ALARM_TYPE_CLT varchar(255)         not null,
   RATING_TYPE_CD       varchar(255)         not null,
   RATING_TYPE_CCD      varchar(255)         not null,
   RATING_TYPE_CST      varchar(255)         not null,
   RATING_TYPE_CLT      varchar(255)         not null,
   CONSTR_TYPE_CD       varchar(255)         not null,
   CONSTR_TYPE_CCD      varchar(255)         not null,
   CONSTR_TYPE_CST      varchar(255)         not null,
   CONSTR_TYPE_CLT      varchar(255)         not null,
   BLDG_COVG_TYPE_CD    varchar(255)         not null,
   BLDG_COVG_TYPE_CCD   varchar(255)         not null,
   BLDG_COVG_TYPE_CST   varchar(255)         not null,
   BLDG_COVG_TYPE_CLT   varchar(255)         not null,
   HEATING_TYPE_CD      varchar(255)         not null,
   HEATING_TYPE_CCD     varchar(255)         not null,
   HEATING_TYPE_CST     varchar(255)         not null,
   HEATING_TYPE_CLT     varchar(255)         not null,
   HEATING_TYPE_OTHR    varchar(255)         not null,
   HEATING_UPDATED_YR   int                  null,
   PLUMBING_UPDATED_YR  int                  null,
   ROOFING_UPDATED_YR   int                  null,
   ROOF_TYPE_CD         varchar(255)         not null,
   ROOF_TYPE_CCD        varchar(255)         not null,
   ROOF_TYPE_CST        varchar(255)         not null,
   ROOF_TYPE_CLT        varchar(255)         not null,
   INC_IN_BLNKT_FL      char(1)              not null,
   TOT_EXTR_WALLS_FACE_PCT_CD varchar(255)         not null,
   TOT_EXTR_WALLS_FACE_PCT_CCD varchar(255)         not null,
   TOT_EXTR_WALLS_FACE_PCT_CST varchar(255)         not null,
   TOT_EXTR_WALLS_FACE_PCT_CLT varchar(255)         not null,
   STORIES_NO_CD        varchar(255)         not null,
   STORIES_NO_CCD       varchar(255)         not null,
   STORIES_NO_CST       varchar(255)         not null,
   STORIES_NO_CLT       varchar(255)         not null,
   OPEN_SIDES_FL        char(1)              not null,
   RENTAL_PROP_FL       char(1)              not null,
   STATE_OWNED_PROP_FL  char(1)              not null,
   SPECIFIC_GROUP_II_RATE numeric(9,5)         not null,
   SPECIFIC_GROUP_I_RATE_TEXT varchar(255)         not null,
   SPRINKLER_SYS_TEXT   varchar(255)         not null,
   BR4_SMBL_FL          char(1)              not null,
   BR4_SMBL_TEXT        varchar(1333)        not null,
   SUB_STD_COND_A_FL    char(1)              not null,
   SUB_STD_COND_B_FL    char(1)              not null,
   SUB_STD_COND_C_FL    char(1)              not null,
   SUB_STD_COND_D_FL    char(1)              not null,
   SUB_STD_COND_E_FL    char(1)              not null,
   HEATING_BY_PLANT_FL  char(1)              not null,
   WATCHMAN_SRVC_CD     varchar(255)         not null,
   WATCHMAN_SRVC_CCD    varchar(255)         not null,
   WATCHMAN_SRVC_CST    varchar(255)         not null,
   WATCHMAN_SRVC_CLT    varchar(255)         not null,
   WIND_CL_CD           varchar(255)         not null,
   WIND_CL_CCD          varchar(255)         not null,
   WIND_CL_CST          varchar(255)         not null,
   WIND_CL_CLT          varchar(255)         not null,
   WIND_CL_OTHR         varchar(255)         not null,
   WIRING_UPDATED_YR    int                  null,
   VACANT_BLDG_FL       char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_CP_BLDG
   add constraint DCBL_PK primary key nonclustered (CP_BLDG_DID)
go

/*==============================================================*/
/* Table: DIM_CP_BLNKT                                          */
/*==============================================================*/
create table DIM_CP_BLNKT (
   CP_BLNKT_DID         int                  not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   BLNKT_NO             int                  null,
   BLNKT_TEXT           varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_CP_BLNKT
   add constraint DCBL1_PK primary key nonclustered (CP_BLNKT_DID)
go

/*==============================================================*/
/* Table: DIM_CP_CL_CODE                                        */
/*==============================================================*/
create table DIM_CP_CL_CODE (
   CP_CL_CD_DID         int                  not null,
   CP_CL_CD_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   EFF_DT               DATE                 not null,
   EXP_DT               DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CL_CD                varchar(255)         not null,
   RATING_TYPE_CD       varchar(255)         not null,
   RATING_TYPE_CCD      varchar(255)         not null,
   RATING_TYPE_CST      varchar(255)         not null,
   RATING_TYPE_CLT      varchar(255)         not null,
   CL_TEXT              varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_CP_CL_CODE
   add constraint DCCC_PK primary key nonclustered (CP_CL_CD_DID)
go

/*==============================================================*/
/* Table: DIM_CP_LOC                                            */
/*==============================================================*/
create table DIM_CP_LOC (
   CP_LOC_DID           int                  not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   LOC_NO               varchar(255)         not null,
   EQ_CAUSE_OF_LOSS_FORM_CD varchar(255)         not null,
   EQ_CAUSE_OF_LOSS_FORM_CCD varchar(255)         not null,
   EQ_CAUSE_OF_LOSS_FORM_CST varchar(255)         not null,
   EQ_CAUSE_OF_LOSS_FORM_CLT varchar(255)         not null,
   MULTI_RES_SPCL_CR_CD varchar(255)         not null,
   MULTI_RES_SPCL_CR_CCD varchar(255)         not null,
   MULTI_RES_SPCL_CR_CST varchar(255)         not null,
   MULTI_RES_SPCL_CR_CLT varchar(255)         not null,
   PROT_CL              varchar(255)         not null,
   FLOOD_COVG_FL        char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_CP_LOC
   add constraint DCLO_PK primary key nonclustered (CP_LOC_DID)
go

/*==============================================================*/
/* Table: DIM_CP_OCC_CL                                         */
/*==============================================================*/
create table DIM_CP_OCC_CL (
   CP_OCC_CL_DID        int                  not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   OCCPNCY_NO           int                  null,
   OCCPNCY_CATG_CD      varchar(255)         not null,
   OCCPNCY_CATG_CCD     varchar(255)         not null,
   OCCPNCY_CATG_CST     varchar(255)         not null,
   OCCPNCY_CATG_CLT     varchar(255)         not null,
   AREA_SF_NO           int                  null,
   COVG_FORM_CD         varchar(255)         not null,
   COVG_FORM_CCD        varchar(255)         not null,
   COVG_FORM_CST        varchar(255)         not null,
   COVG_FORM_CLT        varchar(255)         not null,
   SPRINKLER_SYS_TEXT   varchar(255)         not null,
   OCC_CL_TEXT          varchar(1333)        not null,
   CONF_TO_CONDO_ACTS_FL char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_CP_OCC_CL
   add constraint DCOC_PK primary key nonclustered (CP_OCC_CL_DID)
go

/*==============================================================*/
/* Table: DIM_CP_POL_LINE                                       */
/*==============================================================*/
create table DIM_CP_POL_LINE (
   CP_POL_LINE_DID      int                  not null,
   POL_LINE_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BLKT_RATED_FL        char(1)              not null,
   CP_POL_TYPE_CD       varchar(255)         not null,
   CP_POL_TYPE_CCD      varchar(255)         not null,
   CP_POL_TYPE_CST      varchar(255)         not null,
   CP_POL_TYPE_CLT      varchar(255)         not null,
   COVG_CERT_TISM_TEXT  varchar(255)         not null,
   COVG_OTHR_TISM_TEXT  varchar(255)         not null,
   EQ_SUB_LMT_BLNKT_FL  char(1)              not null,
   FLOOD_COVG_BLNKT_FL  char(1)              not null,
   FUNG_WETDRYROT_BACT_COVG_TEXT varchar(255)         not null,
   IRPM_APPLIES_FL      char(1)              not null,
   MULTI_PREM_DISP_CR_FL char(1)              not null,
   MULTI_PREM_DISP_CR_TEXT varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_CP_POL_LINE
   add constraint DCPL1_PK primary key nonclustered (CP_POL_LINE_DID)
go

/*==============================================================*/
/* Table: DIM_CP_PP                                             */
/*==============================================================*/
create table DIM_CP_PP (
   CP_PERS_PROP_DID     int                  not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   PERS_PROP_COVG_TYPE_CD varchar(255)         not null,
   PERS_PROP_COVG_TYPE_CCD varchar(255)         not null,
   PERS_PROP_COVG_TYPE_CST varchar(255)         not null,
   PERS_PROP_COVG_TYPE_CLT varchar(255)         not null,
   INC_IN_BLNKT_FL      char(1)              not null,
   YARD_PROP_INDC_TEXT  varchar(255)         not null,
   SPECIFIC_GROUP_I_RATE_TEXT varchar(255)         not null,
   STATE_OWNED_PROP_FL  char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_CP_PP
   add constraint DCPP_PK primary key nonclustered (CP_PERS_PROP_DID)
go

/*==============================================================*/
/* Table: DIM_CP_SPCL_CL                                        */
/*==============================================================*/
create table DIM_CP_SPCL_CL (
   CP_SPCL_CL_DID       int                  not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   CP_SPCL_CL_TEXT      varchar(255)         not null,
   CONF_TO_CONDO_ACTS_FL char(1)              not null,
   CP_SPCL_CL_COVG_FORM_CD varchar(255)         not null,
   CP_SPCL_CL_COVG_FORM_CCD varchar(255)         not null,
   CP_SPCL_CL_COVG_FORM_CST varchar(255)         not null,
   CP_SPCL_CL_COVG_FORM_CLT varchar(255)         not null,
   EQ_CL_CD             varchar(255)         not null,
   EQ_CL_CCD            varchar(255)         not null,
   EQ_CL_CST            varchar(255)         not null,
   EQ_CL_CLT            varchar(255)         not null,
   EQ_STRUC_TEXT        varchar(255)         not null,
   EXPSR                varchar(255)         not null,
   INC_IN_BLNKT_FL      char(1)              not null,
   RATING_TYPE_CD       varchar(255)         not null,
   RATING_TYPE_CCD      varchar(255)         not null,
   RATING_TYPE_CST      varchar(255)         not null,
   RATING_TYPE_CLT      varchar(255)         not null,
   RENTAL_PROP_FL       char(1)              not null,
   SPCL_CL_NO           int                  null,
   SPCL_CL_TEXT         varchar(255)         not null,
   BUILD_YR             int                  null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_CP_SPCL_CL
   add constraint DCSC_PK primary key nonclustered (CP_SPCL_CL_DID)
go

/*==============================================================*/
/* Table: DIM_CP_SPCL_CL_BI                                     */
/*==============================================================*/
create table DIM_CP_SPCL_CL_BI (
   CP_SPCL_CL_BUS_INC_DID int                  not null,
   CP_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   INC_IN_BLNKT_FL      char(1)              not null,
   BUS_INC_COVG_TYPE_CD varchar(255)         not null,
   BUS_INC_COVG_TYPE_CCD varchar(255)         not null,
   BUS_INC_COVG_TYPE_CST varchar(255)         not null,
   BUS_INC_COVG_TYPE_CLT varchar(255)         not null,
   RISK_EDU_INST_PCT    varchar(255)         not null,
   RISK_MERC_PCT        varchar(255)         not null,
   RISK_RENTAL_PCT      varchar(255)         not null,
   RISK_MFG_PCT         varchar(255)         not null,
   TYPE_OF_RISK_TEXT    varchar(255)         not null,
   WATERFRONT_CONSTR_TEXT varchar(255)         not null,
   WATERFRONT_PROP_FL   char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_CP_SPCL_CL_BI
   add constraint DCSCB_PK primary key nonclustered (CP_SPCL_CL_BUS_INC_DID)
go

/*==============================================================*/
/* Table: DIM_CUST_ACCT                                         */
/*==============================================================*/
create table DIM_CUST_ACCT (
   ACCT_ID              int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ACCT_KEY             varchar(100)         not null,
   ACCT_BASE_ID         int                  not null,
   ACCT_NO              varchar(255)         not null,
   ACCT_NAME            varchar(255)         not null,
   MAILING_ADDR_1       varchar(255)         not null,
   MAILING_ADDR_2       varchar(255)         not null,
   MAILING_ADDR_3       varchar(255)         not null,
   MAILING_CITY         varchar(255)         not null,
   MAILING_STATE_CD     varchar(255)         not null,
   CONF_MAILING_STATE_CD varchar(255)         not null,
   CONF_MAILING_STATE_SHORT_TEXT varchar(255)         not null,
   CONF_MAILING_STATE_LONG_TEXT varchar(255)         not null,
   MAILING_ZIP_CD       varchar(15)          not null,
   ROW_PROC_DTS         DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table DIM_CUST_ACCT
   add constraint DCAC_PK primary key nonclustered (ACCT_ID)
go

/*==============================================================*/
/* Table: DIM_DATE                                              */
/*==============================================================*/
create table DIM_DATE (
   DT_ID                int                  not null,
   DT_VAL               DATE                 not null,
   YR_NO                int                  not null,
   MTH_ID               int                  not null,
   QTR_ID               int                  not null,
   MTH_NO               char(2)              not null,
   WEEK_NO              char(2)              not null,
   QTR_NO               char(1)              not null,
   DAY_OF_YR_NO         char(3)              not null,
   DAY_OF_MTH_NO        char(2)              not null,
   DAY_OF_WEEK_NO       char(1)              not null,
   MTH_BEGIN_DT         DATE                 not null,
   MTH_END_DT           DATE                 not null,
   QTR_FIRST_MTH_NO     char(2)              not null,
   QTR_BEGIN_DT         DATE                 not null,
   QTR_END_DT           DATE                 not null,
   YR_BEGIN_DT          DATE                 not null,
   YR_END_DT            DATE                 not null,
   MTH_NAME             varchar(255)         not null,
   DAY_NAME             varchar(255)         not null,
   PME_DT               DATE                 not null,
   PYE_DT               DATE                 not null,
   PME_DT_ID            int                  not null,
   PYE_DT_ID            int                  not null,
   DAY_OFFSET_NO        int                  not null
)
go

alter table DIM_DATE
   add constraint DIDA_PK primary key nonclustered (DT_ID)
go

alter table DIM_DATE
   add constraint DIDA_AK1 unique (DT_VAL)
go

/*==============================================================*/
/* Table: DIM_GL_CL_CODE                                        */
/*==============================================================*/
create table DIM_GL_CL_CODE (
   GL_CL_CD_DID         int                  not null,
   GL_CL_CD_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   EFF_DT               DATE                 not null,
   EXP_DT               DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CD_CLASS             varchar(1333)        not null,
   LINE_CL_TEXT         varchar(255)         not null,
   COVG_PART_TYPE_CD    varchar(255)         not null,
   COVG_PART_TYPE_CCD   varchar(255)         not null,
   COVG_PART_TYPE_CST   varchar(255)         not null,
   COVG_PART_TYPE_CLT   varchar(255)         not null,
   SUBLINE_RATED_INDIV_FL char(1)              not null,
   BASIS_CD             varchar(255)         not null,
   BASIS_TEXT           varchar(1333)        not null,
   BASIS_RF             numeric(18,4)        null,
   BASIS_VARIABLE_FL    char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_GL_CL_CODE
   add constraint DGCC_PK primary key nonclustered (GL_CL_CD_DID)
go

/*==============================================================*/
/* Table: DIM_GL_COVG_PART                                      */
/*==============================================================*/
create table DIM_GL_COVG_PART (
   GL_COVG_PART_DID     int                  not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CLAIMS_ORIG_EFF_DT   DATETIME2            null,
   COVG_FORM_CD         varchar(255)         not null,
   COVG_FORM_CCD        varchar(255)         not null,
   COVG_FORM_CST        varchar(255)         not null,
   COVG_FORM_CLT        varchar(255)         not null,
   COVG_PART_TYPE_CD    varchar(255)         not null,
   COVG_PART_TYPE_CCD   varchar(255)         not null,
   COVG_PART_TYPE_CST   varchar(255)         not null,
   COVG_PART_TYPE_CLT   varchar(255)         not null,
   LOC_LIMITS_FL        char(1)              not null,
   POLLUTION_CLEANUP_EXP_FL char(1)              not null,
   CLAIMS_RETROACTIVE_DT DATETIME2            null,
   SPLIT_BIPD_DED_FL    char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_GL_COVG_PART
   add constraint DGCP_PK primary key nonclustered (GL_COVG_PART_DID)
go

/*==============================================================*/
/* Table: DIM_GL_EXPSR                                          */
/*==============================================================*/
create table DIM_GL_EXPSR (
   GL_EXPSR_DID         int                  not null,
   GL_CVRBL_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   AUDITED_BASIS_AMT    numeric(18,2)        null,
   PRODS_COMP_AUDITED_BASIS_AMT numeric(18,2)        null,
   EXPSR_TEXT           varchar(255)         not null,
   FIXED_BASIS_AMT      numeric(18,2)        null,
   PRODS_COMP_FIXED_BASIS_AMT numeric(18,2)        null,
   SCALABLE_BASIS_AMT   numeric(18,2)        null,
   PRODS_COMP_SCALABLE_BASIS_AMT numeric(18,2)        null,
   TERM_BASIS_AMT       numeric(18,2)        null,
   PRODS_COMP_TERM_BASIS_AMT numeric(18,2)        null,
   CURR_CD              varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_GL_EXPSR
   add constraint DGEX_PK primary key nonclustered (GL_EXPSR_DID)
go

/*==============================================================*/
/* Table: DIM_GL_POL_LINE                                       */
/*==============================================================*/
create table DIM_GL_POL_LINE (
   GL_POL_LINE_DID      int                  not null,
   POL_LINE_KEY         varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CLAIMS_ORIG_EFF_DT   DATETIME2            null,
   LOC_LIMITS_FL        char(1)              not null,
   POLLUTION_CLEANUP_EXP_FL char(1)              not null,
   CLAIMS_RETROACTIVE_DT DATETIME2            null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_GL_POL_LINE
   add constraint DGPL_PK primary key nonclustered (GL_POL_LINE_DID)
go

/*==============================================================*/
/* Table: DIM_HO_DWLNG                                          */
/*==============================================================*/
create table DIM_HO_DWLNG (
   HO_DWLNG_DID         int                  not null,
   HO_DWLNG_KEY         varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   BUILD_YR             int                  not null,
   PURCH_YR             int                  not null,
   DWLNG_AREA_SF_NO     int                  not null,
   GARAGE_AREA_SF_NO    int                  not null,
   HO_POL_TYPE_CD       varchar(255)         not null,
   HO_POL_TYPE_CCD      varchar(255)         not null,
   HO_POL_TYPE_CST      varchar(255)         not null,
   HO_POL_TYPE_CLT      varchar(255)         not null,
   GARAGE_TYPE_CD       varchar(255)         not null,
   GARAGE_TYPE_CCD      varchar(255)         not null,
   GARAGE_TYPE_CST      varchar(255)         not null,
   GARAGE_TYPE_CLT      varchar(255)         not null,
   PRIM_HEATING_TYPE_CD varchar(255)         not null,
   PRIM_HEATING_TYPE_CCD varchar(255)         not null,
   PRIM_HEATING_TYPE_CST varchar(255)         not null,
   PRIM_HEATING_TYPE_CLT varchar(255)         not null,
   CONSTR_CD            varchar(255)         not null,
   CONSTR_TYPE_CD       varchar(255)         not null,
   CONSTR_TYPE_CCD      varchar(255)         not null,
   CONSTR_TYPE_CST      varchar(255)         not null,
   CONSTR_TYPE_CLT      varchar(255)         not null,
   ELECTRICAL_TYPE_CD   varchar(255)         not null,
   ELECTRICAL_TYPE_CCD  varchar(255)         not null,
   ELECTRICAL_TYPE_CST  varchar(255)         not null,
   ELECTRICAL_TYPE_CLT  varchar(255)         not null,
   OCCPCY_CD            varchar(255)         not null,
   OCCPCY_CCD           varchar(255)         not null,
   OCCPCY_CST           varchar(255)         not null,
   OCCPCY_CLT           varchar(255)         not null,
   PLUMBING_TYPE_CD     varchar(255)         not null,
   PLUMBING_TYPE_CCD    varchar(255)         not null,
   PLUMBING_TYPE_CST    varchar(255)         not null,
   PLUMBING_TYPE_CLT    varchar(255)         not null,
   LOC_TYPE_CD          varchar(255)         not null,
   LOC_TYPE_CCD         varchar(255)         not null,
   LOC_TYPE_CST         varchar(255)         not null,
   LOC_TYPE_CLT         varchar(255)         not null,
   SPRINKLER_TYPE_CD    varchar(255)         not null,
   SPRINKLER_TYPE_CCD   varchar(255)         not null,
   SPRINKLER_TYPE_CST   varchar(255)         not null,
   SPRINKLER_TYPE_CLT   varchar(255)         not null,
   WIRING_TYPE_CD       varchar(255)         not null,
   WIRING_TYPE_CCD      varchar(255)         not null,
   WIRING_TYPE_CST      varchar(255)         not null,
   WIRING_TYPE_CLT      varchar(255)         not null,
   ROOF_TYPE_CD         varchar(255)         not null,
   ROOF_TYPE_CCD        varchar(255)         not null,
   ROOF_TYPE_CST        varchar(255)         not null,
   ROOF_TYPE_CLT        varchar(255)         not null,
   RESIDENCE_TYPE_CD    varchar(255)         not null,
   RESIDENCE_TYPE_CCD   varchar(255)         not null,
   RESIDENCE_TYPE_CST   varchar(255)         not null,
   RESIDENCE_TYPE_CLT   varchar(255)         not null,
   FOUNDATION_TYPE_CD   varchar(255)         not null,
   FOUNDATION_TYPE_CCD  varchar(255)         not null,
   FOUNDATION_TYPE_CST  varchar(255)         not null,
   FOUNDATION_TYPE_CLT  varchar(255)         not null,
   STORIES_CD           varchar(255)         not null,
   STORIES_CCD          varchar(255)         not null,
   STORIES_CST          varchar(255)         not null,
   STORIES_CLT          varchar(255)         not null,
   WIND_CLASS_CD        varchar(255)         not null,
   WIND_CLASS_CCD       varchar(255)         not null,
   WIND_CLASS_CST       varchar(255)         not null,
   WIND_CLASS_CLT       varchar(255)         not null,
   DWLNG_USAGE_CD       varchar(255)         not null,
   DWLNG_USAGE_CCD      varchar(255)         not null,
   DWLNG_USAGE_CST      varchar(255)         not null,
   DWLNG_USAGE_CLT      varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   REPL_COST_AMT        numeric(18,2)        not null,
   FIRE_EXTINGUISHERS_FL char(1)              not null,
   BURGLAR_ALARM_FL     char(1)              not null,
   BURGLAR_ALARM_TYPE_CD varchar(255)         not null,
   BURGLAR_ALARM_TYPE_CCD varchar(255)         not null,
   BURGLAR_ALARM_TYPE_CST varchar(255)         not null,
   BURGLAR_ALARM_TYPE_CLT varchar(255)         not null,
   FIRE_ALARM_FL        char(1)              not null,
   SMOKE_ALARM_FL       char(1)              not null,
   SMOKE_ALARM_ON_ALL_FLOORS_FL char(1)              not null,
   COVERED_PORCH_FL     char(1)              not null,
   COVERED_PORCH_AREA_NO int                  not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_HO_DWLNG
   add constraint DHDW_PK primary key nonclustered (HO_DWLNG_DID)
go

alter table DIM_HO_DWLNG
   add constraint DHDW_AK1 unique (HO_DWLNG_KEY, ETL_ROW_EFF_DTS, ETL_END_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_HO_LOC                                            */
/*==============================================================*/
create table DIM_HO_LOC (
   HO_LOC_DID           int                  not null,
   HO_LOC_KEY           varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   DIST_FIRE_STATION_NO int                  not null,
   DIST_FIRE_HYDRANT_NO int                  not null,
   PROT_CLASS_CD        varchar(255)         not null,
   FLOODING_HAZARD_FL   char(1)              not null,
   NEAR_COMML_FL        char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_HO_LOC
   add constraint DHLO_PK primary key nonclustered (HO_LOC_DID)
go

/*==============================================================*/
/* Table: DIM_HO_SI                                             */
/*==============================================================*/
create table DIM_HO_SI (
   HO_SI_DID            int                  not null,
   HO_SI_KEY            varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ITEM_NO              varchar(255)         not null,
   SCHED_TYPE_CD        varchar(255)         not null,
   SCHED_TYPE_CCD       varchar(255)         not null,
   SCHED_TYPE_CST       varchar(255)         not null,
   SCHED_TYPE_CLT       varchar(255)         not null,
   ITEM_TEXT            varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   INCR_LMT_PCT_CD      varchar(255)         not null,
   INCR_LMT_PCT         numeric(6,3)         null,
   EXPSR_VAL_AMT        numeric(18,2)        not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_HO_SI
   add constraint DHSI_PK primary key nonclustered (HO_SI_DID)
go

/*==============================================================*/
/* Table: DIM_LOC                                               */
/*==============================================================*/
create table DIM_LOC (
   LOC_ID               int                  not null,
   LOC_KEY              varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   LOC_NO               varchar(255)         not null,
   STREET_1_NAME        varchar(255)         not null,
   STREET_2_NAME        varchar(255)         not null,
   STREET_3_NAME        varchar(255)         not null,
   CNTRY_CD             varchar(255)         not null,
   CNTRY_CCD            varchar(255)         not null,
   CNTRY_CST            varchar(255)         not null,
   CNTRY_CLT            varchar(255)         not null,
   REGION_CD            varchar(255)         not null,
   REGION_CCD           varchar(255)         not null,
   REGION_CST           varchar(255)         not null,
   REGION_CLT           varchar(255)         not null,
   CITY_NAME            varchar(255)         not null,
   POSTAL_CD            varchar(15)          not null,
   COUNTY_NAME          varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_LOC
   add constraint DILO1_PK primary key nonclustered (LOC_ID)
go

/*==============================================================*/
/* Table: DIM_LOSS_TYPE_INFO                                    */
/*==============================================================*/
create table DIM_LOSS_TYPE_INFO (
   LOSS_TYPE_INFO_ID    int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   LOSS_TYPE_CD         varchar(255)         not null,
   CONF_LOSS_TYPE_CD    varchar(255)         not null,
   CONF_LOSS_TYPE_SHORT_TEXT varchar(255)         not null,
   CONF_LOSS_TYPE_LONG_TEXT varchar(255)         not null,
   CAUSE_OF_LOSS_CD     varchar(255)         not null,
   CONF_CAUSE_OF_LOSS_CD varchar(255)         not null,
   CONF_CAUSE_OF_LOSS_SHORT_TEXT varchar(255)         not null,
   CONF_CAUSE_OF_LOSS_LONG_TEXT varchar(255)         not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table DIM_LOSS_TYPE_INFO
   add constraint DLTI_PK primary key nonclustered (LOSS_TYPE_INFO_ID)
go

/*==============================================================*/
/* Table: DIM_MONTH                                             */
/*==============================================================*/
create table DIM_MONTH (
   MTH_ID               int                  not null,
   QTR_ID               int                  not null,
   MTH_NO               char(2)              not null,
   MTH_NAME             varchar(255)         not null,
   MTH_BEGIN_DT         DATE                 not null,
   MTH_END_DT           DATE                 not null,
   QTR_NO               char(1)              not null,
   QTR_FIRST_MTH_NO     char(2)              not null,
   QTR_BEGIN_DT         DATE                 not null,
   YR_NO                int                  not null,
   YR_BEGIN_DT          DATE                 not null,
   MTH_OFFSET_NO        int                  not null,
   PME_DT               DATE                 not null,
   PYE_DT               DATE                 not null,
   PYME_DT              DATE                 not null,
   PY2ME_DT             DATE                 not null,
   PME_MTH_ID           int                  not null,
   PYE_MTH_ID           int                  not null,
   PYME_MTH_ID          int                  not null,
   PY2ME_MTH_ID         int                  not null
)
go

alter table DIM_MONTH
   add constraint DIMO_PK primary key nonclustered (MTH_ID)
go

/*==============================================================*/
/* Table: DIM_MONTH_AGING                                       */
/*==============================================================*/
create table DIM_MONTH_AGING (
   MTH_AGING_ID         bigint               not null,
   MTH_AGING_END_ID     int                  not null,
   MTH_AGING_BEGIN_ID   int                  not null,
   YR_AGE_YRS           int                  not null,
   YR_AGE_QTRS          int                  not null,
   YR_AGE_MTHS          int                  not null,
   QTR_AGE_QTRS         int                  not null,
   QTR_AGE_MTHS         int                  not null,
   MTH_AGE_MTHS         int                  not null
)
go

alter table DIM_MONTH_AGING
   add constraint DMAG_PK primary key nonclustered (MTH_AGING_ID)
go

/*==============================================================*/
/* Table: DIM_NAMED_INSURED                                     */
/*==============================================================*/
create table DIM_NAMED_INSURED (
   NAMED_INSURED_ID     int                  not null,
   NAMED_INSURED_KEY    varchar(100)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   POL_KEY              varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   MAILING_ADDR_1       varchar(255)         not null,
   MAILING_ADDR_2       varchar(255)         not null,
   MAILING_ADDR_3       varchar(255)         not null,
   MAILING_CITY         varchar(255)         not null,
   MAILING_STATE_CD     varchar(255)         not null,
   MAILING_ZIP_CD       varchar(15)          not null,
   PRIM_INSURED_FL      char(1)              not null,
   INSURED_NAME         varchar(255)         not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_NAMED_INSURED
   add constraint DNIN_PK primary key nonclustered (NAMED_INSURED_ID)
go

alter table DIM_NAMED_INSURED
   add constraint DNIN_AK1 unique (NAMED_INSURED_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

/*==============================================================*/
/* Table: DIM_ORGANIZATION                                      */
/*==============================================================*/
create table DIM_ORGANIZATION (
   ORG_ID               int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   MASTER_CO_CD         varchar(255)         not null,
   CONF_MASTER_CO_CD    varchar(255)         not null,
   CONF_MASTER_CO_SHORT_TEXT varchar(255)         not null,
   CONF_MASTER_CO_LONG_TEXT varchar(255)         not null,
   POL_CO_CD            varchar(255)         not null,
   CONF_POL_CO_CD       varchar(255)         not null,
   CONF_POL_CO_SHORT_TEXT varchar(255)         not null,
   CONF_POL_CO_LONG_TEXT varchar(255)         not null,
   BRANCH_OFC_CD        varchar(255)         not null,
   PROFIT_CTR_CD        varchar(255)         not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table DIM_ORGANIZATION
   add constraint DIOR_PK primary key nonclustered (ORG_ID)
go

/*==============================================================*/
/* Table: DIM_PA_VEH                                            */
/*==============================================================*/
create table DIM_PA_VEH (
   PA_VEH_DID           int                  not null,
   VEH_KEY              varchar(100)         not null,
   ETL_END_EFF_DTS      DATETIME2            not null,
   ETL_END_EXP_DTS      DATETIME2            not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   VEH_NO               varchar(255)         not null,
   MODEL_YR             int                  not null,
   MAKE_NAME            varchar(255)         not null,
   MODEL_NAME           varchar(255)         not null,
   VIN_NO               varchar(255)         not null,
   COLOR_NAME           varchar(255)         not null,
   BODY_TYPE_CD         varchar(255)         not null,
   BODY_TYPE_CCD        varchar(255)         not null,
   BODY_TYPE_CST        varchar(255)         not null,
   BODY_TYPE_CLT        varchar(255)         not null,
   VEH_TYPE_CD          varchar(255)         not null,
   VEH_TYPE_CCD         varchar(255)         not null,
   VEH_TYPE_CST         varchar(255)         not null,
   VEH_TYPE_CLT         varchar(255)         not null,
   LIC_JURS_CD          varchar(255)         not null,
   LIC_JURS_CCD         varchar(255)         not null,
   LIC_JURS_CST         varchar(255)         not null,
   LIC_JURS_CLT         varchar(255)         not null,
   LIC_PLATE_NO         varchar(255)         not null,
   LEASE_RENTAL_FL      char(1)              not null,
   LEN_OF_LEASE_CD      varchar(255)         not null,
   LEN_OF_LEASE_CCD     varchar(255)         not null,
   LEN_OF_LEASE_CST     varchar(255)         not null,
   LEN_OF_LEASE_CLT     varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   ORIG_LIST_PRICE_AMT  numeric(18,2)        not null,
   STATED_VAL_AMT       numeric(18,2)        not null,
   PRIM_USE_CD          varchar(255)         not null,
   PRIM_USE_CCD         varchar(255)         not null,
   PRIM_USE_CST         varchar(255)         not null,
   PRIM_USE_CLT         varchar(255)         not null,
   ANNUAL_MILES_NO      int                  not null,
   COMMUTING_MILES_NO   int                  not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_PA_VEH
   add constraint DPVE_AK1 unique (VEH_KEY, ETL_END_EFF_DTS, ETL_ROW_EFF_DTS)
go

alter table DIM_PA_VEH
   add constraint DPVE_PK primary key nonclustered (PA_VEH_DID)
go

/*==============================================================*/
/* Table: DIM_POLICY                                            */
/*==============================================================*/
create table DIM_POLICY (
   POLICY_ID            int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   LAST_STATUS_ID       int                  null,
   LAST_STATUS_PROC_DTS DATETIME2            null,
   LAST_STATUS_EFF_DT   DATE                 null,
   POL_KEY              varchar(100)         not null,
   POL_NO               varchar(255)         not null,
   POL_EFF_DT           DATE                 not null,
   POL_EXP_DT           DATE                 not null,
   ORIG_EFF_DT          DATE                 null,
   POL_YR               int                  not null,
   POL_FORM_CD          varchar(255)         not null,
   PRIM_STATE_CD        varchar(255)         not null,
   CONF_PRIM_STATE_CD   varchar(255)         not null,
   CONF_PRIM_STATE_SHORT_TEXT varchar(255)         not null,
   CONF_PRIM_STATE_LONG_TEXT varchar(255)         not null,
   POL_TERM             varchar(255)         not null,
   SIC_CD               varchar(255)         not null,
   NEW_BUS_FL           char(1)              not null,
   ENTITY_TYPE_CD       varchar(255)         not null,
   BILL_PLAN_CD         varchar(255)         not null,
   PAY_PLAN_CD          varchar(255)         not null,
   AUDIT_PLAN_CD        varchar(255)         not null,
   ELECT_FUNDS_TFR_FL   char(1)              not null,
   CLAIMS_MADE_FL       char(1)              not null,
   RETRO_FL             char(1)              not null,
   UNDERWRITER_CD       varchar(255)         not null,
   CONF_UNDERWRITER_CD  varchar(255)         not null,
   CONF_UNDERWRITER_SHORT_TEXT varchar(255)         not null,
   CONF_UNDERWRITER_LONG_TEXT varchar(255)         not null,
   ROW_PROC_DTS         DATETIME2            not null,
   END_EFF_DT           DATE                 not null,
   END_EXP_DT           DATE                 not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table DIM_POLICY
   add constraint DIPO_PK primary key nonclustered (POLICY_ID)
go

/*==============================================================*/
/* Index: DIPO_DPST_XFK                                         */
/*==============================================================*/




create nonclustered index DIPO_DPST_XFK on DIM_POLICY (LAST_STATUS_ID ASC)
go

/*==============================================================*/
/* Table: DIM_POLICY_STATUS                                     */
/*==============================================================*/
create table DIM_POLICY_STATUS (
   POL_STATUS_ID        int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   STATUS_CD            varchar(255)         not null,
   REASON_CD            varchar(255)         not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table DIM_POLICY_STATUS
   add constraint DPST_PK primary key nonclustered (POL_STATUS_ID)
go

alter table DIM_POLICY_STATUS
   add constraint DPST_AK1 unique (SOURCE_SYSTEM, STATUS_CD, REASON_CD)
go

/*==============================================================*/
/* Table: DIM_PRODUCT                                           */
/*==============================================================*/
create table DIM_PRODUCT (
   PRODUCT_ID           int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   LOB_CD               varchar(255)         not null,
   CONF_LOB_CD          varchar(255)         not null,
   CONF_LOB_SHORT_TEXT  varchar(255)         not null,
   CONF_LOB_LONG_TEXT   varchar(255)         not null,
   LINE_DIV_CD          varchar(255)         not null,
   CONF_LINE_DIV_CD     varchar(255)         not null,
   CONF_LINE_DIV_SHORT_TEXT varchar(255)         not null,
   CONF_LINE_DIV_LONG_TEXT varchar(255)         not null,
   PROD_CD              varchar(255)         not null,
   PROG_CD              varchar(255)         not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table DIM_PRODUCT
   add constraint DIPR_PK primary key nonclustered (PRODUCT_ID)
go

/*==============================================================*/
/* Table: DIM_QUARTER                                           */
/*==============================================================*/
create table DIM_QUARTER (
   QTR_ID               int                  not null,
   QTR_NO               char(1)              not null,
   QTR_FIRST_MTH_NO     char(2)              not null,
   QTR_BEGIN_DT         DATE                 not null,
   QTR_END_DT           DATE                 not null,
   YR_NO                int                  not null,
   YR_BEGIN_DT          DATE                 not null,
   YR_END_DT            DATE                 not null,
   QTR_OFFSET_NO        int                  not null
)
go

alter table DIM_QUARTER
   add constraint DIQU_PK primary key nonclustered (QTR_ID)
go

/*==============================================================*/
/* Table: DIM_RECORD_TYPE                                       */
/*==============================================================*/
create table DIM_RECORD_TYPE (
   REC_TYPE_ID          int                  not null,
   REC_TYPE_KEY         varchar(100)         not null,
   REC_TYPE_CD          varchar(255)         not null,
   REC_TYPE_TEXT        varchar(255)         not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table DIM_RECORD_TYPE
   add constraint DRTY_PK primary key nonclustered (REC_TYPE_ID)
go

/*==============================================================*/
/* Table: DIM_REGION                                            */
/*==============================================================*/
create table DIM_REGION (
   REGION_ID            int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   REGION_CD            varchar(255)         not null,
   CONF_REGION_CD       varchar(255)         not null,
   CONF_REGION_SHORT_TEXT varchar(255)         not null,
   CONF_REGION_LONG_TEXT varchar(255)         not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table DIM_REGION
   add constraint DIRE_PK primary key nonclustered (REGION_ID)
go

/*==============================================================*/
/* Table: DIM_VENDOR                                            */
/*==============================================================*/
create table DIM_VENDOR (
   VENDOR_ID            int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   VNDR_KEY             varchar(100)         not null,
   VNDR_CD              varchar(255)         not null,
   VNDR_TYPE_CD         varchar(255)         not null,
   ENTITY_TYPE_CD       varchar(255)         not null,
   VNDR_NAME            varchar(255)         not null,
   VNDR_ADDR_1          varchar(255)         not null,
   VNDR_ADDR_2          varchar(255)         not null,
   VNDR_ADDR_3          varchar(255)         not null,
   VNDR_CITY            varchar(255)         not null,
   VNDR_STATE_CD        varchar(255)         not null,
   CONF_VNDR_STATE_CD   varchar(255)         not null,
   CONF_VNDR_STATE_SHORT_TEXT varchar(255)         not null,
   CONF_VNDR_STATE_LONG_TEXT varchar(255)         not null,
   VNDR_ZIP_CD          varchar(15)          not null,
   VNDR_TAX_ID          varchar(15)          not null,
   VNDR_PHONE_NO        varchar(255)         not null,
   ETL_ROW_EFF_DTS      DATETIME2            not null,
   ETL_ROW_EXP_DTS      DATETIME2            not null,
   ROW_PROC_DTS         DATETIME2            not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_LATE_ARRIVING_FL char(1)              not null,
   ETL_CURR_ROW_FL      char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table DIM_VENDOR
   add constraint DIVE_PK primary key nonclustered (VENDOR_ID)
go

/*==============================================================*/
/* Table: DIM_YEAR                                              */
/*==============================================================*/
create table DIM_YEAR (
   YR_ID                int                  not null,
   YR_BEGIN_DT          DATE                 not null,
   PYE_DT               DATE                 not null
)
go

alter table DIM_YEAR
   add constraint DIYE_PK primary key nonclustered (YR_ID)
go

/*==============================================================*/
/* Table: ETL_EDW_CYCLE_STATISTIC                               */
/*==============================================================*/
create table ETL_EDW_CYCLE_STATISTIC (
   SOURCE_SYSTEM        varchar(10)          not null,
   CYCLE_DT             DATETIME2            not null,
   ENTITY               varchar(50)          not null,
   TOT_ODS_BASE_CNT     int                  not null,
   TOT_ODS_DELTA_CNT    int                  not null,
   TOT_INSERT_BASE_CNT  int                  not null,
   TOT_UPDATE_BASE_CNT  int                  not null,
   TOT_INSERT_DELTA_CNT int                  not null,
   TOT_UPDATE_DELTA_CNT int                  not null,
   TOT_EDW_CNT          int                  not null,
   TOT_EDW_INSERT_CNT   int                  not null,
   TOT_EDW_UPDATE_CNT   int                  not null,
   BALANCED_FL          char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table ETL_EDW_CYCLE_STATISTIC
   add constraint EECS1_PK primary key nonclustered (SOURCE_SYSTEM, CYCLE_DT, ENTITY)
go

/*==============================================================*/
/* Table: ETL_EDW_CYCLE_STATUS                                  */
/*==============================================================*/
create table ETL_EDW_CYCLE_STATUS (
   CYCLE_ID             int                  not null,
   CYCLE_START_DATE     DATETIME2            not null,
   CYCLE_END_DATE       DATETIME2            null,
   CYCLE_STATUS         varchar(255)         not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table ETL_EDW_CYCLE_STATUS
   add constraint EECS_PK primary key nonclustered (CYCLE_ID)
go

/*==============================================================*/
/* Table: ETL_EDW_ROLLUP_CONTROL                                */
/*==============================================================*/
create table ETL_EDW_ROLLUP_CONTROL (
   LOAD_ID              int                  not null,
   ACCTG_PRD_ID         int                  not null,
   ROLLUP_TYPE_CD       varchar(255)         not null,
   COMPLETE_DTS         DATETIME2            null,
   RELOAD_FL            char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table ETL_EDW_ROLLUP_CONTROL
   add constraint EERC_PK primary key nonclustered (LOAD_ID)
go

/*==============================================================*/
/* Table: FACT_AP_TRAN                                          */
/*==============================================================*/
create table FACT_AP_TRAN (
   AP_JRNL_TID          int                  not null,
   AP_JRNL_KEY          varchar(100)         not null,
   PRDCR_DID            int                  not null,
   CR_PYBL_OF_PRDCR_DID int                  not null,
   PRDCR_CD_DID         int                  not null,
   CHRG_COMM_DID        int                  not null,
   BILL_INV_DID         int                  not null,
   BILL_INV_ITEM_DID    int                  not null,
   BILL_POL_DID         int                  not null,
   BILL_POL_PRD_DID     int                  not null,
   COMM_RED_DID         int                  not null,
   PRDCR_STMNT_DID      int                  not null,
   PRDCR_PMT_DID        int                  not null,
   PYBL_RECEIVER_STMNT_DID int                  not null,
   AGCY_CYCLE_DISTR_DID int                  not null,
   DISTR_ITEM_DID       int                  not null,
   NEG_WO_DID           int                  not null,
   WO_DID               int                  not null,
   TRANS_DT_ID          int                  not null,
   DISTR_DID            int                  null,
   SOURCE_SYSTEM        varchar(10)          not null,
   INCOMMING_PRDCR_PMT_KEY varchar(100)         not null,
   PRDCR_PYBL_TFR_KEY   varchar(100)         not null,
   POL_COMM_KEY         varchar(100)         not null,
   TRANS_DTS            DATETIME2            not null,
   TRANS_NO             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   PYBL_CRITERIA_CD     varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   CASH_BAL             numeric(18,2)        not null,
   COMM_ADVANCE_BAL     numeric(18,2)        not null,
   UNAPPLIED_BAL        numeric(18,2)        not null,
   COMM_PYBL_BAL        numeric(18,2)        not null,
   COMM_RSRV_BAL        numeric(18,2)        not null,
   NEG_COMM_ADJ_BAL     numeric(18,2)        not null,
   NEG_WO_INC_BAL       numeric(18,2)        not null,
   COMM_EXP_BAL         numeric(18,2)        not null,
   POS_COMM_ADJ_BAL     numeric(18,2)        not null,
   WO_EXP_BAL           numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_AP_TRAN
   add constraint FATR1_PK primary key nonclustered (AP_JRNL_TID)
go

alter table FACT_AP_TRAN
   add constraint FATR_AK1 unique (AP_JRNL_KEY)
go

/*==============================================================*/
/* Index: FATR_DBDI2_XFK                                        */
/*==============================================================*/




create nonclustered index FATR_DBDI2_XFK on FACT_AP_TRAN (AGCY_CYCLE_DISTR_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBDI3_XFK                                        */
/*==============================================================*/




create nonclustered index FATR_DBDI3_XFK on FACT_AP_TRAN (DISTR_ITEM_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBCC1_XFK                                        */
/*==============================================================*/




create nonclustered index FATR_DBCC1_XFK on FACT_AP_TRAN (CHRG_COMM_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBIN1_XFK                                        */
/*==============================================================*/




create nonclustered index FATR_DBIN1_XFK on FACT_AP_TRAN (BILL_INV_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBII1_XFK                                        */
/*==============================================================*/




create nonclustered index FATR_DBII1_XFK on FACT_AP_TRAN (BILL_INV_ITEM_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBPR1_XFK                                        */
/*==============================================================*/




create nonclustered index FATR_DBPR1_XFK on FACT_AP_TRAN (PRDCR_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBPR2_XFK                                        */
/*==============================================================*/




create nonclustered index FATR_DBPR2_XFK on FACT_AP_TRAN (CR_PYBL_OF_PRDCR_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBCR_XFK                                         */
/*==============================================================*/




create nonclustered index FATR_DBCR_XFK on FACT_AP_TRAN (COMM_RED_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBPP_XFK                                         */
/*==============================================================*/




create nonclustered index FATR_DBPP_XFK on FACT_AP_TRAN (PRDCR_PMT_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBPC_XFK                                         */
/*==============================================================*/




create nonclustered index FATR_DBPC_XFK on FACT_AP_TRAN (PRDCR_CD_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBPS_XFK                                         */
/*==============================================================*/




create nonclustered index FATR_DBPS_XFK on FACT_AP_TRAN (PRDCR_STMNT_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBPS1_XFK                                        */
/*==============================================================*/




create nonclustered index FATR_DBPS1_XFK on FACT_AP_TRAN (PYBL_RECEIVER_STMNT_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DIDA1_XFK                                        */
/*==============================================================*/




create nonclustered index FATR_DIDA1_XFK on FACT_AP_TRAN (TRANS_DT_ID ASC)
go

/*==============================================================*/
/* Index: FATR_DBWR_XFK                                         */
/*==============================================================*/




create nonclustered index FATR_DBWR_XFK on FACT_AP_TRAN (WO_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBNW_XFK                                         */
/*==============================================================*/




create nonclustered index FATR_DBNW_XFK on FACT_AP_TRAN (NEG_WO_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBDI4_XFK                                        */
/*==============================================================*/




create nonclustered index FATR_DBDI4_XFK on FACT_AP_TRAN (DISTR_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBPO1_XFK                                        */
/*==============================================================*/




create nonclustered index FATR_DBPO1_XFK on FACT_AP_TRAN (BILL_POL_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBPP2_XFK                                        */
/*==============================================================*/




create nonclustered index FATR_DBPP2_XFK on FACT_AP_TRAN (BILL_POL_PRD_DID ASC)
go

/*==============================================================*/
/* Table: FACT_AR_AGING                                         */
/*==============================================================*/
create table FACT_AR_AGING (
   FACT_AR_AGING_ID     int                  not null,
   SNAPSHOT_PRD_ID      int                  not null,
   BILL_ACCT_DID        int                  not null,
   BILL_POL_DID         int                  not null,
   BILL_POL_PRD_DID     int                  not null,
   BILL_INV_DID         int                  not null,
   PRDCR_DID            int                  not null,
   PMT_DUE_DT_ID        int                  not null,
   CHRG_EFF_DT_ID       int                  not null,
   INV_PROC_DT_ID       int                  not null,
   INV_NO               varchar(255)         not null,
   POL_INV_NO           varchar(255)         null,
   INV_ITEM_TYPE_CD     varchar(255)         not null,
   INSTALLMENT_FL       char(1)              not null,
   PMT_DUE_DTS          DATETIME2            null,
   CHRG_EFF_DTS         DATETIME2            null,
   DUE_DT_AGE_DAYS      int                  not null,
   EFF_DT_AGE_DAYS      int                  not null,
   CURR_CD              varchar(255)         not null,
   CHRG_AMT             numeric(18,2)        not null,
   BILLED_AMT           numeric(18,2)        not null,
   DUE_AMT              numeric(18,2)        not null,
   PD_AMT               numeric(18,2)        not null,
   WO_AMT               numeric(18,2)        not null,
   NET_DUE_AMT          numeric(18,2)        not null,
   PREM_FL              char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_AR_AGING
   add constraint FAAG_PK primary key nonclustered (FACT_AR_AGING_ID)
go

/*==============================================================*/
/* Index: FAAG_DBIN_XFK                                         */
/*==============================================================*/




create nonclustered index FAAG_DBIN_XFK on FACT_AR_AGING (BILL_INV_DID ASC)
go

/*==============================================================*/
/* Index: FAAG_DBAC_XFK                                         */
/*==============================================================*/




create nonclustered index FAAG_DBAC_XFK on FACT_AR_AGING (BILL_ACCT_DID ASC)
go

/*==============================================================*/
/* Index: FAAG_DBPR_XFK                                         */
/*==============================================================*/




create nonclustered index FAAG_DBPR_XFK on FACT_AR_AGING (PRDCR_DID ASC)
go

/*==============================================================*/
/* Index: FAAG_DIMO_XFK                                         */
/*==============================================================*/




create nonclustered index FAAG_DIMO_XFK on FACT_AR_AGING (SNAPSHOT_PRD_ID ASC)
go

/*==============================================================*/
/* Index: FAAG_DBPP_XFK                                         */
/*==============================================================*/




create nonclustered index FAAG_DBPP_XFK on FACT_AR_AGING (BILL_POL_PRD_DID ASC)
go

/*==============================================================*/
/* Index: FAAG_DIDA_XFK                                         */
/*==============================================================*/




create nonclustered index FAAG_DIDA_XFK on FACT_AR_AGING (PMT_DUE_DT_ID ASC)
go

/*==============================================================*/
/* Index: FAAG_DIDA1_XFK                                        */
/*==============================================================*/




create nonclustered index FAAG_DIDA1_XFK on FACT_AR_AGING (CHRG_EFF_DT_ID ASC)
go

/*==============================================================*/
/* Index: FAAG_DIDA2_XFK                                        */
/*==============================================================*/




create nonclustered index FAAG_DIDA2_XFK on FACT_AR_AGING (INV_PROC_DT_ID ASC)
go

/*==============================================================*/
/* Index: FAAG_DBPO_XFK                                         */
/*==============================================================*/




create nonclustered index FAAG_DBPO_XFK on FACT_AR_AGING (BILL_POL_DID ASC)
go

/*==============================================================*/
/* Table: FACT_AR_BALANCING                                     */
/*==============================================================*/
create table FACT_AR_BALANCING (
   FACT_AR_BAL_ID       int                  not null,
   SNAPSHOT_PRD_ID      int                  not null,
   BILL_POL_DID         int                  not null,
   BILL_POL_PRD_DID     int                  not null,
   CHRG_PAT_DID         int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   CURR_CD              varchar(255)         not null,
   ADVANCE_PREM_BEG_BAL_AMT numeric(18,2)        not null,
   ADVANCE_PREM_NEW_AMT numeric(18,2)        not null,
   ADVANCE_PREM_RECLASS_AMT numeric(18,2)        not null,
   ADVANCE_PREM_CASH_CREDIT_AMT numeric(18,2)        not null,
   ADVANCE_PREM_WO_AMT  numeric(18,2)        not null,
   ADVANCE_PREM_END_BAL_AMT numeric(18,2)        not null,
   WRITTEN_PREM_BEG_BAL_AMT numeric(18,2)        not null,
   WRITTEN_PREM_NEW_AMT numeric(18,2)        not null,
   WRITTEN_PREM_RECLASS_AMT numeric(18,2)        not null,
   WRITTEN_PREM_CASH_CREDIT_AMT numeric(18,2)        not null,
   WRITTEN_PREM_WO_AMT  numeric(18,2)        not null,
   WRITTEN_PREM_END_BAL_AMT numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_AR_BALANCING
   add constraint FABA_PK primary key nonclustered (FACT_AR_BAL_ID)
go

/*==============================================================*/
/* Index: FABA_DIMO_XFK                                         */
/*==============================================================*/




create nonclustered index FABA_DIMO_XFK on FACT_AR_BALANCING (SNAPSHOT_PRD_ID ASC)
go

/*==============================================================*/
/* Index: FABA_DBPP_XFK                                         */
/*==============================================================*/




create nonclustered index FABA_DBPP_XFK on FACT_AR_BALANCING (BILL_POL_PRD_DID ASC)
go

/*==============================================================*/
/* Index: FABA_DBPO_XFK                                         */
/*==============================================================*/




create nonclustered index FABA_DBPO_XFK on FACT_AR_BALANCING (BILL_POL_DID ASC)
go

/*==============================================================*/
/* Index: FABA_DBCP_XFK                                         */
/*==============================================================*/




create nonclustered index FABA_DBCP_XFK on FACT_AR_BALANCING (CHRG_PAT_DID ASC)
go

/*==============================================================*/
/* Table: FACT_AR_TRAN                                          */
/*==============================================================*/
create table FACT_AR_TRAN (
   BILL_AR_JRNL_TID     int                  not null,
   BILL_AR_JRNL_KEY     varchar(100)         not null,
   CHRG_DID             int                  not null,
   CHRG_PAT_DID         int                  not null,
   BILL_POL_PRD_DID     int                  not null,
   BILL_POL_DID         int                  not null,
   BILL_ACCT_DID        int                  not null,
   BILL_INV_DID         int                  not null,
   BILL_INV_ITEM_DID    int                  not null,
   DISTR_DID            int                  not null,
   DISTR_ITEM_DID       int                  not null,
   CHRG_COMM_DID        int                  not null,
   PRDCR_DID            int                  not null,
   WO_DID               int                  not null,
   TRANS_DT_ID          int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TRANS_DTS            DATETIME2            not null,
   TRANS_NO             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   BILLED_BAL           numeric(18,2)        not null,
   CASH_BAL             numeric(18,2)        not null,
   COLTRL_HELD_BAL      numeric(18,2)        not null,
   DUE_BAL              numeric(18,2)        not null,
   UNAPPLIED_BAL        numeric(18,2)        not null,
   UB_BAL               numeric(18,2)        not null,
   COLTRL_RSRV_BAL      numeric(18,2)        not null,
   COMM_RSRV_BAL        numeric(18,2)        not null,
   RSRV_BAL             numeric(18,2)        not null,
   UNEARNED_BAL         numeric(18,2)        not null,
   NEG_WO_INC_BAL       numeric(18,2)        not null,
   REVENUE_BAL          numeric(18,2)        not null,
   EXP_BAL              numeric(18,2)        not null,
   WO_BAL               numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_AR_TRAN
   add constraint FATR_PK primary key nonclustered (BILL_AR_JRNL_TID)
go

alter table FACT_AR_TRAN
   add constraint FATR1_AK1 unique (BILL_AR_JRNL_KEY)
go

/*==============================================================*/
/* Index: FATR_DBIN_XFK                                         */
/*==============================================================*/




create nonclustered index FATR_DBIN_XFK on FACT_AR_TRAN (BILL_INV_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBAC_XFK                                         */
/*==============================================================*/




create nonclustered index FATR_DBAC_XFK on FACT_AR_TRAN (BILL_ACCT_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBCH_XFK                                         */
/*==============================================================*/




create nonclustered index FATR_DBCH_XFK on FACT_AR_TRAN (CHRG_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBII_XFK                                         */
/*==============================================================*/




create nonclustered index FATR_DBII_XFK on FACT_AR_TRAN (BILL_INV_ITEM_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBDI_XFK                                         */
/*==============================================================*/




create nonclustered index FATR_DBDI_XFK on FACT_AR_TRAN (DISTR_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBDI1_XFK                                        */
/*==============================================================*/




create nonclustered index FATR_DBDI1_XFK on FACT_AR_TRAN (DISTR_ITEM_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DIDA_XFK                                         */
/*==============================================================*/




create nonclustered index FATR_DIDA_XFK on FACT_AR_TRAN (TRANS_DT_ID ASC)
go

/*==============================================================*/
/* Index: FATR_DBPP1_XFK                                        */
/*==============================================================*/




create nonclustered index FATR_DBPP1_XFK on FACT_AR_TRAN (BILL_POL_PRD_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBPR_XFK                                         */
/*==============================================================*/




create nonclustered index FATR_DBPR_XFK on FACT_AR_TRAN (PRDCR_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBCC_XFK                                         */
/*==============================================================*/




create nonclustered index FATR_DBCC_XFK on FACT_AR_TRAN (CHRG_COMM_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBWR1_XFK                                        */
/*==============================================================*/




create nonclustered index FATR_DBWR1_XFK on FACT_AR_TRAN (WO_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBPO_XFK                                         */
/*==============================================================*/




create nonclustered index FATR_DBPO_XFK on FACT_AR_TRAN (BILL_POL_DID ASC)
go

/*==============================================================*/
/* Index: FATR_DBCP_XFK                                         */
/*==============================================================*/




create nonclustered index FATR_DBCP_XFK on FACT_AR_TRAN (CHRG_PAT_DID ASC)
go

/*==============================================================*/
/* Table: FACT_BILL_CASH_BALANCING                              */
/*==============================================================*/
create table FACT_BILL_CASH_BALANCING (
   CASH_BALANCING_ID    int                  not null,
   TRANS_MTH_ID         int                  not null,
   BILL_ACCT_DID        int                  not null,
   BILL_POL_PRD_DID     int                  not null,
   BILL_POL_DID         int                  not null,
   PRDCR_DID            int                  not null,
   SUSP_PMT_DID         int                  not null,
   MON_RCVD_DID         int                  not null,
   UNAPPLIED_FUND_DID   int                  not null,
   DISB_DID             int                  not null,
   SRCE_JRNL_NAME       varchar(255)         not null,
   SUSP_TYPE_NAME       varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   CASH_BAL             numeric(18,2)        not null,
   UNAPPLIED_BAL        numeric(18,2)        not null,
   SUSP_BAL             numeric(18,2)        not null,
   COLTRL_HELD_BAL      numeric(18,2)        not null,
   COLTRL_RSRV_BAL      numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_BILL_CASH_BALANCING
   add constraint FBCB_PK primary key nonclustered (CASH_BALANCING_ID)
go

/*==============================================================*/
/* Index: FBCB_DIMO_XFK                                         */
/*==============================================================*/




create nonclustered index FBCB_DIMO_XFK on FACT_BILL_CASH_BALANCING (TRANS_MTH_ID ASC)
go

/*==============================================================*/
/* Index: FBCB_DBAC_XFK                                         */
/*==============================================================*/




create nonclustered index FBCB_DBAC_XFK on FACT_BILL_CASH_BALANCING (BILL_ACCT_DID ASC)
go

/*==============================================================*/
/* Index: FBCB_DBPR_XFK                                         */
/*==============================================================*/




create nonclustered index FBCB_DBPR_XFK on FACT_BILL_CASH_BALANCING (PRDCR_DID ASC)
go

/*==============================================================*/
/* Index: FBCB_DBSP_XFK                                         */
/*==============================================================*/




create nonclustered index FBCB_DBSP_XFK on FACT_BILL_CASH_BALANCING (SUSP_PMT_DID ASC)
go

/*==============================================================*/
/* Index: FBCB_DBPP_XFK                                         */
/*==============================================================*/




create nonclustered index FBCB_DBPP_XFK on FACT_BILL_CASH_BALANCING (BILL_POL_PRD_DID ASC)
go

/*==============================================================*/
/* Index: FBCB_DBPO_XFK                                         */
/*==============================================================*/




create nonclustered index FBCB_DBPO_XFK on FACT_BILL_CASH_BALANCING (BILL_POL_DID ASC)
go

/*==============================================================*/
/* Index: FBCB_DBMR_XFK                                         */
/*==============================================================*/




create nonclustered index FBCB_DBMR_XFK on FACT_BILL_CASH_BALANCING (MON_RCVD_DID ASC)
go

/*==============================================================*/
/* Index: FBCB_DBUF_XFK                                         */
/*==============================================================*/




create nonclustered index FBCB_DBUF_XFK on FACT_BILL_CASH_BALANCING (UNAPPLIED_FUND_DID ASC)
go

/*==============================================================*/
/* Index: FBCB_DBDI_XFK                                         */
/*==============================================================*/




create nonclustered index FBCB_DBDI_XFK on FACT_BILL_CASH_BALANCING (DISB_DID ASC)
go

/*==============================================================*/
/* Table: FACT_BILL_CASH_TRAN                                   */
/*==============================================================*/
create table FACT_BILL_CASH_TRAN (
   CASH_JRNL_TID        int                  not null,
   CASH_JRNL_KEY        varchar(100)         not null,
   BILL_ACCT_DID        int                  not null,
   MON_RCVD_DID         int                  not null,
   PRDCR_DID            int                  not null,
   DISB_DID             int                  not null,
   NEG_WO_DID           int                  not null,
   UNAPPLIED_FUND_DID   int                  not null,
   SUSP_PMT_DID         int                  not null,
   TRANS_DT_ID          int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   COLTRL_REQ_KEY       varchar(100)         not null,
   SRCE_COLTRL_REQ_KEY  varchar(100)         not null,
   COLTRL_KEY           varchar(100)         not null,
   TRANS_DTS            DATETIME2            not null,
   TRANS_NO             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   WO_CHANNEL_CD        varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   CASH_BAL             numeric(18,2)        not null,
   COLTRL_HELD_BAL      numeric(18,2)        not null,
   UNAPPLIED_BAL        numeric(18,2)        not null,
   COLTRL_RSRV_BAL      numeric(18,2)        not null,
   NEG_WO_INC_BAL       numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_BILL_CASH_TRAN
   add constraint FBCT_PK primary key nonclustered (CASH_JRNL_TID)
go

alter table FACT_BILL_CASH_TRAN
   add constraint FBCT_AK1 unique (CASH_JRNL_KEY)
go

/*==============================================================*/
/* Index: FBCT_DBAC_XFK                                         */
/*==============================================================*/




create nonclustered index FBCT_DBAC_XFK on FACT_BILL_CASH_TRAN (BILL_ACCT_DID ASC)
go

/*==============================================================*/
/* Index: FBCT_DBDI_XFK                                         */
/*==============================================================*/




create nonclustered index FBCT_DBDI_XFK on FACT_BILL_CASH_TRAN (DISB_DID ASC)
go

/*==============================================================*/
/* Index: FBCT_DBPR_XFK                                         */
/*==============================================================*/




create nonclustered index FBCT_DBPR_XFK on FACT_BILL_CASH_TRAN (PRDCR_DID ASC)
go

/*==============================================================*/
/* Index: FBCT_DBMR_XFK                                         */
/*==============================================================*/




create nonclustered index FBCT_DBMR_XFK on FACT_BILL_CASH_TRAN (MON_RCVD_DID ASC)
go

/*==============================================================*/
/* Index: FBCT_DBSP_XFK                                         */
/*==============================================================*/




create nonclustered index FBCT_DBSP_XFK on FACT_BILL_CASH_TRAN (SUSP_PMT_DID ASC)
go

/*==============================================================*/
/* Index: FBCT_DIDA_XFK                                         */
/*==============================================================*/




create nonclustered index FBCT_DIDA_XFK on FACT_BILL_CASH_TRAN (TRANS_DT_ID ASC)
go

/*==============================================================*/
/* Index: FBCT_DBNW_XFK                                         */
/*==============================================================*/




create nonclustered index FBCT_DBNW_XFK on FACT_BILL_CASH_TRAN (NEG_WO_DID ASC)
go

/*==============================================================*/
/* Index: FBCT_DBUF_XFK                                         */
/*==============================================================*/




create nonclustered index FBCT_DBUF_XFK on FACT_BILL_CASH_TRAN (UNAPPLIED_FUND_DID ASC)
go

/*==============================================================*/
/* Table: FACT_BILL_COMM_CAL_YR                                 */
/*==============================================================*/
create table FACT_BILL_COMM_CAL_YR (
   COMM_SNAPSHOT_ID     int                  not null,
   TRANS_MTH_ID         int                  not null,
   PRDCR_DID            int                  not null,
   PRDCR_CD_DID         int                  not null,
   BILL_ACCT_DID        int                  not null,
   BILL_POL_DID         int                  not null,
   BILL_POL_PRD_DID     int                  not null,
   CHRG_DID             int                  not null,
   COMM_PLAN_DID        int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ROLE_CD              varchar(255)         not null,
   PMT_TYPE_CD          varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   AGNT_COMM_RSRV_AMT   numeric(18,2)        not null,
   AGNT_OS_RSRV_AMT     numeric(18,2)        not null,
   AGNT_COMM_EARNED_AMT numeric(18,2)        not null,
   AGNT_COMM_ADJ_AMT    numeric(18,2)        not null,
   BONUS_COMM_AMT       numeric(18,2)        not null,
   INCENTIVE_COMM_AMT   numeric(18,2)        not null,
   AGCY_COMM_EARNED_AMT numeric(18,2)        not null,
   STD_COMM_PD_AMT      numeric(18,2)        not null,
   NORMAL_COMM_PD_AMT   numeric(18,2)        not null,
   AGCY_PMT_RCVD_AMT    numeric(18,2)        not null,
   ADVANCE_COMM_PD_AMT  numeric(18,2)        not null,
   ADVANCE_COMM_REPAID_AMT numeric(18,2)        not null,
   COMM_TFR_AMT         numeric(18,2)        not null,
   COMM_WO_AMT          numeric(18,2)        not null,
   COMM_RATE            numeric(6,3)         not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_BILL_COMM_CAL_YR
   add constraint FBCCY_PK primary key nonclustered (COMM_SNAPSHOT_ID)
go

/*==============================================================*/
/* Index: FBCCY_DBPR_XFK                                        */
/*==============================================================*/




create nonclustered index FBCCY_DBPR_XFK on FACT_BILL_COMM_CAL_YR (PRDCR_DID ASC)
go

/*==============================================================*/
/* Index: FBCCY_DBPC_XFK                                        */
/*==============================================================*/




create nonclustered index FBCCY_DBPC_XFK on FACT_BILL_COMM_CAL_YR (PRDCR_CD_DID ASC)
go

/*==============================================================*/
/* Index: FBCCY_DBAC_XFK                                        */
/*==============================================================*/




create nonclustered index FBCCY_DBAC_XFK on FACT_BILL_COMM_CAL_YR (BILL_ACCT_DID ASC)
go

/*==============================================================*/
/* Index: FBCCY_DBPO_XFK                                        */
/*==============================================================*/




create nonclustered index FBCCY_DBPO_XFK on FACT_BILL_COMM_CAL_YR (BILL_POL_DID ASC)
go

/*==============================================================*/
/* Index: FBCCY_DBPP_XFK                                        */
/*==============================================================*/




create nonclustered index FBCCY_DBPP_XFK on FACT_BILL_COMM_CAL_YR (BILL_POL_PRD_DID ASC)
go

/*==============================================================*/
/* Index: FBCCY_DBCH_XFK                                        */
/*==============================================================*/




create nonclustered index FBCCY_DBCH_XFK on FACT_BILL_COMM_CAL_YR (CHRG_DID ASC)
go

/*==============================================================*/
/* Index: FBCCY_DIMO_XFK                                        */
/*==============================================================*/




create nonclustered index FBCCY_DIMO_XFK on FACT_BILL_COMM_CAL_YR (TRANS_MTH_ID ASC)
go

/*==============================================================*/
/* Index: FBCCY_DBCP_XFK                                        */
/*==============================================================*/




create nonclustered index FBCCY_DBCP_XFK on FACT_BILL_COMM_CAL_YR (COMM_PLAN_DID ASC)
go

/*==============================================================*/
/* Table: FACT_CA_PREM_TRAN                                     */
/*==============================================================*/
create table FACT_CA_PREM_TRAN (
   CA_PREM_TRANS_TID    int                  not null,
   CA_PREM_TRANS_KEY    varchar(100)         not null,
   PRIM_NMD_INS_ID      int                  not null,
   AGCY_ID              int                  not null,
   AGNT_ID              int                  not null,
   PRODUCT_ID           int                  not null,
   ORG_ID               int                  not null,
   POLICY_ID            int                  not null,
   CA_POL_LINE_DID      int                  not null,
   COVG_ID              int                  not null,
   ACCT_ID              int                  not null,
   REC_TYPE_ID          int                  not null,
   CA_VEH_DID           int                  not null,
   CA_DEALER_DID        int                  not null,
   GARAGE_SRVC_DID      int                  not null,
   NMD_INDIV_DID        int                  not null,
   CA_JURS_DID          int                  not null,
   AUTO_DEALER_LOC_ID   int                  not null,
   GARAGE_SRVC_LOC_ID   int                  not null,
   VEH_GARAGE_LOC_ID    int                  not null,
   TRANS_PROC_DT_ID     int                  not null,
   TRANS_EFF_DT_ID      int                  not null,
   TRANS_EXP_DT_ID      int                  not null,
   ACCTG_PRD_ID         int                  not null,
   REC_TYPE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ORIG_TRANS_PROC_DTS  DATETIME2            not null,
   ORIG_END_EFF_DT      DATE                 not null,
   TRANS_EFF_DT         DATE                 not null,
   TRANS_EXP_DT         DATE                 not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   CVRBL_TYPE_CD        varchar(255)         not null,
   PREM_BASIS_AMT       numeric(18,2)        not null,
   STD_BASE_RATE        numeric(18,4)        not null,
   STD_ADJ_RATE         numeric(18,4)        not null,
   STD_AMT              numeric(18,2)        not null,
   STD_TERM_AMT         numeric(18,2)        not null,
   OVERRIDE_BASE_RATE   numeric(18,4)        not null,
   OVERRIDE_ADJ_RATE    numeric(18,4)        not null,
   OVERRIDE_AMT         numeric(18,2)        not null,
   OVERRIDE_TERM_AMT    numeric(18,2)        not null,
   ACT_BASE_RATE        numeric(18,4)        not null,
   ACT_ADJ_RATE         numeric(18,4)        not null,
   ACT_AMT              numeric(18,2)        not null,
   ACT_TERM_AMT         numeric(18,2)        not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   COMM_PCT             numeric(6,3)         not null,
   EST_PREM_AMT         numeric(18,2)        not null,
   RPT_PREM_AMT         numeric(18,2)        not null,
   AUDIT_PREM_AMT       numeric(18,2)        not null,
   AUDITABLE_FL         char(1)              not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_CA_PREM_TRAN
   add constraint FCPT_PK primary key nonclustered (CA_PREM_TRANS_TID)
go

/*==============================================================*/
/* Index: FCPT_DCAC_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DCAC_XFK on FACT_CA_PREM_TRAN (ACCT_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DCDE_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DCDE_XFK on FACT_CA_PREM_TRAN (CA_DEALER_DID ASC)
go

/*==============================================================*/
/* Index: FCPT_DCGS_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DCGS_XFK on FACT_CA_PREM_TRAN (GARAGE_SRVC_DID ASC)
go

/*==============================================================*/
/* Index: FCPT_DCJU_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DCJU_XFK on FACT_CA_PREM_TRAN (CA_JURS_DID ASC)
go

/*==============================================================*/
/* Index: FCPT_DCNI_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DCNI_XFK on FACT_CA_PREM_TRAN (NMD_INDIV_DID ASC)
go

/*==============================================================*/
/* Index: FCPT_DCPL_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DCPL_XFK on FACT_CA_PREM_TRAN (CA_POL_LINE_DID ASC)
go

/*==============================================================*/
/* Index: FCPT_DCVE_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DCVE_XFK on FACT_CA_PREM_TRAN (CA_VEH_DID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIAG_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DIAG_XFK on FACT_CA_PREM_TRAN (AGCY_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIAG1_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DIAG1_XFK on FACT_CA_PREM_TRAN (AGNT_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DICO_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DICO_XFK on FACT_CA_PREM_TRAN (COVG_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIDA_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DIDA_XFK on FACT_CA_PREM_TRAN (TRANS_PROC_DT_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIDA1_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DIDA1_XFK on FACT_CA_PREM_TRAN (TRANS_EFF_DT_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIDA2_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DIDA2_XFK on FACT_CA_PREM_TRAN (TRANS_EXP_DT_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DILO1_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DILO1_XFK on FACT_CA_PREM_TRAN (AUTO_DEALER_LOC_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DILO2_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DILO2_XFK on FACT_CA_PREM_TRAN (GARAGE_SRVC_LOC_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DILO3_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DILO3_XFK on FACT_CA_PREM_TRAN (VEH_GARAGE_LOC_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIMO_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DIMO_XFK on FACT_CA_PREM_TRAN (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIOR_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DIOR_XFK on FACT_CA_PREM_TRAN (ORG_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIPO_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DIPO_XFK on FACT_CA_PREM_TRAN (POLICY_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIPR_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DIPR_XFK on FACT_CA_PREM_TRAN (PRODUCT_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DNIN_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DNIN_XFK on FACT_CA_PREM_TRAN (PRIM_NMD_INS_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DRTY_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DRTY_XFK on FACT_CA_PREM_TRAN (REC_TYPE_ID ASC)
go

/*==============================================================*/
/* Table: FACT_CLAIM_FEATURE_STATUS_TRAN                        */
/*==============================================================*/
create table FACT_CLAIM_FEATURE_STATUS_TRAN (
   CLAIM_FEATURE_STATUS_TRANS_ID int                  not null,
   ALT_FEATURE_STATUS_TRANS_ID int                  not null,
   CLAIM_FEATURE_ID     int                  not null,
   CLAIM_STATUS_ID      int                  not null,
   CLAIM_ID             int                  not null,
   CLMNT_ID             int                  not null,
   STATUS_PROC_DT_ID    int                  not null,
   OPEN_DT_ID           int                  not null,
   REOPEN_DT_ID         int                  not null,
   CLOSED_DT_ID         int                  not null,
   STATUS_PROC_DTS      DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_CLAIM_FEATURE_STATUS_TRAN
   add constraint FCFST_PK primary key nonclustered (CLAIM_FEATURE_STATUS_TRANS_ID)
go

/*==============================================================*/
/* Index: FCFST_DCFE_XFK                                        */
/*==============================================================*/




create nonclustered index FCFST_DCFE_XFK on FACT_CLAIM_FEATURE_STATUS_TRAN (CLAIM_FEATURE_ID ASC)
go

/*==============================================================*/
/* Index: FCFST_DCST_XFK                                        */
/*==============================================================*/




create nonclustered index FCFST_DCST_XFK on FACT_CLAIM_FEATURE_STATUS_TRAN (CLAIM_STATUS_ID ASC)
go

/*==============================================================*/
/* Index: FCFST_DICL_XFK                                        */
/*==============================================================*/




create nonclustered index FCFST_DICL_XFK on FACT_CLAIM_FEATURE_STATUS_TRAN (CLAIM_ID ASC)
go

/*==============================================================*/
/* Index: FCFST_DICL1_XFK                                       */
/*==============================================================*/




create nonclustered index FCFST_DICL1_XFK on FACT_CLAIM_FEATURE_STATUS_TRAN (CLMNT_ID ASC)
go

/*==============================================================*/
/* Index: FCFST_DIDA_XFK                                        */
/*==============================================================*/




create nonclustered index FCFST_DIDA_XFK on FACT_CLAIM_FEATURE_STATUS_TRAN (STATUS_PROC_DT_ID ASC)
go

/*==============================================================*/
/* Index: FCFST_DIDA1_XFK                                       */
/*==============================================================*/




create nonclustered index FCFST_DIDA1_XFK on FACT_CLAIM_FEATURE_STATUS_TRAN (OPEN_DT_ID ASC)
go

/*==============================================================*/
/* Index: FCFST_DIDA2_XFK                                       */
/*==============================================================*/




create nonclustered index FCFST_DIDA2_XFK on FACT_CLAIM_FEATURE_STATUS_TRAN (REOPEN_DT_ID ASC)
go

/*==============================================================*/
/* Index: FCFST_DIDA3_XFK                                       */
/*==============================================================*/




create nonclustered index FCFST_DIDA3_XFK on FACT_CLAIM_FEATURE_STATUS_TRAN (CLOSED_DT_ID ASC)
go

/*==============================================================*/
/* Table: FACT_CLAIM_STATUS_TRAN                                */
/*==============================================================*/
create table FACT_CLAIM_STATUS_TRAN (
   CLAIM_STATUS_TRANS_ID int                  not null,
   ALT_CLAIM_STATUS_TRANS_ID int                  not null,
   CLAIM_ID             int                  not null,
   CLAIM_STATUS_ID      int                  not null,
   STATUS_PROC_DT_ID    int                  not null,
   OPEN_DT_ID           int                  not null,
   REOPEN_DT_ID         int                  not null,
   CLOSED_DT_ID         int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   STATUS_PROC_DTS      DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_CLAIM_STATUS_TRAN
   add constraint FCST_PK primary key nonclustered (CLAIM_STATUS_TRANS_ID)
go

/*==============================================================*/
/* Index: FCST_DICL_XFK                                         */
/*==============================================================*/




create nonclustered index FCST_DICL_XFK on FACT_CLAIM_STATUS_TRAN (CLAIM_ID ASC)
go

/*==============================================================*/
/* Index: FCST_DCST_XFK                                         */
/*==============================================================*/




create nonclustered index FCST_DCST_XFK on FACT_CLAIM_STATUS_TRAN (CLAIM_STATUS_ID ASC)
go

/*==============================================================*/
/* Index: FCST_DIDA_XFK                                         */
/*==============================================================*/




create nonclustered index FCST_DIDA_XFK on FACT_CLAIM_STATUS_TRAN (STATUS_PROC_DT_ID ASC)
go

/*==============================================================*/
/* Index: FCST_DIDA1_XFK                                        */
/*==============================================================*/




create nonclustered index FCST_DIDA1_XFK on FACT_CLAIM_STATUS_TRAN (OPEN_DT_ID ASC)
go

/*==============================================================*/
/* Index: FCST_DIDA2_XFK                                        */
/*==============================================================*/




create nonclustered index FCST_DIDA2_XFK on FACT_CLAIM_STATUS_TRAN (REOPEN_DT_ID ASC)
go

/*==============================================================*/
/* Index: FCST_DIDA3_XFK                                        */
/*==============================================================*/




create nonclustered index FCST_DIDA3_XFK on FACT_CLAIM_STATUS_TRAN (CLOSED_DT_ID ASC)
go

/*==============================================================*/
/* Table: FACT_CP_PREM_TRAN                                     */
/*==============================================================*/
create table FACT_CP_PREM_TRAN (
   CP_PREM_TRANS_TID    int                  not null,
   CP_PREM_TRANS_KEY    varchar(100)         not null,
   AGCY_ID              int                  not null,
   AGNT_ID              int                  not null,
   ORG_ID               int                  not null,
   PRODUCT_ID           int                  not null,
   ACCT_ID              int                  not null,
   POLICY_ID            int                  not null,
   PRIM_NMD_INS_ID      int                  not null,
   COVG_ID              int                  not null,
   REC_TYPE_ID          int                  not null,
   CP_POL_LINE_DID      int                  not null,
   CP_LOC_DID           int                  not null,
   CP_BLDG_DID          int                  not null,
   BLDG_DID             int                  not null,
   CP_SPCL_CL_DID       int                  not null,
   CP_BUS_INC_DID       int                  not null,
   CP_OCC_CL_DID        int                  not null,
   CP_PERS_PROP_DID     int                  not null,
   CP_SPCL_CL_BUS_INC_DID int                  not null,
   CP_BLNKT_DID         int                  not null,
   CP_CL_CD_DID         int                  not null,
   CP_LOC_LOC_ID        int                  not null,
   TRANS_PROC_DT_ID     int                  not null,
   TRANS_EFF_DT_ID      int                  not null,
   TRANS_EXP_DT_ID      int                  not null,
   ACCTG_PRD_ID         int                  not null,
   REC_TYPE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ORIG_TRANS_PROC_DTS  DATETIME2            not null,
   ORIG_END_EFF_DT      DATE                 not null,
   TRANS_EFF_DT         DATE                 not null,
   TRANS_EXP_DT         DATE                 not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   PREM_BASIS_AMT       numeric(18,2)        not null,
   STD_BASE_RATE        numeric(18,4)        not null,
   STD_ADJ_RATE         numeric(18,4)        not null,
   STD_AMT              numeric(18,2)        not null,
   STD_TERM_AMT         numeric(18,2)        not null,
   OVERRIDE_BASE_RATE   numeric(18,4)        not null,
   OVERRIDE_ADJ_RATE    numeric(18,4)        not null,
   OVERRIDE_AMT         numeric(18,2)        not null,
   OVERRIDE_TERM_AMT    numeric(18,2)        not null,
   ACT_BASE_RATE        numeric(18,4)        not null,
   ACT_ADJ_RATE         numeric(18,4)        not null,
   ACT_AMT              numeric(18,2)        not null,
   ACT_TERM_AMT         numeric(18,2)        not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   COMM_PCT             numeric(6,3)         not null,
   EST_PREM_AMT         numeric(18,2)        not null,
   RPT_PREM_AMT         numeric(18,2)        not null,
   AUDIT_PREM_AMT       numeric(18,2)        not null,
   AUDITABLE_FL         char(1)              not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_CP_PREM_TRAN
   add constraint FCPT1_PK primary key nonclustered (CP_PREM_TRANS_TID)
go

/*==============================================================*/
/* Index: FCPT_DCAC1_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DCAC1_XFK on FACT_CP_PREM_TRAN (ACCT_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DCBI_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DCBI_XFK on FACT_CP_PREM_TRAN (CP_BUS_INC_DID ASC)
go

/*==============================================================*/
/* Index: FCPT_DCBL_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DCBL_XFK on FACT_CP_PREM_TRAN (CP_BLDG_DID ASC)
go

/*==============================================================*/
/* Index: FCPT_DCBL1_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DCBL1_XFK on FACT_CP_PREM_TRAN (CP_BLNKT_DID ASC)
go

/*==============================================================*/
/* Index: FCPT_DCCC_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DCCC_XFK on FACT_CP_PREM_TRAN (CP_CL_CD_DID ASC)
go

/*==============================================================*/
/* Index: FCPT_DCLO_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DCLO_XFK on FACT_CP_PREM_TRAN (CP_LOC_DID ASC)
go

/*==============================================================*/
/* Index: FCPT_DCOC_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DCOC_XFK on FACT_CP_PREM_TRAN (CP_OCC_CL_DID ASC)
go

/*==============================================================*/
/* Index: FCPT_DCPL1_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DCPL1_XFK on FACT_CP_PREM_TRAN (CP_POL_LINE_DID ASC)
go

/*==============================================================*/
/* Index: FCPT_DCPP_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DCPP_XFK on FACT_CP_PREM_TRAN (CP_PERS_PROP_DID ASC)
go

/*==============================================================*/
/* Index: FCPT_DCSC_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DCSC_XFK on FACT_CP_PREM_TRAN (CP_SPCL_CL_DID ASC)
go

/*==============================================================*/
/* Index: FCPT_DCSCB_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DCSCB_XFK on FACT_CP_PREM_TRAN (CP_SPCL_CL_BUS_INC_DID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIAG2_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DIAG2_XFK on FACT_CP_PREM_TRAN (AGCY_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIAG3_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DIAG3_XFK on FACT_CP_PREM_TRAN (AGNT_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DICO1_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DICO1_XFK on FACT_CP_PREM_TRAN (COVG_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIDA3_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DIDA3_XFK on FACT_CP_PREM_TRAN (TRANS_PROC_DT_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIDA4_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DIDA4_XFK on FACT_CP_PREM_TRAN (TRANS_EFF_DT_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIDA5_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DIDA5_XFK on FACT_CP_PREM_TRAN (TRANS_EXP_DT_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DILO_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DILO_XFK on FACT_CP_PREM_TRAN (CP_LOC_LOC_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIMO1_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DIMO1_XFK on FACT_CP_PREM_TRAN (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIOR1_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DIOR1_XFK on FACT_CP_PREM_TRAN (ORG_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIPO1_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DIPO1_XFK on FACT_CP_PREM_TRAN (POLICY_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIPR1_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DIPR1_XFK on FACT_CP_PREM_TRAN (PRODUCT_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DNIN1_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DNIN1_XFK on FACT_CP_PREM_TRAN (PRIM_NMD_INS_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DRTY1_XFK                                        */
/*==============================================================*/




create nonclustered index FCPT_DRTY1_XFK on FACT_CP_PREM_TRAN (REC_TYPE_ID ASC)
go

/*==============================================================*/
/* Index: FCPT_DIBL_XFK                                         */
/*==============================================================*/




create nonclustered index FCPT_DIBL_XFK on FACT_CP_PREM_TRAN (BLDG_DID ASC)
go

/*==============================================================*/
/* Table: FACT_GENERAL_TRAN                                     */
/*==============================================================*/
create table FACT_GENERAL_TRAN (
   GENERAL_JRNL_TID     int                  not null,
   GENERAL_JRNL_KEY     varchar(100)         not null,
   CR_ACCT_DID          int                  not null,
   CR_DID               int                  not null,
   SUSP_ACCT_DID        int                  not null,
   SUSP_PRDCR_DID       int                  not null,
   NON_RCVBL_DISTR_ITEM_DID int                  not null,
   SRCE_PRDCR_DID       int                  not null,
   TARGET_PRDCR_DID     int                  not null,
   SRCE_ACCT_DID        int                  not null,
   TARGET_ACCT_DID      int                  not null,
   FUNDS_TFR_DID        int                  not null,
   TRANS_DT_ID          int                  not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   COLTRL_KEY           varchar(100)         not null,
   SRCE_UNAPPLIED_TACCT_KEY varchar(100)         not null,
   TARGET_UNAPPLIED_TACCT_KEY varchar(100)         not null,
   TRANS_DTS            DATETIME2            not null,
   TRANS_NO             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TFR_REASON_CD        varchar(255)         not null,
   TFR_REVERSAL_REASON_CD varchar(255)         not null,
   WO_CHANNEL_CD        varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   CASH_BAL             numeric(18,2)        not null,
   COLTRL_HELD_BAL      numeric(18,2)        not null,
   UNAPPLIED_BAL        numeric(18,2)        not null,
   COLTRL_RSRV_BAL      numeric(18,2)        not null,
   SUSP_BAL             numeric(18,2)        not null,
   COLLECTIONS_CR_BAL   numeric(18,2)        not null,
   GOODWILL_CR_BAL      numeric(18,2)        not null,
   INTRST_CR_BAL        numeric(18,2)        not null,
   OTHR_CR_BAL          numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            not null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_GENERAL_TRAN
   add constraint FGTR_PK primary key nonclustered (GENERAL_JRNL_TID)
go

alter table FACT_GENERAL_TRAN
   add constraint FGTR_AK1 unique (GENERAL_JRNL_KEY)
go

/*==============================================================*/
/* Index: FGTR_DBAC_XFK                                         */
/*==============================================================*/




create nonclustered index FGTR_DBAC_XFK on FACT_GENERAL_TRAN (CR_ACCT_DID ASC)
go

/*==============================================================*/
/* Index: FGTR_DBCR_XFK                                         */
/*==============================================================*/




create nonclustered index FGTR_DBCR_XFK on FACT_GENERAL_TRAN (CR_DID ASC)
go

/*==============================================================*/
/* Index: FGTR_DBNDI_XFK                                        */
/*==============================================================*/




create nonclustered index FGTR_DBNDI_XFK on FACT_GENERAL_TRAN (NON_RCVBL_DISTR_ITEM_DID ASC)
go

/*==============================================================*/
/* Index: FGTR_DBPR_XFK                                         */
/*==============================================================*/




create nonclustered index FGTR_DBPR_XFK on FACT_GENERAL_TRAN (SUSP_PRDCR_DID ASC)
go

/*==============================================================*/
/* Index: FGTR_DBFT_XFK                                         */
/*==============================================================*/




create nonclustered index FGTR_DBFT_XFK on FACT_GENERAL_TRAN (FUNDS_TFR_DID ASC)
go

/*==============================================================*/
/* Index: FGTR_DBAC1_XFK                                        */
/*==============================================================*/




create nonclustered index FGTR_DBAC1_XFK on FACT_GENERAL_TRAN (SRCE_ACCT_DID ASC)
go

/*==============================================================*/
/* Index: FGTR_DBPR1_XFK                                        */
/*==============================================================*/




create nonclustered index FGTR_DBPR1_XFK on FACT_GENERAL_TRAN (SRCE_PRDCR_DID ASC)
go

/*==============================================================*/
/* Index: FGTR_DBAC2_XFK                                        */
/*==============================================================*/




create nonclustered index FGTR_DBAC2_XFK on FACT_GENERAL_TRAN (TARGET_ACCT_DID ASC)
go

/*==============================================================*/
/* Index: FGTR_DBPR2_XFK                                        */
/*==============================================================*/




create nonclustered index FGTR_DBPR2_XFK on FACT_GENERAL_TRAN (TARGET_PRDCR_DID ASC)
go

/*==============================================================*/
/* Index: FGTR_DIDA_XFK                                         */
/*==============================================================*/




create nonclustered index FGTR_DIDA_XFK on FACT_GENERAL_TRAN (TRANS_DT_ID ASC)
go

/*==============================================================*/
/* Index: FGTR_DBAC3_XFK                                        */
/*==============================================================*/




create nonclustered index FGTR_DBAC3_XFK on FACT_GENERAL_TRAN (SUSP_ACCT_DID ASC)
go

/*==============================================================*/
/* Table: FACT_GL_PREM_TRAN                                     */
/*==============================================================*/
create table FACT_GL_PREM_TRAN (
   GL_PREM_TRANS_TID    int                  not null,
   GL_PREM_TRANS_KEY    varchar(100)         not null,
   AGCY_ID              int                  not null,
   AGNT_ID              int                  not null,
   ORG_ID               int                  not null,
   PRODUCT_ID           int                  not null,
   ACCT_ID              int                  not null,
   POLICY_ID            int                  not null,
   PRIM_NMD_INS_ID      int                  not null,
   ADDL_NMD_INS_ID      int                  not null,
   COVG_ID              int                  not null,
   REC_TYPE_ID          int                  not null,
   GL_POL_LINE_DID      int                  not null,
   GL_COVG_PART_DID     int                  not null,
   GL_EXPSR_DID         int                  not null,
   GL_CL_CD_DID         int                  not null,
   EXPSR_LOC_ID         int                  not null,
   TRANS_PROC_DT_ID     int                  not null,
   TRANS_EFF_DT_ID      int                  not null,
   TRANS_EXP_DT_ID      int                  not null,
   ACCTG_PRD_ID         int                  not null,
   REC_TYPE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ORIG_TRANS_PROC_DTS  DATETIME2            not null,
   ORIG_END_EFF_DT      DATE                 not null,
   TRANS_EFF_DT         DATE                 not null,
   TRANS_EXP_DT         DATE                 not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   JURS_CD              varchar(255)         not null,
   STATE_COST_TYPE_CD   varchar(255)         not null,
   PREM_BASIS_AMT       numeric(18,2)        not null,
   STD_BASE_RATE        numeric(18,4)        not null,
   STD_ADJ_RATE         numeric(18,4)        not null,
   STD_AMT              numeric(18,2)        not null,
   STD_TERM_AMT         numeric(18,2)        not null,
   OVERRIDE_BASE_RATE   numeric(18,4)        not null,
   OVERRIDE_ADJ_RATE    numeric(18,4)        not null,
   OVERRIDE_AMT         numeric(18,2)        not null,
   OVERRIDE_TERM_AMT    numeric(18,2)        not null,
   ACT_BASE_RATE        numeric(18,4)        not null,
   ACT_ADJ_RATE         numeric(18,4)        not null,
   ACT_AMT              numeric(18,2)        not null,
   ACT_TERM_AMT         numeric(18,2)        not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   COMM_PCT             numeric(6,3)         not null,
   EST_PREM_AMT         numeric(18,2)        not null,
   RPT_PREM_AMT         numeric(18,2)        not null,
   AUDIT_PREM_AMT       numeric(18,2)        not null,
   AUDITABLE_FL         char(1)              not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_GL_PREM_TRAN
   add constraint FGPT_PK primary key nonclustered (GL_PREM_TRANS_TID)
go

/*==============================================================*/
/* Index: FGPT_DCAC_XFK                                         */
/*==============================================================*/




create nonclustered index FGPT_DCAC_XFK on FACT_GL_PREM_TRAN (ACCT_ID ASC)
go

/*==============================================================*/
/* Index: FGPT_DGCC_XFK                                         */
/*==============================================================*/




create nonclustered index FGPT_DGCC_XFK on FACT_GL_PREM_TRAN (GL_CL_CD_DID ASC)
go

/*==============================================================*/
/* Index: FGPT_DGCP_XFK                                         */
/*==============================================================*/




create nonclustered index FGPT_DGCP_XFK on FACT_GL_PREM_TRAN (GL_COVG_PART_DID ASC)
go

/*==============================================================*/
/* Index: FGPT_DGEX_XFK                                         */
/*==============================================================*/




create nonclustered index FGPT_DGEX_XFK on FACT_GL_PREM_TRAN (GL_EXPSR_DID ASC)
go

/*==============================================================*/
/* Index: FGPT_DGPL_XFK                                         */
/*==============================================================*/




create nonclustered index FGPT_DGPL_XFK on FACT_GL_PREM_TRAN (GL_POL_LINE_DID ASC)
go

/*==============================================================*/
/* Index: FGPT_DIAG_XFK                                         */
/*==============================================================*/




create nonclustered index FGPT_DIAG_XFK on FACT_GL_PREM_TRAN (AGCY_ID ASC)
go

/*==============================================================*/
/* Index: FGPT_DIAG1_XFK                                        */
/*==============================================================*/




create nonclustered index FGPT_DIAG1_XFK on FACT_GL_PREM_TRAN (AGNT_ID ASC)
go

/*==============================================================*/
/* Index: FGPT_DICO_XFK                                         */
/*==============================================================*/




create nonclustered index FGPT_DICO_XFK on FACT_GL_PREM_TRAN (COVG_ID ASC)
go

/*==============================================================*/
/* Index: FGPT_DIDA_XFK                                         */
/*==============================================================*/




create nonclustered index FGPT_DIDA_XFK on FACT_GL_PREM_TRAN (TRANS_PROC_DT_ID ASC)
go

/*==============================================================*/
/* Index: FGPT_DIDA1_XFK                                        */
/*==============================================================*/




create nonclustered index FGPT_DIDA1_XFK on FACT_GL_PREM_TRAN (TRANS_EFF_DT_ID ASC)
go

/*==============================================================*/
/* Index: FGPT_DIDA2_XFK                                        */
/*==============================================================*/




create nonclustered index FGPT_DIDA2_XFK on FACT_GL_PREM_TRAN (TRANS_EXP_DT_ID ASC)
go

/*==============================================================*/
/* Index: FGPT_DILO_XFK                                         */
/*==============================================================*/




create nonclustered index FGPT_DILO_XFK on FACT_GL_PREM_TRAN (EXPSR_LOC_ID ASC)
go

/*==============================================================*/
/* Index: FGPT_DIMO_XFK                                         */
/*==============================================================*/




create nonclustered index FGPT_DIMO_XFK on FACT_GL_PREM_TRAN (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: FGPT_DIOR_XFK                                         */
/*==============================================================*/




create nonclustered index FGPT_DIOR_XFK on FACT_GL_PREM_TRAN (ORG_ID ASC)
go

/*==============================================================*/
/* Index: FGPT_DIPO_XFK                                         */
/*==============================================================*/




create nonclustered index FGPT_DIPO_XFK on FACT_GL_PREM_TRAN (POLICY_ID ASC)
go

/*==============================================================*/
/* Index: FGPT_DIPR_XFK                                         */
/*==============================================================*/




create nonclustered index FGPT_DIPR_XFK on FACT_GL_PREM_TRAN (PRODUCT_ID ASC)
go

/*==============================================================*/
/* Index: FGPT_DNIN_XFK                                         */
/*==============================================================*/




create nonclustered index FGPT_DNIN_XFK on FACT_GL_PREM_TRAN (PRIM_NMD_INS_ID ASC)
go

/*==============================================================*/
/* Index: FGPT_DNIN1_XFK                                        */
/*==============================================================*/




create nonclustered index FGPT_DNIN1_XFK on FACT_GL_PREM_TRAN (ADDL_NMD_INS_ID ASC)
go

/*==============================================================*/
/* Index: FGPT_DRTY_XFK                                         */
/*==============================================================*/




create nonclustered index FGPT_DRTY_XFK on FACT_GL_PREM_TRAN (REC_TYPE_ID ASC)
go

/*==============================================================*/
/* Table: FACT_HO_PREM_TRAN                                     */
/*==============================================================*/
create table FACT_HO_PREM_TRAN (
   HO_PREM_TRANS_TID    int                  not null,
   HO_PREM_TRANS_KEY    varchar(100)         not null,
   PRIM_NAMED_INSURED_ID int                  not null,
   AGCY_ID              int                  not null,
   AGNT_ID              int                  not null,
   PRODUCT_ID           int                  not null,
   ORG_ID               int                  not null,
   POLICY_ID            int                  not null,
   COVG_ID              int                  not null,
   ACCT_ID              int                  not null,
   REC_TYPE_ID          int                  not null,
   HO_SI_DID            int                  not null,
   HO_DWLNG_DID         int                  not null,
   HO_DWLNG_LOC_ID      int                  not null,
   HO_LOC_DID           int                  not null,
   HO_SI_LOC_ID         int                  not null,
   HO_OTHR_LOC_LOC_ID   int                  not null,
   TRANS_PROC_DT_ID     int                  not null,
   TRANS_EFF_DT_ID      int                  not null,
   TRANS_EXP_DT_ID      int                  not null,
   ACCTG_PRD_ID         int                  not null,
   POL_LINE_KEY         varchar(100)         not null,
   REC_TYPE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ORIG_TRANS_PROC_DTS  DATETIME2            not null,
   ORIG_END_EFF_DT      DATE                 not null,
   TRANS_EFF_DT         DATE                 not null,
   TRANS_EXP_DT         DATE                 not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   SCHED_TYPE_CD        varchar(255)         not null,
   SCHED_TYPE_CCD       varchar(255)         not null,
   SCHED_TYPE_CST       varchar(255)         not null,
   SCHED_TYPE_CLT       varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   COMM_PCT             numeric(6,3)         not null,
   PREM_BASIS_AMT       numeric(18,2)        not null,
   AUDITABLE_FL         char(1)              not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_HO_PREM_TRAN
   add constraint FHPT_PK primary key nonclustered (HO_PREM_TRANS_TID)
go

/*==============================================================*/
/* Index: FHPT_DNIN_XFK                                         */
/*==============================================================*/




create nonclustered index FHPT_DNIN_XFK on FACT_HO_PREM_TRAN (PRIM_NAMED_INSURED_ID ASC)
go

/*==============================================================*/
/* Index: FHPT_DIAG1_XFK                                        */
/*==============================================================*/




create nonclustered index FHPT_DIAG1_XFK on FACT_HO_PREM_TRAN (AGCY_ID ASC)
go

/*==============================================================*/
/* Index: FHPT_DIAG_XFK                                         */
/*==============================================================*/




create nonclustered index FHPT_DIAG_XFK on FACT_HO_PREM_TRAN (AGNT_ID ASC)
go

/*==============================================================*/
/* Index: FHPT_DIPR_XFK                                         */
/*==============================================================*/




create nonclustered index FHPT_DIPR_XFK on FACT_HO_PREM_TRAN (PRODUCT_ID ASC)
go

/*==============================================================*/
/* Index: FHPT_DIOR_XFK                                         */
/*==============================================================*/




create nonclustered index FHPT_DIOR_XFK on FACT_HO_PREM_TRAN (ORG_ID ASC)
go

/*==============================================================*/
/* Index: FHPT_DIPO_XFK                                         */
/*==============================================================*/




create nonclustered index FHPT_DIPO_XFK on FACT_HO_PREM_TRAN (POLICY_ID ASC)
go

/*==============================================================*/
/* Index: FHPT_DICO_XFK                                         */
/*==============================================================*/




create nonclustered index FHPT_DICO_XFK on FACT_HO_PREM_TRAN (COVG_ID ASC)
go

/*==============================================================*/
/* Index: FHPT_DCAC_XFK                                         */
/*==============================================================*/




create nonclustered index FHPT_DCAC_XFK on FACT_HO_PREM_TRAN (ACCT_ID ASC)
go

/*==============================================================*/
/* Index: FHPT_DRTY_XFK                                         */
/*==============================================================*/




create nonclustered index FHPT_DRTY_XFK on FACT_HO_PREM_TRAN (REC_TYPE_ID ASC)
go

/*==============================================================*/
/* Index: FHPT_DHSI_XFK                                         */
/*==============================================================*/




create nonclustered index FHPT_DHSI_XFK on FACT_HO_PREM_TRAN (HO_SI_DID ASC)
go

/*==============================================================*/
/* Index: FHPT_DHDW_XFK                                         */
/*==============================================================*/




create nonclustered index FHPT_DHDW_XFK on FACT_HO_PREM_TRAN (HO_DWLNG_DID ASC)
go

/*==============================================================*/
/* Index: FHPT_DILO_XFK                                         */
/*==============================================================*/




create nonclustered index FHPT_DILO_XFK on FACT_HO_PREM_TRAN (HO_SI_LOC_ID ASC)
go

/*==============================================================*/
/* Index: FHPT_DILO1_XFK                                        */
/*==============================================================*/




create nonclustered index FHPT_DILO1_XFK on FACT_HO_PREM_TRAN (HO_OTHR_LOC_LOC_ID ASC)
go

/*==============================================================*/
/* Index: FHPT_DIMO_XFK                                         */
/*==============================================================*/




create nonclustered index FHPT_DIMO_XFK on FACT_HO_PREM_TRAN (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: FHPT_DIDA_XFK                                         */
/*==============================================================*/




create nonclustered index FHPT_DIDA_XFK on FACT_HO_PREM_TRAN (TRANS_PROC_DT_ID ASC)
go

/*==============================================================*/
/* Index: FHPT_DIDA1_XFK                                        */
/*==============================================================*/




create nonclustered index FHPT_DIDA1_XFK on FACT_HO_PREM_TRAN (TRANS_EFF_DT_ID ASC)
go

/*==============================================================*/
/* Index: FHPT_DIDA2_XFK                                        */
/*==============================================================*/




create nonclustered index FHPT_DIDA2_XFK on FACT_HO_PREM_TRAN (TRANS_EXP_DT_ID ASC)
go

/*==============================================================*/
/* Index: FHPT_DILO2_XFK                                        */
/*==============================================================*/




create nonclustered index FHPT_DILO2_XFK on FACT_HO_PREM_TRAN (HO_DWLNG_LOC_ID ASC)
go

/*==============================================================*/
/* Index: FHPT_DHLO_XFK                                         */
/*==============================================================*/




create nonclustered index FHPT_DHLO_XFK on FACT_HO_PREM_TRAN (HO_LOC_DID ASC)
go

/*==============================================================*/
/* Table: FACT_LOSS_CAL_YR                                      */
/*==============================================================*/
create table FACT_LOSS_CAL_YR (
   LOSS_CAL_YR_ID       int                  not null,
   ORG_ID               int                  not null,
   PRODUCT_ID           int                  not null,
   COVG_ID              int                  not null,
   AGNT_ID              int                  not null,
   AGCY_ID              int                  not null,
   REC_TYPE_ID          int                  not null,
   ACCTG_PRD_ID         int                  not null,
   CURR_CD              varchar(255)         not null,
   OS_IND_RSRV_AMT      numeric(18,2)        not null,
   OS_MED_RSRV_AMT      numeric(18,2)        not null,
   OS_ALAE_RSRV_AMT     numeric(18,2)        not null,
   OS_ULAE_RSRV_AMT     numeric(18,2)        not null,
   OS_SLVG_RSRV_AMT     numeric(18,2)        not null,
   OS_SUBRO_RSRV_AMT    numeric(18,2)        not null,
   MTD_IND_PD_AMT       numeric(18,2)        not null,
   MTD_MED_PD_AMT       numeric(18,2)        not null,
   MTD_ALAE_PD_AMT      numeric(18,2)        not null,
   MTD_ULAE_PD_AMT      numeric(18,2)        not null,
   MTD_DED_RCVRY_AMT    numeric(18,2)        not null,
   MTD_SLVG_RCVRY_AMT   numeric(18,2)        not null,
   MTD_SUBRO_RCVRY_AMT  numeric(18,2)        not null,
   MTD_IND_RCVRY_AMT    numeric(18,2)        not null,
   MTD_EXP_RCVRY_AMT    numeric(18,2)        not null,
   MTD_IND_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_MED_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_ALAE_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_ULAE_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_SLVG_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_SUBRO_RSRV_CHNG_AMT numeric(18,2)        not null,
   QTD_IND_PD_AMT       numeric(18,2)        not null,
   QTD_MED_PD_AMT       numeric(18,2)        not null,
   QTD_ALAE_PD_AMT      numeric(18,2)        not null,
   QTD_ULAE_PD_AMT      numeric(18,2)        not null,
   QTD_DED_RCVRY_AMT    numeric(18,2)        not null,
   QTD_SLVG_RCVRY_AMT   numeric(18,2)        not null,
   QTD_SUBRO_RCVRY_AMT  numeric(18,2)        not null,
   QTD_IND_RCVRY_AMT    numeric(18,2)        not null,
   QTD_EXP_RCVRY_AMT    numeric(18,2)        not null,
   QTD_IND_RSRV_CHNG_AMT numeric(18,2)        not null,
   QTD_MED_RSRV_CHNG_AMT numeric(18,2)        not null,
   QTD_ALAE_RSRV_CHNG_AMT numeric(18,2)        not null,
   QTD_ULAE_RSRV_CHNG_AMT numeric(18,2)        not null,
   QTD_SLVG_RSRV_CHNG_AMT numeric(18,2)        not null,
   QTD_SUBRO_RSRV_CHNG_AMT numeric(18,2)        not null,
   YTD_IND_PD_AMT       numeric(18,2)        not null,
   YTD_MED_PD_AMT       numeric(18,2)        not null,
   YTD_ALAE_PD_AMT      numeric(18,2)        not null,
   YTD_ULAE_PD_AMT      numeric(18,2)        not null,
   YTD_DED_RCVRY_AMT    numeric(18,2)        not null,
   YTD_SLVG_RCVRY_AMT   numeric(18,2)        not null,
   YTD_SUBRO_RCVRY_AMT  numeric(18,2)        not null,
   YTD_IND_RCVRY_AMT    numeric(18,2)        not null,
   YTD_EXP_RCVRY_AMT    numeric(18,2)        not null,
   YTD_IND_RSRV_CHNG_AMT numeric(18,2)        not null,
   YTD_MED_RSRV_CHNG_AMT numeric(18,2)        not null,
   YTD_ALAE_RSRV_CHNG_AMT numeric(18,2)        not null,
   YTD_ULAE_RSRV_CHNG_AMT numeric(18,2)        not null,
   YTD_SLVG_RSRV_CHNG_AMT numeric(18,2)        not null,
   YTD_SUBRO_RSRV_CHNG_AMT numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_LOSS_CAL_YR
   add constraint FLCY_PK primary key nonclustered (LOSS_CAL_YR_ID)
go

alter table FACT_LOSS_CAL_YR
   add constraint FLCY_AK1 unique (ORG_ID, PRODUCT_ID, COVG_ID, AGNT_ID, AGCY_ID, REC_TYPE_ID, ACCTG_PRD_ID, CURR_CD)
go

/*==============================================================*/
/* Index: FLCY_DRTY_XFK                                         */
/*==============================================================*/




create nonclustered index FLCY_DRTY_XFK on FACT_LOSS_CAL_YR (REC_TYPE_ID ASC)
go

/*==============================================================*/
/* Index: FLCY_DIMO_XFK                                         */
/*==============================================================*/




create nonclustered index FLCY_DIMO_XFK on FACT_LOSS_CAL_YR (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: FLCY_DIPR_XFK                                         */
/*==============================================================*/




create nonclustered index FLCY_DIPR_XFK on FACT_LOSS_CAL_YR (PRODUCT_ID ASC)
go

/*==============================================================*/
/* Index: FLCY_DICO_XFK                                         */
/*==============================================================*/




create nonclustered index FLCY_DICO_XFK on FACT_LOSS_CAL_YR (COVG_ID ASC)
go

/*==============================================================*/
/* Index: FLCY_DIAG_XFK                                         */
/*==============================================================*/




create nonclustered index FLCY_DIAG_XFK on FACT_LOSS_CAL_YR (AGNT_ID ASC)
go

/*==============================================================*/
/* Index: FLCY_DIAG1_XFK                                        */
/*==============================================================*/




create nonclustered index FLCY_DIAG1_XFK on FACT_LOSS_CAL_YR (AGCY_ID ASC)
go

/*==============================================================*/
/* Table: FACT_LOSS_CAL_YR_BASE                                 */
/*==============================================================*/
create table FACT_LOSS_CAL_YR_BASE (
   LOSS_CAL_YR_BASE_ID  int                  not null,
   ORG_ID               int                  not null,
   PRODUCT_ID           int                  not null,
   POLICY_ID            int                  not null,
   LOC_ID               int                  not null,
   COVG_ID              int                  not null,
   CLAIM_ID             int                  not null,
   CLMNT_ID             int                  not null,
   CLAIM_FEATURE_ID     int                  not null,
   AGNT_ID              int                  not null,
   AGCY_ID              int                  not null,
   VENDOR_ID            int                  not null,
   REC_TYPE_ID          int                  not null,
   PRIM_NAMED_INSURED_ID int                  not null,
   ACCT_ID              int                  not null,
   ACCTG_PRD_ID         int                  not null,
   MTH_END_CLAIM_STATUS_ID int                  not null,
   MTH_END_FEATURE_STATUS_ID int                  not null,
   CURR_CD              varchar(255)         not null,
   OS_IND_RSRV_AMT      numeric(18,2)        not null,
   OS_MED_RSRV_AMT      numeric(18,2)        not null,
   OS_ALAE_RSRV_AMT     numeric(18,2)        not null,
   OS_ULAE_RSRV_AMT     numeric(18,2)        not null,
   OS_SLVG_RSRV_AMT     numeric(18,2)        not null,
   OS_SUBRO_RSRV_AMT    numeric(18,2)        not null,
   MTD_IND_PD_AMT       numeric(18,2)        not null,
   MTD_MED_PD_AMT       numeric(18,2)        not null,
   MTD_ALAE_PD_AMT      numeric(18,2)        not null,
   MTD_ULAE_PD_AMT      numeric(18,2)        not null,
   MTD_DED_RCVRY_AMT    numeric(18,2)        not null,
   MTD_SLVG_RCVRY_AMT   numeric(18,2)        not null,
   MTD_SUBRO_RCVRY_AMT  numeric(18,2)        not null,
   MTD_IND_RCVRY_AMT    numeric(18,2)        not null,
   MTD_EXP_RCVRY_AMT    numeric(18,2)        not null,
   MTD_IND_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_MED_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_ALAE_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_ULAE_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_SLVG_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_SUBRO_RSRV_CHNG_AMT numeric(18,2)        not null,
   QTD_IND_PD_AMT       numeric(18,2)        not null,
   QTD_MED_PD_AMT       numeric(18,2)        not null,
   QTD_ALAE_PD_AMT      numeric(18,2)        not null,
   QTD_ULAE_PD_AMT      numeric(18,2)        not null,
   QTD_DED_RCVRY_AMT    numeric(18,2)        not null,
   QTD_SLVG_RCVRY_AMT   numeric(18,2)        not null,
   QTD_SUBRO_RCVRY_AMT  numeric(18,2)        not null,
   QTD_IND_RCVRY_AMT    numeric(18,2)        not null,
   QTD_EXP_RCVRY_AMT    numeric(18,2)        not null,
   QTD_IND_RSRV_CHNG_AMT numeric(18,2)        not null,
   QTD_MED_RSRV_CHNG_AMT numeric(18,2)        not null,
   QTD_ALAE_RSRV_CHNG_AMT numeric(18,2)        not null,
   QTD_ULAE_RSRV_CHNG_AMT numeric(18,2)        not null,
   QTD_SLVG_RSRV_CHNG_AMT numeric(18,2)        not null,
   QTD_SUBRO_RSRV_CHNG_AMT numeric(18,2)        not null,
   YTD_IND_PD_AMT       numeric(18,2)        not null,
   YTD_MED_PD_AMT       numeric(18,2)        not null,
   YTD_ALAE_PD_AMT      numeric(18,2)        not null,
   YTD_ULAE_PD_AMT      numeric(18,2)        not null,
   YTD_DED_RCVRY_AMT    numeric(18,2)        not null,
   YTD_SLVG_RCVRY_AMT   numeric(18,2)        not null,
   YTD_SUBRO_RCVRY_AMT  numeric(18,2)        not null,
   YTD_IND_RCVRY_AMT    numeric(18,2)        not null,
   YTD_EXP_RCVRY_AMT    numeric(18,2)        not null,
   YTD_IND_RSRV_CHNG_AMT numeric(18,2)        not null,
   YTD_MED_RSRV_CHNG_AMT numeric(18,2)        not null,
   YTD_ALAE_RSRV_CHNG_AMT numeric(18,2)        not null,
   YTD_ULAE_RSRV_CHNG_AMT numeric(18,2)        not null,
   YTD_SLVG_RSRV_CHNG_AMT numeric(18,2)        not null,
   YTD_SUBRO_RSRV_CHNG_AMT numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_LOSS_CAL_YR_BASE
   add constraint FLCYB_PK primary key nonclustered (LOSS_CAL_YR_BASE_ID)
go

/*==============================================================*/
/* Index: FLCYB_DCAC_XFK                                        */
/*==============================================================*/




create nonclustered index FLCYB_DCAC_XFK on FACT_LOSS_CAL_YR_BASE (ACCT_ID ASC)
go

/*==============================================================*/
/* Index: FLCYB_DCFE_XFK                                        */
/*==============================================================*/




create nonclustered index FLCYB_DCFE_XFK on FACT_LOSS_CAL_YR_BASE (CLAIM_FEATURE_ID ASC)
go

/*==============================================================*/
/* Index: FLCYB_DCST_XFK                                        */
/*==============================================================*/




create nonclustered index FLCYB_DCST_XFK on FACT_LOSS_CAL_YR_BASE (MTH_END_CLAIM_STATUS_ID ASC)
go

/*==============================================================*/
/* Index: FLCYB_DCST1_XFK                                       */
/*==============================================================*/




create nonclustered index FLCYB_DCST1_XFK on FACT_LOSS_CAL_YR_BASE (MTH_END_FEATURE_STATUS_ID ASC)
go

/*==============================================================*/
/* Index: FLCYB_DIAG_XFK                                        */
/*==============================================================*/




create nonclustered index FLCYB_DIAG_XFK on FACT_LOSS_CAL_YR_BASE (AGNT_ID ASC)
go

/*==============================================================*/
/* Index: FLCYB_DIAG1_XFK                                       */
/*==============================================================*/




create nonclustered index FLCYB_DIAG1_XFK on FACT_LOSS_CAL_YR_BASE (AGCY_ID ASC)
go

/*==============================================================*/
/* Index: FLCYB_DICL_XFK                                        */
/*==============================================================*/




create nonclustered index FLCYB_DICL_XFK on FACT_LOSS_CAL_YR_BASE (CLAIM_ID ASC)
go

/*==============================================================*/
/* Index: FLCYB_DICL1_XFK                                       */
/*==============================================================*/




create nonclustered index FLCYB_DICL1_XFK on FACT_LOSS_CAL_YR_BASE (CLMNT_ID ASC)
go

/*==============================================================*/
/* Index: FLCYB_DICO_XFK                                        */
/*==============================================================*/




create nonclustered index FLCYB_DICO_XFK on FACT_LOSS_CAL_YR_BASE (COVG_ID ASC)
go

/*==============================================================*/
/* Index: FLCYB_DILO_XFK                                        */
/*==============================================================*/




create nonclustered index FLCYB_DILO_XFK on FACT_LOSS_CAL_YR_BASE (LOC_ID ASC)
go

/*==============================================================*/
/* Index: FLCYB_DIMO_XFK                                        */
/*==============================================================*/




create nonclustered index FLCYB_DIMO_XFK on FACT_LOSS_CAL_YR_BASE (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: FLCYB_DIOR_XFK                                        */
/*==============================================================*/




create nonclustered index FLCYB_DIOR_XFK on FACT_LOSS_CAL_YR_BASE (ORG_ID ASC)
go

/*==============================================================*/
/* Index: FLCYB_DIPO_XFK                                        */
/*==============================================================*/




create nonclustered index FLCYB_DIPO_XFK on FACT_LOSS_CAL_YR_BASE (POLICY_ID ASC)
go

/*==============================================================*/
/* Index: FLCYB_DIPR_XFK                                        */
/*==============================================================*/




create nonclustered index FLCYB_DIPR_XFK on FACT_LOSS_CAL_YR_BASE (PRODUCT_ID ASC)
go

/*==============================================================*/
/* Index: FLCYB_DIVE_XFK                                        */
/*==============================================================*/




create nonclustered index FLCYB_DIVE_XFK on FACT_LOSS_CAL_YR_BASE (VENDOR_ID ASC)
go

/*==============================================================*/
/* Index: FLCYB_DNIN_XFK                                        */
/*==============================================================*/




create nonclustered index FLCYB_DNIN_XFK on FACT_LOSS_CAL_YR_BASE (PRIM_NAMED_INSURED_ID ASC)
go

/*==============================================================*/
/* Index: FLCYB_DRTY_XFK                                        */
/*==============================================================*/




create nonclustered index FLCYB_DRTY_XFK on FACT_LOSS_CAL_YR_BASE (REC_TYPE_ID ASC)
go

/*==============================================================*/
/* Table: FACT_LOSS_CAL_YR_PRODUCT                              */
/*==============================================================*/
create table FACT_LOSS_CAL_YR_PRODUCT (
   LOSS_CAL_YR_PRODUCT_ID int                  not null,
   ORG_ID               int                  not null,
   PRODUCT_ID           int                  not null,
   COVG_ID              int                  not null,
   REC_TYPE_ID          int                  not null,
   ACCTG_PRD_ID         int                  not null,
   CURR_CD              varchar(255)         not null,
   OS_IND_RSRV_AMT      numeric(18,2)        not null,
   OS_MED_RSRV_AMT      numeric(18,2)        not null,
   OS_ALAE_RSRV_AMT     numeric(18,2)        not null,
   OS_ULAE_RSRV_AMT     numeric(18,2)        not null,
   OS_SLVG_RSRV_AMT     numeric(18,2)        not null,
   OS_SUBRO_RSRV_AMT    numeric(18,2)        not null,
   MTD_IND_PD_AMT       numeric(18,2)        not null,
   MTD_MED_PD_AMT       numeric(18,2)        not null,
   MTD_ALAE_PD_AMT      numeric(18,2)        not null,
   MTD_ULAE_PD_AMT      numeric(18,2)        not null,
   MTD_DED_RCVRY_AMT    numeric(18,2)        not null,
   MTD_SLVG_RCVRY_AMT   numeric(18,2)        not null,
   MTD_SUBRO_RCVRY_AMT  numeric(18,2)        not null,
   MTD_IND_RCVRY_AMT    numeric(18,2)        not null,
   MTD_EXP_RCVRY_AMT    numeric(18,2)        not null,
   MTD_IND_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_MED_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_ALAE_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_ULAE_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_SLVG_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_SUBRO_RSRV_CHNG_AMT numeric(18,2)        not null,
   YTD_IND_PD_AMT       numeric(18,2)        not null,
   YTD_MED_PD_AMT       numeric(18,2)        not null,
   YTD_ALAE_PD_AMT      numeric(18,2)        not null,
   YTD_ULAE_PD_AMT      numeric(18,2)        not null,
   YTD_DED_RCVRY_AMT    numeric(18,2)        not null,
   YTD_SLVG_RCVRY_AMT   numeric(18,2)        not null,
   YTD_SUBRO_RCVRY_AMT  numeric(18,2)        not null,
   YTD_IND_RCVRY_AMT    numeric(18,2)        not null,
   YTD_EXP_RCVRY_AMT    numeric(18,2)        not null,
   YTD_IND_RSRV_CHNG_AMT numeric(18,2)        not null,
   YTD_MED_RSRV_CHNG_AMT numeric(18,2)        not null,
   YTD_ALAE_RSRV_CHNG_AMT numeric(18,2)        not null,
   YTD_ULAE_RSRV_CHNG_AMT numeric(18,2)        not null,
   YTD_SLVG_RSRV_CHNG_AMT numeric(18,2)        not null,
   YTD_SUBRO_RSRV_CHNG_AMT numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_LOSS_CAL_YR_PRODUCT
   add constraint FLCYP_PK primary key nonclustered (LOSS_CAL_YR_PRODUCT_ID)
go

alter table FACT_LOSS_CAL_YR_PRODUCT
   add constraint FLCYP_AK1 unique (ORG_ID, PRODUCT_ID, COVG_ID, REC_TYPE_ID, ACCTG_PRD_ID, CURR_CD)
go

/*==============================================================*/
/* Index: FLCYP_DIPR_XFK                                        */
/*==============================================================*/




create nonclustered index FLCYP_DIPR_XFK on FACT_LOSS_CAL_YR_PRODUCT (PRODUCT_ID ASC)
go

/*==============================================================*/
/* Index: FLCYP_DICO_XFK                                        */
/*==============================================================*/




create nonclustered index FLCYP_DICO_XFK on FACT_LOSS_CAL_YR_PRODUCT (COVG_ID ASC)
go

/*==============================================================*/
/* Index: FLCYP_DIMO_XFK                                        */
/*==============================================================*/




create nonclustered index FLCYP_DIMO_XFK on FACT_LOSS_CAL_YR_PRODUCT (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: FLCYP_DRTY_XFK                                        */
/*==============================================================*/




create nonclustered index FLCYP_DRTY_XFK on FACT_LOSS_CAL_YR_PRODUCT (REC_TYPE_ID ASC)
go

/*==============================================================*/
/* Table: FACT_LOSS_DEV_AGG                                     */
/*==============================================================*/
create table FACT_LOSS_DEV_AGG (
   LOSS_DEV_AGG_ID      int                  not null,
   ORG_ID               int                  not null,
   PRODUCT_ID           int                  not null,
   COVG_ID              int                  not null,
   REGION_ID            int                  not null,
   LOSS_TYPE_INFO_ID    int                  not null,
   CATASTROPHE_ID       int                  not null,
   LOSS_MTH_ID          int                  not null,
   ACCTG_PRD_ID         int                  not null,
   MTH_AGING_ID         bigint               not null,
   CURR_CD              varchar(255)         not null,
   OS_IND_RSRV_AMT      numeric(18,2)        not null,
   OS_MED_RSRV_AMT      numeric(18,2)        not null,
   OS_ALAE_RSRV_AMT     numeric(18,2)        not null,
   OS_ULAE_RSRV_AMT     numeric(18,2)        not null,
   OS_SLVG_RSRV_AMT     numeric(18,2)        not null,
   OS_SUBRO_RSRV_AMT    numeric(18,2)        not null,
   IND_PD_AMT           numeric(18,2)        not null,
   MED_PD_AMT           numeric(18,2)        not null,
   ALAE_PD_AMT          numeric(18,2)        not null,
   ULAE_PD_AMT          numeric(18,2)        not null,
   DED_RCVRY_AMT        numeric(18,2)        not null,
   SLVG_RCVRY_AMT       numeric(18,2)        not null,
   SUBRO_RCVRY_AMT      numeric(18,2)        not null,
   IND_RCVRY_AMT        numeric(18,2)        not null,
   EXP_RCVRY_AMT        numeric(18,2)        not null,
   MTD_IND_PD_AMT       numeric(18,2)        not null,
   MTD_MED_PD_AMT       numeric(18,2)        not null,
   MTD_ALAE_PD_AMT      numeric(18,2)        not null,
   MTD_ULAE_PD_AMT      numeric(18,2)        not null,
   MTD_DED_RCVRY_AMT    numeric(18,2)        not null,
   MTD_SLVG_RCVRY_AMT   numeric(18,2)        not null,
   MTD_SUBRO_RCVRY_AMT  numeric(18,2)        not null,
   MTD_IND_RCVRY_AMT    numeric(18,2)        not null,
   MTD_EXP_RCVRY_AMT    numeric(18,2)        not null,
   MTD_IND_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_MED_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_ALAE_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_ULAE_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_SLVG_RSRV_CHNG_AMT numeric(18,2)        not null,
   MTD_SUBRO_RSRV_CHNG_AMT numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_LOSS_DEV_AGG
   add constraint FLDA_PK primary key nonclustered (LOSS_DEV_AGG_ID)
go

/*==============================================================*/
/* Index: FLDA_DICA_XFK                                         */
/*==============================================================*/




create nonclustered index FLDA_DICA_XFK on FACT_LOSS_DEV_AGG (CATASTROPHE_ID ASC)
go

/*==============================================================*/
/* Index: FLDA_DICO_XFK                                         */
/*==============================================================*/




create nonclustered index FLDA_DICO_XFK on FACT_LOSS_DEV_AGG (COVG_ID ASC)
go

/*==============================================================*/
/* Index: FLDA_DIMO_XFK                                         */
/*==============================================================*/




create nonclustered index FLDA_DIMO_XFK on FACT_LOSS_DEV_AGG (LOSS_MTH_ID ASC)
go

/*==============================================================*/
/* Index: FLDA_DIMO1_XFK                                        */
/*==============================================================*/




create nonclustered index FLDA_DIMO1_XFK on FACT_LOSS_DEV_AGG (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: FLDA_DIOR_XFK                                         */
/*==============================================================*/




create nonclustered index FLDA_DIOR_XFK on FACT_LOSS_DEV_AGG (ORG_ID ASC)
go

/*==============================================================*/
/* Index: FLDA_DIPR_XFK                                         */
/*==============================================================*/




create nonclustered index FLDA_DIPR_XFK on FACT_LOSS_DEV_AGG (PRODUCT_ID ASC)
go

/*==============================================================*/
/* Index: FLDA_DIRE_XFK                                         */
/*==============================================================*/




create nonclustered index FLDA_DIRE_XFK on FACT_LOSS_DEV_AGG (REGION_ID ASC)
go

/*==============================================================*/
/* Index: FLDA_DLTI_XFK                                         */
/*==============================================================*/




create nonclustered index FLDA_DLTI_XFK on FACT_LOSS_DEV_AGG (LOSS_TYPE_INFO_ID ASC)
go

/*==============================================================*/
/* Index: FLDA_DMAG_XFK                                         */
/*==============================================================*/




create nonclustered index FLDA_DMAG_XFK on FACT_LOSS_DEV_AGG (MTH_AGING_ID ASC)
go

/*==============================================================*/
/* Table: FACT_LOSS_ITD                                         */
/*==============================================================*/
create table FACT_LOSS_ITD (
   LOSS_ITD_ID          int                  not null,
   ORG_ID               int                  not null,
   PRODUCT_ID           int                  not null,
   POLICY_ID            int                  not null,
   COVG_ID              int                  not null,
   CLAIM_ID             int                  not null,
   CLMNT_ID             int                  not null,
   CLAIM_FEATURE_ID     int                  not null,
   CLAIM_FEATURE_STATUS_ID int                  not null,
   REC_TYPE_ID          int                  not null,
   ACCTG_PRD_ID         int                  not null,
   PRIM_NAMED_INSURED_ID int                  not null,
   ACCT_ID              int                  not null,
   CLAIM_STATUS_ID      int                  null,
   CURR_CD              varchar(255)         not null,
   OS_IND_RSRV_AMT      numeric(18,2)        not null,
   OS_MED_RSRV_AMT      numeric(18,2)        not null,
   OS_ALAE_RSRV_AMT     numeric(18,2)        not null,
   OS_ULAE_RSRV_AMT     numeric(18,2)        not null,
   OS_SLVG_RSRV_AMT     numeric(18,2)        not null,
   OS_SUBRO_RSRV_AMT    numeric(18,2)        not null,
   IND_PD_AMT           numeric(18,2)        not null,
   MED_PD_AMT           numeric(18,2)        not null,
   ALAE_PD_AMT          numeric(18,2)        not null,
   ULAE_PD_AMT          numeric(18,2)        not null,
   DED_RCVRY_AMT        numeric(18,2)        not null,
   SLVG_RCVRY_AMT       numeric(18,2)        not null,
   SUBRO_RCVRY_AMT      numeric(18,2)        not null,
   IND_RCVRY_AMT        numeric(18,2)        not null,
   EXP_RCVRY_AMT        numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_LOSS_ITD
   add constraint FLIT_PK primary key nonclustered (LOSS_ITD_ID)
go

alter table FACT_LOSS_ITD
   add constraint FLIT_AK1 unique (ORG_ID, PRODUCT_ID, POLICY_ID, COVG_ID, CLAIM_ID, CLMNT_ID, CLAIM_FEATURE_ID, CLAIM_FEATURE_STATUS_ID, REC_TYPE_ID, ACCTG_PRD_ID, PRIM_NAMED_INSURED_ID, ACCT_ID, CURR_CD)
go

/*==============================================================*/
/* Index: FLIT_DICL_XFK                                         */
/*==============================================================*/




create nonclustered index FLIT_DICL_XFK on FACT_LOSS_ITD (CLAIM_ID ASC)
go

/*==============================================================*/
/* Index: FLIT_DICO_XFK                                         */
/*==============================================================*/




create nonclustered index FLIT_DICO_XFK on FACT_LOSS_ITD (COVG_ID ASC)
go

/*==============================================================*/
/* Index: FLIT_DIMO_XFK                                         */
/*==============================================================*/




create nonclustered index FLIT_DIMO_XFK on FACT_LOSS_ITD (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: FLIT_DIPO_XFK                                         */
/*==============================================================*/




create nonclustered index FLIT_DIPO_XFK on FACT_LOSS_ITD (POLICY_ID ASC)
go

/*==============================================================*/
/* Index: FLIT_DIPR_XFK                                         */
/*==============================================================*/




create nonclustered index FLIT_DIPR_XFK on FACT_LOSS_ITD (PRODUCT_ID ASC)
go

/*==============================================================*/
/* Index: FLIT_DRTY_XFK                                         */
/*==============================================================*/




create nonclustered index FLIT_DRTY_XFK on FACT_LOSS_ITD (REC_TYPE_ID ASC)
go

/*==============================================================*/
/* Index: FLIT_DCST_XFK                                         */
/*==============================================================*/




create nonclustered index FLIT_DCST_XFK on FACT_LOSS_ITD (CLAIM_FEATURE_STATUS_ID ASC)
go

/*==============================================================*/
/* Index: FLIT_DCFE_XFK                                         */
/*==============================================================*/




create nonclustered index FLIT_DCFE_XFK on FACT_LOSS_ITD (CLAIM_FEATURE_ID ASC)
go

/*==============================================================*/
/* Index: FLIT_DICL1_XFK                                        */
/*==============================================================*/




create nonclustered index FLIT_DICL1_XFK on FACT_LOSS_ITD (CLMNT_ID ASC)
go

/*==============================================================*/
/* Index: FLIT_DNIN_XFK                                         */
/*==============================================================*/




create nonclustered index FLIT_DNIN_XFK on FACT_LOSS_ITD (PRIM_NAMED_INSURED_ID ASC)
go

/*==============================================================*/
/* Index: FLIT_DCAC_XFK                                         */
/*==============================================================*/




create nonclustered index FLIT_DCAC_XFK on FACT_LOSS_ITD (ACCT_ID ASC)
go

/*==============================================================*/
/* Index: FLIT_DCST1_XFK                                        */
/*==============================================================*/




create nonclustered index FLIT_DCST1_XFK on FACT_LOSS_ITD (CLAIM_STATUS_ID ASC)
go

/*==============================================================*/
/* Table: FACT_LOSS_TRAN                                        */
/*==============================================================*/
create table FACT_LOSS_TRAN (
   LOSS_TRANS_ID        int                  not null,
   ALT_LOSS_TRANS_ID    int                  not null,
   ORG_ID               int                  not null,
   PRODUCT_ID           int                  not null,
   POLICY_ID            int                  not null,
   LOC_ID               int                  not null,
   COVG_ID              int                  not null,
   CLAIM_ID             int                  not null,
   CLMNT_ID             int                  not null,
   CLAIM_FEATURE_ID     int                  not null,
   AGNT_ID              int                  not null,
   AGCY_ID              int                  not null,
   VENDOR_ID            int                  not null,
   REC_TYPE_ID          int                  not null,
   PRIM_NAMED_INSURED_ID int                  not null,
   TRANS_PROC_DT_ID     int                  not null,
   ACCTG_PRD_ID         int                  not null,
   CHECK_DT_ID          int                  not null,
   ACCT_ID              int                  not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   RISK_TYPE_CD         varchar(255)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   CHECK_NO             varchar(255)         not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   RSRV_CHNG_AMT        numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_LOSS_TRAN
   add constraint FLTR_PK primary key nonclustered (LOSS_TRANS_ID)
go

/*==============================================================*/
/* Index: FLTR_DIMO_XFK                                         */
/*==============================================================*/




create nonclustered index FLTR_DIMO_XFK on FACT_LOSS_TRAN (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: FLTR_DIOR_XFK                                         */
/*==============================================================*/




create nonclustered index FLTR_DIOR_XFK on FACT_LOSS_TRAN (ORG_ID ASC)
go

/*==============================================================*/
/* Index: FLTR_DICL_XFK                                         */
/*==============================================================*/




create nonclustered index FLTR_DICL_XFK on FACT_LOSS_TRAN (CLMNT_ID ASC)
go

/*==============================================================*/
/* Index: FLTR_DICL1_XFK                                        */
/*==============================================================*/




create nonclustered index FLTR_DICL1_XFK on FACT_LOSS_TRAN (CLAIM_ID ASC)
go

/*==============================================================*/
/* Index: FLTR_DICO_XFK                                         */
/*==============================================================*/




create nonclustered index FLTR_DICO_XFK on FACT_LOSS_TRAN (COVG_ID ASC)
go

/*==============================================================*/
/* Index: FLTR_DIDA_XFK                                         */
/*==============================================================*/




create nonclustered index FLTR_DIDA_XFK on FACT_LOSS_TRAN (TRANS_PROC_DT_ID ASC)
go

/*==============================================================*/
/* Index: FLTR_DILO_XFK                                         */
/*==============================================================*/




create nonclustered index FLTR_DILO_XFK on FACT_LOSS_TRAN (LOC_ID ASC)
go

/*==============================================================*/
/* Index: FLTR_DIPO_XFK                                         */
/*==============================================================*/




create nonclustered index FLTR_DIPO_XFK on FACT_LOSS_TRAN (POLICY_ID ASC)
go

/*==============================================================*/
/* Index: FLTR_DIPR_XFK                                         */
/*==============================================================*/




create nonclustered index FLTR_DIPR_XFK on FACT_LOSS_TRAN (PRODUCT_ID ASC)
go

/*==============================================================*/
/* Index: FLTR_DRTY_XFK                                         */
/*==============================================================*/




create nonclustered index FLTR_DRTY_XFK on FACT_LOSS_TRAN (REC_TYPE_ID ASC)
go

/*==============================================================*/
/* Index: FLTR_DIVE_XFK                                         */
/*==============================================================*/




create nonclustered index FLTR_DIVE_XFK on FACT_LOSS_TRAN (VENDOR_ID ASC)
go

/*==============================================================*/
/* Index: FLTR_DIAG_XFK                                         */
/*==============================================================*/




create nonclustered index FLTR_DIAG_XFK on FACT_LOSS_TRAN (AGNT_ID ASC)
go

/*==============================================================*/
/* Index: FLTR_DIAG1_XFK                                        */
/*==============================================================*/




create nonclustered index FLTR_DIAG1_XFK on FACT_LOSS_TRAN (AGCY_ID ASC)
go

/*==============================================================*/
/* Index: FLTR_DCFE_XFK                                         */
/*==============================================================*/




create nonclustered index FLTR_DCFE_XFK on FACT_LOSS_TRAN (CLAIM_FEATURE_ID ASC)
go

/*==============================================================*/
/* Index: FLTR_DIDA1_XFK                                        */
/*==============================================================*/




create nonclustered index FLTR_DIDA1_XFK on FACT_LOSS_TRAN (CHECK_DT_ID ASC)
go

/*==============================================================*/
/* Index: FLTR_DNIN_XFK                                         */
/*==============================================================*/




create nonclustered index FLTR_DNIN_XFK on FACT_LOSS_TRAN (PRIM_NAMED_INSURED_ID ASC)
go

/*==============================================================*/
/* Index: FLTR_DCAC_XFK                                         */
/*==============================================================*/




create nonclustered index FLTR_DCAC_XFK on FACT_LOSS_TRAN (ACCT_ID ASC)
go

/*==============================================================*/
/* Table: FACT_PA_PREM_TRAN                                     */
/*==============================================================*/
create table FACT_PA_PREM_TRAN (
   PA_PREM_TRANS_TID    int                  not null,
   PA_PREM_TRANS_KEY    varchar(100)         not null,
   ORG_ID               int                  not null,
   PRODUCT_ID           int                  not null,
   POLICY_ID            int                  not null,
   GARAGE_LOC_ID        int                  not null,
   AGNT_ID              int                  not null,
   REC_TYPE_ID          int                  not null,
   AGCY_ID              int                  not null,
   PRIM_NAMED_INSURED_ID int                  not null,
   ACCT_ID              int                  not null,
   PA_VEH_DID           int                  not null,
   COVG_ID              int                  not null,
   ACCTG_PRD_ID         int                  not null,
   TRANS_PROC_DT_ID     int                  not null,
   TRANS_EFF_DT_ID      int                  not null,
   TRANS_EXP_DT_ID      int                  not null,
   POL_LINE_KEY         varchar(100)         not null,
   REC_TYPE_KEY         varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   ORIG_TRANS_PROC_DTS  DATETIME2            not null,
   ORIG_END_EFF_DT      DATE                 not null,
   TRANS_EFF_DT         DATE                 not null,
   TRANS_EXP_DT         DATE                 not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   COMM_PCT             numeric(6,3)         not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   PREM_BASIS_AMT       numeric(18,2)        not null,
   AUDITABLE_FL         char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_PA_PREM_TRAN
   add constraint FPPT_PK primary key nonclustered (PA_PREM_TRANS_TID)
go

/*==============================================================*/
/* Index: FPPT_DIAG1_XFK                                        */
/*==============================================================*/




create nonclustered index FPPT_DIAG1_XFK on FACT_PA_PREM_TRAN (AGCY_ID ASC)
go

/*==============================================================*/
/* Index: FPPT_DIAG_XFK                                         */
/*==============================================================*/




create nonclustered index FPPT_DIAG_XFK on FACT_PA_PREM_TRAN (AGNT_ID ASC)
go

/*==============================================================*/
/* Index: FPPT_DIPR_XFK                                         */
/*==============================================================*/




create nonclustered index FPPT_DIPR_XFK on FACT_PA_PREM_TRAN (PRODUCT_ID ASC)
go

/*==============================================================*/
/* Index: FPPT_DIOR_XFK                                         */
/*==============================================================*/




create nonclustered index FPPT_DIOR_XFK on FACT_PA_PREM_TRAN (ORG_ID ASC)
go

/*==============================================================*/
/* Index: FPPT_DIPO_XFK                                         */
/*==============================================================*/




create nonclustered index FPPT_DIPO_XFK on FACT_PA_PREM_TRAN (POLICY_ID ASC)
go

/*==============================================================*/
/* Index: FPPT_DICO_XFK                                         */
/*==============================================================*/




create nonclustered index FPPT_DICO_XFK on FACT_PA_PREM_TRAN (COVG_ID ASC)
go

/*==============================================================*/
/* Index: FPPT_DCAC_XFK                                         */
/*==============================================================*/




create nonclustered index FPPT_DCAC_XFK on FACT_PA_PREM_TRAN (ACCT_ID ASC)
go

/*==============================================================*/
/* Index: FPPT_DILO_XFK                                         */
/*==============================================================*/




create nonclustered index FPPT_DILO_XFK on FACT_PA_PREM_TRAN (GARAGE_LOC_ID ASC)
go

/*==============================================================*/
/* Index: FPPT_DRTY_XFK                                         */
/*==============================================================*/




create nonclustered index FPPT_DRTY_XFK on FACT_PA_PREM_TRAN (REC_TYPE_ID ASC)
go

/*==============================================================*/
/* Index: FPPT_DNIN_XFK                                         */
/*==============================================================*/




create nonclustered index FPPT_DNIN_XFK on FACT_PA_PREM_TRAN (PRIM_NAMED_INSURED_ID ASC)
go

/*==============================================================*/
/* Index: FPPT_DPVE_XFK                                         */
/*==============================================================*/




create nonclustered index FPPT_DPVE_XFK on FACT_PA_PREM_TRAN (PA_VEH_DID ASC)
go

/*==============================================================*/
/* Index: FPPT_DIDA_XFK                                         */
/*==============================================================*/




create nonclustered index FPPT_DIDA_XFK on FACT_PA_PREM_TRAN (TRANS_EFF_DT_ID ASC)
go

/*==============================================================*/
/* Index: FPPT_DIDA1_XFK                                        */
/*==============================================================*/




create nonclustered index FPPT_DIDA1_XFK on FACT_PA_PREM_TRAN (TRANS_PROC_DT_ID ASC)
go

/*==============================================================*/
/* Index: FPPT_DIDA2_XFK                                        */
/*==============================================================*/




create nonclustered index FPPT_DIDA2_XFK on FACT_PA_PREM_TRAN (TRANS_EXP_DT_ID ASC)
go

/*==============================================================*/
/* Index: FPPT_DIMO_XFK                                         */
/*==============================================================*/




create nonclustered index FPPT_DIMO_XFK on FACT_PA_PREM_TRAN (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Table: FACT_POLICY_STATUS_TRAN                               */
/*==============================================================*/
create table FACT_POLICY_STATUS_TRAN (
   POL_STATUS_TRANS_ID  int                  not null,
   ALT_POL_STATUS_TRANS_ID int                  not null,
   POLICY_ID            int                  not null,
   POL_STATUS_ID        int                  not null,
   ACCTG_PRD_ID         int                  not null,
   PRIM_NAMED_INSURED_ID int                  not null,
   ACCT_ID              int                  not null,
   STATUS_PROC_DT_ID    int                  not null,
   STATUS_EFF_DT_ID     int                  not null,
   STATUS_PROC_DTS      DATETIME2            not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_POLICY_STATUS_TRAN
   add constraint FPST_PK primary key nonclustered (POL_STATUS_TRANS_ID)
go

/*==============================================================*/
/* Index: FPST_DIDA_XFK                                         */
/*==============================================================*/




create nonclustered index FPST_DIDA_XFK on FACT_POLICY_STATUS_TRAN (STATUS_EFF_DT_ID ASC)
go

/*==============================================================*/
/* Index: FPST_DIDA1_XFK                                        */
/*==============================================================*/




create nonclustered index FPST_DIDA1_XFK on FACT_POLICY_STATUS_TRAN (STATUS_PROC_DT_ID ASC)
go

/*==============================================================*/
/* Index: FPST_DIMO_XFK                                         */
/*==============================================================*/




create nonclustered index FPST_DIMO_XFK on FACT_POLICY_STATUS_TRAN (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: FPST_DPST_XFK                                         */
/*==============================================================*/




create nonclustered index FPST_DPST_XFK on FACT_POLICY_STATUS_TRAN (POL_STATUS_ID ASC)
go

/*==============================================================*/
/* Index: FPST_DIPO_XFK                                         */
/*==============================================================*/




create nonclustered index FPST_DIPO_XFK on FACT_POLICY_STATUS_TRAN (POLICY_ID ASC)
go

/*==============================================================*/
/* Index: FPST_DNIN_XFK                                         */
/*==============================================================*/




create nonclustered index FPST_DNIN_XFK on FACT_POLICY_STATUS_TRAN (PRIM_NAMED_INSURED_ID ASC)
go

/*==============================================================*/
/* Index: FPST_DCAC_XFK                                         */
/*==============================================================*/




create nonclustered index FPST_DCAC_XFK on FACT_POLICY_STATUS_TRAN (ACCT_ID ASC)
go

/*==============================================================*/
/* Table: FACT_PREM_CAL_YR                                      */
/*==============================================================*/
create table FACT_PREM_CAL_YR (
   PREM_CAL_YR_ID       int                  not null,
   ORG_ID               int                  not null,
   PRODUCT_ID           int                  not null,
   COVG_ID              int                  not null,
   AGNT_ID              int                  not null,
   REC_TYPE_ID          int                  not null,
   AGCY_ID              int                  not null,
   ACCTG_PRD_ID         int                  not null,
   CURR_CD              varchar(255)         not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   UNEARNED_PREM_AMT    numeric(18,2)        not null,
   MTD_WRITTEN_PREM_AMT numeric(18,2)        not null,
   MTD_COMM_AMT         numeric(18,2)        not null,
   MTD_EARNED_PREM_AMT  numeric(18,2)        not null,
   QTD_WRITTEN_PREM_AMT numeric(18,2)        not null,
   QTD_COMM_AMT         numeric(18,2)        not null,
   QTD_EARNED_PREM_AMT  numeric(18,2)        not null,
   YTD_WRITTEN_PREM_AMT numeric(18,2)        not null,
   YTD_COMM_AMT         numeric(18,2)        not null,
   YTD_EARNED_PREM_AMT  numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_PREM_CAL_YR
   add constraint FPCY_PK primary key nonclustered (PREM_CAL_YR_ID)
go

alter table FACT_PREM_CAL_YR
   add constraint FPCY_AK1 unique (ORG_ID, PRODUCT_ID, COVG_ID, AGNT_ID, REC_TYPE_ID, AGCY_ID, ACCTG_PRD_ID, CURR_CD)
go

/*==============================================================*/
/* Index: FPCY_DRTY_XFK                                         */
/*==============================================================*/




create nonclustered index FPCY_DRTY_XFK on FACT_PREM_CAL_YR (REC_TYPE_ID ASC)
go

/*==============================================================*/
/* Index: FPCY_DIMO_XFK                                         */
/*==============================================================*/




create nonclustered index FPCY_DIMO_XFK on FACT_PREM_CAL_YR (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: FPCY_DIPR_XFK                                         */
/*==============================================================*/




create nonclustered index FPCY_DIPR_XFK on FACT_PREM_CAL_YR (PRODUCT_ID ASC)
go

/*==============================================================*/
/* Index: FPCY_DICO_XFK                                         */
/*==============================================================*/




create nonclustered index FPCY_DICO_XFK on FACT_PREM_CAL_YR (COVG_ID ASC)
go

/*==============================================================*/
/* Index: FPCY_DIAG_XFK                                         */
/*==============================================================*/




create nonclustered index FPCY_DIAG_XFK on FACT_PREM_CAL_YR (AGCY_ID ASC)
go

/*==============================================================*/
/* Index: FPCY_DIAG1_XFK                                        */
/*==============================================================*/




create nonclustered index FPCY_DIAG1_XFK on FACT_PREM_CAL_YR (AGNT_ID ASC)
go

/*==============================================================*/
/* Table: FACT_PREM_CAL_YR_AGENT_GRWTH                          */
/*==============================================================*/
create table FACT_PREM_CAL_YR_AGENT_GRWTH (
   PREM_CAL_YR_AGNT_GRWTH_ID int                  not null,
   ORG_ID               int                  not null,
   PRODUCT_ID           int                  not null,
   AGCY_ID              int                  not null,
   AGNT_ID              int                  not null,
   REC_TYPE_ID          int                  not null,
   ACCTG_PRD_ID         int                  not null,
   CURR_CD              varchar(255)         not null,
   YTD_WRITTEN_PREM_AMT numeric(18,2)        not null,
   PY_YTD_WRITTEN_PREM_AMT numeric(18,2)        not null,
   NEW_WRITTEN_PREM_AMT numeric(18,2)        not null,
   LAPSED_WRITTEN_PREM_AMT numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_PREM_CAL_YR_AGENT_GRWTH
   add constraint FPCYAG_PK primary key nonclustered (PREM_CAL_YR_AGNT_GRWTH_ID)
go

/*==============================================================*/
/* Index: FPCYAG_DIOR_XFK                                       */
/*==============================================================*/




create nonclustered index FPCYAG_DIOR_XFK on FACT_PREM_CAL_YR_AGENT_GRWTH (ORG_ID ASC)
go

/*==============================================================*/
/* Index: FPCYAG_DIPR_XFK                                       */
/*==============================================================*/




create nonclustered index FPCYAG_DIPR_XFK on FACT_PREM_CAL_YR_AGENT_GRWTH (PRODUCT_ID ASC)
go

/*==============================================================*/
/* Index: FPCYAG_DIAG_XFK                                       */
/*==============================================================*/




create nonclustered index FPCYAG_DIAG_XFK on FACT_PREM_CAL_YR_AGENT_GRWTH (AGCY_ID ASC)
go

/*==============================================================*/
/* Index: FPCYAG_DIAG1_XFK                                      */
/*==============================================================*/




create nonclustered index FPCYAG_DIAG1_XFK on FACT_PREM_CAL_YR_AGENT_GRWTH (AGNT_ID ASC)
go

/*==============================================================*/
/* Index: FPCYAG_DRTY_XFK                                       */
/*==============================================================*/




create nonclustered index FPCYAG_DRTY_XFK on FACT_PREM_CAL_YR_AGENT_GRWTH (REC_TYPE_ID ASC)
go

/*==============================================================*/
/* Index: FPCYAG_DIMO_XFK                                       */
/*==============================================================*/




create nonclustered index FPCYAG_DIMO_XFK on FACT_PREM_CAL_YR_AGENT_GRWTH (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Table: FACT_PREM_CAL_YR_BASE                                 */
/*==============================================================*/
create table FACT_PREM_CAL_YR_BASE (
   PREM_CAL_YR_BASE_ID  int                  not null,
   ORG_ID               int                  not null,
   PRODUCT_ID           int                  not null,
   POLICY_ID            int                  not null,
   LOC_ID               int                  not null,
   COVG_ID              int                  not null,
   AGNT_ID              int                  not null,
   REC_TYPE_ID          int                  not null,
   AGCY_ID              int                  not null,
   ACCTG_PRD_ID         int                  not null,
   PRIM_NAMED_INSURED_ID int                  not null,
   ACCT_ID              int                  not null,
   MTH_END_POL_STATUS_ID int                  not null,
   WRITTEN_POL_NO       varchar(255)         null,
   INFORCE_POL_NO       varchar(255)         null,
   NEW_BUS_FL           char(1)              not null,
   CURR_CD              varchar(255)         not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   UNEARNED_PREM_AMT    numeric(18,2)        not null,
   MTD_WRITTEN_PREM_AMT numeric(18,2)        not null,
   MTD_EARNED_PREM_AMT  numeric(18,2)        not null,
   QTD_WRITTEN_PREM_AMT numeric(18,2)        not null,
   QTD_EARNED_PREM_AMT  numeric(18,2)        not null,
   YTD_WRITTEN_PREM_AMT numeric(18,2)        not null,
   YTD_EARNED_PREM_AMT  numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_PREM_CAL_YR_BASE
   add constraint FPCYB_PK primary key nonclustered (PREM_CAL_YR_BASE_ID)
go

/*==============================================================*/
/* Index: FPCYB_DCAC_XFK                                        */
/*==============================================================*/




create nonclustered index FPCYB_DCAC_XFK on FACT_PREM_CAL_YR_BASE (ACCT_ID ASC)
go

/*==============================================================*/
/* Index: FPCYB_DIAG_XFK                                        */
/*==============================================================*/




create nonclustered index FPCYB_DIAG_XFK on FACT_PREM_CAL_YR_BASE (AGNT_ID ASC)
go

/*==============================================================*/
/* Index: FPCYB_DIAG1_XFK                                       */
/*==============================================================*/




create nonclustered index FPCYB_DIAG1_XFK on FACT_PREM_CAL_YR_BASE (AGCY_ID ASC)
go

/*==============================================================*/
/* Index: FPCYB_DICO_XFK                                        */
/*==============================================================*/




create nonclustered index FPCYB_DICO_XFK on FACT_PREM_CAL_YR_BASE (COVG_ID ASC)
go

/*==============================================================*/
/* Index: FPCYB_DILO_XFK                                        */
/*==============================================================*/




create nonclustered index FPCYB_DILO_XFK on FACT_PREM_CAL_YR_BASE (LOC_ID ASC)
go

/*==============================================================*/
/* Index: FPCYB_DIMO_XFK                                        */
/*==============================================================*/




create nonclustered index FPCYB_DIMO_XFK on FACT_PREM_CAL_YR_BASE (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: FPCYB_DIOR_XFK                                        */
/*==============================================================*/




create nonclustered index FPCYB_DIOR_XFK on FACT_PREM_CAL_YR_BASE (ORG_ID ASC)
go

/*==============================================================*/
/* Index: FPCYB_DIPO_XFK                                        */
/*==============================================================*/




create nonclustered index FPCYB_DIPO_XFK on FACT_PREM_CAL_YR_BASE (POLICY_ID ASC)
go

/*==============================================================*/
/* Index: FPCYB_DIPR_XFK                                        */
/*==============================================================*/




create nonclustered index FPCYB_DIPR_XFK on FACT_PREM_CAL_YR_BASE (PRODUCT_ID ASC)
go

/*==============================================================*/
/* Index: FPCYB_DNIN_XFK                                        */
/*==============================================================*/




create nonclustered index FPCYB_DNIN_XFK on FACT_PREM_CAL_YR_BASE (PRIM_NAMED_INSURED_ID ASC)
go

/*==============================================================*/
/* Index: FPCYB_DPST_XFK                                        */
/*==============================================================*/




create nonclustered index FPCYB_DPST_XFK on FACT_PREM_CAL_YR_BASE (MTH_END_POL_STATUS_ID ASC)
go

/*==============================================================*/
/* Index: FPCYB_DRTY_XFK                                        */
/*==============================================================*/




create nonclustered index FPCYB_DRTY_XFK on FACT_PREM_CAL_YR_BASE (REC_TYPE_ID ASC)
go

/*==============================================================*/
/* Table: FACT_PREM_CAL_YR_PRODUCT                              */
/*==============================================================*/
create table FACT_PREM_CAL_YR_PRODUCT (
   PREM_CAL_YR_PRODUCT_ID int                  not null,
   ORG_ID               int                  not null,
   PRODUCT_ID           int                  not null,
   COVG_ID              int                  not null,
   REC_TYPE_ID          int                  not null,
   ACCTG_PRD_ID         int                  not null,
   CURR_CD              varchar(255)         not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   NP_INFORCE_PREM_AMT  numeric(18,2)        not null,
   RP_INFORCE_PREM_AMT  numeric(18,2)        not null,
   UNEARNED_PREM_AMT    numeric(18,2)        not null,
   NP_UNEARNED_PREM_AMT numeric(18,2)        not null,
   RP_UNEARNED_PREM_AMT numeric(18,2)        not null,
   MTD_WRITTEN_PREM_AMT numeric(18,2)        not null,
   MTD_NP_WRITTEN_PREM_AMT numeric(18,2)        not null,
   MTD_RP_WRITTEN_PREM_AMT numeric(18,2)        not null,
   MTD_EARNED_PREM_AMT  numeric(18,2)        not null,
   MTD_NP_EARNED_PREM_AMT numeric(18,2)        not null,
   MTD_RP_EARNED_PREM_AMT numeric(18,2)        not null,
   YTD_WRITTEN_PREM_AMT numeric(18,2)        not null,
   YTD_NP_WRITTEN_PREM_AMT numeric(18,2)        not null,
   YTD_RP_WRITTEN_PREM_AMT numeric(18,2)        not null,
   YTD_EARNED_PREM_AMT  numeric(18,2)        not null,
   YTD_NP_EARNED_PREM_AMT numeric(18,2)        not null,
   YTD_RP_EARNED_PREM_AMT numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_PREM_CAL_YR_PRODUCT
   add constraint FPCYP_PK primary key nonclustered (PREM_CAL_YR_PRODUCT_ID)
go

alter table FACT_PREM_CAL_YR_PRODUCT
   add constraint FPCYP_AK1 unique (ORG_ID, PRODUCT_ID, COVG_ID, REC_TYPE_ID, ACCTG_PRD_ID, CURR_CD)
go

/*==============================================================*/
/* Index: FPCYP_DIPR_XFK                                        */
/*==============================================================*/




create nonclustered index FPCYP_DIPR_XFK on FACT_PREM_CAL_YR_PRODUCT (PRODUCT_ID ASC)
go

/*==============================================================*/
/* Index: FPCYP_DICO_XFK                                        */
/*==============================================================*/




create nonclustered index FPCYP_DICO_XFK on FACT_PREM_CAL_YR_PRODUCT (COVG_ID ASC)
go

/*==============================================================*/
/* Index: FPCYP_DIMO_XFK                                        */
/*==============================================================*/




create nonclustered index FPCYP_DIMO_XFK on FACT_PREM_CAL_YR_PRODUCT (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: FPCYP_DRTY_XFK                                        */
/*==============================================================*/




create nonclustered index FPCYP_DRTY_XFK on FACT_PREM_CAL_YR_PRODUCT (REC_TYPE_ID ASC)
go

/*==============================================================*/
/* Table: FACT_PREM_ITD                                         */
/*==============================================================*/
create table FACT_PREM_ITD (
   FACT_PREM_ITD_ID     int                  not null,
   PRODUCT_ID           int                  not null,
   ORG_ID               int                  not null,
   AGNT_ID              int                  not null,
   REC_TYPE_ID          int                  not null,
   AGCY_ID              int                  not null,
   POL_YR               int                  not null,
   ACCTG_PRD_ID         int                  not null,
   POLICY_ID            int                  not null,
   POL_STATUS_ID        int                  not null,
   ACCT_ID              int                  not null,
   PRIM_NAMED_INSURED_ID int                  not null,
   CURR_CD              varchar(255)         not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   EST_PREM_AMT         numeric(18,2)        not null,
   RPT_PREM_AMT         numeric(18,2)        not null,
   AUDIT_PREM_AMT       numeric(18,2)        not null,
   CANCEL_PREM_AMT      numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   NEW_BUS_PREM_AMT     numeric(18,2)        not null,
   RENEW_BUS_PREM_AMT   numeric(18,2)        not null,
   ENDRSMNT_PREM_AMT    numeric(18,2)        not null,
   REINST_PREM_AMT      numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_PREM_ITD
   add constraint FPIT_PK primary key nonclustered (FACT_PREM_ITD_ID)
go

alter table FACT_PREM_ITD
   add constraint FPIT_AK1 unique (PRODUCT_ID, ORG_ID, AGNT_ID, REC_TYPE_ID, AGCY_ID, POL_YR, ACCTG_PRD_ID, POLICY_ID, POL_STATUS_ID, ACCT_ID, PRIM_NAMED_INSURED_ID, CURR_CD)
go

/*==============================================================*/
/* Index: FPIT_DIOR_XFK                                         */
/*==============================================================*/




create nonclustered index FPIT_DIOR_XFK on FACT_PREM_ITD (ORG_ID ASC)
go

/*==============================================================*/
/* Index: FPIT_DRTY_XFK                                         */
/*==============================================================*/




create nonclustered index FPIT_DRTY_XFK on FACT_PREM_ITD (REC_TYPE_ID ASC)
go

/*==============================================================*/
/* Index: FPIT_DIAG_XFK                                         */
/*==============================================================*/




create nonclustered index FPIT_DIAG_XFK on FACT_PREM_ITD (AGNT_ID ASC)
go

/*==============================================================*/
/* Index: FPIT_DIAG1_XFK                                        */
/*==============================================================*/




create nonclustered index FPIT_DIAG1_XFK on FACT_PREM_ITD (AGCY_ID ASC)
go

/*==============================================================*/
/* Index: FPIT_DIYE_XFK                                         */
/*==============================================================*/




create nonclustered index FPIT_DIYE_XFK on FACT_PREM_ITD (POL_YR ASC)
go

/*==============================================================*/
/* Index: FPIT_DIMO_XFK                                         */
/*==============================================================*/




create nonclustered index FPIT_DIMO_XFK on FACT_PREM_ITD (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: FPIT_DIPO_XFK                                         */
/*==============================================================*/




create nonclustered index FPIT_DIPO_XFK on FACT_PREM_ITD (POLICY_ID ASC)
go

/*==============================================================*/
/* Index: FPIT_DPST_XFK                                         */
/*==============================================================*/




create nonclustered index FPIT_DPST_XFK on FACT_PREM_ITD (POL_STATUS_ID ASC)
go

/*==============================================================*/
/* Index: FPIT_DCAC_XFK                                         */
/*==============================================================*/




create nonclustered index FPIT_DCAC_XFK on FACT_PREM_ITD (ACCT_ID ASC)
go

/*==============================================================*/
/* Index: FPIT_DNIN_XFK                                         */
/*==============================================================*/




create nonclustered index FPIT_DNIN_XFK on FACT_PREM_ITD (PRIM_NAMED_INSURED_ID ASC)
go

/*==============================================================*/
/* Table: FACT_PREM_TRAN                                        */
/*==============================================================*/
create table FACT_PREM_TRAN (
   PREM_TRANS_ID        int                  not null,
   ALT_PREM_TRANS_ID    int                  not null,
   ORG_ID               int                  not null,
   PRODUCT_ID           int                  not null,
   POLICY_ID            int                  not null,
   LOC_ID               int                  not null,
   COVG_ID              int                  not null,
   AGNT_ID              int                  not null,
   REC_TYPE_ID          int                  not null,
   AGCY_ID              int                  not null,
   ACCTG_PRD_ID         int                  not null,
   TRANS_PROC_DT_ID     int                  not null,
   TRANS_EFF_DT_ID      int                  not null,
   TRANS_EXP_DT_ID      int                  not null,
   PRIM_NAMED_INSURED_ID int                  not null,
   ACCT_ID              int                  not null,
   TRANS_PROC_DTS       DATETIME2            not null,
   RISK_TYPE_CD         varchar(255)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   TRANS_TYPE_CD        varchar(255)         not null,
   TRANS_CD             varchar(255)         not null,
   TRANS_SEQ            bigint               not null,
   CURR_CD              varchar(255)         not null,
   TRANS_AMT            numeric(18,2)        not null,
   TRANS_ALLOC_CD       varchar(255)         not null,
   WRITTEN_PREM_AMT     numeric(18,2)        not null,
   INFORCE_PREM_AMT     numeric(18,2)        not null,
   TAX_AMT              numeric(18,2)        not null,
   FEE_AMT              numeric(18,2)        not null,
   SRCHG_AMT            numeric(18,2)        not null,
   COMM_AMT             numeric(18,2)        not null,
   COMM_PCT             numeric(6,3)         not null,
   PREM_FULLY_EARNED_FL char(1)              not null,
   EST_PREM_AMT         numeric(18,2)        not null,
   RPT_PREM_AMT         numeric(18,2)        not null,
   AUDIT_PREM_AMT       numeric(18,2)        not null,
   PREM_BASIS_AMT       numeric(18,2)        not null,
   AUDITABLE_FL         char(1)              not null,
   ETL_ACTIVE_FL        char(1)              not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null
)
go

alter table FACT_PREM_TRAN
   add constraint FPTR_PK primary key nonclustered (PREM_TRANS_ID)
go

/*==============================================================*/
/* Index: FPTR_DICO_XFK                                         */
/*==============================================================*/




create nonclustered index FPTR_DICO_XFK on FACT_PREM_TRAN (COVG_ID ASC)
go

/*==============================================================*/
/* Index: FPTR_DIDA_XFK                                         */
/*==============================================================*/




create nonclustered index FPTR_DIDA_XFK on FACT_PREM_TRAN (TRANS_PROC_DT_ID ASC)
go

/*==============================================================*/
/* Index: FPTR_DILO_XFK                                         */
/*==============================================================*/




create nonclustered index FPTR_DILO_XFK on FACT_PREM_TRAN (LOC_ID ASC)
go

/*==============================================================*/
/* Index: FPTR_DIPO_XFK                                         */
/*==============================================================*/




create nonclustered index FPTR_DIPO_XFK on FACT_PREM_TRAN (POLICY_ID ASC)
go

/*==============================================================*/
/* Index: FPTR_DIPR_XFK                                         */
/*==============================================================*/




create nonclustered index FPTR_DIPR_XFK on FACT_PREM_TRAN (PRODUCT_ID ASC)
go

/*==============================================================*/
/* Index: FPTR_DRTY_XFK                                         */
/*==============================================================*/




create nonclustered index FPTR_DRTY_XFK on FACT_PREM_TRAN (REC_TYPE_ID ASC)
go

/*==============================================================*/
/* Index: FPTR_DIMO_XFK                                         */
/*==============================================================*/




create nonclustered index FPTR_DIMO_XFK on FACT_PREM_TRAN (ACCTG_PRD_ID ASC)
go

/*==============================================================*/
/* Index: FPTR_DIAG_XFK                                         */
/*==============================================================*/




create nonclustered index FPTR_DIAG_XFK on FACT_PREM_TRAN (AGNT_ID ASC)
go

/*==============================================================*/
/* Index: FPTR_DIAG1_XFK                                        */
/*==============================================================*/




create nonclustered index FPTR_DIAG1_XFK on FACT_PREM_TRAN (AGCY_ID ASC)
go

/*==============================================================*/
/* Index: FPTR_DIOR_XFK                                         */
/*==============================================================*/




create nonclustered index FPTR_DIOR_XFK on FACT_PREM_TRAN (ORG_ID ASC)
go

/*==============================================================*/
/* Index: FPTR_DIDA1_XFK                                        */
/*==============================================================*/




create nonclustered index FPTR_DIDA1_XFK on FACT_PREM_TRAN (TRANS_EFF_DT_ID ASC)
go

/*==============================================================*/
/* Index: FPTR_DIDA2_XFK                                        */
/*==============================================================*/




create nonclustered index FPTR_DIDA2_XFK on FACT_PREM_TRAN (TRANS_EXP_DT_ID ASC)
go

/*==============================================================*/
/* Index: FPTR_DNIN_XFK                                         */
/*==============================================================*/




create nonclustered index FPTR_DNIN_XFK on FACT_PREM_TRAN (PRIM_NAMED_INSURED_ID ASC)
go

/*==============================================================*/
/* Index: FPTR_DCAC_XFK                                         */
/*==============================================================*/




create nonclustered index FPTR_DCAC_XFK on FACT_PREM_TRAN (ACCT_ID ASC)
go

/*==============================================================*/
/* Table: RLS_USER_AGENT                                        */
/*==============================================================*/
create table RLS_USER_AGENT (
   USER_AGNT_ID         int                  not null,
   USER_AGNT_KEY        varchar(100)         not null,
   SOURCE_SYSTEM        varchar(10)          not null,
   PC_USERNAME          varchar(255)         not null,
   AGNT_KEY             varchar(100)         not null,
   AGNT_ID              int                  not null,
   ETL_ADD_DTS          DATETIME2            null,
   ETL_LAST_UPDATE_DTS  DATETIME2            not null,
   ETL_ACTIVE_FL        char(1)              not null
)
go

alter table RLS_USER_AGENT
   add constraint RUAG_PK primary key nonclustered (USER_AGNT_ID)
go

alter table RLS_USER_AGENT
   add constraint RUAG_AK1 unique (PC_USERNAME, AGNT_KEY, AGNT_ID, SOURCE_SYSTEM)
go

/*==============================================================*/
/* Table: SYSTEM_INFO                                           */
/*==============================================================*/
create table SYSTEM_INFO (
   KEY_NAME             varchar(100)         not null,
   KEY_VAL              varchar(255)         not null
)
go

alter table SYSTEM_INFO
   add constraint SYIN_PK primary key nonclustered (KEY_NAME)
go

alter table DIM_CLAIM
   add constraint FK_DICL_DCST foreign key (LAST_STATUS_ID)
      references DIM_CLAIM_STATUS (CLAIM_STATUS_ID)
go

alter table DIM_CLAIM_FEATURE
   add constraint FK_DCFE_DCST foreign key (LAST_STATUS_ID)
      references DIM_CLAIM_STATUS (CLAIM_STATUS_ID)
go

alter table DIM_POLICY
   add constraint FK_DIPO_DPST foreign key (LAST_STATUS_ID)
      references DIM_POLICY_STATUS (POL_STATUS_ID)
go

alter table FACT_AP_TRAN
   add constraint FK_FATR_DBCC1 foreign key (CHRG_COMM_DID)
      references DIM_BILL_CHRG_COMM (CHRG_COMM_DID)
go

alter table FACT_AP_TRAN
   add constraint FK_FATR_DBCR foreign key (COMM_RED_DID)
      references DIM_BILL_COMM_REDUCT (COMM_RED_DID)
go

alter table FACT_AP_TRAN
   add constraint FK_FATR_DBDI2 foreign key (AGCY_CYCLE_DISTR_DID)
      references DIM_BILL_DISTR (DISTR_DID)
go

alter table FACT_AP_TRAN
   add constraint FK_FATR_DBDI3 foreign key (DISTR_ITEM_DID)
      references DIM_BILL_DISTR_ITEM (DISTR_ITEM_DID)
go

alter table FACT_AP_TRAN
   add constraint FK_FATR_DBDI4 foreign key (DISTR_DID)
      references DIM_BILL_DISTR (DISTR_DID)
go

alter table FACT_AP_TRAN
   add constraint FK_FATR_DBII1 foreign key (BILL_INV_ITEM_DID)
      references DIM_BILL_INV_ITEM (BILL_INV_ITEM_DID)
go

alter table FACT_AP_TRAN
   add constraint FK_FATR_DBIN1 foreign key (BILL_INV_DID)
      references DIM_BILL_INV (BILL_INV_DID)
go

alter table FACT_AP_TRAN
   add constraint FK_FATR_DBNW foreign key (NEG_WO_DID)
      references DIM_BILL_NEG_WRITEOFF (NEG_WO_DID)
go

alter table FACT_AP_TRAN
   add constraint FK_FATR_DBPC foreign key (PRDCR_CD_DID)
      references DIM_BILL_PROD_CD (PRDCR_CD_DID)
go

alter table FACT_AP_TRAN
   add constraint FK_FATR_DBPO1 foreign key (BILL_POL_DID)
      references DIM_BILL_POL (BILL_POL_DID)
go

alter table FACT_AP_TRAN
   add constraint FK_FATR_DBPP foreign key (PRDCR_PMT_DID)
      references DIM_BILL_PROD_PMT (PRDCR_PMT_DID)
go

alter table FACT_AP_TRAN
   add constraint FK_FATR_DBPP2 foreign key (BILL_POL_PRD_DID)
      references DIM_BILL_POL_PER (BILL_POL_PRD_DID)
go

alter table FACT_AP_TRAN
   add constraint FK_FATR_DBPR1 foreign key (PRDCR_DID)
      references DIM_BILL_PROD (PRDCR_DID)
go

alter table FACT_AP_TRAN
   add constraint FK_FATR_DBPR2 foreign key (CR_PYBL_OF_PRDCR_DID)
      references DIM_BILL_PROD (PRDCR_DID)
go

alter table FACT_AP_TRAN
   add constraint FK_FATR_DBPS foreign key (PRDCR_STMNT_DID)
      references DIM_BILL_PROD_STMNT (PRDCR_STMNT_DID)
go

alter table FACT_AP_TRAN
   add constraint FK_FATR_DBPS1 foreign key (PYBL_RECEIVER_STMNT_DID)
      references DIM_BILL_PROD_STMNT (PRDCR_STMNT_DID)
go

alter table FACT_AP_TRAN
   add constraint FK_FATR_DBWR foreign key (WO_DID)
      references DIM_BILL_WRITEOFF (WO_DID)
go

alter table FACT_AP_TRAN
   add constraint FK_FATR_DIDA1 foreign key (TRANS_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_AR_AGING
   add constraint FK_FAAG_DBAC foreign key (BILL_ACCT_DID)
      references DIM_BILL_ACCT (BILL_ACCT_DID)
go

alter table FACT_AR_AGING
   add constraint FK_FAAG_DBIN foreign key (BILL_INV_DID)
      references DIM_BILL_INV (BILL_INV_DID)
go

alter table FACT_AR_AGING
   add constraint FK_FAAG_DBPO foreign key (BILL_POL_DID)
      references DIM_BILL_POL (BILL_POL_DID)
go

alter table FACT_AR_AGING
   add constraint FK_FAAG_DBPP foreign key (BILL_POL_PRD_DID)
      references DIM_BILL_POL_PER (BILL_POL_PRD_DID)
go

alter table FACT_AR_AGING
   add constraint FK_FAAG_DBPR foreign key (PRDCR_DID)
      references DIM_BILL_PROD (PRDCR_DID)
go

alter table FACT_AR_AGING
   add constraint FK_FAAG_DIDA foreign key (PMT_DUE_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_AR_AGING
   add constraint FK_FAAG_DIDA1 foreign key (CHRG_EFF_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_AR_AGING
   add constraint FK_FAAG_DIDA2 foreign key (INV_PROC_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_AR_AGING
   add constraint FK_FAAG_DIMO foreign key (SNAPSHOT_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_AR_BALANCING
   add constraint FK_FABA_DBCP foreign key (CHRG_PAT_DID)
      references DIM_BILL_CHRG_PAT (CHRG_PAT_DID)
go

alter table FACT_AR_BALANCING
   add constraint FK_FABA_DBPO foreign key (BILL_POL_DID)
      references DIM_BILL_POL (BILL_POL_DID)
go

alter table FACT_AR_BALANCING
   add constraint FK_FABA_DBPP foreign key (BILL_POL_PRD_DID)
      references DIM_BILL_POL_PER (BILL_POL_PRD_DID)
go

alter table FACT_AR_BALANCING
   add constraint FK_FABA_DIMO foreign key (SNAPSHOT_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_AR_TRAN
   add constraint FK_FATR_DBAC foreign key (BILL_ACCT_DID)
      references DIM_BILL_ACCT (BILL_ACCT_DID)
go

alter table FACT_AR_TRAN
   add constraint FK_FATR_DBCC foreign key (CHRG_COMM_DID)
      references DIM_BILL_CHRG_COMM (CHRG_COMM_DID)
go

alter table FACT_AR_TRAN
   add constraint FK_FATR_DBCH foreign key (CHRG_DID)
      references DIM_BILL_CHRG (CHRG_DID)
go

alter table FACT_AR_TRAN
   add constraint FK_FATR_DBCP foreign key (CHRG_PAT_DID)
      references DIM_BILL_CHRG_PAT (CHRG_PAT_DID)
go

alter table FACT_AR_TRAN
   add constraint FK_FATR_DBDI foreign key (DISTR_DID)
      references DIM_BILL_DISTR (DISTR_DID)
go

alter table FACT_AR_TRAN
   add constraint FK_FATR_DBDI1 foreign key (DISTR_ITEM_DID)
      references DIM_BILL_DISTR_ITEM (DISTR_ITEM_DID)
go

alter table FACT_AR_TRAN
   add constraint FK_FATR_DBII foreign key (BILL_INV_ITEM_DID)
      references DIM_BILL_INV_ITEM (BILL_INV_ITEM_DID)
go

alter table FACT_AR_TRAN
   add constraint FK_FATR_DBIN foreign key (BILL_INV_DID)
      references DIM_BILL_INV (BILL_INV_DID)
go

alter table FACT_AR_TRAN
   add constraint FK_FATR_DBPO foreign key (BILL_POL_DID)
      references DIM_BILL_POL (BILL_POL_DID)
go

alter table FACT_AR_TRAN
   add constraint FK_FATR_DBPP1 foreign key (BILL_POL_PRD_DID)
      references DIM_BILL_POL_PER (BILL_POL_PRD_DID)
go

alter table FACT_AR_TRAN
   add constraint FK_FATR_DBPR foreign key (PRDCR_DID)
      references DIM_BILL_PROD (PRDCR_DID)
go

alter table FACT_AR_TRAN
   add constraint FK_FATR_DBWR1 foreign key (WO_DID)
      references DIM_BILL_WRITEOFF (WO_DID)
go

alter table FACT_AR_TRAN
   add constraint FK_FATR_DIDA foreign key (TRANS_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_BILL_CASH_BALANCING
   add constraint FK_FBCB_DBAC foreign key (BILL_ACCT_DID)
      references DIM_BILL_ACCT (BILL_ACCT_DID)
go

alter table FACT_BILL_CASH_BALANCING
   add constraint FK_FBCB_DBDI foreign key (DISB_DID)
      references DIM_BILL_DISB (DISB_DID)
go

alter table FACT_BILL_CASH_BALANCING
   add constraint FK_FBCB_DBMR foreign key (MON_RCVD_DID)
      references DIM_BILL_MONEY_RCVD (MON_RCVD_DID)
go

alter table FACT_BILL_CASH_BALANCING
   add constraint FK_FBCB_DBPO foreign key (BILL_POL_DID)
      references DIM_BILL_POL (BILL_POL_DID)
go

alter table FACT_BILL_CASH_BALANCING
   add constraint FK_FBCB_DBPP foreign key (BILL_POL_PRD_DID)
      references DIM_BILL_POL_PER (BILL_POL_PRD_DID)
go

alter table FACT_BILL_CASH_BALANCING
   add constraint FK_FBCB_DBPR foreign key (PRDCR_DID)
      references DIM_BILL_PROD (PRDCR_DID)
go

alter table FACT_BILL_CASH_BALANCING
   add constraint FK_FBCB_DBSP foreign key (SUSP_PMT_DID)
      references DIM_BILL_SUSP_PMT (SUSP_PMT_DID)
go

alter table FACT_BILL_CASH_BALANCING
   add constraint FK_FBCB_DBUF foreign key (UNAPPLIED_FUND_DID)
      references DIM_BILL_UNAPP_FUND (UNAPPLIED_FUND_DID)
go

alter table FACT_BILL_CASH_BALANCING
   add constraint FK_FBCB_DIMO foreign key (TRANS_MTH_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_BILL_CASH_TRAN
   add constraint FK_FBCT_DBAC foreign key (BILL_ACCT_DID)
      references DIM_BILL_ACCT (BILL_ACCT_DID)
go

alter table FACT_BILL_CASH_TRAN
   add constraint FK_FBCT_DBDI foreign key (DISB_DID)
      references DIM_BILL_DISB (DISB_DID)
go

alter table FACT_BILL_CASH_TRAN
   add constraint FK_FBCT_DBMR foreign key (MON_RCVD_DID)
      references DIM_BILL_MONEY_RCVD (MON_RCVD_DID)
go

alter table FACT_BILL_CASH_TRAN
   add constraint FK_FBCT_DBNW foreign key (NEG_WO_DID)
      references DIM_BILL_NEG_WRITEOFF (NEG_WO_DID)
go

alter table FACT_BILL_CASH_TRAN
   add constraint FK_FBCT_DBPR foreign key (PRDCR_DID)
      references DIM_BILL_PROD (PRDCR_DID)
go

alter table FACT_BILL_CASH_TRAN
   add constraint FK_FBCT_DBSP foreign key (SUSP_PMT_DID)
      references DIM_BILL_SUSP_PMT (SUSP_PMT_DID)
go

alter table FACT_BILL_CASH_TRAN
   add constraint FK_FBCT_DBUF foreign key (UNAPPLIED_FUND_DID)
      references DIM_BILL_UNAPP_FUND (UNAPPLIED_FUND_DID)
go

alter table FACT_BILL_CASH_TRAN
   add constraint FK_FBCT_DIDA foreign key (TRANS_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_BILL_COMM_CAL_YR
   add constraint FK_FBCCY_DBAC foreign key (BILL_ACCT_DID)
      references DIM_BILL_ACCT (BILL_ACCT_DID)
go

alter table FACT_BILL_COMM_CAL_YR
   add constraint FK_FBCCY_DBCH foreign key (CHRG_DID)
      references DIM_BILL_CHRG (CHRG_DID)
go

alter table FACT_BILL_COMM_CAL_YR
   add constraint FK_FBCCY_DBCP foreign key (COMM_PLAN_DID)
      references DIM_BILL_COMM_PLAN (COMM_PLAN_DID)
go

alter table FACT_BILL_COMM_CAL_YR
   add constraint FK_FBCCY_DBPC foreign key (PRDCR_CD_DID)
      references DIM_BILL_PROD_CD (PRDCR_CD_DID)
go

alter table FACT_BILL_COMM_CAL_YR
   add constraint FK_FBCCY_DBPO foreign key (BILL_POL_DID)
      references DIM_BILL_POL (BILL_POL_DID)
go

alter table FACT_BILL_COMM_CAL_YR
   add constraint FK_FBCCY_DBPP foreign key (BILL_POL_PRD_DID)
      references DIM_BILL_POL_PER (BILL_POL_PRD_DID)
go

alter table FACT_BILL_COMM_CAL_YR
   add constraint FK_FBCCY_DBPR foreign key (PRDCR_DID)
      references DIM_BILL_PROD (PRDCR_DID)
go

alter table FACT_BILL_COMM_CAL_YR
   add constraint FK_FBCCY_DIMO foreign key (TRANS_MTH_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DCAC foreign key (ACCT_ID)
      references DIM_CUST_ACCT (ACCT_ID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DCDE foreign key (CA_DEALER_DID)
      references DIM_CA_DEALER (CA_DEALER_DID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DCGS foreign key (GARAGE_SRVC_DID)
      references DIM_CA_GAR_SRVC (GARAGE_SRVC_DID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DCJU foreign key (CA_JURS_DID)
      references DIM_CA_JURS (CA_JURS_DID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DCNI foreign key (NMD_INDIV_DID)
      references DIM_CA_NMD_INDIV (NMD_INDIV_DID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DCPL foreign key (CA_POL_LINE_DID)
      references DIM_CA_POL_LINE (CA_POL_LINE_DID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DCVE foreign key (CA_VEH_DID)
      references DIM_CA_VEH (CA_VEH_DID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DIAG foreign key (AGCY_ID)
      references DIM_AGENCY (AGCY_ID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DIAG1 foreign key (AGNT_ID)
      references DIM_AGENT (AGNT_ID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DICO foreign key (COVG_ID)
      references DIM_COVG (COVG_ID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DIDA foreign key (TRANS_PROC_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DIDA1 foreign key (TRANS_EFF_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DIDA2 foreign key (TRANS_EXP_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DILO1 foreign key (AUTO_DEALER_LOC_ID)
      references DIM_LOC (LOC_ID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DILO2 foreign key (GARAGE_SRVC_LOC_ID)
      references DIM_LOC (LOC_ID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DILO3 foreign key (VEH_GARAGE_LOC_ID)
      references DIM_LOC (LOC_ID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DIMO foreign key (ACCTG_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DIOR foreign key (ORG_ID)
      references DIM_ORGANIZATION (ORG_ID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DIPO foreign key (POLICY_ID)
      references DIM_POLICY (POLICY_ID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DIPR foreign key (PRODUCT_ID)
      references DIM_PRODUCT (PRODUCT_ID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DNIN foreign key (PRIM_NMD_INS_ID)
      references DIM_NAMED_INSURED (NAMED_INSURED_ID)
go

alter table FACT_CA_PREM_TRAN
   add constraint FK_FCPT_DRTY foreign key (REC_TYPE_ID)
      references DIM_RECORD_TYPE (REC_TYPE_ID)
go

alter table FACT_CLAIM_FEATURE_STATUS_TRAN
   add constraint FK_FCFST_DCFE foreign key (CLAIM_FEATURE_ID)
      references DIM_CLAIM_FEATURE (CLAIM_FEATURE_ID)
go

alter table FACT_CLAIM_FEATURE_STATUS_TRAN
   add constraint FK_FCFST_DCST foreign key (CLAIM_STATUS_ID)
      references DIM_CLAIM_STATUS (CLAIM_STATUS_ID)
go

alter table FACT_CLAIM_FEATURE_STATUS_TRAN
   add constraint FK_FCFST_DICL foreign key (CLAIM_ID)
      references DIM_CLAIM (CLAIM_ID)
go

alter table FACT_CLAIM_FEATURE_STATUS_TRAN
   add constraint FK_FCFST_DICL1 foreign key (CLMNT_ID)
      references DIM_CLAIMANT (CLMNT_ID)
go

alter table FACT_CLAIM_FEATURE_STATUS_TRAN
   add constraint FK_FCFST_DIDA foreign key (STATUS_PROC_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_CLAIM_FEATURE_STATUS_TRAN
   add constraint FK_FCFST_DIDA1 foreign key (OPEN_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_CLAIM_FEATURE_STATUS_TRAN
   add constraint FK_FCFST_DIDA2 foreign key (REOPEN_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_CLAIM_FEATURE_STATUS_TRAN
   add constraint FK_FCFST_DIDA3 foreign key (CLOSED_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_CLAIM_STATUS_TRAN
   add constraint FK_FCST_DCST foreign key (CLAIM_STATUS_ID)
      references DIM_CLAIM_STATUS (CLAIM_STATUS_ID)
go

alter table FACT_CLAIM_STATUS_TRAN
   add constraint FK_FCST_DICL foreign key (CLAIM_ID)
      references DIM_CLAIM (CLAIM_ID)
go

alter table FACT_CLAIM_STATUS_TRAN
   add constraint FK_FCST_DIDA foreign key (STATUS_PROC_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_CLAIM_STATUS_TRAN
   add constraint FK_FCST_DIDA1 foreign key (OPEN_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_CLAIM_STATUS_TRAN
   add constraint FK_FCST_DIDA2 foreign key (REOPEN_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_CLAIM_STATUS_TRAN
   add constraint FK_FCST_DIDA3 foreign key (CLOSED_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DCAC1 foreign key (ACCT_ID)
      references DIM_CUST_ACCT (ACCT_ID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DCBI foreign key (CP_BUS_INC_DID)
      references DIM_CP_BI (CP_BUS_INC_DID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DCBL foreign key (CP_BLDG_DID)
      references DIM_CP_BLDG (CP_BLDG_DID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DCBL1 foreign key (CP_BLNKT_DID)
      references DIM_CP_BLNKT (CP_BLNKT_DID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DCCC foreign key (CP_CL_CD_DID)
      references DIM_CP_CL_CODE (CP_CL_CD_DID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DCLO foreign key (CP_LOC_DID)
      references DIM_CP_LOC (CP_LOC_DID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DCOC foreign key (CP_OCC_CL_DID)
      references DIM_CP_OCC_CL (CP_OCC_CL_DID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DCPL1 foreign key (CP_POL_LINE_DID)
      references DIM_CP_POL_LINE (CP_POL_LINE_DID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DCPP foreign key (CP_PERS_PROP_DID)
      references DIM_CP_PP (CP_PERS_PROP_DID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DCSC foreign key (CP_SPCL_CL_DID)
      references DIM_CP_SPCL_CL (CP_SPCL_CL_DID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DCSCB foreign key (CP_SPCL_CL_BUS_INC_DID)
      references DIM_CP_SPCL_CL_BI (CP_SPCL_CL_BUS_INC_DID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DIAG2 foreign key (AGCY_ID)
      references DIM_AGENCY (AGCY_ID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DIAG3 foreign key (AGNT_ID)
      references DIM_AGENT (AGNT_ID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DIBL foreign key (BLDG_DID)
      references DIM_BLDG (BLDG_DID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DICO1 foreign key (COVG_ID)
      references DIM_COVG (COVG_ID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DIDA3 foreign key (TRANS_PROC_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DIDA4 foreign key (TRANS_EFF_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DIDA5 foreign key (TRANS_EXP_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DILO foreign key (CP_LOC_LOC_ID)
      references DIM_LOC (LOC_ID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DIMO1 foreign key (ACCTG_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DIOR1 foreign key (ORG_ID)
      references DIM_ORGANIZATION (ORG_ID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DIPO1 foreign key (POLICY_ID)
      references DIM_POLICY (POLICY_ID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DIPR1 foreign key (PRODUCT_ID)
      references DIM_PRODUCT (PRODUCT_ID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DNIN1 foreign key (PRIM_NMD_INS_ID)
      references DIM_NAMED_INSURED (NAMED_INSURED_ID)
go

alter table FACT_CP_PREM_TRAN
   add constraint FK_FCPT_DRTY1 foreign key (REC_TYPE_ID)
      references DIM_RECORD_TYPE (REC_TYPE_ID)
go

alter table FACT_GENERAL_TRAN
   add constraint FK_FGTR_DBAC foreign key (CR_ACCT_DID)
      references DIM_BILL_ACCT (BILL_ACCT_DID)
go

alter table FACT_GENERAL_TRAN
   add constraint FK_FGTR_DBAC1 foreign key (SRCE_ACCT_DID)
      references DIM_BILL_ACCT (BILL_ACCT_DID)
go

alter table FACT_GENERAL_TRAN
   add constraint FK_FGTR_DBAC2 foreign key (TARGET_ACCT_DID)
      references DIM_BILL_ACCT (BILL_ACCT_DID)
go

alter table FACT_GENERAL_TRAN
   add constraint FK_FGTR_DBAC3 foreign key (SUSP_ACCT_DID)
      references DIM_BILL_ACCT (BILL_ACCT_DID)
go

alter table FACT_GENERAL_TRAN
   add constraint FK_FGTR_DBCR foreign key (CR_DID)
      references DIM_BILL_CREDIT (CR_DID)
go

alter table FACT_GENERAL_TRAN
   add constraint FK_FGTR_DBFT foreign key (FUNDS_TFR_DID)
      references DIM_BILL_FUNDS_TRNFR (FUNDS_TFR_DID)
go

alter table FACT_GENERAL_TRAN
   add constraint FK_FGTR_DBNDI foreign key (NON_RCVBL_DISTR_ITEM_DID)
      references DIM_BILL_NR_DISTR_ITEM (NON_RCVBL_DISTR_ITEM_DID)
go

alter table FACT_GENERAL_TRAN
   add constraint FK_FGTR_DBPR foreign key (SUSP_PRDCR_DID)
      references DIM_BILL_PROD (PRDCR_DID)
go

alter table FACT_GENERAL_TRAN
   add constraint FK_FGTR_DBPR1 foreign key (SRCE_PRDCR_DID)
      references DIM_BILL_PROD (PRDCR_DID)
go

alter table FACT_GENERAL_TRAN
   add constraint FK_FGTR_DBPR2 foreign key (TARGET_PRDCR_DID)
      references DIM_BILL_PROD (PRDCR_DID)
go

alter table FACT_GENERAL_TRAN
   add constraint FK_FGTR_DIDA foreign key (TRANS_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DCAC foreign key (ACCT_ID)
      references DIM_CUST_ACCT (ACCT_ID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DGCC foreign key (GL_CL_CD_DID)
      references DIM_GL_CL_CODE (GL_CL_CD_DID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DGCP foreign key (GL_COVG_PART_DID)
      references DIM_GL_COVG_PART (GL_COVG_PART_DID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DGEX foreign key (GL_EXPSR_DID)
      references DIM_GL_EXPSR (GL_EXPSR_DID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DGPL foreign key (GL_POL_LINE_DID)
      references DIM_GL_POL_LINE (GL_POL_LINE_DID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DIAG foreign key (AGCY_ID)
      references DIM_AGENCY (AGCY_ID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DIAG1 foreign key (AGNT_ID)
      references DIM_AGENT (AGNT_ID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DICO foreign key (COVG_ID)
      references DIM_COVG (COVG_ID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DIDA foreign key (TRANS_PROC_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DIDA1 foreign key (TRANS_EFF_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DIDA2 foreign key (TRANS_EXP_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DILO foreign key (EXPSR_LOC_ID)
      references DIM_LOC (LOC_ID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DIMO foreign key (ACCTG_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DIOR foreign key (ORG_ID)
      references DIM_ORGANIZATION (ORG_ID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DIPO foreign key (POLICY_ID)
      references DIM_POLICY (POLICY_ID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DIPR foreign key (PRODUCT_ID)
      references DIM_PRODUCT (PRODUCT_ID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DNIN foreign key (PRIM_NMD_INS_ID)
      references DIM_NAMED_INSURED (NAMED_INSURED_ID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DNIN1 foreign key (ADDL_NMD_INS_ID)
      references DIM_NAMED_INSURED (NAMED_INSURED_ID)
go

alter table FACT_GL_PREM_TRAN
   add constraint FK_FGPT_DRTY foreign key (REC_TYPE_ID)
      references DIM_RECORD_TYPE (REC_TYPE_ID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DCAC foreign key (ACCT_ID)
      references DIM_CUST_ACCT (ACCT_ID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DHDW foreign key (HO_DWLNG_DID)
      references DIM_HO_DWLNG (HO_DWLNG_DID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DHLO foreign key (HO_LOC_DID)
      references DIM_HO_LOC (HO_LOC_DID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DHSI foreign key (HO_SI_DID)
      references DIM_HO_SI (HO_SI_DID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DIAG foreign key (AGNT_ID)
      references DIM_AGENT (AGNT_ID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DIAG1 foreign key (AGCY_ID)
      references DIM_AGENCY (AGCY_ID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DICO foreign key (COVG_ID)
      references DIM_COVG (COVG_ID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DIDA foreign key (TRANS_PROC_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DIDA1 foreign key (TRANS_EFF_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DIDA2 foreign key (TRANS_EXP_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DILO foreign key (HO_SI_LOC_ID)
      references DIM_LOC (LOC_ID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DILO1 foreign key (HO_OTHR_LOC_LOC_ID)
      references DIM_LOC (LOC_ID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DILO2 foreign key (HO_DWLNG_LOC_ID)
      references DIM_LOC (LOC_ID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DIMO foreign key (ACCTG_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DIOR foreign key (ORG_ID)
      references DIM_ORGANIZATION (ORG_ID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DIPO foreign key (POLICY_ID)
      references DIM_POLICY (POLICY_ID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DIPR foreign key (PRODUCT_ID)
      references DIM_PRODUCT (PRODUCT_ID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DNIN foreign key (PRIM_NAMED_INSURED_ID)
      references DIM_NAMED_INSURED (NAMED_INSURED_ID)
go

alter table FACT_HO_PREM_TRAN
   add constraint FK_FHPT_DRTY foreign key (REC_TYPE_ID)
      references DIM_RECORD_TYPE (REC_TYPE_ID)
go

alter table FACT_LOSS_CAL_YR
   add constraint FK_FLCY_DIAG foreign key (AGNT_ID)
      references DIM_AGENT (AGNT_ID)
go

alter table FACT_LOSS_CAL_YR
   add constraint FK_FLCY_DIAG1 foreign key (AGCY_ID)
      references DIM_AGENCY (AGCY_ID)
go

alter table FACT_LOSS_CAL_YR
   add constraint FK_FLCY_DICO foreign key (COVG_ID)
      references DIM_COVG (COVG_ID)
go

alter table FACT_LOSS_CAL_YR
   add constraint FK_FLCY_DIMO foreign key (ACCTG_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_LOSS_CAL_YR
   add constraint FK_FLCY_DIOR foreign key (ORG_ID)
      references DIM_ORGANIZATION (ORG_ID)
go

alter table FACT_LOSS_CAL_YR
   add constraint FK_FLCY_DIPR foreign key (PRODUCT_ID)
      references DIM_PRODUCT (PRODUCT_ID)
go

alter table FACT_LOSS_CAL_YR
   add constraint FK_FLCY_DRTY foreign key (REC_TYPE_ID)
      references DIM_RECORD_TYPE (REC_TYPE_ID)
go

alter table FACT_LOSS_CAL_YR_BASE
   add constraint FK_FLCYB_DCAC foreign key (ACCT_ID)
      references DIM_CUST_ACCT (ACCT_ID)
go

alter table FACT_LOSS_CAL_YR_BASE
   add constraint FK_FLCYB_DCFE foreign key (CLAIM_FEATURE_ID)
      references DIM_CLAIM_FEATURE (CLAIM_FEATURE_ID)
go

alter table FACT_LOSS_CAL_YR_BASE
   add constraint FK_FLCYB_DCST foreign key (MTH_END_CLAIM_STATUS_ID)
      references DIM_CLAIM_STATUS (CLAIM_STATUS_ID)
go

alter table FACT_LOSS_CAL_YR_BASE
   add constraint FK_FLCYB_DCST1 foreign key (MTH_END_FEATURE_STATUS_ID)
      references DIM_CLAIM_STATUS (CLAIM_STATUS_ID)
go

alter table FACT_LOSS_CAL_YR_BASE
   add constraint FK_FLCYB_DIAG foreign key (AGNT_ID)
      references DIM_AGENT (AGNT_ID)
go

alter table FACT_LOSS_CAL_YR_BASE
   add constraint FK_FLCYB_DIAG1 foreign key (AGCY_ID)
      references DIM_AGENCY (AGCY_ID)
go

alter table FACT_LOSS_CAL_YR_BASE
   add constraint FK_FLCYB_DICL foreign key (CLAIM_ID)
      references DIM_CLAIM (CLAIM_ID)
go

alter table FACT_LOSS_CAL_YR_BASE
   add constraint FK_FLCYB_DICL1 foreign key (CLMNT_ID)
      references DIM_CLAIMANT (CLMNT_ID)
go

alter table FACT_LOSS_CAL_YR_BASE
   add constraint FK_FLCYB_DICO foreign key (COVG_ID)
      references DIM_COVG (COVG_ID)
go

alter table FACT_LOSS_CAL_YR_BASE
   add constraint FK_FLCYB_DILO foreign key (LOC_ID)
      references DIM_LOC (LOC_ID)
go

alter table FACT_LOSS_CAL_YR_BASE
   add constraint FK_FLCYB_DIMO foreign key (ACCTG_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_LOSS_CAL_YR_BASE
   add constraint FK_FLCYB_DIOR foreign key (ORG_ID)
      references DIM_ORGANIZATION (ORG_ID)
go

alter table FACT_LOSS_CAL_YR_BASE
   add constraint FK_FLCYB_DIPO foreign key (POLICY_ID)
      references DIM_POLICY (POLICY_ID)
go

alter table FACT_LOSS_CAL_YR_BASE
   add constraint FK_FLCYB_DIPR foreign key (PRODUCT_ID)
      references DIM_PRODUCT (PRODUCT_ID)
go

alter table FACT_LOSS_CAL_YR_BASE
   add constraint FK_FLCYB_DIVE foreign key (VENDOR_ID)
      references DIM_VENDOR (VENDOR_ID)
go

alter table FACT_LOSS_CAL_YR_BASE
   add constraint FK_FLCYB_DNIN foreign key (PRIM_NAMED_INSURED_ID)
      references DIM_NAMED_INSURED (NAMED_INSURED_ID)
go

alter table FACT_LOSS_CAL_YR_BASE
   add constraint FK_FLCYB_DRTY foreign key (REC_TYPE_ID)
      references DIM_RECORD_TYPE (REC_TYPE_ID)
go

alter table FACT_LOSS_CAL_YR_PRODUCT
   add constraint FK_FLCYP_DICO foreign key (COVG_ID)
      references DIM_COVG (COVG_ID)
go

alter table FACT_LOSS_CAL_YR_PRODUCT
   add constraint FK_FLCYP_DIMO foreign key (ACCTG_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_LOSS_CAL_YR_PRODUCT
   add constraint FK_FLCYP_DIOR foreign key (ORG_ID)
      references DIM_ORGANIZATION (ORG_ID)
go

alter table FACT_LOSS_CAL_YR_PRODUCT
   add constraint FK_FLCYP_DIPR foreign key (PRODUCT_ID)
      references DIM_PRODUCT (PRODUCT_ID)
go

alter table FACT_LOSS_CAL_YR_PRODUCT
   add constraint FK_FLCYP_DRTY foreign key (REC_TYPE_ID)
      references DIM_RECORD_TYPE (REC_TYPE_ID)
go

alter table FACT_LOSS_DEV_AGG
   add constraint FK_FLDA_DICA foreign key (CATASTROPHE_ID)
      references DIM_CATASTROPHE (CATASTROPHE_ID)
go

alter table FACT_LOSS_DEV_AGG
   add constraint FK_FLDA_DICO foreign key (COVG_ID)
      references DIM_COVG (COVG_ID)
go

alter table FACT_LOSS_DEV_AGG
   add constraint FK_FLDA_DIMO foreign key (LOSS_MTH_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_LOSS_DEV_AGG
   add constraint FK_FLDA_DIMO1 foreign key (ACCTG_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_LOSS_DEV_AGG
   add constraint FK_FLDA_DIOR foreign key (ORG_ID)
      references DIM_ORGANIZATION (ORG_ID)
go

alter table FACT_LOSS_DEV_AGG
   add constraint FK_FLDA_DIPR foreign key (PRODUCT_ID)
      references DIM_PRODUCT (PRODUCT_ID)
go

alter table FACT_LOSS_DEV_AGG
   add constraint FK_FLDA_DIRE foreign key (REGION_ID)
      references DIM_REGION (REGION_ID)
go

alter table FACT_LOSS_DEV_AGG
   add constraint FK_FLDA_DLTI foreign key (LOSS_TYPE_INFO_ID)
      references DIM_LOSS_TYPE_INFO (LOSS_TYPE_INFO_ID)
go

alter table FACT_LOSS_DEV_AGG
   add constraint FK_FLDA_DMAG foreign key (MTH_AGING_ID)
      references DIM_MONTH_AGING (MTH_AGING_ID)
go

alter table FACT_LOSS_ITD
   add constraint FK_FLIT_DCAC foreign key (ACCT_ID)
      references DIM_CUST_ACCT (ACCT_ID)
go

alter table FACT_LOSS_ITD
   add constraint FK_FLIT_DCFE foreign key (CLAIM_FEATURE_ID)
      references DIM_CLAIM_FEATURE (CLAIM_FEATURE_ID)
go

alter table FACT_LOSS_ITD
   add constraint FK_FLIT_DCST foreign key (CLAIM_FEATURE_STATUS_ID)
      references DIM_CLAIM_STATUS (CLAIM_STATUS_ID)
go

alter table FACT_LOSS_ITD
   add constraint FK_FLIT_DCST1 foreign key (CLAIM_STATUS_ID)
      references DIM_CLAIM_STATUS (CLAIM_STATUS_ID)
go

alter table FACT_LOSS_ITD
   add constraint FK_FLIT_DICL foreign key (CLAIM_ID)
      references DIM_CLAIM (CLAIM_ID)
go

alter table FACT_LOSS_ITD
   add constraint FK_FLIT_DICL1 foreign key (CLMNT_ID)
      references DIM_CLAIMANT (CLMNT_ID)
go

alter table FACT_LOSS_ITD
   add constraint FK_FLIT_DICO foreign key (COVG_ID)
      references DIM_COVG (COVG_ID)
go

alter table FACT_LOSS_ITD
   add constraint FK_FLIT_DIMO foreign key (ACCTG_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_LOSS_ITD
   add constraint FK_FLIT_DIOR foreign key (ORG_ID)
      references DIM_ORGANIZATION (ORG_ID)
go

alter table FACT_LOSS_ITD
   add constraint FK_FLIT_DIPO foreign key (POLICY_ID)
      references DIM_POLICY (POLICY_ID)
go

alter table FACT_LOSS_ITD
   add constraint FK_FLIT_DIPR foreign key (PRODUCT_ID)
      references DIM_PRODUCT (PRODUCT_ID)
go

alter table FACT_LOSS_ITD
   add constraint FK_FLIT_DNIN foreign key (PRIM_NAMED_INSURED_ID)
      references DIM_NAMED_INSURED (NAMED_INSURED_ID)
go

alter table FACT_LOSS_ITD
   add constraint FK_FLIT_DRTY foreign key (REC_TYPE_ID)
      references DIM_RECORD_TYPE (REC_TYPE_ID)
go

alter table FACT_LOSS_TRAN
   add constraint FK_FLTR_DCAC foreign key (ACCT_ID)
      references DIM_CUST_ACCT (ACCT_ID)
go

alter table FACT_LOSS_TRAN
   add constraint FK_FLTR_DCFE foreign key (CLAIM_FEATURE_ID)
      references DIM_CLAIM_FEATURE (CLAIM_FEATURE_ID)
go

alter table FACT_LOSS_TRAN
   add constraint FK_FLTR_DIAG foreign key (AGNT_ID)
      references DIM_AGENT (AGNT_ID)
go

alter table FACT_LOSS_TRAN
   add constraint FK_FLTR_DIAG1 foreign key (AGCY_ID)
      references DIM_AGENCY (AGCY_ID)
go

alter table FACT_LOSS_TRAN
   add constraint FK_FLTR_DICL foreign key (CLMNT_ID)
      references DIM_CLAIMANT (CLMNT_ID)
go

alter table FACT_LOSS_TRAN
   add constraint FK_FLTR_DICL1 foreign key (CLAIM_ID)
      references DIM_CLAIM (CLAIM_ID)
go

alter table FACT_LOSS_TRAN
   add constraint FK_FLTR_DICO foreign key (COVG_ID)
      references DIM_COVG (COVG_ID)
go

alter table FACT_LOSS_TRAN
   add constraint FK_FLTR_DIDA foreign key (TRANS_PROC_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_LOSS_TRAN
   add constraint FK_FLTR_DIDA1 foreign key (CHECK_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_LOSS_TRAN
   add constraint FK_FLTR_DILO foreign key (LOC_ID)
      references DIM_LOC (LOC_ID)
go

alter table FACT_LOSS_TRAN
   add constraint FK_FLTR_DIMO foreign key (ACCTG_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_LOSS_TRAN
   add constraint FK_FLTR_DIOR foreign key (ORG_ID)
      references DIM_ORGANIZATION (ORG_ID)
go

alter table FACT_LOSS_TRAN
   add constraint FK_FLTR_DIPO foreign key (POLICY_ID)
      references DIM_POLICY (POLICY_ID)
go

alter table FACT_LOSS_TRAN
   add constraint FK_FLTR_DIPR foreign key (PRODUCT_ID)
      references DIM_PRODUCT (PRODUCT_ID)
go

alter table FACT_LOSS_TRAN
   add constraint FK_FLTR_DIVE foreign key (VENDOR_ID)
      references DIM_VENDOR (VENDOR_ID)
go

alter table FACT_LOSS_TRAN
   add constraint FK_FLTR_DNIN foreign key (PRIM_NAMED_INSURED_ID)
      references DIM_NAMED_INSURED (NAMED_INSURED_ID)
go

alter table FACT_LOSS_TRAN
   add constraint FK_FLTR_DRTY foreign key (REC_TYPE_ID)
      references DIM_RECORD_TYPE (REC_TYPE_ID)
go

alter table FACT_PA_PREM_TRAN
   add constraint FK_FPPT_DCAC foreign key (ACCT_ID)
      references DIM_CUST_ACCT (ACCT_ID)
go

alter table FACT_PA_PREM_TRAN
   add constraint FK_FPPT_DIAG foreign key (AGNT_ID)
      references DIM_AGENT (AGNT_ID)
go

alter table FACT_PA_PREM_TRAN
   add constraint FK_FPPT_DIAG1 foreign key (AGCY_ID)
      references DIM_AGENCY (AGCY_ID)
go

alter table FACT_PA_PREM_TRAN
   add constraint FK_FPPT_DICO foreign key (COVG_ID)
      references DIM_COVG (COVG_ID)
go

alter table FACT_PA_PREM_TRAN
   add constraint FK_FPPT_DIDA foreign key (TRANS_EFF_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_PA_PREM_TRAN
   add constraint FK_FPPT_DIDA1 foreign key (TRANS_PROC_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_PA_PREM_TRAN
   add constraint FK_FPPT_DIDA2 foreign key (TRANS_EXP_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_PA_PREM_TRAN
   add constraint FK_FPPT_DILO foreign key (GARAGE_LOC_ID)
      references DIM_LOC (LOC_ID)
go

alter table FACT_PA_PREM_TRAN
   add constraint FK_FPPT_DIMO foreign key (ACCTG_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_PA_PREM_TRAN
   add constraint FK_FPPT_DIOR foreign key (ORG_ID)
      references DIM_ORGANIZATION (ORG_ID)
go

alter table FACT_PA_PREM_TRAN
   add constraint FK_FPPT_DIPO foreign key (POLICY_ID)
      references DIM_POLICY (POLICY_ID)
go

alter table FACT_PA_PREM_TRAN
   add constraint FK_FPPT_DIPR foreign key (PRODUCT_ID)
      references DIM_PRODUCT (PRODUCT_ID)
go

alter table FACT_PA_PREM_TRAN
   add constraint FK_FPPT_DNIN foreign key (PRIM_NAMED_INSURED_ID)
      references DIM_NAMED_INSURED (NAMED_INSURED_ID)
go

alter table FACT_PA_PREM_TRAN
   add constraint FK_FPPT_DPVE foreign key (PA_VEH_DID)
      references DIM_PA_VEH (PA_VEH_DID)
go

alter table FACT_PA_PREM_TRAN
   add constraint FK_FPPT_DRTY foreign key (REC_TYPE_ID)
      references DIM_RECORD_TYPE (REC_TYPE_ID)
go

alter table FACT_POLICY_STATUS_TRAN
   add constraint FK_FPST_DCAC foreign key (ACCT_ID)
      references DIM_CUST_ACCT (ACCT_ID)
go

alter table FACT_POLICY_STATUS_TRAN
   add constraint FK_FPST_DIDA foreign key (STATUS_EFF_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_POLICY_STATUS_TRAN
   add constraint FK_FPST_DIDA1 foreign key (STATUS_PROC_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_POLICY_STATUS_TRAN
   add constraint FK_FPST_DIMO foreign key (ACCTG_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_POLICY_STATUS_TRAN
   add constraint FK_FPST_DIPO foreign key (POLICY_ID)
      references DIM_POLICY (POLICY_ID)
go

alter table FACT_POLICY_STATUS_TRAN
   add constraint FK_FPST_DNIN foreign key (PRIM_NAMED_INSURED_ID)
      references DIM_NAMED_INSURED (NAMED_INSURED_ID)
go

alter table FACT_POLICY_STATUS_TRAN
   add constraint FK_FPST_DPST foreign key (POL_STATUS_ID)
      references DIM_POLICY_STATUS (POL_STATUS_ID)
go

alter table FACT_PREM_CAL_YR
   add constraint FK_FPCY_DIAG foreign key (AGCY_ID)
      references DIM_AGENCY (AGCY_ID)
go

alter table FACT_PREM_CAL_YR
   add constraint FK_FPCY_DIAG1 foreign key (AGNT_ID)
      references DIM_AGENT (AGNT_ID)
go

alter table FACT_PREM_CAL_YR
   add constraint FK_FPCY_DICO foreign key (COVG_ID)
      references DIM_COVG (COVG_ID)
go

alter table FACT_PREM_CAL_YR
   add constraint FK_FPCY_DIMO foreign key (ACCTG_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_PREM_CAL_YR
   add constraint FK_FPCY_DIOR foreign key (ORG_ID)
      references DIM_ORGANIZATION (ORG_ID)
go

alter table FACT_PREM_CAL_YR
   add constraint FK_FPCY_DIPR foreign key (PRODUCT_ID)
      references DIM_PRODUCT (PRODUCT_ID)
go

alter table FACT_PREM_CAL_YR
   add constraint FK_FPCY_DRTY foreign key (REC_TYPE_ID)
      references DIM_RECORD_TYPE (REC_TYPE_ID)
go

alter table FACT_PREM_CAL_YR_AGENT_GRWTH
   add constraint FK_FPCYAG_DIAG foreign key (AGCY_ID)
      references DIM_AGENCY (AGCY_ID)
go

alter table FACT_PREM_CAL_YR_AGENT_GRWTH
   add constraint FK_FPCYAG_DIAG1 foreign key (AGNT_ID)
      references DIM_AGENT (AGNT_ID)
go

alter table FACT_PREM_CAL_YR_AGENT_GRWTH
   add constraint FK_FPCYAG_DIMO foreign key (ACCTG_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_PREM_CAL_YR_AGENT_GRWTH
   add constraint FK_FPCYAG_DIOR foreign key (ORG_ID)
      references DIM_ORGANIZATION (ORG_ID)
go

alter table FACT_PREM_CAL_YR_AGENT_GRWTH
   add constraint FK_FPCYAG_DIPR foreign key (PRODUCT_ID)
      references DIM_PRODUCT (PRODUCT_ID)
go

alter table FACT_PREM_CAL_YR_AGENT_GRWTH
   add constraint FK_FPCYAG_DRTY foreign key (REC_TYPE_ID)
      references DIM_RECORD_TYPE (REC_TYPE_ID)
go

alter table FACT_PREM_CAL_YR_BASE
   add constraint FK_FPCYB_DCAC foreign key (ACCT_ID)
      references DIM_CUST_ACCT (ACCT_ID)
go

alter table FACT_PREM_CAL_YR_BASE
   add constraint FK_FPCYB_DIAG foreign key (AGNT_ID)
      references DIM_AGENT (AGNT_ID)
go

alter table FACT_PREM_CAL_YR_BASE
   add constraint FK_FPCYB_DIAG1 foreign key (AGCY_ID)
      references DIM_AGENCY (AGCY_ID)
go

alter table FACT_PREM_CAL_YR_BASE
   add constraint FK_FPCYB_DICO foreign key (COVG_ID)
      references DIM_COVG (COVG_ID)
go

alter table FACT_PREM_CAL_YR_BASE
   add constraint FK_FPCYB_DILO foreign key (LOC_ID)
      references DIM_LOC (LOC_ID)
go

alter table FACT_PREM_CAL_YR_BASE
   add constraint FK_FPCYB_DIMO foreign key (ACCTG_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_PREM_CAL_YR_BASE
   add constraint FK_FPCYB_DIOR foreign key (ORG_ID)
      references DIM_ORGANIZATION (ORG_ID)
go

alter table FACT_PREM_CAL_YR_BASE
   add constraint FK_FPCYB_DIPO foreign key (POLICY_ID)
      references DIM_POLICY (POLICY_ID)
go

alter table FACT_PREM_CAL_YR_BASE
   add constraint FK_FPCYB_DIPR foreign key (PRODUCT_ID)
      references DIM_PRODUCT (PRODUCT_ID)
go

alter table FACT_PREM_CAL_YR_BASE
   add constraint FK_FPCYB_DNIN foreign key (PRIM_NAMED_INSURED_ID)
      references DIM_NAMED_INSURED (NAMED_INSURED_ID)
go

alter table FACT_PREM_CAL_YR_BASE
   add constraint FK_FPCYB_DPST foreign key (MTH_END_POL_STATUS_ID)
      references DIM_POLICY_STATUS (POL_STATUS_ID)
go

alter table FACT_PREM_CAL_YR_BASE
   add constraint FK_FPCYB_DRTY foreign key (REC_TYPE_ID)
      references DIM_RECORD_TYPE (REC_TYPE_ID)
go

alter table FACT_PREM_CAL_YR_PRODUCT
   add constraint FK_FPCYP_DICO foreign key (COVG_ID)
      references DIM_COVG (COVG_ID)
go

alter table FACT_PREM_CAL_YR_PRODUCT
   add constraint FK_FPCYP_DIMO foreign key (ACCTG_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_PREM_CAL_YR_PRODUCT
   add constraint FK_FPCYP_DIOR foreign key (ORG_ID)
      references DIM_ORGANIZATION (ORG_ID)
go

alter table FACT_PREM_CAL_YR_PRODUCT
   add constraint FK_FPCYP_DIPR foreign key (PRODUCT_ID)
      references DIM_PRODUCT (PRODUCT_ID)
go

alter table FACT_PREM_CAL_YR_PRODUCT
   add constraint FK_FPCYP_DRTY foreign key (REC_TYPE_ID)
      references DIM_RECORD_TYPE (REC_TYPE_ID)
go

alter table FACT_PREM_ITD
   add constraint FK_FPIT_DCAC foreign key (ACCT_ID)
      references DIM_CUST_ACCT (ACCT_ID)
go

alter table FACT_PREM_ITD
   add constraint FK_FPIT_DIAG foreign key (AGNT_ID)
      references DIM_AGENT (AGNT_ID)
go

alter table FACT_PREM_ITD
   add constraint FK_FPIT_DIAG1 foreign key (AGCY_ID)
      references DIM_AGENCY (AGCY_ID)
go

alter table FACT_PREM_ITD
   add constraint FK_FPIT_DIMO foreign key (ACCTG_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_PREM_ITD
   add constraint FK_FPIT_DIOR foreign key (ORG_ID)
      references DIM_ORGANIZATION (ORG_ID)
go

alter table FACT_PREM_ITD
   add constraint FK_FPIT_DIPO foreign key (POLICY_ID)
      references DIM_POLICY (POLICY_ID)
go

alter table FACT_PREM_ITD
   add constraint FK_FPIT_DIPR foreign key (PRODUCT_ID)
      references DIM_PRODUCT (PRODUCT_ID)
go

alter table FACT_PREM_ITD
   add constraint FK_FPIT_DIYE foreign key (POL_YR)
      references DIM_YEAR (YR_ID)
go

alter table FACT_PREM_ITD
   add constraint FK_FPIT_DNIN foreign key (PRIM_NAMED_INSURED_ID)
      references DIM_NAMED_INSURED (NAMED_INSURED_ID)
go

alter table FACT_PREM_ITD
   add constraint FK_FPIT_DPST foreign key (POL_STATUS_ID)
      references DIM_POLICY_STATUS (POL_STATUS_ID)
go

alter table FACT_PREM_ITD
   add constraint FK_FPIT_DRTY foreign key (REC_TYPE_ID)
      references DIM_RECORD_TYPE (REC_TYPE_ID)
go

alter table FACT_PREM_TRAN
   add constraint FK_FPTR_DCAC foreign key (ACCT_ID)
      references DIM_CUST_ACCT (ACCT_ID)
go

alter table FACT_PREM_TRAN
   add constraint FK_FPTR_DIAG foreign key (AGNT_ID)
      references DIM_AGENT (AGNT_ID)
go

alter table FACT_PREM_TRAN
   add constraint FK_FPTR_DIAG1 foreign key (AGCY_ID)
      references DIM_AGENCY (AGCY_ID)
go

alter table FACT_PREM_TRAN
   add constraint FK_FPTR_DICO foreign key (COVG_ID)
      references DIM_COVG (COVG_ID)
go

alter table FACT_PREM_TRAN
   add constraint FK_FPTR_DIDA foreign key (TRANS_PROC_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_PREM_TRAN
   add constraint FK_FPTR_DIDA1 foreign key (TRANS_EFF_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_PREM_TRAN
   add constraint FK_FPTR_DIDA2 foreign key (TRANS_EXP_DT_ID)
      references DIM_DATE (DT_ID)
go

alter table FACT_PREM_TRAN
   add constraint FK_FPTR_DILO foreign key (LOC_ID)
      references DIM_LOC (LOC_ID)
go

alter table FACT_PREM_TRAN
   add constraint FK_FPTR_DIMO foreign key (ACCTG_PRD_ID)
      references DIM_MONTH (MTH_ID)
go

alter table FACT_PREM_TRAN
   add constraint FK_FPTR_DIOR foreign key (ORG_ID)
      references DIM_ORGANIZATION (ORG_ID)
go

alter table FACT_PREM_TRAN
   add constraint FK_FPTR_DIPO foreign key (POLICY_ID)
      references DIM_POLICY (POLICY_ID)
go

alter table FACT_PREM_TRAN
   add constraint FK_FPTR_DIPR foreign key (PRODUCT_ID)
      references DIM_PRODUCT (PRODUCT_ID)
go

alter table FACT_PREM_TRAN
   add constraint FK_FPTR_DNIN foreign key (PRIM_NAMED_INSURED_ID)
      references DIM_NAMED_INSURED (NAMED_INSURED_ID)
go

alter table FACT_PREM_TRAN
   add constraint FK_FPTR_DRTY foreign key (REC_TYPE_ID)
      references DIM_RECORD_TYPE (REC_TYPE_ID)
go

insert into SYSTEM_INFO values ('Product', '9.0.0');
go

